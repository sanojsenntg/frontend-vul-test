import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AdminModule } from './admin/admin.module';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { DispatcherModule } from './dispatcher/dispatcher.module';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { ModuleWithProviders } from '@angular/core';
import { DispatcherRoutingModule } from './app-routing/dispatcher-routing.module';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { HomeModule } from './home/home.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MomentModule } from 'angular2-moment';
import { MomentTimezoneModule } from 'angular-moment-timezone';
import { DeleteDialogComponent } from './common/dialog/delete-dialog/delete-dialog.component'
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { ToastModule, ToastOptions } from 'ng2-toastr';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { ImageUploadModule } from "angular2-image-upload";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RatingModule } from "ngx-rating";
import { RestricedAccesComponent } from './common/dialog/restriced-acces/restriced-acces.component';
import { UserIdleModule } from 'angular-user-idle';
import { DriverModule } from './driver/driver.module';
import { CancelReasonComponent } from './common/dialog/cancel-reason/cancel-reason.component';
import { FormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { OrderService } from './common/services/order/order.service';
import { PoiDialogComponent } from './common/dialog/poi-dialog/poi-dialog.component';
import { CustomOption } from './common/directive/toast-options';
import { FreeVehicleService } from './common/services/free-vehicles/free_vehicle.service';
import { FreeVehicleRestService } from './common/services/free-vehicles/freevehiclerest.service';
import { ZoneRestService } from './common/services/zones/zonerest.service';
import { DriverblockreasonComponent } from './common/dialog/driverblockreason/driverblockreason.component';
import { AdditionalSplitupComponent } from './common/dialog/additional-splitup/additional-splitup.component';
import { DeviceblockreasonComponent } from './common/dialog/deviceblockreason/deviceblockreason.component';
import { CancelSelectedComponent } from './common/dialog/cancel-selected/cancel-selected.component';
import { ZoneSavingComponent } from './common/dialog/zone-saving/zone-saving.component';
import { RenameSubzoneComponent } from './common/dialog/rename-subzone/rename-subzone.component';
import { KeyIndicatorsComponent } from './common/dialog/key-indicators/key-indicators.component';
import { TransactionLogComponent } from './common/dialog/transaction-log/transaction-log.component';
import { EditLogComponent } from './common/dialog/edit-log/edit-log.component';
import { EditOrderComponent } from './common/dialog/edit-order/edit-order.component';
import { NguiMapModule } from '@ngui/map';
import { environment } from '../environments/environment';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AddCommentComponent } from './common/dialog/add-comment/add-comment.component';
import { EncDecService } from './common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { SplitupdataComponent } from './common/dialog/splitupdata/splitupdata.component';
import { DispatchNotificationsComponent } from './common/dialog/dispatch-notifications/dispatch-notifications.component'

@NgModule({
  declarations: [
    AppComponent,
    DeleteDialogComponent,
    RestricedAccesComponent,
    CancelReasonComponent,
    DriverblockreasonComponent,
    DeviceblockreasonComponent,
    PoiDialogComponent,
    CancelSelectedComponent,
    ZoneSavingComponent,
    RenameSubzoneComponent,
    KeyIndicatorsComponent,
    TransactionLogComponent,
    AdditionalSplitupComponent,
    EditLogComponent,
    EditOrderComponent,
    AddCommentComponent,
    SplitupdataComponent,
    DispatchNotificationsComponent,
  ],
  imports: [
    MatTooltipModule,
    MatCheckboxModule,
    FormsModule,
    ToasterModule, 
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DispatcherRoutingModule,
    DispatcherModule,
    AdminModule,
    HomeModule,
    //ToasterModule,
    NgbModule.forRoot(),
    FlashMessagesModule.forRoot(),
    MomentModule,
    MomentTimezoneModule,
    MatDialogModule,
    MatSelectModule,
    NgIdleKeepaliveModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    ToastModule.forRoot(),
    ImageUploadModule.forRoot(),
    RatingModule,
    DriverModule,
    // Optionally you can set time for `idle`, `timeout` and `ping` in seconds.
    // Default values: `idle` is 1200 (20 minutes), `timeout` is 10 (10 seconds) 
    // and `ping` is 120 (2 minutes).
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    UserIdleModule.forRoot({ idle: 1200, timeout: 10, ping: 120 }),
    NguiMapModule.forRoot
      ({ apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.map_key + '&libraries=visualization,places,drawing,geometry' }),
    ReactiveFormsModule
  ],
  bootstrap: [AppComponent],
  providers: [ToasterService, OrderService, FreeVehicleService, FreeVehicleRestService,ZoneRestService,EncDecService,
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } },
    {provide: ToastOptions, useClass: CustomOption},
  ],
  entryComponents: [AdditionalSplitupComponent,DeleteDialogComponent, RestricedAccesComponent, CancelReasonComponent, PoiDialogComponent, DriverblockreasonComponent, DeviceblockreasonComponent,ZoneSavingComponent,RenameSubzoneComponent,DispatchNotificationsComponent],
  exports: []
})
export class AppModule { }
