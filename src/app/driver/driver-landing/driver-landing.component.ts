import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';


@Component({
  selector: 'app-driver-landing',
  templateUrl: './driver-landing.component.html',
  styleUrls: ['./driver-landing.component.css']
})
export class DriverLandingComponent implements OnInit {

  private sub: Subscription;
  public _id;
  public token;
  constructor( private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
      this.token = params['token'];
      localStorage.setItem('driverId', this._id);
      localStorage.setItem('token', this.token);
      this.router.navigate(['/driver/trip']); 
    });
  }
}
