import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { SidenavComponent } from './sidenav/sidenav.component';
import { TripHistoryComponent } from './trip-history/trip-history.component';
import { DriverComponent } from './driver.component';
import { DriverProfileComponent } from './driver-profile/driver-profile.component';
import { DriverLandingComponent } from './driver-landing/driver-landing.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule
  ],
  declarations: [SidenavComponent, TripHistoryComponent, DriverComponent, DriverProfileComponent, DriverLandingComponent]
})
export class DriverModule { }
