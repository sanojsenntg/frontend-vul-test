import { Component, OnInit } from '@angular/core';
import { DriverService } from '../../common/services/driver/driver.service';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-trip-history',
  templateUrl: './trip-history.component.html',
  styleUrls: ['./trip-history.component.css']
})
export class TripHistoryComponent implements OnInit {

  private sub: Subscription;
  public _id;
  public token;

  constructor(public _driverService:DriverService, private route: ActivatedRoute) { }

  public orderHistory;
  public faqData;
  public faqLength;
  itemsPerPage = 10;
  pageNo = 0; 
  key: string = '';
  sortOrder = 'desc';
  public showloader; 
  public driverId = '';

  ngOnInit() {
    this.driverId = localStorage.getItem('driverId');
    console.log(this.driverId);
    this.driverDetails(this.driverId);
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
      this.token = params['token'];
      localStorage.setItem('driverId', this._id);
      localStorage.setItem('token', this.token); 
    });
  }
  public driverDetails(driverId){
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      'driver_id': this.driverId,
      'order_status':'all'
    };
    console.log(params)
    this._driverService.getappDriverdets(params).then((data) => {
      console.log(data);
      this.faqLength = data.count;
      this.orderHistory=data.orders;
    });
  }
  pagingAgent(data) {
    this.faqData = [];
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    var params;
        params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        'driver_id':'5b9df0e47e279b35f4cea825',
        'order_status':'all'
      };
      console.log(params)
      this._driverService.getappDriverdets(params).then((data) => {
      this.showloader = false;
      this.orderHistory=data.orders;
    });
  }
}
