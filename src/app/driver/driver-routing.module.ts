import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { TripHistoryComponent } from './trip-history/trip-history.component';
import { DriverLandingComponent } from './driver-landing/driver-landing.component';
import { DriverProfileComponent } from './driver-profile/driver-profile.component';


export const DriverRoutes: Routes = [
     { path: ':id/:token', component: TripHistoryComponent},
     { path: 'trip', component: TripHistoryComponent },
     { path: 'profile', component: DriverProfileComponent}
];
