import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { LoginComponent } from '../dispatcher/login/login.component';
import { MapComponent } from '../dispatcher/map/map.component';
import { DispatcherService } from '../common/services/dispatcher/dispatcher.service';
import {DispatcherComponent} from '../dispatcher/dispatcher.component';
import { HomeComponent } from '../dispatcher/home/home.component';
import { VehicleComponent } from '../dispatcher/vehicle/vehicle.component';

const DispatcherLoginRoute: Routes = [
  {
    path: 'dispatcher/login',
    component: LoginComponent
  }
];

const dispatcherRouting: Routes = [
  {
    path: 'dispatcher',
    canActivate: [DispatcherService],
    component: DispatcherComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'map', component: MapComponent },
      { path: 'vehicles', component: VehicleComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild( DispatcherLoginRoute ),
    RouterModule.forRoot(dispatcherRouting, { useHash: false, preloadingStrategy: PreloadAllModules })
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class DispatcherRoutingModule { }
