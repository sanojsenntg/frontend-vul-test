import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AdminComponent } from '../admin/admin.component';
import { DashboardComponent } from '../admin/dashboard/dashboard.component';
import { LoginComponent } from '../admin/login/login.component';
import { AdminService } from '../common/services/admin/admin.service';
import { ProfileComponent } from '../admin/profile/profile.component';
import { ChangeProfileComponent } from '../admin/change-profile/change-profile.component';
import { AppComponent } from '../app.component';
import { OrderComponent } from '../admin/order/order.component';
import { orderRoutes } from '../admin/order/order-routing.module';
import { administrationRoutes } from '../admin/administration/administration-routing.module';
import { HomeComponent } from '../home/home/home.component';
import { fleetManagementRoutes } from '../admin/fleetmanagement/fleetmanagement-routing.module';
import { shiftManagementRoutes } from '../admin/shiftmanagement/shiftmanagement-routing';
import { customerRoutes } from '../admin/customer/customer-routing.module';
import { RoleRoute } from '../admin/role-management/role-routing.module';
import { AclAuthervice } from './../common/services/access-control/acl-auth.service'
import { DriverComponent } from '../driver/driver.component';
import { DriverRoutes } from '../driver/driver-routing.module';
import { AclFormComponent } from '../admin/acl-form/acl-form.component';
import { marketingRoutes } from '../admin/marketing/marketing-routing.module';
import { WalletRoute } from '../admin/wallet/wallet-routing.module';
import { FinanceRoute} from '../admin/finance/finance-routing.module';
import { PerformanceMonitoringRoutes } from '../admin/performance-monitoring/performance-monitoring-routing.module'
export const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AdminService],
    component: AdminComponent,
    children: [
      { path: '', component: DashboardComponent },
      { path: 'dashboard', component: DashboardComponent, canActivate: [AclAuthervice], data: { roles: ['dashboard'] } },
      { path: 'login', component: DashboardComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'change-profile', component: ChangeProfileComponent },
      { path: 'orders', children: orderRoutes, canActivate: [AclAuthervice], data: { roles: ['Order Management'] } },
      { path: 'administration', children: administrationRoutes, canActivate: [AclAuthervice], data: { roles: ["Administration"] } },
      { path: 'fleet', children: fleetManagementRoutes, canActivate: [AclAuthervice], data: { roles: ["Fleet Management"] } },
      { path: 'shift', children: shiftManagementRoutes, canActivate: [AclAuthervice], data: { roles: ["Shift Management"] } },
      { path: 'customer', children: customerRoutes, canActivate: [AclAuthervice], data: { roles: ["Customers - List"] } },
      { path: 'role-management', children: RoleRoute, canActivate: [AclAuthervice], data: { roles: ["Role Management"] } },
      { path: 'marketing', children: marketingRoutes, canActivate: [AclAuthervice], data: { roles: ["Marketing"] } },
      { path: 'wallet', children: WalletRoute, canActivate: [AclAuthervice], data: { roles: ["Wallet"] } },
      { path: 'performance-monitoring', children: PerformanceMonitoringRoutes, canActivate: [AclAuthervice], data: { roles: ["performance-monitoring"] } },
      { path: 'aclform', component: AclFormComponent },
      { path: 'finance', children: FinanceRoute, canActivate: [AclAuthervice], data: { roles: ["Finance"] } },
    ]
  }
];

const AdminLoginRoute: Routes = [
  {
    path: 'admin/login',
    component: LoginComponent
  }
];

const Homeroutes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'driver',
    component: DriverComponent,
    children: DriverRoutes
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(Homeroutes),
    RouterModule.forChild(AdminLoginRoute),
    RouterModule.forRoot(routes, { useHash: false, preloadingStrategy: PreloadAllModules }),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
