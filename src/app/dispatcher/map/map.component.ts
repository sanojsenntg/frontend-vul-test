import {
  ElementRef,
  NgZone,
  OnInit,
  Component,
  ChangeDetectorRef,
  ViewEncapsulation
} from "@angular/core";
import { Router } from "@angular/router";
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder
} from "@angular/forms";
import { UsersService } from '../../common/services/user/user.service';
import { DevicelocationService } from "../../common/services/devicelocation/devicelocation.service";
import { DriverService } from "../../common/services/driver/driver.service";
import { VehicleService } from "../../common/services/vehicles/vehicle.service";
import { ShiftsService } from "../../common/services/shifts/shifts.service";
import { EncDecService } from '../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { NgForm } from "@angular/forms";
import { environment } from "../../../environments/environment";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/of";
import { Socket } from "ng-socket-io";
import { MatDialog } from "@angular/material";
import { UserIdleService } from "angular-user-idle";
import { JwtService } from "../../common/services/api/jwt.service";
import { filter } from "rxjs/operator/filter";
import { VehicleModelsService } from "../../common/services/vehiclemodels/vehiclemodels.service";
import { OrderService } from "../../common/services/order/order.service";
import * as moment from "moment/moment";
import { ToastsManager } from "ng2-toastr";
import { CompanyService } from "../../common/services/companies/companies.service";
import { stringify } from "@angular/compiler/src/util";
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { AccessControlService } from "../../common/services/access-control/access-control.service";

declare var $;

@Component({
  selector: "app-map-page",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class MapComponent implements OnInit {
  public refreshTime2;
  public session_token;
  public useremail;
  public company_id_array = [];
  public usercompany;
  public socket_session_token;
  public hidden = true;
  public subscripttimeout;
  public subscripttimestart;
  public DriverList;
  public driverIcons: any = {};
  public teslaIcons: any = {};
  public mapFilterLabels: any = {};
  public positions = [];
  public positions1 = [];
  public mapFilterr;
  public vehicleFilter;
  public sideFilter;
  public vehicleTypeFilter;
  public vehiclesByType;
  public plateFilter;
  public driverFilter;
  public bookedLength;
  public offlineLength;
  public loggedLength;
  public inMaintenacneLength;
  public freeLength;
  public acceptedLength;
  public almostFreeLength;
  public internetOffLength;
  public gpsOffLength;
  private autocomplete: any;
  private address: any = {};
  private map;
  private driverMarkers = [];
  public center: any;
  public locationmsg;
  public selectVehicleData;
  public selectVehicleData2;
  public selectDriverData = [];
  public SingledeviceLocationData;
  public vehicle_id;
  public placeFilter;
  public singleVehicleTracking;
  public livetrackinginterval;
  public marker = {
    display: true,
    lat: "",
    lng: "",
    vehicle_image: "",
    device_id: "",
    device_type: "",
    imei: "",
    company: "",
    driver_image: "",
    driver_name: "",
    driver_id: "",
    emp_id: "",
    driverphone: "",
    device_model: "",
    vehicle_make: "",
    vehicle_model: "",
    vehicle_name: "",
    vehicle_fuel_type: "",
    vehicle_id: "",
    device_location_id: "",
    speed: 0,
    gps_strength: "",
    battery: "",
    shift_start: "",
    updated_at: "",
    pickup_location: "",
    shift_end: "",
    status: ""
  };
  public infoWindow: any;
  public trackloc: any;
  public check_interval = "yes";
  public check_filter_interval = "no";
  public newparam = "";
  public directionsRenderer: google.maps.DirectionsRenderer;
  public directionsResult: google.maps.DirectionsResult;
  public modelList;
  public selectedMoment: any = "";
  public selectedMoment1: any = "";
  public max = new Date();
  public max2 = new Date();
  side_no: FormControl = new FormControl();
  vehicleMapFilter: FormControl = new FormControl();
  public side: any;
  public pixel = 15;
  public changeIconFlag = false;
  public zoomChangedFlag = false;
  public delayMarker = 1;
  public driverGroupData;
  public tempdriverGroupData;
  public driverGroupMap: any = "";
  public imageext;
  public apiUrl;
  public uaeTime = new Date().toDateString();
  public logged_out;
  public normal_pause;
  public uber_careem;
  public vehicleStatus = {
    "/assets/img/map/VIP_car_blue_15h.png": "Booked",
    "/assets/img/map/VIP_car_red_15h.png": "Paused",
    "/assets/img/map/VIP_car_green_15h.png": "Free",
    "/assets/img/map/VIP_car_black_15h.png": "Logged",
    "/assets/img/map/booked_no-internet15.png": "Booked and Offline",
    "/assets/img/map/accepted15.png": "Accepted",//
    "/assets/img/map/almost_free15.png": "Almost Free",//
    "/assets/img/map/free_no-internet15.png": "Free and Offline",
    "/assets/img/map/paused_no-internet15.png": "Paused and Offline",
    "/assets/img/map/paused(uber_careem)15.png": "Paused (Uber/Careem)",
    "/assets/img/map/paused(break)15.png": "Paused (Break)",
    "/assets/img/map/signed_out15.png": "Logged Out",
    "/assets/img/map/tesla_blue15.png": "Booked",
    "/assets/img/map/tesla_red15.png": "Paused",
    "/assets/img/map/tesla_green15.png": "Free",
    "/assets/img/map/tesla_booked_nointernet15.png": "Booked and Offline",
    "/assets/img/map/tesla_accepted15.png": "Accepted",
    "/assets/img/map/tesla_almostfree15.png": "Almost Free",
    "/assets/img/map/tesla_free_nointernet15.png": "Free and Offline",
    "/assets/img/map/tesla_paused_no-internet15.png": "Paused and Offline",
    "/assets/img/map/tesla_pause(uber_careem)15.png": "Paused (Uber/Careem)",
    "/assets/img/map/tesla_pause(break)15.png": "Paused (Break)",
    "/assets/img/map/VIP_car_blue_20h.png": "Booked",
    "/assets/img/map/VIP_car_red_20h.png": "Paused",
    "/assets/img/map/VIP_car_green_20h.png": "Free",
    "/assets/img/map/VIP_car_black_20h.png": "Logged",
    "/assets/img/map/booked_no-internet20.png": "Booked and Offline",
    "/assets/img/map/accepted20.png": "Accepted",//
    "/assets/img/map/almost_free20.png": "Almost Free",//
    "/assets/img/map/free_no-internet20.png": "Free and Offline",
    "/assets/img/map/paused_no-internet20.png": "Paused and Offline",
    "/assets/img/map/paused(uber_careem)20.png": "Paused (Uber/Careem)",
    "/assets/img/map/paused(break)20.png": "Paused (Break)",
    "/assets/img/map/signed_out20.png": "Logged Out",
    "/assets/img/map/tesla_blue20.png": "Booked",
    "/assets/img/map/tesla_red20.png": "Paused",
    "/assets/img/map/tesla_green20.png": "Free",
    "/assets/img/map/tesla_booked_nointernet20.png": "Booked and Offline",
    "/assets/img/map/tesla_accepted20.png": "Accepted",
    "/assets/img/map/tesla_almostfree20.png": "Almost Free",
    "/assets/img/map/tesla_free_nointernet20.png": "Free and Offline",
    "/assets/img/map/tesla_paused_no-internet20.png": "Paused and Offline",
    "/assets/img/map/tesla_pause(uber_careem)20.png": "Paused (Uber/Careem)",
    "/assets/img/map/tesla_pause(break)20.png": "Paused (Break)"
  }
  public direction: any = {
    origin: "penn station, new york, ny",
    destination: "260 Broadway New York NY 10007",
    travelMode: "WALKING"
  };

  public deviceLocationData = [];
  driverMapFilter: FormControl = new FormControl();
  company_id_list: any = [];
  companyData: any = [];
  dtc: boolean = false;
  csvList: any = [];
  createCsvFlag: boolean;
  downloads = true;
  mapSocketCountList: any = [];
  companyIdFilter: any;
  csvAcl: boolean;
  vehicleCategories: any;
  vehicle_category_ids = [];

  constructor(
    private ref: ChangeDetectorRef,
    private _devicelocationService: DevicelocationService,
    private _vehicleService: VehicleService,
    private _driverService: DriverService,
    private _shiftsService: ShiftsService,
    public dialog: MatDialog,
    private userIdle: UserIdleService,
    private _jwtService: JwtService,
    private _usersService: UsersService,
    private _EncDecService: EncDecService,
    private socket: Socket,
    private router: Router,
    private _vehicleModelsService: VehicleModelsService,
    private _orderService: OrderService,
    public toastr: ToastsManager,
    public _companyservice: CompanyService,
    public _aclService: AccessControlService
  ) {
    this.imageext = window.localStorage['ImageExt'];
    this.apiUrl = environment.apiUrl;
    this.mapFilterr = "";
    this.vehicleFilter = "";
    this.sideFilter = "";
    this.driverFilter = "";
    this.plateFilter = "";
    this.map = {};
    this.vehiclesByType = {
      tesla: {
        count: 0
      },
      limo: {
        count: 0
      }
    };
    this.vehiclesByType.tesla.count = 0;
    this.vehiclesByType.limo.count = 0;
    this.driverIcons = {
      booked: "/assets/img/map/VIP_car_blue_15h.png",
      offline: "/assets/img/map/VIP_car_red_15h.png",
      free: "/assets/img/map/VIP_car_green_15h.png",
      loggedIn: "/assets/img/map/VIP_car_black_15h.png",
      internet: "/assets/img/map/booked_no-internet15.png",//
      gps: "/assets/img/map/booked_no-internet15.png",//
      accepted: "/assets/img/map/accepted15.png",//
      almost_free: "/assets/img/map/almost_free15.png",//
      free_internet: "/assets/img/map/free_no-internet15.png",//
      free_gps: "/assets/img/map/free_no-internet15.png",
      paused_offline: "/assets/img/map/paused_no-internet15.png",
      uber_careem: "/assets/img/map/paused(uber_careem)15.png",
      break: "/assets/img/map/paused(break)15.png",
      loggedOut: "/assets/img/map/signed_out15.png"
    };
    this.teslaIcons = {
      booked: "/assets/img/map/tesla_blue15.png",
      offline: "/assets/img/map/tesla_red15.png",
      free: "/assets/img/map/tesla_green15.png",
      loggedIn: "/assets/img/map/VIP_car_black_15h.png",
      internet: "/assets/img/map/tesla_booked_nointernet15.png",//
      gps: "/assets/img/map/tesla_booked_nointernet15.png",//
      accepted: "/assets/img/map/tesla_accepted15.png",//
      almost_free: "/assets/img/map/tesla_almostfree15.png",//
      free_internet: "/assets/img/map/tesla_free_nointernet15.png",//
      free_gps: "/assets/img/map/tesla_free_nointernet15.png",
      paused_offline: "/assets/img/map/tesla_paused_no-internet15.png",
      uber_careem: "/assets/img/map/tesla_pause(uber_careem)15.png",
      break: "/assets/img/map/tesla_pause(break)15.png",
      loggedOut: "/assets/img/map/signed_out15.png"
    };
    this.mapFilterLabels = {
      0: "Driver Status - All",
      1: "Paused",
      3: "Logged",
      5: "Free",
      6: "Booked",
      8: "Logged Out",
      7: "Vehicle status"
    };
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.subscripttimestart = this.userIdle
      .onTimerStart()
      .subscribe(count => console.log(count));

    // Start watch when time is up.
    this.subscripttimeout = this.userIdle.onTimeout().subscribe(() => {
      this._jwtService.destroyDispatcherToken();
      this.userIdle.stopWatching();
      this.router.navigate(["/dispatcher/login"]);
    });
    this.selectedMoment1 = new Date();
    this.selectedMoment = new Date(Date.now() - 3600000);
  }

  ngOnInit() {
    let that = this;
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany)
    this.company_id_list.push(this.usercompany)
    if (this.usercompany === '5ce12918aca1bb08d73ca25d' || this.usercompany === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    console.log('session_token' + this.session_token);
    setInterval(function () {
      that.uaeTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Dubai" });
    }, 1000);
    this.getCompanies();
    this.aclDisplayService()
    document.addEventListener("click", this.restart.bind(this));
    document.addEventListener("mousemove", this.restart.bind(this));
    document.addEventListener("mousedown", this.restart.bind(this));
    document.addEventListener("keypress", this.restart.bind(this));
    document.addEventListener("DOMMouseScroll", this.restart.bind(this));
    document.addEventListener("mousewheel", this.restart.bind(this));
    document.addEventListener("touchmove", this.restart.bind(this));
    document.addEventListener("MSPointerMove", this.restart.bind(this));
    this.getDriverGroupsForListing();
    this.socket.on("devicelocations_updates", arg => {
      if (!arg || arg == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      }
      arg = this._EncDecService.sockDwt(this.socket_session_token, arg);
      if (Object.keys(arg).length !== 0) {
        if (this.dtc && this.company_id_list.length > 0) {
          let companyIndex = this.company_id_list.indexOf(arg.company_id);
          if (companyIndex > -1) {
            this.mapSocketCountList[arg.company_id] = arg.vehicleStatus;
          }
          else {
            if (this.mapSocketCountList[arg.company_id] !== undefined)
              delete this.mapSocketCountList[arg.company_id];
          }
        }
        else if (arg.company_id === window.localStorage['user_company']) {
          this.loggedLength = arg.vehicleStatus.logged;
          this.bookedLength = arg.vehicleStatus.booked;
          this.offlineLength = arg.vehicleStatus.offline;
          this.freeLength = arg.vehicleStatus.free;
          this.acceptedLength = arg.vehicleStatus.accepted;
          this.almostFreeLength = arg.vehicleStatus.almost_free;
          this.logged_out = arg.vehicleStatus.logged_out_vehicle_count;
          this.normal_pause = arg.vehicleStatus.normal_pause
          this.uber_careem = arg.vehicleStatus.uber_careem
        }
      } else {
        let param_socket = {
          company_id: this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._usersService.getSocketsession(data1).then((res1) => {
          //console.log(JSON.stringify(res1));
          if (res1 && res1.status == 200) {
            var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
            if (res1.status == 200) {
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              this.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          }
        })
      }
    });
    this.center = "The Dubai Mall";
    this.locationmsg = "";
    let params = {
      company_id: this.company_id_array,
      offset: 0,
      limit: 10
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._vehicleModelsService
      .getVehicleModels(data1)
      .then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.modelList = data.vehicleModelsList;
          this.modelList.unshift({ name: "All" });
        }
      });
    //}
    //}, 4000);
    this.driverMapFilter.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(query => {
        let params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
          limit: 10,
          search_keyword: query,
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._driverService.searchForDriver(data1).subscribe(result => {
          if (result && result.status === 200) {
            result = this._EncDecService.dwt(this.session_token, result.data);
            this.selectDriverData = result.driver;
          }
        });
      })
    this.side_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(query => {
        let params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
          offset: 0,
          limit: 10,
          sortOrder: "desc",
          sortByColumn: "_id",
          searchsidenumber: query
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._vehicleService.getVehicleListingAdmin(data1).subscribe(result => {
          if (result && result.status === 200) {
            result = this._EncDecService.dwt(this.session_token, result.data);
            this.selectVehicleData2 = result.result;
          }
        })
      });
    this.vehicleMapFilter.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(query => {
        let params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
          offset: 0,
          limit: 10,
          sortOrder: "desc",
          sortByColumn: "vehicle_unique_id",
          search_keyword: query
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._vehicleService.searchVehicle(data1).then(result => {
          if (result && result.status === 200) {
            result = this._EncDecService.dwt(this.session_token, result.data);
            this.selectVehicleData = result.searchVehicle;
          }
        });
      })
    if (this.dtc && this.company_id_list.length > 0)//Gets vehicle count of all companies
      setInterval(() => {
        if (this.vehicle_category_ids.length > 0 || this.vehicleTypeFilter || this.driverGroupMap != "" || this.internetFlag || this.gpsFlag || this.breakFlag || this.uberFlag) {

        } else {
          let loggedLength = 0;
          let booked = 0;
          let offline = 0;
          let acceptedLength = 0;
          let free = 0;
          let almostFreeLength = 0;
          let logged_out = 0;
          let normal_pause = 0;
          let uber_careem = 0;
          this.loggedLength = 0;
          this.bookedLength = 0;
          this.offlineLength = 0;
          this.freeLength = 0;
          this.acceptedLength = 0;
          this.almostFreeLength = 0;
          this.logged_out = 0;
          this.normal_pause = 0;
          this.uber_careem = 0;
          Object.keys(this.mapSocketCountList).forEach(element => {
            loggedLength += this.mapSocketCountList[element].logged;
            booked += this.mapSocketCountList[element].booked;
            offline += this.mapSocketCountList[element].offline;
            free += this.mapSocketCountList[element].free;
            acceptedLength += this.mapSocketCountList[element].accepted;
            almostFreeLength += this.mapSocketCountList[element].almost_free;
            logged_out += this.mapSocketCountList[element].logged_out_vehicle_count;
            normal_pause += this.mapSocketCountList[element].normal_pause;
            uber_careem += this.mapSocketCountList[element].uber_careem;
          });
          this.loggedLength = loggedLength;
          this.bookedLength = booked;
          this.offlineLength = offline;
          this.freeLength = free;
          this.acceptedLength = acceptedLength;
          this.almostFreeLength = almostFreeLength;
          this.logged_out = logged_out;
          this.normal_pause = normal_pause
          this.uber_careem = uber_careem
        }
      }, 4000);
    this.getVehicleCategories()
  }
  since(dt: any) {
    let date1 = new Date(dt).getTime();
    let date2 = new Date(this.uaeTime).getTime();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";
    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output += "(" + Math.round(var1) + " secs ago)";
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      output += "(" + Math.round(var1) + " mins ago)";
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      output += "(" + Math.round(var1) + " hrs ago)";
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output += "(" + Math.round(var1) + " days ago)";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output += "(" + Math.round(var1) + " months ago)";
    }
    return hoursDiff < 0 ? '0 secs ago' : output;
  }
  reset() {
    this.hidden = true;
    this.check_interval = "yes";
    this.check_filter_interval = "no";
    //console.log('Reset');
    this.driverFilter = "";
    this.minutesS = '0';
    this.minutesM = '3';
    this.minutesLimitS = '0';
    this.minutesLimitM = '10000';
    this.vehicleFilter = "";
    this.sideFilter = "";
    this.vehicleTypeFilter = "";
    this.vehicle_id = "";
    this.placeFilter = "";
    this.mapFilterr = "6";
    this.plateFilter = "";
    this.showOrderTrack = false;
    this.side = '';
    this.selectedMoment1 = new Date();
    this.selectedMoment = new Date(Date.now() - 3600000);
    this.max2 = this.selectedMoment1;
    this.showCover = false;
    this.orderButton = false;
    this.startmarker = "";
    this.destinationmarker = "";
    this.positions3 = [];
    this.tripStatus = [];
    this.uberFlag = false;
    this.breakFlag = false;
    this.loggedLength = 0;
    this.bookedLength = 0;
    this.offlineLength = 0;
    this.freeLength = 0;
    this.acceptedLength = 0;
    this.almostFreeLength = 0;
    this.logged_out = 0;
    this.normal_pause = 0;
    this.uber_careem = 0;
    this.showCover = false;
    this.internetFlag = false;
    this.gpsFlag = false;
    this.side = '';
    if (this.dtc) {
      this.company_id_list = this.companyData.slice().map(x => x._id);
      this.companyIdFilter = this.company_id_list.slice()
      this.companyIdFilter.push('all')
    }
    for (var i = 0; i < this.travelled_markers.length; i++)
      this.travelled_markers[i].setMap(null);
    clearInterval(this.interval);
    this.internetFlag = false;
    this.vehicle_category_ids = [];
    this.setMapFilter(this.mapFilterr);
  }
  selectAllCompany() {
    if (this.company_id_list.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.company_id_list = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.mapSocketCountList = [];
      this.company_id_list = [];
      this.companyIdFilter = [];
    }
  }
  restart() {
    this.userIdle.resetTimer();
  }
  onMapReady(map) {
    this.map = map;
    this.map.addListener('click', () => {
      if (this.infoWindow)
        this.infoWindow.close();
    })
    var search_input: any = document.getElementById('search-input');
    var autocomplete3 = new google.maps.places.Autocomplete(search_input);
    autocomplete3.setComponentRestrictions(
      { 'country': ['ae'] });
    google.maps.event.addListener(autocomplete3, 'place_changed', function () {
      that.placeChanged(autocomplete3.getPlace());
    });
    this.map.setOptions({
      styles: [
        {
          "featureType": "all",
          "elementType": "all",
          "stylers": [
            {
              "hue": "#e7ecf0"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#8ed863"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
            {
              "saturation": -70
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
            {
              "lightness": "48"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "simplified"
            },
            {
              "saturation": -60
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#8abdec"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#9cbbf0"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        }
      ]
    });
    let that = this;
    google.maps.event.addListener(map, "zoom_changed", function () {
      var zoom = map.getZoom();
      if (zoom > 12) {
        that.zoomChangedFlag = true;
        that.pixel = 20;
        that.driverIcons = {
          booked: "/assets/img/map/VIP_car_blue_20h.png",
          offline: "/assets/img/map/VIP_car_red_20h.png",
          free: "/assets/img/map/VIP_car_green_20h.png",
          loggedIn: "/assets/img/map/VIP_car_black_20h.png",
          internet: "/assets/img/map/booked_no-internet20.png",//
          gps: "/assets/img/map/booked_no-internet20.png",//
          accepted: "/assets/img/map/accepted20.png",//
          almost_free: "/assets/img/map/almost_free20.png",//
          free_internet: "/assets/img/map/free_no-internet20.png",//
          free_gps: "/assets/img/map/free_no-internet20.png",
          paused_offline: "/assets/img/map/paused_no-internet20.png",
          uber_careem: "/assets/img/map/paused(uber_careem)20.png",
          break: "/assets/img/map/paused(break)20.png",
          loggedOut: "/assets/img/map/signed_out20.png"
        };
        that.teslaIcons = {
          booked: "/assets/img/map/tesla_blue20.png",
          offline: "/assets/img/map/tesla_red20.png",
          free: "/assets/img/map/tesla_green20.png",
          loggedIn: "/assets/img/map/VIP_car_black_20h.png",
          internet: "/assets/img/map/tesla_booked_nointernet20.png",//
          gps: "/assets/img/map/tesla_booked_nointernet20.png",//
          accepted: "/assets/img/map/tesla_accepted20.png",//
          almost_free: "/assets/img/map/tesla_almostfree20.png",//
          free_internet: "/assets/img/map/tesla_free_nointernet20.png",//
          free_gps: "/assets/img/map/tesla_free_nointernet20.png",
          paused_offline: "/assets/img/map/tesla_paused_no-internet20.png",
          uber_careem: "/assets/img/map/tesla_pause(uber_careem)20.png",
          break: "/assets/img/map/tesla_pause(break)20.png",
          loggedOut: "/assets/img/map/signed_out20.png"
        };
      } else if (zoom == 12) {
        that.zoomChangedFlag = true;
        that.pixel = 15;
        that.driverIcons = {
          booked: "/assets/img/map/VIP_car_blue_15h.png",
          offline: "/assets/img/map/VIP_car_red_15h.png",
          free: "/assets/img/map/VIP_car_green_15h.png",
          loggedIn: "/assets/img/map/VIP_car_black_15h.png",
          internet: "/assets/img/map/booked_no-internet15.png",//
          gps: "/assets/img/map/booked_no-internet15.png",//
          accepted: "/assets/img/map/accepted15.png",//
          almost_free: "/assets/img/map/almost_free15.png",//
          free_internet: "/assets/img/map/free_no-internet15.png",//
          free_gps: "/assets/img/map/free_no-internet15.png",
          paused_offline: "/assets/img/map/paused_no-internet15.png",
          uber_careem: "/assets/img/map/paused(uber_careem)15.png",
          break: "/assets/img/map/paused(break)15.png",
          loggedOut: "/assets/img/map/signed_out15.png"
        };
        that.teslaIcons = {
          booked: "/assets/img/map/tesla_blue15.png",
          offline: "/assets/img/map/tesla_red15.png",
          free: "/assets/img/map/tesla_green15.png",
          loggedIn: "/assets/img/map/VIP_car_black_15h.png",
          internet: "/assets/img/map/tesla_booked_nointernet15.png",//
          gps: "/assets/img/map/tesla_booked_nointernet15.png",//
          accepted: "/assets/img/map/tesla_accepted15.png",//
          almost_free: "/assets/img/map/tesla_almostfree15.png",//
          free_internet: "/assets/img/map/tesla_free_nointernet15.png",//
          free_gps: "/assets/img/map/tesla_free_nointernet15.png",
          paused_offline: "/assets/img/map/tesla_paused_no-internet15.png",
          uber_careem: "/assets/img/map/tesla_pause(uber_careem)15.png",
          break: "/assets/img/map/tesla_pause(break)15.png",
          loggedOut: "/assets/img/map/signed_out15.png"
        };
      }
    });
  }

  displayDriverOnMap(lat, lng, id, image, i) {
    let position = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
    let markerIds = this.driverMarkers.map(function (marker) {
      return marker.id;
    });
    let markerExists = markerIds.indexOf(id) !== -1;
    if (markerExists) {
      let marker = this.driverMarkers.filter(function (marker) {
        return marker.id === id;
      });
      if (marker.length > 0) {
        this.slideMarker(
          marker[0].marker,
          [position.lat(), position.lng()],
          image,
          i
        );
      }
    } else {
      var rotation = Math.floor(Math.random() * 350) + 1;
      var img = new Image();
      img.src = image || "../../../../assets/img/map/car-black-top.png";
      img.onload = () => {
        let icon = new RotateIcon({ img: img, pixel: this.pixel })
          .setRotation({ deg: rotation })
          .getUrl();
        let marker = new google.maps.Marker({
          position: position,
          map: this.map,
          icon: icon
        });
        let driver = this.deviceLocationData.filter(function (driver) {
          return driver["device_id"] === id;
        });
        if (this.vehicle_id) {
          if (!this.SingledeviceLocationData[0].driver_id || this.SingledeviceLocationData[0].driver_id == null)
            marker.addListener("mouseover", () => {
              this.clickedSignedOut(marker, driver[0], image);
            });
          else
            marker.addListener("mouseover", () => {
              this.clicked(marker, driver[0], image);
            });
        }
        else if (this.mapFilterr !== '8')
          marker.addListener("mouseover", () => {
            this.clicked(marker, driver[0], image);
          });
        else {
          //console.log(driver[0])
          marker.addListener("mouseover", () => {
            this.clickedSignedOut(marker, driver[0], image);
          });
        }
        if (this.dtc)
          marker.addListener("dblclick", () => {
            this.shiftpop(this.deviceLocationData[i].vehicle_id);
          });
        marker.addListener("click", () => {
          this.setDriverFilter({ 'vehicle_id': driver[0].vehicle_id });
        });
        this.driverMarkers.push({ id: id, marker: marker, image: img });
      };
    }
  }

  removeStaleDriversFromMap() {
    var driverIds = [];
    if (this.singleVehicleTracking) {
      driverIds = this.SingledeviceLocationData.map(function (device) {
        return device["device_id"];
      });
    } else {
      driverIds = this.deviceLocationData.map(function (device) {
        return device["device_id"];
      });
    }
    var staleDrivers = [];
    for (var i = 0; i < this.driverMarkers.length; i++) {
      if (driverIds.indexOf(this.driverMarkers[i].id) === -1) {
        staleDrivers.push(i);
      }
    }
    for (var i = 0; i < staleDrivers.length; i++) {
      this.driverMarkers[staleDrivers[i]].marker.setMap(null);
    }
    this.driverMarkers = this.driverMarkers.filter(function (marker, index) {
      return staleDrivers.indexOf(index) === -1;
    });
  }

  slideMarker(marker, target, image, count) {
    let steps = 10;
    let delay = 10;
    var i = 0;
    var position = [marker.getPosition().lat(), marker.getPosition().lng()];
    let deltaLat = (target[0] - position[0]) / steps;
    let deltaLng = (target[1] - position[1]) / steps;
    var rotation = Math.atan2(deltaLng, deltaLat) * (180 / Math.PI);
    var img = new Image();
    img.src = image || "../../../../assets/img/map/car-black-top.png";
    img.onload = () => {
      let chain = Promise.resolve();
      let icon;
      chain
        .then(() => {
          icon = new RotateIcon({ img: img, pixel: this.pixel })
            .setRotation({ deg: rotation })
            .getUrl();
        })
        .then(() => this.Wait(count * 3))
        .then(() => {
          marker.setIcon(icon);
          var moveMarker = function () {
            position[0] += deltaLat;
            position[1] += deltaLng;
            var newPosition = new google.maps.LatLng(position[0], position[1]);
            marker.setPosition(newPosition);
            if (i != steps) {
              i++;
              setTimeout(moveMarker, delay);
            }
          };
          moveMarker();
        });
    };
  }

  displayFnvehicle(data): string {
    return data ? data.vehicle_unique_id + " " + data.display_name : data;
  }
  displayFnModel(data): string {
    return data ? data.name : data;
  }
  displayFnDriver(data): string {
    return data ? data.username : data;
  }
  displayFnplate(data): string {
    return data ? data.plate_number : data;
  }
  displayFnside(data): string {
    return data ? data.vehicle_identity_number : data;
  }
  log(event) {
    if (event instanceof MouseEvent) {
      return false;
    }
  }
  public infoMarker = null;
  clicked(marker, detail, icon) {
    this.check_interval = "no";
    if (detail == undefined)
      return;
    let device_id = detail["device_id"];
    const params = {
      device_id: device_id,
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    };
    if (!this.infoWindow) {
      this.infoWindow = new google.maps.InfoWindow();
      let that = this;
      this.infoWindow.addListener('closeclick', function () {
        if (that.infoMarker)
          that.infoMarker.setMap(null);
        that.infoMarker = null;
      });
    }
    if (device_id) {
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._shiftsService.getAssignedDrivers(data1).then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          let popupData = data.device_location_data;
          //let correctDriver = popupData.driver_id.username
          let status;
          if (popupData) {
            if (popupData.vehicle_id)
              if (popupData.vehicle_id.online_status == "1") {
                status = "online";
              } else {
                status = "offline";
              }
            this.infoWindow.setContent("");
            this.marker.updated_at = data.device_location_data.updated_at;
            this.marker.device_id = popupData.unique_device_id;
            this.marker.device_model = popupData.device_model;
            this.marker.vehicle_id = popupData.vehicle_id
              ? popupData.vehicle_id.vehicle_identity_number
              : "";
            this.marker.device_type = status;
            this.marker.speed = parseInt(data.device_location_data.speed);
            this.marker.gps_strength = String(
              parseInt(data.device_location_data.gps_strength)
            );
            this.marker.battery = data.device_location_data.battery;
            this.marker.shift_start = popupData.shift_log_id ? popupData.shift_log_id.start_time : '';
            this.marker.imei = popupData.imei;
            this.marker.vehicle_name = popupData.vehicle_id
              ? String(
                popupData.vehicle_id.display_name
              ).toUpperCase() : '';
            this.marker.vehicle_make = popupData.vehicle_id ? popupData.vehicle_id.car_manufacturer_desc
              ? String(popupData.vehicle_id.car_manufacturer_desc).toUpperCase()
              : "" : "";
            this.marker.vehicle_model = popupData.vehicle_id
              ? String(
                popupData.vehicle_id.vehicle_model_id.name
              ).toUpperCase() : "";
            this.marker.vehicle_fuel_type = popupData.vehicle_id
              ? popupData.vehicle_id.car_fuel_type : '';
            this.marker.company = "Dubai Taxi";
            this.marker.driver_name = popupData.driver_id ?
              String(
                popupData.driver_id.display_name
              ).toUpperCase() : '';
            this.marker.driver_id = popupData.driver_id
              ? popupData.driver_id._id : '';
            this.marker.emp_id = popupData.driver_id
              ? popupData.driver_id.emp_id : '';
            this.marker.driverphone = popupData.driver_id
              ? popupData.driver_id.phone_number : '';
            this.marker.status = this.vehicleStatus[icon];
            if (popupData.order_id) {
              this.marker.pickup_location = popupData.order_id.pickup_location;
              // let latlngvalue = popupData.order_id.pickuplocation.coordinates;
              // let map = this.map;
              // let marker = new google.maps.Marker({
              //   position: new google.maps.LatLng(latlngvalue[0], latlngvalue[1]),
              //   map: map,
              //   icon: "../assets/img/marker-red.png",
              //   title: "Pickup Location"
              // });
              // this.infoMarker = marker;
            }
            else {
              this.marker.pickup_location = "";
            }
            this.marker.display = true;
            this.ref.detectChanges();
            let infoWindowContent = <Element>(
              document.getElementById("iw").cloneNode(true)
            );
            infoWindowContent.id = "iw-2";
            infoWindowContent.setAttribute("style", "display:block;");
            this.infoWindow.setContent(infoWindowContent);
            this.infoWindow.open(this.map, marker);
            let that = this;
            google.maps.event.addListener(this.infoWindow, 'domready', function () {
              var clickableItem = document.getElementById('clicka');
              if (that.dtc)
                clickableItem.addEventListener('click', () => {
                  that.shiftpop(popupData.vehicle_id)
                })
            });
            // if (this.infoMarker)
            //   this.infoMarker.setMap(null);
            // this.infoMarker = null;

          } else {
            this.marker.device_id = "";
            this.marker.device_model = "";
            this.marker.vehicle_id = "";
            this.marker.device_type = "";
            this.marker.imei = "";
            this.marker.vehicle_name = "";
            this.marker.vehicle_make = "";
            this.marker.vehicle_model = "";
            this.marker.vehicle_fuel_type = "";
            this.marker.company = "Dubai Taxi";
            this.marker.driver_image = "/assets/img/map/driver.jpg";
            this.marker.driver_name = "";
            this.marker.driver_id = "";
            this.marker.emp_id = "";
            this.marker.driverphone = "";
            this.marker.speed = 0;
            this.marker.gps_strength = "";
            this.marker.battery = "";
            this.marker.vehicle_image =
              "/assets/img/map/1428584997LEXUS_ES350.jpg";
            this.marker.pickup_location = "";
            this.marker.status = ""
          }
        }
      });
    }
  }

  public getRandomMarkers() {
    const positions = [];
    let id;
    for (let i = 0; i < this.deviceLocationData.length; i++) {
      const randomLat = this.deviceLocationData[i].latitude;
      const randomLng = this.deviceLocationData[i].longitude;
      if (this.deviceLocationData[i]["device_id"]) {
        id = this.deviceLocationData[i]["device_id"]["_id"];
      } else {
        id = "";
      }
      positions.push([randomLat, randomLng, id]);
    }
    return positions;
  }

  public initialized(autocomplete: any) {
    this.autocomplete = autocomplete;
  }

  public hideMarkerInfo() {
    this.marker.display = !this.marker.display;
    this.check_interval = "yes";
  }

  public hidepoup() {
    if (this.marker.display) {
      this.marker.display = !this.marker.display;
      var iwOuter = $(".gm-style-iw");
      var iwBackground = iwOuter.prev();
      iwBackground.hide();
      var iwCloseBtn = iwOuter.next();
      iwCloseBtn.hide();
    }
  }

  public placeChanged(place) {
    this.center = place.geometry.location;
    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      this.address[addressType] = place.address_components[i].long_name;
    }
    this.ref.detectChanges();
  }
  liveTracking(vehicleId) {
    this.singleVehicleTracking = vehicleId ? true : false;
    this.check_interval = "no";
    this.vehicle_id = vehicleId;
    let trackloc = this.positions;
    let that = this;
    if (this.livetrackinginterval) {
      console.log("clear interval");
      clearInterval(this.livetrackinginterval);
    }
    if (vehicleId !== null) {
      let that = this;
      this.livetrackinginterval = setInterval(function () {
        let params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
          vehicle_id: vehicleId
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        that._devicelocationService
          .getVehicleLastLocation(data1)
          .then(data => {
            if (data && data.status == 200) {
              data = this._EncDecService.dwt(this.session_token, data.data);
              if (trackloc) {
                that.positions1 = trackloc;
              }
              if (data.location) {
                that.positions = [];
                const randomLat = data.locations.latitude;
                const randomLng = data.locations.longitude;
                that.positions1.push([randomLat, randomLng]);
                that.positions.push([randomLat, randomLng]);
                console.log("this.positions->", this.positions);
                console.log("this.positions1->", this.positions1);
                trackloc = [];
              }
            }
          });
      }, 3000);
    }
  }
  private setFilterLoading(
    filterSelector: string,
    isLoading: Boolean,
    newText: string
  ) {
    if (!$(filterSelector)) {
      return;
    }
    if (isLoading) {
      $(filterSelector).text("Reloading vehicles...");
    } else {
      $(filterSelector).text(newText || "Loading complete.");
    }
  }

  public setVehicleTypeFilter(event) {
    /*this.vehicleTypeFilter = vehicleType.target.value;
    this.setMapFilter(this.mapFilterr);*/
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: "desc",
      sortByColumn: "updated_at",
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    };
    if (typeof event === "object") {
      this.setMapFilter(this.mapFilterr);
    } else if (typeof event === "string") {
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleModelsService.getVehicleModels(data1).then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.modelList = data.vehicleModelsList;
          this.modelList.unshift({ name: "All" });
        }
        //alert(this.selectDriverData.length)}
      });
    }
  }

  public setMapFilter(param) {
    if (!this.createCsvFlag)
      this.csvList = []
    this.setFilterLoading(
      "#dropdownMenuButton",
      false,
      this.mapFilterLabels[parseInt(param) || 0]
    );
    if (this.mapFilterr != param) this.changeIconFlag = true;
    else this.changeIconFlag = false;
    this.newparam = param;
    if (this.mapFilterr != this.newparam) {
      this.mapFilterr = this.newparam;
    }
    this.getRefreshTime(param);
    if (param === "7") {
      $("#dropdownMenuButton").text("Select Vehicle Status");
      for (let i = 0; i < this.driverMarkers.length; i++) {
        this.driverMarkers[i].marker.setMap(null);
        if (i == this.driverMarkers.length - 1) this.driverMarkers = [];
      }
      this.positions = [];
      this.positions1 = [];
      this.deviceLocationData = [];
      return;
    }
    if (param !== '1')
      this.uberFlag = this.breakFlag = false;
    this.vehicle_id = "";
    this.positions = [];
    this.positions1 = [];
    this.locationmsg = "";
    //this.liveTracking(null);
    if (this.mapFilterr != this.newparam) {
      this.mapFilterr = this.newparam;
    }
    this.check_filter_interval = "yes";
    this.check_interval = "no";
    if (!param) {
      this.positions = [];
    }
    this.setFilterLoading("#dropdownMenuButton", true, null);
    try {
      (<HTMLInputElement>(
        document.getElementById("vehicleModel")
      )).disabled = true;
      (<HTMLInputElement>(
        document.getElementById("vehicleCategory")
      )).disabled = true;
    } catch (error) {
      console.log("model");
    }
    let params = {
      status: this.newparam || "10",
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._devicelocationService
      .getMapData(data1)
      .then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.deviceLocationData = data.getdeviceloc;
          var driverIndicesToRemove = [];
          let haveLocationData =
            this.deviceLocationData.length > 0 &&
            typeof this.deviceLocationData[0] != "undefined" &&
            this.deviceLocationData[0].latitude != "";
          if (!haveLocationData) {
            this.positions = [];
            console.log('error')
            //throw new Error('No location data')
          }
          var teslaCount = 0;
          var limoCount = 0;
          let result;
          for (let i = 0; i < this.deviceLocationData.length; i++) {
            if (this.tripStatus.length > 0)
              if (this.tripStatus.indexOf(this.deviceLocationData[i].trip_status) == -1) {
                driverIndicesToRemove.push(i);
                continue;
              }
            if (this.internetFlag) {
              if (this.iconColor2(this.deviceLocationData[i].updated_at) == 'green') {
                driverIndicesToRemove.push(i);
                continue;
              }
            }
            // if (this.internetFlag || (this.mapFilterr == '5' && !this.hidden)) {
            //   if (this.iconColor2(this.deviceLocationData[i].updated_at) == 'green') {
            //     driverIndicesToRemove.push(i);
            //     continue;
            //   }
            // }
            // if(this.iconColor(this.deviceLocationData[i].updated_at) !== 'green' && this.hidden)//hides offline vehicles
            // {
            //   driverIndicesToRemove.push(i);
            //   continue;
            // }
            if (param == '1' && this.uberFlag)
              if (this.deviceLocationData[i].vehicle_ids["pause_reason"] !== 'uber/careem') {
                driverIndicesToRemove.push(i);
                continue;
              }
            if (param == '1' && this.breakFlag)
              if (this.deviceLocationData[i].vehicle_ids["pause_reason"] !== 'break') {
                driverIndicesToRemove.push(i);
                continue;
              }
            if (this.gpsFlag)
              if (this.deviceLocationData[i].gps_strength < 50) {
                driverIndicesToRemove.push(i);
                continue;
              }
            if (!this.paused) {
              if (this.getVehicleStatus(this.deviceLocationData[i].vehicle_ids) == 'offline') {
                driverIndicesToRemove.push(i);
                continue;
              }
            }
            result = this.processDeviceLocationData(
              this.deviceLocationData[i],
              i
            );

            if (result.removeVehicle) {
              driverIndicesToRemove.push(i);
            }
            if (i == this.deviceLocationData.length - 1)
              this.zoomChangedFlag = false;
          }
          this.vehiclesByType.tesla.count = teslaCount;
          this.vehiclesByType.limo.count = limoCount;
          this.deviceLocationData = $.grep(this.deviceLocationData, function (
            device,
            index
          ) {
            return !driverIndicesToRemove.includes(index);
          });
          this.removeStaleDriversFromMap();
          if (this.tripStatus.length > 0) {
            this.setFilterLoading(
              "#dropdownMenuButton",
              false,
              this.tripStatus[0] == 'accepted_by_driver' ? 'Accepted' +
                " - " +
                this.deviceLocationData.length : 'Almost free' +
                " - " +
                this.deviceLocationData.length
            );
          }
          else {
            let third_param = this.deviceLocationData.length;
            if (this.vehicle_category_ids.length == 0 && !this.vehicleTypeFilter && this.driverGroupMap == "") {
              if (parseInt(param) == 3) {
                third_param = this.loggedLength;
              }
              if (parseInt(param) == 5) {
                third_param = this.freeLength;
              }
              if (parseInt(param) == 8) {
                third_param = this.logged_out;
              }
            }
            this.setFilterLoading(
              "#dropdownMenuButton",
              false,
              this.internetFlag ? 'Without internet' +
                " - " +
                this.deviceLocationData.length : this.gpsFlag ? 'Without GPS' +
                  " - " +
                  this.deviceLocationData.length : this.uberFlag ? 'Uber/Careem - ' + this.deviceLocationData.length :
                    this.breakFlag ? 'Break - ' + this.deviceLocationData.length
                      : this.mapFilterLabels[parseInt(param) || 0] +
                      " - " +
                      third_param
            );
          }
          (<HTMLInputElement>(
            document.getElementById("vehicleModel")
          )).disabled = false;
          (<HTMLInputElement>(
            document.getElementById("vehicleCategory")
          )).disabled = false;
        }
      })
      .catch(error => {
        try {
          if (this.tripStatus.length > 0) {
            this.setFilterLoading(
              "#dropdownMenuButton",
              false,
              this.tripStatus[0] == 'accepted_by_driver' ? 'Accepted' +
                " - " +
                this.deviceLocationData.length : 'Almost free' +
                " - " +
                this.deviceLocationData.length
            );
          }
          else {
            let third_param = this.deviceLocationData.length;
            if (this.vehicle_category_ids.length == 0 && !this.vehicleTypeFilter && this.driverGroupMap == "") {
              if (parseInt(param) == 3) {
                third_param = this.loggedLength;
              }
              if (parseInt(param) == 5) {
                third_param = this.freeLength;
              }
              if (parseInt(param) == 8) {
                third_param = this.logged_out;
              }
            }
            this.setFilterLoading(
              "#dropdownMenuButton",
              false,
              this.internetFlag ? 'Without internet' +
                " - " +
                this.deviceLocationData.length : this.gpsFlag ? 'Without GPS' +
                  " - " +
                  this.deviceLocationData.length : this.uberFlag ? 'Uber/Careem - ' + this.deviceLocationData.length :
                    this.breakFlag ? 'Break - ' + this.deviceLocationData.length
                      : this.mapFilterLabels[parseInt(param) || 0] +
                      " - " +
                      third_param
            );
          }
          (<HTMLInputElement>(
            document.getElementById("vehicleModel")
          )).disabled = false;
          (<HTMLInputElement>(
            document.getElementById("vehicleCategory")
          )).disabled = false;
        } catch (error) {
          console.log();
        }
      });
  }

  public processDeviceLocationData(locationData, i) {
    var id;
    const randomLat = locationData.latitude;
    const randomLng = locationData.longitude;
    if (locationData["device_id"]) {
      id = locationData["device_id"];
    } else {
      id = "";
    }
    if (randomLat != "" && randomLng != "" && id != "") {
      // let vehicle = $.grep(this.selectVehicleData, function (value) {
      //   if (value["shift"].length <= 0) {
      //     return false;
      //   }
      //   return value["shift"][0]["device_id"] === locationData["device_id"];
      // });
      // if (!vehicle[0]) {
      //   console.log("no vehicle found for device " + locationData["device_id"]);
      //   return {};
      // }
      let vehicleModelIsValid =
        locationData.vehicle_model_id &&
        (typeof locationData.vehicle_model_id === "string" ||
          locationData.vehicle_model_id instanceof String);
      let isTesla =
        vehicleModelIsValid &&
        (locationData.vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
          locationData.vehicle_model_id === "5bb093892dd48f285e5c3dba");
      var removeVehicle = false;
      if (this.vehicleTypeFilter) {
        if (
          this.vehicleTypeFilter._id != locationData.vehicle_model_id &&
          this.vehicleTypeFilter.name != "All"
        ) {
          removeVehicle = true;
        }
      }
      if (this.driverGroupMap != "") {
        if (this.driverGroupMap.indexOf(locationData.t3) == -1) {
          removeVehicle = true;
        }
      }
      if (this.vehicle_category_ids.length > 0) {
        if (locationData.vehicle_model && locationData.vehicle_model.vehicle_cat_ids.length > 0) {
          if (this.vehicle_category_ids.indexOf(locationData.vehicle_model.vehicle_cat_ids[0]) == -1)
            removeVehicle = true;
        } else
          removeVehicle = true;
      }
      if (removeVehicle) {
        return { removeVehicle: true, vehicleType: isTesla ? "Tesla" : "Limo" };
      } else {
        let vehicleStatus = this.getVehicleStatus(locationData.vehicle_ids);
        var driverIcon = this.driverIcons.loggedIn;
        if (this.newparam == "8") {
          driverIcon = this.teslaIcons['loggedOut'];
        }
        else if (vehicleStatus == 'offline') {
          if (isTesla)
            driverIcon = this.iconColor(locationData.updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
          else
            driverIcon = this.iconColor(locationData.updated_at) != 'green'
              ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
        }
        else if (this.newparam !== "3") {
          if (this.tripStatus.length > 0 || (locationData.trip_status !== "" && this.mapFilterr == '6')) {
            if (isTesla) {
              switch (locationData.trip_status) {
                case 'accepted_by_driver':
                  driverIcon = this.teslaIcons['accepted'];
                  break;
                case 'confirming_rate':
                  driverIcon = this.teslaIcons['almost_free'];
                  break;
                case 'selecting_payment':
                  driverIcon = this.teslaIcons['almost_free'];
                  break;
                case 'rating':
                  driverIcon = this.teslaIcons['almost_free'];
                  break;
                default:
                  driverIcon = this.teslaIcons[vehicleStatus]
                  break;
              }
              driverIcon = this.iconColor(locationData.updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
            }
            else {
              switch (locationData.trip_status) {
                case 'accepted_by_driver':
                  driverIcon = this.driverIcons['accepted'];
                  break;
                case 'confirming_rate':
                  driverIcon = this.driverIcons['almost_free'];
                  break;
                case 'selecting_payment':
                  driverIcon = this.driverIcons['almost_free'];
                  break;
                case 'rating':
                  driverIcon = this.driverIcons['almost_free'];
                  break;
                default:
                  driverIcon = this.driverIcons[vehicleStatus]
                  break;
              }
              driverIcon = this.iconColor(locationData.updated_at) != 'green'
                ? this.driverIcons['internet'] : driverIcon;
            }
          }
          else {
            if (isTesla) {
              driverIcon = this.iconColor(locationData.updated_at) != 'green' && (this.mapFilterr == '5' || this.mapFilterr == '')
                ? this.teslaIcons['free_internet'] : (this.mapFilterr == '5' || this.mapFilterr == '')
                  ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
            }
            else {
              driverIcon = this.iconColor(locationData.updated_at) != 'green' && (this.mapFilterr == '5' || this.mapFilterr == '')
                ? this.driverIcons['free_internet'] : (this.mapFilterr == '5' || this.mapFilterr == '')
                  ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
            }
            // driverIcon = isTesla ? this.iconColor(locationData.updated_at) != 'green' && this.mapFilter == '5' || this.mapFilter == ''
            //   ? this.teslaIcons['free_internet'] : locationData.gps_strength > 50 && this.mapFilter == '5' || this.mapFilter == ''
            //     ? locationData.tripStatus == ''
            //       ? vehicleStatus != 'offline'
            //         ? this.teslaIcons['free_gps'] : this.teslaIcons[vehicleStatus] : this.teslaIcons['free_gps'] : this.teslaIcons[vehicleStatus] : this.iconColor(locationData.updated_at) != 'green' && this.mapFilter == '5'
            //     ? this.driverIcons['free_internet'] : locationData.gps_strength > 50
            //       ? locationData.tripStatus == '' ? vehicleStatus != 'offline' ? this.driverIcons['free_gps'] : this.driverIcons[vehicleStatus] : this.driverIcons['gps'] : this.driverIcons[vehicleStatus];
            // console.log(this.iconColor(locationData.updated_at))
          }
        }
        // if (locationData.trip_status == 'trip_started') {
        // let chain = Promise.resolve()
        // chain.then(() => this.Wait(i)).then(() =>
        this.displayDriverOnMap(randomLat, randomLng, id, driverIcon, i); //);
        this.csvList.push({ icon: driverIcon, data: locationData })
        //}
        return {
          removeVehicle: false,
          vehicleType: isTesla ? "Tesla" : "Limo"
        };
      }
    }
    return { removeVehicle: false, vehicleType: null };
  }

  public getVehicleForSocket(vehicle_id) {
    this.positions = [];
    this.positions1 = [];
    //this.liveTracking(vehicle_id);
    this.vehicle_id = vehicle_id;
    if (vehicle_id) {
      this.getRefreshTime('6');
      this.setFilterLoading(
        "#dropdownMenuButton",
        false,
        this.mapFilterLabels[7]
      );
      let params = {
        vehicle_id: vehicle_id,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array

      }
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._devicelocationService
        .getdatabyvehicleidNew(data1)
        .then(vehicleFreedata => {
          if (vehicleFreedata && vehicleFreedata.status == 200) {
            vehicleFreedata = this._EncDecService.dwt(this.session_token, vehicleFreedata.data);
            this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
            this.selectDriverData = vehicleFreedata.driversArr;
            this.selectVehicleData = vehicleFreedata.vehicleArr;
            this.deviceLocationData = vehicleFreedata.getdeviceloc;
            let id;
            if (
              this.SingledeviceLocationData.length > 0 &&
              typeof this.SingledeviceLocationData[0] != "undefined" &&
              this.SingledeviceLocationData[0].latitude != ""
            ) {
              for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                const randomLat = this.SingledeviceLocationData[i].latitude;
                const randomLng = this.SingledeviceLocationData[i].longitude;
                this.doCenter({
                  lat: parseFloat(randomLat),
                  lng: parseFloat(randomLng)
                });
                if (this.SingledeviceLocationData[i]["device_id"]) {
                  id = this.SingledeviceLocationData[i]["device_id"];
                } else {
                  id = "";
                }
                let vehicleStatus = this.getVehicleStatus(
                  this.selectVehicleData[i]
                );
                var driverIcon = this.driverIcons.loggedIn;
                let vehicleModelIsValid =
                  this.SingledeviceLocationData[i].vehicle_model_id &&
                  (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                    this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                let isTesla =
                  vehicleModelIsValid &&
                  (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                    this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                var driverIcon = this.driverIcons.loggedIn;
                if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                  driverIcon = this.teslaIcons['loggedOut'];
                }
                else if (vehicleStatus == 'offline') {
                  if (isTesla)
                    driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                  else
                    driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                      ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                }
                else {
                  if (this.SingledeviceLocationData[i].trip_status !== "") {
                    if (isTesla) {
                      switch (this.SingledeviceLocationData[i].trip_status) {
                        case 'accepted_by_driver':
                          driverIcon = this.teslaIcons['accepted'];
                          break;
                        case 'confirming_rate':
                          driverIcon = this.teslaIcons['almost_free'];
                          break;
                        case 'selecting_payment':
                          driverIcon = this.teslaIcons['almost_free'];
                          break;
                        case 'rating':
                          driverIcon = this.teslaIcons['almost_free'];
                          break;
                        default:
                          driverIcon = this.teslaIcons[vehicleStatus]
                          break;
                      }
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                    }
                    else {
                      switch (this.SingledeviceLocationData[i].trip_status) {
                        case 'accepted_by_driver':
                          driverIcon = this.driverIcons['accepted'];
                          break;
                        case 'confirming_rate':
                          driverIcon = this.driverIcons['almost_free'];
                          break;
                        case 'selecting_payment':
                          driverIcon = this.driverIcons['almost_free'];
                          break;
                        case 'rating':
                          driverIcon = this.driverIcons['almost_free'];
                          break;
                        default:
                          driverIcon = this.driverIcons[vehicleStatus]
                          break;
                      }
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                        ? this.driverIcons['internet'] : driverIcon;
                    }
                  }
                  else {
                    if (isTesla) {
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                        ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                    }
                    else {
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                        ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                    }
                  }
                }
                if (randomLat != "" && randomLng != "" && id != "") {
                  this.positions.push([randomLat, randomLng, id]);
                  this.displayDriverOnMap(
                    randomLat,
                    randomLng,
                    id,
                    driverIcon,
                    0
                  );
                }
                this.removeStaleDriversFromMap();
              }
              this.locationmsg = "";
            } else {
              this.locationmsg = "No";
            }
          }
        });
    } else {
      this.selectDriverData = [];
      this.locationmsg = "No";
      this.setMapFilter("7");
    }
  }

  public setVehicleFilter(event) {
    this.check_filter_interval = "no";
    this.check_interval = "no";
    //console.log('event', typeof event)
    if (typeof event === "object") {
      this.driverFilter = "";
      clearInterval(this.refreshTime2);
      this.getRefreshTime('6');
      this.vehicle_id = event._id;
      //console.log('Vehicle Based Tracking', this.vehicle_id);
      this.positions = [];
      this.positions1 = [];
      //this.liveTracking(this.vehicle_id);
      if (this.vehicle_id) {
        let params = {
          vehicle_id: this.vehicle_id,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._devicelocationService
          .getdatabyvehicleidNew(data1)
          .then(vehicleFreedata => {
            if (vehicleFreedata && vehicleFreedata.status == 200) {
              vehicleFreedata = this._EncDecService.dwt(this.session_token, vehicleFreedata.data);
              this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
              this.selectDriverData = vehicleFreedata.driversArr;
              this.selectVehicleData = vehicleFreedata.vehicleArr;
              this.deviceLocationData = vehicleFreedata.getdeviceloc;
              //this.vehicleFilter = this.selectVehicleData[0];
              //this.plateFilter = this.selectVehicleData[0];
              let id;
              //console.log(' this.SingledeviceLocationData.length ', this.SingledeviceLocationData.length);
              if (
                this.SingledeviceLocationData.length > 0 &&
                typeof this.SingledeviceLocationData[0] != "undefined" &&
                this.SingledeviceLocationData[0].latitude != ""
              ) {
                for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                  const randomLat = this.SingledeviceLocationData[i].latitude;
                  const randomLng = this.SingledeviceLocationData[i].longitude;
                  this.doCenter({
                    lat: parseFloat(randomLat),
                    lng: parseFloat(randomLng)
                  });
                  if (this.SingledeviceLocationData[i]["device_id"]) {
                    id = this.SingledeviceLocationData[i]["device_id"];
                  } else {
                    id = "";
                  }
                  let vehicleStatus = this.getVehicleStatus(
                    this.selectVehicleData[i]
                  );
                  var driverIcon = this.driverIcons.loggedIn;
                  let vehicleModelIsValid =
                    this.SingledeviceLocationData[i].vehicle_model_id &&
                    (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                      this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                  let isTesla =
                    vehicleModelIsValid &&
                    (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                      this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                  var driverIcon = this.driverIcons.loggedIn;
                  if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                    driverIcon = this.teslaIcons['loggedOut'];
                  }
                  else if (vehicleStatus == 'offline') {
                    if (isTesla)
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                    else
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                        ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                  }
                  else {
                    if (this.SingledeviceLocationData[i].trip_status !== "") {
                      if (isTesla) {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.teslaIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.teslaIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                      }
                      else {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.driverIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.driverIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                          ? this.driverIcons['internet'] : driverIcon;
                      }
                    }
                    else {
                      if (isTesla) {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                      }
                      else {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                      }
                    }
                  }





                  if (randomLat != "" && randomLng != "" && id != "") {
                    this.positions.push([randomLat, randomLng, id]);
                    this.displayDriverOnMap(
                      randomLat,
                      randomLng,
                      id,
                      driverIcon,
                      0
                    );
                  }
                  this.removeStaleDriversFromMap();
                }
                this.locationmsg = "";
              } else {
                this.locationmsg = "No";
              }
            }
          });
      } else {
        this.selectDriverData = [];
        this.locationmsg = "No";
        this.setMapFilter("7");
      }
    }
  }
  public setDriverFilter(event) {
    this.check_filter_interval = "no";
    this.check_interval = "no";
    // this.selectVehicleData='';
    if (typeof event === "object") {
      this.vehicleFilter = "";
      this.sideFilter = "";
      clearInterval(this.refreshTime2)
      this.getRefreshTime('6');
      const vehicle_id = event.vehicle_id;
      //console.log('vehicle_id>>>>', vehicle_id);
      this.positions = [];
      this.positions1 = [];
      //this.liveTracking(vehicle_id);
      this.vehicle_id = vehicle_id;
      if (vehicle_id != null) {
        let params = {
          vehicle_id: this.vehicle_id,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._devicelocationService
          .getdatabyvehicleid(data1)
          .then(vehicleFreedata => {
            if (vehicleFreedata && vehicleFreedata.status == 200) {
              vehicleFreedata = this._EncDecService.dwt(this.session_token, vehicleFreedata.data);
              this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
              this.selectDriverData = vehicleFreedata.driversArr;
              this.selectVehicleData = vehicleFreedata.vehicleArr;
              this.deviceLocationData = vehicleFreedata.getdeviceloc;
              //this.vehicleFilter = this.selectVehicleData[0];
              //this.plateFilter = this.selectVehicleData[0];
              let id;
              //console.log(' this.SingledeviceLocationData.length ', this.SingledeviceLocationData.length);
              if (
                this.SingledeviceLocationData.length > 0 &&
                typeof this.SingledeviceLocationData[0] != "undefined" &&
                this.SingledeviceLocationData[0].latitude != ""
              ) {
                //console.log(this.SingledeviceLocationData);
                for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                  const randomLat = this.SingledeviceLocationData[i].latitude;
                  const randomLng = this.SingledeviceLocationData[i].longitude;
                  this.doCenter({
                    lat: parseFloat(randomLat),
                    lng: parseFloat(randomLng)
                  });
                  if (this.SingledeviceLocationData[i]["device_id"]) {
                    id = this.SingledeviceLocationData[i]["device_id"];
                  } else {
                    id = "";
                  }
                  let vehicleStatus = this.getVehicleStatus(
                    this.selectVehicleData[i]
                  );
                  var driverIcon = this.driverIcons.loggedIn;
                  let vehicleModelIsValid =
                    this.SingledeviceLocationData[i].vehicle_model_id &&
                    (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                      this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                  let isTesla =
                    vehicleModelIsValid &&
                    (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                      this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                  var driverIcon = this.driverIcons.loggedIn;
                  if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                    driverIcon = this.teslaIcons['loggedOut'];
                  }
                  else if (vehicleStatus == 'offline') {
                    if (isTesla)
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                    else
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                        ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                  }
                  else {
                    if (this.SingledeviceLocationData[i].trip_status !== "") {
                      if (isTesla) {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.teslaIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.teslaIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                      }
                      else {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.driverIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.driverIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                          ? this.driverIcons['internet'] : driverIcon;
                      }
                    }
                    else {
                      if (isTesla) {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                      }
                      else {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                      }
                    }
                  }
                  if (randomLat != "" && randomLng != "" && id != "") {
                    this.positions.push([randomLat, randomLng, id]);
                    this.displayDriverOnMap(
                      randomLat,
                      randomLng,
                      id,
                      driverIcon,
                      0
                    );
                  }
                  this.removeStaleDriversFromMap();
                }
                this.locationmsg = "";
              } else {
                this.locationmsg = "No";
              }
            }
          });
      } else {
        this.selectDriverData = [];
        this.locationmsg = "No";
        this.setMapFilter("7");
      }
    }
  }
  public doCenter(place: any) {
    this.center = place;
  }
  ngOnDestroy() {
    if (this.livetrackinginterval) {
      console.log("clear interval");
      clearInterval(this.livetrackinginterval);
    }
    console.log("Clearing dispatcher Interval");
    //this.stop.bind(this);
    this.subscripttimestart.unsubscribe();
    this.subscripttimeout.unsubscribe();
    clearInterval(this.refreshTime2);
  }

  closePop() {
    this.dialog.closeAll();
  }
  getVehicleStatus(vehicle) {
    if (vehicle["booked_status"] === "1") {
      return "booked";
    } else if (vehicle["online_status"] !== "1" && vehicle["booked_status"] !== "1") {
      if (vehicle["pause_reason"] == 'uber/careem')
        return "uber_careem"
      else if (vehicle["pause_reason"] == 'break')
        return "break";
      else
        return "offline";
    } else if (vehicle["booked_status"] !== "1" && vehicle["online_status"] === "1" && vehicle["occupied_status"] === "1") {
      return "free";
    }
    else {
      return "loggedIn";
    }
  }
  Wait(i) {
    return new Promise(r => setTimeout(r, i * 30));
  }
  public showOrderTrack = false;
  public orderButton = false;
  public message = "";
  shiftpop(id) {
    this.showOrderTrack = true;
    this.side = id;
    this.vehicle_id = '';
    //this.showCar({ '_id': id });
  }
  clearEndDate() {
    this.selectedMoment1 = "";
    this.message = "";
    this.max2 = new Date(moment(this.selectedMoment).add(1, 'days').format('YYYY-MM-DD HH:mm'));
    this.selectedMoment1 = this.max2;
  }
  public showCover = false;
  trackOrder() {
    this.startmarker = "";
    this.destinationmarker = "";
    this.orderButton = true;
    for (let i = 0; i < this.travelled_markers.length; i++) {
      this.travelled_markers[i].setMap(null);
      if (i == this.travelled_markers.length - 1) this.travelled_markers = [];
    }
    if (this.selectedMoment != "" && this.side !== undefined && this.side !== '' && this.side !== null && typeof this.side === "object") {
      this.orderButton = false;
      this.message = "";
      this.positions1 = [];
      this.showCover = true;
      let param = {
        start_date: moment(this.selectedMoment).format('YYYY-MM-DD HH:mm'),
        end_date: this.selectedMoment1
          ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm')
          : moment(this.selectedMoment).add(1, 'days').format('YYYY-MM-DD HH:mm'),
        vehicle_id: this.side._id ? this.side._id : "",
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      };
      console.log(param);
      let enc_data = this._EncDecService.nwt(this.session_token, param);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this.positions3 = [];
      this._devicelocationService.vehicleTrackLocations(data1).then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          console.log(data);
          this.travelled_cordinates = data.locations;
          let position;
          if (
            this.travelled_cordinates.length > 0 &&
            this.travelled_cordinates.length != ""
          ) {
            for (var i = 0; i < this.travelled_cordinates.length; ++i) {
              let data2 = [
                this.travelled_cordinates[i].latitude,
                this.travelled_cordinates[i].longitude,
                this.travelled_cordinates[i].speed,
                this.travelled_cordinates[i].created_at,
                this.travelled_cordinates[i].is_gps_on,
                this.travelled_cordinates[i].is_internet_on,
                this.travelled_cordinates[i].accuracy,
                this.travelled_cordinates[i].battery,
                this.travelled_cordinates[i].l_id
              ];
              let orderFlag =
                this.travelled_cordinates[i].l_id == "" ? false : true;

              position = new google.maps.LatLng(
                parseFloat(this.travelled_cordinates[i].latitude),
                parseFloat(this.travelled_cordinates[i].longitude)
              );

              if (i > 0) {
                this.drawPoints(
                  [
                    this.travelled_cordinates[i - 1].latitude,
                    this.travelled_cordinates[i - 1].longitude
                  ],
                  [
                    this.travelled_cordinates[i].latitude,
                    this.travelled_cordinates[i].longitude
                  ],
                  position,
                  data2,
                  i,
                  this.travelled_cordinates.length,
                  orderFlag
                );
              } else {
                var img = new Image();
                img.src = "assets/img/map/arrow_yellow.png";
                let icon = new RotateIcon2({ img: img })
                  .setRotation({ deg: 0 })
                  .getUrl();
                let marker = new google.maps.Marker({
                  position: position,
                  map: this.map,
                  icon: icon
                });
                this.travelled_markers.push(marker);
                marker.addListener("mouseover", () => {
                  this.onOrderMarkerClick(marker, data);
                });
              }
            }
          }
          let length = this.travelled_cordinates.length - 1;
          if (length >= 0) {
            this.startmarker = JSON.parse(
              "[" +
              this.travelled_cordinates[length].latitude +
              "," +
              this.travelled_cordinates[length].longitude +
              "]"
            );
            this.doCenter({
              lat: parseFloat(this.travelled_cordinates[0].latitude),
              lng: parseFloat(this.travelled_cordinates[0].longitude)
            });
            this.map.setZoom(12);
            this.destinationmarker = JSON.parse(
              "[" +
              this.travelled_cordinates[0].latitude +
              "," +
              this.travelled_cordinates[0].longitude +
              "]"
            );
          }
          else
            this.showCover = false;
          this.orderButton = false;
        }
        let length = this.travelled_cordinates.length - 1;
        if (length >= 0) {
          this.startmarker = JSON.parse(
            "[" +
            this.travelled_cordinates[length].latitude +
            "," +
            this.travelled_cordinates[length].longitude +
            "]"
          );
          // this.doCenter({
          //   lat: parseFloat(this.travelled_cordinates[0].latitude),
          //   lng: parseFloat(this.travelled_cordinates[0].longitude)
          // });
          // this.map.setZoom(12);
          this.destinationmarker = JSON.parse(
            "[" +
            this.travelled_cordinates[0].latitude +
            "," +
            this.travelled_cordinates[0].longitude +
            "]"
          );
        }
        else
          this.showCover = false;
        this.orderButton = false;
      });
    } else {
      this.message = "Please fill all fields";
      this.orderButton = false;
    }
  }
  drawPoints(target, source, position, data, i, length, flag) {
    let steps = 10;
    let deltaLat = (target[0] - source[0]) / steps;
    let deltaLng = (target[1] - source[1]) / steps;
    var rotation = Math.atan2(deltaLng, deltaLat) * (180 / Math.PI);
    var img = new Image();
    img.src = "assets/img/map/arrow_blue.png";
    var img2 = new Image();
    img2.src = "assets/img/map/arrow_yellow.png";
    img.onload = () => {
      let icon;
      if (flag)
        icon = new RotateIcon2({ img: img })
          .setRotation({ deg: rotation })
          .getUrl();
      else
        icon = new RotateIcon2({ img: img2 })
          .setRotation({ deg: rotation })
          .getUrl();
      let marker = new google.maps.Marker({
        position: position,
        map: this.map,
        icon: icon
      });
      this.travelled_markers.push(marker);
      marker.addListener("mouseover", () => {
        this.onOrderMarkerClick(marker, data);
      });
      if (length - 1 == i) {
        this.showCover = false;
      }
    };
  }
  public travelled_cordinates;
  public travelled_markers = [];
  public ordermarkers = [];
  public ordermarker = {
    speed: "",
    battery: null,
    date: null,
    is_gps_on: true,
    is_internet_on: true,
    display: true,
    accuracy: null,
    order: null
  };
  public startmarker;
  public destinationmarker;
  onOrderMarkerClick(marker, marker_details) {
    this.infoWindow.setContent("");
    this.infoWindow.close();
    this.ordermarker.speed = marker_details[2] !== 0 && marker_details[2] !== null && marker_details[2] !== "" ? parseInt(marker_details[2]) + " KM/H" : "NA";
    this.ordermarker.date = marker_details[3];
    this.ordermarker.is_gps_on = marker_details[4];
    this.ordermarker.is_internet_on = marker_details[5];
    this.ordermarker.accuracy = marker_details[6]
      ? Math.round(marker_details[6])
      : "";
    this.ordermarker.battery = marker_details[7]
      ? Math.round(marker_details[7])
      : "";
    this.ordermarker.order = marker_details[8] == "" ? "Free" : "In trip";
    setTimeout(() => {
      let infoWindowContent = <Element>(
        document.getElementById("vehicleTrackwindow").cloneNode(true)
      );
      infoWindowContent.setAttribute("style", "display:block;");
      this.infoWindow.setContent(infoWindowContent);
      this.infoWindow.open(this.map, marker);
    }, 500)

  }
  public interval;
  public positions3 = [];
  showCar(event) {
    this.check_filter_interval = "no";
    this.check_interval = "no";
    this.positions3 = [];
    if (typeof event === "object") {
      this.setMapFilter("7");
      let vehicle_id = event._id;
      clearInterval(this.refreshTime2);
      this.getRefreshTime('6');
      //console.log('Vehicle Based Tracking', this.vehicle_id);
      if (vehicle_id) {
        //this.vehicle_id = vehicle_id;
        let params = {
          vehicle_id: this.vehicle_id,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array

        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._devicelocationService
          .getdatabyvehicleid(data1)
          .then(vehicleFreedata => {
            if (vehicleFreedata && vehicleFreedata.status == 200) {
              vehicleFreedata = this._EncDecService.dwt(this.session_token, vehicleFreedata.data);
              console.log(JSON.stringify(vehicleFreedata))
              this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
              this.selectDriverData = vehicleFreedata.driversArr;
              this.selectVehicleData = vehicleFreedata.vehicleArr;
              this.deviceLocationData = vehicleFreedata.getdeviceloc;
              //this.vehicleFilter = this.selectVehicleData[0];
              //this.plateFilter = this.selectVehicleData[0];
              let id;
              //console.log(' this.SingledeviceLocationData.length ', this.SingledeviceLocationData.length);
              if (
                this.SingledeviceLocationData.length > 0 &&
                typeof this.SingledeviceLocationData[0] != "undefined" &&
                this.SingledeviceLocationData[0].latitude != ""
              ) {
                for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                  const randomLat = this.SingledeviceLocationData[i].latitude;
                  const randomLng = this.SingledeviceLocationData[i].longitude;
                  this.doCenter({
                    lat: parseFloat(randomLat),
                    lng: parseFloat(randomLng)
                  });
                  if (this.SingledeviceLocationData[i]["device_id"]) {
                    id = this.SingledeviceLocationData[i]["device_id"];
                  } else {
                    id = "";
                  }
                  let vehicleStatus = this.getVehicleStatus(
                    this.selectVehicleData[i]
                  );
                  var driverIcon = this.driverIcons.loggedIn;
                  let vehicleModelIsValid =
                    this.SingledeviceLocationData[i].vehicle_model_id &&
                    (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                      this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                  let isTesla =
                    vehicleModelIsValid &&
                    (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                      this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                  var driverIcon = this.driverIcons.loggedIn;
                  if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                    driverIcon = this.teslaIcons['loggedOut'];
                  }
                  else if (vehicleStatus == 'offline') {
                    if (isTesla)
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                    else
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                        ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                  }
                  else {
                    if (this.SingledeviceLocationData[i].trip_status !== "") {
                      if (isTesla) {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.teslaIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.teslaIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                      }
                      else {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.driverIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.driverIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                          ? this.driverIcons['internet'] : driverIcon;
                      }
                    }
                    else {
                      if (isTesla) {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                      }
                      else {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                      }
                    }
                  }

                  if (randomLat != "" && randomLng != "" && id != "") {
                    this.positions.push([randomLat, randomLng, id]);
                    this.displayDriverOnMap(
                      randomLat,
                      randomLng,
                      id,
                      driverIcon,
                      0
                    );
                  }
                  this.removeStaleDriversFromMap();
                }
                this.locationmsg = "";
              } else {
                this.locationmsg = "No";
              }
            }
          });
      }
    }
  }
  public getRefreshTime(param) {
    let value = 40000;
    clearInterval(this.refreshTime2);
    switch (param) {
      case "1":
        value = 60000;
        this.delayMarker = 1;
        break;
      case "3":
        value = 120000;
        this.delayMarker = 3;
        break;
      case "5":
        value = 60000;
        this.delayMarker = 2;
        break;
      case "6":
        value = 60000;
        this.delayMarker = 1;
        break;
      default:
        this.delayMarker = 3;
        value = 120000;
    }
    let that = this;
    that.refreshTime2 = setInterval(function () {
      if (that.vehicle_id) {
        that.getVehicleForSocket(that.vehicle_id);
      }
      else {
        that.setMapFilter(that.mapFilterr);
      }
    }, value);
  }
  public getDriverGroupsForListing() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'unique_driver_model_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._driverService.getDriversGroup(data1).subscribe((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.driverGroupData = data.getDriverGroups;
        this.tempdriverGroupData = data.getDriverGroups;
      }
    });
  }
  changeDriverGroupsMap(event) {
    let t3 = [];
    let PositionArray = [];
    this.driverGroupMap = "";
    if (event.value == 'All') {
      this.setMapFilter(this.mapFilterr)
      return;
    }
    let index = this.tempdriverGroupData.findIndex(x => x._id == event.value);
    PositionArray.push(index);
    for (let j = this.tempdriverGroupData.length - 1; j >= 0; j--) {
      let index = PositionArray.indexOf(j)
      if (index !== -1)
        this.driverGroupMap = this.driverGroupMap + "1";
      else
        this.driverGroupMap = this.driverGroupMap + "0";
    }
    let temp_ids = this.driverGroupMap;
    temp_ids = temp_ids.replace(/^0+/, '');
    let temp_before_id = temp_ids;
    t3.push(temp_ids);
    if (this.tempdriverGroupData.length > temp_ids.length) {
      for (let j = 0; j < this.tempdriverGroupData.length - temp_ids.length; j++) {
        temp_before_id = "0" + temp_before_id;
        t3.push(temp_before_id);
      }
    }
    this.driverGroupMap = t3;
    this.setMapFilter(this.mapFilterr)
  }
  iconColor(dt: any) {
    let date1 = new Date(dt).getTime();
    let date2 = new Date(this.uaeTime).getTime();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";
    if (hoursDiff <= 180) {
      output = "green";
    } else if (hoursDiff < 60 * 60) {
      output = "orange";
    } else {
      output = "red";
    }
    return output;
  }
  changeMinues() {
    this.setMapFilter(this.mapFilterr);
  }
  changeMinues2(min) {
    this.paused = min;
    this.setMapFilter(this.mapFilterr);
  }
  public minutesS = '0';
  public minutesM = '3';
  public minutesLimitS = '0';
  public minutesLimitM = '10000';
  public paused = true;
  iconColor2(dt: any) {
    let date1 = new Date(dt).getTime();
    let date2 = new Date(this.uaeTime).getTime();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";
    let minutes = (parseInt(this.minutesM) * 60) + parseInt(this.minutesS);
    let minutesLimit = (parseInt(this.minutesLimitM) * 60) + parseInt(this.minutesLimitS);
    if (hoursDiff <= minutes) {
      output = "green";
    } else if ((hoursDiff >= minutesLimit) && !this.hidden) {
      output = "green";
    } else {
      output = "red";
    }
    return output;
  }
  public internetFlag = false;
  public gpsFlag = false;
  public setVehicleStatusFilter(vehicleType) {
    for (let i = 0; i < this.driverMarkers.length; i++) {
      this.driverMarkers[i].marker.setMap(null);
      if (i == this.driverMarkers.length - 1) this.driverMarkers = [];
    }
    this.positions = [];
    this.positions1 = [];
    this.deviceLocationData = [];
    if (vehicleType != '6')
      this.tripStatus = [];
    // this.driverFilter = "";
    // this.vehicleFilter = "";
    // this.sideFilter = "";
    this.hidden = true;
    this.internetFlag = this.gpsFlag = false;
    if (vehicleType == '0') {
      this.internetFlag = true;
      this.mapFilterr = '';
      this.hidden = false;
    }
    else if (vehicleType == '2') {
      this.gpsFlag = true;
      this.mapFilterr = '';
    }
    else
      this.mapFilterr = vehicleType;
    this.setMapFilter(this.mapFilterr);
  }
  public tripStatus = [];
  public uberFlag = false;
  public breakFlag = false;
  setTripStatus(status) {
    this.tripStatus = [];
    this.uberFlag = false;
    this.breakFlag = false;
    this.hidden = true;
    this.vehicleFilter = "";
    this.sideFilter = "";
    this.driverFilter = "";
    switch (status) {
      case 0:
        this.tripStatus = [];
        break;
      case 1:
        this.tripStatus.push('accepted_by_driver');
        break;
      case 2:
        this.tripStatus.push('confirming_rate', 'selecting_payment', 'rating');
        break;
      case 3:
        this.uberFlag = true;
        break;
      case 4:
        this.breakFlag = true;
        break;
      default:
        break;
    }
    if (this.breakFlag || this.uberFlag) {
      for (let i = 0; i < this.driverMarkers.length; i++) {
        this.driverMarkers[i].marker.setMap(null);
        if (i == this.driverMarkers.length - 1) this.driverMarkers = [];
      }
      this.positions = [];
      this.positions1 = [];
      this.deviceLocationData = [];
      this.setMapFilter('1');
    }
    else
      this.setVehicleStatusFilter('6');
  }
  unhide() {
    this.hidden = false;
  }
  clickedSignedOut(marker, detail, icon) {
    this.check_interval = "no";
    let vehicle_id = detail["vehicle_id"];
    const params = {
      vehicle_id: vehicle_id,
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    };
    if (!this.infoWindow) {
      this.infoWindow = new google.maps.InfoWindow();
      // let that = this;
      // this.infoWindow.addListener('closeclick', function () {
      //   if (that.infoMarker)
      //     that.infoMarker.setMap(null);
      //   that.infoMarker = null;
      // });
    }
    if (vehicle_id) {
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._shiftsService.getAssignedSignedOutDrivers(data1).then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          let popupData = data.shiftinfo.device_id;
          //let correctDriver = popupData.driver_id.username
          let status;
          if (popupData) {
            if (data.shiftinfo.vehicle_id)
              if (data.shiftinfo.vehicle_id.online_status == "1") {
                status = "online";
              } else {
                status = "offline";
              }
            this.infoWindow.setContent("");
            this.marker.updated_at = popupData.updated_at;
            this.marker.device_id = popupData.unique_device_id;
            this.marker.device_model = popupData.device_model;
            this.marker.vehicle_id = data.shiftinfo.vehicle_id
              ? data.shiftinfo.vehicle_id.vehicle_identity_number
              : "";
            this.marker.device_type = status;
            //this.marker.speed = parseInt(popupData.speed);
            //this.marker.gps_strength = String(parseInt(popupData.gps_strength));
            //this.marker.battery = popupData.battery;
            this.marker.shift_start = data.shiftinfo ? data.shiftinfo.shift_start : '';
            this.marker.shift_end = data.shiftinfo ? data.shiftinfo.shift_end : '';
            this.marker.imei = popupData.imei;
            this.marker.vehicle_name = data.shiftinfo.vehicle_id
              ? String(
                data.shiftinfo.vehicle_id.display_name
              ).toUpperCase() : '';
            this.marker.vehicle_make = data.shiftinfo.vehicle_id ? data.shiftinfo.vehicle_id.car_manufacturer_desc
              ? String(data.shiftinfo.vehicle_id.car_manufacturer_desc).toUpperCase()
              : "" : "";
            this.marker.vehicle_model = "";
            this.marker.vehicle_fuel_type = data.shiftinfo.vehicle_id
              ? data.shiftinfo.vehicle_id.car_fuel_type : '';
            this.marker.company = "Dubai Taxi";
            this.marker.driver_name = data.shiftinfo.driver_id ?
              String(
                data.shiftinfo.driver_id.display_name
              ).toUpperCase() : '';
            this.marker.driver_id = data.shiftinfo.driver_id
              ? data.shiftinfo.driver_id._id : '';
            this.marker.emp_id = data.shiftinfo.driver_id
              ? data.shiftinfo.driver_id.emp_id : '';
            this.marker.driverphone = data.shiftinfo.driver_id
              ? data.shiftinfo.driver_id.phone_number : '';
            this.marker.status = this.vehicleStatus[icon];
            this.marker.display = true;
            this.ref.detectChanges();
            let infoWindowContent = <Element>(
              document.getElementById("iw").cloneNode(true)
            );
            infoWindowContent.id = "iw-2";
            infoWindowContent.setAttribute("style", "display:block;");
            this.infoWindow.setContent(infoWindowContent);
            this.infoWindow.open(this.map, marker);
            this.infoMarker = null;
            let that = this;
            google.maps.event.addListener(this.infoWindow, 'domready', function () {
              var clickableItem = document.getElementById('clicka');
              if (that.dtc)
                clickableItem.addEventListener('click', () => {
                  that.shiftpop(data.shiftinfo.vehicle_id)
                })
            });
          } else {
            this.marker.device_id = "";
            this.marker.device_model = "";
            this.marker.vehicle_id = "";
            this.marker.device_type = "";
            this.marker.imei = "";
            this.marker.vehicle_name = "";
            this.marker.vehicle_make = "";
            this.marker.vehicle_model = "";
            this.marker.vehicle_fuel_type = "";
            this.marker.company = "Dubai Taxi";
            this.marker.driver_image = "/assets/img/map/driver.jpg";
            this.marker.driver_name = "";
            this.marker.driver_id = "";
            this.marker.emp_id = "";
            this.marker.driverphone = "";
            this.marker.speed = 0;
            this.marker.gps_strength = "";
            this.marker.battery = "";
            this.marker.vehicle_image =
              "/assets/img/map/1428584997LEXUS_ES350.jpg";
            this.marker.pickup_location = "";
            this.marker.shift_end = "";
            this.marker.status = ""
          }
        }
      });
    }
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.company_id_array
    };
    var encrypted = this._EncDecService.nwt(this.session_token, param);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.companyData = data.getCompanies;
        if (this.dtc && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
          this.company_id_list = data.getCompanies.map(x => x._id)
          this.companyIdFilter = data.getCompanies.map(x => x._id)
          this.companyIdFilter.push('all')
        }
        this.setMapFilter("6");
      }
      //this.searchSubmit = false;
    });
  }

  public createCsv() {
    this.createCsvFlag = true;
    let labels = [
      'Vehicle Side Number',
      'Driver id',
      'Driver name',
      'Driver phone number',
      'Status',
      'Last updated'
    ];
    let resArray = [];
    for (let i = 0; i < this.csvList.length; i++) {
      const csvArray = this.csvList[i];
      resArray.push
        ({
          side_number: csvArray.data.vehicle_ids && csvArray.data.vehicle_ids.vehicle_identity_number ? csvArray.data.vehicle_ids.vehicle_identity_number : 'NA',
          driver_id: csvArray.data.drivers && csvArray.data.drivers.emp_id ? csvArray.data.drivers.emp_id : 'NA',
          driver_name: csvArray.data.drivers && csvArray.data.drivers.display_name ? csvArray.data.drivers.display_name : 'NA',
          phone_number: csvArray.data.drivers && csvArray.data.drivers.phone_number ? csvArray.data.drivers.phone_number : 'NA',
          status: csvArray.icon ? this.vehicleStatus[csvArray.icon] : 'NA',
          updated: csvArray.data.updated_at ? csvArray.data.updated_at : 'NA',
        });
    }
    var options =
    {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      headers: (labels)
    };
    this.createCsvFlag = false;
    new Angular2Csv(resArray, 'Device-List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
  }
  companyManualSelect() {
    this.mapSocketCountList = [];
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.company_id_list = this.companyIdFilter.slice()
    }
    else {
      this.company_id_list = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }

  public aclDisplayService() {
    var params = {
      company_id: this.company_id_array
    }
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Dispatcher-Map csv") {
              this.csvAcl = true;
            }
          };
        }
      }
    })
  }
  getVehicleCategories() {
    const params = {
      offset: 0,
      limit: 100,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.company_id_array
    };
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._vehicleModelsService.getVehicleCategories(enc_data)
      .then(userdata => {
        if (userdata && userdata.status == 200) {
          userdata = this._EncDecService.dwt(this.session_token, userdata.data);
          this.vehicleCategories = userdata.vehicleModelsList.filter(el => el._id !== '5d8c86b7ce733810e243cc26');
        }
      });
  }
}

class RotateIcon {
  options: any;
  img: HTMLImageElement;
  context: CanvasRenderingContext2D;
  canvas: any;

  constructor(options) {
    this.options = options;
    this.img = options.img.cloneNode();
    this.options.width = options.pixel;
    this.options.height = options.pixel;
    let canvas = document.createElement("canvas");
    canvas.width = this.options.width;
    canvas.height = this.options.height;
    this.context = canvas.getContext("2d");
    this.canvas = canvas;
  }

  setRotation(options) {
    let angle = options.deg ? (options.deg * Math.PI) / 180 : options.rad;
    let centerX = this.options.width / 2;
    let centerY = this.options.height / 2;
    this.context.clearRect(0, 0, this.options.width, this.options.height);
    this.context.save();
    this.context.translate(centerX, centerY);
    this.context.rotate(angle);
    this.context.translate(-centerX, -centerY);
    this.context.drawImage(this.img, 0, 0);
    this.context.restore();
    return this;
  }
  getUrl() {
    return this.canvas.toDataURL("image/png");
  }
}
class RotateIcon2 {
  options: any;
  img: HTMLImageElement;
  context: CanvasRenderingContext2D;
  canvas: any;

  constructor(options) {
    this.options = options;
    this.img = options.img.cloneNode();
    this.options.width = 12;
    this.options.height = 12;
    let canvas = document.createElement("canvas");
    canvas.width = this.options.width;
    canvas.height = this.options.height;
    this.context = canvas.getContext("2d");
    this.canvas = canvas;
  }

  setRotation(options) {
    let angle = options.deg ? (options.deg * Math.PI) / 180 : options.rad;
    let centerX = this.options.width / 2;
    let centerY = this.options.height / 2;
    this.context.clearRect(0, 0, this.options.width, this.options.height);
    this.context.save();
    this.context.translate(centerX, centerY);
    this.context.rotate(angle);
    this.context.translate(-centerX, -centerY);
    this.context.drawImage(this.img, 0, 0);
    this.context.restore();
    return this;
  }
  getUrl() {
    return this.canvas.toDataURL("image/png");
  }
}
