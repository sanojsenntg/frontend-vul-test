import {  NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MapComponent } from './map.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NguiMapModule } from '@ngui/map';
import { MatInputModule, MatNativeDateModule,MatSelectModule,MatTooltipModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { DevicelocationService } from './../../common/services/devicelocation/devicelocation.service';
import { DevicelocationRestService } from './../../common/services/devicelocation/devicelocationrest.service';
import {environment} from '../../../environments/environment';
import { ShiftsService } from '../../common/services/shifts/shifts.service';
import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';


@NgModule({
  imports: [
    NguiMapModule.forRoot
    ({apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.map_key + '&libraries=visualization,places,drawing,geometry'}),
    FormsModule,
    BrowserModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatInputModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatTooltipModule,
    BootstrapSwitchModule
  ],
  declarations: [
    MapComponent
  ],
  providers: [ DevicelocationService, DevicelocationRestService, ShiftsService
  ]
})
export class MapModule {}
