import { NgModule, Component, OnInit, AfterViewInit, ViewChild, ViewEncapsulation, ViewContainerRef, ChangeDetectorRef, } from '@angular/core';
import { JwtService } from '../../../common/services/api/jwt.service';
import { Router } from '@angular/router';

import { UsersService } from '../../../common/services/user/user.service';
import { Socket } from 'ng-socket-io';
import * as moment from 'moment/moment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { NotificationComponent } from '../../../common/dialog/notification/notification.component';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { FreeVehicleService } from '../../../common/services/free-vehicles/free_vehicle.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class HeaderComponent implements OnInit {
  public session_token;
  public useremail;
  public company_id_array = [];
  public usercompany;
  public socket_session_token;
  time: Date;
  public dateName;
  public dateTime;
  public connIcon: boolean = true;
  public adminshow = 0;
  public notificationCount = 0;
  public isActive = false;
  companyType: any;
  inactiveCount = 0;
  /**
   *
   * @param {JwtService} _jwtService
   * @param {Router} router
   */
  constructor(private _jwtService: JwtService,
    private router: Router,
    private _usersService: UsersService,
    public socket: Socket,
    private cdRef: ChangeDetectorRef,
    private _EncDecService: EncDecService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    public _FreeVehicleService: FreeVehicleService
  ) {
    const jun = moment(new Date);
    this.dateName = jun.tz('Asia/Dubai').format('MMMM DD');
    this.dateTime = jun.tz('Asia/Dubai').format('HH:mm:ss');
  }

  /**
   * Update time every second
   */
  ngOnInit() {
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany)
    this.companyType = JSON.parse(window.localStorage['companydata']).company_type ? JSON.parse(window.localStorage['companydata']).company_type : '3';
    const vm = this;
    var token = this._jwtService.getToken();
    this._usersService.verifyJWTToken(token, '2').then((data) => {
      if (data.success) {
        console.log('success');
      } else {
        console.log('failed');
        vm.logout();
      }
    });
    this.socket.on('connect', function () {
      vm.connIcon = true;
      vm.cdRef.detectChanges();
      if (window.localStorage['dispatcherUser']) {
        var dispatcher_id = JSON.parse(window.localStorage['dispatcherUser']);
        if (dispatcher_id) {
          dispatcher_id.token = vm._jwtService.getToken();
          let params = {
            dispatcher: dispatcher_id,
            company_id: vm.company_id_array
          }
          let enc_data = vm._EncDecService.sockNwt(vm.socket_session_token, params);
          //alert(this.dispatcher_id.email)
          vm.socket.emit('open_from_dashboard', enc_data);
          //alert(vm.connIcon);
          console.log('Connection is active...');
        }
      }
    });
    this.socket.on('invalid_token_dash', function () {
      vm.logout();
      //alert(vm.connIcon);
    });
    this.socket.on('disconnect', function () {
      vm.connIcon = false;
      vm.cdRef.detectChanges();
      //alert(vm.connIcon);
      console.log('Connection is lost...');
    });
  }
  ngAfterViewInit() {
    const vm = this;
    setInterval(function () {
      var jun = moment(new Date);
      vm.dateName = jun.tz('Asia/Dubai').format('MMMM DD');
      vm.dateTime = jun.tz('Asia/Dubai').format('HH:mm:ss');
    }, 1000);
    this._FreeVehicleService.observableCount.subscribe(item => {
      this.notificationCount = item.devices + item.scheduled + item.timedout + item.free + item.overspeed + this.inactiveCount;
    });
    this.inactiveVehicles();
    setInterval(()=>{
      this.inactiveVehicles();
    }, 15 * 60 * 1000)
  }

  public inactiveVehicles(){
    var params = {
      limit: 1,
      offset: 0,
    }
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._FreeVehicleService.getInactiveVehicles(enc_data).then((dec)=>{
      if(dec && dec.data){
        var data:any =  this._EncDecService.dwt(this.session_token, dec.data);
        this.inactiveCount = data.count;
      }
    })
  }
  /**
   *  Dispatcher Logout
   */
  logout() {
    this._jwtService.destroyDispatcherToken();
    this._jwtService.destroyAdminToken();
    window.localStorage.removeItem('CUser');
    window.localStorage.removeItem("notificationList");
    window.localStorage.removeItem('CUser');
    window.localStorage.removeItem('Sessiontoken');
    window.localStorage.removeItem('user_email');
    window.localStorage.removeItem('user_company');
    window.localStorage.removeItem('companydata');
    window.localStorage.removeItem('SocketSessiontoken');
    sessionStorage.clear();
    // this.socket.removeAllListeners();
    this.router.navigate(['/dispatcher/login']);
  }
  full_map(event) {
    /*this.dialog.closeAll();
    let dialogRef = this.dialog.open(MapComponent, {
      width: '100%',
      height: '95%',
      hasBackdrop: true
    });
    dialogRef.afterClosed().subscribe(result => {

    });*/
    event.preventDefault();
    let url = 'https://mobilityae.com/dispatcher/map'; 
    window.open(url, "_blank", "width=1200,height=700");
  }
  admin() {
    window.localStorage['adminUser'] = window.localStorage['dispatcherUser'];
    this.router.navigate(['/admin/dashboard']);
  }

  public toastAlert() {
    this.isActive = true;
    let dialogRef = this.dialog.open(NotificationComponent, {
      panelClass: 'myapp-no-padding-dialog',
      width: '350px',
      data: '1',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop()
    });
    dialogRef.updatePosition({ top: '50px', right: '50px' });
    dialogRef.afterClosed().subscribe(result => {
      this.isActive = false;
      this._FreeVehicleService.setFreeVehicle(0)
    });
  }
}
