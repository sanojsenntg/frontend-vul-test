import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DispatcherComponent } from './dispatcher.component';
import { MapModule } from './map/map.module';
import { LoginComponent } from './login/login.component';
import { DispatcherService } from '../common/services/dispatcher/dispatcher.service';
import { DispatcherRestService } from '../common/services/dispatcher/dispatcherrest.service';
import { OrderService } from '../common/services/order/order.service';
import { OrderRestService } from '../common/services/order/orderrest.service';
import { HomeComponent } from './home/home.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { DispatcherRoutingModule } from '../app-routing/dispatcher-routing.module';
import { CustomerService } from '../common/services/customer/customer.service';
import { CustomerRestService } from '../common/services/customer/customerrest.service';
import { HeaderComponent } from './layouts/header/header.component';
import { NguiMapModule } from '@ngui/map';
import { CommonModule } from '@angular/common';
import { PartnerService } from '../common/services/partners/partner.service';
import { PartnerRestService } from '../common/services/partners/partnerrest.service';
import { ToasterModule } from 'angular2-toaster';
import { PaymentService } from '../common/services/payment/payment.service';
import { PaymentRestService } from '../common/services/payment/paymentrest.service';
import { TariffService } from '../common/services/tariff/tariff.service';
import { TariffRestService } from '../common/services/tariff/tariffrest.service';
import { VehicleService } from '../common/services/vehicles/vehicle.service';
import { VehicleRestService } from '../common/services/vehicles/vehiclerest.service';
import { DriverService } from '../common/services/driver/driver.service';
import { DriverRestService } from '../common/services/driver/driverrest.service';
import { AdditionalService } from '../common/services/additional_service/additional_service.service';
import { AdditionalrestService } from '../common/services/additional_service/additional_servicerest.service';
import { OrderGroupService } from '../common/services/order_group/order_group.service';
import { OrderGrouprestService } from '../common/services/order_group/order_grouprest.service';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SendMessageComponent } from './vehicle/dialog/send-message/send-message';
import { MessageService } from '../common/services/message/message.service';
import { MessagerestService } from '../common/services/message/messagerest.service';
import { MessageHistoryComponent } from './vehicle/dialog/message-history/message-history';
import { OrderInfoComponent } from './vehicle/dialog/order-info/order-info';
import { ShiftDetailsComponent } from './vehicle/dialog/shift-details/shift-details';
import { ShiftsDetailsService } from '../common/services/shift_details/shift_details.service';
import { ShiftDetailsRestService } from '../common/services/shift_details/shift_detailsrest.service';
import { environment } from '../../environments/environment';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: environment.socketUrl, options: {} };
import { RemoveUnderscorePipe } from '../common/pipes/remove_underscore_pipe';
import { VehicleInfoComponent } from './vehicle/dialog/vehicle-info/vehicle-info.component';
import { CoverageareaService } from '../common/services/coveragearea/coveragearea.service'
import { CoveragearearestService } from '../common/services/coveragearea/coveragearearest.service'
import { MatTooltipModule, MatIconModule, MatChipsModule } from '@angular/material';
import { ReassignComponent } from '../common/dialog/reassign/reassign.component';
import { PoiService } from '../common/services/poi/poi.service';
import { NotificationComponent } from '../common/dialog/notification/notification.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { TagInputModule } from 'ngx-chips';
import { AccessControlService } from '../common/services/access-control/access-control.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CancelSelectedComponent } from '../common/dialog/cancel-selected/cancel-selected.component';
import { KeyIndicatorsComponent } from '../common/dialog/key-indicators/key-indicators.component';
import { EditOrderComponent } from '../common/dialog/edit-order/edit-order.component'
import { AddCommentComponent } from '../common/dialog/add-comment/add-comment.component';
import { ProgressBarModule } from "angular-progress-bar"
import { TwoGisService } from '../common/services/2gis/two-gis.service';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { RemoveSpacePipe } from '../common/pipes/remove_space_pipe';
import { TimeDifferencePipe } from '../common/pipes/time_difference';
import { ClipboardModule } from 'ngx-clipboard';
@NgModule({
  imports: [
    FormsModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatSelectModule,
    DispatcherRoutingModule,
    MatProgressBarModule,
    MapModule,
    CommonModule,
    ToasterModule,
    MatDialogModule,
    MatNativeDateModule,
    MatDatepickerModule,
    BootstrapSwitchModule,
    BrowserAnimationsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgbModule,
    SocketIoModule.forRoot(config),
    NguiMapModule.forRoot
      ({ apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.map_key + '&libraries=visualization,places,drawing,geometry' }),
    BrowserAnimationsModule,
    MatTooltipModule,
    TagInputModule,
    MatCheckboxModule,
    ProgressBarModule,
    MatSlideToggleModule,
    ClipboardModule
  ],
  declarations: [
    DispatcherComponent,
    LoginComponent,
    HomeComponent,
    VehicleComponent,
    HeaderComponent,
    SendMessageComponent,
    MessageHistoryComponent,
    OrderInfoComponent,
    ShiftDetailsComponent,
    RemoveUnderscorePipe,
    VehicleInfoComponent,
    ReassignComponent,
    NotificationComponent,
    RemoveSpacePipe,
    TimeDifferencePipe
  ],
  providers: [
    PoiService,
    DispatcherService,
    DispatcherRestService,
    OrderService,
    OrderRestService,
    CustomerService,
    CustomerRestService,
    PartnerService,
    PartnerRestService,
    PaymentService,
    PaymentRestService,
    TariffService,
    TariffRestService,
    VehicleService,
    VehicleRestService,
    DriverService,
    DriverRestService,
    AdditionalService,
    AdditionalrestService,
    OrderGroupService,
    OrderGrouprestService,
    MessageService,
    MessagerestService,
    ShiftsDetailsService,
    ShiftDetailsRestService,
    CoverageareaService,
    CoveragearearestService,
    AccessControlService,
    TwoGisService
  ],
  exports: [],
  entryComponents: [AddCommentComponent, EditOrderComponent, KeyIndicatorsComponent, CancelSelectedComponent, SendMessageComponent, MessageHistoryComponent, OrderInfoComponent, ShiftDetailsComponent, VehicleInfoComponent, ReassignComponent, NotificationComponent]
})
export class DispatcherModule { }