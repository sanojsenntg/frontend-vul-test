import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ViewEncapsulation,
  ViewContainerRef,
  ChangeDetectorRef,
  OnDestroy,
  NgZone
} from "@angular/core";
import { OrderService } from "../../common/services/order/order.service";
import { JwtService } from "../../common/services/api/jwt.service";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { Router } from "@angular/router";
import { UsersService } from '../../common/services/user/user.service';
import { CustomerService } from "../../common/services/customer/customer.service";
import { CompanyService } from '../../common/services/companies/companies.service';
import { TariffService } from "../../common/services/tariff/tariff.service";
import { ShiftsService } from "../../common/services/shifts/shifts.service";
import { ZoneService } from '../../common/services/zones/zone.service';
import { PartnerService } from "../../common/services/partners/partner.service";
import { DevicelocationService } from "../../common/services/devicelocation/devicelocation.service";
import { VehicleService } from "../../common/services/vehicles/vehicle.service";
import { DriverService } from "../../common/services/driver/driver.service";
import { AdditionalService } from "../../common/services/additional_service/additional_service.service";
import { PaymentService } from "../../common/services/payment/payment.service";
import { CoverageareaService } from "../../common/services/coveragearea/coveragearea.service";
import { PaymentTypeService } from "../../common/services/paymenttype/paymenttype.service";
import { OrderGroupService } from "../../common/services/order_group/order_group.service";
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/map";
import { DirectionsRenderer } from "@ngui/map";
import {
  MatDialog,
  MatAutocompleteSelectedEvent
} from "@angular/material";
import { DeleteDialogComponent } from "../../common/dialog/delete-dialog/delete-dialog.component";
import { CancelReasonComponent } from "../../common/dialog/cancel-reason/cancel-reason.component";
import { ToastsManager } from "ng2-toastr";
import { Overlay } from "@angular/cdk/overlay";
import * as moment from "moment/moment";
declare let $: any;
import { Socket } from "ng-socket-io";
import { environment } from "../../../environments/environment";
// import { Promise, reject } from 'q';
import { } from "q";
import { COMMA, ENTER } from "@angular/cdk/keycodes";
import { UserIdleService } from "angular-user-idle";
import { Subscription } from "rxjs/Subscription";
import { ReassignComponent } from "../../common/dialog/reassign/reassign.component";
import { NotificationComponent } from "../../common/dialog/notification/notification.component";
import { PoiService } from "../../common/services/poi/poi.service";
import { PoiDialogComponent } from "../../common/dialog/poi-dialog/poi-dialog.component";
import { VehicleModelsService } from "../../common/services/vehiclemodels/vehiclemodels.service";
import { AccessControlService } from './../../common/services/access-control/access-control.service';
import { FreeVehicleService } from '../../common/services/free-vehicles/free_vehicle.service';
import { CancelSelectedComponent } from "../../common/dialog/cancel-selected/cancel-selected.component";
import { EditOrderComponent } from "../../common/dialog/edit-order/edit-order.component";
import { DriverDialogComponent } from "../../common/dialog/driver-dialog/driver-dialog.component";
import { AddCommentComponent } from "../../common/dialog/add-comment/add-comment.component";
import { Angular2Csv } from "angular2-csv";
import { GlobalService } from "../../common/services/global/global.service";
import { EncDecService } from "../../common/services/encrypt-decrypt-service/encrypt_decrypt_service";
import { TwoGisService } from "../../common/services/2gis/two-gis.service";
import { DispatchNotificationsComponent } from "../../common/dialog/dispatch-notifications/dispatch-notifications.component";

@Component({
  selector: "app-home",
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  public freeVehicles;
  byStarRating: string[] = [];
  public dtc = false;
  public hideme;
  public searchReasonsChange;
  public timer = 0;
  public companyData;
  public loadingIndicator;
  public matLoadingInd = false;
  public userMatloading = false;
  public sideNoMatLoading = false;
  public vehicleNoMatLoading = false;
  public modelMatLoading = false;
  public mapVehicleMatLoading = false;
  public mapSideMatLoading = false;
  public maploaded = false;
  public showLoader = false;
  public ordersLength;
  public currentPage = 1;
  public searchMode = false;
  public mode;
  public positions1;
  public alldriverData = [];
  public paymenttypeData;
  public isLoading;
  public subscriptions: Subscription[] = [];
  map;
  public trackTime = "";
  beforeData = [];
  afterData = [];
  mapLoader = false;
  public modelList = [];
  public modelList2 = [];
  public modelList4 = [];
  public vehiclesByType;
  public vehicleTypeFilter;
  public vehicleTypeFilter2;
  public mapFilterLabels: any = {};
  public driverIcons: any = {};
  public teslaIcons: any = {};
  private driverMarkers = [];
  public infoWindow: any;
  public singleVehicleTracking;
  public mapClickedAddress;
  public livetrackinginterval;
  public driverGroupData;
  public driverGroupData2;
  public tempdriverGroupData;
  public PositionArray = [];
  public driverGroupDataLength;
  public livetrackingOrder;
  public multigroupselected = false;
  public dispatcherBooking = false;
  public dispatcherCancelOrder = false;
  public dispatcherCopyOrder = false;
  public DispatcherSendMessage = false;
  public DispatcherOrderInfo = false;

  public marker = {
    display: true,
    lat: "",
    lng: "",
    vehicle_image: "",
    device_id: "",
    device_type: "",
    imei: "",
    company: "",
    driver_image: "",
    driver_name: "",
    driver_id: "",
    emp_id: "",
    driverphone: "",
    device_model: "",
    vehicle_make: "",
    vehicle_model: "",
    vehicle_name: "",
    vehicle_fuel_type: "",
    vehicle_id: "",
    device_location_id: "",
    speed: 0,
    gps_strength: "",
    battery: "",
    shift_start: "",
    updated_at: "",
    pickup_location: "",
    shift_end: "",
    status: ""
  };
  public logged_out;
  public normal_pause;
  public uber_careem;
  flag = false;
  public inPick = false;
  public inDrop = false;
  public paymenterror;
  public newparam = "";
  public mapFilter = "7";
  public vehicleFilter = "";
  public pickup_loc: any = [];
  public drop_loc: any = '';
  public sideFilter = "";
  public plateFilter;
  public plateFilter2;
  public driverFilter;
  public automaticorder = true;
  public selectVehicleData = [];
  public selectVehicleData2 = [];
  public selectDriverData = [];
  public selectDriverData2 = [];
  public check_interval = 'yes';
  public check_filter_interval = 'yes';
  public vehicle_id;
  public placeFilter;
  public bookedLength;
  public offlineLength;
  public loggedLength;
  public inMaintenacneLength;
  public freeLength;
  public acceptedLength;
  public almostFreeLength;
  public internetOffLength;
  public gpsOffLength;
  public locationmsg;
  public SingledeviceLocationData;
  public messagetoPopup;
  public orderInfo: any = [];
  public orderInfoKey: any = [];
  public selectedOrder = [];
  public orders: any = [];
  public customerData = [];
  public partnerData: any = [];
  public partnerData2 = [];
  public customerPhone: any = [];
  public selectPartner: any = "";
  private autocomplete: any;
  public pickup_location;
  public pickup_place_id;
  public drop_location;
  public pickup_country;
  public drop_country;
  public tariffData: any = [];
  public tariffDataTemp: any = [];
  public dispatcher_id = {
    _id: ""
  };
  public dispatcher: any;
  public vehicleData: any = [];
  public driverData: any = [];
  public coverageData: any = [];
  public AdditionalData: any = [];
  public orderdata;
  public paymetExtraData: any = [];
  public show: boolean = false;
  public showmap: boolean = false;
  public showmapbtn = false;
  public cdr: any;
  public driver_groups_id = "";
  public driver_groups_id2: any = [];
  public sourceLatitude;
  public sourceLongitude;
  public destinationLatitude;
  public destinationLongtitude;
  public paymentTableData: any = [];
  public payment_extra_ids: any = [];
  public Totalprice;
  public calcPrice: any = 0;
  public price_data: any = 0;
  public center;
  public address: any = {};
  public NumberOfCars: any;
  public OrderGroup: any;
  public positions = [];
  public company_id_array = [];
  picking = [];
  dropOffing = [];
  public display = false;
  public lat;
  public lang;
  public dropOffLat;
  public pickupLang;
  public pickupLat;
  public dropOffLang;
  public markers = [];
  public pickupMarkers = [];
  public dropOffMarkers = [];
  public roughMarkers = [];
  public markerIcon;
  public dropofficon = false;
  public pickupIcon = false;
  clickedAdress;
  markerData;
  clickedLatLang;
  public SelectedCustomer;
  public orderHistory = [];
  public name;
  public phone_number;
  public firstname;
  public lastname;
  public SelectedCustomerAddress = [];
  public selectedCustomerMessages = [];
  public showCustomerInfo = false;
  public selectVehicle = false;
  public selectDriver = false;
  public selectAdditional = false;
  indexStart = 0;
  indexEnd = 10;
  pageSize = 10;
  pageNo = 0;
  totalPage = [1];
  public distribution_status = true;
  public st = true;
  public OrderUpdatesStatus = true;
  public isReset = false;
  public directionsService;
  public directionsDisplay;
  public show_paymentExtra: boolean = false;
  public onColor = "blue";
  public offColor = "red";
  public onText = "Automatic";
  public offText = "Manual";
  public OrderUpdateOnText = "Trip Updates On";
  public OrderUpdateOffText = "Trip Updates Off";
  public san = "Default";
  public sano = "Off";
  public Textsize = "small";
  public TextsizeOrder = "smaller";
  public OrderUpdateStatusText;
  public OrderUpdateStatusOnText = "Live updates on";
  public OrderUpdateStatusOffText = "Live updates off";
  public bgcolorstatus;
  public bgcoloron = "#91d34f";
  public bgcoloroff = "red";
  public on = "On";
  public off = "Off";
  public orderTypeFlag = true;
  public disableSendbtn = false;
  public orderSearchSubmit = false;
  public cancelOrdermsg;
  public tariff_id = "";
  public sort_by_coloumn = "";
  public date_filter_order = "";
  public current_order_status = [];
  public sort_type = '';
  public order_limit: any = '';
  public driver_id = '';
  public driver_id2: any = '';
  public driver_id3 = '';
  public _id = '';
  public unique_order_id = [];
  public user_id = "";
  public guest_user_id = "";
  public guest_user_phone = '';
  public selectedMoment: any = "";
  public selectedMoment1: any = "";
  public tagStartDate: any = "";
  public tagEndDate: any = "";
  public searchSubmit = false;
  public imageurl;
  del = false;
  public showpaymenterror;
  public schedulerStaus = false;
  public min = new Date(moment(Date.now()).subtract(90, 'days').format('YYYY-MM-DD, hh:mm'));
  public max = new Date();
  public max2 = new Date(Date.now() - 86400000);
  public max3 = new Date();
  public vehicles;
  public place1;
  public place2;
  public vehicle_id2;
  public oldCords = [];
  public myInterval: any;
  public pNo = 1;
  public socketFlag = true;
  public generated_by = "";
  public orderUpdateFlag = false;
  public update_id;
  public showPopInfo = false;
  public pixel = 20;
  public changeIconFlag = false;
  public zoomChangedFlag = false;
  public refreshTime: any = 10000;
  public infoLoader = false;
  public driverIconsBig;
  public teslaIconsBig;
  public driverGroupMap: any = "";
  public pickupOrder;
  public dropOrder;
  public zones;
  public statusCloud: any = [];
  public promo_id = '';
  public orderComments: any;
  public poiAddress;
  public poiModel: any = {
    name: "",
    company: "",
    status: "",
    priority: "",
    is_favorite: "",
    xcoordinate: "",
    ycoordinate: ""
  };
  public partner;
  public vehicleTypeFilterOrder;
  public user_name = "";
  public filter_name = "";
  public timely_user_name = "";
  public user_phone = "";
  public timely_user_phone = "";
  public session_token;
  public socket_session_token;
  public twoGispickEntrance: any;
  public twoGisDropEntrance: any;
  public soursePickup;
  public sourseDrop;
  public tgisStatus = true;
  tGisPickupcomplete: FormControl = new FormControl();
  tGisDropcomplete: FormControl = new FormControl();
  tGispickEntrance: FormControl = new FormControl();
  tGisDropEntrance: FormControl = new FormControl();
  customerNameList: FormControl = new FormControl();
  customerPhoneList: FormControl = new FormControl();
  customerCountryCode: FormControl = new FormControl();
  vehicleList: FormControl = new FormControl();
  driverList: FormControl = new FormControl();
  additionalList: FormControl = new FormControl();
  driver_groups: FormControl = new FormControl();
  driver_group_mode: FormControl = new FormControl();
  paymentExtraList: FormControl = new FormControl();
  partnerList: FormControl = new FormControl();
  order_type: FormControl = new FormControl();
  tarrifList: FormControl = new FormControl();
  payment_type: FormControl = new FormControl();
  sourceLatLang: FormControl = new FormControl();
  destinationLatLang: FormControl = new FormControl();
  public collapseItem;
  customer_email: FormControl = new FormControl();
  work_phone: FormControl = new FormControl();
  company: FormControl = new FormControl();
  flight_no: FormControl = new FormControl();
  modelCtrl = new FormControl();
  dispatcherForOrder = new FormControl();
  public uaeTime = new Date().toDateString();
  formErrors = {
    customerPhoneList: "",
    customerNameList: "",
    pickup_location: "",
    drop_location: "",
    tarrifList: "",
    vehicleList: "",
    driverList: "",
    multiple: "",
    tariff_price: "",
    makaniPickup: "",
    makaniDrop: ""
  };
  public dispatcherList = []
  public dispatcher_id2 = "";
  public allVehicleCategs: any = [];

  validationMessages = {
    customerPhoneList: {
      required: "Customer Phone is required.",
      maxlength: "Customer Phone should not exceed 15 digits.",
      pattern: "Customer Phone must be a number"
    },
    customerNameList: {
      required: "Customer Name is required.",
      maxlength: "Customer Name should not exceed 30 characters."
    },
    pickup_location: {
      required: "Pickup location is required."
    },
    tarrifList: {
      required: "Tariff is required."
    },
    vehicleList: {
      required: "Vehicle is required."
    },
    driverList: {
      required: "Driver is required."
    },
    multiple: {
      pattern: "Only numbers are allowed"
    },
    tariff_price: {
      required: "Price is required",
      pattern: "Only numbers are allowed"
    },
    // tbStartTime: {
    //   required: "Time based start time is required"
    // },
    tbEndTime: {
      required: "Time based end time is required"
    }
  };
  public tGisPickPlace;
  public tGisDropPlace;
  public imageext;
  public apiUrl;

  public subscripttimeout;
  public subscripttimestart;
  public addDispatcher: FormGroup;
  public addMessage: FormGroup;
  public deviceLocationData = [];
  public notificationList = [];
  public vehicleStatus = {
    "/assets/img/map/VIP_car_blue_15h.png": "Booked",
    "/assets/img/map/VIP_car_red_15h.png": "Paused",
    "/assets/img/map/VIP_car_green_15h.png": "Free",
    "/assets/img/map/VIP_car_black_15h.png": "Logged",
    "/assets/img/map/booked_no-internet15.png": "Booked and Offline",
    "/assets/img/map/accepted15.png": "Accepted",//
    "/assets/img/map/almost_free15.png": "Almost Free",//
    "/assets/img/map/free_no-internet15.png": "Free and Offline",
    "/assets/img/map/paused_no-internet15.png": "Paused and Offline",
    "/assets/img/map/paused(uber_careem)15.png": "Paused (Uber/Careem)",
    "/assets/img/map/paused(break)15.png": "Paused (Break)",
    "/assets/img/map/signed_out15.png": "Logged Out",
    "/assets/img/map/tesla_blue15.png": "Booked",
    "/assets/img/map/tesla_red15.png": "Paused",
    "/assets/img/map/tesla_green15.png": "Free",
    "/assets/img/map/tesla_booked_nointernet15.png": "Booked and Offline",
    "/assets/img/map/tesla_accepted15.png": "Accepted",
    "/assets/img/map/tesla_almostfree15.png": "Almost Free",
    "/assets/img/map/tesla_free_nointernet15.png": "Free and Offline",
    "/assets/img/map/tesla_paused_no-internet15.png": "Paused and Offline",
    "/assets/img/map/tesla_pause(uber_careem)15.png": "Paused (Uber/Careem)",
    "/assets/img/map/tesla_pause(break)15.png": "Paused (Break)",
    "/assets/img/map/VIP_car_blue_20h.png": "Booked",
    "/assets/img/map/VIP_car_red_20h.png": "Paused",
    "/assets/img/map/VIP_car_green_20h.png": "Free",
    "/assets/img/map/VIP_car_black_20h.png": "Logged",
    "/assets/img/map/booked_no-internet20.png": "Booked and Offline",
    "/assets/img/map/accepted20.png": "Accepted",//
    "/assets/img/map/almost_free20.png": "Almost Free",//
    "/assets/img/map/free_no-internet20.png": "Free and Offline",
    "/assets/img/map/paused_no-internet20.png": "Paused and Offline",
    "/assets/img/map/paused(uber_careem)20.png": "Paused (Uber/Careem)",
    "/assets/img/map/paused(break)20.png": "Paused (Break)",
    "/assets/img/map/signed_out20.png": "Logged Out",
    "/assets/img/map/tesla_blue20.png": "Booked",
    "/assets/img/map/tesla_red20.png": "Paused",
    "/assets/img/map/tesla_green20.png": "Free",
    "/assets/img/map/tesla_booked_nointernet20.png": "Booked and Offline",
    "/assets/img/map/tesla_accepted20.png": "Accepted",
    "/assets/img/map/tesla_almostfree20.png": "Almost Free",
    "/assets/img/map/tesla_free_nointernet20.png": "Free and Offline",
    "/assets/img/map/tesla_paused_no-internet20.png": "Paused and Offline",
    "/assets/img/map/tesla_pause(uber_careem)20.png": "Paused (Uber/Careem)",
    "/assets/img/map/tesla_pause(break)20.png": "Paused (Break)"
  }
  orderInfoMarkers: any = [];
  comment_status: string = '';
  public csvInterval;
  company_id_list: any = [];
  company_id_list2: any = [];
  vehicle_id_list: any = [];
  useremail: any;
  usercompany: any;
  countryList: any = [{ "name": "United Arab Emirates", "code": "971" }, { "name": "Afghanistan", "code": "93" }, { "name": "Albania", "code": "355" }, { "name": "Algeria", "code": "213" }, { "name": "AmericanSamoa", "code": "1 684" }, { "name": "Andorra", "code": "376" }, { "name": "Angola", "code": "244" }, { "name": "Anguilla", "code": "1 264" }, { "name": "Antarctica", "code": "672" }, { "name": "Antigua and Barbuda", "code": "1268" }, { "name": "Argentina", "code": "54" }, { "name": "Armenia", "code": "374" }, { "name": "Aruba", "code": "297" }, { "name": "Australia", "code": "61" }, { "name": "Austria", "code": "43" }, { "name": "Azerbaijan", "code": "994" }, { "name": "Bahamas", "code": "1 242" }, { "name": "Bahrain", "code": "973" }, { "name": "Bangladesh", "code": "880" }, { "name": "Barbados", "code": "1 246" }, { "name": "Belarus", "code": "375" }, { "name": "Belgium", "code": "32" }, { "name": "Belize", "code": "501" }, { "name": "Benin", "code": "229" }, { "name": "Bermuda", "code": "1 441" }, { "name": "Bhutan", "code": "975" }, { "name": "Bolivia, Plurinational State of Bolivia", "code": "591" }, { "name": "Bosnia and Herzegovina", "code": "387" }, { "name": "Botswana", "code": "267" }, { "name": "Bouvet Island", "code": "55" }, { "name": "Brazil", "code": "55" }, { "name": "British Indian Ocean Territory", "code": "246" }, { "name": "Brunei Darussalam", "code": "673" }, { "name": "Bulgaria", "code": "359" }, { "name": "Burkina Faso", "code": "226" }, { "name": "Burundi", "code": "257" }, { "name": "Cambodia", "code": "855" }, { "name": "Cameroon", "code": "237" }, { "name": "Canada", "code": "1" }, { "name": "Cape Verde", "code": "238" }, { "name": "Cayman Islands", "code": "1345" }, { "name": "Central African Republic", "code": "236" }, { "name": "Chad", "code": "235" }, { "name": "Chile", "code": "56" }, { "name": "China", "code": "86" }, { "name": "Christmas Island", "code": "61" }, { "name": "Cocos (Keeling) Islands", "code": "61" }, { "name": "Colombia", "code": "57" }, { "name": "Comoros", "code": "269" }, { "name": "Congo", "code": "242" }, { "name": "Congo, The Democratic Republic of the", "code": "243" }, { "name": "Cook Islands", "code": "682" }, { "name": "Costa Rica", "code": "506" }, { "name": "Cote d'Ivoire", "code": "225" }, { "name": "Croatia", "code": "385" }, { "name": "Cuba", "code": "53" }, { "name": "Cyprus", "code": "357" }, { "name": "Czech Republic", "code": "420" }, { "name": "Denmark", "code": "45" }, { "name": "Djibouti", "code": "253" }, { "name": "Dominica", "code": "1 767" }, { "name": "Dominican Republic", "code": "1 849" }, { "name": "Ecuador", "code": "593" }, { "name": "Egypt", "code": "20" }, { "name": "El Salvador", "code": "503" }, { "name": "Equatorial Guinea", "code": "240" }, { "name": "Eritrea", "code": "291" }, { "name": "Estonia", "code": "372" }, { "name": "Ethiopia", "code": "251" }, { "name": "Falkland Islands (Malvinas)", "code": "500" }, { "name": "Faroe Islands", "code": "298" }, { "name": "Fiji", "code": "679" }, { "name": "Finland", "code": "358" }, { "name": "France", "code": "33" }, { "name": "French Guiana", "code": "594" }, { "name": "French Polynesia", "code": "689" }, { "name": "French Southern and Antarctic Lands", "code": "262" }, { "name": "Gabon", "code": "241" }, { "name": "Gambia", "code": "220" }, { "name": "Georgia", "code": "995" }, { "name": "Germany", "code": "49" }, { "name": "Ghana", "code": "233" }, { "name": "Gibraltar", "code": "350" }, { "name": "Greece", "code": "30" }, { "name": "Greenland", "code": "299" }, { "name": "Grenada", "code": "1 473" }, { "name": "Guadeloupe", "code": "590" }, { "name": "Guam", "code": "1 671" }, { "name": "Guatemala", "code": "502" }, { "name": "Guernsey", "code": "44" }, { "name": "Guinea", "code": "224" }, { "name": "Guinea-Bissau", "code": "245" }, { "name": "Guyana", "code": "592" }, { "name": "Haiti", "code": "509" }, { "name": "Heard Island and McDonald Islands", "code": "672" }, { "name": "Holy See (Vatican City State)", "code": "379" }, { "name": "Honduras", "code": "504" }, { "name": "Hong Kong", "code": "852" }, { "name": "Hungary", "code": "36" }, { "name": "Iceland", "code": "354" }, { "name": "India", "code": "91" }, { "name": "Indonesia", "code": "62" }, { "name": "Iran, Islamic Republic of", "code": "98" }, { "name": "Iraq", "code": "964" }, { "name": "Ireland", "code": "353" }, { "name": "Isle of Man", "code": "44" }, { "name": "Israel", "code": "972" }, { "name": "Italy", "code": "39" }, { "name": "Jamaica", "code": "1 876" }, { "name": "Japan", "code": "81" }, { "name": "Jersey", "code": "44" }, { "name": "Jordan", "code": "962" }, { "name": "Kazakhstan", "code": "7" }, { "name": "Kenya", "code": "254" }, { "name": "Kiribati", "code": "686" }, { "name": "Korea, Democratic People's Republic of", "code": "850" }, { "name": "Korea, Republic of", "code": "82" }, { "name": "Kuwait", "code": "965" }, { "name": "Kyrgyzstan", "code": "996" }, { "name": "Lao People's Democratic Republic", "code": "856" }, { "name": "Latvia", "code": "371" }, { "name": "Lebanon", "code": "961" }, { "name": "Lesotho", "code": "266" }, { "name": "Liberia", "code": "231" }, { "name": "Libyan Arab Jamahiriya", "code": "218" }, { "name": "Liechtenstein", "code": "423" }, { "name": "Lithuania", "code": "370" }, { "name": "Luxembourg", "code": "352" }, { "name": "Macao", "code": "853" }, { "name": "Macedonia, The Former Yugoslav Republic of", "code": "389" }, { "name": "Madagascar", "code": "261" }, { "name": "Malawi", "code": "265" }, { "name": "Malaysia", "code": "60" }, { "name": "Maldives", "code": "960" }, { "name": "Mali", "code": "223" }, { "name": "Malta", "code": "356" }, { "name": "Marshall Islands", "code": "692" }, { "name": "Martinique", "code": "596" }, { "name": "Mauritania", "code": "222" }, { "name": "Mauritius", "code": "230" }, { "name": "Mayotte", "code": "262" }, { "name": "Mexico", "code": "52" }, { "name": "Micronesia, Federated States of", "code": "691" }, { "name": "Moldova, Republic of", "code": "373" }, { "name": "Monaco", "code": "377" }, { "name": "Mongolia", "code": "976" }, { "name": "Montenegro", "code": "382" }, { "name": "Montserrat", "code": "1664" }, { "name": "Morocco", "code": "212" }, { "name": "Mozambique", "code": "258" }, { "name": "Myanmar", "code": "95" }, { "name": "Namibia", "code": "264" }, { "name": "Nauru", "code": "674" }, { "name": "Nepal", "code": "977" }, { "name": "Netherlands", "code": "31" }, { "name": "Netherlands Antilles", "code": "599" }, { "name": "New Caledonia", "code": "687" }, { "name": "New Zealand", "code": "64" }, { "name": "Nicaragua", "code": "505" }, { "name": "Niger", "code": "227" }, { "name": "Nigeria", "code": "234" }, { "name": "Niue", "code": "683" }, { "name": "Norfolk Island", "code": "672" }, { "name": "Northern Mariana Islands", "code": "1 670" }, { "name": "Norway", "code": "47" }, { "name": "Oman", "code": "968" }, { "name": "Pakistan", "code": "92" }, { "name": "Palau", "code": "680" }, { "name": "Palestinian Territory, Occupied", "code": "970" }, { "name": "Panama", "code": "507" }, { "name": "Papua New Guinea", "code": "675" }, { "name": "Paraguay", "code": "595" }, { "name": "Peru", "code": "51" }, { "name": "Philippines", "code": "63" }, { "name": "Pitcairn", "code": "870" }, { "name": "Poland", "code": "48" }, { "name": "Portugal", "code": "351" }, { "name": "Puerto Rico", "code": "1 939" }, { "name": "Qatar", "code": "974" }, { "name": "Réunion", "code": "262" }, { "name": "Romania", "code": "40" }, { "name": "Russia", "code": "7" }, { "name": "Rwanda", "code": "250" }, { "name": "Saint Helena, Ascension and Tristan Da Cunha", "code": "290" }, { "name": "Saint Kitts and Nevis", "code": "1 869" }, { "name": "Saint Lucia", "code": "1 758" }, { "name": "Saint Pierre and Miquelon", "code": "508" }, { "name": "Saint Vincent and the Grenadines", "code": "1 784" }, { "name": "Samoa", "code": "685" }, { "name": "San Marino", "code": "378" }, { "name": "Sao Tome and Principe", "code": "239" }, { "name": "Saudi Arabia", "code": "966" }, { "name": "Senegal", "code": "221" }, { "name": "Serbia", "code": "381" }, { "name": "Seychelles", "code": "248" }, { "name": "Sierra Leone", "code": "232" }, { "name": "Singapore", "code": "65" }, { "name": "Slovakia", "code": "421" }, { "name": "Slovenia", "code": "386" }, { "name": "Solomon Islands", "code": "677" }, { "name": "Somalia", "code": "252" }, { "name": "South Africa", "code": "27" }, { "name": "South Georgia and the South Sandwich Islands", "code": "500" }, { "name": "Spain", "code": "34" }, { "name": "Sri Lanka", "code": "94" }, { "name": "Sudan", "code": "249" }, { "name": "Suriname", "code": "597" }, { "name": "Svalbard and Jan Mayen", "code": "47" }, { "name": "Swaziland", "code": "268" }, { "name": "Sweden", "code": "46" }, { "name": "Switzerland", "code": "41" }, { "name": "Syrian Arab Republic", "code": "963" }, { "name": "Taiwan", "code": "886" }, { "name": "Tajikistan", "code": "992" }, { "name": "Tanzania, United Republic of", "code": "255" }, { "name": "Thailand", "code": "66" }, { "name": "Timor-Leste", "code": "670" }, { "name": "Togo", "code": "228" }, { "name": "Tokelau", "code": "690" }, { "name": "Tonga", "code": "676" }, { "name": "Trinidad and Tobago", "code": "1 868" }, { "name": "Tunisia", "code": "216" }, { "name": "Turkey", "code": "90" }, { "name": "Turkmenistan", "code": "993" }, { "name": "Turks and Caicos Islands", "code": "1 649" }, { "name": "Tuvalu", "code": "688" }, { "name": "Uganda", "code": "256" }, { "name": "Ukraine", "code": "380" },
  { "name": "United Kingdom", "code": "44" }, { "name": "United States", "code": "1" }, { "name": "United States Minor Outlying Islands", "code": "1581" }, { "name": "Uruguay", "code": "598" }, { "name": "Uzbekistan", "code": "998" }, { "name": "Vanuatu", "code": "678" }, { "name": "Venezuela, Bolivarian Republic of", "code": "58" }, { "name": "Viet Nam", "code": "84" }, { "name": "Virgin Islands, British", "code": "1 284" }, { "name": "Virgin Islands, U.S.", "code": "1 340" }, { "name": "Wallis and Futuna", "code": "681" }, { "name": "Western Sahara", "code": "732" }, { "name": "Yemen", "code": "967" }, { "name": "Zambia", "code": "260" }, { "name": "Zimbabwe", "code": "263" }];
  max4: any = new Date();
  editCustomer: boolean;
  timely_user_id: any;
  gisDroped: any;
  gisPicked: any;
  tbEndFlag: boolean;
  editCustomerACL: boolean;
  notificationAcl: boolean = false;
  timeBasedOrderACL: boolean;
  filteredCountryList: any = [];
  order_edited: any;
  flagged_orders: any;
  CUser;
  saveFilterFlag: boolean;
  is_scheduler: any;
  defaultTariff: any;
  socket_session_token_map: any;
  companyIdFilter: any;
  companyIdFilter2: any;
  mapSocketCountList: any = [];
  inTripCordinates: any;
  downloads: boolean;
  aaaCompany: boolean;
  companyType: any;
  vehicle_category_ids = [];
  mapVehicleCategory: any;
  customerAcl: boolean = false;
  adminAccessAcl: boolean = false;
  subscription: Subscription;
  constructor(
    public zone: NgZone,
    public formBuilder: FormBuilder,
    public _poiService: PoiService,
    private _orderService: OrderService,
    private _partnerService: PartnerService,
    private router: Router,
    public dialog: MatDialog,
    private _devicelocationService: DevicelocationService,
    private _jwtService: JwtService,
    private _customerService: CustomerService,
    private _vehicleService: VehicleService,
    private _driverService: DriverService,
    private _additionalService: AdditionalService,
    private _paymentService: PaymentService,
    private _usersService: UsersService,
    private _paymentTypeService: PaymentTypeService,
    private _tariffService: TariffService,
    private _zoneService: ZoneService,
    private _orderGroupService: OrderGroupService,
    private _coverageAreaService: CoverageareaService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef,
    private socket: Socket,
    private ref: ChangeDetectorRef,
    private userIdle: UserIdleService,
    private _shiftsService: ShiftsService,
    public overlay: Overlay,
    private _companyservice: CompanyService,
    private _vehicleModelsService: VehicleModelsService,
    public _aclService: AccessControlService,
    public _FreeVehicleService: FreeVehicleService,
    private _global: GlobalService,
    private _EncDecService: EncDecService,
    public twoGisService: TwoGisService
  ) {
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.CUser = window.localStorage['CUser'];

    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    if (this.usercompany === '5ce12918aca1bb08d73ca25d' || this.usercompany === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    if (this.usercompany === '5dcba9e0b0e06e5c5ac1aea1')
      this.aaaCompany = true;
    this.company_id_list.push(this.usercompany)
    this.company_id_list2.push(this.usercompany)
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.socket_session_token_map = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany)
    this.aclDisplayService();
    this.toastr.setRootViewContainerRef(vcr);
    this.vehiclesByType = {
      tesla: {
        count: 0
      },
      limo: {
        count: 0
      }
    };
    this.driverIcons = {
      booked: "/assets/img/map/VIP_car_blue_20h.png",
      offline: "/assets/img/map/VIP_car_red_20h.png",
      free: "/assets/img/map/VIP_car_green_20h.png",
      loggedIn: "/assets/img/map/VIP_car_black_20h.png",
      internet: "/assets/img/map/booked_no-internet20.png",//
      gps: "/assets/img/map/booked_no-internet20.png",//
      accepted: "/assets/img/map/accepted20.png",//
      almost_free: "/assets/img/map/almost_free20.png",//
      free_internet: "/assets/img/map/free_no-internet20.png",//
      free_gps: "/assets/img/map/free_no-internet20.png",
      paused_offline: "/assets/img/map/paused_no-internet20.png",
      uber_careem: "/assets/img/map/paused(uber_careem)20.png",
      break: "/assets/img/map/paused(break)20.png",
      loggedOut: "/assets/img/map/signed_out20.png"
    };
    this.teslaIcons = {
      booked: "/assets/img/map/tesla_blue20.png",
      offline: "/assets/img/map/tesla_red20.png",
      free: "/assets/img/map/tesla_green20.png",
      loggedIn: "/assets/img/map/VIP_car_black_20h.png",
      internet: "/assets/img/map/tesla_booked_nointernet20.png",//
      gps: "/assets/img/map/tesla_booked_nointernet20.png",//
      accepted: "/assets/img/map/tesla_accepted20.png",//
      almost_free: "/assets/img/map/tesla_almostfree20.png",//
      free_internet: "/assets/img/map/tesla_free_nointernet20.png",//
      free_gps: "/assets/img/map/tesla_free_nointernet20.png",
      paused_offline: "/assets/img/map/tesla_paused_no-internet20.png",
      loggedOut: "/assets/img/map/signed_out20.png",
      uber_careem: "/assets/img/map/tesla_pause(uber_careem)20.png",
      break: "/assets/img/map/tesla_pause(break)20.png",
    };
    this.driverIconsBig = {
      booked: "/assets/img/map/limo_black_car_alone_blue.png",
      offline: "/assets/img/map/limo_black_car_alone_red.png",
      free: "/assets/img/map/limo_black_car_alone_green.png",
      loggedIn: "/assets/img/map/car_black_top.png",
      internet: "/assets/img/map/booked_no-internet40.png",//
      gps: "/assets/img/map/booked_no-internet40.png",//
      accepted: "/assets/img/map/accepted40.png",//
      almost_free: "/assets/img/map/almost_free40.png",//
      free_internet: "/assets/img/map/free_no-internet40.png",//
      free_gps: "/assets/img/map/free_no-internet40.png",
      paused_offline: "/assets/img/map/paused_no-internet40.png",
      uber_careem: "/assets/img/map/paused(uber_careem)40.png",
      break: "/assets/img/map/paused(break)40.png",
      loggedOut: "/assets/img/map/signed_out40.png"
    };
    this.teslaIconsBig = {
      booked: "/assets/img/map/tesla_blue40.png",
      offline: "/assets/img/map/tesla_red40.png",
      free: "/assets/img/map/tesla_green40.png",
      loggedIn: "/assets/img/map/car_black_top.png",
      internet: "/assets/img/map/tesla_booked_nointernet40.png",//
      gps: "/assets/img/map/tesla_booked_nointernet40.png.png",//
      accepted: "/assets/img/map/tesla_accepted40.png",//
      almost_free: "/assets/img/map/tesla_almostfree40.png",//
      free_internet: "/assets/img/map/tesla_free_nointernet40.png",//
      free_gps: "/assets/img/map/tesla_free_nointernet40.png",
      paused_offline: "/assets/img/map/tesla_paused_no-internet40.png",
      loggedOut: "/assets/img/map/signed_out40.png",
      uber_careem: "/assets/img/map/tesla_pause(uber_careem)40.png",
      break: "/assets/img/map/tesla_pause(break)40.png",
    };
    this.mapFilterLabels = {
      0: "Driver Status - All",
      1: "Paused",
      3: "Logged",
      5: "Free",
      6: "Booked",
      7: "Vehicle Status",
      8: "Logged Out"
    };
    this.userIdle.startWatching();
    this.subscription = this._FreeVehicleService.getMessage().subscribe(message => {
      this.setVehicleFilter(message);
    });
    // Start watching when user idle is starting.
    this.subscripttimestart = this.userIdle
      .onTimerStart()
      .subscribe(count => console.log(count));

    // Start watch when time is up.
    this.subscripttimeout = this.userIdle.onTimeout().subscribe(() => {
      this._jwtService.destroyDispatcherToken();
      clearInterval(this.dataRefresher);
      this.userIdle.stopWatching();
      this.router.navigate(["/dispatcher/login"]);
    });
    this.selectedMoment = new Date(Date.now() - 86400000);
    this.selectedMoment1 = new Date(Date.now() + 86400000);
    this.tagStartDate = new Date(Date.now() - 86400000);
    this.tagEndDate = new Date(Date.now() + 86400000);
    this.vehicleFilter = "";
    this.sideFilter = "";
    this.driverFilter = "";

    this.addDispatcher = formBuilder.group({
      tGisDropcomplete: [''],
      tGisPickupcomplete: [''],
      tGispickEntrance: [''],
      tGisDropEntrance: [''],
      customerCountryCode: [this.countryList[0]],
      customerPhoneList: ['', [Validators.maxLength(15)]],
      customerNameList: ['', [Validators.maxLength(30)]],
      pickup_location: ['', [Validators.required]],
      distance: [''],
      eta: [''],
      drop_location: [''],
      pickup_country: ['', [Validators.required]],
      drop_country: [''],
      message: [''],
      tarrifList: ['', [Validators.required]],
      vehicleList: [''],
      driverList: [''],
      order_type: ['1', []],
      tariff_price: ['', [Validators.pattern('^[0-9]+$')]],
      price: [''],
      multiple: ['', [Validators.pattern('^[0-9]+$')]],
      total_sum: [''],
      payment_extra_price: [''],
      NumberOfCars: [''],
      distance_radius: [''],
      OrderGroup: [''],
      sourceLatLang: [''],
      destinationLatLang: [''],
      additionalList: [''],
      driver_groups: [''],
      driver_group_mode: [''],
      scheduler_start_date: [''],
      scheduler_start_time: [''],
      schedule_before: [''],
      is_scheduler: [''],
      partnerList: [''],
      trip_id: [''],
      paymentExtraList: [''],
      customer_email: [''],
      work_phone: [''],
      company: [''],
      flight_no: [''],
      payment_type: [''],
      credit_card: [''],
      //tbStartTime: [''],
      tbEndTime: ['']
    });
    this.addMessage = formBuilder.group({
      message: ["", [Validators.required]],
      driver_id: [""]
    });
    this.addDispatcher.controls["tariff_price"].disable();
    this.addDispatcher.controls["multiple"].setValue(1);
    this.addDispatcher
      .get("tGisPickupcomplete")
      .valueChanges.debounceTime(400).distinctUntilChanged()
      .switchMap((query) => {
        return this.twoGisService.twoGisAutoComplete(query)
      }).subscribe(dec => {
        if (dec.meta.code == 200) {
          this.tGisPickPlace = dec.result.items;
        }
      });
    this.addDispatcher
      .get("tGisDropcomplete")
      .valueChanges.debounceTime(400).distinctUntilChanged()
      .switchMap((query) => {
        return this.twoGisService.twoGisAutoComplete(query)
      }).subscribe(dec => {
        if (dec.meta.code == 200) {
          this.tGisDropPlace = dec.result.items;
        }
      });

    this.addDispatcher
      .get("customerPhoneList").valueChanges.debounceTime(400).distinctUntilChanged().switchMap(data => {
        if (typeof data !== "object") {
          this.showCustomerInfo = false;
        }
        let params = {
          company_id: this.company_id_array,
          search: typeof data !== 'object' ? data.trim().substr(0, 1) == '+' ? data.substr(1) : data.trim() : '',
          limit: 10
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._customerService.getCustomerByPhone(data1)
      })
      .subscribe(dataaa => {
        if (dataaa && dataaa.status == 200) {
          let resp_data: any = this._EncDecService.dwt(this.session_token, dataaa.data);
          this.customerPhone = resp_data.result;
        }
      });
    this.addDispatcher
      .get("customerCountryCode").valueChanges.debounceTime(400).subscribe(value => {
        if (typeof value == 'object')
          return;
        value = value ? value.trim().substr(0, 1) == '+' ? value.substr(1) : value.trim() : value;
        this.filteredCountryList = value ? this.countryList.filter(s => (s && new RegExp(`${value}`, 'gi').test(s.code)))
          : this.countryList;
      });

    this.addDispatcher
      .get("customerNameList")
      .valueChanges.debounceTime(400).distinctUntilChanged().switchMap(datacustomerNameList => {
        if (typeof datacustomerNameList !== "object") {
          this.showCustomerInfo = false;
        }
        let params = {
          offset: 0,
          limit: 10,
          sortOrder: 'asc',
          sortByColumn: 'firstname',
          search: datacustomerNameList,
          company_id: this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._customerService.getCustomerByName(data1)
      }).subscribe(data_customerService => {
        if (data_customerService && data_customerService.status == 200) {
          data_customerService = this._EncDecService.dwt(this.session_token, data_customerService.data);
          this.customerData = data_customerService.result;
        }
      });

    this.addDispatcher
      .get("vehicleList")
      .valueChanges.debounceTime(400).distinctUntilChanged()
      .switchMap((data_vehicleList) => {
        if (typeof data_vehicleList === "string") {
          this.selectVehicle = false;
          this.selectDriver = false;
          const params = {
            offset: 0,
            limit: 10,
            sortOrder: "desc",
            sortByColumn: "_id",
            search_keyword: data_vehicleList,
            vehicle_model_id: this.vehicleTypeFilter2
              ? this.vehicleTypeFilter2._id
              : "",
            company_id: this.company_id_array
          };
          let enc_data = this._EncDecService.nwt(this.session_token, params);
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          return this._vehicleService.getOccupiedVehicle(data1);
        }
        else
          return [];
      }).subscribe(dataa => {
        if (dataa && dataa.status == 200) {
          dataa = this._EncDecService.dwt(this.session_token, dataa.data);
          this.vehicleData = dataa.searchVehicle;
        }
      });
    this.addDispatcher.get('additionalList').valueChanges.debounceTime(400).distinctUntilChanged()
      .switchMap((data) => {
        if (typeof data === 'string') {
          this.selectAdditional = false;
          const params = {
            offset: 0,
            limit: 10,
            sortOrder: 'desc',
            sortByColumn: '_id',
            search: data,
            company_id: this.company_id_array
          };
          let enc_data = this._EncDecService.nwt(this.session_token, params);
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          return this._additionalService.getByName(data1);
        } else {
          this.selectAdditional = true;
          return [];
        }
      }).subscribe((data_additionalService) => {
        if (data_additionalService && data_additionalService.status == 200) {
          data_additionalService = this._EncDecService.dwt(this.session_token, data_additionalService.data);
          this.AdditionalData = data_additionalService.data;
        }
      });
    this.addDispatcher
      .get("driverList")
      .valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((text) => {
        if (typeof text == 'object')
          return [];
        let params12 = {
          offset: 0,
          limit: 10,
          sortOrder: "desc",
          sortByColumn: "_id",
          search: text,
          role: "dispatcher",
          vehicle_model_id: this.vehicleTypeFilter2 ? this.vehicleTypeFilter2._id : "",
          company_id: this.company_id_array
        };
        let enc_data12 = this._EncDecService.nwt(this.session_token, params12);
        let data12 = {
          data: enc_data12,
          email: this.useremail
        }
        return this._driverService.getLoginDriverByName(data12);
      })
      .subscribe(dataaaa => {
        if (dataaaa && dataaaa.status == 200) {
          dataaaa = this._EncDecService.dwt(this.session_token, dataaaa.data);
          this.driverData = dataaaa.drivers;
        }
      });

    this.addDispatcher.controls["paymentExtraList"].valueChanges
      .debounceTime(400)
      .subscribe(data => {
        this.showpaymenterror = false;
        if (typeof data === "object") {
          const found = this.isExist(data._id);
          if (found) {
            data.is_disable = true;
          } else {
            data.is_disable = false;
          }

          if (data.editable === 1) {
            this.addDispatcher.controls["payment_extra_price"].enable();
          } else {
            this.addDispatcher.controls["payment_extra_price"].disable();
          }

          let default_amount = data.default_amount;
          if (data.price_with_vat) {
            if (data.vat !== 0 && data.vat !== "" && data.vat !== null) {
              const vat_price =
                (parseFloat(data.vat) * parseFloat(default_amount)) / 100;
              default_amount = parseFloat(default_amount) + vat_price;
            }
          }
          this.addDispatcher.controls["payment_extra_price"].setValue(
            default_amount
          );
        } else {
          const params = {
            offset: 0,
            limit: 10,
            sortOrder: 'desc',
            sortByColumn: 'name',
            search: data,
            company_id: this.company_id_array
          };
          let enc_data = this._EncDecService.nwt(this.session_token, params);
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._paymentService
            .getPaymentListingByname(data1)
            .subscribe(data_paymentService => {
              if (data_paymentService && data_paymentService.status == 200) {
                data_paymentService = this._EncDecService.dwt(this.session_token, data_paymentService.data);
                this.paymetExtraData = data_paymentService.getPayment;
                this.paymetExtraData.forEach(datapaymetExtraData => {
                  if (datapaymetExtraData.multi_applicable === 0) {
                    const found = this.isExist(datapaymetExtraData._id);
                    if (found) {
                      datapaymetExtraData.is_disable = true;
                    } else {
                      datapaymetExtraData.is_disable = false;
                    }
                  }
                });
              }
            });
        }
      });

    this.addDispatcher.get("tarrifList").valueChanges.subscribe(data => {
      if (this.copyOrderFlag)
        data = this.addDispatcher.get("tarrifList").value;
      if (data != null) {
        if (!this.copyOrderFlag) {
          if (this.modelFlag) this.modelChips = data.vehicle_model_id.slice();
          this.modelList2 = data.vehicle_model_id.slice();
        }
        this.addDispatcher.controls["tbEndTime"].disable();
        //this.addDispatcher.controls["tbStartTime"].disable();
        this.addDispatcher.controls["tbEndTime"].setValidators([]);
        //this.addDispatcher.controls["tbStartTime"].setValidators([]);
        if (data.name === "Fixed Rate") {
          this.tbEndFlag = false;
          this.addDispatcher.controls["tariff_price"].enable();
          this.addDispatcher.controls["tariff_price"].setValidators(
            Validators.required
          );
          this.addDispatcher.controls["tariff_price"].updateValueAndValidity();
          this.addDispatcher.controls["multiple"].setValue(1);
        }
        else if (data._id === "5d77650dff812501664b4d31") {
          this.tbEndFlag = true;
          this.addDispatcher.controls["tariff_price"].setValidators([]);
          this.addDispatcher.controls["tariff_price"].disable();
          this.addDispatcher.controls["tariff_price"].setValue("");
          this.addDispatcher.controls["price"].setValue(0);

        } else {
          this.tbEndFlag = false;
          this.addDispatcher.controls["tariff_price"].setValidators([]);
          this.addDispatcher.controls["tariff_price"].disable();
          this.addDispatcher.controls["tariff_price"].setValue("");
          this.addDispatcher.controls["price"].setValue(0);
          this.total_sum();
        }
      }
    });

    this.addDispatcher.controls["tariff_price"].valueChanges.subscribe(data => {
      this.addDispatcher.controls["price"].setValue(data);
      this.total_sum();
    });

    this.addDispatcher.controls["multiple"].valueChanges.subscribe(data => {
      this.total_sum();
    });
    this.statusCtrl.valueChanges.subscribe(data => {
      this.filteredStatus = data ? this.allStatus.filter(s => (s && new RegExp(`${data}`, 'gi').test(s.name)))
        : this.allStatus;
    });
    this.queryFieldDriver.valueChanges.subscribe(data => {
      this.loadingIndicator = "dispSearchDriver";
    });
    this.queryFieldDriver.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          offset: 0,
          limit: 10,
          search_keyword: query,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._driverService.searchForDriver(data1)
      }).subscribe(result => {
        if (result && result.status === 200) {
          result = this._EncDecService.dwt(this.session_token, result.data);
          this.alldriverData = result.driver;
          this.loadingIndicator = "";
        }
      })

    this.queryFieldVehicle_side_no.valueChanges.subscribe(data => {
      this.sideNoMatLoading = true;
    });
    this.queryFieldVehicle_side_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let param = {
          offset: 0,
          limit: 10,
          sortOrder: "desc",
          sortByColumn: "_id",
          searchsidenumber: query,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, param);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._vehicleService.getVehicleListingAdmin(data1);
      }).subscribe(result => {
        if (result && result.status === 200) {
          result = this._EncDecService.dwt(this.session_token, result.data);
          this.selectVehicleData = result.result;
          this.sideNoMatLoading = false;
        }
      })

    this.queryFieldVehicle_plate_no.valueChanges.subscribe(data => {
      this.vehicleNoMatLoading = true;
    });
    this.queryFieldVehicle_plate_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          company_id: this.company_id_array,
          offset: 0,
          limit: 10,
          sortOrder: "desc",
          sortByColumn: "_id",
          searchsidenumber: query
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._vehicleService.getVehicleListingAdmin(data1);
      }).subscribe(result => {
        if (result && result.status === 200) {
          result = this._EncDecService.dwt(this.session_token, result.data);
          this.selectVehicleData = result.result;
          this.vehicleNoMatLoading = false;
        }
      });
    this.driverMapFilter.valueChanges.subscribe(data => {
      this.loadingIndicator = "mapDriverFilter";
    });
    this.driverMapFilter.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          limit: 10,
          search_keyword: query,
          company_id: this.company_id_list2
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._driverService.searchForDriver(data1)
      }).subscribe(result => {
        if (result && result.status === 200) {
          result = this._EncDecService.dwt(this.session_token, result.data);
          this.selectDriverData2 = result.driver;
          this.loadingIndicator = "";
        }
      });

    this.vehicleMapFilter.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'vehicle_unique_id',
          'search_keyword': query,
          company_id: this.company_id_list2
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._vehicleService.searchVehicle(data1)
      }).subscribe(result => {
        if (result && result.status === 200) {
          result = this._EncDecService.dwt(this.session_token, result.data);
          this.selectVehicleData2 = result.searchVehicle;
          this.mapVehicleMatLoading = false;
          this.mapSideMatLoading = false;
          this.loadingIndicator = ''
        }
      });
    this.vehicleSideMapFilter.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'vehicle_unique_id',
          'search_keyword': query,
          company_id: this.company_id_list2
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._vehicleService.searchVehicle(data1)
      }).subscribe(result => {
        if (result && result.status === 200) {
          result = this._EncDecService.dwt(this.session_token, result.data);
          this.selectVehicleData2 = result.searchVehicle;
          this.loadingIndicator = '';
        }
      })



    this.orderCtrl.valueChanges.subscribe(result => {
      this.loadingIndicator = 'orderIdselected';
    })
    this.orderCtrl.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          search_keyword: query,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._orderService.getAllOrderId(data1)
      }).subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.allOrderIddata = result.getOrders;
          this.loadingIndicator = ''
        };
        this.loadingIndicator = '';
      })

    this.statusCloud = [{ name: "search_for_driver", value: "search_for_driver", display: "Search For Driver" }, { name: "search_for_driver_manual", value: "search_for_driver_manual", display: "Search For Driver Manual" }, { name: "scheduled_trip", value: "scheduled_trip", display: "Scheduled" }, { name: "trip_started", value: "trip_started", display: "In Progress" }]
    this.addDispatcher.controls["scheduler_start_date"].valueChanges.subscribe(data => {
      this.clearTBEnd()
      if (data !== null && data !== "") {
        if (this.copyOrderFlag)
          return;
        if (data.getTime() == new Date(moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).format()).getTime()) {
          this.max3 = new Date(moment(Date.now()).format('YYYY-MM-DD, hh:mm:ss a'));
          this.max4 = new Date(moment(Date.now()).format('YYYY-MM-DD, hh:mm:ss a'));
        }
        else {
          this.max3 = new Date(Date.now() - 86400000);
          this.max4 = data;
        }
      }
    });
    this.addDispatcher.controls["scheduler_start_time"].valueChanges.subscribe(data => {
      this.clearTBEnd();
      if (data !== null && data !== "") {
        if (this.addDispatcher.value.scheduler_start_date) {
          let a = new Date(moment(Date.now()).set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).format()).getTime()
          a = data.getTime() - a;
          this.max4 = new Date(new Date(moment(this.addDispatcher.value.scheduler_start_date).set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).format()).getTime() + a);
        }
      }
    })
    this.dispatcherForOrder.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'updated_at',
          'search_keyword': query
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._aclService.getAclUsers(data1)
      }).subscribe(result => {
        if (result && result.status === 200) {
          result = this._EncDecService.dwt(this.session_token, result.data);
          this.dispatcherList = result.users;
          let index = this.dispatcherList.findIndex(x => x._id == "5b9a0f0336aad01ed0e7d49e");
          if (index > -1) {
            this.dispatcherList.splice(index, 1);
          }
        }
      });
    this.addDispatcher
      .get("partnerList")
      .valueChanges.subscribe((data) => {
        if (typeof data == 'object')
          return [];
        this.partnerData = data ? this.tempPartner.filter(s => (s && new RegExp(`${data}`, 'gi').test(s.name)))
          : this.tempPartner;
      });
    this.selectedMoment3 = new Date();
    this.selectedMoment4 = new Date(Date.now() - 3600000);
  }
  public tempPartner;
  public aclDisplayService() {
    let params1 = {
      company_id: this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params1);
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._aclService.getAclUserMenu(data1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Dispatcher-Booking") {
            this.dispatcherBooking = true;
          } else if (data.menu[i] == "Dispatcher-Cancel Order") {
            this.dispatcherCancelOrder = true;
          } else if (data.menu[i] == "Dispatcher-Copy Order") {
            this.dispatcherCopyOrder = true;
          } else if (data.menu[i] == "Dispatcher-Send Order Message") {
            this.DispatcherSendMessage = true;
          } else if (data.menu[i] == "Dispatcher-Order Info") {
            this.DispatcherOrderInfo = true;
          } else if (data.menu[i] == "Dispatcher-Edit Customer") {
            this.editCustomerACL = true;
          } else if (data.menu[i] == "Dispatcher-Timebased Order") {
            this.timeBasedOrderACL = true;
          } else if (data.menu[i] == 'Downloads-Order Management') {
            this.downloads = true;
          } else if (data.menu[i] == 'Dispatcher-Notification') {
            this.notificationAcl = true;
          } else if (data.menu[i] == 'Customers - List') {
            this.customerAcl = true;
          } else if (data.menu[i] == 'Admin Login') {
            this.adminAccessAcl = true;
          }
        };
      }
    })
  }

  tgisSelector = new FormControl();
  queryFieldDriver: FormControl = new FormControl();
  queryFieldVehicle_side_no: FormControl = new FormControl();
  queryFieldVehicle_plate_no: FormControl = new FormControl();
  driverMapFilter: FormControl = new FormControl();
  vehicleMapFilter: FormControl = new FormControl();
  vehicleSideMapFilter: FormControl = new FormControl();
  orderCtrl: FormControl = new FormControl();
  usermatSelect: FormControl = new FormControl();
  usermatSelectPhone: FormControl = new FormControl();
  guestmatSelectPhone: FormControl = new FormControl();
  timelyUsermatSelectPhone: FormControl = new FormControl();
  vehicleCategCtrl: FormControl = new FormControl();
  userRating = new FormControl();
  customerRating = new FormControl();
  userList: any[] = [{ 'text': '1 Star', 'rating': '1' }, { 'text': '2 Star', 'rating': '2' }, { 'text': '3 Star', 'rating': '3' }, { 'text': '4 Star', 'rating': '4' }, { 'text': '5 Star', 'rating': '5' }];
  custList: any[] = [{ 'text': '1 Star', 'rating': '1.0' }, { 'text': '2 Star', 'rating': '2.0' }, { 'text': '3 Star', 'rating': '3.0' }, { 'text': '4 Star', 'rating': '4.0' }, { 'text': '5 Star', 'rating': '5.0' }];
  userRate;
  CustomerRate;
  searchReason: any = [];
  cancelReasons: string[] = [];
  cancelReasonId: any = [];
  vehicleCategory;
  vehicleCateg: any = [];
  vehicleCategItems: string[] = [];
  vehicleCategId: any = [];
  vehicleChpModel: string[] = [];
  vehicleChpModelId: any = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  statusCtrl = new FormControl();
  searchReasonCtrl2 = new FormControl();
  searchVehclCtrl = new FormControl();
  vehicleModels: any = []
  searchVehclModel: any = [];
  statuses: string[] = [];
  allStatus = [
    { name: "scheduled_trip", value: "Scheduled" },
    { name: "search_for_driver", value: "Search For Driver" },
    { name: "search_for_driver_manual", value: "Search For Driver Manual" },
    { name: "rejected_by_driver", value: "Rejected" },
    { name: "accepted_by_driver", value: "Accepted" },
    { name: "waiting_for_customer", value: "Waiting for customer" },
    { name: "trip_started", value: "In Progress" },
    { name: "confirming_rate", value: "Confirming rate" },
    { name: "selecting_payment", value: "Selecting payment" },
    { name: "rating", value: "Rating" },
    { name: "completed", value: "Completed" },
    { name: "canceled", value: "Canceled" },
    { name: "order_timed_out", value: "Order timeout" },
    { name: "customer_cancel", value: "Customer cancel" },
    { name: "driver_cancel", value: "Driver cancel" },
    { name: "dispatcher_cancel", value: "Dispatcher cancel" }
  ];
  filteredStatus = this.allStatus;
  remove(status: string, type): void {
    if (type == '0') {
      const index = this.statuses.indexOf(status);
      if (index >= 0) {
        this.statuses.splice(index, 1);
        this.current_order_status.splice(index, 1);
      }
      this.autoSearch()
    } else if (type == '1') {
      const index = this.cancelReasons.indexOf(status);
      if (index >= 0) {
        this.cancelReasons.splice(index, 1);
        this.cancelReasonId.splice(index, 1);
      }
    } else if (type == '2') {
      const index = this.vehicleCategItems.indexOf(status);
      if (index >= 0) {
        this.vehicleCategItems.splice(index, 1);
        this.vehicleCategId.splice(index, 1);
      }
    } else if (type == '3') {
      const index = this.vehicleChpModel.indexOf(status);
      if (index >= 0) {
        this.vehicleChpModel.splice(index, 1);
        this.vehicleChpModelId.splice(index, 1);
      }
    }
  }
  selected(event: MatAutocompleteSelectedEvent, type): void {
    if (type == '0') {
      if (this.statuses.indexOf(event.option.value.value) > -1) {
        return;
      } else {
        this.statuses.push(event.option.value.value);
        this.current_order_status.push(event.option.value.name);
        this.statusCtrl.setValue(null);
      }
    } else if (type == '1') {
      if (this.statuses.indexOf(event.option.viewValue) > -1) {
        return;
      } else {
        this.cancelReasons.push(event.option.value.reason);
        this.cancelReasonId.push(event.option.value._id);
        this.searchReasonCtrl2.setValue(null);
      }
    } else if (type == '2') {
      if (this.statuses.indexOf(event.option.viewValue) > -1) {
        return;
      } else {
        this.vehicleCategItems.push(event.option.value.reason);
        this.vehicleCategId.push(event.option.value._id);
        this.vehicleCategCtrl.setValue(null);
      }
    } else if (type == '3') {
      if (this.statuses.indexOf(event.option.viewValue) > -1) {
        return;
      } else {
        this.vehicleChpModel.push(event.option.value.name);
        this.vehicleChpModelId.push(event.option.value._id);
        this.searchVehclCtrl.setValue(null);
      }
    }

  }
  onChanges(): void {
    this.customerRating.valueChanges.subscribe(val => {
      this.CustomerRate = val;
    });
    this.userRating.valueChanges.subscribe(val => {
      this.userRate = val;
    });
  }

  searchVehicle(event) {
    var params = {
      offset: '0',
      limit: '5',
      search: event,
      company_id: this.company_id_array
    }
    let enc_data0 = this._EncDecService.nwt(this.session_token, params);
    let data0 = {
      data: enc_data0,
      email: this.useremail
    }
    // this._orderService.getCancelStatus(data0).then((data) => {
    //   if (data && data.status == 200) {
    //     data = this._EncDecService.dwt(this.session_token, data.data);
    //     this.vehicleCateg = data.data;
    //   }
    // })
  }
  /**
   * OnInt Function
   */
  orderComment = [];
  orderCmnt() {
    this.orderComment = [];
    if (this.orderComments == '') {
      this.orderComment = [];
    } else {
      this.orderComment.push(this.orderComments);
    }
  }
  ordersFilter = [];
  allOrderIddata = [];
  getOrderId() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: "desc",
      sortByColumn: "_id",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getAllOrderId(data1).then(dec => {
      if (dec && dec.status == 200) {
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.allOrderIddata = data.getOrders;
      }
    });
    //this.allOrderIddata=[100041116,100041169,100041168,250493,112334];
  }
  /*add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
  
    // Add our fruit
    if ((value || '').trim()) {
      this.ordersFilter.push({id: value.trim()});
      this.unique_order_id.push(value.trim());
    }
  
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }*/
  orderIdselected(event: MatAutocompleteSelectedEvent): void {
    if (this.ordersFilter.indexOf(event.option.value.unique_order_id) > -1) {
      return;
    } else {
      this.ordersFilter.push(event.option.value.unique_order_id);
      this.unique_order_id.push(event.option.value.unique_order_id);
      this.orderCtrl.setValue(null);
      this.loadingIndicator = '';
      this.autoSearch();
    }
  }
  removeOrder(fruit): void {
    const index = this.ordersFilter.indexOf(fruit);
    if (index >= 0) {
      this.ordersFilter.splice(index, 1);
      this.unique_order_id.splice(index, 1);
    }
    this.loadingIndicator = '';
    if (this.countTag > 0)
      return;
    this.autoSearch();
  }
  /* order id multi tag end */
  @ViewChild(DirectionsRenderer)
  directionsRendererDirective: DirectionsRenderer;
  directionsRenderer: google.maps.DirectionsRenderer;
  directionsResult: google.maps.DirectionsResult;
  public direction: any = {
    origin: "",
    destination: "",
    travelMode: "DRIVING"
  };
  private message = {
    author: "Tutorial Edge",
    message: "this is a test message",
    case: "map"
  };

  ngOnInit() {
    this.twoGisService.twoGisAutoComplete('dubai taxi').subscribe((dec) => {
      if (dec.result)
        this.tGisPickPlace = dec.result.items;
    })
    this.companyType = JSON.parse(window.localStorage['companydata']).company_type ? JSON.parse(window.localStorage['companydata']).company_type : '3';
    this.imageext = window.localStorage['ImageExt'];
    this.apiUrl = environment.apiUrl;
    document.addEventListener("click", this.restart.bind(this));
    document.addEventListener("mousemove", this.restart.bind(this));
    document.addEventListener("mousedown", this.restart.bind(this));
    document.addEventListener("keypress", this.restart.bind(this));
    document.addEventListener("DOMMouseScroll", this.restart.bind(this));
    document.addEventListener("mousewheel", this.restart.bind(this));
    document.addEventListener("touchmove", this.restart.bind(this));
    document.addEventListener("MSPointerMove", this.restart.bind(this));
    if (window.localStorage["dispatcherUser"])
      this.dispatcher_id = JSON.parse(window.localStorage["dispatcherUser"]);
    this._FreeVehicleService.observableNotificationList.subscribe(item => {
      this.notificationList = item;
    });
    this.dispatcher = this.dispatcher_id._id;
    this.imageurl = environment.imgUrl;
    this.getPhoneNumber();
    this.getCustomer();
    this.getPartner();
    this.getZones();
    this.getDriverGroups()
    this.getAllDriver();
    this.getAdditionalService();
    this.getpaymentExtra();
    this.getTariff();
    this.getDriverGroupsForListing();
    this.getOrderGroup();
    this.getPaymentType();
    //this.getUser();
    this.getAllCoverageArea();
    this.getAllorderCount();
    this.getCompanies();
    this.refreshOrderCountData();
    this.getOrderId();
    this.getDispatcherUsers();
    // call validators
    this.addDispatcher.valueChanges
      .debounceTime(500)
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
    this.onChanges();
    this.getFilters('');
    this.center = "The Dubai Mall";
    // var params2 = {
    //   offset: '0',
    //   limit: '5',
    //   search: event,
    //   company_id: this.company_id_array
    // }
    // let enc_data0 = this._EncDecService.nwt(this.session_token, params2);
    // let data0 = {
    //   data: enc_data0,
    //   email: this.useremail
    // }
    // this._orderService.getCancelStatus(data0).then((data) => {
    //   if (data && data.status == 200) {
    //     data = this._EncDecService.dwt(this.session_token, data.data);
    //     this.searchReason = data.data;
    //     this.vehicleCateg = data.data;
    //   }
    // });
    let params = {
      offset: 0,
      company_id: this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._vehicleModelsService.getVehicleModels(data1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.modelList = data.vehicleModelsList ? data.vehicleModelsList.slice() : [];
        this.modelList2 = data.vehicleModelsList ? data.vehicleModelsList.slice() : [];
        this.modelList4 = data.vehicleModelsList ? data.vehicleModelsList.slice() : [];
      }
    });
    this.loadingIndicator = '';
    let that = this;
    setInterval(function () {
      that.uaeTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Dubai" });
      if (that._FreeVehicleService.orderId !== '') {
        that.ordersFilter.push(that._FreeVehicleService.orderId);
        that.unique_order_id.push(that._FreeVehicleService.orderId);
        that._FreeVehicleService.orderId = '';
        that.orderCtrl.setValue(null);
        that.autoSearch();
      }
    }, 1000);
    // Number.prototype.after = function () {
    //   var value = parseInt(this.toString().split(".")[1], 10);//after
    //   return value ? value : 0;
    // }
  }
  public setVehicleFilterNew(event) {
    this.driverFilter = "";
    this.check_filter_interval = "no";
    this.check_interval = "no";
    //this.refreshMap();
    if (typeof event === "object") {
      this.clicked1(event);
      this.driverFilter = "";
      clearInterval(this.refreshTime);
      this.getRefreshTime('6');
      this.vehicle_id = event._id;
      this.positions = [];
      this.positions1 = [];
      if (this.vehicle_id) {
        let params = {
          company_id: this.company_id_list2,
          vehicle_id: this.vehicle_id
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._devicelocationService
          .getdatabyvehicleidNew(data1)
          .then(vehicleFreedata => {
            if (vehicleFreedata && vehicleFreedata.status == 200) {
              vehicleFreedata = this._EncDecService.dwt(this.session_token, vehicleFreedata.data);
              this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
              this.selectDriverData = vehicleFreedata.driversArr;
              this.selectVehicleData = vehicleFreedata.vehicleArr;
              this.deviceLocationData = vehicleFreedata.getdeviceloc;
              //this.vehicleFilter = this.selectVehicleData[0];
              //this.plateFilter = this.selectVehicleData[0];
              let id;
              if (
                this.SingledeviceLocationData.length > 0 &&
                typeof this.SingledeviceLocationData[0] != "undefined" &&
                this.SingledeviceLocationData[0].latitude != ""
              ) {
                for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                  const randomLat = this.SingledeviceLocationData[i].latitude;
                  const randomLng = this.SingledeviceLocationData[i].longitude;
                  this.doCenter({
                    lat: parseFloat(randomLat),
                    lng: parseFloat(randomLng)
                  });
                  if (this.SingledeviceLocationData[i]["device_id"]) {
                    id = this.SingledeviceLocationData[i]["device_id"];
                  } else {
                    id = "";
                  }
                  let vehicleStatus = this.getVehicleStatus(
                    this.selectVehicleData[i]
                  );
                  var driverIcon = this.driverIcons.loggedIn;
                  let vehicleModelIsValid =
                    this.SingledeviceLocationData[i].vehicle_model_id &&
                    (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                      this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                  let isTesla =
                    vehicleModelIsValid &&
                    (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                      this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                  var driverIcon = this.driverIcons.loggedIn;
                  if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                    driverIcon = this.teslaIcons['loggedOut'];
                  }
                  else if (vehicleStatus == 'offline') {
                    if (isTesla)
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                    else
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                        ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                  }
                  else {
                    if (this.SingledeviceLocationData[i].trip_status !== "") {
                      if (isTesla) {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.teslaIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.teslaIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                      }
                      else {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.driverIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.driverIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                          ? this.driverIcons['internet'] : driverIcon;
                      }
                    }
                    else {
                      if (isTesla) {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                      }
                      else {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                      }
                    }
                  }

                  if (randomLat != "" && randomLng != "" && id != "") {
                    this.positions.push([randomLat, randomLng, id]);
                    this.displayDriverOnMap(
                      randomLat,
                      randomLng,
                      id,
                      driverIcon,
                      0
                    );
                  }
                  this.removeStaleDriversFromMap();
                }
                this.locationmsg = "";
              } else {
                this.locationmsg = "No";
              }
            } else {
              this.locationmsg = "No";
            }
          });
      } else {
        this.selectDriverData = [];
        this.locationmsg = "No";
        this.setMapFilter("7");
      }
    }
  }
  public setVehicleFilter(event) {
    this.driverFilter = "";
    this.check_filter_interval = "no";
    this.check_interval = "no";
    //this.refreshMap();
    if (typeof event === "object") {
      this.clicked1(event);
      this.driverFilter = "";
      clearInterval(this.refreshTime);
      this.getRefreshTime('6');
      this.vehicle_id = event._id;
      this.positions = [];
      this.positions1 = [];
      if (this.vehicle_id) {
        let params = {
          company_id: this.company_id_list2,
          vehicle_id: this.vehicle_id
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._devicelocationService
          .getdatabyvehicleid(data1)
          .then(vehicleFreedata => {
            if (vehicleFreedata && vehicleFreedata.status == 200) {
              vehicleFreedata = this._EncDecService.dwt(this.session_token, vehicleFreedata.data);
              this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
              this.selectDriverData = vehicleFreedata.driversArr;
              this.selectVehicleData = vehicleFreedata.vehicleArr;
              this.deviceLocationData = vehicleFreedata.getdeviceloc;
              //this.vehicleFilter = this.selectVehicleData[0];
              //this.plateFilter = this.selectVehicleData[0];
              let id;
              if (
                this.SingledeviceLocationData.length > 0 &&
                typeof this.SingledeviceLocationData[0] != "undefined" &&
                this.SingledeviceLocationData[0].latitude != ""
              ) {
                for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                  const randomLat = this.SingledeviceLocationData[i].latitude;
                  const randomLng = this.SingledeviceLocationData[i].longitude;
                  this.doCenter({
                    lat: parseFloat(randomLat),
                    lng: parseFloat(randomLng)
                  });
                  if (this.SingledeviceLocationData[i]["device_id"]) {
                    id = this.SingledeviceLocationData[i]["device_id"];
                  } else {
                    id = "";
                  }
                  let vehicleStatus = this.getVehicleStatus(
                    this.selectVehicleData[i]
                  );
                  var driverIcon = this.driverIcons.loggedIn;
                  let vehicleModelIsValid =
                    this.SingledeviceLocationData[i].vehicle_model_id &&
                    (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                      this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                  let isTesla =
                    vehicleModelIsValid &&
                    (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                      this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                  var driverIcon = this.driverIcons.loggedIn;
                  if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                    driverIcon = this.teslaIcons['loggedOut'];
                  }
                  else if (vehicleStatus == 'offline') {
                    if (isTesla)
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                    else
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                        ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                  }
                  else {
                    if (this.SingledeviceLocationData[i].trip_status !== "") {
                      if (isTesla) {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.teslaIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.teslaIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                      }
                      else {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.driverIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.driverIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                          ? this.driverIcons['internet'] : driverIcon;
                      }
                    }
                    else {
                      if (isTesla) {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                      }
                      else {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                      }
                    }
                  }

                  if (randomLat != "" && randomLng != "" && id != "") {
                    this.positions.push([randomLat, randomLng, id]);
                    this.displayDriverOnMap(
                      randomLat,
                      randomLng,
                      id,
                      driverIcon,
                      0
                    );
                  }
                  this.removeStaleDriversFromMap();
                }
                this.locationmsg = "";
              } else {
                this.locationmsg = "No";
              }
            } else {
              this.locationmsg = "No";
            }
          });
      } else {
        this.selectDriverData = [];
        this.locationmsg = "No";
        this.setMapFilter("7");
      }
    }
  }

  public setVehicleFilter2(event) {
    if (typeof event === "object") {
      this.driver_id = "";
      this.vehicle_id2 = event._id;
      if (this.multiFilter.side.findIndex(x => x._id == event._id) == -1)
        this.multiFilter.side.push(event)
    }
  }
  ngOnDestroy() {
    let temp = this.notificationList.slice();
    this._FreeVehicleService.setNotification(this.notificationList.splice(-10, 10));
    this.notificationList = temp;
    //this.stop.bind(this);
    this.subscripttimestart.unsubscribe();
    this.subscripttimeout.unsubscribe();
    this.subscription.unsubscribe();
    let that = this;
    if (that.dataRefresher) {
      clearInterval(that.dataRefresher);
    }
    if (that.dataRefresher2) {
      clearInterval(that.dataRefresher2);
    }
    if (that.livetrackinginterval) {
      clearInterval(that.livetrackinginterval);
    }
    if (that.livetrackingOrder) {
      clearInterval(that.livetrackingOrder);
    }
    that.socket.removeListener("free_vehicle");
    clearInterval(that.refreshTime);
    //localStorage.setItem("notifications", JSON.stringify(this.notificationList));
  }
  public setDriverFilter(event) {
    this.vehicleFilter = "";
    this.sideFilter = "";
    this.check_filter_interval = "no";
    this.check_interval = "no";
    // this.selectVehicleData='';

    if (typeof event === "object") {
      if (event.vehicle_id != null) {
        let params = {
          company_id: this.company_id_list2,
          _id: event.vehicle_id
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._vehicleService.getById(data1).then((res) => {
          if (res && res.status == 200) {
            res = this._EncDecService.dwt(this.session_token, res.data);
            this.clicked1(res.SingleDetail);
          }
        })
        clearInterval(this.refreshTime);
        this.getRefreshTime('6');
        this.vehicle_id = event.vehicle_id;
        this.positions = [];
        this.positions1 = [];
        //this.liveTracking(this.vehicle_id);
        if (this.vehicle_id) {
          let params = {
            company_id: this.company_id_array,
            vehicle_id: this.vehicle_id
          }
          let enc_data = this._EncDecService.nwt(this.session_token, params);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._devicelocationService
            .getdatabyvehicleid(data1)
            .then(vehicleFreedata => {
              if (vehicleFreedata && vehicleFreedata.status == 200) {
                vehicleFreedata = this._EncDecService.dwt(this.session_token, vehicleFreedata.data);
                this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
                this.selectDriverData = vehicleFreedata.driversArr;
                this.selectVehicleData = vehicleFreedata.vehicleArr;
                this.deviceLocationData = vehicleFreedata.getdeviceloc;
                //this.vehicleFilter = this.selectVehicleData[0];
                //this.plateFilter = this.selectVehicleData[0];
                let id;
                if (
                  this.SingledeviceLocationData.length > 0 &&
                  typeof this.SingledeviceLocationData[0] != "undefined" &&
                  this.SingledeviceLocationData[0].latitude != ""
                ) {
                  for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                    const randomLat = this.SingledeviceLocationData[i].latitude;
                    const randomLng = this.SingledeviceLocationData[i].longitude;
                    this.doCenter({
                      lat: parseFloat(randomLat),
                      lng: parseFloat(randomLng)
                    });
                    if (this.SingledeviceLocationData[i]["device_id"]) {
                      id = this.SingledeviceLocationData[i]["device_id"];
                    } else {
                      id = "";
                    }
                    let vehicleStatus = this.getVehicleStatus(
                      this.selectVehicleData[i]
                    );
                    var driverIcon = this.driverIcons.loggedIn;
                    let vehicleModelIsValid =
                      this.SingledeviceLocationData[i].vehicle_model_id &&
                      (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                        this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                    let isTesla =
                      vehicleModelIsValid &&
                      (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                        this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                    var driverIcon = this.driverIcons.loggedIn;
                    if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                      driverIcon = this.teslaIcons['loggedOut'];
                    }
                    else if (vehicleStatus == 'offline') {
                      if (isTesla)
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                      else
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                          ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                    }
                    else {
                      if (this.SingledeviceLocationData[i].trip_status !== "") {
                        if (isTesla) {
                          switch (this.SingledeviceLocationData[i].trip_status) {
                            case 'accepted_by_driver':
                              driverIcon = this.teslaIcons['accepted'];
                              break;
                            case 'confirming_rate':
                              driverIcon = this.teslaIcons['almost_free'];
                              break;
                            case 'selecting_payment':
                              driverIcon = this.teslaIcons['almost_free'];
                              break;
                            case 'rating':
                              driverIcon = this.teslaIcons['almost_free'];
                              break;
                            default:
                              driverIcon = this.teslaIcons[vehicleStatus]
                              break;
                          }
                          driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                        }
                        else {
                          switch (this.SingledeviceLocationData[i].trip_status) {
                            case 'accepted_by_driver':
                              driverIcon = this.driverIcons['accepted'];
                              break;
                            case 'confirming_rate':
                              driverIcon = this.driverIcons['almost_free'];
                              break;
                            case 'selecting_payment':
                              driverIcon = this.driverIcons['almost_free'];
                              break;
                            case 'rating':
                              driverIcon = this.driverIcons['almost_free'];
                              break;
                            default:
                              driverIcon = this.driverIcons[vehicleStatus]
                              break;
                          }
                          driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                            ? this.driverIcons['internet'] : driverIcon;
                        }
                      }
                      else {
                        if (isTesla) {
                          driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                              ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                        }
                        else {
                          driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                              ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                        }
                      }
                    }
                    if (randomLat != "" && randomLng != "" && id != "") {
                      this.positions.push([randomLat, randomLng, id]);
                      this.displayDriverOnMap(
                        randomLat,
                        randomLng,
                        id,
                        driverIcon,
                        0
                      );
                    }
                    this.removeStaleDriversFromMap();
                  }
                  this.locationmsg = "";
                } else {
                  this.locationmsg = "No";
                }
              }
              else {
                this.locationmsg = "No";
              }
            });
        } else {
          this.selectDriverData = [];
          this.locationmsg = "No";
          this.setMapFilter("7");
        }
      }
      else {
        this.locationmsg = "No";
      }
    }
  }
  public hideMarkerInfo() {
    this.marker.display = !this.marker.display;
    this.check_interval = "yes";
  }
  public hidepoup() {
    if (this.marker.display) {
      this.marker.display = !this.marker.display;
      var iwOuter = $(".gm-style-iw");
      var iwBackground = iwOuter.prev();
      iwBackground.hide();
      var iwCloseBtn = iwOuter.next();
      iwCloseBtn.hide();
    }
  }
  public placeChanged(place) {
    this.center = place.geometry.location;
    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      this.address[addressType] = place.address_components[i].long_name;
    }
    this.ref.detectChanges();
  }

  public myTrack(event) {
    this.check_filter_interval = "no";
    this.check_interval = "no";
    this.refreshMap();
    if (typeof event === "object") {
      this.vehicleFilter = "";
      this.sideFilter = "";
      clearInterval(this.refreshTime)
      this.getRefreshTime('6');
      const vehicle_id = event.vehicle_id;
      this.positions = [];
      this.positions1 = [];
      //this.liveTracking(vehicle_id);
      this.vehicle_id = vehicle_id;
      if (vehicle_id != null) {
        let params = {
          company_id: this.company_id_array,
          vehicle_id: vehicle_id
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._devicelocationService
          .getdatabyvehicleid(data1)
          .then(vehicleFreedata => {
            if (vehicleFreedata && vehicleFreedata.status == 200) {
              vehicleFreedata = this._EncDecService.dwt(this.session_token, vehicleFreedata.data);
              this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
              this.selectDriverData = vehicleFreedata.driversArr;
              this.selectVehicleData = vehicleFreedata.vehicleArr;
              this.deviceLocationData = vehicleFreedata.getdeviceloc;
              //this.vehicleFilter = this.selectVehicleData[0];
              //this.plateFilter = this.selectVehicleData[0];
              let id;
              if (
                this.SingledeviceLocationData.length > 0 &&
                typeof this.SingledeviceLocationData[0] != "undefined" &&
                this.SingledeviceLocationData[0].latitude != ""
              ) {
                for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                  const randomLat = this.SingledeviceLocationData[i].latitude;
                  const randomLng = this.SingledeviceLocationData[i].longitude;
                  this.doCenter({
                    lat: parseFloat(randomLat),
                    lng: parseFloat(randomLng)
                  });
                  if (this.SingledeviceLocationData[i]["device_id"]) {
                    id = this.SingledeviceLocationData[i]["device_id"];
                  } else {
                    id = "";
                  }
                  let vehicleStatus = this.getVehicleStatus(
                    this.selectVehicleData[i]
                  );
                  var driverIcon = this.driverIcons.loggedIn;
                  let vehicleModelIsValid =
                    this.SingledeviceLocationData[i].vehicle_model_id &&
                    (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                      this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                  let isTesla =
                    vehicleModelIsValid &&
                    (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                      this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                  var driverIcon = this.driverIcons.loggedIn;
                  if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                    driverIcon = this.teslaIcons['loggedOut'];
                  }
                  else if (vehicleStatus == 'offline') {
                    if (isTesla)
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                    else
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                        ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                  }
                  else {
                    if (this.SingledeviceLocationData[i].trip_status !== "") {
                      if (isTesla) {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.teslaIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.teslaIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                      }
                      else {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.driverIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.driverIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                          ? this.driverIcons['internet'] : driverIcon;
                      }
                    }
                    else {
                      if (isTesla) {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                      }
                      else {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                      }
                    }
                  }
                  if (randomLat != "" && randomLng != "" && id != "") {
                    this.positions.push([randomLat, randomLng, id]);
                    this.displayDriverOnMap(
                      randomLat,
                      randomLng,
                      id,
                      driverIcon,
                      0
                    );
                  }
                  this.removeStaleDriversFromMap();
                }
                this.locationmsg = "";
              } else {
                this.locationmsg = "No";
              }
            }
            else {
              this.locationmsg = "No";
            }
          });
      } else {
        this.selectDriverData = [];
        this.locationmsg = "No";
        this.setMapFilter("7");
      }
    }
  }

  liveTracking(vehicleId) {
    if (this.mapFilter == "7" && vehicleId == null) {
      this.positions = [];
      this.positions1 = [];
      this.deviceLocationData = [];
      return;
    }
    this.singleVehicleTracking = vehicleId ? true : false;
    if (this.livetrackinginterval) {
      clearInterval(this.livetrackinginterval);
    }
    this.check_interval = "no";
    this.vehicle_id = vehicleId;
    this.positions1 = [];
    let trackloc = this.positions;
    if (vehicleId !== null) {
      let that = this;
      this.livetrackinginterval = setInterval(function () {
        let params = {
          company_id: this.company_id_array,
          vehicle_id: vehicleId
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        that._devicelocationService
          .getVehicleLastLocation(data1)
          .then(data => {
            if (data && data.status == 200) {
              data = this._EncDecService.dwt(this.session_token, data.data);
              if (trackloc) {
                that.positions1 = trackloc;
              }
              if (data.location) {
                that.positions = [];
                const randomLat = data.locations.latitude;
                const randomLng = data.locations.longitude;
                that.positions1.push([randomLat, randomLng]);
                that.positions.push([randomLat, randomLng]);
                trackloc = [];
              }
            }
          });
      }, 3000);
    }
  }
  public setMapFilter(param) {
    this.showPopInfo = false;
    this.setFilterLoading(
      "#dropdownMenuButton",
      false,
      this.mapFilterLabels[parseInt(param) || 0]
    );
    if (this.mapFilter != param) this.changeIconFlag = true;
    else this.changeIconFlag = false;
    this.getRefreshTime(param);
    if (param == "7") {
      $("#dropdownMenuButton").text("Vehicle Status");
      for (let i = 0; i < this.driverMarkers.length; i++) {
        this.driverMarkers[i].marker.setMap(null);
        if (i == this.driverMarkers.length - 1) this.driverMarkers = [];
      }
      this.positions = [];
      this.positions1 = [];
      this.deviceLocationData = [];
      return;
    }
    if (param !== '1')
      this.uberFlag = this.breakFlag = false;
    if (this.inPick) {
      for (let i = 0; i < this.picking.length; i++) {
        this.picking[i].setMap(null);
      }
      this.inPick = false;
    }
    if (this.inDrop) {
      for (let i = 0; i < this.dropOffing.length; i++) {
        this.dropOffing[i].setMap(null);
      }
      this.inDrop = false;
    }
    if (this.mapFilter != this.newparam) {
      this.mapFilter = this.newparam;
    }
    this.vehicle_id = "";
    this.positions = [];
    this.positions1 = [];
    this.locationmsg = "";
    this.driverFilter = "";
    this.vehicleFilter = "";
    this.sideFilter = "";
    //this.plateFilter = "";
    this.deviceLocationData = [];
    this.liveTracking(null);
    this.positions = [];
    this.positions1 = [];
    this.newparam = param;
    if (this.mapFilter != this.newparam) {
      this.mapFilter = this.newparam;
    }
    this.check_filter_interval = "yes";
    this.check_interval = "no";
    if (!param) {
      this.positions = [];
    }
    this.setFilterLoading("#dropdownMenuButton", true, null);
    if (<HTMLInputElement>document.getElementById("vehicleModel") !== null)
      (<HTMLInputElement>(
        document.getElementById("vehicleModel")
      )).disabled = true;
    let params = {
      company_id: this.company_id_list2,
      status: this.newparam || "10"
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._devicelocationService
      .getMapData(data1)
      .then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.deviceLocationData = data.getdeviceloc;
          // this.selectVehicleData = data.vehicleArr;
          // this.selectDriverData = data.driversArr;
          var driverIndicesToRemove = [];
          let haveLocationData =
            this.deviceLocationData.length > 0 &&
            typeof this.deviceLocationData[0] != "undefined" &&
            this.deviceLocationData[0].latitude != "";
          if (!haveLocationData) {
            this.positions = [];
          }
          var teslaCount = 0;
          var limoCount = 0;
          let result;
          for (let i = 0; i < this.deviceLocationData.length; i++) {
            if (this.tripStatus.length > 0)
              if (this.tripStatus.indexOf(this.deviceLocationData[i].trip_status) == -1) {
                driverIndicesToRemove.push(i);
                continue;
              }
            if (this.internetFlag) {
              if (this.iconColor2(this.deviceLocationData[i].updated_at) == 'green') {
                driverIndicesToRemove.push(i);
                continue;
              }
            }
            if (this.gpsFlag)
              if (this.deviceLocationData[i].gps_strength < 50) {
                driverIndicesToRemove.push(i);
                continue;
              }
            if (param == '1' && this.uberFlag)
              if (this.deviceLocationData[i].vehicle_ids["pause_reason"] !== 'uber/careem') {
                driverIndicesToRemove.push(i);
                continue;
              }
            if (param == '1' && this.breakFlag)
              if (this.deviceLocationData[i].vehicle_ids["pause_reason"] !== 'break') {
                driverIndicesToRemove.push(i);
                continue;
              }
            result = this.processDeviceLocationData(
              this.deviceLocationData[i],
              i
            );
            if (result.removeVehicle) {
              driverIndicesToRemove.push(i);
            }
          }
          this.vehiclesByType.tesla.count = teslaCount;
          this.vehiclesByType.limo.count = limoCount;
          this.deviceLocationData = $.grep(this.deviceLocationData, function (
            device,
            index
          ) {
            return !driverIndicesToRemove.includes(index);
          });
          this.removeStaleDriversFromMap();
          if (this.tripStatus.length > 0) {
            this.setFilterLoading(
              "#dropdownMenuButton",
              false,
              this.tripStatus[0] == 'accepted_by_driver' ? 'Accepted' +
                " - " +
                this.deviceLocationData.length : 'Almost free' +
                " - " +
                this.deviceLocationData.length
            );
          }
          else {
            let third_param = this.deviceLocationData.length;
            if (this.vehicle_category_ids.length == 0 && !this.vehicleTypeFilter && this.driverGroupMap == "" && !this.internetFlag && !this.gpsFlag && !this.breakFlag && !this.uberFlag) {
              if (parseInt(param) == 3) {
                third_param = this.loggedLength;
              }
              if (parseInt(param) == 5) {
                third_param = this.freeLength;
              }
              if (parseInt(param) == 8) {
                third_param = this.logged_out;
              }
            } else {
              console.log("filter case")
              if (parseInt(param) == 3) {
                console.log("logged");
                this.loggedLength = third_param;
              }
              if (parseInt(param) == 5) {
                this.freeLength = third_param;
              }
              if (parseInt(param) == 8) {
                this.logged_out = third_param;
              }
            }
            this.setFilterLoading(
              "#dropdownMenuButton",
              false,
              this.internetFlag ? 'Without internet' +
                " - " +
                this.deviceLocationData.length : this.gpsFlag ? 'Without GPS' +
                  " - " +
                  this.deviceLocationData.length : this.uberFlag ? 'Uber/Careem - ' + this.deviceLocationData.length :
                    this.breakFlag ? 'Break - ' + this.deviceLocationData.length
                      : this.mapFilterLabels[parseInt(param) || 0] +
                      " - " +
                      third_param
            );
          }
          if (<HTMLInputElement>document.getElementById("vehicleModel") !== null)
            (<HTMLInputElement>(
              document.getElementById("vehicleModel")
            )).disabled = false;
        }
      })
      .catch(error => {
        if (this.tripStatus.length > 0) {
          this.setFilterLoading(
            "#dropdownMenuButton",
            false,
            this.tripStatus[0] == 'accepted_by_driver' ? 'Accepted' +
              " - " +
              this.deviceLocationData.length : 'Almost free' +
              " - " +
              this.deviceLocationData.length
          );
        }
        else {
          let third_param = this.deviceLocationData.length;
          if (this.vehicle_category_ids.length == 0 && !this.vehicleTypeFilter && this.driverGroupMap == "") {
            if (parseInt(param) == 3) {
              third_param = this.loggedLength;
            }
            if (parseInt(param) == 5) {
              third_param = this.freeLength;
            }
            if (parseInt(param) == 8) {
              third_param = this.logged_out;
            }
          }
          else {
            if (parseInt(param) == 3) {
              this.loggedLength = third_param;
            }
            if (parseInt(param) == 5) {
              this.freeLength = third_param;
            }
            if (parseInt(param) == 8) {
              this.logged_out = third_param;
            }
          }
          this.setFilterLoading(
            "#dropdownMenuButton",
            false,
            this.internetFlag ? 'Without internet' +
              " - " +
              this.deviceLocationData.length : this.gpsFlag ? 'Without GPS' +
                " - " +
                this.deviceLocationData.length : this.uberFlag ? 'Uber/Careem - ' + this.deviceLocationData.length :
                  this.breakFlag ? 'Break - ' + this.deviceLocationData.length
                    : this.mapFilterLabels[parseInt(param) || 0] +
                    " - " +
                    third_param
          );
        }
        if (<HTMLInputElement>document.getElementById("vehicleModel") !== null)
          (<HTMLInputElement>(
            document.getElementById("vehicleModel")
          )).disabled = false;
        this.refreshMap();
      });
  }
  public processDeviceLocationData(locationData, i) {
    var id;
    const randomLat = locationData.latitude;
    const randomLng = locationData.longitude;
    if (locationData["device_id"]) {
      id = locationData["device_id"];
    } else {
      id = "";
    }
    if (randomLat != "" && randomLng != "" && id != "") {
      // let vehicle = $.grep(this.selectVehicleData, function (value) {
      //   if (value["shift"].length <= 0) {
      //     return false;
      //   }
      //   return value["shift"][0]["device_id"] === locationData["device_id"];
      // });
      // if (!vehicle[0]) {
      //   return {};
      // }
      let vehicleModelIsValid = locationData.vehicle_model_id && (typeof locationData.vehicle_model_id === "string" || locationData.vehicle_model_id instanceof String);
      let isTesla = vehicleModelIsValid && (locationData.vehicle_model_id === "5bb084342dd48f285e5b6c3d" || locationData.vehicle_model_id === "5bb093892dd48f285e5c3dba");
      var removeVehicle = false;
      if (this.vehicleTypeFilter) {
        if (
          this.vehicleTypeFilter._id != locationData.vehicle_model_id &&
          this.vehicleTypeFilter != "All"
        ) {
          removeVehicle = true;
        }
      }
      if (this.driverGroupMap != "") {
        if (this.driverGroupMap.indexOf(locationData.t3) == -1) {
          removeVehicle = true;
        }
      }
      if (this.vehicle_category_ids.length > 0) {
        if (locationData.vehicle_model && locationData.vehicle_model.vehicle_cat_ids.length > 0) {
          if (this.vehicle_category_ids.indexOf(locationData.vehicle_model.vehicle_cat_ids[0]) == -1)
            removeVehicle = true;
        } else
          removeVehicle = true;
      }
      if (removeVehicle) {
        return { removeVehicle: true, vehicleType: isTesla ? "Tesla" : "Limo" };
      } else {
        let vehicleStatus = this.getVehicleStatus(locationData.vehicle_ids);
        var driverIcon = this.driverIcons.loggedIn;
        if (this.newparam == "8") {
          driverIcon = this.teslaIcons['loggedOut'];
        }
        else if (vehicleStatus == 'offline') {
          if (isTesla)
            driverIcon = this.iconColor(locationData.updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
          else
            driverIcon = this.iconColor(locationData.updated_at) != 'green'
              ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
        }
        else if (this.newparam !== "3") {
          if (this.tripStatus.length > 0 || locationData.trip_status !== "" || this.mapFilter == '6') {
            if (isTesla) {
              switch (locationData.trip_status) {
                case 'accepted_by_driver':
                  driverIcon = this.teslaIcons['accepted'];
                  break;
                case 'confirming_rate':
                  driverIcon = this.teslaIcons['almost_free'];
                  break;
                case 'selecting_payment':
                  driverIcon = this.teslaIcons['almost_free'];
                  break;
                case 'rating':
                  driverIcon = this.teslaIcons['almost_free'];
                  break;
                default:
                  driverIcon = this.teslaIcons[vehicleStatus]
                  break;
              }
              driverIcon = this.iconColor(locationData.updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
            }
            else {
              switch (locationData.trip_status) {
                case 'accepted_by_driver':
                  driverIcon = this.driverIcons['accepted'];
                  break;
                case 'confirming_rate':
                  driverIcon = this.driverIcons['almost_free'];
                  break;
                case 'selecting_payment':
                  driverIcon = this.driverIcons['almost_free'];
                  break;
                case 'rating':
                  driverIcon = this.driverIcons['almost_free'];
                  break;
                default:
                  driverIcon = this.driverIcons[vehicleStatus]
                  break;
              }
              driverIcon = this.iconColor(locationData.updated_at) != 'green'
                ? this.driverIcons['internet'] : driverIcon;
            }
          }
          else {
            if (isTesla) {
              driverIcon = this.iconColor(locationData.updated_at) != 'green' && (this.mapFilter == '5' || this.mapFilter == '')
                ? this.teslaIcons['free_internet'] : (this.mapFilter == '5' || this.mapFilter == '')
                  ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
            }
            else {
              driverIcon = this.iconColor(locationData.updated_at) != 'green' && (this.mapFilter == '5' || this.mapFilter == '')
                ? this.driverIcons['free_internet'] : (this.mapFilter == '5' || this.mapFilter == '')
                  ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
            }
            // driverIcon = isTesla ? this.iconColor(locationData.updated_at) != 'green' && this.mapFilter == '5' || this.mapFilter == ''
            //   ? this.teslaIcons['free_internet'] : locationData.gps_strength > 50 && this.mapFilter == '5' || this.mapFilter == ''
            //     ? locationData.tripStatus == ''
            //       ? vehicleStatus != 'offline'
            //         ? this.teslaIcons['free_gps'] : this.teslaIcons[vehicleStatus] : this.teslaIcons['free_gps'] : this.teslaIcons[vehicleStatus] : this.iconColor(locationData.updated_at) != 'green' && this.mapFilter == '5'
            //     ? this.driverIcons['free_internet'] : locationData.gps_strength > 50
            //       ? locationData.tripStatus == '' ? vehicleStatus != 'offline' ? this.driverIcons['free_gps'] : this.driverIcons[vehicleStatus] : this.driverIcons['gps'] : this.driverIcons[vehicleStatus];
          }
        }
        this.displayDriverOnMap(randomLat, randomLng, id, driverIcon, i);
        return {
          removeVehicle: false,
          vehicleType: isTesla ? "Tesla" : "Limo"
        };
      }
    }
    return { removeVehicle: false, vehicleType: null };
  }
  public customerPhoneData = [];
  public guestPhoneData = [];
  public timelyCustomerPhoneData = [];
  getVehicleCategs() {
    const params = {
      offset: 0,
      limit: 100,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.company_id_array
    };
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._vehicleModelsService.getVehicleCategories(enc_data)
      .then(userdata => {
        if (userdata && userdata.status == 200) {
          userdata = this._EncDecService.dwt(this.session_token, userdata.data);
          this.allVehicleCategs = userdata.vehicleModelsList;
          this.mapVehicleCategory = userdata.vehicleModelsList.filter(el => el._id !== '5d8c86b7ce733810e243cc26');
        }
      });
  }
  ngAfterViewInit() {
    this.getVehicleCategs();
    this.side_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe(query => {
        let params = {
          company_id: this.company_id_array,
          offset: 0,
          limit: 10,
          sortOrder: "desc",
          sortByColumn: "_id",
          searchsidenumber: query
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._vehicleService.getVehicleListingAdmin(data1).subscribe(result => {
          this.loadingIndicator = '';
          if (result && result.status === 200) {
            result = this._EncDecService.dwt(this.session_token, result.data);
            this.selectVehicleData2 = result.result;
          }
        });
      })

    this.usermatSelect.valueChanges.debounceTime(400).distinctUntilChanged().switchMap(result => {
      this.loadingIndicator = 'userMatloading';
      let params = {
        offset: 0,
        limit: 10,
        sortOrder: 'asc',
        sortByColumn: 'firstname',
        search: result,
        company_id: this.company_id_array
      }
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      return this._customerService.getCustomerByName(data1)
    }).subscribe(userdata => {
      if (userdata && userdata.status == 200) {
        userdata = this._EncDecService.dwt(this.session_token, userdata.data);
        this.customerData = userdata.result;
      }
      this.orderSearchSubmit = false;
      this.loadingIndicator = '';
    });
    this.usermatSelectPhone.valueChanges.debounceTime(400).distinctUntilChanged().switchMap(data => {
      let params = {
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
        search: typeof data !== 'object' ? data.trim().substr(0, 1) == '+' ? data.substr(1) : data.trim() : '',
        limit: 10
      }
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this.loadingIndicator = 'userMatloadingPhone';
      return this._customerService
        .getCustomerByPhone(data1)
    }).subscribe(userdata => {
      if (userdata && userdata.status == 200) {
        userdata = this._EncDecService.dwt(this.session_token, userdata.data);
        this.customerPhoneData = userdata.result;
      }
      this.orderSearchSubmit = false;
      this.loadingIndicator = '';
    });
    this.guestmatSelectPhone.valueChanges.debounceTime(400).distinctUntilChanged().switchMap(data => {
      let params = {
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
        search: typeof data !== 'object' ? data.trim().substr(0, 1) == '+' ? data.substr(1) : data.trim() : '',
        limit: 10
      }
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this.loadingIndicator = 'userMatloadingPhone';
      return this._customerService.getGuestPhone(data1)
    }).subscribe(userdata => {
      if (userdata && userdata.status == 200) {
        userdata = this._EncDecService.dwt(this.session_token, userdata.data);
        this.guestPhoneData = userdata.result;
      }
      this.orderSearchSubmit = false;
      this.loadingIndicator = '';
    });
    this.timelyUsermatSelectPhone.valueChanges.debounceTime(400).distinctUntilChanged().switchMap(data => {
      let params = {
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
        search: typeof data !== 'object' ? data.trim().substr(0, 1) == '+' ? data.substr(1) : data.trim() : '',
        limit: 10
      }
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }

      console.log(this.generated_by);
      this.loadingIndicator = 'userMatloadingTimelyPhone';
      if (this.generated_by != 'delivery') {
        return this._customerService.getCustomerByPhone(data1)
      } else {
        return this._customerService.getGuestPhone(data1)
      }
    }).subscribe(userdata => {
      if (userdata && userdata.status == 200) {
        userdata = this._EncDecService.dwt(this.session_token, userdata.data);
        console.log(userdata);
        this.timelyCustomerPhoneData = userdata.result;
      }
      this.loadingIndicator = '';
    });
    this.vehicleMapFilter.valueChanges.subscribe(data => {
      this.loadingIndicator = "mapVehicleFilter";
    });

    this.vehicleSideMapFilter.valueChanges.subscribe(data => {
      this.loadingIndicator = "mapSideNoFilter";
    });
    this.searchReasonCtrl2.valueChanges.debounceTime(400)
      .distinctUntilChanged().switchMap(data => {
        let params = {
          offset: '0',
          limit: '5',
          search: data,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        return this._orderService.getCancelStatus(data1);
      })
      .subscribe(userdata => {
        if (userdata && userdata.status == 200) {
          userdata = this._EncDecService.dwt(this.session_token, userdata.data);
          this.searchReason = userdata.data;
        }
      });
    this.searchVehclCtrl.valueChanges.debounceTime(400)
      .distinctUntilChanged().switchMap(data => {
        const params = {
          offset: 0,
          limit: this.pageSize,
          sortOrder: 'desc',
          sortByColumn: 'updated_at',
          search: data,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        };
        var encrypted = this._EncDecService.nwt(this.session_token, params);
        var enc_data = {
          data: encrypted,
          email: this.useremail
        }
        return this._vehicleModelsService.getVehicleModels(enc_data);
      }).subscribe(result => {
        this.loadingIndicator = '';
        if (result && result.status === 200) {
          result = this._EncDecService.dwt(this.session_token, result.data);
          this.vehicleModels = result.vehicleModelsList;
        }
      });

    let that = this;
    that.socket.on("cancelled_order", function (dec) {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var data: any = that._EncDecService.sockDwt(that.socket_session_token, dec);
        if (Object.keys(data).length !== 0) {
          if (data.company_id === window.localStorage['user_company']) {
            if (data) {
              if (data.cancel_response === "N") {
                that.toastr.error("Order Cannot be cancelled now");
              } else if (data.cancel_response === "C") {
                that.toastr.info("Order is already completed");
              } else {
                that.orders.forEach(order => {
                  if (order._id === data.order_id) {
                    that.toastr.success("Order Cancelled Successfully");
                    order.order_status = "dispatcher_cancel";
                  }
                });
              }
            }
          }
        } else {
          let param_socket = {
            company_id: this.company_id_array
          }
          let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._usersService.getSocketsession(data1).then((res1) => {
            if (res1 && res1.status == 200) {
              var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
              if (res1.status == 200) {
                window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
                this.socket_session_token = window.localStorage['SocketSessiontoken'];
              }
            }
          })
        }
      }
    });
    that.socket.on("order_updates", function (dec) {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var request: any = that._EncDecService.sockDwt(that.socket_session_token, dec);
        if (Object.keys(request).length !== 0) {
          if (request.company_id === window.localStorage['user_company']) {

            if (that.orders.length > 10) {
              that.orders.splice(-1, 1);
            } else {
              that.orders.splice(-1, 0);
            }
            that.ordersLength = that.orders.length;
            that.orders.unshift(request.order_for_dispatcher);
            that.orderSearchSubmit = false;
          }
        } else {
          let param_socket = {
            company_id: that.company_id_array
          }
          let enc_data = that._EncDecService.nwt(that.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: that.useremail
          }
          that._usersService.getSocketsession(data1).then((res1) => {
            if (res1)
              if (res1.status == 200) {
                var res_data1: any = that._EncDecService.dwt(that.session_token, res1.data);
                window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
                that.socket_session_token = window.localStorage['SocketSessiontoken'];
              } else {
                console.log("failed to fetch");
              }
          })
        }
      }
    });

    that.socket.on("personal_order_updates", function (dec) {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var orderupdateallow = true;
        //alert(JSON.stringify(request.order_for_dispatcher.driver_id));
        //alert('personal_order_updates>>' + JSON.stringify(request.order_for_dispatcher.order_status));
        var request: any = that._EncDecService.sockDwt(that.socket_session_token, dec);
        if (Object.keys(request).length !== 0) {
          if (request.company_id === window.localStorage['user_company']) {
            var orderexist = false;
            var mom1;
            var mom2;
            if (this.selectedMoment) {
              mom1 = this.selectedMoment.clone();
            }
            if (this.selectedMoment1) {
              mom2 = this.selectedMoment1.clone();
            }
            that.orders.forEach(order => {
              if (
                order &&
                typeof order != "undefined" &&
                typeof request.order_for_dispatcher.order_status != "undefined"
              ) {
                if (order._id === request.order_for_dispatcher._id) {
                  //alert("already exist order no push");
                  orderexist = true;
                }
                //alert(mom2);
                if (mom2 == "" || mom2 == undefined) {
                  orderupdateallow = true;
                } else if (
                  !moment(request.order_for_dispatcher.created_at).isBetween(
                    mom1,
                    mom2
                  )
                ) {
                  orderupdateallow = false;
                }
              }
            });
            if (!orderexist) {
              if (that.socketFlag) {
                if (that.orders.length > 9) {
                  that.orders.splice(-1, 1);
                } else {
                  that.orders.splice(-1, 0);
                }
                that.orders.unshift(request.order_for_dispatcher);
                that.ordersLength = that.orders.length;
                that.orderSearchSubmit = false;
              }
            }
          }
        } else {
          let param_socket = {
            company_id: that.company_id_array
          }
          let enc_data = that._EncDecService.nwt(that.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: that.useremail
          }
          that._usersService.getSocketsession(data1).then((res1) => {
            if (res1 && res1.status == 200) {
              var res_data1: any = this._EncDecService.dwt(that.session_token, res1.data);
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              that.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          })
        }
        //that.getAllorderCount();
      }
    });

    that.socket.on("order_status", function (dec) {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var data: any = that._EncDecService.sockDwt(that.socket_session_token, dec);
        if (Object.keys(data).length !== 0) {
          if (data.company_id === window.localStorage['user_company']) {
            if (that.tagOrdersLength > 0 && !that.isLoading)
              that.getorderTagsForSocket(data.response.order_status);
            if (that.socketFlag) {
              let i = 0;
              let index;
              while (i < data.response.order_status.length) {
                index = that.orders.findIndex(x => x._id == data.response.order_status[i]._id);
                if (index > -1) {
                  that.orders[index].drop_location = data.response.order_status[i].drop_location;
                  that.orders[index].pickup_location = data.response.order_status[i].pickup_location;
                  that.orders[index].sync_trip_id = data.response.order_status[i].sync_trip_id;
                  that.orders[index].company_id = data.response.order_status[i].company_id;
                  that.orders[index].driver_id = data.response.order_status[i].driver_id;
                  that.orders[index].vehicle_id = data.response.order_status[i].vehicle_id;
                  that.orders[index].order_status = data.response.order_status[i].order_status;
                  that.orders[index].price = data.response.order_status[i].price;
                  that.orders[index].updated_at = data.response.order_status[i].updated_at;
                  that.orders[index].distance = data.response.order_status[i].distance;
                  that.orders[index].start_time = data.response.order_status[i].start_time;
                  that.orders[index].scheduler_start_date = data.response.order_status[i].scheduler_start_date;
                  that.orders[index].schedule_before = data.response.order_status[i].schedule_before;

                } else {
                  if (that.orders.length > 9) {
                    that.orders.splice(-1, 1);
                  }
                  that.orders.unshift(data.response.order_status[i]);
                  that.orders.sort(
                    (a, b) =>
                      new Date(b.updated_at).getTime() -
                      new Date(a.updated_at).getTime()
                  );
                  that.ordersLength = that.orders.length;
                }
                i++;
              }
            } else {
              let i = 0;
              let index;
              while (i < data.response.order_status.length) {
                index = that.orders.findIndex(x => x._id == data.response.order_status[i]._id);
                if (index > -1) {
                  that.orders[index].drop_location =
                    data.response.order_status[i].drop_location;
                  that.orders[index].pickup_location =
                    data.response.order_status[i].pickup_location;
                  that.orders[index].sync_trip_id = data.response.order_status[i].sync_trip_id;
                  that.orders[index].driver_id = data.response.order_status[i].driver_id;
                  that.orders[index].vehicle_id = data.response.order_status[i].vehicle_id;
                  that.orders[index].company_id = data.response.order_status[i].company_id;
                  that.orders[index].order_status = data.response.order_status[i].order_status;
                  that.orders[index].price = data.response.order_status[i].price;
                  that.orders[index].updated_at = data.response.order_status[i].updated_at;
                  that.orders[index].distance = data.response.order_status[i].distance;
                  that.orders[index].start_time = data.response.order_status[i].start_time;
                }
                i++;
              }
            }
          }
        } else {
          let param_socket = {
            company_id: that.company_id_array
          }
          let enc_data = that._EncDecService.nwt(that.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: that.useremail
          }
          that._usersService.getSocketsession(data1).then((res1) => {
            if (res1 && res1.status == 200) {
              var res_data1: any = that._EncDecService.dwt(that.session_token, res1.data);
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              that.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          })
        }
      }
    });
    this.socket.on("devicelocations_updates", dec => {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var arg: any = this._EncDecService.sockDwt(this.socket_session_token_map, dec);
        if (arg && Object.keys(arg).length !== 0) {
          //console.log("company ids+================="+JSON.stringify(this.company_id_list2)+"======================"+JSON.stringify(arg.company_id));
          if (this.dtc && this.company_id_list2.length > 0) {
            let companyIndex = this.company_id_list2.indexOf(arg.company_id);
            if (companyIndex > -1) {
              console.log("dtc multiple companies++++" + JSON.stringify(arg.vehicleStatus.logged_out_vehicle_count));
              this.mapSocketCountList[arg.company_id] = arg.vehicleStatus;
            }
            else {
              if (this.mapSocketCountList[arg.company_id] !== undefined)
                delete this.mapSocketCountList[arg.company_id];
            }
          }
          else if (arg.company_id === window.localStorage['user_company']) {
            console.log("dtc only============");
            this.loggedLength = arg.vehicleStatus.logged;
            this.bookedLength = arg.vehicleStatus.booked;
            this.offlineLength = arg.vehicleStatus.offline;
            this.freeLength = arg.vehicleStatus.free;
            this.acceptedLength = arg.vehicleStatus.accepted;
            this.almostFreeLength = arg.vehicleStatus.almost_free;
            this.logged_out = arg.vehicleStatus.logged_out_vehicle_count;
            this.normal_pause = arg.vehicleStatus.normal_pause;
            this.uber_careem = arg.vehicleStatus.uber_careem;
            console.log("loggedout==============" + this.logged_out);
          }
        } else {
          let param_socket = {
            company_id: this.company_id_list2.length > 0 ? this.company_id_list2 : this.company_id_array
          }
          let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._usersService.getSocketsession(data1).then((res1) => {
            if (res1 && res1.status == 200) {
              var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
              this.socket_session_token_map = res_data1.socket_token;
              // window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              // this.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          })
        }
      }
    });
    this.socket.on("inactive_devices", dec => {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var dev: any = this._EncDecService.sockDwt(this.socket_session_token, dec);
        if (Object.keys(dev).length !== 0) {
          if (dev.company_id === window.localStorage['user_company']) {
            this._FreeVehicleService.setDevices(dev);
          }
        } else {
          let param_socket = {
            company_id: this.company_id_array
          }
          let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._usersService.getSocketsession(data1).then((res1) => {
            var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
            if (res1.status == 200) {
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              this.socket_session_token = window.localStorage['SocketSessiontoken'];
            } else {
              console.log("failed to fetch");
            }
          })
        }
        //this.notification(dev);
      }

    });
    this.socket.on("Scheduled_trips", dec => {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var data: any = this._EncDecService.sockDwt(this.socket_session_token, dec);
        if (data && Object.keys(data).length !== 0) {
          if (data.company_id === window.localStorage['user_company']) {
            this._FreeVehicleService.setScheduledTrips(data);
          }
        } else {
          let param_socket = {
            company_id: this.company_id_array
          }
          let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._usersService.getSocketsession(data1).then((res1) => {
            var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
            if (res1.status == 200) {
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              this.socket_session_token = window.localStorage['SocketSessiontoken'];
            } else {
              console.log("failed to fetch");
            }
          })
        }
      }

    });
    this.socket.on("timed_out_orders", dec => {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var data: any = this._EncDecService.sockDwt(this.socket_session_token, dec);
        if (Object.keys(data).length !== 0) {
          if (data.company_id === window.localStorage['user_company']) {
            this._FreeVehicleService.setTimedOutOrders(data);
            let i = 0;
            while (i < data.timed_out.length) {
              if (data.timed_out[i].scheduler_start_date != null)
                this.notification(data.timed_out[i], 4);
              else if (data.timed_out[i].driver_id == null)
                this.notification(data.timed_out[i], 3);
              i++;
            }
          }
        } else {
          let param_socket = {
            company_id: this.company_id_array
          }
          let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._usersService.getSocketsession(data1).then((res1) => {
            if (res1 && res1.status == 200) {
              var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              this.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          })
        }
      }
    });
    this.socket.on("hourly_notification", data => {
      if (!data || data == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      }
      data = this._EncDecService.sockDwt(this.socket_session_token, data);
      if (Object.keys(data).length !== 0) {
        if (data.company_id === window.localStorage['user_company']) {
          //this._FreeVehicleService.setTimedOutOrders(data);
          let i = 0;
          while (i < data.timed_out.length) {
            this.notification(data.timed_out[i], 6);
            i++;
          }
        }
      } else {
        let param_socket = {
          company_id: this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._usersService.getSocketsession(data1).then((res1) => {
          if (res1 && res1.status == 200) {
            var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
            window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
            this.socket_session_token = window.localStorage['SocketSessiontoken'];
          }
        })
      }
    });
    this.socket.on("over_speed_trip", dec => {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var data: any = this._EncDecService.sockDwt(this.socket_session_token, dec);
        if (Object.keys(data).length !== 0) {
          if (data.company_id === window.localStorage['user_company']) {
            this.notification(data.data, 5);
          }
        } else {
          let param_socket = {
            company_id: this.company_id_array
          }
          let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._usersService.getSocketsession(data1).then((res1) => {
            if (res1 && res1.status == 200) {
              var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              this.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          })
        }
      }
    });
    this.socket.on("over_speed_emit", dec => {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var data: any = this._EncDecService.sockDwt(this.socket_session_token, dec);
        if (Object.keys(data).length !== 0) {
          if (data.company_id === window.localStorage['user_company']) {
            this._FreeVehicleService.setOverSpeed(data.Orders);
          }
        } else {
          let param_socket = {
            company_id: this.company_id_array
          }
          let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._usersService.getSocketsession(data1).then((res1) => {
            if (res1 && res1.status == 200) {
              var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              this.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          })
        }
      }
    });

    this.socket.on("flagged_trip", dec => {
      if (!dec || dec == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      } else {
        var data: any = this._EncDecService.sockDwt(this.socket_session_token, dec);
        if (Object.keys(data).length !== 0) {
          if (data.company_id === window.localStorage['user_company']) {
            this.notification(data.data, 7);
          }
        } else {
          let param_socket = {
            company_id: this.company_id_array
          }
          let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._usersService.getSocketsession(data1).then((res1) => {
            if (res1 && res1.status == 200) {
              var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              this.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          })
        }
      }
    });

    /* This section handles the storage of filter data for data persistancy */
    let dData: any = this._global.dispatcher_orders;
    if (dData && dData.order) {
      this.vehicle_id2 = dData.vehicle_id;
      setTimeout(() => {
        this.plateFilter = dData.vehicle;
        this.dispatcherForOrder.setValue(dData.dispatcher)
      }, 1000)
      this.tariff_id = dData.tariffid;
      this.driver_id = dData.driverid;
      //this.pickup_loc = dData.pickup_location;
      this.driver_id2 = dData.driver;
      //this.drop_loc = dData.drop_location;
      this.current_order_status = dData.current_order_status;
      dData.status.forEach(element => {
        this.statuses.push(element)
      });
      dData.order.forEach(element => {
        this.ordersFilter.push(element)
      });
      this.selectedMoment = dData.start_date ? new Date(dData.start_date) : '';
      this.selectedMoment1 = dData.end_date ? new Date(dData.end_date) : '';
      this.user_id = dData.userid;
      this.user_phone = dData.user;
      this.user_name = dData.user_name;
      this.generated_by = dData.generate_by;
      this.partner = dData.partner_id;
      this.vehicleTypeFilter = dData.model_id;
      this.promo_id = dData.promo_id;
      this.payment_type_id = dData.payment_type_id;
      this.order_edited = dData.order_edited;
      this.flagged_orders = dData.flagged_orders;
      //this.unique_order_id = dData.keyword;
      this.getOrders(false);
    }
    else
      this.getOrders(true);
    /* --------end---------*/
    if (this.dtc && this.company_id_list2.length > 0)//Gets vehicle count of all companies
      setInterval(() => {
        if (this.vehicle_category_ids.length > 0 || this.vehicleTypeFilter || this.driverGroupMap != "" || this.internetFlag || this.gpsFlag || this.breakFlag || this.uberFlag) {

        } else {
          let loggedLength = 0;
          let booked = 0;
          let offline = 0;
          let acceptedLength = 0;
          let free = 0;
          let almostFreeLength = 0;
          let logged_out = 0;
          let normal_pause = 0;
          let uber_careem = 0;
          this.loggedLength = 0;
          this.bookedLength = 0;
          this.offlineLength = 0;
          this.freeLength = 0;
          this.acceptedLength = 0;
          this.almostFreeLength = 0;
          this.logged_out = 0;
          this.normal_pause = 0;
          this.uber_careem = 0;
          console.log("dataaaaaaaaaaaaaaaaaa+", this.mapSocketCountList);
          Object.keys(this.mapSocketCountList).forEach(element => {
            loggedLength += this.mapSocketCountList[element].logged;
            booked += this.mapSocketCountList[element].booked;
            offline += this.mapSocketCountList[element].offline;
            free += this.mapSocketCountList[element].free;
            acceptedLength += this.mapSocketCountList[element].accepted;
            almostFreeLength += this.mapSocketCountList[element].almost_free;
            logged_out += this.mapSocketCountList[element].logged_out_vehicle_count;
            normal_pause += this.mapSocketCountList[element].normal_pause;
            uber_careem += this.mapSocketCountList[element].uber_careem;
          });
          this.loggedLength = loggedLength;
          this.bookedLength = booked;
          this.offlineLength = offline;
          this.freeLength = free;
          this.acceptedLength = acceptedLength;
          this.almostFreeLength = almostFreeLength;
          this.logged_out = logged_out;
          this.normal_pause = normal_pause;
          this.uber_careem = uber_careem;
        }
        console.log("loggedout+++++++++++++=" + this.logged_out);
      }, 4000)
  }
  public alertText1: any;
  public toastAlert(arg) {
    if (arg.myFreeVehicles.length > 0) {
      this.alertText1 =
        "Driver id : " + arg.myFreeVehicles[0].driver_id.unique_driver_id;
      this.toastr.info(
        "has been inactive for more than 1 hr",
        this.alertText1,
        { data: "alert", dismiss: "click", toastLife: 6000, enableHTML: true }
      );
    } else if (arg.myFreeVehicles && arg.sosVehicles) {
      this.alertText1 =
        "Driver id : " + arg.myFreeVehicles[0].driver_id.unique_driver_id;
      this.toastr.success("There are SOS vehicles", this.alertText1, {
        data: "alert",
        dismiss: "click",
        toastLife: 6000
      });
    } else if (arg.sosVehicles) {
      this.toastr.error("There are SOS vehicles", null, {
        data: "alert",
        dismiss: "click",
        toastLife: 6000
      });
    }

    this.toastr.onClickToast().subscribe(toast => {
      if (toast.data == "alert") {
        if (this.dialog.openDialogs.length == 0) {
          let dialogRef = this.dialog.open(NotificationComponent, {
            width: "350px",
            data: "dada",
            scrollStrategy: this.overlay.scrollStrategies.noop()
          });
          dialogRef.updatePosition({ top: "50px", right: "50px" });
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
            } else {
            }
          });
        }
      }
    });

  }
  full_map(event) {
    event.preventDefault();
    let url = "https://mobilityae.com/dispatcher/map";
    window.open(url, "_blank", "width=1200,height=700");
  }
  stop() {
    this.userIdle.stopWatching();
  }
  restart() {
    this.userIdle.resetTimer();
  }
  public clicked1(detail) {
    this.addDispatcher.controls["vehicleList"].setValue("");
    this.addDispatcher.controls["driverList"].setValue("");
    this.addDispatcher.controls["vehicleList"].updateValueAndValidity();
    this.addDispatcher.controls["driverList"].updateValueAndValidity();
    if (detail.driver_id) {
      this.onFlagChange(false);
      this.selectVehicle = true;
      this.selectDriver = true;
      this.vehicle_id2 = '';
      const params = {
        driver_id: detail.driver_id,
        offset: 0,
        limit: 0,
        sortOrder: 'desc',
        sortByColumn: '_id',
        role: 'dispatcher',
        company_id: this.company_id_array
      };
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getVehicleByDriverId(data1).subscribe((datab) => {
        if (datab && datab.status == 200) {
          datab = this._EncDecService.dwt(this.session_token, datab.data);
          datab = datab.getSingleDriver;
          this.addDispatcher.get('vehicleList').setValue(datab[0]);
          const params1 = {
            offset: 0,
            limit: 0,
            sortOrder: 'desc',
            sortByColumn: '_id',
            role: 'dispatcher',
            search_by_driverid: datab[0].driver_id,
            company_id: this.company_id_array
          };
          let enc_data = this._EncDecService.nwt(this.session_token, params1);
          //alert(this.dispatcher_id.email)
          let data2 = {
            data: enc_data,
            email: this.useremail
          }
          this._driverService.getLoginDriverById(data2).subscribe((data_driverService) => {
            if (data_driverService && data_driverService.status == 200) {
              data_driverService = this._EncDecService.dwt(this.session_token, data_driverService.data);
              if (data_driverService.drivers.length == 0) {
                this.onFlagChange(true);
                this.addDispatcher.controls['vehicleList'].setValue('');
                this.addDispatcher.controls['driverList'].setValue('');
                this.addDispatcher.controls['vehicleList'].updateValueAndValidity();
                this.addDispatcher.controls['driverList'].updateValueAndValidity();
                return;
              }
              let model_id = detail.vehicle_model_id._id ? detail.vehicle_model_id._id : detail.vehicle_model_id;
              let params = {
                company_id: this.company_id_array,
                _id: model_id
              }
              let enc_data = this._EncDecService.nwt(this.session_token, params);
              //alert(this.dispatcher_id.email)
              let data1 = {
                data: enc_data,
                email: this.useremail
              }
              this._vehicleModelsService.getVehicleModelById(data1).then((res) => {
                if (res && res.status == 200) {
                  res = this._EncDecService.dwt(this.session_token, res.data);
                  this.modelChips = [res.vehicleModel];
                  this.modelList2 = [res.vehicleModel];
                  this.tariffData = this.tariffDataTemp.slice()
                  var flag = -1;
                  var i = this.tariffData.length;
                  while (i--) {
                    flag = this.tariffData[i].vehicle_model_id.findIndex(x => x._id == res.vehicleModel._id);
                    if (flag == -1) {
                      this.tariffData.splice(i, 1);
                    }
                  }
                }
              });
              this.addDispatcher
                .get("driverList")
                .setValue(data_driverService.drivers[0]);
            }
          });
        }
      });
    }
    return;
  }

  public getVehicleForSocket(vehicle_id) {
    this.positions = [];
    this.positions1 = [];
    this.vehicle_id = vehicle_id;
    //this.liveTracking(vehicle_id);
    if (vehicle_id) {
      this.getRefreshTime('6');
      this.setFilterLoading(
        "#dropdownMenuButton",
        false,
        this.mapFilterLabels[7]
      );
      let params = {
        company_id: this.company_id_array,
        vehicle_id: vehicle_id
      }
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._devicelocationService
        .getdatabyvehicleidNew(data1)
        .then(vehicleFreedata => {
          if (vehicleFreedata && vehicleFreedata.status == 200) {
            vehicleFreedata = this._EncDecService.dwt(this.session_token, vehicleFreedata.data);
            this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
            this.selectDriverData = vehicleFreedata.driversArr;
            this.selectVehicleData = vehicleFreedata.vehicleArr;
            this.deviceLocationData = vehicleFreedata.getdeviceloc;
            //this.vehicleFilter = this.selectVehicleData[0];
            //this.plateFilter = this.selectVehicleData[0];
            let id;
            if (
              this.SingledeviceLocationData.length > 0 &&
              typeof this.SingledeviceLocationData[0] != "undefined" &&
              this.SingledeviceLocationData[0].latitude != ""
            ) {
              for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                const randomLat = this.SingledeviceLocationData[i].latitude;
                const randomLng = this.SingledeviceLocationData[i].longitude;
                // this.doCenter({
                //   lat: parseFloat(randomLat),
                //   lng: parseFloat(randomLng)
                // });
                if (this.SingledeviceLocationData[i]["device_id"]) {
                  id = this.SingledeviceLocationData[i]["device_id"];
                } else {
                  id = "";
                }
                let vehicleStatus = this.getVehicleStatus(
                  this.selectVehicleData[i]
                );
                var driverIcon = this.driverIcons.loggedIn;
                let vehicleModelIsValid =
                  this.SingledeviceLocationData[i].vehicle_model_id &&
                  (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                    this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                let isTesla =
                  vehicleModelIsValid &&
                  (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                    this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                var driverIcon = this.driverIcons.loggedIn;
                if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                  driverIcon = this.teslaIcons['loggedOut'];
                }
                else if (vehicleStatus == 'offline') {
                  if (isTesla)
                    driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                  else
                    driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                      ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                }
                else {
                  if (this.SingledeviceLocationData[i].trip_status !== "") {
                    if (isTesla) {
                      switch (this.SingledeviceLocationData[i].trip_status) {
                        case 'accepted_by_driver':
                          driverIcon = this.teslaIcons['accepted'];
                          break;
                        case 'confirming_rate':
                          driverIcon = this.teslaIcons['almost_free'];
                          break;
                        case 'selecting_payment':
                          driverIcon = this.teslaIcons['almost_free'];
                          break;
                        case 'rating':
                          driverIcon = this.teslaIcons['almost_free'];
                          break;
                        default:
                          driverIcon = this.teslaIcons[vehicleStatus]
                          break;
                      }
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                    }
                    else {
                      switch (this.SingledeviceLocationData[i].trip_status) {
                        case 'accepted_by_driver':
                          driverIcon = this.driverIcons['accepted'];
                          break;
                        case 'confirming_rate':
                          driverIcon = this.driverIcons['almost_free'];
                          break;
                        case 'selecting_payment':
                          driverIcon = this.driverIcons['almost_free'];
                          break;
                        case 'rating':
                          driverIcon = this.driverIcons['almost_free'];
                          break;
                        default:
                          driverIcon = this.driverIcons[vehicleStatus]
                          break;
                      }
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                        ? this.driverIcons['internet'] : driverIcon;
                    }
                  }
                  else {
                    if (isTesla) {
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                        ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                    }
                    else {
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                        ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                    }
                  }
                }
                if (randomLat != "" && randomLng != "" && id != "") {
                  this.positions.push([randomLat, randomLng, id]);
                  this.displayDriverOnMap(
                    randomLat,
                    randomLng,
                    id,
                    driverIcon,
                    0
                  );
                }
                this.removeStaleDriversFromMap();
              }
              this.locationmsg = "";
            } else {
              this.locationmsg = "No";
            }
          }
          else {
            this.locationmsg = "No";
          }
        });
    } else {
      this.selectDriverData = [];
      this.locationmsg = "No";
      this.setMapFilter("7");
    }
  }

  public isExist(id) {
    return this.paymentTableData.some(function (el) {
      return el.id == id;
    });
  }
  public reset1() {
    this.schedulerStaus = false;
    this.check_interval = "yes";
    this.check_filter_interval = "no";
    this.driverFilter = "";
    this.vehicleFilter = "";
    this.sideFilter = "";
    this.vehicle_id = "";
    this.vehicle_id2 = "";
    this.placeFilter = "";
    this.mapFilter = "";
    this.pNo = 1;
    this.socketFlag = true;
  }

  onValueChanged(data?: any) {
    if (!this.addDispatcher) {
      return;
    }
    const form = this.addDispatcher;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = "";
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + " ";
        }
      }
    }
  }

  /**
   * get Payment Type
   */
  getPaymentType() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: "desc",
      sortByColumn: "_id",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._paymentTypeService.getPaymentTypes(data1).then(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.paymenttypeData = data.paymentTypes;
      }
    });
  }
  /**
   * Customer by Name search AutoComplete
   * @param item
   */
  public getCustomer() {
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: "desc",
      sortByColumn: "updated_at",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._customerService.get(data1).then(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.customerData = data.getCustomerContact;
        this.userMatloading = false;
      }
    });
  }
  twoGisPickup(event) {
    if (event.id) {
      this.gisPickPlot(event);
    } else {
      this.twoGisService.twoGisFindId(event.hint.text).subscribe((dec) => {
        this.gisPicked = dec;
        this.gisPickPlot(this.gisPicked);
      })
    }
  }
  public gisPickPlot(event) {
    var gisId = event.id ? event.id : event.result.items[0].id;
    this.twoGisService.twoGisPlaceFind(gisId).subscribe((dec) => {
      if (dec.result.items[0].entrances.length > 0) {
        for (let i = 0; i < this.picking.length; i++) {
          this.picking[i].setMap(null);
        }
        this.twoGispickEntrance = dec.result.items[0].entrances;
        this.pickupLat = '';
        this.pickupLang = '';
        this.soursePickup = '';
        this.pickupMarkers = [];
        var regExp = /\(([^)]+)\)/;
        this.addDispatcher.controls["pickup_location"].setValue(event.hint ? event.hint.text : event.result.items[0].name);
        this.addDispatcher.controls["pickup_country"].setValue("United Arab Emirates");
        var matches = regExp.exec(this.twoGispickEntrance[0].geometry.points[0]);
        var splits = matches[1].split(' ');
        this.pickupMarkers.push([splits[1], splits[0]]);
        this.pickupLat = splits[1];
        this.pickupLang = splits[0];
        this.soursePickup = this.pickupLat + ',' + this.pickupLang;
        this.direction.origin = String(
          this.pickupLat + "," + this.pickupLang
        );
        this.showDirection();
      } else {
        for (let i = 0; i < this.picking.length; i++) {
          this.picking[i].setMap(null);
        }
        this.pickupLat = '';
        this.pickupLang = '';
        this.soursePickup = '';
        this.pickupMarkers = [];
        this.addDispatcher.controls["pickup_location"].setValue(event.hint ? event.hint.text : event.result.items[0].name);
        this.addDispatcher.controls["pickup_country"].setValue("United Arab Emirates");
        var regExp = /\(([^)]+)\)/;
        var matches = regExp.exec(dec.result.items[0].geometry.centroid);
        var splits = matches[1].split(' ');
        this.pickupMarkers.push([splits[1], splits[0]]);
        this.pickupLat = splits[1];
        this.pickupLang = splits[0];
        this.direction.origin = String(
          this.pickupLat + "," + this.pickupLang
        );
        this.showDirection();
      }
    })
  }
  public twoGispickupEntry(event) {
    this.pickupLat = '';
    this.pickupLang = '';
    this.soursePickup = '';
    this.pickupMarkers = [];
    var regExp = /\(([^)]+)\)/;
    var matches = regExp.exec(event.geometry.points[0]);
    var splits = matches[1].split(' ');
    this.pickupMarkers.push([splits[1], splits[0]]);
    this.pickupLat = splits[1];
    this.pickupLang = splits[0];
    this.soursePickup = this.pickupLat + ',' + this.pickupLang;
    this.direction.origin = String(
      this.pickupLat + "," + this.pickupLang
    );
    this.showDirection();
  }
  public twoGisDrop(event) {
    if (event.id) {
      this.gisDropPlot(event);
    } else {
      this.twoGisService.twoGisFindId(event.hint.text).subscribe((dec) => {
        this.gisDroped = dec;
        this.gisDropPlot(this.gisDroped);
      })
    }
  }
  public gisDropPlot(event) {
    var gisId = event.id ? event.id : event.result.items[0].id;
    this.twoGisService.twoGisPlaceFind(gisId).subscribe((dec) => {
      if (dec.result.items[0].entrances.length > 0) {
        for (let i = 0; i < this.dropOffing.length; i++) {
          this.dropOffing[i].setMap(null);
        }
        this.twoGisDropEntrance = dec.result.items[0].entrances;
        this.dropOffLang = "";
        this.dropOffLat = "";
        this.sourseDrop = "";
        this.dropOffMarkers = [];
        var regExp = /\(([^)]+)\)/;
        this.addDispatcher.controls["drop_location"].setValue(event.hint ? event.hint.text : event.result.items[0].name);
        this.addDispatcher.controls["drop_country"].setValue("United Arab Emirates");
        var matches = regExp.exec(this.twoGisDropEntrance[0].geometry.points[0]);
        var splits = matches[1].split(' ');
        this.dropOffMarkers.push([splits[1], splits[0]]);
        this.dropOffLat = splits[1];
        this.dropOffLang = splits[0];
        this.sourseDrop = this.pickupLat + ',' + this.pickupLang;
        this.direction.destination = String(
          this.dropOffLat + "," + this.dropOffLang
        );
        this.showDirection();
      } else {
        for (let i = 0; i < this.dropOffing.length; i++) {
          this.dropOffing[i].setMap(null);
        }
        this.dropOffLang = "";
        this.dropOffLat = "";
        this.sourseDrop = "";
        this.dropOffMarkers = [];
        this.addDispatcher.controls["drop_location"].setValue(event.hint ? event.hint.text : event.result.items[0].name);
        this.addDispatcher.controls["drop_country"].setValue("United Arab Emirates");
        var regExp = /\(([^)]+)\)/;
        var matches = regExp.exec(dec.result.items[0].geometry.centroid);
        var splits = matches[1].split(' ');
        this.dropOffMarkers.push([splits[1], splits[0]]);
        this.dropOffLat = splits[1];
        this.dropOffLang = splits[0];
        this.sourseDrop = this.dropOffLat + ',' + this.dropOffLang;
        this.direction.destination = String(
          this.dropOffLat + "," + this.dropOffLang
        );
        this.showDirection();
      }
    })
  }
  public twoGisDropEntry(event) {
    this.dropOffLang = "";
    this.dropOffLat = "";
    this.sourseDrop = "";
    this.dropOffMarkers = [];
    var regExp = /\(([^)]+)\)/;
    var matches = regExp.exec(event.geometry.points[0]);
    var splits = matches[1].split(' ');
    this.dropOffMarkers.push([splits[1], splits[0]]);
    this.dropOffLat = splits[1];
    this.dropOffLang = splits[0];
    this.sourseDrop = this.pickupLat + ',' + this.pickupLang;
    this.direction.destination = String(
      this.dropOffLat + "," + this.dropOffLang
    );
    this.showDirection();
  }
  public tgisOnOff() {
    this.tgisStatus = this.tgisSelector.value;
  }
  public displayNameFn(data): string {
    return data ? data.firstname : data;
  }
  public displayGisPlace(data): string {
    return data ? data.hint.text : data;
  }
  public displayTpik(data): string {
    var val = 'Entrance ' + data;
    return data ? val : '';
  }
  public displayTdrp(data): string {
    var val = 'Entrance ' + data;
    return data ? val : '';
  }

  /**
   *  start time timepicker
   */
  checkShiftstartTime() {
    this.selectedMoment1 = "";
    this.tagEndDate = ""
  }

  /**
   *  end time timepicker
   */
  checkShiftstartTime1() {
    if (this.selectedMoment1 != '') {
      //this.selectedMoment="";
    }
  }
  OnSelectPhone(data) {
    //this.getTariff();
    this.customerEditForm.dialCode = data.source.value.country_code ? data.source.value.country_code : '971';
    this.showCustomerInfo = true;
    this.addDispatcher.get("customerNameList").setValue(data.source.value);
    this.SelectedCustomer = data.source.value;
    this.addDispatcher.get("customerCountryCode").setValue(this.SelectedCustomer.country_code ? this.SelectedCustomer.country_code : '971');
    this.customerEditForm.name = this.SelectedCustomer ? this.SelectedCustomer.firstname : '';
    this.customerEditForm.phone = this.SelectedCustomer ? this.SelectedCustomer.phone_number : '';
    this.customerEditForm.email = this.SelectedCustomer ? this.SelectedCustomer.email : '';
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'unique_order_id',
      limit: 10,
      user_id: data.source.value._id,
      company_id: this.company_id_array
    }
    //alert(this.useremail);
    let enc_data = this._EncDecService.nwt(this.session_token, param);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOrderByUserId(data1).then(data => {
      if (data && data.stats == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.orderHistory = data.data;
      }
    });
    let params1 = {
      company_id: this.company_id_array,
      _id: data.source.value._id
    }
    let enc_data1 = this._EncDecService.nwt(this.session_token, params1);
    //alert(this.dispatcher_id.email)
    let data2 = {
      data: enc_data1,
      email: this.useremail
    }
    //alert(JSON.stringify(data2));
    this._customerService
      .getCustomerAddrress(data2)
      .then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.SelectedCustomerAddress = data.result;
        }
      });
    this._customerService
      .getCustomerMessages(data2)
      .then(data => {
        if (data2 && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.selectedCustomerMessages = data.result;
        }
      });
  }

  /**
   * Customer By Phone number Autocomple Search
   * @param item
   */
  public getPhoneNumber() {
    let params = {
      offset: 0,
      limit: 10,
      sortOrder: 'asc',
      sortByColumn: 'phone_number',
      company_id: this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._customerService.getPhoneNumber(data1).subscribe(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.customerPhone = data.getCustomerContact;
      }
    });
  }

  displayFn(data): string {
    return data ? data.phone_number : data;
  }
  displayFnCountry(data): string {
    return data ? data.code ? '+' + data.code : '+' + data : data;
  }
  displayFnside(data): string {
    return data ? data.vehicle_identity_number : data;
  }
  displayFnModel(data): string {
    return "";
  }
  displayFnModel2(data): string {
    return data ? data.name : "";
  }
  OnSelectName(data) {
    this.showCustomerInfo = true;
    this.addDispatcher.get("customerPhoneList").setValue(data.source.value);
    this.SelectedCustomer = data.source.value;
    this.addDispatcher.get("customerCountryCode").setValue(this.SelectedCustomer.country_code ? this.SelectedCustomer.country_code : '971');
    this.addDispatcher.get("customerPhoneList").setValue(data.source.value);
    this.customerEditForm.name = this.SelectedCustomer ? this.SelectedCustomer.firstname : '';
    this.customerEditForm.phone = this.SelectedCustomer ? this.SelectedCustomer.phone_number : '';
    this.customerEditForm.dialCode = this.SelectedCustomer.country_code ? this.SelectedCustomer.country_code : '971';
    this.customerEditForm.email = this.SelectedCustomer ? this.SelectedCustomer.email : '';
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'unique_order_id',
      limit: 10,
      user_id: data.source.value._id,
      company_id: this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, param);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService
      .getOrderByUserId(data1)
      .then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.orderHistory = data.data;
        }
      });
    let params = {
      company_id: this.company_id_array,
      _id: data.source.value._id
    }
    let enc_data1 = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data2 = {
      data: enc_data1,
      email: this.useremail
    }
    this._customerService
      .getCustomerAddrress(data2)
      .then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.SelectedCustomerAddress = data.result;
        }
      });
    this._customerService
      .getCustomerMessages(data2)
      .then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.selectedCustomerMessages = data.result;
        }
      });
  }

  /**
   * Get Partner Autocomplete
   * @param {string} val
   */
  public getPartner() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: "desc",
      sortByColumn: "updated_at",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._partnerService.getPartner(data1).then(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.tempPartner = data.getPartners;
        this.partnerData = data.getPartners;
        this.partnerData2 = data.getPartners;
      }
    });
  }

  onInputChangedPartner(val: string) { }

  onSelectPartner(item: any) {
    this.selectPartner = item;
  }

  /**
   * Location Autocomplete
   * @param autocomplete
   */
  public initialized(autocomplete: any) {
    this.autocomplete = autocomplete;
  }

  /**
   * Pickup Location place on select
   */
  public pickupChanged(place) {
    this.place1 = place;
    for (let i = 0; i < this.picking.length; i++) {
      this.picking[i].setMap(null);
    }
    for (let i = 0; i < place.address_components.length; i++) {
      let addressType = place.address_components[i].types[0];
      this.address[addressType] = place.address_components[i].long_name;
    }
    this.pickupLat = place.geometry.location.lat();
    this.pickupLang = place.geometry.location.lng();
    this.pickup_location = place.formatted_address;
    this.pickup_place_id = place.id || place.place_id;
    this.addDispatcher.controls["pickup_location"].setValue(
      place.name
        ? place.name + " " + place.formatted_address
        : place.formatted_address
    );
    this.addDispatcher.controls["pickup_country"].setValue(
      this.address.country
    );
    this.direction.origin = place.name + " " + place.formatted_address;
    this.showDirection();
  }

  /**
   * Drop Location Place on Select
   */
  public dropChanged(dropplace) {
    this.place2 = dropplace;
    for (let i = 0; i < this.dropOffing.length; i++) {
      this.dropOffing[i].setMap(null);
    }
    this.togglebtn();
    this.drop_location = dropplace.formatted_address;
    this.addDispatcher.controls["drop_location"].setValue(
      dropplace.name + " " + dropplace.formatted_address
    );
    for (let i = 0; i < dropplace.address_components.length; i++) {
      let addressType = dropplace.address_components[i].types[0];
      this.address[addressType] = dropplace.address_components[i].long_name;
    }
    this.addDispatcher.controls["drop_country"].setValue(this.address.country);
    this.direction.destination =
      dropplace.name + " " + dropplace.formatted_address;
    this.showDirection();
  }

  /**
   *  Get vehicle List Autocomplete
   */
  public getVehicles() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: "desc",
      sortByColumn: "_id",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._vehicleService.getOccupiedVehicle(params).subscribe(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.vehicleData = data.searchVehicle;
      }
    });
  }

  displayFnvehicle(data): string {
    return data ? data.vehicle_identity_number : data;
  }

  OnSelectVehicle(data) {
    this.selectDriver = true;
    this.selectVehicle = true;
    const params1 = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      role: 'dispatcher',
      search_by_driverid: data.source.value.driver_id,
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params1);
    //alert(this.dispatcher_id.email)
    let data2 = {
      data: enc_data,
      email: this.useremail
    }
    this._driverService.getLoginDriverById(data2).subscribe((data_driverService) => {
      if (data_driverService && data_driverService.status == 200) {
        data_driverService = this._EncDecService.dwt(this.session_token, data_driverService.data);
        this.modelChips = [data.source.value.vehicle_model_id];
        this.modelList2 = [data.source.value.vehicle_model_id];
        this.tariffData = this.tariffDataTemp.slice()
        var flag = -1;
        var i = this.tariffData.length;
        while (i--) {
          flag = this.tariffData[i].vehicle_model_id.findIndex(x => x._id == data.source.value.vehicle_model_id._id);
          if (flag == -1) {
            this.tariffData.splice(i, 1);
          }
        }
        this.addDispatcher
          .get("driverList")
          .setValue(data_driverService.drivers[0]);
      }
    });
  }

  checkVehicleControl() {
    if (!this.selectVehicle) {
      this.addDispatcher.get("vehicleList").setValue("");
    }
  }
  public getAllDriver() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: "desc",
      sortByColumn: "_id",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._driverService.searchForDriver(data1).subscribe(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.alldriverData = data.driver;
      }
    });
  }

  /**
   * Driver Autocomplete Search
   * @param data
   */
  searchDriver(data) {
    if (typeof data === "object") {
      this.plateFilter = "";
      this.plateFilter2 = "";
      this.driver_id = data._id;
      if (this.multiFilter.driver.findIndex(x => x._id == data._id) == -1)
        this.multiFilter.driver.push(data);
    }
  }
  public autoSearch() {
    this.loadingIndicator = '';
    setTimeout(() => {
      this.getOrders(false);
    }, 500)
  }
  displayFnDriver(data): string {
    return data ? data.username : data;
  }

  displayFndriver(data): string {
    return data ? data.username : data;
  }
  displayFnplate(data): string {
    return data ? data.plate_number : data;
  }
  displayFnOrderId(data): string {
    return data ? "" : "";
  }

  OnSelectDriver(data) {
    if (!this.copyOrderFlag)
      this.getTariff();
    this.selectVehicle = true;
    this.selectDriver = true;
    this.vehicle_id2 = '';
    const params = {
      driver_id: data.source.value._id,
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      role: 'dispatcher',
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._vehicleService.getVehicleByDriverId(data1).subscribe((datab) => {
      if (datab && datab.status == 200) {
        datab = this._EncDecService.dwt(this.session_token, datab.data);
        if (datab.getSingleDriver[0].length == [])
          return;
        let params1 = {
          company_id: this.company_id_array,
          _id: datab.getSingleDriver[0].vehicle_model_id
        }
        let enc_data1 = this._EncDecService.nwt(this.session_token, params1);
        //alert(this.dispatcher_id.email)
        let data2 = {
          data: enc_data1,
          email: this.useremail
        }
        this._vehicleModelsService.getVehicleModelById(data2).then((res) => {
          if (res && res.status == 200) {
            res = this._EncDecService.dwt(this.session_token, res.data);
            this.modelChips = [res.vehicleModel];
            this.modelList2 = [res.vehicleModel];
            if (!this.copyOrderFlag) {
              this.tariffData = this.tariffDataTemp.slice()
              var flag = -1;
              var i = this.tariffData.length;
              while (i--) {
                flag = this.tariffData[i].vehicle_model_id.findIndex(x => x._id == res.vehicleModel._id);
                if (flag == -1) {
                  this.tariffData.splice(i, 1);
                }
              }
            }
          }
        })
        this.addDispatcher.get('vehicleList').setValue(datab.getSingleDriver[0]);
      }
    });
  }

  checkDriverControl() {
    if (!this.selectDriver) {
      this.addDispatcher.get("driverList").setValue("");
    }
  }

  /**
   * multiple driver dispatch
   */

  public checkNumberOfCarsControl() {
    this.addDispatcher.get("NumberOfCars").setValue("");
  }

  /**
   * get Additional Service
   */
  public getAdditionalService() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: "asc",
      sortByColumn: "name",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._additionalService.get(data1).subscribe(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.AdditionalData = data.data;
      }
    });
  }

  displayFnAdditional(data): string {
    return data ? data.name : data;
  }

  checkAdditionalControl() {
    if (!this.selectAdditional) {
      this.addDispatcher.get("additionalList").setValue("");
    }
  }

  /**
   * get Payment Extra
   */
  public getpaymentExtra() {
    const params1 = {
      offset: 0,
      limit: 0,
      sortOrder: "desc",
      sortByColumn: "payment_extra",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params1);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._paymentService.getPaymentListing(data1).subscribe(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.paymetExtraData = data.getPayment;
        this.paymetExtraData.forEach(data => {
          if (data.multi_applicable == 0) {
            const found = this.isExist(data._id);
            if (found) {
              data.is_disable = true;
            } else {
              data.is_disable = false;
            }
          }
        });
      }
    });
  }

  displayFnpaymentExtra(data): string {
    return data ? data.payment_extra : data;
  }

  public onFlagChange(eve: any) {
    this.orderTypeFlag = eve;
    this.addDispatcher.get('NumberOfCars').setValue('')
    if (eve) {
      (<HTMLInputElement>(
        document.getElementById("schedulerdis")
      )).style.display = "block";
      this.st = true;
      this.show = false;
      this.addDispatcher.get("vehicleList").setValue("");
      // this.addDispatcher.get("driverList").setValue("");
      this.addDispatcher.controls["vehicleList"].setValidators([]);
      this.addDispatcher.controls["driverList"].setValidators([]);
      this.addDispatcher.controls["vehicleList"].updateValueAndValidity();
      this.addDispatcher.controls["driverList"].updateValueAndValidity();
      this.modelCtrl.enable();
      this.removableModel = true;
      this.automaticorder = true;
    } else {
      this.automaticorder = false;
      this.schedulerStaus = false;
      (<HTMLInputElement>document.getElementById("schedulerdis")).style.display = 'none';
      this.st = false;
      this.show = true;
      this.modelCtrl.disable();
      this.removableModel = false;
      this.modelFlag = false;
      this.addDispatcher.controls["vehicleList"].setValidators(
        Validators.required
      );
      this.addDispatcher.controls["driverList"].setValidators(
        Validators.required
      );
    }
  }
  public onSockFlagChange() {
    if (!this.socketFlag) {
      this.getOrders(true);
    }
    else
      this.getOrders(false)
  }
  public onChangeToggle(data) {
    if (data) {
      this.show_paymentExtra = true;
      this.total_sum();
    } else {
      this.show_paymentExtra = false;
      this.total_sum();
    }
  }

  public AddPaymentExtra() {
    this.show_paymentExtra = true;
    if (this.addDispatcher.controls["payment_extra_price"].value) {
      const paymentData = {
        id: this.addDispatcher.controls["paymentExtraList"].value._id,
        payment: this.addDispatcher.controls["paymentExtraList"].value
          .payment_extra,
        price: this.addDispatcher.controls["payment_extra_price"].value,
        multi_applicable: this.addDispatcher.controls["paymentExtraList"].value
          .multi_applicable
      };
      this.paymentTableData.push(paymentData);
      this.calcPrice = this.totalPrice(this.paymentTableData);
      this.addDispatcher.controls["paymentExtraList"].setValue("");
      this.addDispatcher.controls["payment_extra_price"].setValue("");
      this.total_sum();
    } else {
      this.showpaymenterror = true;
      this.show_paymentExtra = false;
      this.paymenterror = "Please select payment extra.";
    }
  }

  public totalPrice(data) {
    this.Totalprice = 0;
    this.payment_extra_ids = [];
    data.forEach(item => {
      this.Totalprice += parseFloat(item.price);
      this.payment_extra_ids.push(item.id);
    });
    return this.Totalprice.toFixed(2);
  }

  public total_sum() {
    if (
      this.addDispatcher.controls["tariff_price"].value &&
      this.show_paymentExtra
    ) {

      this.price_data = (
        parseFloat(this.addDispatcher.controls["multiple"].value) *
        parseFloat(this.addDispatcher.controls["price"].value) +
        parseFloat(this.calcPrice)
      ).toFixed(2);
      /*this.price_data="";*/
    } else if (this.show_paymentExtra) {
      this.price_data = this.calcPrice;
    } else if (this.addDispatcher.controls["tariff_price"].value) {
      this.price_data = (
        this.addDispatcher.controls["multiple"].value *
        this.addDispatcher.controls["tariff_price"].value
      ).toFixed(2);
    } else {
      this.price_data = "";
    }
    if (this.price_data === "NaN") {
      this.price_data = "";
    }
    this.addDispatcher.controls["total_sum"].setValue(this.price_data);
  }

  public deletePaymetExtra(index) {
    this.paymentTableData.splice(index, 1);
    this.payment_extra_ids.splice(index, 1);
    this.calcPrice = this.totalPrice(this.paymentTableData);
    this.total_sum();
  }

  public deleteAllPaymet() {
    this.paymentTableData = [];
    this.show_paymentExtra = false;
    this.addDispatcher.controls["payment_extra_price"].setValue("");
    this.calcPrice = 0;
    this.total_sum();
  }

  public getTariff() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: "desc",
      sortByColumn: "name",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._tariffService.getTariffs(data1).then(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.tariffData = data.getTariff;
        this.tariffDataTemp = data.getTariff.slice();
        this.defaultTariff = this.tariffData[this.tariffData.findIndex(x => x._id == '5a7c311fa9e5af79afebcc8f')]
      }
    });
  }

  toggle() {
    this.showmap = !this.showmap;
    this.showDirection();
  }

  togglebtn() {
    this.showmapbtn = !this.showmapbtn;
  }

  directionsChanged() {
    this.directionsResult = this.directionsRenderer.getDirections();
    this.cdr.detectChanges();
  }

  showDirection() {
    //alert(this.addDispatcher.value.drop_location);
    let geocoder = new google.maps.Geocoder();
    let sourceAddress = this.direction.origin;
    let d = this;
    if (
      this.addDispatcher.value.pickup_location !== "" &&
      !this.addDispatcher.value.drop_location && this.addDispatcher.value.drop_location !== ""
    ) {
      this.pickupMarkers = [];
      this.dropOffMarkers = [];
      let sourceAddress = this.direction.origin;
      let d = this;
      geocoder.geocode({ address: sourceAddress }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          this.sourceLatitude = results[0].geometry.location.lat();
          this.sourceLongitude = results[0].geometry.location.lng();
          d.addDispatcher.controls["sourceLatLang"].setValue(
            this.sourceLatitude + "," + this.sourceLongitude
          );
          d.positions.push([this.sourceLatitude, this.sourceLongitude]);
          d.pickupMarkers = [];
        }
      });
    } else if (
      this.addDispatcher.value.pickup_location !== "" &&
      this.addDispatcher.value.drop_location !== ""
    ) {
      this.directionsDisplay.setMap(this.map);
      this.directionsDisplay.setOptions({ suppressMarkers: true });
      this.pickupMarkers = [];
      this.dropOffMarkers = [];
      let sourceAddress = this.direction.origin;
      geocoder.geocode({ address: sourceAddress }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          d.sourceLatitude = results[0].geometry.location.lat();
          d.sourceLongitude = results[0].geometry.location.lng();
          d.addDispatcher.controls["sourceLatLang"].setValue(
            d.sourceLatitude + "," + d.sourceLongitude
          );
          d.positions.push([d.sourceLatitude, d.sourceLongitude]);
          d.pickupMarkers = [];
        }
      });
    }
    if (this.addDispatcher.value.drop_location !== "") {
      let destinationAddress = this.direction.destination;
      geocoder.geocode({ address: destinationAddress }, function (
        results,
        status
      ) {
        if (status == google.maps.GeocoderStatus.OK) {
          d.destinationLatitude = results[0].geometry.location.lat();
          d.destinationLongtitude = results[0].geometry.location.lng();
          d.addDispatcher.controls["destinationLatLang"].setValue(
            d.destinationLatitude + "," + d.destinationLongtitude
          );
          d.positions.push([
            d.destinationLatitude,
            d.destinationLongtitude
          ]);
          d.dropOffMarkers = [];
        }
      });

      let selectedMode = "DRIVING";
      let request = {
        origin: sourceAddress,
        destination: destinationAddress,
        travelMode: google.maps.TravelMode[selectedMode]
      };
      this.directionsService.route(request, function (response, status) {
        if (response.routes.length === 0) {
          d.toastr.error("Invalid path!");
          d.directionsDisplay.setMap(null);
          d.pickupMarkers = [];
          d.dropOffMarkers = [];
          d.addDispatcher.controls["distance"].setValue("");
          d.addDispatcher.controls["eta"].setValue("");
          d.directionsDisplay.setMap(null);
          d.addDispatcher.controls["pickup_location"].setValue("");
          d.addDispatcher.controls["drop_location"].setValue("");
          d.addDispatcher.controls["pickup_country"].setValue("");
          d.addDispatcher.controls["drop_country"].setValue("");
          d.pickupLat = "";
          d.pickupLang = "";
          d.dropOffLang = "";
          d.dropOffLat = "";
          for (let i = 0; i < d.picking.length; i++) {
            d.picking[i].setMap(null);
          }
          for (let i = 0; i < d.dropOffing.length; i++) {
            d.dropOffing[i].setMap(null);
          }
        } else {
          d.addDispatcher.controls["distance"].setValue(
            response.routes[0]["legs"][0]["distance"]["text"]
          );
          d.addDispatcher.controls["eta"].setValue(
            response.routes[0]["legs"][0]["duration"]["text"]
          );
          d.directionsDisplay.setDirections(response);

          d.makePickupMarker(
            response.routes[0]["legs"][0].start_location,
            "title",
            "pickup"
          );
          d.makeDropOffMarker(
            response.routes[0]["legs"][0].end_location,
            "title",
            "dropOff"
          );
        }
      });
    }

    if (this.flag) {
    }
  }

  makePickupMarker(position, title, type) {
    this.inPick = true;
    let marker1 = new google.maps.Marker({
      position: position,
      map: this.map,
      icon: "assets/img/marker-red.png",
      title: "Pickup Location"
    });
    this.picking.push(marker1);
  }

  makeDropOffMarker(position, title, type) {
    this.inDrop = true;
    let marker2 = new google.maps.Marker({
      position: position,
      map: this.map,
      icon: "assets/img/marker-customer.png",
      title: "Drop Location"
    });
    this.dropOffing.push(marker2);
  }
  /**
   * Get Order Group
   */
  public getOrderGroup() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: "desc",
      sortByColumn: "_id",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderGroupService.get(data1).then(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.OrderGroup = data.OrderGroup;
      }
    });
  }

  /**
   * Delete Customer Address
   */
  DeleteAddressDialog(address): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: "400px",
      data: { text: "Are you sure you want to remove this address" }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteAddress(address);
      } else {
      }
    });
  }

  public deleteAddress(address) {
    let params = {
      company_id: this.company_id_array,
      customer_address_id: address._id
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._customerService.deleteAddress(data1).then(data => {
      if (data && data.status == 200) {
        let params = {
          company_id: this.company_id_array,
          _id: address.customer_id
        }
        let enc_data1 = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data2 = {
          data: enc_data1,
          email: this.useremail
        }
        this._customerService
          .getCustomerAddrress(data2)
          .then(data => {
            if (data && data.status == 200) {
              data = this._EncDecService.dwt(this.session_token, data.data);
              this.SelectedCustomerAddress = data.result;
            }
          });
      }
    });
  }

  /**
   * Reset Dispatcher form
   */

  resetDispatcherForm() {
    this.rejectedDriverList = [];
    (<HTMLInputElement>document.getElementById("schedulerdis")).style.display =
      "block";
    this.copyOrderFlag = false;
    this.tbEndFlag = false;
    this.customerEditForm.dialCode = '971';
    this.modelCtrl.enable();
    this.getTariff();
    this.modelFlag = true;
    this.removableModel = true;
    this.modelChips = [];
    this.orderUpdateFlag = false;
    this.showCustomerInfo = false;
    this.showpaymenterror = false;
    this.addDispatcher.reset();
    this.showLoader = false;
    this.st = true;
    this.show = false;
    this.isReset = true;
    this.show_paymentExtra = false;
    this.directionsDisplay.setMap(null);
    this.addDispatcher.controls['pickup_location'].setValue('');
    this.addDispatcher.controls['drop_location'].setValue('');
    this.addDispatcher.value.driver_groups = '';
    this.addDispatcher.value.driver_group_mode = '';
    this.pickupMarkers = [];
    this.dropOffMarkers = [];
    this.pickupLat = "";
    this.pickupLang = "";
    this.dropOffLang = "";
    this.dropOffLat = " ";
    this.paymentTableData = [];
    this.schedulerStaus = false;
    this.modelChips = [];
    this.automaticorder = true;
    for (let i = 0; i < this.picking.length; i++) {
      this.picking[i].setMap(null);
    }
    for (let i = 0; i < this.dropOffing.length; i++) {
      this.dropOffing[i].setMap(null);
    }
    this.addDispatcher.controls["vehicleList"].setValidators([]);
    this.addDispatcher.controls["driverList"].setValidators([]);
    this.addDispatcher.controls["vehicleList"].updateValueAndValidity();
    this.addDispatcher.controls["driverList"].updateValueAndValidity();
    this.addDispatcher.controls["paymentExtraList"].setValue("");
    this.addDispatcher.controls["payment_extra_price"].setValue("");
    this.modelChips = [];
    this.total_sum();
    this.getDriverGroupsForListing();
    this.max4 = new Date();
    this.editCustomer = false;
    this.addDispatcher.get("customerCountryCode").setValue('971');
  }

  /**
   Save Booking Function
   */
  public orderCount = 0;
  public orderCountFlag = true;
  public initCount;
  public orderCallCount = 1;
  public rejectedDriverList = [];
  public findInvalidControls() {
    const invalid = [];
    const controls = this.addDispatcher.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }
  saveOrderBooking() {
    if (this.copyOrderFlag)
      this.copyOrderFlag = false;
    if (this.addDispatcher.status == "INVALID") {
      this.toastr.error("Please fill all fields");
      return;
    }
    if (this.addDispatcher.value.customerCountryCode) {
      var regex = /^(\+?\d{1,3}|\d{1,4})$/gm;
      if (this.addDispatcher.value.customerCountryCode.code && !regex.test(this.addDispatcher.value.customerCountryCode.code)) {
        this.toastr.error("Invalid country code");
        return;
      }
      else if (!this.addDispatcher.value.customerCountryCode.code && !regex.test(this.addDispatcher.value.customerCountryCode)) {
        this.toastr.error("Invalid country code");
        return;
      }
      if (!this.addDispatcher.value.customerCountryCode.code) {
        this.addDispatcher.value.customerCountryCode = this.addDispatcher.value.customerCountryCode.substr(0, 1) == '+' ? this.addDispatcher.value.customerCountryCode.substr(1) : this.addDispatcher.value.customerCountryCode;
      }
    }
    // if (this.addDispatcher.value.partnerList !== "" && (this.addDispatcher.value.payment_type=="" || this.addDispatcher.value.payment_type==null)){
    //   this.toastr.error("Please select payment type");
    //   return;
    // }
    var allow_order = this.checkOrderIsAllowed();
    if (allow_order == true) {
      this.showLoader = true;
      if (this.addDispatcher.value.customerPhoneList == "" || !this.addDispatcher.value.customerPhoneList)
        this.phone_number = '042080793';
      else if (this.addDispatcher.value.customerPhoneList.phone_number) {
        this.phone_number = this.addDispatcher.value.customerPhoneList.phone_number;
      } else {
        const phone = this.addDispatcher.value.customerPhoneList ? this.addDispatcher.value.customerPhoneList : '042080793';
        var item = phone.slice(0);// Removes 0 prefix
        if (item[0] == 0) {//
          this.phone_number = phone.substr(1);//
        }
        else
          this.phone_number = phone;
      }
      if (this.addDispatcher.value.customerNameList !== ('' || null) && this.addDispatcher.value.customerNameList) {
        if (this.addDispatcher.value.customerNameList.firstname)
          this.name = this.addDispatcher.value.customerNameList.firstname + " " + this.addDispatcher.value.customerNameList.lastname;
        else if (this.addDispatcher.value.customerNameList.firstname == '') {
          this.toastr.error('Customer name must not be empty');
          this.formErrors.customerNameList = 'Customer name must not be empty';
          this.showLoader = false;
          return;
        }
        else
          this.name = this.addDispatcher.value.customerNameList;
      }
      if (this.name && this.name !== '') {
        const split_name = this.name.split(" ");
        if (split_name.length > 1) {
          this.firstname = split_name[0];
          this.lastname = split_name[split_name.length - 1];
        } else {
          this.firstname = split_name[0];
          this.lastname = "";
        }
      }
      let multiDispatch = parseInt(this.addDispatcher.value.NumberOfCars) > 1;
      if (!multiDispatch && this.orderCountFlag) {//this.orderCountFlag checks if it is a multiple order with orderder to dispatch
        this.dispatchOrder();
        return;
      }
      if (this.orderCountFlag) {
        this.initCount = parseInt(this.addDispatcher.value.NumberOfCars);
      }
      let pickup_location = this.pickup_location;
      let pickup_place_id = this.pickup_place_id;
      var model = [];
      if (this.modelChips.length > 0) {
        for (var i = 0; i < this.modelChips.length; i++)
          model.push(this.modelChips[i]._id);
      }
      let enc_data = this._EncDecService.nwt(this.session_token, { status: '5', company_id: window.localStorage['user_company'] });
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      return this._devicelocationService.getFilteredData(data1).then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          if (this.rejectedDriverList.length !== null) {
            var i = data.driversArr.length;
            while (i--) {
              let index = model.indexOf(
                data.driversArr[i].vehicle_id.vehicle_model_id
              );
              if (index === -1)
                this.rejectedDriverList.push(data.driversArr[i]._id);
              let index1 = this.rejectedDriverList.indexOf(
                data.driversArr[i]._id
              );
              if (index1 !== -1) {
                if (data.driversArr[i].device_id) {
                  let device_id = data.driversArr[i].device_id._id;
                  data.driversArr.splice(i, 1);
                  let index2 = data.getdeviceloc.findIndex(
                    x => x.device_id == device_id
                  );
                  if (index2 !== -1) {
                    data.getdeviceloc.splice(index2, 1);
                  }
                }
                else {
                  this.rejectedDriverList.push(data.driversArr[i]._id);
                }
              }
            }
          }
          let deviceLocationData = data.getdeviceloc;
          let driverData = data.driversArr;
          this.driversForMultiOrder(
            parseInt(this.addDispatcher.value.NumberOfCars),
            deviceLocationData,
            pickup_place_id
          ).then(results => {
            var dispatchOrders = [];
            let radius =
              this.addDispatcher.value.distance_radius !== ""
                ? this.addDispatcher.value.distance_radius
                : 15000;
            $.each(results, (index, device) => {
              if (parseInt(device.distance) > parseInt(radius)) {
                this.rejectedDriverList.push(device.device.driver_id);
                return false;
              }
              let driverSearch = $.grep(driverData, driver => {
                if (driver.device_id)
                  return driver.device_id._id === device.device.device_id;
              });
              if (!driverSearch[0]) {
                return;
              }
              let driver = driverSearch[0];
              let orderData = this.orderDataForDriver(
                driver,
                device.distance,
                pickup_place_id,
                pickup_location
              );
              dispatchOrders.push(
                new Promise((resolve, reject) => {
                  let enc_data = this._EncDecService.nwt(this.session_token, orderData);
                  //alert(this.dispatcher_id.email)
                  let data1 = {
                    data: enc_data,
                    email: this.useremail
                  }
                  this._orderService
                    .saveOrder(data1, false)
                    .then(res => {
                      if (res) {
                        if (res.status === 201 || res.status === 202) {
                          //res = this._EncDecService.dwt(this.session_token, res.data);
                          this.rejectedDriverList.push(orderData.driver_id);
                        }
                        else if (res.status == 200) {
                          res = this._EncDecService.dwt(this.session_token, res.data);
                          if (this.orders.length > 10) {
                            this.orders.splice(-1, 1);
                          } else {
                            this.orders.splice(-1, 0);
                          }
                          this.orders.unshift(res.order);
                          this.ordersLength = this.orders.length;
                          this.orderCount++;
                        }
                        resolve(res);
                      }
                      else {
                        resolve(res);
                      }
                    })
                    .catch(error => {
                      console.log(error);
                      resolve();
                    });
                })
              );
            });
            this.toastr.success("Dispatching orders to closest drivers...");
            Promise.all(dispatchOrders).then(results => {
              if (this.orderCallCount > 2) {
                this.toastr.info(
                  "Couldnt find enough drivers, Total dispathed order = " +
                  this.orderCount
                );
                this.resetScheduler();
              } else if (this.initCount != this.orderCount) {
                this.addDispatcher.controls["NumberOfCars"].setValue(
                  this.initCount - this.orderCount
                );
                this.orderCallCount++;
                this.orderCountFlag = false;
                this.toastr.warning(
                  "Not enough free drivers dispatching again"
                );
                this.Wait().then(() => {
                  this.saveOrderBooking();
                });
              } else {
                this.toastr.success("Order is successfully created.");
                this.resetScheduler();
                let order_exist;
                results.forEach(response_order => {
                  order_exist = false;
                  if (response_order && response_order.status == 200) {
                    this.orders.forEach(order => {
                      if (order._id === response_order.order._id) {
                        order_exist = true;
                      }
                      if (!order_exist) {
                        if (this.orders.length > 10) {
                          this.orders.splice(-1, 1);
                        } else {
                          this.orders.splice(-1, 0);
                        }
                        this.orders.unshift(response_order.order);
                        this.ordersLength = this.orders.length;
                        this.orderSearchSubmit = false;
                      }
                    })
                  }
                });
              }
            });
          });
        }
      });
    }
    // else {
    //   this.toastr.error("Trips starting outside coverage area not allowed");
    // }
    //this.getAllorderCount();
  }

  public driversForMultiOrder(driverCount, deviceLocationData, pickupPlaceId) {
    this.toastr.success("Locating drivers...");
    /*var batchPromises2 = [];
    let closestDevices=[];
    return this.getMyDrivers(deviceLocationData,batchPromises2,pickupPlaceId,0,99).then(batchPromises=>{
    this.toastr.success('Finding closest ' + driverCount + ' drivers...');
    var deviceDistances = [];
    return Promise.all(batchPromises[Symbol.iterator]).then(results => {
      console.log(results)
      $.each(results, (index, batch) => {
          $.each(batch, (index, distanceElement) => {
          if(distanceElement.elements[0].status !== 'OK') { return }
          deviceDistances.push({ distance: distanceElement.elements[0].distance.value, device: deviceLocationData[index] });
          });
      });
      deviceDistances.sort((a, b) => { return a.distance - b.distance });
      closestDevices = deviceDistances.slice(0, driverCount);
      return closestDevices;
    });});*/
    var batchPromises = [];
    let chunk = 10;
    for (var i = 0; i < 99 && i < deviceLocationData.length; i += chunk) {
      let batch = deviceLocationData.slice(i, i + chunk);
      batchPromises.push(this.distanceMatrixForDevices(batch, pickupPlaceId));
    }
    this.toastr.success("Finding closest " + driverCount + " drivers...");
    return Promise.all(batchPromises).then(results => {
      var deviceDistances = [];
      let j = 0;
      $.each(results, (index, batch) => {
        $.each(batch, (index, distanceElement) => {
          if (distanceElement.elements[0].status !== "OK") {
            j++;
            return;
          }
          deviceDistances.push({
            distance: distanceElement.elements[0].distance.value,
            device: deviceLocationData[j]
          });
          j++;
        });
      });
      deviceDistances.sort((a, b) => {
        return a.distance - b.distance;
      });
      let closestDevices = deviceDistances.slice(0, driverCount);
      let i = deviceDistances.length;
      while (i--) {
        let index2 = closestDevices.indexOf(deviceDistances[i]);
        if (index2 === -1) {
          this.rejectedDriverList.push(deviceDistances[i].device.driver_id);
        }
      }
      return closestDevices;
    });
  }
  /* getMyDrivers(deviceLocationData,batchPromises,pickupPlaceId,start,end){
     let chunk = 10;
     let batchPromises2=batchPromises;
     for (var i = start;i<end; i += chunk) {
       if(i>=deviceLocationData.length)
       {
         return new Promise((resolve,reject)=>{resolve(batchPromises2)});
       }
       else{
       let batch = deviceLocationData.slice(i, i + chunk);
       batchPromises2.push(this.distanceMatrixForDevices(batch, pickupPlaceId));
       }
     }
     if(i>=end-10){
     let chain = Promise.resolve();
     chain.then(this.Wait).then(()=>{
       return  this.getMyDrivers(deviceLocationData,batchPromises2, pickupPlaceId,start+99,end+99);});
     }
   }*/
  Wait() {
    return new Promise(r => setTimeout(r, 7000));
  }
  Wait2() {
    return new Promise(r => setTimeout(r, 200));
  }
  Wait3(i) {
    return new Promise(r => setTimeout(r, i * 30));
  }

  distanceMatrixForDevices(originDevices: Array<any>, destination: string) {
    let distanceMatrix = new google.maps.DistanceMatrixService();
    let origins = $.map(originDevices, function (device) {
      return new google.maps.LatLng(device.latitude, device.longitude);
    });
    return new Promise((resolve, reject) => {
      distanceMatrix.getDistanceMatrix(
        {
          origins: origins,
          destinations: [
            new google.maps.LatLng(this.pickupLat, this.pickupLang)
          ],
          travelMode: google.maps.TravelMode.DRIVING
        },
        function (response, status) {
          if (status !== google.maps.DistanceMatrixStatus.OK) {
            return reject(google.maps.DistanceMatrixStatus);
          }
          resolve(response.rows);
        }
      );
    });
  }

  orderDataForDriver(
    driver,
    distance,
    pickupLocationId,
    pickupLocationCoordinates
  ) {
    /*if(this.addDispatcher.value.scheduler_start_time!='') {
      var scheduler_start_time = moment(this.addDispatcher.value.scheduler_start_time).format('HH:mm:ss');
      scheduler_start_time = scheduler_start_time.toString();
    } else {
        var scheduler_start_time='';
    }*/

    var model = [];
    if (this.modelChips.length > 0) {
      for (var i = 0; i < this.modelChips.length; i++)
        model.push(this.modelChips[i]._id);
    } else {
      this.toastr.error("Plase select a vehicle model");
      this.showLoader = false;
      return;
    }
    var orderData = {
      phone_number: this.phone_number,
      firstname: this.firstname ? this.firstname : "3c",
      lastname: this.lastname ? this.lastname : "",
      dispatcher_id: this.dispatcher,
      scheduler_start_date: this.addDispatcher.value.scheduler_start_date
        ? moment(this.addDispatcher.value.scheduler_start_date).format(
          "YYYY-MM-DD"
        ) +
        " " +
        moment(this.addDispatcher.value.scheduler_start_time).format(
          "HH:mm:ss"
        )
        : "",
      //'scheduler_start_time': this.addDispatcher.value.scheduler_start_time?moment(this.addDispatcher.value.scheduler_start_time).format('HH:mm:ss'):'',
      schedule_before: this.addDispatcher.value.schedule_before,
      is_scheduler: this.addDispatcher.value.scheduler_start_date ? 1 : 0,
      pickup_location: this.addDispatcher.value.pickup_location
        ? this.addDispatcher.value.pickup_location
        : "",
      distance: distance || "",
      drop_location: "",
      partner_id: this.addDispatcher.value.partnerList
        ? this.addDispatcher.value.partnerList._id
        : "",
      vehicle_id: driver.vehicle_id._id || "",
      driver_id: driver._id || "",
      tariff_id: this.addDispatcher.value.tarrifList
        ? this.addDispatcher.value.tarrifList._id
        : "",
      additional_service_id: this.addDispatcher.value.additionalList
        ? this.addDispatcher.value.additionalList._id
        : "",
      payment_extra_id: this.payment_extra_ids,
      price: this.addDispatcher.value.total_sum
        ? this.addDispatcher.value.total_sum
        : "",
      payment_type_id: this.payment_type.value ? this.payment_type.value : "",
      order_group_id: this.addDispatcher.value.OrderGroup
        ? this.addDispatcher.value.OrderGroup._id
        : "",
      message: this.addDispatcher.value.message
        ? this.addDispatcher.value.message
        : "",
      customer_drop_lat_long: "",
      customer_pickup_lat_long: this.pickupLat + "," + this.pickupLang || "",
      customer_email: this.addDispatcher.value.customer_email,
      work_phone: this.addDispatcher.value.work_phone,
      company: this.addDispatcher.value.company,
      flight_no: this.addDispatcher.value.flight_no,
      eta: this.addDispatcher.value.eta ? this.addDispatcher.value.eta : "",
      vehicle_model_ids: model.length > 0 ? model : "",
      payment_type_identifier: this.addDispatcher.value.payment_type ? this.addDispatcher.value.payment_type : '',
      company_id: window.localStorage['user_company'],
      country_code: this.addDispatcher.value.customerCountryCode ? this.addDispatcher.value.customerCountryCode.code ? this.addDispatcher.value.customerCountryCode.code : this.addDispatcher.value.customerCountryCode : '971',
      isHourlyOrder: this.addDispatcher.value.tbEndTime ? 1 : 0,
      hourlyend: this.addDispatcher.value.tbEndTime ? moment(this.addDispatcher.value.tbEndTime).format("YYYY-MM-DD HH:mm:ss") : '',
    };
    return orderData;
  }

  resetScheduler() {
    this.modelCtrl.enable();
    this.getTariff();
    this.modelFlag = true;
    this.removableModel = true;
    this.modelChips = [];
    this.orderCount = 0;
    this.orderCountFlag = true;
    this.initCount = 0;
    this.orderCallCount = 1;
    this.rejectedDriverList = [];
    this.showLoader = false;
    this.st = true;
    this.showpaymenterror = false;
    this.showCustomerInfo = false;
    this.addDispatcher.reset();
    this.show = false;
    this.directionsDisplay.setMap(null);
    this.addDispatcher.controls["pickup_location"].setValue("");
    this.addDispatcher.controls["drop_location"].setValue("");
    this.pickupMarkers = [];
    this.dropOffMarkers = [];
    this.pickupLat = "";
    this.pickupLang = "";
    this.dropOffLang = "";
    this.dropOffLat = "";
    this.paymentTableData = [];
    for (let i = 0; i < this.picking.length; i++) {
      this.picking[i].setMap(null);
    }
    for (let i = 0; i < this.dropOffing.length; i++) {
      this.dropOffing[i].setMap(null);
    }
    this.addDispatcher.controls["vehicleList"].setValidators([]);
    this.addDispatcher.controls["driverList"].setValidators([]);
    this.addDispatcher.controls["vehicleList"].updateValueAndValidity();
    this.addDispatcher.controls["driverList"].updateValueAndValidity();
    this.show_paymentExtra = false;
    this.addDispatcher.controls["paymentExtraList"].setValue("");
    this.addDispatcher.controls["payment_extra_price"].setValue("");
    this.total_sum();
  }
  binaryCombos(n) {
    let condition = Math.pow(2, n);
    let bit;
    let combination_array = [];
    for (var i = 0; i < condition; ++i) {
      let j = i.toString();
      bit = parseInt(j, 10).toString(2);
      if (bit.length < n) {
        let size = n - bit.length;
        for (var k = 0; k < size; k++) {
          bit = "0" + bit;
        }
        combination_array.push(bit);
      } else {
        combination_array.push(bit);
      }
    }
    return combination_array;
  }
  dispatchOrder() {
    /*if(this.addDispatcher.value.scheduler_start_time!='') {
      var scheduler_start_time = moment(this.addDispatcher.value.scheduler_start_time).format('HH:mm:ss');
      scheduler_start_time = scheduler_start_time.toString();
    } else {
        var scheduler_start_time='';
    }*/
    var model = [];
    if (this.modelChips.length > 0) {
      for (var i = 0; i < this.modelChips.length; i++)
        model.push(this.modelChips[i]._id);
    } else {
      this.toastr.error("Plase select a vehicle model");
      this.showLoader = false;
      return;
    }
    if (this.orderTypeFlag) {
      let t3 = [];
      let PositionArray = [];
      this.driver_groups_id = "";
      if (this.addDispatcher.value.driver_groups && this.addDispatcher.value.driver_groups.length == 1) {
        for (var i = 0; i < this.tempdriverGroupData.length; ++i) {
          for (var w = 0; w < this.addDispatcher.value.driver_groups.length; ++w) {
            if (this.tempdriverGroupData[i]._id === this.addDispatcher.value.driver_groups[w]) {
              PositionArray.push(i);
            }
          }
          if (i === this.tempdriverGroupData.length - 1) {
            for (let j = this.tempdriverGroupData.length - 1; j >= 0; j--) {
              let index = PositionArray.indexOf(j)
              if (index !== -1)
                this.driver_groups_id = this.driver_groups_id + "1";
              else
                this.driver_groups_id = this.driver_groups_id + "0";
            }
          }
        }
        let temp_ids = this.driver_groups_id;
        temp_ids = temp_ids.replace(/^0+/, '');
        let temp_before_id = temp_ids;
        t3.push(temp_ids);
        if (this.tempdriverGroupData.length > temp_ids.length) {
          for (let j = 0; j < this.tempdriverGroupData.length - temp_ids.length; j++) {
            temp_before_id = "0" + temp_before_id;
            t3.push(temp_before_id);
          }
        }
      }
      else if (this.addDispatcher.value.driver_groups && this.addDispatcher.value.driver_groups.length > 1) {
        if (this.addDispatcher.value.driver_group_mode == "O") {
          // Matching Only
          for (var i = 0; i < this.tempdriverGroupData.length; ++i) {
            for (var w = 0; w < this.addDispatcher.value.driver_groups.length; ++w) {
              if (this.tempdriverGroupData[i]._id === this.addDispatcher.value.driver_groups[w]) {
                PositionArray.push(i);
              }
            }
            if (i === this.tempdriverGroupData.length - 1) {
              for (let j = this.tempdriverGroupData.length - 1; j >= 0; j--) {
                let index = PositionArray.indexOf(j)
                if (index !== -1)
                  this.driver_groups_id = this.driver_groups_id + "1";
                else
                  this.driver_groups_id = this.driver_groups_id + "0";
              }
            }
          }
          let temp_ids = this.driver_groups_id;
          temp_ids = temp_ids.replace(/^0+/, '');
          let temp_before_id = temp_ids;
          t3.push(temp_ids);
          if (this.tempdriverGroupData.length > temp_ids.length) {
            for (let j = 0; j < this.tempdriverGroupData.length - temp_ids.length; j++) {
              temp_before_id = "0" + temp_before_id;
              t3.push(temp_before_id);
            }
          }
        } else {
          // Matching All
          for (var i = 0; i < this.tempdriverGroupData.length; ++i) {
            for (var w = 0; w < this.addDispatcher.value.driver_groups.length; ++w) {
              if (this.tempdriverGroupData[i]._id === this.addDispatcher.value.driver_groups[w]) {
                PositionArray.push(i);
              }
            }
            if (i === this.tempdriverGroupData.length - 1) {
              for (let j = this.tempdriverGroupData.length - 1; j >= 0; j--) {
                let index = PositionArray.indexOf(j)
                if (index !== -1)
                  this.driver_groups_id = this.driver_groups_id + "1";
                else
                  this.driver_groups_id = this.driver_groups_id + "0";
              }
            }
          }
          let temp_ids;
          let temp_array = [];
          temp_ids = this.driver_groups_id;
          temp_ids = temp_ids.replace(/^0+/, '');
          let temp_before_id_zero = temp_ids;
          let temp_before_id_ones = temp_ids;
          t3.push(temp_ids);
          let combination_array = this.binaryCombos(temp_ids.length);
          let bit;
          let temp_ids_bit;
          temp_ids_bit = parseInt(temp_ids, 2);
          for (let i = 0; i < combination_array.length; i++) {
            let j = parseInt(combination_array[i], 2)
            if (j & temp_ids_bit) {
              temp_array.push(combination_array[i]);
              // pushing all combination for trimmed case
              t3.push(combination_array[i]);
            }
          }
          // creating matching cases for all remaining
          for (var w = 0; w < temp_array.length; ++w) {
            if (this.tempdriverGroupData.length > temp_array[w].length) {
              let combination_array = this.binaryCombos(this.tempdriverGroupData.length - temp_array[w].length);
              for (var k = 0; k < combination_array.length; ++k) {
                t3.push(combination_array[k] + temp_array[w]);
              }
            }
          }
        }
      }
      else {
        t3 = [];
      }
      this.orderdata = {
        phone_number: this.phone_number,
        firstname: this.firstname ? this.firstname : "3c",
        lastname: this.lastname ? this.lastname : "",
        dispatcher_id: this.dispatcher,
        scheduler_start_date: this.addDispatcher.value.scheduler_start_date
          ? moment(this.addDispatcher.value.scheduler_start_date).format(
            "YYYY-MM-DD"
          ) +
          " " +
          moment(this.addDispatcher.value.scheduler_start_time).format(
            "HH:mm:ss"
          )
          : "",
        //'scheduler_start_time': this.addDispatcher.value.scheduler_start_time?moment(this.addDispatcher.value.scheduler_start_time).format('HH:mm:ss'):'',
        'schedule_before': this.addDispatcher.value.schedule_before,
        'is_scheduler': this.addDispatcher.value.scheduler_start_date ? 1 : 0,
        'pickup_location': this.addDispatcher.value.pickup_location ? this.addDispatcher.value.pickup_location : '',
        'distance': this.addDispatcher.value.distance ? this.addDispatcher.value.distance : '',
        'drop_location': this.addDispatcher.value.drop_location ? this.addDispatcher.value.drop_location : '',
        'partner_id': this.addDispatcher.value.partnerList ? this.addDispatcher.value.partnerList._id : '',
        'vehicle_id': '',
        'driver_id': '',
        'tariff_id': this.addDispatcher.value.tarrifList ? this.addDispatcher.value.tarrifList._id : '',
        'additional_service_id': this.addDispatcher.value.additionalList ? this.addDispatcher.value.additionalList._id : '',
        'payment_extra_id': this.payment_extra_ids,
        'order_type': this.orderTypeFlag,
        'price': this.addDispatcher.value.total_sum ? this.addDispatcher.value.total_sum : '',
        'payment_type_id': this.payment_type.value ? this.payment_type.value : '',
        'order_group_id': this.addDispatcher.value.OrderGroup ? this.addDispatcher.value.OrderGroup._id : '',
        'message': this.addDispatcher.value.message ? this.addDispatcher.value.message : '',
        'customer_drop_lat_long': this.addDispatcher.value.destinationLatLang ? this.addDispatcher.value.destinationLatLang : '',
        'customer_pickup_lat_long': this.addDispatcher.value.sourceLatLang ? this.addDispatcher.value.sourceLatLang : '',
        'customer_email': this.addDispatcher.value.customer_email,
        'work_phone': this.addDispatcher.value.work_phone,
        'company': this.addDispatcher.value.company,
        'flight_no': this.addDispatcher.value.flight_no,
        'company_id': window.localStorage['user_company'],
        'eta': this.addDispatcher.value.eta ? this.addDispatcher.value.eta : '',
        'vehicle_model_ids': model.length > 0 ? model : '',
        'driver_groups': this.addDispatcher.value.driver_groups ? this.addDispatcher.value.driver_groups : [],
        't3': t3,
        'payment_type_identifier': this.addDispatcher.value.payment_type ? this.addDispatcher.value.payment_type : '',
        'distance_radius': this.addDispatcher.value.distance_radius ? this.addDispatcher.value.distance_radius : '',
        't5': this.addDispatcher.value.credit_card ? this.addDispatcher.value.credit_card : '',
        'country_code': this.addDispatcher.value.customerCountryCode ? this.addDispatcher.value.customerCountryCode.code : '971',
        'isHourlyOrder': this.addDispatcher.value.tbEndTime ? 1 : 0,
        'hourlyend': this.addDispatcher.value.tbEndTime ? moment(this.addDispatcher.value.tbEndTime).format("YYYY-MM-DD HH:mm:ss"
        ) : ''
      };
    } else {
      this.orderdata = {
        phone_number: this.phone_number,
        firstname: this.firstname ? this.firstname : "3c",
        lastname: this.lastname ? this.lastname : "",
        dispatcher_id: this.dispatcher,
        scheduler_start_date: this.addDispatcher.value.scheduler_start_date
          ? moment(this.addDispatcher.value.scheduler_start_date).format(
            "YYYY-MM-DD"
          ) +
          " " +
          moment(this.addDispatcher.value.scheduler_start_time).format(
            "HH:mm:ss"
          )
          : "",
        //'scheduler_start_time': this.addDispatcher.value.scheduler_start_time?moment(this.addDispatcher.value.scheduler_start_time).format('HH:mm:ss'):'',
        schedule_before: this.addDispatcher.value.schedule_before,
        is_scheduler: this.addDispatcher.value.scheduler_start_date ? 1 : 0,
        pickup_location: this.addDispatcher.value.pickup_location
          ? this.addDispatcher.value.pickup_location
          : "",
        distance: this.addDispatcher.value
          ? this.addDispatcher.value.distance
          : "",
        drop_location: this.addDispatcher.value.drop_location
          ? this.addDispatcher.value.drop_location
          : "",
        partner_id: this.addDispatcher.value.partnerList
          ? this.addDispatcher.value.partnerList._id
          : "",
        vehicle_id: this.addDispatcher.value.vehicleList
          ? this.addDispatcher.value.vehicleList._id
          : "",
        driver_id: this.addDispatcher.value.driverList
          ? this.addDispatcher.value.driverList._id
          : "",
        tariff_id: this.addDispatcher.value.tarrifList
          ? this.addDispatcher.value.tarrifList._id
          : "",
        additional_service_id: this.addDispatcher.value.additionalList
          ? this.addDispatcher.value.additionalList._id
          : "",
        payment_extra_id: this.payment_extra_ids,
        order_type: this.orderTypeFlag,
        price: this.addDispatcher.value.total_sum
          ? this.addDispatcher.value.total_sum
          : "",
        payment_type_id: this.payment_type.value ? this.payment_type.value : "",
        order_group_id: this.addDispatcher.value.OrderGroup
          ? this.addDispatcher.value.OrderGroup._id
          : "",
        message: this.addDispatcher.value.message
          ? this.addDispatcher.value.message
          : "",
        customer_drop_lat_long: this.addDispatcher.value.destinationLatLang
          ? this.addDispatcher.value.destinationLatLang
          : "",
        customer_pickup_lat_long: this.addDispatcher.value.sourceLatLang
          ? this.addDispatcher.value.sourceLatLang
          : "",
        customer_email: this.addDispatcher.value.customer_email,
        work_phone: this.addDispatcher.value.work_phone,
        company: this.addDispatcher.value.company,
        flight_no: this.addDispatcher.value.flight_no,
        eta: this.addDispatcher.value.eta ? this.addDispatcher.value.eta : "",
        vehicle_model_ids: model.length > 0 ? model : "",
        payment_type_identifier: this.addDispatcher.value.payment_type ? this.addDispatcher.value.payment_type : '',
        company_id: window.localStorage['user_company'],
        country_code: this.addDispatcher.value.customerCountryCode ? this.addDispatcher.value.customerCountryCode.code ? this.addDispatcher.value.customerCountryCode.code : this.addDispatcher.value.customerCountryCode : '971',
        isHourlyOrder: this.addDispatcher.value.tbEndTime ? 1 : 0,
        hourlyend: this.addDispatcher.value.tbEndTime ? moment(this.addDispatcher.value.tbEndTime).format("YYYY-MM-DD HH:mm:ss"
        ) : '',
      };
    }
    let enc_data = this._EncDecService.nwt(this.session_token, this.orderdata);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService
      .saveOrder(data1, this.orderTypeFlag)
      .then(res => {
        if (res) {
          if (res.status === 201) {
            this.toastr.error("Order creation failed!!");
          } else if (res.status === 202) {
            this.toastr.error("Driver has order!!");
            this.showLoader = false;
          } else if (res.status == 200) {
            res = this._EncDecService.dwt(this.session_token, res.data);
            this.resetDispatcherForm();
            this.modelChips = [];
            this.showLoader = false;
            this.st = true;
            this.toastr.success("Order is successfully created.");
            this.showpaymenterror = false;
            this.showCustomerInfo = false;
            this.addDispatcher.reset();
            this.show = false;
            this.directionsDisplay.setMap(null);
            this.addDispatcher.controls["pickup_location"].setValue("");
            this.addDispatcher.controls["drop_location"].setValue("");
            this.pickupMarkers = [];
            this.dropOffMarkers = [];
            this.pickupLat = "";
            this.pickupLang = "";
            this.dropOffLang = "";
            this.dropOffLat = "";
            this.paymentTableData = [];
            for (let i = 0; i < this.picking.length; i++) {
              this.picking[i].setMap(null);
            }
            for (let i = 0; i < this.dropOffing.length; i++) {
              this.dropOffing[i].setMap(null);
            }
            this.addDispatcher.controls["vehicleList"].setValidators([]);
            this.addDispatcher.controls["driverList"].setValidators([]);
            this.addDispatcher.controls["vehicleList"].updateValueAndValidity();
            this.addDispatcher.controls["driverList"].updateValueAndValidity();
            this.show_paymentExtra = false;
            this.addDispatcher.controls["paymentExtraList"].setValue("");
            this.addDispatcher.controls["payment_extra_price"].setValue("");
            this.total_sum();
            if (this.orders.length > 10) {
              this.orders.splice(-1, 1);
            } else {
              this.orders.splice(-1, 0);
            }
            this.orders.unshift(res.order);
            this.ordersLength = this.orders.length;
          }
          else {
            this.toastr.error(res.message);
          }
        }
        this.showLoader = false;
      })
  }

  public swap_address(event) {
    event.preventDefault();
    if (
      this.addDispatcher.value.pickup_location !== " " &&
      this.addDispatcher.value.drop_location !== ""
    ) {
      let temp = this.addDispatcher.value.pickup_location;
      this.addDispatcher.controls["pickup_location"].setValue(
        this.addDispatcher.value.drop_location
      );
      this.addDispatcher.controls["drop_location"].setValue(temp);
      temp = this.addDispatcher.value.pickup_country;
      this.addDispatcher.controls["pickup_country"].setValue(
        this.addDispatcher.value.drop_country
      );
      this.addDispatcher.controls["drop_country"].setValue(temp);
      temp = this.place1;
      this.dropChanged(temp);
      this.pickupChanged(this.place2);
    }
  }
  public sendMessage(section = null) {
    this.disableSendbtn = true;
    const user = this._jwtService.getDispatcherUser();
    const msgData = {
      message: this.addMessage.value.message,
      send_to: this.addMessage.value.driver_id,
      send_by: user._id,
      status: "",
      validity: "",
      popup: "",
      sender_role: "dispatcher",
      receiver_role: "driver",
      message_type: "single",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, msgData);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService
      .sendmessage(data1)
      .then(res => {
        if (res && res.status == 200) {
          this.disableSendbtn = false;
          this.addMessage.reset();
          if (section == null) {
            $("#messageModal").modal("hide");
            this.toastr.success("Order message sent successfully.");
          }
        }
      })
      .catch(error => {
        console.log("error", error);
      });
  }
  /*
   * Reset for dispatcher Order Search
   *
   */
  refresh() {
    this.filter_name = ""
    this.user_name = "";
    this.user_phone = "";
    this.guest_user_phone = '';
    this.guest_user_id = '';
    this.tariff_id = '';
    this.order_limit = '';
    this.driver_id = '';
    this.driver_id2 = '';
    this.vehicle_id2 = '';
    this._id = '';
    this.unique_order_id = [];
    this.ordersFilter = [];
    this.sort_by_coloumn = "";
    this.sort_type = "";
    this.user_id = "";
    this.selectedMoment = new Date(Date.now() - 86400000);
    this.selectedMoment1 = new Date(Date.now() + 86400000);
    this.pageSize = 10;
    this.orders = [];
    this.del = true;
    this.current_order_status = [];
    this.driver_groups_id2 = [];
    this.statuses = [];
    this.plateFilter = "";
    this.generated_by = "";
    this.partner = "";
    this.payment_type_id = "";
    this.order_edited = '';
    this.flagged_orders = '';
    this.customerOrderFilt = false;
    this.vehicle_id_list = [];
    this.pickup_loc = '';
    this.drop_loc = '';
    this.vehicleTypeFilterOrder = "";
    this.socketFlag = true;
    this.promo_id = '';
    this.dispatcher_id2 = '';
    this.dispatcherForOrder.setValue('');
    this.comment_status = '';
    //sessionStorage.removeItem('dispatcher')
    this.progressvalue = 0;
    this.cancelReasons = [];
    this.cancelReasonId = [];
    this.searchReasonCtrl2.setValue(null);
    this.customerRating.setValue(null);
    this.userRating.setValue(null);
    this.vehicleChpModel = [];
    this.vehicleChpModelId = [];
    this.dispatcherOrderFilt = false;
    this.searchVehclCtrl.setValue(null);
    this.CustomerRate = [];
    this.userRate = [];
    this.orderComment = [];
    this.orderComments = '';
    if (this.dtc) {
      this.company_id_list = this.companyData.slice().map(x => x._id);
      this.companyIdFilter = this.company_id_list.slice();
      this.companyIdFilter.push('all');
    }
    this.companyIdFilter2 = this.companyIdFilter.slice();
    this.multiFilter = {
      driver: [],
      side: [],
      partner: [],
      pickup: [],
      drop: [],
      driver_group: []
    };
    this.is_scheduler = undefined;
    this.date_filter_order = '';
    this.getOrders(true);
    setTimeout(() => {
      this.loadingIndicator = '';
    }, 2000)
    let i = 0;
    while (i < this.orders.length) {
      this.orders[i].checked = false;
      i++;
    }
    this.selectedOrder = [];
  }
  /*
   * Search for dispatcher Order List
   *
   */
  public getOrders(flag) {
    if (this.creatingcsv)
      return;
    this.loadingIndicator = '';
    this.socketFlag = flag;
    this.pNo = 1;
    this.reverseGCList = [];
    //this.getAllorderCount();
    if (this.socketFlag) {
      this.OrderUpdateStatusText = this.OrderUpdateStatusOnText;
      this.bgcolorstatus = this.bgcoloron;
    } else {
      this.OrderUpdateStatusText = this.OrderUpdateStatusOffText;
      this.bgcolorstatus = this.bgcoloroff;
    }
    this.orderSearchSubmit = true;
    if (this.currentPage > 1) {
      //this.mode = true;
    }
    if (this.order_limit) {
      this.pageSize = this.order_limit;
    }
    /*if(this.current_order_status.indexOf('scheduled_trip')!==-1)
    this.sort_by_coloumn='scheduler_start_date';*/
    this.sort_by_coloumn = this.date_filter_order ? this.date_filter_order : this.sort_by_coloumn;
    let params, params2;
    if (this.unique_order_id.length !== 0) {
      params = {
        offset: 0,
        limit: this.order_limit != '' ? this.order_limit : this.pageSize,
        sortOrder: this.sort_type != '' ? this.sort_type : 'desc',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        pickup_location: '',
        drop_location: '',
        partner_id: '',
        start_date: '',
        end_date: '',
        promo_id: '',
        cancel_reason_id: '',
        vehicle_model_ids: '',
        vehicle_category_ids: '',
        order_comment_status: '',
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      };
      params2 = {
        limit: this.order_limit != '' ? this.order_limit : this.pageSize,
        sortOrder: this.sort_type != '' ? this.sort_type : 'desc',
        sortByColumn: this.sort_by_coloumn != '' ? this.sort_by_coloumn : 'updated_at',
        vehicle_id: '',
        vehicle: '',
        tariffid: '',
        driverid: '',
        driver: '',
        pickup_location: '',
        drop_location: '',
        current_order_status: '',
        status: [],
        keyword: this.unique_order_id ? this.unique_order_id : '',
        start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
        userid: '',
        user: '',
        user_name: '',
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
        generate_by: '',
        partner_id: '',
        model_id: '',
        promo_id: '',
        payment_type_id: '',
        dispatcher: '',
        cancel_reason_id: '',
        vehicle_model_ids: '',
        vehicle_category_ids: '',
        order_comment_status: '',
      };
    } else {
      let drivers = this.multiFilter.driver.map(x => x._id);
      let vehicles = this.multiFilter.side.map(x => x._id);
      let partners = this.multiFilter.partner.map(x => x._id);
      let pickup_location: any = '';
      let drop_location: any = '';
      if (this.multiFilter.pickup.length > 0) {
        pickup_location = [];
        this.multiFilter.pickup.forEach(element => {
          pickup_location.push([element.loc.coordinates[0]])
        });
      }
      if (this.multiFilter.drop.length > 0) {
        drop_location = [];
        this.multiFilter.drop.forEach(element => {
          drop_location.push([element.loc.coordinates[0]])
        });
      }
      params = {
        offset: 0,
        limit: this.order_limit != '' ? this.order_limit : this.pageSize,
        sortOrder: this.sort_type != '' ? this.sort_type : 'desc',
        sortByColumn: this.sort_by_coloumn != '' ? this.sort_by_coloumn : 'updated_at',
        vehicle_id: vehicles.length > 0 ? vehicles : '',
        tariffid: this.tariff_id ? this.tariff_id : '',
        driver_id: drivers.length > 0 ? drivers : '',
        pickup_location: pickup_location,
        drop_location: drop_location,
        current_order_status: this.current_order_status ? this.current_order_status : '',
        keyword: this.unique_order_id.length != 0 ? this.unique_order_id : '',
        start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
        userid: this.user_id ? this.user_id : '',
        user_name: this.user_name ? this.user_name : '',
        generate_by: this.generated_by ? this.generated_by : '',
        partner_id: partners.length > 0 ? partners : '',
        model_id: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder._id ? this.vehicleTypeFilterOrder._id : '' : '',
        dispatcher_id: this.dispatcher_id2 ? this.dispatcher_id2 : '',
        promo_id: this.promo_id ? this.promo_id : '',
        customer_rating: this.CustomerRate ? this.CustomerRate : '',
        driver_rating: this.userRate ? this.userRate : '',
        payment_type_id: this.payment_type_id ? this.payment_type_id : '',
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
        cancel_reason_id: this.cancelReasonId.length != 0 ? this.cancelReasonId : '',
        vehicle_model_ids: this.vehicleChpModelId.length != 0 ? this.vehicleChpModelId : '',
        vehicle_category_ids: this.vehicle_id_list ? this.vehicle_id_list : '',
        order_comment_status: this.orderComment.length != 0 ? this.orderComment : [],
        order_edited: this.order_edited ? this.order_edited : '',
        flagged_orders: this.flagged_orders ? this.flagged_orders : '',
        is_scheduler: this.is_scheduler ? '1' : '',
        date_filter_order: this.date_filter_order ? this.date_filter_order : 'updated_at',
        guest_user_id: this.guest_user_id ? this.guest_user_id : '',
        guest_user_phone: this.guest_user_phone ? this.guest_user_phone : ''
      };
      params2 = {
        limit: this.order_limit != '' ? this.order_limit : this.pageSize,
        sortOrder: this.sort_type != '' ? this.sort_type : 'desc',
        sortByColumn: this.sort_by_coloumn != '' ? this.sort_by_coloumn : 'updated_at',
        vehicle_id: this.vehicle_id2 ? this.vehicle_id2 : '',
        vehicle: this.plateFilter ? this.plateFilter : '',
        tariffid: this.tariff_id ? this.tariff_id : '',
        driverid: this.driver_id ? this.driver_id : '',
        driver: this.driver_id2 ? this.driver_id2 : '',
        pickup_location: this.pickup_loc ? this.pickup_loc : '',
        drop_location: this.drop_loc ? this.drop_loc : '',
        current_order_status: this.current_order_status ? this.current_order_status : '',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        status: this.statuses ? this.statuses : [],
        order: this.ordersFilter ? this.ordersFilter : [],
        start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
        userid: this.user_id ? this.user_id : '',
        user: this.user_phone ? this.user_phone : '',
        user_name: this.user_name ? this.user_name : '',
        generate_by: this.generated_by ? this.generated_by : '',
        partner_id: this.partner ? this.partner : '',
        model_id: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder : '',
        promo_id: this.promo_id ? this.promo_id : '',
        payment_type_id: this.payment_type_id ? this.payment_type_id : '',
        dispatcher: this.dispatcherForOrder.value ? this.dispatcherForOrder.value : '',
        cancel_reason_id: this.cancelReasonId.length != 0 ? this.cancelReasonId : '',
        vehicle_model_ids: this.vehicleChpModelId.length != 0 ? this.vehicleChpModelId : '',
        vehicle_category_ids: this.vehicle_id_list ? this.vehicle_id_list : '',
        order_comment_status: this.orderComment.length != 0 ? this.orderComment : [],
        customer_rating: this.CustomerRate ? this.CustomerRate : '',
        driver_rating: this.userRate ? this.userRate : '',
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
        order_edited: this.order_edited ? this.order_edited : '',
        flagged_orders: this.flagged_orders ? this.flagged_orders : '',
        is_scheduler: this.is_scheduler ? '1' : '',
        date_filter_order: this.date_filter_order ? this.date_filter_order : 'updated_at',
        guest_user_id: this.guest_user_id ? this.guest_user_id : '',
        guest_user_phone: this.guest_user_phone ? this.guest_user_phone : ''
      };
    }
    if (!flag)
      setTimeout(() => {
        this._global.dispatcher_orders = params2;
      }, 1500)
    this.searchMode = true;
    if (
      this.tariff_id != "" ||
      this.driver_id != "" ||
      this.selectedMoment != "" ||
      this.selectedMoment1 != "" ||
      this.unique_order_id[0] != "" ||
      this.user_id != "" ||
      this.current_order_status[0] != "" ||
      this.vehicle_id2 != ""
    ) {
      this.orders = [];
      this.del = true;
      this.isLoading = true;

      if (this.multiFilter.driver_group.length > 0 && !params.driver_id) {//gets the list of drivers specific to one more driver group
        this.getDriversInDriverGroup(this.multiFilter.driver_group.map(x => x._id)).then((res) => {
          params.driver_id = res;
          let enc_data = this._EncDecService.nwt(this.session_token, params);
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          console.log(params);
          this._orderService.searchDispatcherOrder(data1).then(dec => {
            if (dec) {
              if (dec.status == 200) {
                var data: any = this._EncDecService.dwt(this.session_token, dec.data);
                this.ordersLength = 0;
                this.orderSearchSubmit = true;
                // this.zone.run(() => {
                this.orders = data.searchOrder;
                // })
                // this.orders = data.searchOrder;
                this.isLoading = false;
                this.ordersLength = data.count;
                this.orderSearchSubmit = false;
                this.getAllorderCount();
              }
              else {
                this.isLoading = false;
                this.ordersLength = 0;
                this.orderSearchSubmit = false;
              }
            }
            else {
              this.isLoading = false;
              this.ordersLength = 0;
              this.orderSearchSubmit = false;
            }
          });
        });
      }
      else {
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._orderService.searchDispatcherOrder(data1).then(dec => {
          if (dec) {
            if (dec.status == 200) {
              var data: any = this._EncDecService.dwt(this.session_token, dec.data);
              console.log(data);
              this.ordersLength = 0;
              this.orderSearchSubmit = true;
              // this.zone.run(() => {
              this.orders = data.searchOrder;
              // })
              // this.orders = data.searchOrder;

              this.isLoading = false;
              this.ordersLength = data.count;
              this.orderSearchSubmit = false;
              this.getAllorderCount();
            }
            else {
              this.isLoading = false;
              this.ordersLength = 0;
              this.orderSearchSubmit = false;
            }
          }
          else {
            this.isLoading = false;
            this.ordersLength = 0;
            this.orderSearchSubmit = false;
          }
        });
      }
    }
    // } else {
    //   const params = {
    //     offset: 0,
    //     limit: this.order_limit != "" ? this.order_limit : this.pageSize,
    //     sortOrder: this.sort_type != "" ? this.sort_type : "desc",
    //     sortByColumn: this.sort_by_coloumn != "" ? this.sort_by_coloumn : "updated_at"
    //   };
    //   let enc_data = this._EncDecService.nwt(this.session_token, params);
    //   //alert(this.dispatcher_id.email)
    //   let data1 = {
    //     data: enc_data,
    //     email: this.useremail
    //   }
    //   this.searchMode = false;
    //   this.orders = [];
    //   this.del = true;
    //   this.isLoading = true;
    //   this._orderService.getOrder(data1).then(dec => {
    //     if (dec) {
    //       if (dec.status == 403) {
    //         this.toastr.error("Session timed out");
    //         this._jwtService.destroyDispatcherToken();
    //         this._jwtService.destroyAdminToken();
    //         this.router.navigate(["/dispatcher/login"]);
    //       } else if (dec.status == 200) {
    //         var data: any = this._EncDecService.dwt(this.session_token, dec.data);
    //         this.orderSearchSubmit = true;
    //         this.isLoading = false;
    //         this.orders = data.getOrder;
    //         //console.log(this.orders);
    //         this.ordersLength = data.count;
    //         this.orderSearchSubmit = false;
    //         this.del = false;
    //       }
    //     }
    //     this.isLoading = false;
    //     this.orderSearchSubmit = false;
    //     this.del = false;
    //   });
    // }
  }
  public changeDriverGroups() {
    if (this.addDispatcher.value.driver_groups.length > 1) {
      this.multigroupselected = true;
    }
    if (this.addDispatcher.value.driver_groups.length == 1) {
      this.multigroupselected = false;
    }
  }
  public hidedrivergroups() {
    this.automaticorder = false;
    this.driver_groups_id = '';
  }
  public getDriverGroups() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'unique_driver_model_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._driverService.getDriversGroup(data1).subscribe((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.driverGroupData2 = data.getDriverGroups;
      }
    });
  }
  public getDriverGroupsForListing() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'unique_driver_model_id',
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._driverService.getDriversGroup(data1).subscribe((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.driverGroupData = data.getDriverGroups;
        this.tempdriverGroupData = data.getDriverGroups;
      }
    });
  }
  /**
   * Get all Coverage area
   */
  public getAllCoverageArea() {
    //alert("calling");
    let params = {
      company_id: this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._coverageAreaService.getCoverageAreas(data1).then(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        //console.log(JSON.stringify(data));
        this.coverageData = data.coverageArea;
      }
    });
  }
  /**
   * Get Customer for drop down
   *
   */

  public getUser() {
    let searchText = this.user_id;
    let params = {
      offset: 0,
      limit: 10,
      sortOrder: 'asc',
      sortByColumn: 'firstname',
      search: searchText,
      company_id: this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._customerService.getCustomerByName(data1).subscribe(userdata => {
      if (userdata && userdata.status == 200) {
        userdata = this._EncDecService.dwt(this.session_token, userdata.data);
        this.customerData = userdata.result;
      }
      this.orderSearchSubmit = false;
      this.loadingIndicator = '';
    });
  }

  displayCustomerFn(data): string {
    return data ? data.firstname : data;
  }

  public getSelectOrders(data) {
    this.user_id = data.source.value._id;
    this.autoSearch();
  }
  public getUserSelect(data) {
    this.guest_user_id = data.source.value._id;
    this.autoSearch();
  }
  public getTimelySelectOrders(data) {
    this.timely_user_id = data.source.value._id;
  }
  scroll() {
    window.scrollTo(0, document.getElementById("google").offsetTop);
  }
  pagingAgent(data) {
    this.orderSearchSubmit = true;
    this.currentPage = data;
    this.socketFlag = false;
    let i = 0;
    while (i < this.orders.length) {
      this.orders[i].checked = false;
      i++;
    }
    this.selectedOrder = [];
    if (this.socketFlag) {
      this.OrderUpdateStatusText = this.OrderUpdateStatusOnText;
      this.bgcolorstatus = this.bgcoloron;
    } else {
      this.OrderUpdateStatusText = this.OrderUpdateStatusOffText;
      this.bgcolorstatus = this.bgcoloroff;
    }
    this.pageSize =
      this.order_limit != "" ? parseInt(this.order_limit) : this.pageSize;
    if (!this.mode) {
      this.orders = [];
      this.pageNo = data * this.pageSize - this.pageSize;
      let pickup_location: any = '';
      let drop_location: any = '';
      if (this.multiFilter.pickup.length > 0) {
        pickup_location = [];
        this.multiFilter.pickup.forEach(element => {
          pickup_location.push([element.loc.coordinates[0]])
        });
      }
      if (this.multiFilter.drop.length > 0) {
        drop_location = [];
        this.multiFilter.drop.forEach(element => {
          drop_location.push([element.loc.coordinates[0]])
        });
      }
      let drivers = this.multiFilter.driver.map(x => x._id);
      let vehicles = this.multiFilter.side.map(x => x._id);
      let partners = this.multiFilter.partner.map(x => x._id);
      this.sort_by_coloumn = this.date_filter_order ? this.date_filter_order : this.sort_by_coloumn;
      const params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sort_type != '' ? this.sort_type : 'desc',
        sortByColumn: this.sort_by_coloumn != '' ? this.sort_by_coloumn : 'updated_at',
        vehicle_id: vehicles.length > 0 ? vehicles : '',
        tariffid: this.tariff_id ? this.tariff_id : '',
        driver_id: drivers.length > 0 ? drivers : '',
        pickup_location: pickup_location,
        drop_location: drop_location,
        current_order_status: this.current_order_status ? this.current_order_status : '',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
        userid: this.user_id ? this.user_id : '',
        user_name: this.user_name ? this.user_name : '',
        generate_by: this.generated_by ? this.generated_by : '',
        partner_id: partners.length > 0 ? partners : '',
        model_id: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder._id ? this.vehicleTypeFilterOrder._id : '' : '',
        dispatcher_id: this.dispatcher_id2 ? this.dispatcher_id2 : '',
        promo_id: this.promo_id ? this.promo_id : '',
        payment_type_id: this.payment_type_id ? this.payment_type_id : '',
        cancel_reason_id: this.cancelReasonId.length != 0 ? this.cancelReasonId : '',
        vehicle_model_ids: this.vehicleChpModelId.length != 0 ? this.vehicleChpModelId : '',
        vehicle_category_ids: this.vehicle_id_list ? this.vehicle_id_list : '',
        order_comment_status: this.orderComment.length != 0 ? this.orderComment : [],
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
        order_edited: this.order_edited ? this.order_edited : '',
        flagged_orders: this.flagged_orders ? this.flagged_orders : '',
        is_scheduler: this.is_scheduler ? '1' : '',
        date_filter_order: this.date_filter_order ? this.date_filter_order : 'updated_at',
        guest_user_id: this.guest_user_id ? this.guest_user_id : '',
        guest_user_phone: this.guest_user_phone ? this.guest_user_phone : ''
      };
      this.orderSearchSubmit = true;
      this.orders = [];
      /*this._orderService.searchOrder(params).then((data) => {
        this.orders = data.searchOrder;
        this.orderSearchSubmit = false;
      });*/
      if (this.multiFilter.driver_group.length > 0 && !params.driver_id) {//gets the list of drivers specific to one more driver group
        this.getDriversInDriverGroup(this.multiFilter.driver_group.map(x => x._id)).then((res) => {
          params.driver_id = res;
          let enc_data = this._EncDecService.nwt(this.session_token, params);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._orderService.searchDispatcherOrder(data1).then(data => {
            if (data && data.status == 200) {
              data = this._EncDecService.dwt(this.session_token, data.data);
              this.orderSearchSubmit = true;
              this.orders = data.searchOrder;
              this.ordersLength = data.count;
            }
            this.orderSearchSubmit = false;
          });
        });
      }
      else {
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._orderService.searchDispatcherOrder(data1).then(data => {
          if (data && data.status == 200) {
            data = this._EncDecService.dwt(this.session_token, data.data);
            this.orderSearchSubmit = true;
            this.orders = data.searchOrder;
            this.ordersLength = data.count;
          }
          this.orderSearchSubmit = false;
        });
      }
    }
    this.mode = false;
  }
  public td;
  public copyOrderFlag = false;
  public copyOrder(orderId) {
    this.reset1();
    this.resetDispatcherForm();
    this.orderUpdateFlag = true;
    let params = {
      company_id: this.company_id_array,
      _id: orderId
    }
    this.editCustomer = false;
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOrderById(data1).then(dec => {
      if (dec && dec.status == 200) {
        let res: any = this._EncDecService.dwt(this.session_token, dec.data);
        res = res.response;
        this.update_id = orderId;
        if (
          res.SingleDetail.pickup_location &&
          res.SingleDetail.customer_pickup_lat_long
        ) {
          var address = {
            geo_locations: res.SingleDetail.customer_pickup_lat_long,
            address: res.SingleDetail.pickup_location
          };
          this.makeItPickupFromExisting(address);
        }
        this.copyOrderFlag = true;
        if (res.SingleDetail.vehicle_model_ids.length > 0)
          this.modelChips = res.SingleDetail.vehicle_model_ids;
        else if (res.SingleDetail.vehicle_id && res.SingleDetail.vehicle_id.vehicle_model_id) {
          let params = {
            company_id: this.company_id_array,
            _id: res.SingleDetail.vehicle_id.vehicle_model_id
          }
          let enc_data = this._EncDecService.nwt(this.session_token, params);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._vehicleModelsService.getVehicleModelById(data1).then((res) => {
            if (res && res.status == 200) {
              res = this._EncDecService.dwt(this.session_token, res.data);
              if (res.vehicleModel)
                this.modelChips = [res.vehicleModel];
            }
          })
        }
        if (res.SingleDetail.drop_location && res.SingleDetail.customer_drop_lat_long) {
          var address = {
            geo_locations: res.SingleDetail.customer_drop_lat_long,
            address: res.SingleDetail.drop_location
          };
          this.makeItDropoffFromExisting(address);
        }
        this.addDispatcher.patchValue({
          customerCountryCode: res.SingleDetail.user_id.country_code ? res.SingleDetail.user_id.country_code : '971',
          customerPhoneList: res.SingleDetail.user_id
            ? res.SingleDetail.user_id
            : "",
          customerNameList: res.SingleDetail.user_id
            ? res.SingleDetail.user_id
            : "",
          eta: res.SingleDetail.eta ? res.SingleDetail.eta : "",
          scheduler_start_date: res.SingleDetail.scheduler_start_date
            ? res.SingleDetail.scheduler_start_date.split(" ")[0]
            : "",
          scheduler_start_time: res.SingleDetail.scheduler_start_date
            ? moment(
              res.SingleDetail.scheduler_start_date
            ).format()
            : "",
          schedule_before: res.SingleDetail.schedule_before,
          distance: res.SingleDetail.distance ? res.SingleDetail.distance : "",
          partnerList: res.SingleDetail.partner_id
            ? res.SingleDetail.partner_id
            : "",
          vehicleList: res.SingleDetail.vehicle_id
            ? res.SingleDetail.vehicle_id
            : "",
          driverList: res.SingleDetail.driver_id
            ? res.SingleDetail.driver_id
            : "",
          additionalList: res.SingleDetail.additional_service_id
            ? res.SingleDetail.additional_service_id
            : "",
          paymentExtraList: res.SingleDetail.payment_extra_id
            ? res.SingleDetail.payment_extra_id
            : "",
          order_type: res.SingleDetail.is_assigned,
          price: res.SingleDetail.tariff_id ? res.SingleDetail.tariff_id.tariff_type == 'FIX_RATE' ? Math.trunc(res.SingleDetail.price) ? Math.trunc(res.SingleDetail.price) : "" : "" : "",
          OrderGroup: res.SingleDetail.order_group_id
            ? res.SingleDetail.order_group_id
            : "",
          tariff_price: res.SingleDetail.tariff_id ? res.SingleDetail.tariff_id.tariff_type == 'FIX_RATE' ? Math.trunc(res.SingleDetail.price) ? Math.trunc(res.SingleDetail.price) : "" : "" : "",
          is_scheduler: res.SingleDetail.scheduler_start_date ? 1 : 0,
          payment_extra_price: res.SingleDetail.payment_extra_id
            ? res.SingleDetail.payment_extra_id
            : "",
          total_sum: res.SingleDetail.tariff_id ? res.SingleDetail.tariff_id.tariff_type == 'FIX_RATE' ? Math.trunc(res.SingleDetail.price) ? Math.trunc(res.SingleDetail.price) : "" : "" : "",
          message: res.SingleDetail.message ? res.SingleDetail.message : "",
          multiple: 1,
          tbEndTime: res.SingleDetail.hourlyend ? moment(
            res.SingleDetail.hourlyend
          ).format()
            : ""
        });
        if (res.SingleDetail.user_id)
          this.OnSelectPhone({ source: { value: res.SingleDetail.user_id } });
        else this.SelectedCustomerAddress = [];
        if (res.SingleDetail.scheduler_start_date) this.schedulerStaus = true;
        if (res.SingleDetail.driver_id) this.onFlagChange(false);
        if (res.SingleDetail.tariff_id) this.addDispatcher.controls["tarrifList"].setValue(res.SingleDetail.tariff_id);
      }
    })
      .catch(error => {
        console.log(error);
      });
  }


  public cancelOrder(orderId, order_status) {
    if (
      order_status == "dispatcher_cancel" ||
      order_status == "completed" ||
      order_status == "ended_by_admin" ||
      order_status == "cancel_acknowledged" ||
      order_status == "customer_cancel" ||
      order_status == "rejected_by_driver" ||
      order_status == "order_timed_out" ||
      order_status == "driver_cancel"
    ) {
      if (order_status == "dispatcher_cancel") {
        this.cancelOrdermsg = " This order already cancelled By dispatcher.";
      } else if (order_status == "ended_by_admin") {
        this.cancelOrdermsg = " This order already ended By admin.";
      } else if (order_status == "cancel_acknowledged") {
        this.cancelOrdermsg = " This order already cancelled By dispatcher.";
      } else if (order_status == "customer_cancel") {
        this.cancelOrdermsg = " This order already cancelled By customer.";
      } else if (order_status == "rejected_by_driver") {
        this.cancelOrdermsg = " This order already cancelled By Driver.";
      } else if (order_status == "order_timed_out") {
        this.cancelOrdermsg = " This order already timedout.";
      } else if (order_status == "driver_cancel") {
        this.cancelOrdermsg = " This order already cancelled.";
      } else {
        this.cancelOrdermsg = "This Order is already Completed.";
      }
      $("#cancel_order").modal("show");
    } else {
      this.tripCancelPopup(orderId, order_status);
    }
  }
  public tripCancelPopup(orderId, order_status) {
    let dialogRef = this.dialog.open(CancelReasonComponent, {
      width: "315px",
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: {
        type: "order_cancel",
        reason_head: "Cancellation Reason",
        order_status: order_status
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        var user = JSON.parse(window.localStorage["dispatcherUser"]);
        var data = {
          email: user.email,
          type: "order_cancel",
          reason: res.cancelreason,
          status: true,
          company_id: this.company_id_array,
          _id: orderId
        };
        let enc_data = this._EncDecService.nwt(this.session_token, data);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._orderService.cancelOrder(data1).then(cancel => {
          if (cancel) {
            if (cancel.status == 202) {
              this.toastr.success(cancel.message);
            } else if (cancel.status == 205) {
              this.toastr.success(cancel.message);
            } else if (cancel.status == 200) {
              this.toastr.success(cancel.message);
            } else {
              this.toastr.error(cancel.message);
            }
          } else {
            this.toastr.warning("Error while Order cancellation!");
          }
        });
      } else {
      }
    });
  }

  orderHistoryTab = false;
  customerOrderHistory = [];
  public customerOrderCount = 0;
  public showModel(order, flag) {
    this.trackTime = "";
    this.beforeData = [];
    this.afterData = [];
    this.beforeDataFirst = [];
    this.afterDataFirst = [];
    if (this.directionsDisplay)
      this.directionsDisplay.setMap(null);
    if (flag)
      setTimeout(() => {
        let el: HTMLElement = document.getElementById('orderInfo') as HTMLElement;
        el.click()
      }, 500);
    this.acceptedLocation = '';
    this.distanceToReach = '';
    this.timeToReach = '';
    this.acceptedTime = '';
    for (let i = 0; i < this.orderInfoMarkers.length; i++) {
      this.orderInfoMarkers[i].setMap(null);
      if (i + 1 == this.orderInfoMarkers.length)
        this.orderInfoMarkers = []
    }
    this.orderHistoryTab = false;
    this.customerOrderHistory = [];
    if (this.livetrackingOrder) {
      clearInterval(this.livetrackingOrder);
    }
    this.infoLoader = false;
    this.showLoader = true;
    //Order Status
    //'new', 'accepted_request', 'waiting_for_customer', 'dispatcher_cancel', 'accepted_by_driver', 'rejected_by_driver',
    //'trip_started', 'confirming_rate', 'selecting_payment', 'rating', 'completed', 'cancel_acknowledged'
    if (this.mapready) {
      if (this.polypath != null) {
        this.polypath.setMap(null);
      }
      if (this.livetrackingmarker != null) {
        this.livetrackingmarker.setMap(null);
      }
      this.pendinginfoorder = false;
      this.pendingorder = "";
      this.maploaded = false;
      this.startmarker = "";
      this.destinationmarker = "";
      this.ordermarkers.length = 0;
      this.path.length = 0;
      this.orderInfo.length = 0;
      let params1 = {
        company_id: this.company_id_array,
        _id: order
      }
      let enc_data = this._EncDecService.nwt(this.session_token, params1);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this.orderInfo = '';
      this._orderService.getOrderById(data1).then(dec => {
        if (dec && dec.status == 200) {
          let that = this;
          var orderData: any = this._EncDecService.dwt(this.session_token, dec.data);
          console.log(orderData);
          that.orderInfo = orderData.response.SingleDetail;
          that.travelled_cordinates = orderData.response.travelled;
          //alert(JSON.stringify(that.orderInfo.sync_trip_id));
          if (that.orderInfo.sync_trip_id != null)
            if (
              // that.travelled_cordinates.length > 0 &&
              // that.travelled_cordinates.length != ""
              that.orderInfo.sync_trip_id.trip_status === "completed" ||
              that.orderInfo.sync_trip_id.trip_status == "confirming_rate" ||
              that.orderInfo.sync_trip_id.trip_status == "selecting_payment" ||
              that.orderInfo.sync_trip_id.trip_status == "rating" ||
              that.orderInfo.sync_trip_id.trip_status == "dispatcher_cancel"
            ) {

              this.bounds = new google.maps.LatLngBounds();
              //alert(that.travelled_cordinates.length );
              if (
                that.travelled_cordinates.length > 0 &&
                that.travelled_cordinates.length != ""
              ) {
                for (var i = 0; i < that.travelled_cordinates.length; ++i) {
                  that.path.push({
                    lat: that.travelled_cordinates[i].lat,
                    lng: that.travelled_cordinates[i].lng
                  });
                  that.ordermarkers.push([
                    that.travelled_cordinates[i].lat,
                    that.travelled_cordinates[i].lng,
                    that.travelled_cordinates[i].bearing,
                    that.travelled_cordinates[i].speed,
                    that.travelled_cordinates[i].date,
                    that.travelled_cordinates[i].is_gps_on,
                    that.travelled_cordinates[i].is_internet_on,
                    that.travelled_cordinates[i].accuracy,
                    that.travelled_cordinates[i].battery_percent
                  ]);
                  //alert(JSON.stringify(that.ordermarkers));
                  this.bounds.extend(
                    new google.maps.LatLng(
                      that.ordermarkers[i][0],
                      that.ordermarkers[i][1]
                    )
                  );
                  if (i == that.travelled_cordinates.length - 1) {
                    var car =
                      "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
                    var icon = {
                      path: car,
                      scale: 0.7,
                      strokeColor: "white",
                      strokeWeight: 0.1,
                      fillOpacity: 1,
                      fillColor: "#404040",
                      offset: "5%",
                      // rotation: parseInt(heading[i]),
                      anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
                    };
                    this.polypath = new google.maps.Polyline({
                      path: that.path,
                      geodesic: true,
                      icons: [
                        {
                          icon: icon,
                          offset: "100%"
                        }
                      ],
                      strokeColor: "#FF0000",
                      strokeOpacity: 1.0,
                      strokeWeight: 2
                    });
                    this.polypath.setMap(this.ordermap);
                    this.ordermap.fitBounds(this.bounds);
                    let ordermarkerlen = that.ordermarkers.length;
                    ordermarkerlen = Math.ceil(ordermarkerlen / 2);
                    this.ordermap.setCenter(
                      new google.maps.LatLng(
                        that.ordermarkers[ordermarkerlen - 1][0],
                        that.ordermarkers[ordermarkerlen - 1][1]
                      )
                    );
                    this.setMapzoom(this.ordermarkers.length);
                    this.startmarker = JSON.parse(
                      "[" + that.orderInfo.sync_trip_id.trip_start_lat_long + "]"
                    );
                    if (that.orderInfo.sync_trip_id.trip_status == "dispatcher_cancel")
                      this.destinationmarker = JSON.parse(
                        "[" + that.travelled_cordinates[i].lat + ',' +
                        that.travelled_cordinates[i].lng + "]"
                      );
                    else
                      this.destinationmarker = JSON.parse(
                        "[" + that.orderInfo.sync_trip_id.trip_end_lat_long + "]"
                      );
                    this.maploaded = true;
                  }
                }
              } else {
                if (that.orderInfo.sync_trip_id.trip_status !== "dispatcher_cancel")
                  this.toastr.error("Cant Retreive travelled cordinates");
                if (that.orderInfo.sync_trip_id.trip_start_lat_long) {
                  this.maploaded = true;
                  this.startmarker = JSON.parse(
                    "[" + that.orderInfo.sync_trip_id.trip_start_lat_long + "]"
                  );
                  if (that.orderInfo.sync_trip_id.trip_status !== "dispatcher_cancel")
                    this.destinationmarker = JSON.parse(
                      "[" + that.orderInfo.sync_trip_id.trip_end_lat_long + "]"
                    );
                  let latlngvalue = that.orderInfo.sync_trip_id.trip_start_lat_long.split(
                    ","
                  );
                  this.ordermap.setCenter(
                    new google.maps.LatLng(latlngvalue[0], latlngvalue[1])
                  );
                }
              }
            } else if (that.orderInfo.sync_trip_id.trip_status === "trip_started") {
              this.bounds = new google.maps.LatLngBounds();
              that.travelled_cordinates = orderData.response.travelled;
              if (
                that.travelled_cordinates.length > 0 &&
                that.travelled_cordinates.length != ""
              ) {
                for (var i = 0; i < that.travelled_cordinates.length; ++i) {
                  that.ordermarkers.push([
                    that.travelled_cordinates[i].lat,
                    that.travelled_cordinates[i].lng,
                    that.travelled_cordinates[i].bearing,
                    that.travelled_cordinates[i].speed,
                    that.travelled_cordinates[i].date,
                    that.travelled_cordinates[i].is_gps_on,
                    that.travelled_cordinates[i].is_internet_on,
                    that.travelled_cordinates[i].accuracy,
                    that.travelled_cordinates[i].battery_percent
                  ]);
                  that.bounds.extend(
                    new google.maps.LatLng(
                      that.ordermarkers[i][0],
                      that.ordermarkers[i][1]
                    )
                  );
                }
                this.ordermap.fitBounds(this.bounds);
                this.ordermarkerlen = that.ordermarkers.length;
                this.ordermarkerlen = Math.ceil(that.ordermarkerlen / 2);
                this.ordermap.setCenter(
                  new google.maps.LatLng(
                    that.ordermarkers[that.ordermarkerlen - 1][0],
                    that.ordermarkers[that.ordermarkerlen - 1][1]
                  )
                );
                this.setMapzoom(this.ordermarkers.length);
              }
              this.startmarker = JSON.parse(
                "[" + that.orderInfo.sync_trip_id.trip_start_lat_long + "]"
              );
              //alert(JSON.stringify(that.orderInfo.sync_trip_id.trip_start_lat_long));
              let latlngvalue = that.orderInfo.sync_trip_id.trip_start_lat_long.split(
                ","
              );
              this.ordermap.setCenter(
                new google.maps.LatLng(latlngvalue[0], latlngvalue[1])
              );
              this.maploaded = true;
              let temp_cordinates = [];
              this.sockettrackno = 0;
              let updated_trip_cordinates = [];
              let params1 = {
                company_id: this.company_id_array,
                vehicle_id: that.orderInfo.vehicle_id
              }
              let enc_data1 = this._EncDecService.nwt(this.session_token, params1);
              //alert(this.dispatcher_id.email)
              let data2 = {
                data: enc_data1,
                email: this.useremail
              }
              that.livetrackingOrder = setInterval(function () {
                that._devicelocationService
                  .getVehicleLastLocation(data2)
                  .then(data => {
                    if (data && data.status == 200) {
                      data = that._EncDecService.dwt(that.session_token, data.data);
                      if (data.locations.l_id && data.locations.l_id != "") {
                        that.ordermarkers.push([
                          data.locations.latitude,
                          data.locations.longitude,
                          0,
                          data.locations.speed,
                          data.locations.created_at,
                          data.locations.is_gps_on,
                          data.locations.is_internet_on,
                          data.locations.accuracy,
                          data.locations.battery
                        ]);
                      } else {
                        //that.toastr.success("Trip Completed");
                        if (that.livetrackingOrder) {
                          clearInterval(that.livetrackingOrder);
                        }
                        that._orderService.getOrderById(data1).then(dec => {
                          if (dec && dec.status == 200) {
                            var orderData: any = that._EncDecService.dwt(this.session_token, dec.data);
                            that.orderInfo = orderData.response.SingleDetail;
                            that.travelled_cordinates = orderData.response.travelled;
                            //alert(JSON.stringify(that.orderInfo.sync_trip_id));
                            if (
                              that.orderInfo.sync_trip_id.trip_status ===
                              "completed" ||
                              that.orderInfo.sync_trip_id.trip_status ==
                              "confirming_rate" ||
                              that.orderInfo.sync_trip_id.trip_status ==
                              "selecting_payment" ||
                              that.orderInfo.sync_trip_id.trip_status == "rating"
                            ) {
                              that.bounds = new google.maps.LatLngBounds();
                              if (
                                that.travelled_cordinates.length > 0 &&
                                that.travelled_cordinates.length != ""
                              ) {
                                for (
                                  var i = 0;
                                  i < that.travelled_cordinates.length;
                                  ++i
                                ) {
                                  that.path.push({
                                    lat: that.travelled_cordinates[i].lat,
                                    lng: that.travelled_cordinates[i].lng
                                  });
                                  that.ordermarkers.push([
                                    that.travelled_cordinates[i].lat,
                                    that.travelled_cordinates[i].lng,
                                    that.travelled_cordinates[i].bearing,
                                    that.travelled_cordinates[i].speed,
                                    that.travelled_cordinates[i].date,
                                    that.travelled_cordinates[i].is_gps_on,
                                    that.travelled_cordinates[i].is_internet_on,
                                    that.travelled_cordinates[i].accuracy,
                                    that.travelled_cordinates[i].battery_percent
                                  ]);
                                  //alert(JSON.stringify(that.ordermarkers));
                                  that.bounds.extend(
                                    new google.maps.LatLng(
                                      that.ordermarkers[i][0],
                                      that.ordermarkers[i][1]
                                    )
                                  );
                                  if (i == that.travelled_cordinates.length - 1) {
                                    var car =
                                      "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
                                    var icon = {
                                      path: car,
                                      scale: 0.7,
                                      strokeColor: "white",
                                      strokeWeight: 0.1,
                                      fillOpacity: 1,
                                      fillColor: "#404040",
                                      offset: "5%",
                                      // rotation: parseInt(heading[i]),
                                      anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
                                    };
                                    that.polypath = new google.maps.Polyline({
                                      path: that.path,
                                      geodesic: true,
                                      icons: [
                                        {
                                          icon: icon,
                                          offset: "100%"
                                        }
                                      ],
                                      strokeColor: "#FF0000",
                                      strokeOpacity: 1.0,
                                      strokeWeight: 2
                                    });
                                    that.polypath.setMap(that.ordermap);
                                    that.ordermap.fitBounds(that.bounds);
                                    let ordermarkerlen = that.ordermarkers.length;
                                    ordermarkerlen = Math.ceil(ordermarkerlen / 2);
                                    that.ordermap.setCenter(
                                      new google.maps.LatLng(
                                        that.ordermarkers[ordermarkerlen - 1][0],
                                        that.ordermarkers[ordermarkerlen - 1][1]
                                      )
                                    );
                                    that.setMapzoom(that.ordermarkers.length);
                                    that.startmarker = JSON.parse(
                                      "[" +
                                      that.orderInfo.sync_trip_id
                                        .trip_start_lat_long +
                                      "]"
                                    );
                                    if (that.orderInfo.sync_trip_id.trip_status !== "dispatcher_cancel")
                                      this.destinationmarker = JSON.parse(
                                        "[" +
                                        that.orderInfo.sync_trip_id
                                          .trip_end_lat_long +
                                        "]"
                                      );
                                    this.maploaded = true;
                                  }
                                }
                              } else {
                                that.toastr.error(
                                  "Cant Retreive travelled cordinates"
                                );
                                that.maploaded = true;
                                that.startmarker = JSON.parse(
                                  "[" +
                                  that.orderInfo.sync_trip_id.trip_start_lat_long +
                                  "]"
                                );
                                if (that.orderInfo.sync_trip_id.trip_status !== "dispatcher_cancel")
                                  this.destinationmarker = JSON.parse(
                                    "[" +
                                    that.orderInfo.sync_trip_id.trip_end_lat_long +
                                    "]"
                                  );
                                let latlngvalue = that.orderInfo.sync_trip_id.trip_start_lat_long.split(
                                  ","
                                );
                                that.ordermap.setCenter(
                                  new google.maps.LatLng(
                                    latlngvalue[0],
                                    latlngvalue[1]
                                  )
                                );
                              }
                            }
                          }
                        });
                      }
                    }
                  });
              }, 3000);
            } else {
            }
          if (that.orderInfo.user_id != null) {
            const param2 = {
              offset: 0,
              sortOrder: 'desc',
              sortByColumn: 'unique_order_id',
              limit: 10,
              user_id: that.orderInfo.user_id._id,
              company_id: this.company_id_array
            }
            let enc_data3 = this._EncDecService.nwt(this.session_token, param2);
            //alert(this.dispatcher_id.email)
            let data3 = {
              data: enc_data3,
              email: this.useremail
            }
            that._orderService.getOrderByUserId(data3).then(data => {
              if (data && data.status == 200) {
                data = this._EncDecService.dwt(this.session_token, data.data);
                that.orderHistoryTab = true;
                that.customerOrderHistory = data.data;
                that.customerOrderCount = data.count
              }
            });
          }
          if (that.orderInfo.generate_by != 'driver') {
            var data3 = {
              company_id: this.company_id_array,
              _id: order
            };
            let enc_data = this._EncDecService.nwt(this.session_token, data3);
            //alert(this.dispatcher_id.email)
            let data = {
              data: enc_data,
              email: this.useremail
            }
            this.showLoader = true;
            that._orderService.getKeyIndicators(data).then(dec => {
              this.showLoader = false;
              if (dec && dec.status == 200) {
                let orderData: any = this._EncDecService.dwt(this.session_token, dec.data);
                that.reverseGCListKey = true;
                that.orderInfoKey = orderData.response;
                //let rejectedList = orderData.ExtraDetail.map(a => a.driver_id._id);
                //let driverNameList = orderData.SingleDetail.convertedAssignedDriversId.map(a => { a._id, a.username });
                let title = ''
                let icon = "../assets/img/marker-red.png";
                let label: any = '';
                // if (that.orderInfo.customer_pickup_lat_long) {
                //   let positionArray = that.orderInfo.customer_pickup_lat_long.split(',');
                //   let position = new google.maps.LatLng(positionArray[0], positionArray[1]);
                //   title = 'Actual pickup';
                //   that.createMarker(position, '', icon, label, title);
                //   that.ordermap.setCenter(position);
                // }
                // if (that.orderInfo.customer_drop_lat_long) {
                //   let positionArray = that.orderInfo.customer_drop_lat_long.split(',');
                //   let position = new google.maps.LatLng(positionArray[0], positionArray[1]);
                //   title = 'Actual drop off';
                //   that.createMarker(position, '', icon, label, title)
                // }
                if (that.orderInfo.customer_selected_pickup_lat_long) {
                  let positionArray = that.orderInfo.customer_selected_pickup_lat_long.split(',');
                  let position = new google.maps.LatLng(positionArray[0], positionArray[1]);
                  let content = 'Selected pickup';
                  icon = "../assets/img/map/dispatcher pins/selected_pickup.png";
                  that.createMarker(position, content, icon, '', '')
                }
                if (that.orderInfo.customer_selected_drop_lat_long) {
                  let positionArray = that.orderInfo.customer_selected_drop_lat_long.split(',');
                  let position = new google.maps.LatLng(positionArray[0], positionArray[1]);
                  let title = 'Selected drop off';
                  icon = "../assets/img/map/dispatcher pins/selected_drop.png";
                  that.createMarker(position, title, icon, '', '')
                }
                if (that.orderInfoKey.assigned_drivers) {
                  that.getOrderAcceptanceHistory(order);
                  for (let i = 0; i < that.orderInfoKey.assigned_drivers.length; i++) {
                    let positionArray = that.orderInfoKey.assigned_drivers[i].driver_loc.split(',');
                    let position = new google.maps.LatLng(positionArray[0], positionArray[1]);
                    // if (rejectedList.indexOf(that.orderInfoKey.assigned_drivers[i].driver_id) > -1) {
                    //   content = "Rejected"
                    // } else {
                    //   content = 'Assigned';
                    // }
                    title = '';
                    let content = ''
                    label = (i + 1).toString();
                    if (that.orderInfoKey.driver_id == that.orderInfoKey.assigned_drivers[i].driver_id) {
                      icon = "../assets/img/map/dispatcher pins/accepted_driver_location.png";
                      content = 'Accepted time: ' + that.orderInfoKey.accept_time + '</br>' +
                        'Assigned time: ' + that.orderInfoKey.assigned_drivers[i].assigned_at + '</br>' +
                        'Latlong: ' + that.orderInfoKey.assigned_drivers[i].driver_loc + '</br>' +
                        'Driver: ' + that.orderInfoKey.convertedAssignedDriversId[i].username + '&nbsp;&nbsp; Phone:' + that.orderInfoKey.convertedAssignedDriversId[i].phone_number;
                      let selectedMode = "DRIVING";
                      let originArray = that.orderInfo.customer_selected_pickup_lat_long.split(',');
                      let origin = new google.maps.LatLng(originArray[0], originArray[1]);
                      let request = {
                        origin: position,
                        destination: origin,
                        travelMode: google.maps.TravelMode[selectedMode]
                      };
                      that.directionsDisplay.setMap(null);
                      that.directionsDisplay.setMap(that.ordermap);
                      var polylineOptionsActual = new google.maps.Polyline({
                        strokeColor: '#000000'
                      });
                      that.directionsDisplay.setOptions({ suppressMarkers: true, preserveViewport: true });
                      that.directionsDisplay.setOptions({ polylineOptions: polylineOptionsActual });
                      that.directionsService.route(request, function (response, status) {
                        that.directionsDisplay.setDirections(response);
                      });
                      that.createMarker(position, content, icon, '', title)
                    }
                    else {
                      icon = "../assets/img/map/dispatcher pins/assigned_driver_location.png";
                      content = 'Assigned time: ' + that.orderInfoKey.assigned_drivers[i].assigned_at + '</br>' +
                        'Latlong: ' + that.orderInfoKey.assigned_drivers[i].driver_loc + '</br>' +
                        'Driver: ' + that.orderInfoKey.convertedAssignedDriversId[i].username + '&nbsp;&nbsp; Phone:' + that.orderInfoKey.convertedAssignedDriversId[i].phone_number;
                      that.createMarker(position, content, icon, label, title)
                    }
                  }
                }
              }
            });
          }
          this.infoLoader = true;
        }
      });
      let params2 = {
        company_id: this.company_id_array,
        order_id: order
      }
      let enc_data2 = this._EncDecService.nwt(this.session_token, params2);
      let data2 = {
        data: enc_data2,
        email: this.useremail
      }
      this.inTripCordinates = [];
      this._orderService.getInTripCordinates(data2).then(dec => {
        this.showLoader = false;
        if (dec && dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          let cordinates = data.locations;
          let bounds = new google.maps.LatLngBounds();
          for (var i = 0; i < cordinates.length; ++i) {
            this.inTripCordinates.push([
              cordinates[i].latitude,
              cordinates[i].longitude,
              cordinates[i].bearing,
              cordinates[i].speed,
              cordinates[i].created_at,
              cordinates[i].is_gps_on,
              cordinates[i].is_internet_on,
              cordinates[i].accuracy,
              cordinates[i].battery
            ]);
            //alert(JSON.stringify(that.ordermarkers));
            bounds.extend(
              new google.maps.LatLng(
                this.inTripCordinates[i][0],
                this.inTripCordinates[i][1]
              )
            );
            this.ordermap.fitBounds(bounds);
          }
        }
      });
    } else {
      if (flag) {
        this.pendinginfoorder = true;
        this.pendingorder = order;
      }
      else
        setTimeout(() => this.showModel(order, false), 1000)
    }
  }
  beforeDataFirst = [];
  afterDataFirst = [];
  public findCarTrack(order) {
    console.log(order)
    this.mapLoader = true;
    var params = {
      company_id: this.company_id_array,
      order_id: order._id,
      time_interval: this.trackTime
    }
    var enc_data = this._EncDecService.nwt(this.session_token, params);
    var datas = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getShiftbyOrder(datas).then((dec) => {
      this.mapLoader = false;
      if (dec && dec.data) {
        this.beforeData = [];
        this.afterData = [];
        this.beforeDataFirst = [];
        this.afterDataFirst = [];
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        var beforeData = data.beforeData;
        var afterData = data.afterData;
        for (var i = 0; i < beforeData.length; ++i) {
          this.beforeData.push([
            beforeData[i].latitude,
            beforeData[i].longitude,
            beforeData[i].bearing,
            beforeData[i].speed,
            beforeData[i].created_at,
            beforeData[i].is_gps_on,
            beforeData[i].is_internet_on,
            beforeData[i].accuracy,
            beforeData[i].battery
          ]);
        }
        for (var i = 0; i < afterData.length; ++i) {
          this.afterData.push([
            afterData[i].latitude,
            afterData[i].longitude,
            afterData[i].bearing,
            afterData[i].speed,
            afterData[i].created_at,
            afterData[i].is_gps_on,
            afterData[i].is_internet_on,
            afterData[i].accuracy,
            afterData[i].battery
          ]);
        }
        this.beforeDataFirst.push(this.beforeData ? this.beforeData.pop() : []);
        this.beforeDataFirst.push(this.beforeData ? this.beforeData.shift() : []);
        this.afterDataFirst.push(this.afterData ? this.afterData.pop() : []);
        this.afterDataFirst.push(this.afterData ? this.afterData.shift() : []);
      }
    })
  }
  public createMarker(position, content, icon, label: string, title) {
    let that = this;
    let marker;
    if (label !== '') {
      marker = new google.maps.Marker({
        position: position,
        map: this.ordermap,
        icon: icon,
        label: { text: label, color: "white" },
        title: title
      });
    }
    else {
      marker = new google.maps.Marker({
        position: position,
        map: this.ordermap,
        icon: icon,
        title: title
      });
    }
    this.orderInfoMarkers.push(marker);
    if (content !== '') {
      var infowindow = new google.maps.InfoWindow({
        content: content
      });
      marker.addListener('mouseover', function () {
        infowindow.open(that.ordermap, marker);
      });
      marker.addListener('mouseout', function () {
        infowindow.close();
      });
    }
  }
  public showMessageModel(order) {
    this.orderInfo = order;
    let params1 = {
      company_id: this.company_id_array,
      _id: order._id
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params1);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOrderById(data1).then(dec => {
      if (dec && dec.status == 200) {
        var orderData: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.orderInfo.driver_id = orderData.response.SingleDetail.driver_id;
        if (this.orderInfo.driver_id._id) {
          this.addMessage.controls["driver_id"].setValue(
            this.orderInfo.driver_id._id
          );
        } else {
          this.addMessage.controls["driver_id"].setValue(
            this.orderInfo.driver_id
          );
        }
      }
    });
  }

  /**
   * Show Driver and Vehicle Inputs on click
   */
  clicked2() {
    this.show = !this.show;
  }
  onMapClick(event) {
    if (!event.latLng) return;
    this.display = true;
    this.lat = event.latLng.lat();
    this.lang = event.latLng.lng();
    let RoughPickup = [];
    RoughPickup.push([this.lat, this.lang]);
    if (RoughPickup.length > 0) {
      this.roughMarkers = [];
      this.roughMarkers.push([this.lat, this.lang]);
    }
    let latlng = { lat: this.lat, lng: this.lang };
    this.clickedLatLang = this.lat + "," + this.lang;
    let geocoder = new google.maps.Geocoder();
    let d = this;
    geocoder.geocode({ location: latlng }, function (results, status) {
      if (results) {
        d.clickedAdress = results[0].formatted_address;
        this.mapClickedAddress = d.clickedAdress;
      }
    });
  }

  makeItPoi(data) {
    let dialogRef = this.dialog.open(PoiDialogComponent, {
      width: "350px",
      data: { address: data, lat: this.lat, lng: this.lang }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.poi_name) {
          this.poiModel.name = result.poi_name;
          this.poiModel.company = "DubaiTaxi";
          this.poiModel.xcoordinate = this.lat;
          this.poiModel.ycoordinate = this.lang;
          this.poiModel.is_favorite = result.poi_is_fav;
          this.poiModel.company_id = window.localStorage['user_company'];
          let enc_data = this._EncDecService.nwt(this.session_token, this.poiModel);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: this.useremail
          }
          this._poiService.poiAdd(data1).then(res => {
            if (res && res.staus == 200)
              this.toastr.success("Poi successfully created.");
          });
        }
      } else {
      }
    });
  }

  makeItPickup() {
    if (this.inPick) {
      for (let i = 0; i < this.picking.length; i++) {
        this.picking[i].setMap(null);
      }
      this.inPick = false;
    }
    this.markerData.nguiMapComponent.closeInfoWindow("iw", this.markerData);
    let collectPickup = [];
    this.display = false;
    this.pickupLat = this.lat;
    this.pickupLang = this.lang;
    this.addDispatcher.controls["sourceLatLang"].setValue(
      this.lat + "," + this.lang
    );
    collectPickup.push([this.pickupLat, this.pickupLang]);
    if (collectPickup.length > 0) {
      this.pickupMarkers = [];
      this.dropOffMarkers = [];
      this.pickupMarkers.push([this.pickupLat, this.pickupLang]);
      if (this.dropOffLat || this.dropOffLang)
        this.dropOffMarkers.push([this.dropOffLat, this.dropOffLang]);
    }
    let latlng = { lat: this.pickupLat, lng: this.pickupLang };
    let geocoder = new google.maps.Geocoder();
    let d = this;
    geocoder.geocode({ location: latlng }, function (results, status) {
      if (results) {
        d.pickupChanged(results[0]);
        // this.pickup_location = results[0].formatted_address;
        // d.addDispatcher.controls['pickup_location'].setValue(results[0].formatted_address);
      }
      this.address = [];
      if (results) {
        for (let i = 0; i < results[0].address_components.length; i++) {
          let addressType = results[0].address_components[i].types[0];
          this.address[addressType] =
            results[0].address_components[i].long_name;
          d.addDispatcher.controls["pickup_country"].setValue(
            this.address.country
          );
        }
      }
      d.direction.origin = String(d.pickupLat + "," + d.pickupLang);
      d.showDirection();
    });
  }

  makeItDropoff() {
    if (this.inDrop) {
      for (let i = 0; i < this.dropOffing.length; i++) {
        this.dropOffing[i].setMap(null);
      }
      this.inDrop = false;
    }
    this.markerData.nguiMapComponent.closeInfoWindow("iw", this.markerData);
    let geocoder = new google.maps.Geocoder();
    this.display = false;
    this.dropOffLat = this.lat;
    this.dropOffLang = this.lang;
    let collectDropoff = [];
    collectDropoff.push([this.dropOffLat, this.dropOffLang]);
    if (collectDropoff.length > 0) {
      this.pickupMarkers = [];
      this.dropOffMarkers = [];
      this.dropOffMarkers.push([this.dropOffLat, this.dropOffLang]);
      this.pickupMarkers.push([this.pickupLat, this.pickupLang]);
    }

    let latlng = { lat: this.dropOffLat, lng: this.dropOffLang };
    let d = this;
    geocoder.geocode({ location: latlng }, function (results, status) {
      d.direction.destination = String(d.dropOffLat + "," + d.dropOffLang);
      let sourceAddress = d.direction.origin;
      let destinationAddress = d.direction.destination;
      if (results) {
        this.drop_location = results[0].formatted_address;
        d.addDispatcher.controls["drop_location"].setValue(
          results[0].formatted_address
        );
      }

      this.address = [];
      if (results) {
        for (let i = 0; i < results[0].address_components.length; i++) {
          let addressType = results[0].address_components[i].types[0];
          this.address[addressType] =
            results[0].address_components[i].long_name;

          d.addDispatcher.controls["drop_country"].setValue(
            this.address.country
          );
        }
      }

      geocoder.geocode({ address: sourceAddress }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          this.sourceLatitude = results[0].geometry.location.lat();
          this.sourceLongitude = results[0].geometry.location.lng();
          d.addDispatcher.controls["sourceLatLang"].setValue(
            this.sourceLatitude + "," + this.sourceLongitude
          );
        }
      });
      geocoder.geocode({ address: destinationAddress }, function (
        results,
        status
      ) {
        if (status == google.maps.GeocoderStatus.OK) {
          d.destinationLatitude = results[0].geometry.location.lat();
          d.destinationLongtitude = results[0].geometry.location.lng();
          d.addDispatcher.controls["destinationLatLang"].setValue(
            d.destinationLatitude + "," + d.destinationLongtitude
          );
        }
      });
      let selectedMode = "DRIVING";
      let request = {
        origin: sourceAddress,
        destination: destinationAddress,
        travelMode: google.maps.TravelMode[selectedMode]
      };
      d.directionsService.route(request, function (response, status) {
        d.addDispatcher.controls["distance"].setValue(
          response.routes[0]["legs"][0]["distance"]["text"]
        );
        d.addDispatcher.controls["eta"].setValue(
          response.routes[0]["legs"][0]["duration"]["text"]
        );
      });
      d.showDirection();
    });
  }

  onMarkerInit(marker) {
    this.markerData = marker;
    this.marker.lat = marker.getPosition().lat();
    this.marker.lng = marker.getPosition().lng();
    marker.nguiMapComponent.openInfoWindow("iw", marker);
  }

  makeItPickupFromExisting(address) {
    if (this.inPick) {
      for (let i = 0; i < this.picking.length; i++) {
        this.picking[i].setMap(null);
      }
      this.inPick = false;
    }
    const geocoder = new google.maps.Geocoder();
    const latlang = address.geo_locations;
    const latlangSplit = latlang.split(",");
    const latitude = latlangSplit[0];
    const longitude = latlangSplit[1];
    const collectPickup = [];
    const that = this;
    that.pickupLat = latitude;
    that.pickupLang = longitude;

    that.addDispatcher.controls["pickup_location"].setValue(address.address);
    geocoder.geocode({ address: address.address }, function (results, status) {
      that.address = [];
      if (results.length <= 0)
        return;
      for (let i = 0; i < results[0].address_components.length; i++) {
        let addressType = results[0].address_components[i].types[0];
        that.address[addressType] = results[0].address_components[i].long_name;
      }
      that.addDispatcher.controls["pickup_country"].setValue(
        that.address.country
      );
    });

    that.direction.origin = String(that.pickupLat + "," + that.pickupLang);
    that.showDirection();
    collectPickup.push([Number(that.pickupLat), Number(that.pickupLang)]);
    if (collectPickup.length > 0) {
      that.pickupMarkers = [];
      that.pickupMarkers.push([
        Number(that.pickupLat),
        Number(that.pickupLang)
      ]);
    }
  }
  makeItDropoffFromExisting(address) {
    if (this.inDrop) {
      for (let i = 0; i < this.dropOffing.length; i++) {
        this.dropOffing[i].setMap(null);
      }
      this.inDrop = false;
    }
    const geocoder = new google.maps.Geocoder();
    const latlang = address.geo_locations;
    const latlangSplit = latlang.split(",");
    const latitude = latlangSplit[0];
    const longitude = latlangSplit[1];
    const that = this;
    that.dropOffLat = latitude;
    that.dropOffLang = longitude;
    const collectDropoff = [];
    collectDropoff.push([Number(that.dropOffLat), Number(that.dropOffLang)]);
    that.addDispatcher.controls["drop_location"].setValue(address.address);
    geocoder.geocode({ address: address.address }, function (results, status) {
      that.address = [];
      for (let i = 0; i < results[0].address_components.length; i++) {
        let addressType = results[0].address_components[i].types[0];
        that.address[addressType] = results[0].address_components[i].long_name;
      }
      that.addDispatcher.controls["drop_country"].setValue(
        that.address.country
      );
    });

    that.direction.destination = String(
      that.dropOffLat + "," + that.dropOffLang
    );
    if (collectDropoff.length > 0) {
      that.dropOffMarkers = [];
      that.dropOffMarkers.push([
        Number(that.dropOffLat),
        Number(that.dropOffLang)
      ]);
    }
    that.showDirection();
  }
  checkDisTime(mode) {
    if (this.orderUpdateFlag && mode == "pick")
      this.addDispatcher.controls["drop_location"].setValue("");
    else if (this.orderUpdateFlag && mode == "drop")
      this.addDispatcher.controls["pickup_location"].setValue("");
    this.orderUpdateFlag = false;
    if (
      this.addDispatcher.value.pickup_location == "" &&
      this.addDispatcher.value.drop_location == ""
    ) {
      this.flag = true;
      this.addDispatcher.controls["eta"].setValue("");
      this.addDispatcher.controls["distance"].setValue("");
    }
  }
  onMapReady(map) {
    map.setOptions({
      styles: [
        {
          "featureType": "all",
          "elementType": "all",
          "stylers": [
            {
              "hue": "#e7ecf0"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#8ed863"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
            {
              "saturation": -70
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
            {
              "lightness": "48"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "simplified"
            },
            {
              "saturation": -60
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#8abdec"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#9cbbf0"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        }
      ]
    });
    this.map = map;
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    /* google map places auto complete */
    var drop_input: any = document.getElementById('drop-input');
    var autocomplete = new google.maps.places.Autocomplete(drop_input);
    autocomplete.setComponentRestrictions(
      { 'country': ['ae'] });
    let that = this
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
      that.dropChanged(autocomplete.getPlace());
    });
    var pickup_input: any = document.getElementById('pickup-input');
    var autocomplete2 = new google.maps.places.Autocomplete(pickup_input);
    autocomplete2.setComponentRestrictions(
      { 'country': ['ae'] });
    google.maps.event.addListener(autocomplete2, 'place_changed', function () {
      that.pickupChanged(autocomplete2.getPlace());
    });
    var search_input: any = document.getElementById('search-input');
    var autocomplete3 = new google.maps.places.Autocomplete(search_input);
    autocomplete3.setComponentRestrictions(
      { 'country': ['ae'] });
    google.maps.event.addListener(autocomplete3, 'place_changed', function () {
      that.placeChanged(autocomplete3.getPlace());
    });
    /* places autocomplete end */
    this.directionsDisplay.setMap(this.map);
    this.map.addListener('click', () => {
      if (this.infoWindow)
        this.infoWindow.close();
    })
  }

  NotSendMessage(order) {
    if (order.order_status === "search_for_driver") {
      this.messagetoPopup =
        "This Order is search for driver. So you cannot send message to driver.";
    } else {
      this.messagetoPopup =
        "This order already cancelled. So you cannot send message to driver.";
    }
    $("#cancel_message").modal("show");
  }

  /**
   * Schedular
   */
  public onChangeSchedular(data) {
    this.schedulerStaus = data;
    this.onFlagChange(true)
    if (this.schedulerStaus) {
      this.addDispatcher.controls["scheduler_start_date"].setValidators([
        Validators.required
      ]);
      this.addDispatcher.controls["scheduler_start_time"].setValidators([
        Validators.required
      ]);
    } else {
      this.addDispatcher.controls["scheduler_start_date"].setValidators([]);
      this.addDispatcher.controls["scheduler_start_time"].setValidators([]);
    }
  }
  /**
   * Order info amap
   */
  public bounds;
  public ordermap;
  public travelled_cordinates: any = [];
  public deviceid;
  public updated_travelled_cordinates;
  public ordermarkers = [];
  public tripDetails;
  public ordermarkerlen: number;
  public sockettrackno: number;
  public startmarker;
  public destinationmarker;
  public pendingorder;
  public pendinginfoorder = false;
  public polypath;
  public lineSymbol;
  public livetrackingmarker;
  public ordermarker = {
    lat: null,
    lng: null,
    location: "",
    bearing: "",
    speed: "",
    calender: null,
    battery: null,
    date: null,
    is_gps_on: true,
    is_internet_on: true,
    display: true,
    accuracy: ""
  };
  public path = [];
  public mapready = false;
  onMapOrderReady(map) {
    this.mapready = true;
    if (this.mapready) {
      if (this.pendinginfoorder) {
        this.showModel(this.pendingorder, true);
      }
    }
    this.ordermarkers.length = 0;
    this.path.length = 0;
    map.setOptions({
      styles: [
        {
          "featureType": "all",
          "elementType": "all",
          "stylers": [
            {
              "hue": "#e7ecf0"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#8ed863"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
            {
              "saturation": -70
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.icon",
          "stylers": [
            {
              "lightness": "48"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "simplified"
            },
            {
              "saturation": -60
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#8abdec"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#9cbbf0"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        }
      ]
    });
    this.ordermap = map;

  }
  public closeModel() {
    this.maploaded = false;
    this.path.length = 0;
    this.ordermarkers.length = 0;
    this.orderInfo.length = 0;
    this.path.length = 0;
    if (this.livetrackingOrder) {
      clearInterval(this.livetrackingOrder);
    }
  }

  onOrderMarkerInit(marker) {
  }
  onOrderMarkerClick({ target: marker }, marker_details) {
    this.ordermarker.lat = marker.getPosition().lat();
    this.ordermarker.lng = marker.getPosition().lng();
    this.ordermarker.speed = marker_details[3];
    this.ordermarker.date = marker_details[4];
    this.ordermarker.is_gps_on = marker_details[5];
    this.ordermarker.is_internet_on = marker_details[6];
    this.ordermarker.accuracy = marker_details[7];
    this.ordermarker.battery = marker_details[8];
    marker.nguiMapComponent.openInfoWindow("orderinfowindow", marker);
    google.maps.event.addListener(this.ordermap, 'click', () => {
      marker.nguiMapComponent.closeInfoWindow("orderinfowindow")
    });
  }
  public animateRoute() {
    let interval;
    let line = this.polypath;
    var count = 0;
    interval = setInterval(function () {
      //alert(count);
      if (count == 199) {
        clearInterval(interval);
      }
      count = (count + 1) % 200;
      var icons = line.get("icons");
      icons[0].offset = count / 2 + "%";
      line.set("icons", icons);
    }, 50);
  }
  public removeLine() {
    this.polypath.setMap(null);
  }
  public setMapzoom(cordlen: number) {
    if (cordlen < 200) {
      this.ordermap.setZoom(12);
    } else if (cordlen > 200 && cordlen < 500) {
      this.ordermap.setZoom(11);
    } else if (cordlen > 500 && cordlen < 1000) {
      this.ordermap.setZoom(10);
    } else {
      this.ordermap.setZoom(9);
    }
  }
  public orderTotal;
  public orderCompleted;
  public orderCancelled;
  public orderInProgress;
  public orderAccepted;
  public orderScheduledTrip;
  public orderSearch;
  public dataRefresher;
  public dataRefresher2;
  public orderRejected;
  public getAllorderCount() {
    let params1: any;
    if (this.timeBarFlag) {
      if (this.unique_order_id.length !== 0) {
        params1 = {
          offset: 0,
          limit: this.order_limit != '' ? this.order_limit : this.pageSize,
          sortOrder: this.sort_type != '' ? this.sort_type : 'desc',
          keyword: this.unique_order_id ? this.unique_order_id : '',
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
          pickup_location: '',
          drop_location: '',
          partner_id: '',
          start_date: '',
          end_date: '',
          promo_id: ''
        };
      } else {
        let pickup_location: any = '';
        let drop_location: any = '';
        if (this.multiFilter.pickup.length > 0) {
          pickup_location = [];
          this.multiFilter.pickup.forEach(element => {
            pickup_location.push([element.loc.coordinates[0]])
          });
        }
        if (this.multiFilter.drop.length > 0) {
          drop_location = [];
          this.multiFilter.drop.forEach(element => {
            drop_location.push([element.loc.coordinates[0]])
          });
        }
        let drivers = this.multiFilter.driver.map(x => x._id);
        let vehicles = this.multiFilter.side.map(x => x._id);
        let partners = this.multiFilter.partner.map(x => x._id);
        params1 = {
          offset: 0,
          limit: this.order_limit != '' ? this.order_limit : this.pageSize,
          sortOrder: this.sort_type != '' ? this.sort_type : 'desc',
          sortByColumn: this.sort_by_coloumn != '' ? this.sort_by_coloumn : 'updated_at',
          vehicle_id: vehicles.length > 0 ? vehicles : '',
          tariffid: this.tariff_id ? this.tariff_id : '',
          driver_id: drivers.length > 0 ? drivers : '',
          pickup_location: pickup_location,
          drop_location: drop_location,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
          current_order_status: this.current_order_status ? this.current_order_status : '',
          keyword: this.unique_order_id ? this.unique_order_id : '',
          start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
          end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
          userid: this.user_id ? this.user_id : '',
          user_name: this.user_name ? this.user_name : '',
          generate_by: this.generated_by ? this.generated_by : '',
          partner_id: partners.length > 0 ? partners : '',
          model_id: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder._id ? this.vehicleTypeFilterOrder._id : '' : '',
          dispatcher_id: this.dispatcher_id2 ? this.dispatcher_id2 : '',
          promo_id: this.promo_id ? this.promo_id : '',
          payment_type_id: this.payment_type_id ? this.payment_type_id : '',
          cancellation_reason_id: this.cancelReasonId.length != 0 ? this.cancelReasonId : '',
          vehicle_model_ids: this.vehicleChpModelId.length != 0 ? this.vehicleChpModelId : '',
          vehicle_category_ids: this.vehicle_id_list ? this.vehicle_id_list : '',
          order_comment_status: this.orderComment.length != 0 ? this.orderComment : [],
          customer_rating: this.CustomerRate ? this.CustomerRate : '',
          driver_rating: this.userRate ? this.userRate : '',
          order_edited: this.order_edited ? this.order_edited : '',
          flagged_orders: this.flagged_orders ? this.flagged_orders : '',
          is_scheduler: this.is_scheduler ? '1' : '',
          date_filter_order: this.date_filter_order ? this.date_filter_order : 'updated_at',
          guest_user_id: this.guest_user_id ? this.guest_user_id : '',
          guest_user_phone: this.guest_user_phone ? this.guest_user_phone : ''
        };
      }
    }
    else {
      let status = [];
      if (this.statusCloud.length > 0)
        status = this.statusCloud.map(a => a.name)
      params1 = {
        offset: 0,
        limit: this.order_limit != '' ? this.order_limit : this.pageSize,
        current_order_status: status ? status : '',
        keyword: '',
        start_date: this.tagStartDate ? moment(this.tagStartDate).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.tagEndDate ? moment(this.tagEndDate).format('YYYY-MM-DD HH:mm') : '',
        driver_id: this.driver_id ? [this.driver_id] : '',
        vehicle_id: this.vehicle_id2 ? [this.vehicle_id2] : '',
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
        pickup_location: '',
        drop_location: '',
        partner_id: '',
        dispatcher_id: '',
        model_id: '',
        promo_id: '',
        userid: this.timely_user_id ? this.timely_user_id : '',
        user_name: this.timely_user_name ? this.timely_user_name : '',
      }
    }
    //alert(this.session_token);
    if (this.multiFilter.driver_group.length > 0 && !params1.driver_id) {//gets the list of drivers specific to one more driver group
      this.getDriversInDriverGroup(this.multiFilter.driver_group.map(x => x._id)).then((res) => {
        params1.driver_id = res;
        let enc_data = this._EncDecService.nwt(this.session_token, params1);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._orderService.countOrder(data1).then(dec => {
          if (dec && dec.status == 200) {
            var data: any = this._EncDecService.dwt(this.session_token, dec.data);
            this.orderTotal =
              data.count.length > 0 ? data.count[0].total_orders : 0;
            this.orderCompleted =
              data.count.length > 0 ? data.count[0].completed_orders : 0;
            this.orderSearch =
              data.count.length > 0 ? data.count[0].search_order : 0;
            this.orderAccepted =
              data.count.length > 0 ? data.count[0].accepted_by_drivers : 0;
            this.orderInProgress =
              data.count.length > 0 ? data.count[0].trip_started_orders : 0;
            this.orderCancelled =
              data.count.length > 0 ? data.count[0].cancelled_orders : 0;
            if (this.timeBarFlag)
              this.orderScheduledTrip =
                data.count.length > 0 ? data.count[0].scheduled_orders : 0;
            else {
              let temp = data.count.length > 0 ? data.count[0].scheduled_orders : 0;
              this.orderTotal -= temp;
            }
            this.orderRejected =
              data.count.length > 0 ? data.count[0].rejected_orders : 0;
          }
        });
      });
    }
    else {
      let enc_data = this._EncDecService.nwt(this.session_token, params1);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._orderService.countOrder(data1).then(dec => {
        if (dec && dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.orderTotal =
            data.count.length > 0 ? data.count[0].total_orders : 0;
          this.orderCompleted =
            data.count.length > 0 ? data.count[0].completed_orders : 0;
          this.orderSearch =
            data.count.length > 0 ? data.count[0].search_order : 0;
          this.orderAccepted =
            data.count.length > 0 ? data.count[0].accepted_by_drivers : 0;
          this.orderInProgress =
            data.count.length > 0 ? data.count[0].trip_started_orders : 0;
          this.orderCancelled =
            data.count.length > 0 ? data.count[0].cancelled_orders : 0;
          if (this.timeBarFlag)
            this.orderScheduledTrip =
              data.count.length > 0 ? data.count[0].scheduled_orders : 0;
          else {
            let temp = data.count.length > 0 ? data.count[0].scheduled_orders : 0;
            this.orderTotal -= temp;
          }
          this.orderRejected =
            data.count.length > 0 ? data.count[0].rejected_orders : 0;
        }
      });
    }
  }
  refreshOrderCountData() {
    this.dataRefresher = setInterval(() => {
      this.getAllorderCount();
    }, 10000);
  }
  customerOrderFilt = false;
  dispatcherOrderFilt = false;
  public colorFilter(status) {

    this.vehicleChpModel = [];
    this.vehicleChpModelId = [];
    this.cancelReasons = [];
    this.cancelReasonId = [];
    this.vehicle_id_list = [];

    if (status == "All") {
      this.statuses = [];
      this.current_order_status = [];
      this.getOrders(false);
    } else {
      if (status == "customer" || status == "dispatcher" || status == "driver" || status == "website" || status == "kiosk" || status == "hotel/website" || status == "delivery") {
        this.generated_by = status;
        if (status == "customer") {
          this.customerOrderFilt = true;
        } else {
          this.customerOrderFilt = false;
        }
        if (status == "dispatcher") {
          this.dispatcherOrderFilt = true;
        } else {
          this.dispatcherOrderFilt = false;
        }
      }
      else {
        if (this.statuses.indexOf(status) == -1) {
          if (status == 'search_for_driver') {
            this.statuses.push(status, 'search_for_driver_manual');
            this.current_order_status.push(status, 'search_for_driver_manual');
          }
          else {
            this.statuses.push(status);
            this.current_order_status.push(status);
          }
        }
      }
      this.getOrders(false);
    }
    this.socketFlag = false;
  }
  public statusColor(data) {
    //alert(data);
    //'new', 'accepted_request', 'waiting_for_customer', 'dispatcher_cancel', 'accepted_by_driver', 'rejected_by_driver',
    //'trip_started', 'confirming_rate', 'selecting_payment', 'rating', 'completed', 'cancel_acknowledged'
    if (data == "Waiting for customer" || data == "waiting_for_customer") {
      data = "#4cb050";
      return data;
    }
    if (data == "rejected_by_driver" || data == "Rejected") {
      data = "#b0bfc6";
      return data;
    }
    if (data == "confirming_rate" || data == "Confirming rate") {
      data = "#b39ddb";
      return data;
    }
    if (data == "trip_started" || data == "In Progress") {
      data = "#8bc24a";
      return data;
    }
    if (data == "selecting_payment" || data == "Selecting payment") {
      data = "#ff8a66";
      return data;
    }
    if (data == "customer_cancel" || data == "Customer cancel") {
      data = "#f5f5f5";
      return data;
    }
    if (data == "selecting_payment" || data == "Selecting payment") {
      data = "#ff8a66";
      return data;
    }
    if (data == "rating" || data == "Rating") {
      data = "#f48fb1";
      return data;
    } else if (data == "accepted" || data == "Accepted") {
      data = "#81b64e";
      return data;
    } else if (data == "canceled" || data == "Canceled") {
      data = "#2e2e2e";
      return data;
    }
    else if (data == "order_timed_out" || data == "Order timeout") {
      data = "#795547";
      return data;
    }
    else if (data == "driver_cancel" || data == "Driver cancel") {
      data = "#8c6e63";
      return data;
    }
    if (data == "dispatcher_cancel" || data == "Dispatcher cancel") {
      data = "#fe5722";
      return data;
    } else if (data == "completed" || data == "Completed") {
      data = "#42a5f6";
      return data;
    } else if (data == "search_for_driver" || data == "Search For Driver") {
      data = "#c9cdcf";
      return data;
    } else if (data == "search_for_driver_manual" || data == "Search For Driver Manual") {
      data = "#bbb";
      return data;
    } else if (data == "accepted_by_driver" || data == "Accepted") {
      data = "#00e676";
      return data;
    } else if (data == "scheduled_trip" || data == "Scheduled") {
      data = "#ffff00";
      return data;
    } else if (data == "dispatcher" || data == "customer" || data == "driver" || data == "website" || data == "kiosk" || data == "hotel/website" || data == "delivery") {
      if (this.generated_by == data) data = "rgb(191, 0, 0)";
      else data = "#211c47";
      return data;
    }
  }
  public checkOrderIsAllowed() {
    //var dest_latlngarray = droplocation.split(',');
    //var dlat = parseFloat(dest_latlngarray[0]);
    //var dlng = parseFloat(dest_latlngarray[1]);
    if (this.addDispatcher.controls["tariff_price"].value) {
      if (this.addDispatcher.controls["tariff_price"].value < 1) {
        this.toastr.error('The price should be more than 1')
        return false;
      }
    }
    var slat = this.pickupLat;
    var slng = this.pickupLang;
    if (this.addDispatcher.value.sourceLatLang == '' || !this.addDispatcher.value.sourceLatLang) {
      this.addDispatcher.controls["sourceLatLang"].setValue(
        slat + "," + slng
      );
    }
    //var destlatlng = new google.maps.LatLng(dlat, dlng);
    var sourcelatlng = new google.maps.LatLng(slat, slng);
    //alert(sourcelatlng);
    let coverage_array = this.coverageData;
    if (coverage_array.length > 0) {
      for (var i = 0; i < coverage_array.length; ++i) {
        if (
          coverage_array[i].bounds.length > 0 &&
          coverage_array[i].bounds != ""
        ) {
          let bounds = coverage_array[i].bounds;
          //alert(JSON.stringify(bounds));
          let bounds_array = [];
          bounds_array.length = 0;
          for (var j = 0; j < bounds.length; ++j) {
            bounds_array.push(
              new google.maps.LatLng(bounds[j].lat, bounds[j].lng)
            );
          }
          //alert(JSON.stringify(bounds_array));
          var bermudaTriangle = new google.maps.Polygon({
            paths: bounds_array
          });
          //var destination_inside_value = google.maps.geometry.poly.containsLocation(destlatlng, bermudaTriangle)
          var source_inside_value = google.maps.geometry.poly.containsLocation(
            sourcelatlng,
            bermudaTriangle
          );
          //alert(source_inside_value);
          if (i === coverage_array.length - 1) {
            if (!source_inside_value) {
              this.toastr.error("Trips starting outside coverage area not allowed");
              return false;
            } else {
              return true;
            }
          }
        }
      }
    } else {
      return true;
    }
  }
  public since(dt: any) {
    //var uaeTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Dubai"});
    let date1 = new Date(dt).getTime();
    let date2 = new Date(this.uaeTime).valueOf();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";

    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output += "(" + Math.round(var1) + " secs ago)";
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      output += "(" + Math.round(var1) + " mins ago)";
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      output += "(" + Math.round(var1) + " hrs ago)";
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output += "(" + Math.round(var1) + " days ago)";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output += "(" + Math.round(var1) + " months ago)";
    }
    return hoursDiff < 0 ? '0 secs ago' : output;
  }
  public doCenter(place: any) {
    this.center = place;
  }

  myOldTrip(pickup_location, drop_location) {
    for (var i = 0; i < this.picking.length; i++) {
      this.picking[i].setMap(null);
    }

    for (let i = 0; i < this.dropOffing.length; i++) {
      this.dropOffing[i].setMap(null);
    }
    this.directionsDisplay.setMap(null);
    this.pickupMarkers = [];
    this.pickupMarkers.push(pickup_location)
    this.doCenter(pickup_location);
    let geocoder = new google.maps.Geocoder();
    if (pickup_location !== "" && drop_location !== "") {
      this.directionsDisplay.setMap(this.map);
      this.directionsDisplay.setOptions({ suppressMarkers: true });
      this.pickupMarkers = [];
      this.dropOffMarkers = [];
      let sourceAddress = pickup_location;
      let destinationAddress = drop_location;
      let d = this;
      geocoder.geocode({ address: sourceAddress }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          this.sourceLatitude = results[0].geometry.location.lat();
          this.sourceLongitude = results[0].geometry.location.lng();
          d.addDispatcher.controls["sourceLatLang"].setValue(
            this.sourceLatitude + "," + this.sourceLongitude
          );
          d.pickupMarkers = [];
        }
      });
      geocoder.geocode({ address: destinationAddress }, function (
        results,
        status
      ) {
        if (status == google.maps.GeocoderStatus.OK) {
          d.destinationLatitude = results[0].geometry.location.lat();
          d.destinationLongtitude = results[0].geometry.location.lng();
          d.addDispatcher.controls["destinationLatLang"].setValue(
            d.destinationLatitude + "," + d.destinationLongtitude
          );
          d.dropOffMarkers = [];
        }
      });

      let selectedMode = "DRIVING";
      let request = {
        origin: sourceAddress,
        destination: destinationAddress,
        travelMode: google.maps.TravelMode[selectedMode]
      };
      this.directionsService.route(request, function (response, status) {
        if (response.routes.length === 0) {
          d.toastr.error("Invalid path!");
          d.directionsDisplay.setMap(null);
          d.pickupMarkers = [];
          d.dropOffMarkers = [];
          d.addDispatcher.controls["distance"].setValue("");
          d.addDispatcher.controls["eta"].setValue("");
          d.directionsDisplay.setMap(null);
          d.addDispatcher.controls["pickup_location"].setValue("");
          d.addDispatcher.controls["drop_location"].setValue("");
          d.addDispatcher.controls["pickup_country"].setValue("");
          d.addDispatcher.controls["drop_country"].setValue("");
          d.pickupLat = "";
          d.pickupLang = "";
          d.dropOffLang = "";
          d.dropOffLat = "";
          for (let i = 0; i < d.picking.length; i++) {
            d.picking[i].setMap(null);
          }
          for (let i = 0; i < d.dropOffing.length; i++) {
            d.dropOffing[i].setMap(null);
          }
        } else {
          d.addDispatcher.controls["distance"].setValue(
            response.routes[0]["legs"][0]["distance"]["text"]
          );
          d.addDispatcher.controls["eta"].setValue(
            response.routes[0]["legs"][0]["duration"]["text"]
          );
          d.directionsDisplay.setDirections(response);

          d.makePickupMarker(
            response.routes[0]["legs"][0].start_location,
            "title",
            "pickup"
          );
          d.makeDropOffMarker(
            response.routes[0]["legs"][0].end_location,
            "title",
            "dropOff"
          );
        }
      });
    }

    if (this.flag) {
    }
  }
  showReassignOrder(order) {
    this.orderInfo.length = 0;
    let params1 = {
      company_id: this.company_id_array,
      _id: order
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params1);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOrderById(data1).then(dec => {
      if (dec && dec.status == 200) {
        var orderData: any = this._EncDecService.dwt(this.session_token, dec.data);
        let that = this;
        that.orderInfo = orderData.response.SingleDetail;
        that.orderInfo.dispatcher = this.dispatcher;
        that.orderInfo.company_id = this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        this.dialog.closeAll();
        let dialogRef = this.dialog.open(ReassignComponent, {
          width: "600px",
          hasBackdrop: true,
          data: that.orderInfo,
          scrollStrategy: this.overlay.scrollStrategies.noop()
        });
      }
    });
  } /*
  tariffListchanged(data){
    console.log(this.addDispatcher.value)
    if (this.addDispatcher.value.tariffList != null) {
      if (this.addDispatcher.value.tariffList.name === 'Fixed Rate') {
        this.addDispatcher.controls['tariff_price'].enable();
        this.addDispatcher.controls['multiple'].setValue(1);
      } else {
        this.addDispatcher.controls['tariff_price'].disable();
        this.addDispatcher.controls['tariff_price'].setValue('');
        this.addDispatcher.controls['price'].setValue(0);
        this.total_sum();
      }
    }
  }*/
  compareObjects(o1: any, o2: any): boolean {
    return o1.name === o2.name && o1.id === o2.id;
  }
  hideCollapse(id) {
    if (this.collapseItem && this.collapseItem != id)
      $("#" + this.collapseItem).collapse("hide");
    this.collapseItem = id;
  }
  displayDriverOnMap(lat, lng, id, image, i) {
    let position = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
    let markerIds = this.driverMarkers.map(function (marker) {
      return marker.id;
    });
    let markerExists = markerIds.indexOf(id) !== -1;
    if (markerExists) {
      let marker = this.driverMarkers.filter(function (marker) {
        return marker.id === id;
      });
      if (marker.length > 0) {
        this.slideMarker(
          marker[0].marker,
          [position.lat(), position.lng()],
          image,
          i
        );
      }
    } else {
      var rotation = Math.floor(Math.random() * 350) + 1;
      var img = new Image();
      img.src = image || "../../../../assets/img/map/car-black-top.png";
      img.onload = () => {
        let icon = new RotateIcon({ img: img, pixel: this.pixel })
          .setRotation({ deg: rotation })
          .getUrl();
        let marker = new google.maps.Marker({
          position: position,
          map: this.map,
          icon: icon
        });
        let driver = this.deviceLocationData.filter(function (driver) {
          return driver["device_id"] === id;
        });
        if (this.vehicle_id) {
          if (!this.SingledeviceLocationData[0].driver_id || this.SingledeviceLocationData[0].driver_id == null)
            marker.addListener("mouseover", () => {
              this.clickedSignedOut(marker, driver[0], image);
            });
          else
            marker.addListener("mouseover", () => {
              this.clicked(marker, driver[0], image);
            });
        }
        else if (this.mapFilter !== '8')
          marker.addListener("mouseover", () => {
            this.clicked(marker, driver[0], image);
          });
        else {
          marker.addListener("mouseover", () => {
            this.clickedSignedOut(marker, driver[0], image);
          });
        }
        marker.addListener("dblclick", () => {
          this.shiftpop(this.deviceLocationData[i].vehicle_id);
        });
        marker.addListener("click", () => {
          this.setDriverFilter({ 'vehicle_id': driver[0].vehicle_id });
        });
        this.driverMarkers.push({ id: id, marker: marker, image: img });
      };
    }

  }

  removeStaleDriversFromMap() {
    var driverIds = [];
    if (this.singleVehicleTracking) {
      driverIds = this.SingledeviceLocationData.map(function (device) {
        return device["device_id"];
      });
    } else {
      driverIds = this.deviceLocationData.map(function (device) {
        return device["device_id"];
      });
    }
    var staleDrivers = [];
    for (var i = 0; i < this.driverMarkers.length; i++) {
      if (driverIds.indexOf(this.driverMarkers[i].id) === -1) {
        staleDrivers.push(i);
      }
    }
    for (var i = 0; i < staleDrivers.length; i++) {
      this.driverMarkers[staleDrivers[i]].marker.setMap(null);
    }
    this.driverMarkers = this.driverMarkers.filter(function (marker, index) {
      return staleDrivers.indexOf(index) === -1;
    });
  }
  slideMarker(marker, target, image, count) {
    let steps = 20;
    let delay = 20;
    var i = 0;
    var position = [marker.getPosition().lat(), marker.getPosition().lng()];
    let deltaLat = (target[0] - position[0]) / steps;
    let deltaLng = (target[1] - position[1]) / steps;
    var rotation = Math.atan2(deltaLng, deltaLat) * (180 / Math.PI);
    var img = new Image();
    img.src = image || "../../../../assets/img/map/car-black-top.png";
    img.onload = () => {
      let chain = Promise.resolve();
      let icon;
      chain
        .then(() => {
          icon = new RotateIcon({ img: img, pixel: this.pixel })
            .setRotation({ deg: rotation })
            .getUrl();
        })
        .then(() => this.Wait3(count * 3))
        .then(() => {
          marker.setIcon(icon);
          var moveMarker = function () {
            position[0] += deltaLat;
            position[1] += deltaLng;
            var newPosition = new google.maps.LatLng(position[0], position[1]);
            marker.setPosition(newPosition);
            if (i != steps) {
              i++;
              setTimeout(moveMarker, delay);
            }
          };
          moveMarker();
        });
    };
  }
  public infoMarker = null;
  clicked(marker, detail, icon) {
    this.check_interval = "no";
    let device_id = detail["device_id"];
    const params = {
      device_id: device_id,
      company_id: this.company_id_array
    };
    this.marker.pickup_location = "";
    this.marker.shift_end = "";
    if (!this.infoWindow) {
      this.infoWindow = new google.maps.InfoWindow();
      let that = this;
      this.infoWindow.addListener('closeclick', function () {
        if (that.infoMarker)
          that.infoMarker.setMap(null);
        that.infoMarker = null;
        that.directionsDisplay.setMap(null);
      });
    }
    if (device_id) {
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._shiftsService.getAssignedDrivers(data1).then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          let popupData = data.device_location_data;
          //let correctDriver = popupData.driver_id.username
          let status;
          if (popupData) {
            if (popupData.vehicle_id)
              if (popupData.vehicle_id.online_status == "1") {
                status = "online";
              } else {
                status = "offline";
              }
            if (this.infoMarker)
              this.infoMarker.setMap(null)
            this.infoMarker = null;
            this.infoWindow.setContent("");
            this.marker.updated_at = data.device_location_data.updated_at;
            this.marker.device_id = popupData.unique_device_id;
            this.marker.device_model = popupData.device_model;
            this.marker.vehicle_id = popupData.vehicle_id
              ? popupData.vehicle_id.vehicle_identity_number
              : "";
            this.marker.device_type = status;
            this.marker.speed = parseInt(data.device_location_data.speed);
            this.marker.gps_strength = String(
              parseInt(data.device_location_data.gps_strength)
            );
            this.marker.battery = data.device_location_data.battery;
            this.marker.shift_start = popupData.shift_log_id ? popupData.shift_log_id.start_time : '';
            this.marker.imei = popupData.imei;
            this.marker.vehicle_name = popupData.vehicle_id
              ? String(
                popupData.vehicle_id.display_name
              ).toUpperCase() : '';
            this.marker.vehicle_make = popupData.vehicle_id ? popupData.vehicle_id.car_manufacturer_desc
              ? String(popupData.vehicle_id.car_manufacturer_desc).toUpperCase()
              : "" : "";
            this.marker.vehicle_model = popupData.vehicle_id
              ? String(
                popupData.vehicle_id.vehicle_model_id.name
              ).toUpperCase() : "";
            this.marker.vehicle_fuel_type = popupData.vehicle_id
              ? popupData.vehicle_id.car_fuel_type : '';
            this.marker.company = "Dubai Taxi";
            this.marker.driver_name = popupData.driver_id ?
              String(
                popupData.driver_id.display_name
              ).toUpperCase() : '';
            this.marker.driver_id = popupData.driver_id
              ? popupData.driver_id._id : '';
            this.marker.emp_id = popupData.driver_id
              ? popupData.driver_id.emp_id : '';
            this.marker.driverphone = popupData.driver_id
              ? popupData.driver_id.phone_number : '';
            this.marker.status = this.vehicleStatus[icon];
            if (popupData.order_id) {
              this.marker.pickup_location = popupData.order_id.pickup_location;
              if (popupData.order_id.order_status == 'accepted_by_driver') {
                //customer_selected_pickup_lat_long
                let latlngvalue = popupData.order_id.customer_pickup_lat_long.split(",");
                let marker = new google.maps.Marker({
                  position: new google.maps.LatLng(parseFloat(latlngvalue[0]), parseFloat(latlngvalue[1])),
                  map: this.map,
                  icon: "assets/img/marker-red.png",
                  title: "Customer Location"
                });
                this.picking.push(marker);
                let sourceAddress = detail.latitude + ',' + detail.longitude;
                let request = {
                  origin: sourceAddress,
                  destination: popupData.order_id.customer_pickup_lat_long,
                  travelMode: google.maps.TravelMode.DRIVING
                };
                this.directionsDisplay.setMap(this.map);
                var polylineOptionsActual = new google.maps.Polyline({
                  strokeColor: '#000000'
                });
                this.directionsDisplay.setOptions({ suppressMarkers: true });
                this.directionsDisplay.setOptions({ polylineOptions: polylineOptionsActual });
                let d = this;
                this.directionsService.route(request, function (response, status) {
                  d.directionsDisplay.setDirections(response);
                  d.directionsDisplay.setOptions({ suppressMarkers: true });
                });
                this.infoMarker = marker;
              }
            }
            if (this.infoMarker)
              this.infoMarker.setMap(null)
            this.infoMarker = null;
            this.infoWindow.setContent("");
            this.marker.updated_at = data.device_location_data.updated_at;
            this.marker.device_id = popupData.unique_device_id;
            this.marker.device_model = popupData.device_model;
            this.marker.vehicle_id = popupData.vehicle_id
              ? popupData.vehicle_id.vehicle_identity_number
              : "";
            this.marker.device_type = status;
            this.marker.speed = parseInt(data.device_location_data.speed);
            this.marker.gps_strength = String(
              parseInt(data.device_location_data.gps_strength)
            );
            this.marker.battery = data.device_location_data.battery;
            this.marker.shift_start = popupData.shift_log_id ? popupData.shift_log_id.start_time : '';
            this.marker.imei = popupData.imei;
            this.marker.vehicle_name = popupData.vehicle_id
              ? String(
                popupData.vehicle_id.display_name
              ).toUpperCase() : '';
            this.marker.vehicle_make = popupData.vehicle_id ? popupData.vehicle_id.car_manufacturer_desc
              ? String(popupData.vehicle_id.car_manufacturer_desc).toUpperCase()
              : "" : "";
            this.marker.vehicle_model = popupData.vehicle_id
              ? String(
                popupData.vehicle_id.vehicle_model_id.name
              ).toUpperCase() : "";
            this.marker.vehicle_fuel_type = popupData.vehicle_id
              ? popupData.vehicle_id.car_fuel_type : '';
            this.marker.company = "Dubai Taxi";
            this.marker.driver_name = popupData.driver_id ?
              String(
                popupData.driver_id.display_name
              ).toUpperCase() : '';
            this.marker.driver_id = popupData.driver_id
              ? popupData.driver_id._id : '';
            this.marker.emp_id = popupData.driver_id
              ? popupData.driver_id.emp_id : '';
            this.marker.driverphone = popupData.driver_id
              ? popupData.driver_id.phone_number : '';
            this.marker.status = this.vehicleStatus[icon];
            if (popupData.order_id) {
              this.marker.pickup_location = popupData.order_id.pickup_location;
              // if (popupData.order_id.order_status == 'accepted_by_driver') {
              //   let latlngvalue = popupData.order_id.customer_pickup_lat_long.split(",");
              //   let map = this.map;
              //   let marker = new google.maps.Marker({
              //     position: new google.maps.LatLng(latlngvalue[0], latlngvalue[1]),
              //     map: map,
              //     icon: "../assets/img/marker-red.png",
              //     title: "Customer Location"
              //   });
              //   this.picking.push(marker);
              //   let sourceAddress = detail.latitude + ',' + detail.longitude;
              //   let request = {
              //     origin: sourceAddress,
              //     destination: popupData.order_id.customer_pickup_lat_long,
              //     travelMode: google.maps.TravelMode.DRIVING
              //   };
              //   this.directionsDisplay.setMap(this.map);
              //   let d = this;
              //   this.directionsService.route(request, function (response, status) {
              //     d.directionsDisplay.setDirections(response);
              //     d.directionsDisplay.setOptions({ suppressMarkers: true });
              //   });
              //   this.infoMarker = marker;
              // }
            }
            else {
              this.marker.pickup_location = "";
              if (this.infoMarker)
                this.infoMarker.setMap(null);
              this.infoMarker = null;
              let that = this;
              // google.maps.event.addListener(this.infoWindow, 'domready', function () {
              //   var clickableItem = document.getElementById('clicka');
              //   clickableItem.addEventListener('click', () => {
              //     that.shiftpop(popupData.vehicle_id)
              //   })
              // });
            }
            this.marker.display = true;
            this.ref.detectChanges();
            let infoWindowContent = <Element>(
              document.getElementById("iw1").cloneNode(true)
            );
            infoWindowContent.id = "iw-2";
            infoWindowContent.setAttribute("style", "display:block;");
            this.infoWindow.setContent(infoWindowContent);
            this.infoWindow.open(this.map, marker);
            this.infoMarker = null;
            let that = this;
          } else {
            this.marker.device_id = "";
            this.marker.device_model = "";
            this.marker.vehicle_id = "";
            this.marker.device_type = "";
            this.marker.imei = "";
            this.marker.vehicle_name = "";
            this.marker.vehicle_make = "";
            this.marker.vehicle_model = "";
            this.marker.vehicle_fuel_type = "";
            this.marker.company = "Dubai Taxi";
            this.marker.driver_image = "/assets/img/map/driver.jpg";
            this.marker.driver_name = "";
            this.marker.driver_id = "";
            this.marker.emp_id = "";
            this.marker.driverphone = "";
            this.marker.speed = 0;
            this.marker.gps_strength = "";
            this.marker.battery = "";
            this.marker.vehicle_image =
              "/assets/img/map/1428584997LEXUS_ES350.jpg";
            this.marker.pickup_location = "";
            this.marker.status = ""
          }
        }
      });
    }
  }
  getVehicleStatus(vehicle) {
    if (vehicle["booked_status"] === "1") {
      return "booked";
    } else if (vehicle["online_status"] !== "1" && vehicle["booked_status"] !== "1") {
      if (vehicle["pause_reason"] == 'uber/careem')
        return "uber_careem"
      else if (vehicle["pause_reason"] == 'break')
        return "break";
      else
        return "offline";
    } else if (vehicle["booked_status"] !== "1" && vehicle["online_status"] === "1" && vehicle["occupied_status"] === "1") {
      return "free";
    }
    else {
      return "loggedIn";
    }
  }
  public setVehicleTypeFilter(event) {
    this.vehicleTypeFilter = event.value;
    this.loadingIndicator = "";
    this.setMapFilter(this.mapFilter);
  }
  public setVehicleModelFilter(event) {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: "desc",
      sortByColumn: "updated_at",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this.vehicleData = [];
    this.addDispatcher.get("driverList").setValue("");
    this.addDispatcher.get("vehicleList").setValue("");
    if (typeof event === "object") {
      this.vehicleTypeFilter2 = event;
    } else if (typeof event === "string") {
      this._vehicleModelsService.getVehicleModels(data1).then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.modelList2 = data.vehicleModelsList;
        }
        //alert(this.selectDriverData.length)
      });
    }
  }
  public internetFlag = false;
  public gpsFlag = false;
  public setVehicleStatusFilter(vehicleType) {
    if (vehicleType != '6')
      this.tripStatus = [];
    this.hidden = true;
    this.driverFilter = "";
    this.vehicleFilter = "";
    this.sideFilter = "";
    this.internetFlag = this.gpsFlag = false;
    if (vehicleType == '0') {
      this.internetFlag = true;
      this.mapFilter = '';
      this.hidden = false;
    }
    else if (vehicleType == '2') {
      this.gpsFlag = true;
      this.mapFilter = '';
    }
    else
      this.mapFilter = vehicleType;
    this.setMapFilter(this.mapFilter);
  }
  public refreshMap() {
    this.loggedLength = 0;
    this.bookedLength = 0;
    this.offlineLength = 0;
    this.freeLength = 0;
    this.acceptedLength = 0;
    this.almostFreeLength = 0;
    this.logged_out = 0;
    this.normal_pause = 0;
    this.uber_careem = 0;
    this.showCover = false;
    this.internetFlag=false;
    this.gpsFlag=false;
    this.side = '';
    (<HTMLInputElement>document.getElementById("schedulerdis")).style.display =
      "block";
    $("#dropdownMenuButton").text(this.mapFilterLabels[7]);
    if (this.directionsDisplay)
      this.directionsDisplay.setMap(null);
    this.vehicle_id = "";
    this.vehicleFilter = "";
    this.driverFilter = "";
    this.sideFilter = "";
    this.showPopInfo = false;
    this.vehicleTypeFilter = "";
    this.pickupMarkers = [];
    this.vehicle_category_ids=[];
    this.driverGroupMap="";
    this.dropOffMarkers = [];
    this.pickupLat = "";
    this.pickupLang = "";
    this.dropOffLang = "";
    this.dropOffLat = " ";
    this.addDispatcher.controls["pickup_location"].setValue("");
    this.addDispatcher.controls["drop_location"].setValue("");
    this.addDispatcher.controls["vehicleList"].setValue("");
    this.addDispatcher.controls["driverList"].setValue("");
    this.addDispatcher.controls["vehicleList"].updateValueAndValidity();
    this.addDispatcher.controls["driverList"].updateValueAndValidity();
    this.mapFilter = "7";
    this.locationmsg = "";
    this.showOrderTrack = false;
    this.loadingIndicator = '';
    this.hidden = true;
    for (let i = 0; i < this.picking.length; i++) {
      this.picking[i].setMap(null);
    }
    for (let i = 0; i < this.dropOffing.length; i++) {
      this.dropOffing[i].setMap(null);
    }
    for (let i = 0; i < this.driverMarkers.length; i++) {
      this.driverMarkers[i].marker.setMap(null);
    }
    for (let i = 0; i < this.travelled_markers.length; i++) {
      this.travelled_markers[i].setMap(null);
      if (i == this.travelled_markers.length - 1) this.travelled_markers = [];
    }
    this.driverMarkers = [];
    this.uberFlag = false;
    this.breakFlag = false;
    if (this.infoMarker)
      this.infoMarker.setMap(null);
    this.infoMarker = null;
    if (this.directionsDisplay)
      this.directionsDisplay.setMap(null);
    this.vehicle_category_ids = [];
    this.company_id_list2 = this.companyData.slice().map(x => x._id);
    this.setMapFilter("7");
    this.loadingIndicator = '';
  }
  private setFilterLoading(
    filterSelector: string,
    isLoading: Boolean,
    newText: string
  ) {
    if (!$(filterSelector)) {
      return;
    }
    if (isLoading) {
      $(filterSelector).text("Reloading vehicles...");
    } else {
      $(filterSelector).text(newText || "Loading complete.");
    }
  }
  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
  }

  drop(ev) {
    try {
      ev.preventDefault();
      var data = ev.dataTransfer.getData("text");
      ev.target.appendChild(document.getElementById(data));
    } catch (error) { }
  }
  clickVehicle(data) {
    const event = { _id: data };
    event._id = data;
    this.showPopInfo = true;
    this.setVehicleFilterNew(event);
  }
  public setVehicleTypeFilterOrder(event) {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: "desc",
      sortByColumn: "updated_at",
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    if (typeof event === "object") {
      this.vehicleTypeFilterOrder = event._id;
    } else if (typeof event === "string") {
      this.modelMatLoading = true;
      this._vehicleModelsService.getVehicleModels(data1).then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.modelList = data.vehicleModelsList;
        }
        this.modelMatLoading = false;
      });
    }
  }
  public searchPartner(data) {
    if (typeof data !== "object") {
      this.partnerData2 = data ? this.tempPartner.filter(s => (s && new RegExp(`${data}`, 'gi').test(s.name)))
        : this.tempPartner;
    } else {
      this.partner = data._id;
      if (this.multiFilter.partner.findIndex(x => x._id == data._id) == -1)
        this.multiFilter.partner.push(data);
    }
  }
  imageError(event, src) {
    event.target.src = src;
  }
  modelChips: any[] = [];
  removableModel = true;
  modelFlag = true;
  removeModel(model): void {
    const index = this.modelChips.indexOf(model);
    if (index >= 0) {
      this.modelChips.splice(index, 1);
    }
  }
  selectedModel(event: MatAutocompleteSelectedEvent): void {
    if (this.modelChips.indexOf(event.option.value) > -1) {
      return;
    }
    if (!this.addDispatcher.get('tarrifList').value) {
      this.tariffData = this.tariffDataTemp.slice()
      var flag = -1;
      var i = this.tariffData.length;
      while (i--) {
        flag = this.tariffData[i].vehicle_model_id.findIndex(
          x => x._id == event.option.value._id
        );
        if (flag == -1) {
          this.tariffData.splice(i, 1);
        }
        this.modelFlag = false;
      }
    }
    this.modelChips.push(event.option.value);
    this.modelCtrl.setValue(null);
  }
  getScheduledTime(myEndDateTime, durationInMinutes) {
    var myStartDate = new Date(myEndDateTime);
    return moment(
      new Date(
        myStartDate.setMinutes(myStartDate.getMinutes() - durationInMinutes)
      )
    ).format("YYYY-MM-DD, h:mm:ss a");
  }
  getBeforeTime(dispatch, delay) {
    var then = moment(dispatch).subtract(delay, 'minutes').format('DD-MM-YYYY, hh:mm:ss a');
    return then;
  }
  getSchedules(dispatch, delay) {
    const now = moment(new Date).tz('Asia/Dubai').format('YYYY-MM-DD, HH:mm:ss');
    const then = moment(dispatch).subtract(delay, 'minutes').format('YYYY-MM-DD, HH:mm:ss');
    var day = moment(then, "YYYY-MM-DD, HH:mm:ss").diff(moment(now, "YYYY-MM-DD, HH:mm:ss"), 'hours');
    if (day > 0 && day > 24) {
      var days = '( ' + Math.floor(day / 24) + ' days to dispatch ' + ')';
      return days;
    } if (day > 0 && day < 24) {
      var time = moment.utc(moment(then, "YYYY-MM-DD HH:mm:ss").diff(moment(now, "YYYY-MM-DD HH:mm:ss"))).format("YYYY-MM-DD HH:mm:ss");
      var hh = moment(time).hours();
      var mm = moment(time).minutes();
      var ss = moment(time).seconds();
      var timeFormated = '(' + (hh != 0 ? hh + ' Hours ' : '') + (mm != 0 ? mm + ' minutes ' : '') + (ss != 0 ? ss + ' seconds' : '0 seconds') + ' to dispatch ' + ')';
      return timeFormated;

    } else {
      return;
    }
  }

  public getRefreshTime(param) {
    let value = 40000;
    clearInterval(this.refreshTime);
    switch (param) {
      case "1":
        value = 60000;
        break;
      case "3":
        value = 120000;
        break;
      case "5":
        value = 60000;
        break;
      case "6":
        value = 60000;
        break;
      default:
        value = 120000;
    }
    let that = this;
    that.refreshTime = setInterval(function () {
      if (that.vehicle_id) {
        //that.pixel = 40;
        that.getVehicleForSocket(that.vehicle_id);
      } else {
        that.pixel = 20;
        that.setMapFilter(that.mapFilter);
      }
    }, value);
  }
  displayVehicleModel(models) {
    let list = '';
    if (models.length > 0)
      for (let i = 0; i < models.length; i++) {
        let index = this.modelList4.findIndex(x => x._id == models[i])
        if (index !== -1)
          list += this.modelList4[index].name + ' ';
      }
    return list;
  }
  changeDriverGroupsMap(event) {
    let t3 = [];
    let PositionArray = [];
    this.driverGroupMap = "";
    if (event.value == 'All') {
      this.setMapFilter(this.mapFilter)
      return;
    }
    let index = this.tempdriverGroupData.findIndex(x => x._id == event.value);
    PositionArray.push(index);
    for (let j = this.tempdriverGroupData.length - 1; j >= 0; j--) {
      let index = PositionArray.indexOf(j)
      if (index !== -1)
        this.driverGroupMap = this.driverGroupMap + "1";
      else
        this.driverGroupMap = this.driverGroupMap + "0";
    }
    let temp_ids = this.driverGroupMap;
    temp_ids = temp_ids.replace(/^0+/, '');
    let temp_before_id = temp_ids;
    t3.push(temp_ids);
    if (this.tempdriverGroupData.length > temp_ids.length) {
      for (let j = 0; j < this.tempdriverGroupData.length - temp_ids.length; j++) {
        temp_before_id = "0" + temp_before_id;
        t3.push(temp_before_id);
      }
    }
    this.driverGroupMap = t3;
    this.setMapFilter(this.mapFilter)
  }
  iconColor(dt: any) {
    var uaeTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Dubai" });
    let date1 = new Date(dt).getTime();
    let date2 = new Date(uaeTime).getTime();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";
    if (hoursDiff <= 180) {
      output = "green";
    } else if (hoursDiff < 60 * 60) {
      output = "orange";
    } else {
      output = "red";
    }
    return output;
  }

  onSelectedStatusTag(status) {
    if (this.allStatus.findIndex(x => x.name == status.name) == -1) {
      let index = this.statusCloud.indexOf(status);
      this.statusCloud.splice(index, 1);
    }
  }
  public getZones() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._zoneService.getZones(data1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.zones = data.zones
        this.tempZones = data.zones;
      }
    });
  }
  public timeBarFlag = true;
  resetTagOrders(flag) {
    this.tagStartDate = new Date(Date.now() - 86400000);
    this.tagEndDate = new Date(Date.now() + 86400000);
    //this.statusCloud = [{ name: "scheduled_trip", value: "scheduled_trip", display: "Scheduled" }, { name: "search_for_driver", value: "search_for_driver", display: "Search For Driver" }, { name: "search_for_driver_manual", value: "search_for_driver_manual", display: "Search For Driver Manual" }, { name: "trip_started", value: "trip_started", display: "In Progress" }];
    this.tagOrders = [];
    this.statusCount = [];
    this.tagOrdersLength = 0;
    this.driver_id = '';
    this.driver_id3 = '';
    this.unique_order_id = [];
    this.ordersFilter = [];
    this.countTag = 0;
    this.plateFilter2 = "";
    this.vehicle_id2 = "";
    this.drop_loc = '';
    this.pickup_loc = '';
    this.timely_user_id = '';
    this.timely_user_name = '';
    this.timely_user_phone = '';
    let i = 0;
    while (i < this.orders.length) {
      this.orders[i].checked = false;
      i++;
    }
    this.selectedOrder = [];
    if (!flag) {
      this.OrderUpdateStatusText = this.OrderUpdateStatusOnText;
      this.bgcolorstatus = this.bgcoloron;
      this.timeBarFlag = false;
      this.getTagOrders(1);
      this.refreshTagOrderCountData();
      this.getAllorderCount();
    }
    else {
      this.timeBarFlag = true;
      clearInterval(this.dataRefresher2);
      this.getAllorderCount();
    }
  }
  public tagOrders = [];
  getTagOrders(length) {
    this.pNo2 = 1;
    if (length == 1) {
      this.tagOrders = [];
      this.statusCount = [];
    }
    if (this.tagOrders.length <= 10 && this.statusCloud.length > 0) {
      let params = {
        offset: 0,
        limit: 10,
        sortOrder: this.statusCloud[length - 1].name == 'scheduled_trip' ? 'asc' : 'desc',
        sortByColumn: this.statusCloud[length - 1].name == 'scheduled_trip' ? 'scheduler_start_date' : 'updated_at',
        current_order_status: this.unique_order_id[0] ? '' : this.statusCloud[length - 1].name ? [this.statusCloud[length - 1].name] : '',
        start_date: this.unique_order_id[0] ? '' : this.tagStartDate ? moment(this.tagStartDate).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.unique_order_id[0] ? '' : this.tagEndDate ? moment(this.tagEndDate).format('YYYY-MM-DD HH:mm') : '',
        driver_id: this.driver_id ? [this.driver_id] : '',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        vehicle_id: this.vehicle_id2 ? [this.vehicle_id2] : '',
        pickup_location: '',
        drop_location: '',
        partner_id: '',
        promo_id: '',
        is_scheduled: this.statusCloud[length - 1].name == 'scheduled_trip' ? 1 : 0,
        company_id: this.company_id_array,
        userid: this.timely_user_id ? this.timely_user_id : '',
        user_name: this.timely_user_name ? this.timely_user_name : '',
        //cancel_reason_id: this.cancelReasonId.length !=0 ? this.cancelReasonId : '',
        //vehicle_model_ids: this.vehicleChpModelId.length != 0 ? this.vehicleChpModelId : '',
        //vehicle_category_ids: this.vehicle_id_list ? this.vehicle_id_list : '',
        //order_comment_status: this.orderComment.length !=0 ? this.orderComment : []
        guest_user_id: this.guest_user_id ? this.guest_user_id : '',
        guest_user_phone: this.guest_user_phone ? this.guest_user_phone : ''
      };
      if (
        this.tagStartDate != "" ||
        this.tagEndDate != "" ||
        this.statusCloud[length - 1].name != ""
      ) {
        this.isLoading = true;
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._orderService.searchDispatcherOrder(data1).then(data => {
          if (data && data.status == 200) {
            data = this._EncDecService.dwt(this.session_token, data.data);
            let count = 0;
            for (let i = 0; i < data.searchOrder.length && this.tagOrders.length < 10; i++) {
              this.tagOrders.push(data.searchOrder[i])
              count++;
            };
            if (this.statusCloud.length > length && this.tagOrders.length <= 10 && !this.unique_order_id[0]) {
              length++;
              this.Wait3(1).then(() => { this.getTagOrders(length) });
            }
            else {
              this.countTag = 0;
              this.statusCount = [];
              this.getAllorderCountForTag(this.statusCloud[0].name, 0)
            }
          }
        });
      }
    }
  }
  public finalStatus;
  public tagOrdersLength = 0;
  public pNo2 = 1;
  public statusCount = [];
  tagOrderPagination(data) {
    this.pNo2 = data;
    let offset = 0;
    let status;
    let pos = 0;
    this.tagOrders = [];
    let start = (data - 1) * 10 + 1;
    this.isLoading = true;
    for (let i = 0; i < this.statusCount.length; i++) {
      if (start >= this.statusCount[i]['start'] && start <= this.statusCount[i]['end']) {
        offset = start - this.statusCount[i]['start'];
        status = this.statusCount[i]['status'];
        pos = i;
        this.ordersForPaging(offset, status, pos);
      }
    }
  }
  public ordersForPaging(offset, status, pos) {
    let params = {
      offset: offset,
      limit: 10,
      sortOrder: this.statusCloud[length - 1].name == 'scheduled_trip' ? 'asc' : 'desc',
      sortByColumn: this.statusCloud[length - 1].name == 'scheduled_trip' ? 'scheduler_start_date' : 'updated_at',
      current_order_status: [status] ? [status] : '',
      keyword: '',
      start_date: this.tagStartDate ? moment(this.tagStartDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.tagEndDate ? moment(this.tagEndDate).format('YYYY-MM-DD HH:mm') : '',
      driver_id: this.driver_id ? [this.driver_id] : '',
      vehicle_id: this.vehicle_id2 ? this.vehicle_id2 : '',
      pickup_location: '',
      drop_location: '',
      partner_id: '',
      is_scheduled: status == 'scheduled_trip' ? 1 : 0,
      promo_id: '',
      userid: this.timely_user_id ? this.timely_user_id : '',
      user_name: this.timely_user_name ? this.timely_user_name : '',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
      guest_user_id: this.guest_user_id ? this.guest_user_id : '',
      guest_user_phone: this.guest_user_phone ? this.guest_user_phone : ''
      //cancel_reason_id: this.cancelReasonId.length !=0 ? this.cancelReasonId : '',
      //vehicle_model_ids: this.vehicleChpModelId .length !=0 ? this.vehicleChpModelId  : '',
      //vehicle_category_ids: this.vehicle_id_list ? this.vehicle_id_list : '',
      //order_comment_status: this.orderComment.length !=0 ? this.orderComment : [],
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this.isLoading = true;
    this._orderService.searchDispatcherOrder(data1).then(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        for (let i = 0; i < data.searchOrder.length && this.tagOrders.length < 10; i++) {
          this.tagOrders.push(data.searchOrder[i])
        };
        pos++;
        if (this.statusCloud.length > pos && this.tagOrders.length < 10) {
          this.Wait3(1).then(() => { this.ordersForPaging(0, this.statusCloud[pos].name, pos) });
        }
        else {
          this.isLoading = false;
        }
      }
    });
  }
  public countTag = 0;
  public getAllorderCountForTag(status, index) {
    let params = {
      offset: 0,
      limit: 1,
      sortOrder: this.statusCloud[length - 1].name == 'scheduled_trip' ? 'asc' : 'desc',
      sortByColumn: this.statusCloud[length - 1].name == 'scheduled_trip' ? 'scheduler_start_date' : 'updated_at',
      current_order_status: status ? [status] : '',
      keyword: '',
      start_date: this.tagStartDate ? moment(this.tagStartDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.tagEndDate ? moment(this.tagEndDate).format('YYYY-MM-DD HH:mm') : '',
      driver_id: this.driver_id ? [this.driver_id] : '',
      vehicle_id: this.vehicle_id2 ? [this.vehicle_id2] : '',
      pickup_location: '',
      drop_location: '',
      partner_id: '',
      is_scheduled: status == 'scheduled_trip' ? 1 : 0,
      promo_id: '',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
      userid: this.timely_user_id ? this.timely_user_id : '',
      user_name: this.timely_user_name ? this.timely_user_name : '',
      //cancel_reason_id: this.cancelReasonId.length !=0 ? this.cancelReasonId : '',
      //vehicle_model_ids: this.vehicleChpModelId .length !=0 ? this.vehicleChpModelId  : '',
      //vehicle_category_ids: this.vehicle_id_list ? this.vehicle_id_list : '',
      //order_comment_status: this.orderComment.length !=0 ? this.orderComment : [],
      guest_user_id: this.guest_user_id ? this.guest_user_id : '',
      guest_user_phone: this.guest_user_phone ? this.guest_user_phone : ''
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.searchDispatcherOrder(data1).then(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        let index2 = this.statusCount.findIndex(x => x.status == status)
        if (index2 > -1) {
          this.statusCount[index2].status = status;
          this.statusCount[index2].count = data.count;
          if (status == 'scheduled_trip')
            this.orderScheduledTrip = data.count;
          this.statusCount[index2].start = this.countTag + 1;
          this.statusCount[index2].end = this.countTag + data.count;
        }
        else {
          if (status == 'scheduled_trip')
            this.orderScheduledTrip = data.count;
          this.statusCount.push({ 'status': status, 'count': data.count, 'start': this.countTag + 1, 'end': this.countTag + data.count });
        }
        this.countTag += data.count;
        if (this.statusCloud[index + 1] != undefined)
          this.getAllorderCountForTag(this.statusCloud[index + 1].name, index + 1)
        else {
          this.tagOrdersLength = this.countTag;
          this.isLoading = false;
        }
      }
    });
  }
  public getorderTagsForSocket(orders) {
    let i = 0;
    let start = (this.pNo2 - 1) * 10 + 1;
    while (i < orders.length) {
      if (orders[i].order_status == 'scheduled_trip')
        continue;
      let index = this.tagOrders.findIndex(x => x._id == orders[i]._id);
      let index2 = this.statusCount.findIndex(x => x.status == orders[i].order_status)
      if (index > -1 && index2 > -1) {
        if (start >= this.statusCount[index2]['start'] && start <= this.statusCount[index2]['end']) {
          if (this.pNo2 > 1) {
            this.tagOrders[index].drop_location = orders[i].drop_location;
            this.tagOrders[index].pickup_location = orders[i].pickup_location;
            this.tagOrders[index].sync_trip_id = orders[i].sync_trip_id;
            this.tagOrders[index].driver_id = orders[i].driver_id;
            this.tagOrders[index].vehicle_id = orders[i].vehicle_id;
            this.tagOrders[index].order_status = orders[i].order_status;
            this.tagOrders[index].price = orders[i].price;
            this.tagOrders[index].updated_at = orders[i].updated_at;
            this.tagOrders[index].distance = orders[i].distance;
            this.tagOrders[index].start_time = orders[i].start_time;
            this.tagOrders[index].scheduler_start_date = orders[i].scheduler_start_date;
            this.tagOrders[index].schedule_before = orders[i].schedule_before;
          }
          else if (this.statusCount[index2]['start'] <= 10) {
            this.tagOrders.splice(index, 1);
            this.tagOrders.splice(this.statusCount[index2]['start'] - 1, 0, orders[i])
          }
        }
      }
      else if (index2 > -1) {
        if (this.unique_order_id[0] || this.driver_id || this.vehicle_id2)
          return;
        if (this.pNo2 > 1) {
          if (start <= this.statusCount[index2]['start'] && start + 10 >= this.statusCount[index2]['start']) {
            if (this.tagOrders.length >= 10) {
              this.tagOrders.splice(9, 1);
            }
            this.tagOrders.splice((this.statusCount[index2]['start'] % 10) - 1, 0, orders[i])
          }
        }
        else {
          if (this.statusCount[index2]['start'] <= 10) {
            if (this.tagOrders.length >= 10) {
              this.tagOrders.splice(9, 1);
            }
            this.tagOrders.splice(this.statusCount[index2]['start'] - 1, 0, orders[i])
          }
        }
      }
      i++;
    }
  }
  setMessage(message) {
    this.addDispatcher.controls["message"].setValue(message);
  }
  refreshTagOrderCountData() {
    this.dataRefresher2 = setInterval(() => {
      this.countTag = 0;
      this.statusCount = [];
      this.getAllorderCountForTag(this.statusCloud[0].name, 0)
    }, 60000);
  }
  public tripStatus = [];
  public uberFlag = false;
  public breakFlag = false;
  setTripStatus(status) {
    this.uberFlag = false;
    this.breakFlag = false;
    this.tripStatus = [];
    this.vehicleFilter = "";
    this.sideFilter = "";
    this.driverFilter = "";
    this.hidden = true;
    switch (status) {
      case 0:
        this.tripStatus = [];
        break;
      case 1:
        this.tripStatus.push('accepted_by_driver');
        break;
      case 2:
        this.tripStatus.push('confirming_rate', 'selecting_payment', 'rating');
        break;
      case 3:
        this.uberFlag = true;
        break;
      case 4:
        this.breakFlag = true;
        break;
      default:
        break;
    }
    if (this.breakFlag || this.uberFlag)
      this.setMapFilter('1');
    else
      this.setVehicleStatusFilter('6');
  }
  notification(data, keyword) {
    if (!this._jwtService.getDispatcherUser() || this._global.logout == true) {
      this.dialog.closeAll();
      return;
    }
    if (this.notificationList.indexOf(data.unique_order_id) > -1)
      return;
    this.notificationList.push(data.unique_order_id);
    let audio = new Audio();
    audio.src = "../../assets/mobility/audio/notification.wav";
    audio.loop = true;
    audio.load();
    audio.play();
    //localStorage.setItem("blockedDevices", JSON.stringify(this.blockedDevices));
    data.key = keyword;
    let dialogRef = this.dialog.open(NotificationComponent, {
      panelClass: 'myapp-alert-dialog',
      width: '300px',
      data: data,
      scrollStrategy: this.overlay.scrollStrategies.noop()
    });
    dialogRef.updatePosition({ top: '50px', right: '50px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ordersFilter.push(result);
        this.unique_order_id.push(result);
        this.orderCtrl.setValue(null);
        this.autoSearch();
      }
      audio.pause();
      audio = null;
    });
  }
  clearVehicleModels() {
    this.modelChips = [];
  }
  clearStatusCloud() {
    this.statusCloud = [];
  }
  displayFnDispatcher(data): string {
    return data ? data.firstname : "";
  }
  public searchDispatcher(data) {
    if (typeof data === 'object') {
      this.dispatcher_id2 = data._id;
    }
    else {
      this.dispatcher_id2 = '';
    }
  }
  public getDispatcherUsers() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._aclService.getAclUsers(data1).then((result) => {
      if (result && result.status == 200) {
        result = this._EncDecService.dwt(this.session_token, result.data);
        this.dispatcherList = result.users;
        let index = this.dispatcherList.findIndex(x => x._id == "5b9a0f0336aad01ed0e7d49e");
        if (index > -1) {
          this.dispatcherList.splice(index, 1);
        }
      }
    })
  }
  customerOrderPagination(event) {
    this.customerOrderHistory = [];
    let offset = event * 10 - 10;
    const param = {
      offset: offset,
      sortOrder: 'desc',
      sortByColumn: 'unique_order_id',
      limit: 10,
      user_id: this.orderInfo.user_id._id
    }
    let enc_data = this._EncDecService.nwt(this.session_token, param);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOrderByUserId(data1).then(data => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.customerOrderHistory = data.data;
        this.customerOrderCount = data.count
      }
    });
  }
  public toGo(dt: any) {
    let date2 = new Date(dt).getTime();
    let date1 = new Date(this.uaeTime).valueOf();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";
    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output = Math.round(var1) + " seconds";
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      let decimal = var1 - Math.floor(var1)
      output = Math.round(var1) + " minutes";
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      output = Math.round(var1) + " hours";
      // let decimal=((var1.after())*.6).toString().substring(0,2)
      // decimal=decimal=='01'?decimal+' minute':decimal+' minutes'
      // output = Math.floor(var1)!==1?Math.floor(var1) + " hours and" +decimal:Math.floor(var1) + " hour and" +decimal;
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output = Math.round(var1) + " days";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output = Math.round(var1) + " months";
    }
    return hoursDiff < 0 ? '0 second' : output;
  }
  checkIfAllSelected(order, i) {
    if (order) {
      this.orders[i].checked = !this.orders[i].checked;
      if (this.orders[i].checked == true) {
        if (this.selectedOrder.indexOf(this.orders[i]) > -1)
          return;
        this.selectedOrder.push(order);
      }
      else if (this.orders[i].checked == false)
        this.selectedOrder.splice(i, 1);
    }
  }
  public cancelSelected() {
    let dialogRef = this.dialog.open(CancelSelectedComponent, {
      width: "315px",
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: {
        type: "order_cancel",
        reason_head: "Cancellation Reason",
        order_status: this.selectedOrder
      }
      , disableClose: true, hasBackdrop: true
    });
    dialogRef.afterClosed().subscribe(res => {
      let i = 0;
      while (i < this.orders.length) {
        this.orders[i].checked = false;
        i++;
      }
      this.selectedOrder = [];
    });
  }
  public setPaymentType(data) {
    const partner = "5bd1e5881e83214734676f70";
    if (typeof data == "object") {
      if (data._id == partner)
        this.addDispatcher.controls["payment_type"].setValue('1');
      else
        this.addDispatcher.controls["payment_type"].setValue('3');
    }
  }
  clickedSignedOut(marker, detail, icon) {
    this.check_interval = "no";
    let vehicle_id = detail["vehicle_id"];
    const params = {
      vehicle_id: vehicle_id,
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    this.marker.pickup_location = "";
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    if (!this.infoWindow) {
      this.infoWindow = new google.maps.InfoWindow();
      // let that = this;
      // this.infoWindow.addListener('closeclick', function () {
      //   if (that.infoMarker)
      //     that.infoMarker.setMap(null);
      //   that.infoMarker = null;
      // });
    }
    if (vehicle_id) {
      this._shiftsService.getAssignedSignedOutDrivers(data1).then(data => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          let popupData = data.shiftinfo.device_id;
          //let correctDriver = popupData.driver_id.username
          let status;
          if (popupData) {
            if (data.shiftinfo.vehicle_id)
              if (data.shiftinfo.vehicle_id.online_status == "1") {
                status = "online";
              } else {
                status = "offline";
              }
            this.infoWindow.setContent("");
            this.marker.updated_at = popupData.updated_at;
            this.marker.device_id = popupData.unique_device_id;
            this.marker.device_model = popupData.device_model;
            this.marker.vehicle_id = data.shiftinfo.vehicle_id
              ? data.shiftinfo.vehicle_id.vehicle_identity_number
              : "";
            this.marker.device_type = status;
            //this.marker.speed = parseInt(popupData.speed);
            //this.marker.gps_strength = String(parseInt(popupData.gps_strength));
            //this.marker.battery = popupData.battery;
            this.marker.shift_start = data.shiftinfo ? data.shiftinfo.shift_start : '';
            this.marker.shift_end = data.shiftinfo ? data.shiftinfo.shift_end : '';
            this.marker.imei = popupData.imei;
            this.marker.vehicle_name = data.shiftinfo.vehicle_id
              ? String(
                data.shiftinfo.vehicle_id.display_name
              ).toUpperCase() : '';
            this.marker.vehicle_make = data.shiftinfo.vehicle_id ? data.shiftinfo.vehicle_id.car_manufacturer_desc
              ? String(data.shiftinfo.vehicle_id.car_manufacturer_desc).toUpperCase()
              : "" : "";
            this.marker.vehicle_model = "";
            this.marker.vehicle_fuel_type = data.shiftinfo.vehicle_id
              ? data.shiftinfo.vehicle_id.car_fuel_type : '';
            this.marker.company = "Dubai Taxi";
            this.marker.driver_name = data.shiftinfo.driver_id ?
              String(
                data.shiftinfo.driver_id.display_name
              ).toUpperCase() : '';
            this.marker.driver_id = data.shiftinfo.driver_id
              ? data.shiftinfo.driver_id._id : '';
            this.marker.emp_id = data.shiftinfo.driver_id
              ? data.shiftinfo.driver_id.emp_id : '';
            this.marker.driverphone = data.shiftinfo.driver_id
              ? data.shiftinfo.driver_id.phone_number : '';
            this.marker.status = this.vehicleStatus[icon];
            this.marker.display = true;
            this.ref.detectChanges();
            let infoWindowContent = <Element>(
              document.getElementById("iw1").cloneNode(true)
            );
            infoWindowContent.id = "iw-2";
            infoWindowContent.setAttribute("style", "display:block;");
            this.infoWindow.setContent(infoWindowContent);
            this.infoWindow.open(this.map, marker);
            this.infoMarker = null;
          } else {
            this.marker.device_id = "";
            this.marker.device_model = "";
            this.marker.vehicle_id = "";
            this.marker.device_type = "";
            this.marker.imei = "";
            this.marker.vehicle_name = "";
            this.marker.vehicle_make = "";
            this.marker.vehicle_model = "";
            this.marker.vehicle_fuel_type = "";
            this.marker.company = "Dubai Taxi";
            this.marker.driver_image = "/assets/img/map/driver.jpg";
            this.marker.driver_name = "";
            this.marker.driver_id = "";
            this.marker.emp_id = "";
            this.marker.driverphone = "";
            this.marker.speed = 0;
            this.marker.gps_strength = "";
            this.marker.battery = "";
            this.marker.vehicle_image =
              "/assets/img/map/1428584997LEXUS_ES350.jpg";
            this.marker.pickup_location = "";
            this.marker.shift_end = "";
            this.marker.status = ""
          }
        }
      });
    }
  }
  public payment_type_id: any = "";
  public paymentTypeData = [{ "id": "1", "type": "CASH" }, { "id": "2", "type": "CREDIT" }, { "id": "3", "type": "ACCOUNT" }, { "id": "4", "type": "ONLINE" }]
  showKeyIndicators(order) {
    this.orderInfoKey = []
    this.showModel(order, false);
    setTimeout(() => {
      let el: HTMLElement = document.getElementById('keyIndicator') as HTMLElement;
      el.click()
    }, 1000);
    // var data1 = {
    //   company_id: this.company_id_array,
    //   _id: order
    // };
    // let enc_data = this._EncDecService.nwt(this.session_token, data1);
    // //alert(this.dispatcher_id.email)
    // let data = {
    //   data: enc_data,
    //   email: this.useremail
    // }
    // this._orderService.getKeyIndicators(data).then(dec => {
    //   if (dec && dec.status == 200) {
    //     let orderData: any = this._EncDecService.dwt(this.session_token, dec.data);
    //     let that = this;
    //     this.dialog.closeAll();
    //     let dialogRef = this.dialog.open(KeyIndicatorsComponent, {
    //       width: "600px",
    //       hasBackdrop: true,
    //       data: orderData.response,
    //       scrollStrategy: this.overlay.scrollStrategies.noop()
    //     });
    //   }
    // });
  }

  public forceCancelOrder(orderId, order_status) {
    if (
      order_status == "dispatcher_cancel" ||
      order_status == "completed" ||
      order_status == "ended_by_admin" ||
      order_status == "cancel_acknowledged" ||
      order_status == "customer_cancel" ||
      order_status == "rejected_by_driver" ||
      order_status == "order_timed_out"
    ) {
      if (order_status == "dispatcher_cancel") {
        this.cancelOrdermsg = " This order already cancelled By dispatcher.";
      } else if (order_status == "ended_by_admin") {
        this.cancelOrdermsg = " This order already ended By admin.";
      } else if (order_status == "cancel_acknowledged") {
        this.cancelOrdermsg = " This order already cancelled By dispatcher.";
      } else if (order_status == "customer_cancel") {
        this.cancelOrdermsg = " This order already cancelled By customer.";
      } else if (order_status == "rejected_by_driver") {
        this.cancelOrdermsg = " This order already cancelled By Driver.";
      } else if (order_status == "order_timed_out") {
        this.cancelOrdermsg = " This order already timedout.";
      } else {
        this.cancelOrdermsg = "This Order is already Completed.";
      }
      $("#cancel_order").modal("show");
    } else {
      var user = JSON.parse(window.localStorage["dispatcherUser"]);
      var data = {
        email: user.email,
        type: "order_cancel",
        reason: { reason: "changed mind", type: "order_cancel", _id: "5bdc42f8a08907a499d45afd" },
        status: true,
        _id: orderId
      };
      let enc_data = this._EncDecService.nwt(this.session_token, data);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._orderService.forceCancelOrder(data1).then(cancel => {
        if (cancel) {
          if (cancel.status == 202) {
            this.toastr.success(cancel.message);
          } else if (cancel.status == 205) {
            this.toastr.success(cancel.message);
          } else if (cancel.status == 200) {
            this.toastr.success(cancel.message);
          } else {
            this.toastr.error(cancel.message);
          }
        } else {
          this.toastr.warning("Error while Order cancellation!");
        }
      });
    }
  }
  public reverseGCList: any = [];
  reversGC(loc, i, order) {
    if (this.reverseGCList.indexOf(i) > -1)
      return;
    else
      this.reverseGCList.push(i);
    let geocoder = new google.maps.Geocoder();
    let latlngvalue = loc.split(",");
    let that = this;
    let latlng = { lat: parseFloat(latlngvalue[0]), lng: parseFloat(latlngvalue[1]) }
    geocoder.geocode({ location: latlng }, function (results, status) {
      if (results) {
        let index = -1;
        if (that.timeBarFlag) {
          index = that.orders.findIndex(x => x.unique_order_id == order);
          if (index > -1)
            if (i >= 100)
              that.orders[index].drop_location = results[0].formatted_address;
            else
              that.orders[index].pickup_location = results[0].formatted_address;
        }
        else {
          index = that.tagOrders.findIndex(x => x.unique_order_id == order);
          if (index > -1)
            if (i >= 100)
              that.tagOrders[index].drop_location = results[0].formatted_address;
            else
              that.tagOrders[index].pickup_location = results[0].formatted_address;
        }
      }
    });

  }
  onNavigate(link) {
    window.open('https://www.google.com/maps/place/?q=' + link, "_blank");
  }
  parse(data) {
    if (parseInt(data) > 0)
      return parseInt(data);
    else
      return 0;
  }
  editOrder(order) {
    let params1 = {
      company_id: this.company_id_array,
      _id: order
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params1);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOrderById(data1).then(dec => {
      if (dec && dec.status == 200) {
        let orderData: any = this._EncDecService.dwt(this.session_token, dec.data);
        let that = this;
        this.dialog.closeAll();
        let dialogRef = this.dialog.open(EditOrderComponent, {
          width: "600px",
          hasBackdrop: true,
          data: orderData.response,
          scrollStrategy: this.overlay.scrollStrategies.noop()
        });
        dialogRef.afterClosed().subscribe(res => {
          this.getOrders(true)
        });
      }
    });
  }
  dispatchNow(id, type) {
    let dialogRef = this.dialog.open(DriverDialogComponent, {
      width: "315px",
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: {
        text: "Are you sure you want to dispatch this order now",
      }
      , disableClose: true, hasBackdrop: true
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        console.log(type.toLowerCase());

        let param = {
          dispatcher_id: this.dispatcher_id._id,
          order_id: id,
          dispatch_now: true,// for rescheduling done by user history
          disp_email: this.dispatcher.email,
        }
        console.log(param);

        let enc_data = this._EncDecService.nwt(this.session_token, param);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        if (type.toLowerCase() == 'customer') {
          console.log(type.toLowerCase());
          this._orderService.customerOrderDispatchNow(data1).then(res => {
            console.log(res);
            if (res.status == 403) {
              this.toastr.error("Session timed out");
              this._jwtService.destroyDispatcherToken();
              this._jwtService.destroyAdminToken();
              this.router.navigate(["/dispatcher/login"]);
            }
            if (res.status == 200) {
              this.toastr.success("Order Updated successfully");
            }
            else if (res) {
              this.toastr.warning(res.message)
            }
          });
        } else if (type.toLowerCase() == 'delivery' || type.toLowerCase() == 'hotel/website') {
          console.log(type.toLowerCase());
          this._orderService.hotelDeliveryDispatchNow(data1).then(res => {
            console.log(res);
            if (res.status == 403) {
              this.toastr.error("Session timed out");
              this._jwtService.destroyDispatcherToken();
              this._jwtService.destroyAdminToken();
              this.router.navigate(["/dispatcher/login"]);
            }
            if (res.status == 200) {
              this.toastr.success("Order Updated successfully");
            }
            else if (res) {
              this.toastr.warning(res.message)
            }
          });
        } else {
          console.log(type.toLowerCase());
          this._orderService.editScheduledOrder(data1).then(res => {
            console.log(res);

            if (res.status == 403) {
              this.toastr.error("Session timed out");
              this._jwtService.destroyDispatcherToken();
              this._jwtService.destroyAdminToken();
              this.router.navigate(["/dispatcher/login"]);
            }
            if (res.status == 200) {
              this.toastr.success("Order Updated successfully");
            }
            else if (res) {
              this.toastr.warning(res.message)
            }
          });
        }
      }
      else {
      }
    })
  }
  public hidden = true;
  unhide() {
    this.hidden = false;
  }
  public minutesS = '0';
  public minutesM = '3';
  public minutesLimitS = '0';
  public minutesLimitM = '10000';
  public paused = true;
  iconColor2(dt: any) {
    let date1 = new Date(dt).getTime();
    let date2 = new Date(this.uaeTime).getTime();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";
    let minutes = (parseInt(this.minutesM) * 60) + parseInt(this.minutesS);
    let minutesLimit = (parseInt(this.minutesLimitM) * 60) + parseInt(this.minutesLimitS);
    if (hoursDiff <= minutes) {
      output = "green";
    } else if ((hoursDiff >= minutesLimit) && !this.hidden) {
      output = "green";
    } else {
      output = "red";
    }
    return output;
  }
  public showOrderTrack = false;
  side_no: FormControl = new FormControl();
  public selectedMoment3: any = "";
  public selectedMoment4: any = "";
  shiftpop(id) {
    this.showOrderTrack = true;
    this.side = id;
    this.showCar({ '_id': id });
  }
  public positions3 = [];
  showCar(event) {
    this.check_filter_interval = "no";
    this.check_interval = "no";
    this.positions3 = [];
    if (typeof event === "object") {
      let vehicle_id = event._id;
      clearInterval(this.refreshTime);
      this.getRefreshTime('6');
      if (vehicle_id) {
        this.vehicle_id = vehicle_id;
        let params = {
          company_id: this.company_id_array,
          vehicle_id: this.vehicle_id
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._devicelocationService
          .getdatabyvehicleid(data1)
          .then(dec => {
            if (dec && dec.status == 200) {
              let vehicleFreedata: any = this._EncDecService.dwt(this.session_token, dec.data);
              this.SingledeviceLocationData = vehicleFreedata.getdeviceloc;
              this.selectDriverData = vehicleFreedata.driversArr;
              this.selectVehicleData = vehicleFreedata.vehicleArr;
              this.deviceLocationData = vehicleFreedata.getdeviceloc;
              //this.vehicleFilter = this.selectVehicleData[0];
              //this.plateFilter = this.selectVehicleData[0];
              let id;
              if (
                this.SingledeviceLocationData.length > 0 &&
                typeof this.SingledeviceLocationData[0] != "undefined" &&
                this.SingledeviceLocationData[0].latitude != ""
              ) {
                for (let i = 0; i < this.SingledeviceLocationData.length; i++) {
                  const randomLat = this.SingledeviceLocationData[i].latitude;
                  const randomLng = this.SingledeviceLocationData[i].longitude;
                  this.doCenter({
                    lat: parseFloat(randomLat),
                    lng: parseFloat(randomLng)
                  });
                  if (this.SingledeviceLocationData[i]["device_id"]) {
                    id = this.SingledeviceLocationData[i]["device_id"];
                  } else {
                    id = "";
                  }
                  let vehicleStatus = this.getVehicleStatus(
                    this.selectVehicleData[i]
                  );
                  var driverIcon = this.driverIcons.loggedIn;
                  let vehicleModelIsValid =
                    this.SingledeviceLocationData[i].vehicle_model_id &&
                    (typeof this.SingledeviceLocationData[i].vehicle_model_id === "string" ||
                      this.SingledeviceLocationData[i].vehicle_model_id instanceof String);
                  let isTesla =
                    vehicleModelIsValid &&
                    (this.SingledeviceLocationData[i].vehicle_model_id === "5bb084342dd48f285e5b6c3d" ||
                      this.SingledeviceLocationData[i].vehicle_model_id === "5bb093892dd48f285e5c3dba");
                  var driverIcon = this.driverIcons.loggedIn;
                  if (!this.SingledeviceLocationData[i].driver_id || this.SingledeviceLocationData[i].driver_id == null) {
                    driverIcon = this.teslaIcons['loggedOut'];
                  }
                  else if (vehicleStatus == 'offline') {
                    if (isTesla)
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['paused_offline'] : this.teslaIcons[vehicleStatus];
                    else
                      driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                        ? this.driverIcons['paused_offline'] : this.driverIcons[vehicleStatus]
                  }
                  else {
                    if (this.SingledeviceLocationData[i].trip_status !== "") {
                      if (isTesla) {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.teslaIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.teslaIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.teslaIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' ? this.teslaIcons['internet'] : driverIcon;
                      }
                      else {
                        switch (this.SingledeviceLocationData[i].trip_status) {
                          case 'accepted_by_driver':
                            driverIcon = this.driverIcons['accepted'];
                            break;
                          case 'confirming_rate':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'selecting_payment':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          case 'rating':
                            driverIcon = this.driverIcons['almost_free'];
                            break;
                          default:
                            driverIcon = this.driverIcons[vehicleStatus]
                            break;
                        }
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green'
                          ? this.driverIcons['internet'] : driverIcon;
                      }
                    }
                    else {
                      if (isTesla) {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.teslaIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.teslaIcons['free'] : this.teslaIcons[vehicleStatus];
                      }
                      else {
                        driverIcon = this.iconColor(this.SingledeviceLocationData[i].updated_at) != 'green' && (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                          ? this.driverIcons['free_internet'] : (vehicleStatus == 'free' || vehicleStatus == 'loggedIn')
                            ? this.driverIcons['free'] : this.driverIcons[vehicleStatus];
                      }
                    }
                  }
                  if (randomLat != "" && randomLng != "" && id != "") {
                    this.positions.push([randomLat, randomLng, id]);
                    this.displayDriverOnMap(
                      randomLat,
                      randomLng,
                      id,
                      driverIcon,
                      0
                    );
                  }
                  this.removeStaleDriversFromMap();
                }
                this.locationmsg = "";
              } else {
                this.locationmsg = "No";
              }
            }
          });
      }
    }
  }
  clearEndDate() {
    this.selectedMoment4 = "";
    this.max2 = new Date(moment(this.selectedMoment).add(1, 'days').format('YYYY-MM-DD HH:mm'));
    this.selectedMoment1 = this.max2;
  }
  public orderButton = false;
  public travelled_cordinates2;
  public travelled_markers = [];
  public ordermarker2 = {
    speed: "",
    battery: null,
    date: null,
    is_gps_on: true,
    is_internet_on: true,
    display: true,
    accuracy: null,
    order: null
  };
  public side: any;
  public showCover = false;
  trackOrder() {
    this.startmarker = "";
    this.destinationmarker = "";
    this.orderButton = true;
    for (let i = 0; i < this.travelled_markers.length; i++) {
      this.travelled_markers[i].setMap(null);
      if (i == this.travelled_markers.length - 1) this.travelled_markers = [];
    }
    if (this.selectedMoment3 != "" && this.side !== undefined && this.side !== '' && this.side !== null && typeof this.side === "object") {
      this.orderButton = false;
      this.positions1 = [];
      this.showCover = true;
      let param = {
        start_date: moment(this.selectedMoment3).format('YYYY-MM-DD HH:mm'),
        end_date: this.selectedMoment4
          ? moment(this.selectedMoment4).format('YYYY-MM-DD HH:mm')
          : moment(this.selectedMoment3).add(1, 'days').format('YYYY-MM-DD HH:mm'),
        vehicle_id: this.side._id ? this.side._id : "",
        company_id: this.company_id_array
      };
      let enc_data = this._EncDecService.nwt(this.session_token, param);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this.positions3 = [];
      this._devicelocationService.vehicleTrackLocations(data1).then(dec => {
        if (dec && dec.status == 200) {
          let data: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.travelled_cordinates2 = data.locations;
          let position;
          if (
            this.travelled_cordinates2.length > 0 &&
            this.travelled_cordinates2.length != ""
          ) {
            for (var i = 0; i < this.travelled_cordinates2.length; ++i) {
              let data2 = [
                this.travelled_cordinates2[i].latitude,
                this.travelled_cordinates2[i].longitude,
                this.travelled_cordinates2[i].speed,
                this.travelled_cordinates2[i].created_at,
                this.travelled_cordinates2[i].is_gps_on,
                this.travelled_cordinates2[i].is_internet_on,
                this.travelled_cordinates2[i].accuracy,
                this.travelled_cordinates2[i].battery,
                this.travelled_cordinates2[i].l_id
              ];
              let orderFlag =
                this.travelled_cordinates2[i].l_id == "" ? false : true;

              position = new google.maps.LatLng(
                parseFloat(this.travelled_cordinates2[i].latitude),
                parseFloat(this.travelled_cordinates2[i].longitude)
              );

              if (i > 0) {
                this.drawPoints(
                  [
                    this.travelled_cordinates2[i - 1].latitude,
                    this.travelled_cordinates2[i - 1].longitude
                  ],
                  [
                    this.travelled_cordinates2[i].latitude,
                    this.travelled_cordinates2[i].longitude
                  ],
                  position,
                  data2,
                  i,
                  this.travelled_cordinates2.length,
                  orderFlag
                );
              } else {
                var img = new Image();
                img.src = "assets/img/map/arrow_yellow.png";
                let icon = new RotateIcon2({ img: img })
                  .setRotation({ deg: 0 })
                  .getUrl();
                let marker = new google.maps.Marker({
                  position: position,
                  map: this.map,
                  icon: icon
                });
                this.travelled_markers.push(marker);
                marker.addListener("mouseover", () => {
                  this.onOrderMarkerClick2(marker, data);
                });
              }
            }
          }
          let length = this.travelled_cordinates2.length - 1;
          if (length >= 0) {
            this.startmarker = JSON.parse(
              "[" +
              this.travelled_cordinates2[length].latitude +
              "," +
              this.travelled_cordinates2[length].longitude +
              "]"
            );
            this.doCenter({
              lat: parseFloat(this.travelled_cordinates2[0].latitude),
              lng: parseFloat(this.travelled_cordinates2[0].longitude)
            });
            this.map.setZoom(12);
            this.destinationmarker = JSON.parse(
              "[" +
              this.travelled_cordinates2[0].latitude +
              "," +
              this.travelled_cordinates2[0].longitude +
              "]"
            );
          }
          else {
            this.showCover = false;
            this.locationmsg = 'Y';
          }
          this.orderButton = false;
        }
      });
    } else {
      this.orderButton = false;
    }
  }
  onOrderMarkerClick2(marker, marker_details) {
    if (!this.infoWindow) {
      this.infoWindow = new google.maps.InfoWindow();
    }
    this.infoWindow.setContent("");
    this.ordermarker2.speed = marker_details[2] !== 0 && marker_details[2] !== null && marker_details[2] !== "" ? parseInt(marker_details[2]) + " KM/H" : "NA";
    this.ordermarker2.date = marker_details[3];
    this.ordermarker2.is_gps_on = marker_details[4];
    this.ordermarker2.is_internet_on = marker_details[5];
    this.ordermarker2.accuracy = marker_details[6]
      ? Math.round(marker_details[6])
      : "";
    this.ordermarker2.battery = marker_details[7]
      ? Math.round(marker_details[7])
      : "";
    this.ordermarker2.order = marker_details[8] == "" ? "Free" : "In trip";
    setTimeout(() => {
      let infoWindowContent = <Element>(
        document.getElementById("vehicleTrackwindow").cloneNode(true)
      );
      infoWindowContent.setAttribute("style", "display:block;");
      this.infoWindow.setContent(infoWindowContent);
      this.infoWindow.open(this.map, marker);
    }, 500)
  }
  drawPoints(target, source, position, data, i, length, flag) {
    let steps = 10;
    let deltaLat = (target[0] - source[0]) / steps;
    let deltaLng = (target[1] - source[1]) / steps;
    var rotation = Math.atan2(deltaLng, deltaLat) * (180 / Math.PI);
    var img = new Image();
    img.src = "assets/img/map/arrow_blue.png";
    var img2 = new Image();
    img2.src = "assets/img/map/arrow_yellow.png";
    img.onload = () => {
      let icon;
      if (flag)
        icon = new RotateIcon2({ img: img })
          .setRotation({ deg: rotation })
          .getUrl();
      else
        icon = new RotateIcon2({ img: img2 })
          .setRotation({ deg: rotation })
          .getUrl();
      let marker = new google.maps.Marker({
        position: position,
        map: this.map,
        icon: icon
      });
      this.travelled_markers.push(marker);
      marker.addListener("mouseover", () => {
        this.onOrderMarkerClick2(marker, data);
      });
      if (length - 1 == i) {
        this.showCover = false;
      }
    };
  }
  addComment(id, status) {
    let data = {
      order_id: id,
      status: status
    }
    let dialogRef = this.dialog.open(AddCommentComponent, {
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      width: "450px",
      data: data,
      hasBackdrop: true
    });
    dialogRef.afterClosed().subscribe(res => {
      this.getOrders(false);
    })
  }
  public creatingcsv = false;
  public getconstantsforcsv() {
    if (this.creatingcsv) {
      this.toastr.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      var params = {
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      };
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._orderService.getCSVConstants(data1).then((res) => {
        if (res.status == 200) {
          this.createCsv(2000, 20);
        } else {
          this.creatingcsv = false
          this.toastr.error("Please try again")
        }
      })
    }
  }
  public progressvalue = 0;
  public createCsv(interval_value, offset) {
    var count = this.orderTotal;
    let res1Array = [];
    let i = 0;
    let fetch_status = true;
    let that = this;
    this.csvInterval = setInterval(function () {
      if (fetch_status) {
        if (res1Array.length >= count) {
          that.progressvalue = 100;
          that.creatingcsv = false;
          clearInterval(that.csvInterval);
          let labels = [
            'ID',
            'Company',
            'Customer Name',
            'Customer Phone',
            'Customer E-mail',
            'Source',
            'Start',
            'Destination',
            'Stop',
            'Distance',
            'Driver',
            'Assigned drivers',
            'Estimated Fare (AED)',
            'Time took to accept (Seconds)',
            'Accepted Location',
            'Assigned Time',
            'Distance to reach when accepted',
            'ETA (Minutes)',
            'Time took to reach customer (Seconds)',
            'Total trip time (Seconds)',
            'Time took to cancel order (Seconds)',
            'Vehicle',
            'Vehicle-model',
            'Customer Given Rating',
            'Driver Given Rating',
            //'Feedback',
            'Partner',
            'Additional Service',
            'Order-type',
            'Payment Type',
            'Status',
            'Cancel Reason',
            'Tariff',
            'Kilometer Rate(AED)',
            'Distance Amount(AED)',
            'Discount(AED)',
            'Extra Toll (AED)',
            'Extra Service (AED)',
            'Slow Speed (AED)',
            'Base (AED)',
            'Vat (AED)',
            'Adjustment (AED)',
            'Total (AED)',
            'Bill Printed'
          ];
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(res1Array, 'List-Order' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        } else {
          that.creatingcsv = true;
          fetch_status = false;
          let diff = count - i;
          let perc = diff / count;
          let remaining = 100 * perc;
          that.progressvalue = 100 - remaining;
          let new_params;
          if (that.unique_order_id.length !== 0) {
            new_params = {
              offset: i,
              //limit: res1Array.length>=40?10:parseInt(offset),
              limit: parseInt(offset),
              sortOrder: that.sort_type != '' ? that.sort_type : 'desc',
              keyword: that.unique_order_id ? that.unique_order_id : '',
              pickup_location: '',
              drop_location: '',
              partner_id: '',
              start_date: '',
              end_date: '',
              promo_id: '',
              company_id: that.company_id_list.length > 0 ? that.company_id_list : that.company_id_array
            };
          } else {
            let pickup_location: any = '';
            let drop_location: any = '';
            if (that.multiFilter.pickup.length > 0) {
              pickup_location = [];
              that.multiFilter.pickup.forEach(element => {
                pickup_location.push([element.loc.coordinates[0]])
              });
            }
            if (that.multiFilter.drop.length > 0) {
              drop_location = [];
              that.multiFilter.drop.forEach(element => {
                drop_location.push([element.loc.coordinates[0]])
              });
            }
            let drivers = that.multiFilter.driver.map(x => x._id);
            let vehicles = that.multiFilter.side.map(x => x._id);
            let partners = that.multiFilter.partner.map(x => x._id);
            that.sort_by_coloumn = that.date_filter_order ? that.date_filter_order : that.sort_by_coloumn;
            new_params = {
              offset: i,
              //limit: res1Array.length>=40?10:parseInt(offset),
              limit: parseInt(offset),
              sortOrder: that.sort_type != '' ? that.sort_type : 'desc',
              sortByColumn: that.sort_by_coloumn != '' ? that.sort_by_coloumn : 'updated_at',
              vehicle_id: vehicles.length > 0 ? vehicles : '',
              tariffid: that.tariff_id ? that.tariff_id : '',
              driver_id: drivers.length > 0 ? drivers : '',
              pickup_location: pickup_location,
              drop_location: drop_location,
              current_order_status: that.current_order_status ? that.current_order_status : '',
              keyword: that.unique_order_id ? that.unique_order_id : '',
              start_date: that.selectedMoment ? moment(that.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
              end_date: that.selectedMoment1 ? moment(that.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
              userid: that.user_id ? that.user_id : '',
              user_name: that.user_name ? that.user_name : '',
              generate_by: that.generated_by ? that.generated_by : '',
              partner_id: partners.length > 0 ? partners : '',
              model_id: that.vehicleTypeFilterOrder ? that.vehicleTypeFilterOrder._id ? that.vehicleTypeFilterOrder._id : '' : '',
              dispatcher_id: that.dispatcher_id2 ? that.dispatcher_id2 : '',
              promo_id: that.promo_id ? that.promo_id : '',
              payment_type_id: that.payment_type_id ? that.payment_type_id : '',
              cancel_reason_id: that.cancelReasonId.length != 0 ? that.cancelReasonId : '',
              vehicle_model_ids: that.vehicleChpModelId.length != 0 ? that.vehicleChpModelId : '',
              vehicle_category_ids: that.vehicle_id_list ? that.vehicle_id_list : '',
              order_comment_status: that.orderComment.length != 0 ? that.orderComment : [],
              company_id: that.company_id_list.length > 0 ? that.company_id_list : that.company_id_array,
              order_edited: that.order_edited ? that.order_edited : '',
              flagged_orders: that.flagged_orders ? that.flagged_orders : '',
              is_scheduler: that.is_scheduler ? '1' : '',
              date_filter_order: this.date_filter_order ? this.date_filter_order : 'updated_at',
              guest_user_id: this.guest_user_id ? this.guest_user_id : '',
              guest_user_phone: this.guest_user_phone ? this.guest_user_phone : ''
            };
          }
          if (that.multiFilter.driver_group.length > 0 && !new_params.driver_id) {//gets the list of drivers specific to one more driver group
            that.getDriversInDriverGroup(that.multiFilter.driver_group.map(x => x._id)).then((res) => {
              new_params.driver_id = res;
              let enc_data = that._EncDecService.nwt(that.session_token, new_params);
              //alert(this.dispatcher_id.email)
              let data1 = {
                data: enc_data,
                email: that.useremail
              }
              that._orderService.searchDispatcherOrder(data1).then((res) => {
                if (res && res.status == 200) {
                  res = that._EncDecService.dwt(that.session_token, res.data);
                  if (res.searchOrder.length > 0) {
                    for (let j = 0; j < res.searchOrder.length; j++) {
                      const csvArray = res.searchOrder[j];
                      if (csvArray.eta) {
                        let temp = csvArray.eta.split(' ');
                        csvArray.eta = temp[0]
                      }
                      let adjustment = '0';
                      let discount2 = '0';
                      if (csvArray.sync_trip_id && csvArray.sync_trip_id.amt_splitup != null && csvArray.sync_trip_id.amt_splitup.length > 0) {
                        let index = csvArray.sync_trip_id.amt_splitup.findIndex(x => x.name == "Adjustment")
                        adjustment = index > -1 ? csvArray.sync_trip_id.amt_splitup[index].amt : '0';
                        let index2 = csvArray.sync_trip_id.amt_splitup.findIndex(x => x.id == "A008")
                        discount2 = index2 > -1 ? csvArray.sync_trip_id.amt_splitup[index2].amt : '0';
                      }
                      let drivers = '';
                      if (csvArray.assigned_drivers_ids) {
                        for (let dData of csvArray.assigned_drivers_ids) {
                          drivers += dData.username + ',';
                        }
                      }
                      let vehicle_model = '';
                      if (csvArray.vehicle_id && csvArray.vehicle_id.vehicle_model_id) {
                        let index = that.modelList4.findIndex(x => x._id == csvArray.vehicle_id.vehicle_model_id)
                        if (index > -1)
                          vehicle_model = that.modelList4[index].name;
                      }
                      let km_rate = 0;
                      if (csvArray.sync_trip_id && csvArray.sync_trip_id.amt_splitup != null && csvArray.sync_trip_id.amt_splitup.length > 0) {
                        let index = csvArray.sync_trip_id.amt_splitup.findIndex(x => x.name == "Distance Amount")
                        km_rate = index > -1 && parseFloat(csvArray.sync_trip_id.total_distance_travelled) !== 0 ? parseFloat(csvArray.sync_trip_id.amt_splitup[index].amt) / parseFloat(csvArray.sync_trip_id.total_distance_travelled) : 0;
                      }
                      res1Array.push({
                        id: csvArray.unique_order_id ? csvArray.unique_order_id : 'N/A',
                        company: csvArray.company_id.company_name,
                        customer_name: csvArray.user_id && csvArray.user_id.firstname ? csvArray.user_id.firstname : 'Offline Customer',
                        customer_phone: csvArray.user_id && csvArray.user_id.phone_number ? csvArray.user_id.phone_number : 'Offline Customer',
                        customer_email: csvArray.user_id && csvArray.user_id.email ? csvArray.user_id.email : 'Offline Customer',
                        source: csvArray.pickup_location ? csvArray.pickup_location : 'N/A',
                        start: csvArray.sync_trip_id != null ? csvArray.sync_trip_id.start_trip_calendar ? csvArray.sync_trip_id.start_trip_calendar : 'N/A' : 'N/A',
                        destination: csvArray.drop_location ? csvArray.drop_location : 'N/A',
                        Stop: csvArray.sync_trip_id != null ? csvArray.sync_trip_id.finished_trip_calendar ? csvArray.sync_trip_id.finished_trip_calendar : 'N/A' : 'N/A',
                        distance: csvArray.sync_trip_id ? csvArray.sync_trip_id.total_distance_travelled : csvArray.distance,
                        driver: csvArray.driver_id ? csvArray.driver_id.username : 'N/A',
                        assigned_drivers: drivers ? drivers : 'N/A',
                        estimated: csvArray.min_est_fare && csvArray.max_est_fare ? csvArray.min_est_fare + ' -- ' + csvArray.max_est_fare : 'N/A',
                        timetook: csvArray.scheduler_start_date == null ? csvArray.sync_trip_id ? that.timetook(csvArray.accept_time, csvArray.created_at) : 'N/A' : that.timetookScheduled(csvArray.accept_time, csvArray.scheduler_start_date, csvArray.schedule_before),
                        acceptedLocation: csvArray.driver_id && csvArray.assigned_drivers.length > 0 ? that.getAssignedLocation(csvArray.driver_id._id, csvArray.assigned_drivers, 'location') : 'N/A',
                        assignedTime: csvArray.driver_id && csvArray.assigned_drivers.length > 0 ? that.getAssignedLocation(csvArray.driver_id._id, csvArray.assigned_drivers, 'assigned') : 'N/A',
                        distanceToReach: csvArray.driver_id && csvArray.assigned_drivers.length > 0 ? that.getAssignedLocation(csvArray.driver_id._id, csvArray.assigned_drivers, 'distance') : 'N/A',
                        ETA: csvArray.eta ? csvArray.eta : 'N/A',
                        TTRCustomer: csvArray.sync_trip_id ? that.timetook(csvArray.sync_trip_id.reached_customer_calendar, csvArray.accept_time) : 'N/A',
                        TTripTime: csvArray.sync_trip_id ? that.timetook(csvArray.sync_trip_id.finished_trip_calendar, csvArray.sync_trip_id.start_trip_calendar) : 'N/A',
                        TTCancel: csvArray.order_status == 'customer_cancel' ? that.timetook(csvArray.updated_at, csvArray.created_at) : 'N/A',

                        vehicle: csvArray.vehicle_id ? csvArray.vehicle_id.display_name : 'N/A ',
                        vehicle_model: vehicle_model ? vehicle_model : 'N/A',
                        rating: csvArray.rating ? csvArray.rating != -1 ? csvArray.rating : ' N/A ' : 'N/A',
                        //feedback: csvArray.sync_trip_id ? csvArray.sync_trip_id.trip_comment ? csvArray.sync_trip_id.trip_comment : 'N/A' : 'N/A',
                        driver_rating: csvArray.sync_trip_id ? csvArray.sync_trip_id.trip_rating ? csvArray.sync_trip_id.trip_rating != -1 ? csvArray.sync_trip_id.trip_rating : 'N/A' : 'N/A' : 'N/A',
                        partner: csvArray.partner_id ? csvArray.partner_id.name : 'N/A',
                        additional_service: csvArray.additional_service_id ? csvArray.additional_service_id.name : 'N/A',
                        order_type: csvArray.assign_type ? csvArray.assign_type : 'N/A',
                        payment_type: csvArray.sync_trip_id ? csvArray.sync_trip_id.payment_type ? (csvArray.sync_trip_id.payment_type == 1 ? 'Cash' : csvArray.sync_trip_id.payment_type == 2 ? 'Credit' : csvArray.sync_trip_id.payment_type == 3 ? 'Account' : csvArray.sync_trip_id.payment_type == 4 ? 'Online' : 'N/A') : 'N/A' : 'N/A',
                        status: csvArray.order_status ? csvArray.order_status : 'N/A',
                        cancel_reason: csvArray.cancellation_reason_id != null && csvArray.cancellation_reason_id !== '' ? csvArray.cancellation_reason_id.reason : 'N/A',
                        tariff: csvArray.tariff_id ? csvArray.tariff_id.name : 'N/A',
                        km_rate: km_rate.toFixed(2),
                        distance_amount: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_distance : "0",
                        discount: Math.abs(parseFloat(discount2)),
                        extra_toll: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_extra_toll : "0",
                        extra_service: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_extra_service : "0",
                        slow_speed: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_slow_speed : "0",
                        base: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_tariff : "0",
                        vat: csvArray.sync_trip_id ? csvArray.sync_trip_id.vat : "0",
                        adjustment: adjustment,
                        total: csvArray.sync_trip_id ? csvArray.sync_trip_id.total_cost : '0',
                        bill: csvArray.sync_trip_id ? csvArray.sync_trip_id.bill_printed == '1' ? 'Printed' : 'Not Printed' : 'Not Printed'
                      });
                      if (j == res.searchOrder.length - 1) {
                        i = i + parseInt(offset);
                        fetch_status = true;
                      }
                    }
                  }
                }
              }).catch((Error) => {
                that.toastr.warning('Network reset, csv creation restarted')
                that.createCsv(interval_value, offset);
              })
            });
          }
          else {
            let enc_data = that._EncDecService.nwt(that.session_token, new_params);
            //alert(this.dispatcher_id.email)
            let data1 = {
              data: enc_data,
              email: that.useremail
            }
            that._orderService.searchDispatcherOrder(data1).then((res) => {
              if (res && res.status == 200) {
                res = that._EncDecService.dwt(that.session_token, res.data);
                if (res.searchOrder.length > 0) {
                  for (let j = 0; j < res.searchOrder.length; j++) {
                    const csvArray = res.searchOrder[j];
                    if (csvArray.eta) {
                      let temp = csvArray.eta.split(' ');
                      csvArray.eta = temp[0]
                    }
                    let adjustment = '0';
                    let discount2 = '0';
                    if (csvArray.sync_trip_id && csvArray.sync_trip_id.amt_splitup != null && csvArray.sync_trip_id.amt_splitup.length > 0) {
                      let index = csvArray.sync_trip_id.amt_splitup.findIndex(x => x.name == "Adjustment")
                      adjustment = index > -1 ? csvArray.sync_trip_id.amt_splitup[index].amt : '0';
                      let index2 = csvArray.sync_trip_id.amt_splitup.findIndex(x => x.id == "A008")
                      discount2 = index2 > -1 ? csvArray.sync_trip_id.amt_splitup[index2].amt : '0';
                    }
                    let drivers = '';
                    if (csvArray.assigned_drivers_ids) {
                      for (let dData of csvArray.assigned_drivers_ids) {
                        drivers += dData.username + ',';
                      }
                    }
                    let vehicle_model = '';
                    if (csvArray.vehicle_id && csvArray.vehicle_id.vehicle_model_id) {
                      let index = that.modelList4.findIndex(x => x._id == csvArray.vehicle_id.vehicle_model_id)
                      if (index > -1)
                        vehicle_model = that.modelList4[index].name;
                    }
                    let km_rate = 0;
                    if (csvArray.sync_trip_id && csvArray.sync_trip_id.amt_splitup != null && csvArray.sync_trip_id.amt_splitup.length > 0) {
                      let index = csvArray.sync_trip_id.amt_splitup.findIndex(x => x.name == "Distance Amount")
                      km_rate = index > -1 && parseFloat(csvArray.sync_trip_id.total_distance_travelled) !== 0 ? parseFloat(csvArray.sync_trip_id.amt_splitup[index].amt) / parseFloat(csvArray.sync_trip_id.total_distance_travelled) : 0;
                    }
                    res1Array.push({
                      id: csvArray.unique_order_id ? csvArray.unique_order_id : 'N/A',
                      company: csvArray.company_id.company_name,
                      customer_name: csvArray.user_id && csvArray.user_id.firstname ? csvArray.user_id.firstname : 'Offline Customer',
                      customer_phone: csvArray.user_id && csvArray.user_id.phone_number ? csvArray.user_id.phone_number : 'Offline Customer',
                      customer_email: csvArray.user_id && csvArray.user_id.email ? csvArray.user_id.email : 'Offline Customer',
                      source: csvArray.pickup_location ? csvArray.pickup_location : 'N/A',
                      start: csvArray.sync_trip_id != null ? csvArray.sync_trip_id.start_trip_calendar ? csvArray.sync_trip_id.start_trip_calendar : 'N/A' : 'N/A',
                      destination: csvArray.drop_location ? csvArray.drop_location : 'N/A',
                      Stop: csvArray.sync_trip_id != null ? csvArray.sync_trip_id.finished_trip_calendar ? csvArray.sync_trip_id.finished_trip_calendar : 'N/A' : 'N/A',
                      distance: csvArray.sync_trip_id ? csvArray.sync_trip_id.total_distance_travelled : csvArray.distance,
                      driver: csvArray.driver_id ? csvArray.driver_id.username : 'N/A',
                      assigned_drivers: drivers ? drivers : 'N/A',
                      estimated: csvArray.min_est_fare && csvArray.max_est_fare ? csvArray.min_est_fare + ' -- ' + csvArray.max_est_fare : 'N/A',
                      timetook: csvArray.scheduler_start_date == null ? csvArray.sync_trip_id ? that.timetook(csvArray.accept_time, csvArray.created_at) : 'N/A' : that.timetookScheduled(csvArray.accept_time, csvArray.scheduler_start_date, csvArray.schedule_before),
                      acceptedLocation: csvArray.driver_id && csvArray.assigned_drivers.length > 0 ? that.getAssignedLocation(csvArray.driver_id._id, csvArray.assigned_drivers, 'location') : 'N/A',
                      assignedTime: csvArray.driver_id && csvArray.assigned_drivers.length > 0 ? that.getAssignedLocation(csvArray.driver_id._id, csvArray.assigned_drivers, 'assigned') : 'N/A',
                      distanceToReach: csvArray.driver_id && csvArray.assigned_drivers.length > 0 ? that.getAssignedLocation(csvArray.driver_id._id, csvArray.assigned_drivers, 'distance') : 'N/A',
                      ETA: csvArray.eta ? csvArray.eta : 'N/A',
                      TTRCustomer: csvArray.sync_trip_id ? that.timetook(csvArray.sync_trip_id.reached_customer_calendar, csvArray.accept_time) : 'N/A',
                      TTripTime: csvArray.sync_trip_id ? that.timetook(csvArray.sync_trip_id.finished_trip_calendar, csvArray.sync_trip_id.start_trip_calendar) : 'N/A',
                      TTCancel: csvArray.order_status == 'customer_cancel' ? that.timetook(csvArray.updated_at, csvArray.created_at) : 'N/A',

                      vehicle: csvArray.vehicle_id ? csvArray.vehicle_id.display_name : 'N/A ',
                      vehicle_model: vehicle_model ? vehicle_model : 'N/A',
                      rating: csvArray.rating ? csvArray.rating != -1 ? csvArray.rating : ' N/A ' : 'N/A',
                      //feedback: csvArray.sync_trip_id ? csvArray.sync_trip_id.trip_comment ? csvArray.sync_trip_id.trip_comment : 'N/A' : 'N/A',
                      driver_rating: csvArray.sync_trip_id ? csvArray.sync_trip_id.trip_rating ? csvArray.sync_trip_id.trip_rating != -1 ? csvArray.sync_trip_id.trip_rating : 'N/A' : 'N/A' : 'N/A',
                      partner: csvArray.partner_id ? csvArray.partner_id.name : 'N/A',
                      additional_service: csvArray.additional_service_id ? csvArray.additional_service_id.name : 'N/A',
                      order_type: csvArray.assign_type ? csvArray.assign_type : 'N/A',
                      payment_type: csvArray.sync_trip_id ? csvArray.sync_trip_id.payment_type ? (csvArray.sync_trip_id.payment_type == 1 ? 'Cash' : csvArray.sync_trip_id.payment_type == 2 ? 'Credit' : csvArray.sync_trip_id.payment_type == 3 ? 'Account' : csvArray.sync_trip_id.payment_type == 4 ? 'Online' : 'N/A') : 'N/A' : 'N/A',
                      status: csvArray.order_status ? csvArray.order_status : 'N/A',
                      cancel_reason: csvArray.cancellation_reason_id != null && csvArray.cancellation_reason_id !== '' ? csvArray.cancellation_reason_id.reason : 'N/A',
                      tariff: csvArray.tariff_id ? csvArray.tariff_id.name : 'N/A',
                      km_rate: km_rate.toFixed(2),
                      distance_amount: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_distance : "0",
                      discount: Math.abs(parseFloat(discount2)),
                      extra_toll: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_extra_toll : "0",
                      extra_service: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_extra_service : "0",
                      slow_speed: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_slow_speed : "0",
                      base: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_tariff : "0",
                      vat: csvArray.sync_trip_id ? csvArray.sync_trip_id.vat : "0",
                      adjustment: adjustment,
                      total: csvArray.sync_trip_id ? csvArray.sync_trip_id.total_cost : '0',
                      bill: csvArray.sync_trip_id ? csvArray.sync_trip_id.bill_printed == '1' ? 'Printed' : 'Not Printed' : 'Not Printed'
                    });
                    if (j == res.searchOrder.length - 1) {
                      i = i + parseInt(offset);
                      fetch_status = true;
                    }
                  }
                }
              }
            }).catch((Error) => {
              that.toastr.warning('Network reset, csv creation restarted')
              that.createCsv(interval_value, offset);
            })
          }
        }
      }
    }, parseInt(interval_value));
  }
  public timetook(a: any, b: any) {
    //var uaeTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Dubai"});
    let date1 = new Date(a).getTime();
    let date2 = new Date(b).getTime();
    let time = date1 - date2; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";
    let var1 = hoursDiff;
    output += Math.round(var1)

    // if (hoursDiff <= 60) {
    //   let var1 = hoursDiff;
    //   output +=  Math.round(var1) + " seconds";
    // } else if (hoursDiff < 60 * 60) {
    //   let var1 = hoursDiff / 60;
    //   output += Math.round(var1) + " minutes";
    // } else if (hoursDiff < 60 * 60 * 24) {
    //   let var1 = hoursDiff / (60 * 60);
    //   output +=  Math.round(var1) + " hours";
    // } else if (hoursDiff < 60 * 60 * 24 * 30) {
    //   let var1 = hoursDiff / (60 * 60 * 24);
    //   output += Math.round(var1) + " days";
    // } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
    //   let var1 = hoursDiff / (60 * 60 * 24 * 30);
    //   output += Math.round(var1) + " months";
    // }
    return hoursDiff < 0 ? '0' : output == 'NaN' ? 'N/A' : output;
  }
  public timetookScheduled(a: any, b: any, c: any) {
    //var uaeTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Dubai"});
    let date1 = new Date(a).getTime();
    let date2 = new Date(b).getTime();
    let time = date1 - (date2 - (c * 60000)); //msec
    let hoursDiff = time / 1000; //secs
    var output = "";
    let var1 = hoursDiff;
    output += Math.round(var1);
    // if (hoursDiff <= 60) {
    //   let var1 = hoursDiff;
    //   output +=  Math.round(var1) + " seconds";
    // } else if (hoursDiff < 60 * 60) {
    //   let var1 = hoursDiff / 60;
    //   output += Math.round(var1) + " minutes";
    // } else if (hoursDiff < 60 * 60 * 24) {
    //   let var1 = hoursDiff / (60 * 60);
    //   output +=  Math.round(var1) + " hours";
    // } else if (hoursDiff < 60 * 60 * 24 * 30) {
    //   let var1 = hoursDiff / (60 * 60 * 24);
    //   output += Math.round(var1) + " days";
    // } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
    //   let var1 = hoursDiff / (60 * 60 * 24 * 30);
    //   output += Math.round(var1) + " months";
    // }
    return hoursDiff < 0 ? '0' : output == 'NaN' ? 'N/A' : output;
  }
  public getAssignedLocation(a, data, type) {
    let data2 = '';
    data.forEach(element => {
      if (a == element.driver_id) {
        //   if(element.driver_loc){
        //   let geocoder = new google.maps.Geocoder();
        //   let latlngvalue = element.driver_loc.split(",");
        //   let latlng = { lat: parseFloat(latlngvalue[0]), lng: parseFloat(latlngvalue[1]) }
        //   let that=this
        //   geocoder.geocode({ location: latlng }, function (results, status) {
        //     if (results) {
        //       switch(type){
        //         case 'location':
        //         data2= results[0].formatted_address+'  ('+element.driver_loc+')';
        //         break;
        //         case 'distance':
        //         data2= element.distance_customer;
        //         break;
        //         case 'assigned':
        //         data2= element.assigned_at;
        //         break;
        //       }
        //     }
        //     else{
        //       data2= 'NA'
        //     }
        //   });
        // }}
        // else{
        switch (type) {
          case 'location':
            data2 = element.driver_loc ? element.driver_loc : 'NA';
            break;
          case 'distance':
            data2 = element.distance_customer ? element.distance_customer : 'NA';
            break;
          case 'assigned':
            data2 = element.assigned_at ? element.assigned_at : 'NA';
            break;
        }
      }
    })
    return data2;
  }
  public timetookKey(a: any, b: any) {
    //var uaeTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Dubai"});
    let date1 = new Date(a).getTime();
    let date2 = new Date(b).getTime();
    let time = date1 - date2; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";

    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output += Math.round(var1) == 1 ? Math.round(var1) + " second" : Math.round(var1) + " seconds";
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      output += Math.round(var1) + " minutes";
      // let decimal=(Math.floor((var1.after())*.6)).toString().substring(0,2)
      // decimal=decimal=='01' || decimal=='1'?decimal+' second':decimal+' seconds';
      // output = Math.floor(var1)!==1?Math.floor(var1) + " minutes and " +decimal:Math.floor(var1) + " minutes and " +decimal;    
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      output += Math.round(var1) + " hours";
      // let decimal=(Math.floor((var1.lo)*.6)).toString().substring(0,2)
      // decimal=decimal=='01' || decimal=='1'?decimal+' minute':decimal+' minutes'
      // output = Math.floor(var1)!==1?Math.floor(var1) + " hours and " +decimal:Math.floor(var1) + " hour and " +decimal;    
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output += Math.floor(var1) + " days";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output += Math.floor(var1) + " months";
    }
    return hoursDiff < 0 ? '0 second' : output;
  }
  public reverseGCListKey = true;
  public acceptedLocation = '';
  public distanceToReach = '';
  public timeToReach = '';
  public acceptedTime = '';
  reversGCKey(loc, c_loc, c_dur, assigned_at) {
    if (!this.reverseGCListKey)
      return;
    else
      this.reverseGCListKey = false;
    let geocoder = new google.maps.Geocoder();
    let latlngvalue = loc.split(",");
    let latlng = { lat: parseFloat(latlngvalue[0]), lng: parseFloat(latlngvalue[1]) }
    let that = this
    geocoder.geocode({ location: latlng }, function (results, status) {
      if (results) {
        that.acceptedLocation = results[0].formatted_address + '  (' + loc + ')';
        that.distanceToReach = c_loc;
        that.timeToReach = c_dur;
        that.acceptedTime = assigned_at;
      }
    });
  }
  public timetookScheduledKey(a: any, b: any, c: any) {
    //var uaeTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Dubai"});
    let date1 = new Date(a).getTime();
    let date2 = new Date(b).getTime();
    let time = date1 - (date2 - (c * 60000)); //msec
    let hoursDiff = time / 1000; //secs
    var output = "";

    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output += Math.round(var1) == 1 ? Math.round(var1) + " second" : Math.round(var1) + " seconds";
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      output += Math.round(var1) + " minutes";
      // let decimal=(Math.floor((var1.after())*.6)).toString().substring(0,2)
      // decimal=decimal=='01'?decimal+' second':decimal+' seconds'
      // output = Math.floor(var1)!==1?Math.floor(var1) + " minutes and " +decimal:Math.floor(var1) + " minute and " +decimal;    
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      output += Math.round(var1) + " hours";
      // let decimal=(Math.floor((var1.after())*.6)).toString().substring(0,2)
      // decimal=decimal=='01' || decimal=='1'?decimal+' minute':decimal+' minutes'
      // output = Math.floor(var1)!==1?Math.floor(var1) + " hours and " +decimal:Math.floor(var1) + " hour and " +decimal;    
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output += Math.floor(var1) + " days";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output += Math.floor(var1) + " months";
    }
    return hoursDiff < 0 ? '0 secs' : output;
  }
  round(data) {
    if (parseFloat(data) > 0)
      return parseFloat(data).toFixed(2);
    else
      return 0;
  }
  public createMarkerForLiveTracking(position, content, icon, label: string, title) {
    let that = this;
    let marker;
    if (label !== '') {
      marker = new google.maps.Marker({
        position: position,
        map: this.map,
        icon: icon,
        label: { text: label, color: "white" },
        title: title
      });
    }
    else {
      marker = new google.maps.Marker({
        position: position,
        map: this.map,
        icon: icon,
        title: title
      });
    }
    this.picking.push(marker);
    if (content !== '') {
      var infowindow = new google.maps.InfoWindow({
        content: content
      });
      marker.addListener('mouseover', function () {
        infowindow.open(that.map, marker);
      });
      marker.addListener('mouseout', function () {
        infowindow.close();
      });
    }
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.company_id_array
    };
    var encrypted = this._EncDecService.nwt(this.session_token, param);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        //this.CompanyLength = data.count;
        this.companyData = data.getCompanies;
        if (this.dtc && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
          this.company_id_list2 = data.getCompanies.map(x => x._id);
          this.company_id_list = data.getCompanies.map(x => x._id);
          this.companyIdFilter = data.getCompanies.map(x => x._id)
          this.companyIdFilter.push('all')
          this.companyIdFilter2 = this.companyIdFilter.slice();
        }
      }
      //this.searchSubmit = false;
    });
  }
  clearTBEnd() { //clears timebased input field
    this.addDispatcher.controls["tbEndTime"].setValue('');
  }
  customerEditForm: any = {
    name: '',
    phone: 0,
    email: '',
    dialCode: 971,
    error: ''
  }
  toggleCustomerEdit() {
    this.editCustomer = !this.editCustomer;
  }
  updateCustomerDetails() {
    this.customerEditForm.error = '';
    var re = /^[0-9]+$/;
    var ree = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!this.customerEditForm.phone || !this.customerEditForm.name) {
      this.customerEditForm.error = 'Please fill all fields';
      return;
    }
    if (!re.test(this.customerEditForm.phone)) {
      this.customerEditForm.error = 'Please enter valid phone number';
      return;
    }
    if (this.customerEditForm.email && !ree.test(this.customerEditForm.email)) {
      this.customerEditForm.error = 'Please enter valid E-mail address';
      return;
    }
    if (this.addDispatcher.value.customerCountryCode) {
      var regex = /^(\+?\d{1,3}|\d{1,4})$/gm;
      if (this.addDispatcher.value.customerCountryCode.code && !regex.test(this.addDispatcher.value.customerCountryCode.code.toString())) {
        this.customerEditForm.error = 'Please enter valid country code 1';
        return;
      }
      else if (!this.addDispatcher.value.customerCountryCode.code && !regex.test(this.addDispatcher.value.customerCountryCode)) {
        this.customerEditForm.error = 'Please enter valid country code';
        return;
      }
      if (!this.addDispatcher.value.customerCountryCode.code) {
        this.addDispatcher.value.customerCountryCode = this.addDispatcher.value.customerCountryCode.substr(0, 1) == '+' ? this.addDispatcher.value.customerCountryCode.substr(1) : this.addDispatcher.value.customerCountryCode;
      }
    }

    var item = this.customerEditForm.phone.toString().slice(0);
    if (item[0] == 0) {
      this.customerEditForm.phone = this.customerEditForm.phone.toString().substr(1);
    }
    let params = {
      company_id: this.company_id_array,
      customer_id: this.SelectedCustomer._id,
      firstname: this.customerEditForm.name,
      country_code: this.addDispatcher.value.customerCountryCode ? this.addDispatcher.value.customerCountryCode.code ? this.addDispatcher.value.customerCountryCode.code : this.addDispatcher.value.customerCountryCode : '971',
      phone_number: this.customerEditForm.phone.toString(),
      email: this.customerEditForm.email
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let data = {
      data: enc_data,
      email: this.useremail
    }
    this.customerEditForm.isLoading = true;
    this._customerService
      .updateCustomerDetails(data)
      .then(data => {
        this.customerEditForm.isLoading = false;
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.OnSelectName({ source: { value: data.data } });
          this.OnSelectPhone({ source: { value: data.data } });
          this.toggleCustomerEdit();
          this.customerEditForm.error = '';
        }
        else if (data) {
          this.toastr.error(data.message)
          this.customerEditForm.error = data.message;
        }
      });
  }
  toggleTimeBased(event) {
    if (event.checked) {
      this.addDispatcher.controls["tbEndTime"].enable();
      this.addDispatcher.controls["tbEndTime"].setValidators(
        Validators.required
      );
      this.addDispatcher.controls["tbEndTime"].updateValueAndValidity();
    }
    else {
      this.addDispatcher.controls["tbEndTime"].disable();
      this.addDispatcher.controls["tbEndTime"].setValidators([]);
    }
  }
  async getDriversInDriverGroup(id) {
    const params = {
      group_id: id,
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
    };
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    return this._driverService.driversInDriverGroup(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        if (data.Drivers.length > 0)
          return data.Drivers.map(x => x._id);
        else
          return [];
      }
      else {
        return [];
      }
    });
  }
  multiFilter = {
    driver: [],
    side: [],
    partner: [],
    pickup: [],
    drop: [],
    driver_group: []
  };
  removeMultiSelect(data, id) {
    let index;
    switch (id) {
      case 1:
        index = this.multiFilter.driver.indexOf(data);
        this.multiFilter.driver.splice(index, 1);
        break;
      case 2:
        index = this.multiFilter.side.indexOf(data);
        this.multiFilter.side.splice(index, 1);
        break;
      case 3:
        index = this.multiFilter.partner.indexOf(data);
        this.multiFilter.partner.splice(index, 1);
        break;
      case 4:
        index = this.multiFilter.pickup.indexOf(data);
        this.multiFilter.pickup.splice(index, 1);
        break;
      case 5:
        index = this.multiFilter.drop.indexOf(data);
        this.multiFilter.drop.splice(index, 1);
        break;
      case 6:
        index = this.multiFilter.driver_group.indexOf(data);
        this.multiFilter.driver_group.splice(index, 1);
        break;
    }
  }
  filters;
  getFilters(event) {
    if (typeof event == 'object') {
      let params = event.value.params;
      this.ordersFilter = this.unique_order_id = params.keyword;
      this.multiFilter.driver = params.driver;
      this.tariff_id = params.tariffid;
      this.dispatcherForOrder.setValue(params.dispatcher);
      this.selectedMoment = params.start_date ? new Date(params.start_date) : '';
      this.selectedMoment1 = params.end_date ? new Date(params.end_date) : '';
      this.order_limit = params.limit;
      this.sort_type = params.sortOrder;
      this.sort_by_coloumn = params.sortByColumn;
      if (params.status.value.length > 0) {
        this.current_order_status = params.status.name;
        this.statuses = params.status.value;
      }
      this.orderComments = params.order_comment;
      this.multiFilter.side = params.side;
      this.multiFilter.pickup = params.pickup_location;
      this.multiFilter.drop = params.drop_location;
      this.user_name = params.user_name;
      this.user_phone = params.user;
      this.multiFilter.partner = params.partner;
      this.generated_by = params.generate_by;
      if (this.generated_by)
        this.colorFilter(this.generated_by)
      this.promo_id = params.promo_id;
      this.payment_type_id = params.payment_type_id;
      this.flagged_orders = params.flagged_orders;
      this.multiFilter.driver_group = params.driver_groups_id;
      this.order_edited = params.order_edited;
      this.cancelReasons = params.cancel_reason;
      this.cancelReasonId = params.cancel_reason_id;
      this.vehicle_id_list = params.vehicle_category_ids;
      this.vehicleChpModel = params.vehicle_model;
      this.vehicleChpModelId = params.model_id;
      this.company_id_list = params.company_id;
      this.getOrders(false)
    }
    else {
      const params = {
        user_id: this.CUser,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
      };
      var encrypted = this._EncDecService.nwt(this.session_token, params);
      var enc_data = {
        data: encrypted,
        email: this.useremail
      }
      this._orderService.getFilters(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.filters = data.data;
        }
      });
    }
  }
  saveFilter() {
    this.saveFilterFlag = true;
    let params2 = {
      keyword: this.unique_order_id.length != 0 ? this.unique_order_id : [],
      driver: this.multiFilter.driver.length != 0 ? this.multiFilter.driver : [],
      tariffid: this.tariff_id ? this.tariff_id : '',
      dispatcher: this.dispatcherForOrder.value ? this.dispatcherForOrder.value : '',
      offset: 0,
      limit: this.order_limit != '' ? this.order_limit : this.pageSize,
      sortOrder: this.sort_type != '' ? this.sort_type : '',
      sortByColumn: this.sort_by_coloumn != '' ? this.sort_by_coloumn : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      order_comment: this.orderComments ? this.orderComments : '',
      side: this.multiFilter.side.length != 0 ? this.multiFilter.side : [],
      status: this.statuses ? { name: this.current_order_status, value: this.statuses } : [],
      pickup_location: this.multiFilter.pickup.length != 0 ? this.multiFilter.pickup : [],
      drop_location: this.multiFilter.drop.length != 0 ? this.multiFilter.drop : [],
      user: this.user_phone ? this.user_phone : '',
      user_name: this.user_name ? this.user_name : '',
      userid: this.user_id ? this.user_id : '',
      partner: this.multiFilter.partner.length != 0 ? this.multiFilter.partner : [],
      promo_id: this.promo_id ? this.promo_id : '',
      payment_type_id: this.payment_type_id ? this.payment_type_id : '',
      driver_groups_id: this.multiFilter.driver_group.length != 0 ? this.multiFilter.driver_group : [],
      order_edited: this.order_edited ? this.order_edited : '',
      cancel_reason: this.cancelReasons && this.cancelReasons.length != 0 ? this.cancelReasons : [],
      cancel_reason_id: this.cancelReasonId && this.cancelReasonId.length != 0 ? this.cancelReasonId : [],
      vehicle_category_ids: this.vehicle_id_list ? this.vehicle_id_list : '',
      flagged_orders: this.flagged_orders ? this.flagged_orders : '',
      vehicle_model: this.vehicleChpModel && this.vehicleChpModel.length != 0 ? this.vehicleChpModel : [],
      model_id: this.vehicleChpModelId && this.vehicleChpModelId.length != 0 ? this.vehicleChpModelId : [],
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
      generate_by: this.generated_by ? this.generated_by : '',
      is_scheduler: this.is_scheduler ? '1' : '',
      date_filter_order: this.date_filter_order ? this.date_filter_order : 'updated_at'
    };
    const params = {
      user_id: this.CUser,
      page: "dashboard",
      filter_name: this.filter_name,
      params: params2,
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array,
    };
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._orderService.saveFilters(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        this.toastr.success('Filter saved successfully')
        this.getFilters('')
        this.filter_name = "";
      }
      else {
        this.toastr.error(dec.message ? dec.message : 'Something went wrong')
      }
      this.saveFilterFlag = false;
    });
  }
  public tempZones;
  searchPickupDrop(data, id) {
    if (typeof data == 'object' && id == 1) {
      if (this.multiFilter.pickup.findIndex(x => x._id == data._id) == -1)
        this.multiFilter.pickup.push(data)
    }
    else if (typeof data == 'object' && id == 2) {
      if (this.multiFilter.drop.findIndex(x => x._id == data._id) == -1)
        this.multiFilter.drop.push(data)
    }
    else {
      this.zones = data ? this.tempZones.filter(s => (s && new RegExp(`${data}`, 'gi').test(s.name)))
        : this.zones;
    }
  }
  searchDriverGroup(data) {
    if (typeof data == 'object')
      if (this.multiFilter.driver_group.findIndex(x => x._id == data._id) == -1)
        this.multiFilter.driver_group.push(data)
  }
  getSocketSession() {
    let param_socket = {
      company_id: this.company_id_list2.length > 0 ? this.company_id_list2 : this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._usersService.getSocketsession(data1).then((res1) => {
      if (res1 && res1.status == 200) {
        var res_data1: any = this._EncDecService.dwt(this.session_token, res1.data);
        if (res1.status == 200) {
          this.socket_session_token_map = res_data1.socket_token;
        }
      }
    })
  }
  selectAllCompany(id) {
    if (this.company_id_list.length !== this.companyData.length && id == 1) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.company_id_list = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else if (this.company_id_list2.length !== this.companyData.length && id == 2) {
      this.companyIdFilter2 = this.companyData.slice().map(x => x._id);
      this.company_id_list2 = this.companyIdFilter2.slice()
      this.companyIdFilter2.push('all')
    }
    else {
      if (id == 1) {
        this.company_id_list = [];
        this.companyIdFilter = [];
      }
      else {
        this.mapSocketCountList = [];
        this.company_id_list2 = [];
        this.companyIdFilter2 = [];
      }
    }
  }
  companyManualSelect(id) {
    if (id == 1) {
      let tempArray = this.companyIdFilter.slice()
      this.companyIdFilter = [];
      let index = tempArray.indexOf('all');
      if (index > -1) {
        tempArray.splice(index, 1);
        this.companyIdFilter = tempArray;
        this.company_id_list = this.companyIdFilter.slice()
      }
      else {
        this.company_id_list = tempArray.slice();
        this.companyIdFilter = tempArray;
      }
    }
    else {
      this.mapSocketCountList = [];
      let tempArray = this.companyIdFilter2.slice()
      this.companyIdFilter2 = [];
      let index = tempArray.indexOf('all');
      if (index > -1) {
        tempArray.splice(index, 1);
        this.companyIdFilter2 = tempArray;
        this.company_id_list2 = this.companyIdFilter2.slice()
      }
      else {
        this.company_id_list2 = tempArray.slice();
        this.companyIdFilter2 = tempArray;
      }
    }
  }
  alertOrder(dt: any) {
    var uaeTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Dubai" });
    let date1 = new Date(dt).getTime();
    let date2 = new Date(uaeTime).getTime();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    if (hoursDiff >= 60) {
      return true;
    }
    else
      return false;
  }
  notificationPop(event) {
    let dialogRef = this.dialog.open(DispatchNotificationsComponent, {
      width: "400px",
      data: event,
      scrollStrategy: this.overlay.scrollStrategies.noop()
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  customerRoute(event) {
    if (this.customerAcl && this.adminAccessAcl) {
      window.open('https://mobilityae.com/admin/customer/detail/' + event.user_id._id, '_blank');
    }
  }
  getOrderAcceptanceHistory(id) {
    let param_socket = {
      order_id: id,
      company_id: this.company_id_list2.length > 0 ? this.company_id_list2 : this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, param_socket);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOrderAcceptanceHistory(data1).then((res) => {
      if (res && res.status == 200) {
        if (this.showModel) {
          var res_data: any = this._EncDecService.dwt(this.session_token, res.data);
          res_data.data.forEach(element => {
            let index = this.orderInfoKey.assigned_drivers.findIndex(x => x.driver_id == element.driver_id);
            if (index > -1) {
              switch (element.order_accept_reject) {
                case '1':
                  this.orderInfoKey.assigned_drivers[index].status = 'Accepted';
                  break;
                case '2':
                  this.orderInfoKey.assigned_drivers[index].status = 'Rejected';
                  break;
                case '3':
                  this.orderInfoKey.assigned_drivers[index].status = 'Accepted then rejected';
                  break;
                case '4':
                  this.orderInfoKey.assigned_drivers[index].status = 'Driver cancel';
                  break;
                default:
                  break;
              }
            }
          });
        }
      }
    })
  }
  sendOrdertoCustomer(event) {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: "400px",
      data: { text: "Are you sure you want to send order details as SMS ?", type: true },
      scrollStrategy: this.overlay.scrollStrategies.noop()
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        const params = {
          phone_number: event.user_id.phone_number,
          country_code: event.user_id.country_code,
          unique_order_id: event.unique_order_id,
          company_id: this.company_id_array
        };
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._customerService.dispatcherSms(data1).then(data => {
          if (data && data.status == 200) {
            this.toastr.success("Order details send successfully")
          } else {
            this.toastr.error(data.message)
          }
        });
      } else {
      }
    });
  }
  makaniPickup = [];
  makaniDrop = [];
  getMakani(makani_number, input_type) {
    if (typeof makani_number == 'object')
      return;
    if (input_type == 'pickup')
      this.loadingIndicator = "makaniPickup";
    else
      this.loadingIndicator = "makaniDrop";
    var patt = /([0-9]){5,5}\s?([0-9]){5,5}$/;
    var result = patt.test(makani_number);
    if (!result) {
      this.loadingIndicator = "";
      if (input_type == 'pickup')
        this.formErrors.makaniPickup = "Invalid makani number";
      else
        this.formErrors.makaniDrop = "Invalid makani number";
      return
    }
    if (input_type == 'pickup')
      this.formErrors.makaniPickup = "";
    else
      this.formErrors.makaniDrop = "";
    var http = new XMLHttpRequest();
    var url = 'https://mobilityae.com/makani/get-makani-details.php';
    var params = 'makani_number=' + makani_number.replace(' ', '');
    http.open('POST', url, true);

    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    let that = this
    http.onreadystatechange = function () {
      if (http.readyState == 4 && http.status == 200) {
        that.loadingIndicator = "";
        let data = JSON.parse(http.responseText).data;
        if (input_type == 'pickup') {
          that.makaniPickup = data.MAKANI_INFO;
        }
        else {
          that.makaniDrop = data.MAKANI_INFO;
        }
      }
    }
    http.send(params);
  }
  displayFnMakani(data) {
    return data ? data.COMMUNITY_E : '';
  }
  setMakani(data, input_type) {
    if (input_type == 'pickup') {
      this.pickupLat = '';
      this.pickupLang = '';
      this.pickupMarkers = [];
      this.addDispatcher.controls["pickup_location"].setValue(data.COMMUNITY_E);
      this.addDispatcher.controls["pickup_country"].setValue("United Arab Emirates");
      this.direction.origin = String(
        data.LATLNG.split(',')[0] + "," + data.LATLNG.split(',')[1]
      );
      this.showDirection();
    }
    else if (input_type == 'drop') {
      this.dropOffLang = "";
      this.dropOffLat = "";
      this.dropOffMarkers = [];
      this.addDispatcher.controls["drop_location"].setValue(data.COMMUNITY_E);
      this.addDispatcher.controls["drop_country"].setValue("United Arab Emirates");
      //this.dropOffMarkers.push([splits[1], splits[0]]);
      //this.dropOffLat = splits[1];
      //this.dropOffLang = splits[0];
      this.direction.destination = String(
        data.LATLNG.split(',')[0] + "," + data.LATLNG.split(',')[1]
      );
      this.showDirection();
    }
  }
}
class RotateIcon {
  options: any;
  img: HTMLImageElement;
  context: CanvasRenderingContext2D;
  canvas: any;

  constructor(options) {
    this.options = options;
    this.img = options.img.cloneNode();
    this.options.width = options.pixel;
    this.options.height = options.pixel;
    let canvas = document.createElement("canvas");
    canvas.width = this.options.width;
    canvas.height = this.options.height;
    this.context = canvas.getContext("2d");
    this.canvas = canvas;
  }

  setRotation(options) {
    let angle = options.deg ? (options.deg * Math.PI) / 180 : options.rad;
    let centerX = this.options.width / 2;
    let centerY = this.options.height / 2;
    this.context.clearRect(0, 0, this.options.width, this.options.height);
    this.context.save();
    this.context.translate(centerX, centerY);
    this.context.rotate(angle);
    this.context.translate(-centerX, -centerY);
    this.context.drawImage(this.img, 0, 0);
    this.context.restore();
    return this;
  }
  getUrl() {
    return this.canvas.toDataURL("image/png");
  }
}
class RotateIcon2 {
  options: any;
  img: HTMLImageElement;
  context: CanvasRenderingContext2D;
  canvas: any;

  constructor(options) {
    this.options = options;
    this.img = options.img.cloneNode();
    this.options.width = 12;
    this.options.height = 12;
    let canvas = document.createElement("canvas");
    canvas.width = this.options.width;
    canvas.height = this.options.height;
    this.context = canvas.getContext("2d");
    this.canvas = canvas;
  }

  setRotation(options) {
    let angle = options.deg ? (options.deg * Math.PI) / 180 : options.rad;
    let centerX = this.options.width / 2;
    let centerY = this.options.height / 2;
    this.context.clearRect(0, 0, this.options.width, this.options.height);
    this.context.save();
    this.context.translate(centerX, centerY);
    this.context.rotate(angle);
    this.context.translate(-centerX, -centerY);
    this.context.drawImage(this.img, 0, 0);
    this.context.restore();
    return this;
  }
  getUrl() {
    return this.canvas.toDataURL("image/png");
  }
}
