import { Component, OnInit, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../common/services/user/user.service';
import { JwtService } from '../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { Headers, Http, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../environments/environment.prod'
import * as moment from "moment/moment";
import { EncDecService } from '../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { GlobalService } from '../../common/services/global/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {

  // public company_id = '5ce12918aca1bb08d73ca25d'; 
  // dev company_id
  // public company_id = '5cd982667a64fe51e5d3f7a0';
  // Live company
  // public company_id = '5d0cc77b9528d408cb14e2e5'; 
  // Hala Company
  // public company_id = '5dcba9e0b0e06e5c5ac1aea1';
  // AAA company
  //public company_id = '5dd4e69ce127306596f0e247';
  // Pak company

  public DispatcherLogin: FormGroup;
  public loginSubmit = false;
  public userDetails = {
    browser: '',
    browser_version: '',
    os: '',
    user_ip: '',
    user_city: '',
    user_country: '',
    user_lat: '',
    user_lng: ''
  }
  constructor(private formBuilder: FormBuilder,
    private _usersService: UsersService,
    private router: Router,
    private _jwtService: JwtService,
    public toastr: ToastsManager,
    private _EncDecService: EncDecService,
    vcr: ViewContainerRef,
    public http: Http,
    public _global: GlobalService
  ) {
    this._global.setLogoutStatus(true)
    this.toastr.setRootViewContainerRef(vcr);
    this.DispatcherLogin = formBuilder.group({
      email: ['', [
        Validators.email,
      ]],
      password: ['', [
        Validators.required
      ]]
    });

    this.http.get('https://ipapi.co/json/').subscribe((data) => {
      var ipdetails = data.json();
      this.userDetails.user_ip = ipdetails.ip;
      this.userDetails.user_city = ipdetails.city;
      this.userDetails.user_country = ipdetails.country_name;
      this.userDetails.user_lat = ipdetails.latitude;
      this.userDetails.user_lng = ipdetails.longitude;
      localStorage.setItem('user-details', JSON.stringify(this.userDetails));
    })
    sessionStorage.clear();
  }

  ngOnInit() {
    this.browserDet();
    const adminUser = this._jwtService.getDispatcherUser();
    if (adminUser) {
      this.router.navigate(['/dispatcher']);
    } else {
      this._jwtService.destroyDispatcherToken();
      this.router.navigate(['/dispatcher/login']);
    }
  }
  public makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  public makezeroid(length) {
    var result = '';
    for (var i = 0; i < length; i++) {
      result += '0';
    }
    return result;
  }
  public login() {
    this.loginSubmit = true;
    if (!this.DispatcherLogin.valid) {
      this.toastr.error('Invalid Login Details.');
      this.loginSubmit = false;
      return;
    }
    let email = this.DispatcherLogin.value.email;
    let pwd = this.DispatcherLogin.value.password;
    let that = this;
    var date = moment();
    let timestamp = moment(date).format("X");
    //console.log(timestamp);
    let remaining_key_length_required = 32 - timestamp.length;
    let extra_key_data = this.makeid(remaining_key_length_required);
    let final_key = timestamp + extra_key_data;
    //console.log(final_key);
    let initial_param = {
      'emaildata': email,
      //'company_id': that.company_id,
      'timestamp': final_key
    }
    //console.log("response+++++++++++++++++" + JSON.stringify(initial_param));
    that._usersService.get_initial_token(initial_param)
      .then((res) => {
        if (res.status == 200) {
          var responseitem: any = that._EncDecService.loginDwt(final_key, res.data);
          let initial_token = responseitem.token;
          //console.log("initial_token" + initial_token);
          const userdata = {
            'email': email,
            'password': pwd,
            'role': 2
          };
          var encrypted_data: any = this._EncDecService.loginNwt(initial_token, userdata);
          //console.log("encrypted_data" + encrypted_data);
          let data = [];
          data.push(that.DispatcherLogin.value.email);
          data.push(encrypted_data);
          that._usersService.login(data)
            .then((res) => {
              if (res.status == 200) {
                this._global.setLogoutStatus(false)
                let remaining_key_length_required1 = 32 - pwd.length;
                let extra_key_data1 = that.makezeroid(remaining_key_length_required1);
                let final_key1 = pwd + extra_key_data1;
                //console.log("zero added key+++++" + final_key1)
                var res_data: any = that._EncDecService.loginDwt(final_key1, res.data);
                //console.log("response" + JSON.stringify(res_data));
                if (res_data.token) {
                  this._jwtService.saveUser(res_data.token, res_data.user, res_data.menu);
                  window.localStorage['CUser'] = res_data.user._id;
                  window.localStorage['Sessiontoken'] = res_data.session_token;
                  window.localStorage['user_email'] = res_data.user.email;
                  window.localStorage['user_company'] = res_data.user.company_id;
                  window.localStorage['companydata'] = JSON.stringify(res_data.company_details[0]);
                  //window.localStorage.setItem('user-details', JSON.stringify(this.userDetails));
                  window.localStorage['ImageExt'] = "/" + res_data.user._id + "/" + res_data.token + "/" + "DI";
                  let param_socket = {
                    company_id: res_data.user.company_id
                  }
                  let enc_data = this._EncDecService.loginNwt(res_data.session_token, param_socket);
                  //alert(this.dispatcher_id.email)
                  let data1 = {
                    data: enc_data,
                    email: res_data.user.email
                  }
                  that._usersService.getSocketsession(data1).then((res1) => {
                    //console.log(JSON.stringify(res1));
                    if (res1.status == 200) {
                      this.loginSubmit = false;
                      this.toastr.success('login successfully!');
                      var res_data1: any = that._EncDecService.loginDwt(res_data.session_token, res1.data);
                      window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
                      window.localStorage['socketFlag'] = '1';
                      this.router.navigate(['/dispatcher']);
                    } else {
                      this.loginSubmit = false;
                      this.toastr.error(res.message);
                    }
                  })
                } else {
                  this.loginSubmit = false;
                  this.toastr.error('Invalid login or password!');
                }
              } else {
                this.loginSubmit = false;
                this.toastr.error('Invalid login or password!');
              }
            })
            .catch((error) => {
              this.loginSubmit = false;
              console.log('error', error);
            });
        } else {
          this.loginSubmit = false;
          this.toastr.error(res.message);
        }
      }).catch((error) => {
        console.log('error', error);
        this.loginSubmit = false;
        this.toastr.error('Failed to connect to server');
      });
  }
  browserDet() {
    var objappVersion = navigator.appVersion;
    var objAgent = navigator.userAgent;
    var objbrowserName = navigator.appName;
    var objfullVersion = '' + parseFloat(navigator.appVersion);
    var objBrMajorVersion = parseInt(navigator.appVersion, 10);
    var objOffsetName, objOffsetVersion, ix;

    // In Chrome 
    if ((objOffsetVersion = objAgent.indexOf("Chrome")) != -1) {
      objbrowserName = "Chrome";
      objfullVersion = objAgent.substring(objOffsetVersion + 7);
    }
    // In Microsoft internet explorer
    else if ((objOffsetVersion = objAgent.indexOf("MSIE")) != -1) {
      objbrowserName = "Microsoft Internet Explorer";
      objfullVersion = objAgent.substring(objOffsetVersion + 5);
    }

    // In Firefox
    else if ((objOffsetVersion = objAgent.indexOf("Firefox")) != -1) {
      objbrowserName = "Firefox";
    }
    // In Safari 
    else if ((objOffsetVersion = objAgent.indexOf("Safari")) != -1) {
      objbrowserName = "Safari";
      objfullVersion = objAgent.substring(objOffsetVersion + 7);
      if ((objOffsetVersion = objAgent.indexOf("Version")) != -1)
        objfullVersion = objAgent.substring(objOffsetVersion + 8);
    }
    // For other browser "name/version" is at the end of userAgent 
    else if ((objOffsetName = objAgent.lastIndexOf(' ') + 1) <
      (objOffsetVersion = objAgent.lastIndexOf('/'))) {
      objbrowserName = objAgent.substring(objOffsetName, objOffsetVersion);
      objfullVersion = objAgent.substring(objOffsetVersion + 1);
      if (objbrowserName.toLowerCase() == objbrowserName.toUpperCase()) {
        objbrowserName = navigator.appName;
      }
    }
    // trimming the fullVersion string at semicolon/space if present
    if ((ix = objfullVersion.indexOf(";")) != -1)
      objfullVersion = objfullVersion.substring(0, ix);
    if ((ix = objfullVersion.indexOf(" ")) != -1)
      objfullVersion = objfullVersion.substring(0, ix);

    objBrMajorVersion = parseInt('' + objfullVersion, 10);
    if (isNaN(objBrMajorVersion)) {
      objfullVersion = '' + parseFloat(navigator.appVersion);
      objBrMajorVersion = parseInt(navigator.appVersion, 10);
    }
    this.userDetails.browser = objbrowserName;
    this.userDetails.browser_version = objfullVersion
    this.userDetails.os = navigator.appVersion;
    localStorage.setItem('user-details', JSON.stringify(this.userDetails));
  }
}
