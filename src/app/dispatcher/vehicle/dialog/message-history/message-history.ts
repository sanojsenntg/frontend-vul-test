import { Component, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MessageService } from '../../../../common/services/message/message.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { DispatcherService } from '../../../../common/services/dispatcher/dispatcher.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-message-history',
  templateUrl: 'message-history.html',
  styleUrls: ['./message-history.css']
})
export class MessageHistoryComponent {
  public session_token;
  public useremail;
  public company_id_array = [];
  public usercompany;
  public socket_session_token;
  public vehicleData;
  public DispatcherUser;
  public vehicleList;
  public CurrentUser;
  public messageData;
  public dispatcher_id = '';
  public vehicle_id = '';
  public days = '1';
  public driver_id;
  public

  constructor(
    private router: Router,
    public _messageService: MessageService,
    public _JwtService: JwtService,
    public _vehicleService: VehicleService,
    public _dispatcherService: DispatcherService,
    public toastr: ToastsManager,
    private _EncDecService: EncDecService,
    vcr: ViewContainerRef,
    public dialogRef: MatDialogRef<MessageHistoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.vehicleData = data;
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany)
    let params1 = {
      company_id: this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params1);
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._dispatcherService.getDispatcherUser(data1).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.DispatcherUser = data.getDispatchers;
      }
    });
    const params2 = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      role: 'dispatcher',
      company_id: this.company_id_array
    };
    let enc_data1 = this._EncDecService.nwt(this.session_token, params2);
    let data2 = {
      data: enc_data1,
      email: this.useremail
    }
    this._vehicleService.getVehicleListing(data2).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var vehicleListData: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.vehicleList = vehicleListData.getVehicles;
      }
    });

    if (typeof this.vehicleData.driver_id === 'object') {
      this.driver_id = this.vehicleData.driver_id._id;
    } else {
      this.driver_id = this.vehicleData.driver_id;
    }
    this.vehicle_id = this.driver_id;
    let params = {
      'dispatcher_id': this.dispatcher_id,
      'vehicle_id': this.driver_id,
      'days': this.days,
      'offset': 0,
      'limit': 5,
      'sortOrder': 'desc',
      'sortByColumn': 'message_created_time',
      company_id: this.company_id_array
    }
    let enc_data2 = this._EncDecService.nwt(this.session_token, params);
    let data3 = {
      data: enc_data2,
      email: this.useremail
    }
    this._vehicleService.getVehicleListing(data3).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var vehicleListData: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.vehicleList = vehicleListData.getVehicles;
      }
    });
    this._messageService.getMessageHistory(data3).then((dec) => {
      if (dec && dec.status == 200) {
        var messageData: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.messageData = messageData.msgData;
      }
    });


  }

  SelectDispatcher() {
    let params = {
      'dispatcher_id': this.dispatcher_id,
      'vehicle_id': this.vehicle_id,
      'days': this.days,
      'offset': 0,
      'limit': 0,
      'sortOrder': 'desc',
      'sortByColumn': 'message_created_time',
      company_id: this.company_id_array
    }
    let enc_data2 = this._EncDecService.nwt(this.session_token, params);
    let data3 = {
      data: enc_data2,
      email: this.useremail
    }
    this._messageService.getMessageHistory(data3).then((dec) => {
      if (dec && dec.status == 200) {
        var messageData: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.messageData = messageData.msgData;
      }
    });
  }

  SelectVehicle() {
    let params = {
      'dispatcher_id': this.dispatcher_id,
      'vehicle_id': this.vehicle_id,
      'days': this.days,
      'offset': 0,
      'limit': 0,
      'sortOrder': 'desc',
      'sortByColumn': 'message_created_time',
      company_id: this.company_id_array
    }
    let enc_data2 = this._EncDecService.nwt(this.session_token, params);
    let data3 = {
      data: enc_data2,
      email: this.useremail
    }
    this._messageService.getMessageHistory(data3).then((dec) => {
      if (dec && dec.status == 200) {
        var messageData: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.messageData = messageData.msgData;
      }
    });
  }

  Selectdays() {
    let params = {
      'dispatcher_id': this.dispatcher_id,
      'vehicle_id': this.vehicle_id,
      'days': this.days,
      'offset': 0,
      'limit': 0,
      'sortOrder': 'desc',
      'sortByColumn': 'message_created_time',
      company_id: this.company_id_array
    }
    let enc_data2 = this._EncDecService.nwt(this.session_token, params);
    let data3 = {
      data: enc_data2,
      email: this.useremail
    }
    this._messageService.getMessageHistory(data3).then((dec) => {
      if (dec && dec.status == 200) {
        var messageData: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.messageData = messageData.msgData;
      }
    });
  }

  closePop() {
    this.dialogRef.close();
  }
}
