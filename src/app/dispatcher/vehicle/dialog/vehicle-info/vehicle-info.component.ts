import { Component, OnInit, Inject, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, throwToolbarMixedModesError } from '@angular/material';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-vehicle-info',
  templateUrl: './vehicle-info.component.html',
  styleUrls: ['./vehicle-info.component.css']
})
export class VehicleInfoComponent implements OnInit {
  public session_token;
  public useremail;
  public company_id_array = [];
  public usercompany;
  public socket_session_token;
  public vehicleData;
  public vehicle_id;
  public vehicle_detail_data;
  public vehicle_location_data;

  constructor(
    private router: Router,
    public toastr: ToastsManager,
    private _EncDecService: EncDecService,
    public VehicleDetailsService: VehicleService,
    public dialogRef: MatDialogRef<VehicleInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _JwtService: JwtService) {
    this.vehicleData = data;
  }

  ngOnInit() {
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany)
    this.vehicle_id = this.vehicleData._id ? this.vehicleData._id : this.vehicleData._id = '';
    let params = {
      vehicle_id: this.vehicle_id,
      company_id: this.company_id_array
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this.VehicleDetailsService.getvehicleinfo(data1).then((data) => {
      if(data && data.status==200){
      data = this._EncDecService.dwt(this.session_token, data.data);
      this.vehicle_detail_data = data.SingleDetail;
      this.vehicle_location_data = data.device_location;}
    });
  }

  closePop() {
    this.dialogRef.close();
  }

}
