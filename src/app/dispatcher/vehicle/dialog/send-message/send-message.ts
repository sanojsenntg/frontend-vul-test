import { Component, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent } from '@angular/material';
import { MessageService } from '../../../../common/services/message/message.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { ToastsManager } from 'ng2-toastr';
import { DriverService } from '../../../../common/services/driver/driver.service';
import * as moment from 'moment/moment';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CompanyService } from '../../../../common/services/companies/companies.service';
@Component({
  selector: 'app-send-message',
  templateUrl: 'send-message.html',
  styleUrls: ['./send-message.css']
})
export class SendMessageComponent {
  public session_token;
  public useremail;
  public company_id_array = [];
  public usercompany;
  public socket_session_token;
  public vehicleData;
  public predefineMessages;
  public showdropdown = false;
  public message;
  public driverGroupData;
  public predefined_message;
  public device_status = '';
  public validity;
  public popup;
  public enable_reply;
  public CurrentUser;
  public vehiclesLength = '';
  public validity_check;
  public disableSendmMsg = false;
  public showModel = false;
  drivers: FormControl = new FormControl();
  queryField: FormControl = new FormControl();
  public selectDriverData;
  public modelList;
  public driver_id;
  public driverFilter;
  public modelFilter: any = [];
  public selectVehicleData;
  public vehicleFilter = [];

  @ViewChild('f') form: any;
  vehicleCategoryList: any;
  vehicleCategoryFilter: any = [];
  companyData: any;
  dtc: boolean;
  companyIdFilter=[];
  constructor(
    private _companyservice:CompanyService,
    public _messageService: MessageService,
    public _JwtService: JwtService,
    public _vehicleService: VehicleService,
    public toastr: ToastsManager, vcr: ViewContainerRef,
    private _driverService: DriverService,
    private _EncDecService: EncDecService,
    public dialogRef: MatDialogRef<SendMessageComponent>,
    private _vehicleModelsService: VehicleModelsService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.vehicleData = data;
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany);
    this.companyIdFilter.push(this.usercompany)
    if(this.usercompany === '5ce12918aca1bb08d73ca25d' || this.usercompany === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    if(this.dtc)
    this.getCompanies();
    this.getDriverGroups();
    this.getVehicleCategory();
    this.CurrentUser = this._JwtService.getDispatcherUser();
    this.validity = '24';
    let params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'message',
      company_id: this.company_id_array
    };
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._messageService.preMessage(enc_data).then(dec => {
      if (dec && dec.status == 200) {
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.predefineMessages = data.predefineMessages;
      }
    });
    this.drivers.valueChanges
      .debounceTime(200)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.company_id_array
        }
        var encrypted = this._EncDecService.nwt(this.session_token, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        return this._driverService.searchForDriver(enc_data)
      }
      ).subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.selectDriverData = result.driver;
        }
      });
    this.queryField.valueChanges
      .debounceTime(200)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'vehicle_unique_id',
          'search_keyword': query,
          company_id: this.company_id_array
        }
        var encrypted = this._EncDecService.nwt(this.session_token, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        return this._vehicleService.searchVehicle(enc_data)
      }).subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.selectVehicleData = result.searchVehicle;
        }
      });
  }

  public getCompanies() {const param = {
    offset: 0,
    sortOrder: 'desc',
    sortByColumn: 'updated_at',
    company_id: this.company_id_array
  };
  var encrypted = this._EncDecService.nwt(this.session_token, param);
  var enc_data = {
    data: encrypted,
    email: this.useremail
  }
  this._companyservice.getCompanyListing(enc_data).then((dec) => {
    if (dec && dec.status == 200) {
      var data: any = this._EncDecService.dwt(this.session_token, dec.data);
      this.companyData = data.getCompanies;
    }
    //this.searchSubmit = false;
  });

  }

  OnSelectPredefine(data) {
    this.message = data.value;
    this.predefined_message = '';
  }

  clearStatus() {
    this.device_status = '';
    this.vehiclesLength = '';
  }

  onSelectStatus(data) {
    if (data.value == '8') {
      this.showModel = true;
      this.showdropdown = false;
      this.vehiclesLength = '';
    }
    else if (data.value != '7') {
      var params = {
        keyword: parseInt(data.value),
        offset: 0,
        limit: 0,
        sortOrder: 'desc',
        sortByColumn: '_id',
        company_id: this.company_id_array
      }
      var encrypted = this._EncDecService.nwt(this.session_token, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._vehicleService.getStatusBasedVehicles(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var vehicleStatusdata: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.vehiclesLength = vehicleStatusdata.count;
        }

      });
      this.showdropdown = false;
      this.showModel = false;
    }
    else if (data.value == '7') {
      this.showdropdown = true;
      this.showModel = false;
      this.vehiclesLength = '';
    }
    else {
      this.vehiclesLength = '';
      this.showdropdown = false;
      this.showModel = false;
    }
  }

  closePop() {
    this.dialogRef.close();
  }

  onSubmit() {
    if (!this.form.value.validity_check) {
      this.form.value.validity = 0;
    }
    this.disableSendmMsg = true;
    if (this.form.value.device_status) {
      let broadcast_params = {
        'keyword': this.form.value.device_status,
        'message': this.form.value.message,
        'validity': this.form.value.validity,
        'popup': this.form.value.popup ? this.form.value.popup : false,
        'send_by': this.CurrentUser._id,
        'enable_reply': this.form.value.enable_reply ? this.form.value.enable_reply : false,
        'sender_role': 'dispatcher',
        'receiver_role': 'driver',
        'message_type': 'broadcast',
        'driver_groups': this.form.value.driver_groups ? this.form.value.driver_groups : '',
        'driver_id': this.driver_id ? [this.driver_id] : [],
        'vehicle_model_id': this.modelFilter ? this.modelFilter.map(x => x._id) : [],
        'vehicle_identity_number': this.vehicleFilter ? this.vehicleFilter : [],
        'vehicle_category_id': this.vehicleCategoryFilter ? this.vehicleCategoryFilter.map(x => x._id) : [],
        company_id: this.company_id_array,
        company_ids: this.companyIdFilter.length>0?this.companyIdFilter:this.company_id_array
      };
      var encrypted = this._EncDecService.nwt(this.session_token, broadcast_params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._messageService.sendBroadcastMessage(enc_data).then(data => {
        if (data) {
          if (data.status == 200) {
            this.toastr.success('Message send successfully.', 'Success!');
          } else {
            console.log(data)
            this.toastr.warning('No Driver found.', 'warning!');
          }
          setTimeout(() => {
            this.dialogRef.close();
          }, 1000);
        }
      });
    } else {
      let params = {
        'message': this.form.value.message,
        'validity': this.form.value.validity,
        'popup': this.form.value.popup ? this.form.value.popup : false,
        'send_by': this.CurrentUser._id,
        'send_to': (typeof (this.vehicleData.driver_id) == 'object') ? this.vehicleData.driver_id._id : this.vehicleData.driver_id,
        'enable_reply': this.form.value.enable_reply ? this.form.value.enable_reply : false,
        'sender_role': 'dispatcher',
        'receiver_role': 'driver',
        'message_type': 'single',
        company_id: this.company_id_array
      };
      var encrypted = this._EncDecService.nwt(this.session_token, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._messageService.sendMessage(enc_data).then(dec => {
        if (dec) {
          this.disableSendmMsg = false;
          this.toastr.success('Message send successfully.');
          setTimeout(() => {
            this.dialogRef.close();
          }, 1000);
        }
      });
    }
  }

  /**
  * Function To Get Driver Groups For Dropdown
  *
  */
  public getDriverGroups() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      company_id: this.company_id_array
    };
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._driverService.getDriversGroup(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200)
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
      this.driverGroupData = data.getDriverGroups;
    });
  }
  public setModelFilter(event) {
    this.driverFilter = '';
    this.vehicleFilter = [];
    this.driver_id = '';
    this.vehicleCategoryFilter = [];
    if (typeof event === 'object') {
      if (this.modelFilter.findIndex(x => x._id === event._id) == -1)
        this.modelFilter.push(event)
    } else if (typeof event === 'string') {
      const params = {
        offset: 0,
        limit: 10,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        search: event,
        company_id: this.company_id_array
      };
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleModelsService.getVehicleModels(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.modelList = data.vehicleModelsList;
        }
      });
    }
  }
  public setDriverFilter(event) {
    this.modelFilter = [];
    this.vehicleFilter = [];
    if (typeof event === 'object') {
      this.driver_id = event._id;
    }
    this.vehicleCategoryFilter = [];
  }
  public setVehicleCategoryFilter(event) {
    this.modelFilter = [];
    this.vehicleFilter = [];
    this.driver_id = '';
    if (typeof event == 'object')
      if (this.vehicleCategoryFilter.findIndex(x => x._id == event._id) == -1)
        this.vehicleCategoryFilter.push(event);
    console.log(this.vehicleCategoryFilter)
  }
  displayFnDriver(data): string {
    return data ? data.display_name : data;
  }
  displayFnModel(data): string {
    return '';
  }
  displayFnside(data): string {
    return data ? '' : '';
  }
  vehicleSelected(event: MatAutocompleteSelectedEvent): void {
    console.log(event)
    if (this.vehicleFilter.indexOf(event.option.value.vehicle_identity_number) > -1) {
      return;
    } else {
      this.vehicleFilter.push(event.option.value.vehicle_identity_number);
      this.queryField.setValue(null);
    }
  }
  removeVehicle(fruit): void {
    const index = this.vehicleFilter.indexOf(fruit);
    if (index >= 0) {
      this.vehicleFilter.splice(index, 1);
    }
  }
  getVehicleCategory() {
    const params = {
      offset: 0,
      limit: 0,
      company_id: this.company_id_array
    };
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        this.vehicleCategoryList = data.vehicleModelsList;
      }
    });
  }

  removeMultiSelect(data, id) {
    let index;
    switch (id) {
      case 1:
        // index = this.multiFilter.driver.indexOf(data);
        // this.multiFilter.driver.splice(index, 1);
        break;
      case 2:
        index = this.modelFilter.indexOf(data);
        this.modelFilter.splice(index, 1);
        break;
      case 3:
        index = this.vehicleCategoryFilter.indexOf(data);
        this.vehicleCategoryFilter.splice(index, 1);
        break;
      case 4:
        // index = this.multiFilter.pickup.indexOf(data);
        // this.multiFilter.pickup.splice(index, 1);
        break;
      case 5:
        // index = this.multiFilter.drop.indexOf(data);
        // this.multiFilter.drop.splice(index, 1);
        break;
      case 6:
        // index = this.multiFilter.driver_group.indexOf(data);
        // this.multiFilter.driver_group.splice(index, 1);
        break;
    }
  }
}
