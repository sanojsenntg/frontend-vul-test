import { Component, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ShiftsDetailsService } from '../../../../common/services/shift_details/shift_details.service';
import { ShiftsService } from '../../../../common/services/shifts/shifts.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-shift-details',
  templateUrl: 'shift-details.html',
  styleUrls: ['./shift-details.css']
})
export class ShiftDetailsComponent {
  public session_token;
  public useremail;
  public company_id_array = [];
  public usercompany;
  public socket_session_token;
  public vehicleData;
  //public driver_id;
  public vehicle_id;
  public currentPage;
  public pageSize = 5;
  public pageNo = 1;
  public shift;
  public Shiftlength;
  constructor(
    private router: Router,
    public toastr: ToastsManager,
    public shiftsDetailsService: ShiftsDetailsService,
    public shiftsService: ShiftsService,
    public dialogRef: MatDialogRef<ShiftDetailsComponent>,
    private _EncDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _JwtService: JwtService) {
    this.vehicleData = data;
  }

  ngOnInit() {
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany)
    // this.driver_id = this.vehicleData.driver_id._id ? this.vehicleData.driver_id._id : this.vehicleData.drivers['0']._id;
    // console.log('sd>>', this.driver_id);
    this.vehicle_id = this.vehicleData._id ? this.vehicleData._id : this.vehicleData._id = '';
    let params = {
      //driver_id : this.driver_id,
      vehicle_id: this.vehicle_id,
      offset: 0,
      limit: 1,
      sortByColumn: '_id',
      sortOrder: 'desc',
      company_id: this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this.shiftsService.getShiftData(data1).then((dec) => {
      if (dec && dec.status == 200) {
        var data:any = this._EncDecService.dwt(this.session_token, dec.data);
        this.shift = data.getShifts;
      }
    });
  }

  closePop() {
    this.dialogRef.close();
  }
}
