import { Component, AfterViewInit, OnInit, ViewEncapsulation } from '@angular/core';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import { Router } from '@angular/router';
import { VehicleService } from '../../common/services/vehicles/vehicle.service';
import { SendMessageComponent } from './dialog/send-message/send-message';
import { MessageHistoryComponent } from './dialog/message-history/message-history';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { OrderInfoComponent } from './dialog/order-info/order-info';
import { ShiftDetailsComponent } from './dialog/shift-details/shift-details';
import { EncDecService } from '../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { VehicleInfoComponent } from './dialog/vehicle-info/vehicle-info.component';
import { DriverService } from '../../common/services/driver/driver.service';
import { VehicleModelsService } from '../../common/services/vehiclemodels/vehiclemodels.service';
import { DeviceService } from '../../common/services/devices/devices.service';
import { FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { UserIdleService } from 'angular-user-idle';
import { JwtService } from '../../common/services/api/jwt.service';
import * as moment from 'moment/moment';
import { Socket } from 'ng-socket-io';
import { CompanyService } from '../../common/services/companies/companies.service';
import { AccessControlService } from "../../common/services/access-control/access-control.service";
import { Angular2Csv } from 'angular2-csv';
@Component({
  selector: 'app-dispatcher-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VehicleComponent implements OnInit {
  public session_token;
  public useremail;
  public company_id_array = [];
  public usercompany;
  public socket_session_token;
  public subscripttimeout;
  public subscripttimestart;
  public vehicles: any = '';
  public selectedItem: 'itemA';
  public vehicleInMaintenace: any = '';
  public vehicleBooked: any = '';
  public vehicleOffline: any = '';
  public vehicleLogged: any = '';
  public vehiclePaused: any = '';
  public vehicleFree: any = '';
  public inactivelist;
  public Inactive;
  public showinactive = false;
  public modelopened = false;
  public vehicleData = [];
  public test: any;
  indexStart = 0;
  indexEnd = 10;
  pageSize = 10;
  pageNo = 0;
  totalPage = [1];
  public keyword = 0;
  vehiclesLength;
  searchDataVehicles = [];
  public selectedItemA;
  public selectedItemB;
  public selectedItemC;
  public selectedItemD;
  public selectedItemE;
  public selectedItemF;
  public selectedItemG;
  public selectedItemH;
  public selectedItemI = '';
  public searchSubmit = false;
  public driver_id = '';
  public additional_service_id = '';
  public limit;
  public searchLoader = false;
  public selectVehicleData;
  public selectDriverData;
  public vehicleList: any = [];
  public modelList: any = [];
  public driverList: any = [];
  public deviceSearch;
  public status;
  public sort_type = 'Desc';
  public sort_by_column = '_id';
  public pNo = 1;
  public pNoM = 1;
  unique_device_id;
  public loadingIndicator = '';
  devices: FormControl = new FormControl();
  modelFilter;
  driverFilter;
  drivers: FormControl = new FormControl();
  vehicleFilter;
  vehicleFilter1;
  vehicleFilter2;
  queryField: FormControl = new FormControl();
  queryField2: FormControl = new FormControl();
  queryField3: FormControl = new FormControl();

  public dataRefresher: any;
  public vehicleBlocked: any = '';
  company_id_list: any = [];
  companyData: any;
  dtc: boolean = false; companyIdFilter: any;
  downloads: boolean;
  createCsvFlag: boolean;
  maintenanceInfo: any=[];
  maintenanceInfoLength: any;
  maintenanceInfoVehicle: any;
  ;
  constructor(private router: Router,
    public dialog: MatDialog,
    private _vehicleService: VehicleService,
    private _vehicleModelsService: VehicleModelsService,
    private _devicesService: DeviceService,
    private _driverService: DriverService,
    private _EncDecService: EncDecService,
    private userIdle: UserIdleService,
    public toastr: ToastsManager,
    private _jwtService: JwtService,
    private socket: Socket,
    public _companyservice: CompanyService,
    public _aclService: AccessControlService
  ) {
    this.keyword = 0;
    this.userIdle.startWatching();
    this.socket.removeAllListeners("devicelocations_updates");
    // Start watching when user idle is starting.
    this.subscripttimestart = this.userIdle.onTimerStart().subscribe(count => console.log(count));

    // Start watch when time is up.
    this.subscripttimeout = this.userIdle.onTimeout().subscribe(() => {
      this.userIdle.stopWatching();
      this._jwtService.destroyDispatcherToken();
      clearInterval(this.dataRefresher);
      this.router.navigate(['/dispatcher/login']);
    });

  }

  // set received message callback

  ngOnInit() {
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany)
    this.company_id_list.push(this.usercompany)
    if (this.usercompany === '5ce12918aca1bb08d73ca25d' || this.usercompany === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.aclDisplayService();
    this.getCompanies();
    // this.queryField.valueChanges.subscribe(data => {
    //   this.loadingIndicator = 'setVehicleFilter';
    // })
    this.queryField.valueChanges
      .debounceTime(200)
      .distinctUntilChanged()
      .subscribe((query) => {
        let params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'vehicle_unique_id',
          'search_keyword': query,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, params);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._vehicleService.searchVehicle(data1).then(result => {
          if (result && result.status == 200) {
            result = this._EncDecService.dwt(this.session_token, result.data);
            this.selectVehicleData = result.searchVehicle;
            this.loadingIndicator = '';
          }
        });
      })

    this.drivers.valueChanges.subscribe(data => {
      this.loadingIndicator = 'setDriverFilter';
    })
    this.drivers.valueChanges
      .debounceTime(200)
      .distinctUntilChanged()
      .subscribe((query) => {
        let param = {
          limit: 10,
          'search_keyword': query,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, param);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._driverService.searchForDriver(data1)
          .subscribe(result => {
            if (result && result.status == 200) {
              result = this._EncDecService.dwt(this.session_token, result.data);
              this.selectDriverData = result.driver;
              this.loadingIndicator = '';
            }
          })
      })

    this.devices.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDevice';
    })
    this.devices.valueChanges
      .debounceTime(200)
      .distinctUntilChanged()
      .subscribe((query) => {
        let param = {
          offset: 0,
          limit: 10,
          sortOrder: 'asc',
          sortByColumn: 'unique_device_id',
          search_keyword: query,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, param);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._devicesService.getDevicesListing(data1).then(result => {
          if (result && result.status == 200) {
            result = this._EncDecService.dwt(this.session_token, result.data);
            this.deviceSearch = result.devices;
            this.loadingIndicator = '';
          }
        })
      });
    // this.queryField2.valueChanges.subscribe(data => {
    //   this.loadingIndicator = 'setVehiclePlate';
    // })
    this.queryField2.valueChanges
      .debounceTime(200)
      .distinctUntilChanged()
      .subscribe((query) => {
        let param = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'vehicle_unique_id',
          'search_keyword': query,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, param);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._vehicleService.searchVehicle(data1).then(result => {
          if (result && result.status == 200) {
            result = this._EncDecService.dwt(this.session_token, result.data);
            this.selectVehicleData = result.searchVehicle;
          }
          this.loadingIndicator = '';
        })
      })

    // this.queryField3.valueChanges.subscribe(data => {
    //   this.loadingIndicator = 'setVehicleSideNo';
    // })
    this.queryField3.valueChanges
      .debounceTime(200)
      .distinctUntilChanged()
      .subscribe((query) => {
        let param = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'vehicle_unique_id',
          'search_keyword': query,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
        }
        let enc_data = this._EncDecService.nwt(this.session_token, param);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: this.useremail
        }
        this._vehicleService.searchVehicle(data1).then(result => {
          if (result && result.status == 200) {
            result = this._EncDecService.dwt(this.session_token, result.data);
            this.selectVehicleData = result.searchVehicle;
          }
          this.loadingIndicator = '';
        })
      })
    document.addEventListener("click", this.restart.bind(this));
    document.addEventListener("mousemove", this.restart.bind(this));
    document.addEventListener("mousedown", this.restart.bind(this));
    document.addEventListener("keypress", this.restart.bind(this));
    document.addEventListener("DOMMouseScroll", this.restart.bind(this));
    document.addEventListener("mousewheel", this.restart.bind(this));
    document.addEventListener("touchmove", this.restart.bind(this));
    document.addEventListener("MSPointerMove", this.restart.bind(this));
    this.loadingIndicator = '';
  }

  ngOnDestroy() {
    this.subscripttimestart.unsubscribe();
    this.subscripttimeout.unsubscribe();
    if (this.dataRefresher) {
      clearInterval(this.dataRefresher);
    }
  }
  restart() {
    this.userIdle.resetTimer();
  }
  public getVehiclesForInput() {
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._vehicleService.getAllVehicleListing(data1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.selectVehicleData = data.getAllVehicle;
      }
    });
    this._driverService.searchForDriver(data1).subscribe((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.selectDriverData = data.driver;
      }
    });
    this._vehicleModelsService.getVehicleModels(data1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.modelList = data.vehicleModelsList;
      }
    });
  }

  /**
   * Autocomplete search for vehicles.
   * @param data
   */
  public setVehicleFilter(event, data) {
    this.unique_device_id = '';
    this.modelFilter = '';
    this.driverFilter = '';
    this.loadingIndicator = data;
    if (typeof event === 'object') {
      this.filter(event.display_name, 'name');
    }
  }
  public setDriverFilter(event) {
    this.unique_device_id = '';
    this.modelFilter = '';
    this.vehicleFilter = '';
    this.vehicleFilter1 = '';
    this.vehicleFilter2 = '';
    if (typeof event === 'object') {
      this.filter(event._id, 'driver');
    }
  }
  public setModelFilter(event) {
    this.loadingIndicator = 'setModelFilter'
    this.unique_device_id = '';
    this.driverFilter = '';
    this.vehicleFilter = '';
    this.vehicleFilter1 = '';
    this.vehicleFilter2 = '';
    if (typeof event === 'object') {
      this.filter(event._id, 'model');
    } else if (typeof event === 'string') {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      };
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail

      }
      this._vehicleModelsService.getVehicleModels(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.modelList = data.vehicleModelsList;
          //alert(this.selectDriverData.length)}
        }
        this.loadingIndicator = '';
      });
    }
  }
  searchDevice(data) {
    this.modelFilter = '';
    this.driverFilter = '';
    this.vehicleFilter = '';
    this.vehicleFilter1 = '';
    this.vehicleFilter2 = '';
    if (typeof data === 'object') {
      this.filter(data._id, 'device');
    }
  }
  displayFnDriver(data): string {
    return data ? data.display_name : data;
  }
  displayFnModel(data): string {
    return data ? data.name : data;
  }
  displayFnvehicle(data): string {
    return data ? data.vehicle_unique_id + ' ' + data.display_name : data;
  }
  displayFnDevice(data): string {
    return data ? data.unique_device_id : data;
  }
  displayFnplate(data): string {
    return data ? data.plate_number : data;
  }
  displayFnside(data): string {
    return data ? data.vehicle_identity_number : data;
  }
  public getSelectedVehicles(status, item) {
    this.getVehiclesForDiffStatus();
    this.reset2();
    this.pNo = 1;
    this.showinactive = false;
    this.status = status;
    this.searchSubmit = true;
    if (item == 'itemA') {
      this.selectedItemA = 'active';
      this.selectedItemB = '';
      this.selectedItemC = '';
      this.selectedItemD = '';
      this.selectedItemE = '';
      this.selectedItemF = '';
      this.selectedItemG = '';
      this.selectedItemH = '';

    } else if (item == 'itemB') {
      this.selectedItemB = 'active';
      this.selectedItemA = '';
      this.selectedItemC = '';
      this.selectedItemD = '';
      this.selectedItemE = '';
      this.selectedItemF = '';
      this.selectedItemG = '';
      this.selectedItemH = '';

    } else if (item == 'itemC') {
      this.selectedItemC = 'active';
      this.selectedItemA = '';
      this.selectedItemB = '';
      this.selectedItemD = '';
      this.selectedItemE = '';
      this.selectedItemF = '';
      this.selectedItemG = '';
      this.selectedItemH = '';

    } else if (item == 'itemD') {
      this.selectedItemD = 'active';
      this.selectedItemA = '';
      this.selectedItemB = '';
      this.selectedItemC = '';
      this.selectedItemE = '';
      this.selectedItemF = '';
      this.selectedItemG = '';
      this.selectedItemH = '';

    } else if (item == 'itemE') {
      this.selectedItemE = 'active';
      this.selectedItemA = '';
      this.selectedItemB = '';
      this.selectedItemC = '';
      this.selectedItemD = '';
      this.selectedItemF = '';
      this.selectedItemG = '';
      this.selectedItemH = '';

    } else if (item == 'itemF') {
      this.selectedItemF = 'active';
      this.selectedItemA = '';
      this.selectedItemB = '';
      this.selectedItemC = '';
      this.selectedItemD = '';
      this.selectedItemE = '';
      this.selectedItemG = '';
      this.selectedItemH = '';

    } else if (item == 'itemH') {
      this.selectedItemF = 'active';
      this.selectedItemA = '';
      this.selectedItemB = '';
      this.selectedItemC = '';
      this.selectedItemD = '';
      this.selectedItemE = '';
      this.selectedItemG = '';
      this.selectedItemH = '';

    } else {
      this.selectedItemG = 'active';
      this.selectedItemA = '';
      this.selectedItemB = '';
      this.selectedItemC = '';
      this.selectedItemD = '';
      this.selectedItemE = '';
      this.selectedItemF = '';
      this.selectedItemH = '';

    }
    this.selectedItem = item;
    if (status === 1) {
      this.vehicleData = [];
      this.searchDataVehicles = [];
      this.keyword = 1;
      this.params = {
        keyword: this.keyword,
        offset: 0,
        limit: this.pageSize,
        sortOrder: this.sort_type,
        sortByColumn: this.sort_by_column,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }

      let enc_data = this._EncDecService.nwt(this.session_token, this.params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          this.vehiclesLength = data.count;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            let rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
        //console.log(JSON.stringify(this.searchDataVehicles));
      });
    } else if (status === 2) {
      this.vehicleData = [];
      this.searchDataVehicles = [];
      this.keyword = 2;
      this.params = {
        keyword: this.keyword,
        offset: 0,
        limit: this.pageSize,
        sortOrder: this.sort_type,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }

      let enc_data = this._EncDecService.nwt(this.session_token, this.params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          this.vehiclesLength = data.count;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            let rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
      });
    } else if (status === 3) {
      this.vehicleData = [];
      this.searchDataVehicles = [];
      this.keyword = 3;
      this.params = {
        keyword: this.keyword,
        offset: 0,
        limit: this.pageSize,
        sortOrder: this.sort_type,
        sortByColumn: this.sort_by_column,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }

      let enc_data = this._EncDecService.nwt(this.session_token, this.params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          this.vehiclesLength = data.count;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            let rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
        //(JSON.stringify(this.searchDataVehicles));
      });
    } else if (status === 4) {
      this.vehicleData = [];
      this.searchDataVehicles = [];
      this.keyword = 4;
      this.params = {
        keyword: this.keyword,
        offset: 0,
        limit: this.pageSize,
        sortOrder: this.sort_type,
        sortByColumn: this.sort_by_column,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }

      let enc_data = this._EncDecService.nwt(this.session_token, this.params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          this.vehiclesLength = data.count;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            let rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
      });
    } else if (status === 5) {
      this.vehicleData = [];
      this.searchDataVehicles = [];
      this.keyword = 5;
      this.params = {
        keyword: this.keyword,
        offset: 0,
        limit: this.pageSize,
        sortOrder: this.sort_type,
        sortByColumn: this.sort_by_column,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }

      let enc_data = this._EncDecService.nwt(this.session_token, this.params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          this.vehiclesLength = data.count;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            let rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
      });
    } else if (status === 6) {
      this.vehicleData = [];
      this.searchDataVehicles = [];
      this.keyword = 6;
      this.params = {
        keyword: this.keyword,
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: '_id',
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }

      let enc_data = this._EncDecService.nwt(this.session_token, this.params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          this.vehiclesLength = data.count;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            let rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
      });
    } else if (status === 7) {
      this.vehicleData = [];
      this.searchDataVehicles = [];
      this.keyword = 7;
      this.params = {
        keyword: this.keyword,
        offset: 0,
        limit: this.pageSize,
        sortOrder: this.sort_type,
        sortByColumn: this.sort_by_column,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }

      let enc_data = this._EncDecService.nwt(this.session_token, this.params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          this.vehiclesLength = data.count;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            let rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
      });
    } else if (status === 8) {
      this.vehicleData = [];
      this.searchDataVehicles = [];
      this.keyword = 8;
      this.showinactive = true;
      this.params = {
        keyword: this.keyword,
        offset: 0,
        limit: this.pageSize,
        sortOrder: this.sort_type,
        sortByColumn: this.sort_by_column,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }

      let enc_data = this._EncDecService.nwt(this.session_token, this.params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          this.vehiclesLength = data.count;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            let rowNum = i;
            this.searchDataVehicles.push({ arr, "row": rowNum });
          }
          //console.log(JSON.stringify(this.searchDataVehicles));
          //console.log(this.keyword);
        }
        this.searchSubmit = false;
      });
    } else {
      this.vehicleData = [];
      this.searchDataVehicles = [];
      this.keyword = 0;
      this.params = {
        keyword: this.keyword,
        offset: 0,
        limit: this.pageSize,
        sortOrder: this.sort_type,
        sortByColumn: this.sort_by_column,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }

      let enc_data = this._EncDecService.nwt(this.session_token, this.params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          this.vehiclesLength = data.count;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            let rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
      });
      //console.log(this.searchDataVehicles)
    }
  }
  pagingAgent(data, key) {
    this.searchSubmit = true;
    this.vehicleData = [];
    this.searchDataVehicles = [];
    this.keyword = key;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    this.params['offset'] = this.pageNo;
    this.params['company_id'] = this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array;
    let enc_data = this._EncDecService.nwt(this.session_token, this.params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    if (key == 8) {
      this._vehicleService.getInActiveVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            const rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
      });
    } else {
      this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
        if (data && data.status == 200) {
          data = this._EncDecService.dwt(this.session_token, data.data);
          this.vehicleData = data.vehicles;
          for (let i = 0; i < this.vehicleData.length; i++) {
            let arr = [];
            arr = this.vehicleData[i];
            const rowNum = i;
            /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
              let params = {
                vehicle_id : this.vehicleData[i]._id,
              }
              this._vehicleService.getvehicleinfo(params).then((Data) => {
               let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
                this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
              });
              }
              else{             
                this.searchDataVehicles.push({ arr, 'row': rowNum });
              }*/
            this.searchDataVehicles.push({ arr, 'row': rowNum });
          }
        }
        this.searchSubmit = false;
      });
    }
  }
  public getVehiclesForDiffStatus() {
    let params1 = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data_1 = this._EncDecService.nwt(this.session_token, params1);
    //alert(this.dispatcher_id.email)
    let data_to_send = {
      data: enc_data_1,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data_to_send).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.vehicles = data.count;
      }
    });
    let param2 = {
      keyword: 6,
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data_2 = this._EncDecService.nwt(this.session_token, param2);
    //alert(this.dispatcher_id.email)
    let data_to_send1 = {
      data: enc_data_2,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data_to_send1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.vehicleBooked = data.count;
      }
    });
    let param3 = {
      keyword: 1,
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data_3 = this._EncDecService.nwt(this.session_token, param3);
    //alert(this.dispatcher_id.email)
    let data_to_send2 = {
      data: enc_data_3,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data_to_send2).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.vehicleOffline = data.count;
      }
    });
    let param4 = {
      keyword: 3,
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data_4 = this._EncDecService.nwt(this.session_token, param4);
    //alert(this.dispatcher_id.email)
    let data_to_send4 = {
      data: enc_data_4,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data_to_send4).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.vehicleLogged = data.count;
      }
    });
    let param5 = {
      keyword: 2,
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data_5 = this._EncDecService.nwt(this.session_token, param5);
    //alert(this.dispatcher_id.email)
    let data_to_send5 = {
      data: enc_data_5,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data_to_send5).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.vehiclePaused = data.count;
      }
    });
    let param6 = {
      keyword: 4,
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data_6 = this._EncDecService.nwt(this.session_token, param6);
    //alert(this.dispatcher_id.email)
    let data_to_send6 = {
      data: enc_data_6,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data_to_send6).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.vehicleInMaintenace = data.count;
      }
    });
    let param7 = {
      keyword: 5,
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data_7 = this._EncDecService.nwt(this.session_token, param7);
    //alert(this.dispatcher_id.email)
    let data_to_send7 = {
      data: enc_data_7,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data_to_send7).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.vehicleFree = data.count;
      }
    });
    let param8 = {
      keyword: 7,
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data_8 = this._EncDecService.nwt(this.session_token, param8);
    //alert(this.dispatcher_id.email)
    let data_to_send_8 = {
      data: enc_data_8,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data_to_send_8).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.vehicleBlocked = data.count;
      }
    });
    let param9 = {
      keyword: 8,
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    }
    let enc_data_9 = this._EncDecService.nwt(this.session_token, param9);
    //alert(this.dispatcher_id.email)
    let data_to_send9 = {
      data: enc_data_9,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data_to_send9).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.Inactive = data.count;
        this.inactivelist = data.vehicles;
      }
    });
  }

  /**
   * Show Message Popup
  */

  SendMessageDialog(vehicleData): void {
    let dialogRef = this.dialog.open(SendMessageComponent, {
      width: '700px',
      hasBackdrop: true,
      data: vehicleData
    });
  }

  ShowMessageHistory(vehicleData): void {
    let dialogRef = this.dialog.open(MessageHistoryComponent, {
      width: '800px',
      height: '450px',
      hasBackdrop: true,
      data: vehicleData
    });
    dialogRef.afterClosed().subscribe(result => { });
  }

  OrderInfo(vehicleData) {
    this.dialog.closeAll();
    let dialogRef = this.dialog.open(OrderInfoComponent, {
      width: '800px',
      height: '450px',
      hasBackdrop: true,
      data: vehicleData
    });
    dialogRef.afterClosed().subscribe(result => {

    });
  }

  ShiftDetails(vehicleData) {
    this.dialog.closeAll();
    let dialogRef = this.dialog.open(ShiftDetailsComponent, {
      width: '800px',
      height: '450px',
      hasBackdrop: true,
      data: vehicleData
    });
    dialogRef.afterClosed().subscribe(result => { });
  }

  vehicleDetails(vehicleData) {

    this.dialog.closeAll();
    let dialogRef = this.dialog.open(VehicleInfoComponent, {
      width: '800px',
      height: '450px',
      hasBackdrop: true,
      data: vehicleData
    });
    dialogRef.afterClosed().subscribe(result => { });
  }
  sortColumn(event) {
    this.sort_by_column = event;
    if (this.modelFilter == '')
      this.getSelectedVehicles(this.status, 'itemA');
    else {
      this.filter(this.modelFilter, 'model');
    }
  }
  sortOrder(event) {
    this.sort_type = event;
    if (this.modelFilter == '')
      this.getSelectedVehicles(this.status, 'itemA');
    else {
      this.filter(this.modelFilter, 'model');
    }
  }
  sortPageSize(event) {
    if (this.modelFilter == '') {
      this.limit = event;
      this.getSelectedVehicles(this.status, 'itemA');
    }
    else {
      this.filter(this.modelFilter, 'model');
    }
  }
  public params;
  public filter(param, type) {
    this.loadingIndicator = '';
    this.pNo = 1;
    this.searchSubmit = true;
    this.params = {}
    this.searchLoader = true;
    switch (type) {
      case 'name':
        this.params = {
          keyword: this.keyword,
          offset: 0,
          limit: this.pageSize,
          sortOrder: this.sort_type,
          sortByColumn: this.sort_by_column,
          vehiclename: param
        }
        //this.searchDataVehicles = data.filter(x => x.vehicles.display_name === param); 
        break;
      case 'driver':
        this.params = {
          keyword: this.keyword,
          offset: 0,
          limit: this.pageSize,
          sortOrder: this.sort_type,
          sortByColumn: this.sort_by_column,
          driver: param
        }
        break;
      //this.searchDataVehicles = this.searchDataVehicles.filter(x => x.arr.driver_id.username === param); 
      case 'model':
        //this.searchDataVehicles = this.searchDataVehicles.filter(x => x.arr.vehicle_model_id.name === param); 
        this.params = {
          keyword: this.keyword,
          offset: 0,
          limit: this.pageSize,
          sortOrder: this.sort_type,
          sortByColumn: this.sort_by_column,
          model: param
        }
        break;
      case 'device':
        //this.searchDataVehicles = this.searchDataVehicles.filter(x => x.arr.vehicle_model_id.name === param); 
        this.params = {
          keyword: this.keyword,
          offset: 0,
          limit: this.pageSize,
          sortOrder: this.sort_type,
          sortByColumn: this.sort_by_column,
          device: param
        }
        break;
      default:
        break;
    }
    this.params['company_id'] = this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array;
    let enc_data = this._EncDecService.nwt(this.session_token, this.params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(data1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.searchDataVehicles = [];
        this.vehicleData = data.vehicles;
        if (data.count)
          this.vehiclesLength = data.count;
        else
          this.vehiclesLength = 0;
        for (let i = 0; i < this.vehicleData.length; i++) {
          let arr = [];
          arr = this.vehicleData[i];
          const rowNum = i;
          /*if(this.vehicleData[i].driver_id  && this.vehicleData[i].driver_id.device_id){
            let params = {
              vehicle_id : this.vehicleData[i]._id,
            }
            this._vehicleService.getvehicleinfo(params).then((Data) => {
             let deviceData=({"battery": Data.device_location.battery,"device_id": Data.SingleDetail.device_id,"gps_strength":Data.device_location.gps_strength})
              this.searchDataVehicles.push({ arr,deviceData , 'row': rowNum });
            });
            }
            else{             
              this.searchDataVehicles.push({ arr, 'row': rowNum });
            }*/
          this.searchDataVehicles.push({ arr, 'row': rowNum });
        }
      }
      this.searchSubmit = false;
    });

  }
  iconColor(num) {
    if (num > 50)
      return 'green';
    else if (num > 0)
      return 'orange'
    else
      return 'red';
  }
  reset() {
    this.unique_device_id = '';
    this.modelFilter = '';
    this.driverFilter = '';
    this.vehicleFilter = '';
    this.vehicleFilter1 = '';
    this.vehicleFilter2 = '';
    if (this.dtc) {
      this.company_id_list = this.companyData.slice().map(x => x._id);
      this.companyIdFilter = this.company_id_list.slice()
      this.companyIdFilter.push('all');
    }
    this.getSelectedVehicles(null, 'itemA')
  }
  reset2() {
    this.unique_device_id = '';
    this.modelFilter = '';
    this.driverFilter = '';
    this.vehicleFilter = '';
    this.vehicleFilter1 = '';
    this.vehicleFilter2 = '';
    return;
  }
  public deviceinfo;
  public shiftinfo;
  public showModel(vehicle) {
    this.loadingIndicator = 'modalLoader';
    var params = {
      vehicle_id: vehicle._id,
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    //alert(this.dispatcher_id.email)
    let data1 = {
      data: enc_data,
      email: this.useremail
    }
    this._vehicleService.getVehicleHistory(data1).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.loadingIndicator = '';
        if (data.devices.length == 0) {
          this.toastr.error('No device Assigned to Vehicle');
        } else {
          this.modelopened = true;
          this.deviceinfo = data.devices[0];
          //console.log(JSON.stringify(this.deviceinfo));
          this.shiftinfo = data.shiftinfo;
          //console.log("shift++++++++++++++++" + JSON.stringify(this.shiftinfo))
        }
      }
    })
  }

  public icons;
  mouseEnter(data) {
    if (data.driver_id && data.driver_id.device_id) {
      let params = {
        vehicle_id: data._id,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
      }
      let enc_data = this._EncDecService.nwt(this.session_token, params);
      //alert(this.dispatcher_id.email)
      let data1 = {
        data: enc_data,
        email: this.useremail
      }
      this._vehicleService.getvehicleinfo(data1).then((Data) => {
        if (Data && Data.status == 200) {
          Data = this._EncDecService.dwt(this.session_token, Data.data);
          this.icons = ({ "battery": Data.device_location.battery, "device_id": Data.SingleDetail.device_id, "gps_strength": Data.device_location.gps_strength, "company": Data.SingleDetail.device_id.company, "manufacturer": Data.SingleDetail.device_id.manufacturer })
        }
      });
    }
    else {
      this.icons = [];
    }
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.company_id_array
    };
    var encrypted = this._EncDecService.nwt(this.session_token, param);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        //this.CompanyLength = data.count;
        this.companyData = data.getCompanies;
        if (this.dtc && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
          this.company_id_list = data.getCompanies.map(x => x._id);
          this.companyIdFilter = data.getCompanies.map(x => x._id)
          this.companyIdFilter.push('all')
        }
        this.getSelectedVehicles(null, 'itemA');
        this.getVehiclesForInput();
      }
      //this.searchSubmit = false;
    });
  }

  selectAllCompany() {
    if (this.company_id_list.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.company_id_list = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.company_id_list = [];
      this.companyIdFilter = [];
    }
  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.company_id_list = this.companyIdFilter.slice()
    }
    else {
      this.company_id_list = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }

  public aclDisplayService() {
    var params = {
      company_id: this.company_id_array
    }
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this._EncDecService.dwt(this.session_token, dec.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == 'Downloads - Fleet Management') {
            this.downloads = true;
          }
        };
      }
    })
  }
  public createCsv() {
    if (this.createCsvFlag) {
      this.toastr.warning('CSV creation already started');
      return;
    }
    this.createCsvFlag = true;
    let params = this.params;
    // params.limit = 0;
    // params.offset = 0;
    params.limit=this.vehiclesLength;
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._vehicleService.getStatusBasedVehicles(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this._EncDecService.dwt(this.session_token, dec.data);
        let labels = [
          'Registration No',
          'License No',
          'Display Name',
          'Vehicle Model',
          'Insurance Expiry Date',
          'Plate Number',
          'Plate Type',
          'Vehicle Identity Number',
          'Register With WASL',
          'Send To RTA',
          'Maintenance Status',
          'Created At',
          'Credit Card Machine',
          'Vehicle status'
        ];
        let resArray = [];
        for (let i = 0; i < res.vehicles.length; i++) {
          const csvArray = res.vehicles[i];
          resArray.push
            ({
              registration_number: csvArray.registration_number ? csvArray.registration_number : 'N/A',
              license_number: csvArray.license_number ? csvArray.license_number : 'N/A',
              display_name: csvArray.display_name ? csvArray.display_name : 'N/A',
              vehicle_model: csvArray.vehicle_model && csvArray.vehicle_model.name ? csvArray.vehicle_model.name : 'N/A',
              insuarance_expiration_date: csvArray.insuarance_expiration_date ? csvArray.insuarance_expiration_date : 'N/A ',
              plate_number: csvArray.plate_number ? csvArray.plate_number : 'N/A',
              plate_type: csvArray.plate_type ? csvArray.plate_type : 'N/A',
              vehicle_identity_number: csvArray.vehicle_identity_number ? csvArray.vehicle_identity_number : 'N/A',
              register_with_wasl: csvArray.register_with_wasl ? csvArray.register_with_wasl : 'N/A',
              send_to_rta: csvArray.send_to_rta ? csvArray.send_to_rta : 'N/A',
              maintenance_status: csvArray.maintenance_status ? csvArray.maintenance_status : 'N/A',
              created_at: csvArray.created_at ? csvArray.created_at : 'N/A ',
              credit_card: csvArray.credit_card ? csvArray.credit_card : 'Not Added',
              vehicle_status: csvArray.vehicle_status ? csvArray.vehicle_status : 'N/A'
            });
        }
        var options =
        {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: false,
          useBom: true,
          headers: (labels)
        };
        new Angular2Csv(resArray, 'Vehicle-List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
      }
      this.createCsvFlag = false;
    })
  }
  public showMaintenance(vehicle) {
    this.pNoM=1;
    this.maintenanceInfo=[];
    this.loadingIndicator = 'modalLoader';
    var params = {
      offset: 0,
      limit: 10,
      sortByColumn: 'date',
      sortOrder: 'desc',
      startDate: '',
      endDate: '',
      vehicle_id: vehicle._id,
      vehicle_status: '',//VM, VA, VAC, VD
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    };
    this.maintenanceInfoVehicle=vehicle._id;
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let data = {
      data: enc_data,
      email: this.useremail,
    }
    this._vehicleService.getVehicleMaintenanceHistory(data).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.loadingIndicator = '';
        this.maintenanceInfo = data.data;
        this.maintenanceInfoLength=data.count;
      }
    })
  }
  maintenancePaging(data){
    this.maintenanceInfo=[];
    let pageNo = (data * 10) - 10;
    this.loadingIndicator = 'modalLoader';
    var params = {
      offset: pageNo,
      limit: 10,
      sortByColumn: 'date',
      sortOrder: 'desc',
      startDate: '',
      endDate: '',
      vehicle_id: this.maintenanceInfoVehicle,
      vehicle_status: '',//VM, VA, VAC, VD
      company_id: this.company_id_list.length > 0 ? this.company_id_list : this.company_id_array
    };
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let data2 = {
      data: enc_data,
      email: this.useremail,
    }
    this._vehicleService.getVehicleMaintenanceHistory(data2).then((data) => {
      if (data && data.status == 200) {
        data = this._EncDecService.dwt(this.session_token, data.data);
        this.loadingIndicator = '';
        this.maintenanceInfo = data.data;
      }
    })
  }
}

