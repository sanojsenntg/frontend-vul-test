import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { WalletServicesService } from '../../../common/services/wallet/wallet-services.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { SplitupdataComponent } from "../../../common/dialog/splitupdata/splitupdata.component";
import { DriverService } from '../../../common/services/driver/driver.service';
import { OrderService } from '../../../common/services/order/order.service'
import { Router } from '@angular/router';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as moment from "moment/moment";
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-wallet-driver-report',
  templateUrl: './wallet-driver-report.component.html',
  styleUrls: ['./wallet-driver-report.component.css']
})
export class WalletDriverReportComponent implements OnInit {
  queryField: FormControl = new FormControl();
  queryField1: FormControl = new FormControl();
  public driver_id;
  public selectedAll = false;
  public job_id;
  public jobDataDataLength = 0;
  public companyId: any = [];
  public type_value = 'S';
  public selected_driver_id;
  public selected_job_id;
  public bankcode;
  public bankexcelcode;
  pageSize = 10;
  pageNo = 0;
  public is_search = false;
  page = 1;
  email: string;
  session: string;
  public searchSubmit = false;
  public searchSubmit1 = false;
  public settle_button = true;
  public export_button = true;
  public loadingIndicator;
  public driverData = [];
  public jobData = [];
  public InnerjobData = [];
  public haveData = false;
  public job = null;
  public pendingamount = 0;
  public to_pay;
  public diff;
  constructor(public _walletService: WalletServicesService,
    private router: Router,
    public jwtService: JwtService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public _driverService: DriverService,
    public _orderService: OrderService,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    formBuilder: FormBuilder,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    //this.selectedMoment = this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'));
    //this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
    //this.getFinalisationReports();
    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriver';
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: [],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._driverService.searchForTempDriver(enc_data)
          .subscribe(dec => {
            if (dec && dec.status == 200) {
              var result: any = this.encDecService.dwt(this.session, dec.data);
              console.log("drivers" + result.driver.length);
              this.driverData = result.driver;
            }
            this.loadingIndicator = '';
          });
      });
  }
  searchDriver(data) {
    if (typeof data === 'object') {
      this.selected_driver_id = data._id;
      console.log("driver_id" + this.selected_driver_id);
    }
    else {
      this.selected_driver_id = '';
    }
  }
  displayFnDriver(data): string {
    console.log(data);
    return data ? data.name : data;
  }
  reset() {
    this.res1Array = [];
    this.selected_job_id = '';
    this.selected_driver_id = '';
    this.creatingcsv = false;
    this.settle_button = false;
    this.driver_id = '';
    this.job_id = '';
    this.job = null;
  }
  public creatingcsv = false;
  public selected_status = "UNPAID";
  public selected_status2 = "topay";
  public progressvalue = 0;
  public k = 1;
  public res1Array = [];
  public ongoing = false;
  public createCsv() {
    if (this.ongoing) {
      return this.toastr.error("Please wait")
    } else {
      if (this.selected_driver_id) {
        this.ongoing = true;
        this.res1Array = [];
        let params1 = {
          driver_id: [this.selected_driver_id],
          status: this.selected_status,
          sub_status: this.selected_status2,
          bank_excel: status
        }
        var encrypted2 = this.encDecService.nwt(this.session, params1);
        var enc_data2 = {
          data: encrypted2,
          email: this.email
        }
        this._walletService.getJobDetailCSV2(enc_data2).subscribe((dec) => {
          this.ongoing = false;
          if (dec && dec.status == 200) {
            var res: any = this.encDecService.dwt(this.session, dec.data);
            console.log("data+=======================================" + res.jobs.length);
            if (res.jobs.length > 0) {
              this.job = res.jobs[0];
              const csvArray = this.job;
              //console.log("detailssssss+++++++++++++++"+JSON.stringify(csvArray));
              let bankcode;
              let bankexcelcode;
              if (csvArray.driver_id.account_details == "Emirates NBD" || csvArray.driver_id.account_details == "Emirates Islamic") {
                bankcode = "TR";
              } else {
                bankcode = "OR";
              }
              if (csvArray.driver_id.account_details === "Al Masraf") {
                bankexcelcode = "ABIN";
              }
              if (csvArray.driver_id.account_details === "Abu Dhabi Islamic Bank") {
                bankexcelcode = "ABDI";
              }
              if (csvArray.driver_id.account_details === "United Bank Limited (UBL)") {
                bankexcelcode = "UNIL";
              }
              if (csvArray.driver_id.account_details === "Abu Dhabi Commercial Bank") {
                bankexcelcode = "ADCB";
              }
              if (csvArray.driver_id.account_details === "Mashreq") {
                bankexcelcode = "BOML";
              }
              if (csvArray.driver_id.account_details === "National Bank of Ras Al-Khaimah PJSC (RAKBANK)") {
                bankexcelcode = "NRAK";
              }
              if (csvArray.driver_id.account_details === "Dubai Islamic Bank") {
                bankexcelcode = "DUIB";
              }
              if (csvArray.driver_id.account_details === "Emirates Islamic") {
                bankexcelcode = "MEBL";
              }
              if (csvArray.driver_id.account_details === "Emirates NBD") {
                bankexcelcode = "EBIL";
              }
              //console.log("Additional+++++++++++++++++++++" + JSON.stringify(csvArray));
              csvArray.pendingamount = 0;
              if (csvArray.order_details.length > 0) {
                if (csvArray.previous_unsettled && csvArray.previous_unsettled.length > 0) {
                  for (let l = 0; l < csvArray.previous_unsettled.length; l++) {
                    //console.log("pending+++++++++++++++++++++++++++inlooopbefore");
                    csvArray.pendingamount = parseFloat(csvArray.pendingamount) + (parseFloat(csvArray.previous_unsettled[l].due_amount.$numberDecimal) - parseFloat((csvArray.previous_unsettled[l].settled_amount ? csvArray.previous_unsettled[l].settled_amount.$numberDecimal : "0")))
                    //console.log("pending+++++++++++++++++++++++++++inlooop" + JSON.stringify(csvArray.pendingamount));
                  }
                }
                let to_pay = (parseFloat(csvArray.pendingamount) + parseFloat(csvArray.total_online_order) + parseFloat(csvArray.total_discount)) - (parseFloat(csvArray.total_fee_with_without_rta));
                let diff = to_pay - parseFloat(csvArray.due_amount.$numberDecimal)
                this.res1Array.push({
                  job_id: csvArray.job_id.job_id,
                  driver: csvArray.driver_id ? csvArray.driver_id.emp_id : '',
                  driver_name: csvArray.driver_id ? csvArray.driver_id.name : '',
                  bank_details: csvArray.driver_id ? csvArray.driver_id.account_details : '',
                  bank: bankcode,
                  bank_code: bankexcelcode,
                  iban: csvArray.driver_id ? csvArray.driver_id.iban : '',
                  settled_amount: csvArray.settled_amount ? csvArray.settled_amount.$numberDecimal : 0,
                  total_revenue: csvArray.total_revenue ? csvArray.total_revenue : 0,
                  total_cash_in_hand: csvArray.total_cash_in_hand ? csvArray.total_cash_in_hand : 0,
                  previous: csvArray.pendingamount,
                  amount: csvArray.due_amount.$numberDecimal,
                  date: csvArray.created_at ? csvArray.created_at : '',
                  status: csvArray.status,
                  salik_total: csvArray.total_salik,
                  sharjah_total: csvArray.total_sharjah,
                  total_cash_order: csvArray.total_cash_order,
                  total_online_order: csvArray.total_online_order,
                  total_pos_order: csvArray.total_pos_order,
                  total_discount: csvArray.total_discount,
                  total_fee_with_rta: csvArray.total_fee_with_rta,
                  total_fee_with_without_rta: csvArray.total_fee_with_without_rta,
                  trip_id: '',
                  trip_date: '',
                  trip_amount: csvArray.total_trip_amount,
                  online: csvArray.total_online_order,
                  payment_mode: '',
                  discount: csvArray.total_discount,
                  driver_revenue: csvArray.total_revenue ? csvArray.total_revenue : 0,
                  fee_collected: csvArray.total_fee_with_rta,
                  fee_without_rta: csvArray.total_fee_with_without_rta,
                  salik: csvArray.total_salik,
                  sharjah: csvArray.total_sharjah,
                  customer_paid: csvArray.total_customer_paid,
                  cybersource: '',
                  authcode: '',
                  to_pay: to_pay,
                  diff: parseFloat(parseFloat(to_pay.toString()).toFixed(2)) - parseFloat(csvArray.due_amount.$numberDecimal)
                });
                for (let k = 0; k < csvArray.order_details.length; k++) {
                  this.res1Array.push({
                    job_id: '',
                    driver: '',
                    driver_name: '',
                    bank_details: '',
                    bank: '',
                    bank_code: '',
                    iban: '',
                    settled_amount: '',
                    total_revenue: '',
                    total_cash_in_hand: '',
                    previous: '',
                    amount: '',
                    date: '',
                    status: '',
                    salik_total: '',
                    sharjah_total: '',
                    total_cash_order: '',
                    total_online_order: '',
                    total_pos_order: '',
                    total_discount: '',
                    total_fee_with_rta: '',
                    total_fee_with_without_rta: '',
                    trip_id: csvArray.order_details[k].unique_order_id,
                    trip_date: csvArray.order_details[k].trip_date_time,
                    trip_amount: csvArray.order_details[k].trip_amount.$numberDecimal,
                    online: csvArray.order_details[k].online,
                    payment_mode: csvArray.order_details[k].payment_mode,
                    discount: csvArray.order_details[k].discount_amount,
                    driver_revenue: csvArray.order_details[k].driver_revenue.$numberDecimal,
                    fee_collected: csvArray.order_details[k].fee_collected,
                    fee_without_rta: csvArray.order_details[k].fee_without_rta,
                    salik: csvArray.order_details[k].salik_amount,
                    sharjah: csvArray.order_details[k].sharjah_amount,
                    customer_paid: csvArray.order_details[k].customer_paid,
                    cybersource: csvArray.order_details[k].cybersource_ref ? csvArray.order_details[k].cybersource_ref : '',
                    authcode: csvArray.order_details[k].auth_code ? csvArray.order_details[k].auth_code : '',
                    to_pay: '',
                    diff: '',
                  });
                  if (k == csvArray.order_details.length - 1) {
                    let labels = [
                      'Job id',
                      'Driver',
                      'Driver Name',
                      'Bank Name',
                      'Bank Code',
                      'Bank',
                      'IBAN',
                      'Settled Amount',
                      'Total Driver revenue',
                      'Total Cash in hand',
                      'Previous Due Amount',
                      'Due Amount (Total Driver revenue - (Total Cash in hand + Previous Due Amount))',
                      'Date',
                      'status',
                      'Total Salik',
                      'Total Sharjah',
                      'Total cash order',
                      'Total online order',
                      'Total POS order',
                      'Total Discount',
                      'Total Fee collected(with RTA)',
                      'Total Fee collected(without RTA)',
                      'Trip Id',
                      'Trip Date',
                      'Trip amount',
                      'Online',
                      'Payment Mode',
                      'Discount amount',
                      'Driver revenue amount',
                      'Fee collected(including RTA)',
                      'Fee collected(excluding RTA)',
                      'Salik',
                      'Sharjah',
                      'Customer Paid',
                      'Cybersource ref',
                      'Auth code',
                      'To pay',
                      'Diff'
                    ];
                    var options =
                    {
                      fieldSeparator: ',',
                      quoteStrings: '"',
                      decimalseparator: '.',
                      showLabels: true,
                      showTitle: false,
                      useBom: true,
                      headers: (labels)
                    };
                    new Angular2Csv(this.res1Array, 'Wallet Driver Report-' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
                  }
                }
              }
            } else {
              this.toastr.error("No record Found")
            }
          }
        })
      } else {
        this.toastr.error("please select driver")
      }
    }
  }
  searchReco() {
    console.log("hit");
    if (this.selected_driver_id) {
      console.log(this.selected_driver_id);
      let params1 = {
        driver_id: [this.selected_driver_id],
        status: this.selected_status,
        sub_status: this.selected_status2,
        bank_excel: status
      }
      var encrypted2 = this.encDecService.nwt(this.session, params1);
      var enc_data2 = {
        data: encrypted2,
        email: this.email
      }
      //console.log("")
      this._walletService.getJobDetailCSV2(enc_data2).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          console.log("data+=======================================" + res.jobs.length);
          if (res.jobs.length > 0) {
            this.job = res.jobs[0];
            console.log("job" + JSON.stringify(this.job))
            if (this.job.driver_id.account_details == "Emirates NBD" || this.job.driver_id.account_details == "Emirates Islamic") {
              this.bankcode = "TR";
            } else {
              this.bankcode = "OR";
            }
            if (this.job.driver_id.account_details === "Al Masraf") {
              this.bankexcelcode = "ABIN";
            }
            if (this.job.driver_id.account_details === "Abu Dhabi Islamic Bank") {
              this.bankexcelcode = "ABDI";
            }
            if (this.job.driver_id.account_details === "United Bank Limited (UBL)") {
              this.bankexcelcode = "UNIL";
            }
            if (this.job.driver_id.account_details === "Abu Dhabi Commercial Bank") {
              this.bankexcelcode = "ADCB";
            }
            if (this.job.driver_id.account_details === "Mashreq") {
              this.bankexcelcode = "BOML";
            }
            if (this.job.driver_id.account_details === "National Bank of Ras Al-Khaimah PJSC (RAKBANK)") {
              this.bankexcelcode = "NRAK";
            }
            if (this.job.driver_id.account_details === "Dubai Islamic Bank") {
              this.bankexcelcode = "DUIB";
            }
            if (this.job.driver_id.account_details === "Emirates Islamic") {
              this.bankexcelcode = "MEBL";
            }
            if (this.job.driver_id.account_details === "Emirates NBD") {
              this.bankexcelcode = "EBIL";
            }
            if (this.job.previous_unsettled && this.job.previous_unsettled.length > 0) {
              for (let l = 0; l < this.job.previous_unsettled.length; l++) {
                //console.log("pending+++++++++++++++++++++++++++inlooopbefore");
                this.pendingamount = parseFloat(this.pendingamount.toString()) + (parseFloat(this.job.previous_unsettled[l].due_amount.$numberDecimal) - parseFloat((this.job.previous_unsettled[l].settled_amount ? this.job.previous_unsettled[l].settled_amount.$numberDecimal : "0")))
                //console.log("pending+++++++++++++++++++++++++++inlooop" + JSON.stringify(csvArray.pendingamount));
              }
            }
            this.to_pay = (parseFloat(this.pendingamount.toString()) + parseFloat(this.job.total_online_order) + parseFloat(this.job.total_discount)) - (parseFloat(this.job.total_fee_with_without_rta));
            this.diff = this.to_pay - parseFloat(this.job.due_amount.$numberDecimal)
          } else {
            console.log("no records found");
            this.job = null;
          }
        } else {
          console.log(dec);
        }
      })
    } else {
      this.toastr.error("please select driver")
    }
  }
}

