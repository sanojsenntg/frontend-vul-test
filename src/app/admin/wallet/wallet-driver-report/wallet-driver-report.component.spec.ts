import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletDriverReportComponent } from './wallet-driver-report.component';

describe('WalletDriverReportComponent', () => {
  let component: WalletDriverReportComponent;
  let fixture: ComponentFixture<WalletDriverReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletDriverReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletDriverReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
