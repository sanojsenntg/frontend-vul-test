import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { WalletServicesService } from '../../../common/services/wallet/wallet-services.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { DriverService } from '../../../common/services/driver/driver.service';
import { Router } from '@angular/router';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as moment from "moment/moment";
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-wallet-cashier',
  templateUrl: './wallet-cashier.component.html',
  styleUrls: ['./wallet-cashier.component.css']
})
export class WalletCashierComponent implements OnInit {
  queryField: FormControl = new FormControl();
  queryField1: FormControl = new FormControl();
  public walletData = [];
  public driver_id;
  public amount;
  public allowcollect = true;
  public bank_reference;
  public walletDataLength = 0;
  public companyId: any = [];
  public selected_driver_id;
  public min = new Date(moment(Date.now()).subtract(90, 'days').format('YYYY-MM-DD, hh:mm'));
  public max = new Date();
  pageSize = 10;
  pageNo = 0;
  public is_search = false;
  public buttionclick = false;
  page = 1;
  email: string;
  session: string;
  public searchSubmit = false;
  public loadingIndicator;
  public driverData = [];
  public driver_cash_hand_balance;
  public driver_paid_balance;
  public driver_revenue;
  constructor(public _walletService: WalletServicesService,
    private router: Router,
    public jwtService: JwtService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public _driverService: DriverService,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    formBuilder: FormBuilder,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriver';
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: []//this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._driverService.searchForTempDriver(enc_data)
          .subscribe(dec => {
            if (dec && dec.status == 200) {
              var result: any = this.encDecService.dwt(this.session, dec.data);
              this.driverData = result.driver;
            }
            this.loadingIndicator = '';
          });
      });
  }
  searchDriver(data) {
    if (typeof data === 'object') {
      this.selected_driver_id = data._id;
      console.log("driver_id" + this.selected_driver_id);
    }
    else {
      this.selected_driver_id = '';
    }
  }
  displayFnDriver(data): string {
    console.log(data);
    return data ? data.name : data;
  }
  getWalletsOfUser() {
    if (this.buttionclick) {

    } else {
      if (this.selected_driver_id) {
        this.buttionclick = true;
        var params = {
          driver_id: this.selected_driver_id,
          company_id: this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._walletService.getWalletFullDetails(enc_data)
          .subscribe(dec => {
            this.buttionclick = false;
            if (dec && dec.status == 200) {
              var result: any = this.encDecService.dwt(this.session, dec.data);
              this.walletData = result.walletData;
              console.log(JSON.stringify(this.walletData));
              for (var i = 0; i < this.walletData.length; ++i) {
                if (this.walletData[i].wallet_type == "TDP") {
                  this.driver_paid_balance = this.walletData[i].balance;
                }
                if (this.walletData[i].wallet_type == "TDR") {
                  this.driver_revenue = this.walletData[i].balance;
                }
                if (this.walletData[i].wallet_type == "TDCH") {
                  this.driver_cash_hand_balance = this.walletData[i].balance;
                }
                if (this.walletData[i].wallet_type == "TDD") {
                  if (this.walletData[i].balance < 0) {
                    this.walletData[i].balance = Math.abs(this.walletData[i].balance);
                    this.allowcollect = true;
                  } else {
                    this.allowcollect = false;
                  }
                }
              }
            }
            this.loadingIndicator = '';
          });
      } else {
        this.toastr.warning("Please select a Driver")
      }
    }
  }
  reset() {
    this.selected_driver_id = '';
    this.driver_id = '';
    this.amount = "";
    this.bank_reference = "";
    this.walletData = [];
    this.bank_reference = "";
    this.driver_cash_hand_balance = '';
    this.driver_revenue = '';
    this.driver_paid_balance = '';
  }
  collect() {
    if (this.buttionclick) {

    } else {
      if (this.allowcollect) {
        console.log(this.driver_cash_hand_balance);
        console.log(this.driver_paid_balance);
        console.log(this.driver_revenue);
        let allowd_amount = 0.0;
        allowd_amount = (parseFloat(this.driver_cash_hand_balance) + parseFloat(this.driver_paid_balance)) - parseFloat(this.driver_revenue);
        if (!this.selected_driver_id) {
          this.toastr.warning("please select a driver");
        } else if (!this.amount || this.amount == 0) {
          this.toastr.warning("Enter valid amount");
          if (allowd_amount < 0) {
            parseFloat(allowd_amount.toString()) * -1;
          }
        } else if (this.amount && this.amount > parseFloat(allowd_amount.toString()).toFixed(2)) {
          this.toastr.warning("Maximum amount allowed is " + parseFloat(allowd_amount.toString()).toFixed(2));
        } else {
          this.buttionclick = true;
          var params = {
            driver_id: this.selected_driver_id,
            amount: this.amount,
            company_id: this.companyId,
          }
          var encrypted = this.encDecService.nwt(this.session, params);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this._walletService.collectFromDriver(enc_data)
            .subscribe(dec => {
              this.buttionclick = false;
              if (dec && dec.status == 200) {
                this.toastr.success("payment collected successfully");
                this.getWalletsOfUser();
              } else {
                this.toastr.error(dec.message);
              }
              this.loadingIndicator = '';
            });
        }
      } else {
        this.toastr.error("Driver has no due")
      }

    }
  }
  pay() {
    if (this.buttionclick) {

    } else {
      if (!this.selected_driver_id) {
        this.toastr.warning("please select a driver");
      } else if (!this.amount || this.amount == 0) {
        this.toastr.warning("Enter valid amount");
      } else if (!this.bank_reference) {
        this.toastr.warning("Enter Bank reference number");
      } else {
        this.buttionclick = true;
        var params = {
          driver_id: this.selected_driver_id,
          amount: this.amount,
          bank_reference: this.bank_reference,
          company_id: this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._walletService.payDriver(enc_data)
          .subscribe(dec => {
            this.buttionclick = false;
            if (dec && dec.status == 200) {
              this.toastr.success("Paid successfully");
              this.getWalletsOfUser();
            } else {
              this.toastr.error(dec.message);
            }
            this.loadingIndicator = '';
          });
      }
    }
  }
}
