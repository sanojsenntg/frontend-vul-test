import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AclAuthervice } from './../../common/services/access-control/acl-auth.service'
import { WalletsRoute } from './../../admin/wallet/wallets/wallets-routing.module'
import { WalletTransactionsComponent } from './../../admin/wallet/wallet-transactions/wallet-transactions.component'
import { WalletSettlementComponent } from './../../admin/wallet/wallet-settlement/wallet-settlement.component'
import { WalletManagmentComponent } from './wallet-managment/wallet-managment.component';
import { WalletCashierComponent } from './wallet-cashier/wallet-cashier.component';
import { WalletBonusPenaltyComponent } from './wallet-bonus-penalty/wallet-bonus-penalty.component';
import { WalletDriverReportComponent } from './wallet-driver-report/wallet-driver-report.component';

export const WalletRoute: Routes = [
      { path: '', component: WalletManagmentComponent, canActivate: [AclAuthervice], data: { roles: ["Wallet"] } },
      { path: 'wallet-transactions', component: WalletTransactionsComponent, canActivate: [AclAuthervice], data: { roles: ["Wallet - Transactions"] } },
      { path: 'wallet-settlements', component: WalletSettlementComponent, canActivate: [AclAuthervice], data: { roles: ["Wallet - Settlements"] } },
      { path: 'wallet-cashier', component: WalletCashierComponent, canActivate: [AclAuthervice], data: { roles: ["Wallet - Cashier"] } },
      { path: 'wallet-bonus-penalty', component: WalletBonusPenaltyComponent, canActivate: [AclAuthervice], data: { roles: ["Wallet - Adjustment"] } },
      { path: 'wallet-driver', component: WalletDriverReportComponent, canActivate: [AclAuthervice], data: { roles: ["Wallet - Driver_records"] } },
      { path: 'wallets', children: WalletsRoute },
];

