import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr';

import { JwtService } from '../../../../common/services/api/jwt.service';
import { WalletServicesService } from '../../../../common/services/wallet/wallet-services.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { PartnerService } from '../../../../common/services/partners/partner.service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { DriverService } from '../../../../common/services/driver/driver.service';
@Component({
  selector: 'app-wallet-add',
  templateUrl: './wallet-add.component.html',
  styleUrls: ['./wallet-add.component.css']
})
export class WalletAddComponent implements OnInit {
  queryField: FormControl = new FormControl();
  public companyId: any = [];
  public userData = [];
  public driverData = [];
  public saveForm: FormGroup;
  private sub: Subscription;
  public _id;
  public loadingIndicator;
  email: string;
  session: string;
  public user_type = 'S';
  public c_user;
  public user = false;
  public driver_id;
  public selected_driver_id;
  public driveruser = false;
  constructor(private _walletService: WalletServicesService,
    private _partnerService: PartnerService,
    private _companyService: CompanyService,
    private router: Router,
    public jwtService: JwtService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public toastr: ToastsManager,
    public _driverService: DriverService,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.saveForm = formBuilder.group({
      user_type: ['S', [
        Validators.required,
      ]],
      wallet_type: '',
      user_id: '',
      company_id: '',
      description: '',
    });
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriver';
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._driverService.searchForTempDriver(enc_data)
          .subscribe(dec => {
            if (dec && dec.status == 200) {
              var result: any = this.encDecService.dwt(this.session, dec.data);
              this.driverData = result.driver;
            }
            this.loadingIndicator = '';
          });
      });
  }
  public setCategory(event) {
    console.log(event);
    this.user_type = event;
    if (this.user_type == "CP") {
      this.driveruser = false;
      this.getCompanies();
    } else if (this.user_type == "P") {
      this.driveruser = false;
      this.getPartner();
    } else if (this.user_type == "D") {
      this.driveruser = false;
      this.getDriver();
    } else {
      let c_user_id = JSON.parse(window.localStorage['adminUser']);
      this.c_user = c_user_id._id
      this.user = false;
    }
  }
  public getDriver() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.searchForTempDriver(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        this.driveruser = true;
        var Driverdata: any = this.encDecService.dwt(this.session, dec.data);
        this.driverData = Driverdata.driver;
        this.user = false;
      }
    });
  }
  public getCompanies() {
    console.log("calling");
    var params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._companyService.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.user = true;
          this.userData = data.getCompanies;
        }
      }
    });
  }
  public getPartner() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._partnerService.getPartner(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.userData = data.getPartners;
          this.user = true;
        } else {
          console.log(dec.message)
        }
      }
    });

  }
  public addWallet() {
    if (!this.saveForm.valid) {
      // this.toastr.error('All fields are required');
      return;
    }
    if (this.user_type == "S" && this.c_user == '') {
      return;
    }
    if (this.user_type == "D" && this.selected_driver_id == '') {
      return;
    }
    if (this.saveForm.value.user_type && this.saveForm.value.wallet_type) {
      const walletData = {
        wallet_type: this.saveForm.value.user_type,
        category: this.saveForm.value.wallet_type,
        user_id: this.saveForm.value.user_id,
        driver_id: null,
        description: this.saveForm.value.description,
        company_id: this.companyId
      };
      if (this.user_type == "S") {
        walletData.user_id = this.c_user;
        this.toastr.warning("Please fill all fields");
      }
      if (this.user_type == "D") {
        walletData.driver_id = this.selected_driver_id;
        this.toastr.warning("Please fill all fields");
      }
      console.log(JSON.stringify(walletData) + "usertype+++++++" + this.user_type);
      var encrypted = this.encDecService.nwt(this.session, walletData);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._walletService.addWallet(enc_data)
        .subscribe((dec) => {
          if (dec.status == 200) {
            this.toastr.success('Wallet added successfully');
            this.router.navigate(['/admin/wallet/wallets']);
          } else {
            this.toastr.error(dec.message);
          }
        })
    } else {
      //this.toastr.warning("Please fill all fields");
    }
  }
  searchDriver(data) {
    if (typeof data === 'object') {
      this.selected_driver_id = data._id;
      console.log("driver_id" + this.selected_driver_id);
    }
    else {
      this.selected_driver_id = '';
    }
  }
  displayFnDriver(data): string {
    console.log(data);
    return data ? data.name : data;
  }
}
