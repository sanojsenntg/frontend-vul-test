import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { WalletServicesService } from '../../../../common/services/wallet/wallet-services.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-wallet-list',
  templateUrl: './wallet-list.component.html',
  styleUrls: ['./wallet-list.component.css'],
})
export class WalletListComponent implements OnInit {
  public walletData = [];
  public walletDataLength = 0;
  public companyId: any = [];
  public type_value = 'S';
  pageSize = 10;
  pageNo = 0;
  public is_search = false;
  page = 1;
  email: string;
  session: string;
  public searchSubmit = false;
  constructor(private _walletService: WalletServicesService,
    private router: Router,
    public jwtService: JwtService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.getWalletListing();
  }
  public getWalletListing() {
    console.log("calling+++" + this.type_value);
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      company_id: this.companyId,
      type: this.type_value
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
    }
    this._walletService.getWalletList(enc_data).subscribe((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        //console.log(JSON.stringify(data));
        this.walletData = data.walletData;
        this.walletDataLength = data.count;
      } else {
        this.toastr.error(dec.message);
      }
    });
  }
  public getBalance(wallet) {
    console.log(JSON.stringify(wallet));
    let user_id;
    if (wallet.driver_id) {
      user_id = wallet.driver_id._id;
    }
    if (wallet.user_id) {
      user_id = wallet.user_id._id;
    }
    if (wallet.customer_id) {
      user_id = wallet.customer_id._id;
    }
    if (wallet.company_id) {
      user_id = wallet.company_id._id;
    }
    if (wallet.payment_gateway_id) {
      user_id = wallet.payment_gateway_id._id;
    }
    if (wallet.partner_id) {
      user_id = wallet.partner_id._id;
    }
    console.log("user" + user_id);
    if (this.is_search) {
      this.toastr.warning("Checking Balance...")
    } else {
      this.is_search = true;
      const params = {
        company_id: this.companyId,
        user_id: user_id,
        wallet_type: wallet.user_type,
        category: wallet.wallet_type
      };
      //console.log("params+++++++" + JSON.stringify(params));
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email,
      }
      this._walletService.getWalletBalance(enc_data).subscribe((dec) => {
        this.is_search = false;
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          console.log(JSON.stringify(data));
          var index = this.walletData.indexOf(wallet);
          this.walletData[index].balance = data.balance;
          //console.log(JSON.stringify(this.walletData[index]));
        } else {
          this.toastr.error(dec.message);
        }
      });
    }

  }
  pagingAgent(data) {
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    let params;
    params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      company_id: this.companyId,
      type: this.type_value
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
    }
    this._walletService.getWalletList(enc_data).subscribe((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        //console.log(JSON.stringify(data));
        this.walletData = data.walletData;
        this.walletDataLength = data.count;
      } else {
        this.toastr.error(dec.message);
      }
    });
  }
}
