import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { WalletServicesService } from '../../../common/services/wallet/wallet-services.service';
import { OrderService } from '../../../common/services/order/order.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { DriverService } from '../../../common/services/driver/driver.service';
import { Router } from '@angular/router';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as moment from "moment/moment";
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-wallet-bonus-penalty',
  templateUrl: './wallet-bonus-penalty.component.html',
  styleUrls: ['./wallet-bonus-penalty.component.css']
})
export class WalletBonusPenaltyComponent implements OnInit {
  queryField: FormControl = new FormControl();
  queryField1: FormControl = new FormControl();
  public walletData = [];
  public driver_id;
  public amount;
  public allOrderIddata;
  public bank_reference;
  public walletDataLength = 0;
  public companyId: any = [];
  public buttionclick = false;
  public selected_driver_id;
  public min = new Date(moment(Date.now()).subtract(90, 'days').format('YYYY-MM-DD, hh:mm'));
  public max = new Date();
  pageSize = 10;
  pageNo = 0;
  public is_search = false;
  //public buttionclick = false;
  public operation_type = '';
  page = 1;
  email: string;
  session: string;
  public ops_type;
  public ops_mode;
  public searchSubmit = false;
  public loadingIndicator;
  public driverData = [];
  public driver_cash_hand_balance;
  public driver_paid_balance;
  public driver_revenue;
  public order_id;
  public order_id_selected;
  public notes;
  public unique_order_id;
  constructor(public _walletService: WalletServicesService,
    private _orderService: OrderService,
    private router: Router,
    public jwtService: JwtService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public _driverService: DriverService,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    formBuilder: FormBuilder,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.queryField1.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchOrder';
    })
    this.queryField1.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          search_keyword: query,
          driver_id: this.selected_driver_id ? this.selected_driver_id : null,
          company_id: this.companyId.length > 0 ? this.companyId : [this.companyId],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._orderService.getAllOrderIdDriver(enc_data).then(dec => {
          if (dec && dec.status == 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.allOrderIddata = result.getOrders;
          }
          this.loadingIndicator = '';
        });
      })

    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriver';
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: []//this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._driverService.searchForTempDriver(enc_data)
          .subscribe(dec => {
            if (dec && dec.status == 200) {
              var result: any = this.encDecService.dwt(this.session, dec.data);
              this.driverData = result.driver;
            }
            this.loadingIndicator = '';
          });
      });
  }
  searchDriver(data) {
    if (typeof data === 'object') {
      this.selected_driver_id = data._id;
      console.log("driver_id" + this.selected_driver_id);
      this.getWalletsOfUser();
    }
    else {
      this.selected_driver_id = '';
    }
  }
  displayFnDriver(data): string {
    console.log(data);
    return data ? data.name : data;
  }
  getWalletsOfUser() {
    if (this.buttionclick) {

    } else {
      if (this.selected_driver_id) {
        this.buttionclick = true;
        var params = {
          driver_id: this.selected_driver_id,
          company_id: this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._walletService.getWalletFullDetails(enc_data)
          .subscribe(dec => {
            this.buttionclick = false;
            if (dec && dec.status == 200) {
              var result: any = this.encDecService.dwt(this.session, dec.data);
              this.walletData = result.walletData;
              //console.log(JSON.stringify(this.walletData));
              for (var i = 0; i < this.walletData.length; ++i) {
                if (this.walletData[i].wallet_type == "TDP") {
                  this.driver_paid_balance = this.walletData[i].balance;
                }
                if (this.walletData[i].wallet_type == "TDR") {
                  this.driver_revenue = this.walletData[i].balance;
                }
                if (this.walletData[i].wallet_type == "TDCH") {
                  this.driver_cash_hand_balance = this.walletData[i].balance;
                }
              }
            }
            this.loadingIndicator = '';
          });
      } else {
        this.toastr.warning("Please select a Driver")
      }
    }
  }
  public setOperation(event) {
    console.log(event);
    this.ops_type = event;
    console.log("ops" + this.ops_type)
  }
  public setOperationMode(event) {
    console.log(event);
    this.ops_mode = event;
    console.log("ops" + this.ops_mode)
  }

  orderIdselected(event: MatAutocompleteSelectedEvent): void {
    console.log("orders++++++" + JSON.stringify(event.option.value))
    this.order_id = event.option.value.unique_order_id;
    this.order_id_selected = event.option.value._id;
    this.unique_order_id = event.option.value.unique_order_id;
    console.log(this.order_id);
  }
  carryoperation() {
    if (this.buttionclick) {

    } else {
      if (!this.notes || this.notes == null || this.notes == '') {
        this.toastr.error("Please enter notes");
      }
      else if (!this.selected_driver_id || this.selected_driver_id == null) {
        this.toastr.error("Please select Driver");
      } else if (this.ops_type == 'A' && this.ops_mode == '') {
        this.toastr.error("Operation mode is necessery")
      } else if (this.amount < 0 || this.amount == 0 || typeof this.amount != 'number') {
        this.toastr.error("Please enter a valid amount")
      } else {
        if (this.ops_type == "A") {
          if (this.ops_mode == "A") {
            this.buttionclick = true;
            var params = {
              driver_id: this.selected_driver_id,
              company_id: this.companyId,
              amount: this.amount,
              order_id: this.order_id_selected,
              unique_order_id: this.unique_order_id,
              description: this.notes + " -- " + this.email
            }
            var encrypted = this.encDecService.nwt(this.session, params);
            var enc_data = {
              data: encrypted,
              email: this.email
            }
            this._walletService.AdjustAddAmount(enc_data)
              .subscribe(dec => {
                this.buttionclick = false;
                console.log("resss"+JSON.stringify(dec))
                if (dec && dec.status == 200) {
                  this.toastr.success("Operation done successfully")
                  this.reset();
                } else {
                  this.toastr.error(dec.message);
                  this.reset();
                }
                this.loadingIndicator = '';
              });
          }
          if (this.ops_mode == "S") {
            this.buttionclick = true;
            var params1 = {
              driver_id: this.selected_driver_id,
              company_id: this.companyId,
              amount: this.amount,
              order_id: this.order_id_selected,
              unique_order_id: this.unique_order_id,
              description: this.notes + " -- " + this.email
            }
            var encrypted = this.encDecService.nwt(this.session, params1);
            var enc_data = {
              data: encrypted,
              email: this.email
            }
            this._walletService.AdjustReduceAmount(enc_data)
              .subscribe(dec => {
                this.buttionclick = false;
                if (dec && dec.status == 200) {
                  this.toastr.success("Operation done successfully")
                  this.reset();
                } else {
                  this.toastr.error(dec.message)
                  this.reset();
                }
                this.loadingIndicator = '';
              });
          }
        }
        if (this.ops_type == "B") {
          this.buttionclick = true;
          var params2 = {
            driver_id: this.selected_driver_id,
            company_id: this.companyId,
            amount: this.amount,
            description: this.notes + " -- " + this.email
          }
          var encrypted = this.encDecService.nwt(this.session, params2);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this._walletService.BonusTransaction(enc_data)
            .subscribe(dec => {
              this.buttionclick = false;
              if (dec && dec.status == 200) {
                this.toastr.success("Operation done successfully");
                this.reset();
              } else {
                this.toastr.error(dec.message);
                this.reset();
              }
              this.loadingIndicator = '';
            });
        }
        if (this.ops_type == "P") {
          this.buttionclick = true;
          var params3 = {
            driver_id: this.selected_driver_id,
            company_id: this.companyId,
            amount: this.amount,
            description: this.notes + " -- " + this.email
          }
          var encrypted = this.encDecService.nwt(this.session, params3);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this._walletService.PenaltyTransaction(enc_data)
            .subscribe(dec => {
              this.buttionclick = false;
              if (dec && dec.status == 200) {
                this.toastr.success("Operation done successfully");
                this.reset();
              } else {
                this.toastr.error(dec.message);
                this.reset();
              }
              this.loadingIndicator = '';
            });
        }
      }
    }
  }
  reset() {
    this.selected_driver_id = '';
    this.amount = 0;
    this.order_id = '';
    this.ops_mode = '';
    this.ops_type = '';
  }
}
