import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { WalletServicesService } from '../../../common/services/wallet/wallet-services.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-wallet-transactions',
  templateUrl: './wallet-transactions.component.html',
  styleUrls: ['./wallet-transactions.component.css']
})
export class WalletTransactionsComponent implements OnInit {
  public TransactionData = [];
  public TransactionDataLength = 0;
  public companyId: any = [];
  pageSize = 10;
  pageNo = 0;
  public is_search = false;
  page = 1;
  email: string;
  session: string;
  public searchSubmit = false;
  public type_value;
  constructor(private _walletService: WalletServicesService,
    private router: Router,
    public jwtService: JwtService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
  }
  ngOnInit() {
    this.getTransactionListing();
  }
  public getTransactionListing() {
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      company_id: this.companyId,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
    }
    this._walletService.getTransactionList(enc_data).subscribe((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        console.log(JSON.stringify(data));
        this.TransactionData = data.TransactionData;
        this.TransactionDataLength = data.count;
      } else {
        this.toastr.error(dec.message);
      }
    });
  }
  pagingAgent(data) {
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    let params;
    params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      company_id: this.companyId,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
    }
    this._walletService.getTransactionList(enc_data).subscribe((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        console.log(JSON.stringify(data));
        this.TransactionData = data.TransactionData;
        this.TransactionDataLength = data.count;
      } else {
        this.toastr.error(dec.message);
      }
    });
  }
}
