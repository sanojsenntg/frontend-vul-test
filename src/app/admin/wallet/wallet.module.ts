import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WalletManagmentComponent } from './wallet-managment/wallet-managment.component';
import { WalletsModule } from './wallets/wallets.module';
import { WalletTransactionsComponent } from './wallet-transactions/wallet-transactions.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WalletServicesService } from '../../common/services/wallet/wallet-services.service';
import { WalletServicesRestService } from '../../common/services/wallet/wallet-services-rest.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WalletSettlementComponent } from './wallet-settlement/wallet-settlement.component';
import { MatCardModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { WalletCashierComponent } from './wallet-cashier/wallet-cashier.component';
import { SplitupdataComponent } from '../../common/dialog/splitupdata/splitupdata.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { WalletBonusPenaltyComponent } from './wallet-bonus-penalty/wallet-bonus-penalty.component';
import { MatSelectModule } from '@angular/material/select';
import { WalletDriverReportComponent } from './wallet-driver-report/wallet-driver-report.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatCardModule,
    MatAutocompleteModule,
    OwlDateTimeModule, OwlNativeDateTimeModule,
    WalletsModule,
    MatTooltipModule,
    RouterModule,
    MatSelectModule,
    ReactiveFormsModule,
    NgbModule
  ],
  exports: [
    RouterModule, FormsModule
  ],
  providers: [
    WalletServicesRestService,
    WalletServicesService
  ],
  declarations: [WalletManagmentComponent, WalletTransactionsComponent, WalletSettlementComponent, WalletCashierComponent, WalletBonusPenaltyComponent, WalletDriverReportComponent],
  entryComponents: [SplitupdataComponent]
})
export class WalletModule { }
