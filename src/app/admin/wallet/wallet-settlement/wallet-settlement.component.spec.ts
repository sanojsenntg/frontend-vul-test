import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletSettlementComponent } from './wallet-settlement.component';

describe('WalletSettlementComponent', () => {
  let component: WalletSettlementComponent;
  let fixture: ComponentFixture<WalletSettlementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletSettlementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletSettlementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
