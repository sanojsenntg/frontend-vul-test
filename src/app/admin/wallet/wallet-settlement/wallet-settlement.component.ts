import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { WalletServicesService } from '../../../common/services/wallet/wallet-services.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { SplitupdataComponent } from "../../../common/dialog/splitupdata/splitupdata.component";
import { DriverService } from '../../../common/services/driver/driver.service';
import { OrderService } from '../../../common/services/order/order.service'
import { Router } from '@angular/router';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as moment from "moment/moment";
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-wallet-settlement',
  templateUrl: './wallet-settlement.component.html',
  styleUrls: ['./wallet-settlement.component.css']
})
export class WalletSettlementComponent implements OnInit {
  queryField: FormControl = new FormControl();
  queryField1: FormControl = new FormControl();
  public walletData = [];
  public TransactionData = [];
  public TransactionDataLength;
  public driver_id;
  public selectedAll = false;
  public job_id;
  public checkbox_button = false;
  public showexpanded = false;
  public bank_button = false;
  public walletDataLength = 0;
  public jobDataDataLength = 0;
  public companyId: any = [];
  public type_value = 'S';
  public bank_reference_number = "";
  public selectedMoment: any = "";
  public selectedMoment1: any = "";
  public selected_driver_id;
  public finalcount = 0;
  public selected_job_id;
  public min = new Date(moment(Date.now()).subtract(90, 'days').format('YYYY-MM-DD, hh:mm'));
  public max = new Date();
  pageSize = 10;
  pageNo = 0;
  public is_search = false;
  page = 1;
  email: string;
  session: string;
  public searchSubmit = false;
  public searchSubmit1 = false;
  public settle_button = true;
  public export_button = true;
  public loadingIndicator;
  public driverData = [];
  public jobData = [];
  public InnerjobData = [];
  public haveData = false;
  constructor(public _walletService: WalletServicesService,
    private router: Router,
    public jwtService: JwtService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public _driverService: DriverService,
    public _orderService: OrderService,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    formBuilder: FormBuilder,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
    this.getFinalisationReports();
    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriver';
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._driverService.searchForTempDriver(enc_data)
          .subscribe(dec => {
            if (dec && dec.status == 200) {
              var result: any = this.encDecService.dwt(this.session, dec.data);
              this.driverData = result.driver;
            }
            this.loadingIndicator = '';
          });
      });
    this.queryField1.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._walletService.searchForJob(enc_data)
          .subscribe(dec => {
            if (dec && dec.status == 200) {
              var result: any = this.encDecService.dwt(this.session, dec.data);
              this.jobData = result.jobs;
            }
            this.loadingIndicator = '';
          });
      });
  }
  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }

  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }
  searchDriver(data) {
    if (typeof data === 'object') {
      this.selected_driver_id = data._id;
      console.log("driver_id" + this.selected_driver_id);
    }
    else {
      this.selected_driver_id = '';
    }
  }
  displayFnDriver(data): string {
    console.log(data);
    return data ? data.name : data;
  }
  searchJobs(data) {
    if (typeof data === 'object') {
      this.selected_job_id = data._id;
      console.log("job_id" + this.selected_job_id);
    }
    else {
      this.selected_job_id = '';
    }
  }
  displayFnJobs(data): string {
    console.log(data);
    return data ? data.job_id : data;
  }
  checkShiftstartTime() {
    this.selectedMoment1 = "";
  }

  /**
   *  end time timepicker
   */
  checkShiftstartTime1() {
    if (this.selectedMoment1 != '') {
      //this.selectedMoment="";
    }
  }
  getWalletsOfUser() {
    var params = {
      driver_id: this.selected_driver_id,
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._walletService.getWalletDetails(enc_data)
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.walletData = result.walletData;
          console.log(JSON.stringify(this.walletData));
        }
        this.loadingIndicator = '';
      });
  }
  getFinalisationReports() {
    var params = {}
    this.searchSubmit = true;
    if (this.selected_job_id !== '') {
      params = {
        offset: 0,
        limit: this.pageSize,
        job_id: this.selected_job_id,
        start_date: '',
        end_date: ''
      }
    } else if (this.selected_driver_id !== '') {
      params = {
        offset: 0,
        limit: this.pageSize,
        driver_id: this.selected_driver_id,
        start_date: '',
        end_date: ''
      }
    } else {
      params = {
        offset: 0,
        limit: this.pageSize,
        start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
        end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      }
    }
    console.log("params+++++" + JSON.stringify(params));
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._walletService.searchFullJob(enc_data)
      .subscribe(dec => {
        this.searchSubmit = false;
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.jobData = result.jobs;
          if (this.jobData.length > 0) {
            for (var i = 0; i < this.jobData.length; ++i) {
              this.jobData[i].detailed_list = [];
              this.jobData[i].showinner = false;
            }
          }
          console.log("data+++" + JSON.stringify(this.jobData));
          this.jobDataDataLength = result.count;
        }
        this.loadingIndicator = '';
      });
  }
  pagingAgent(data) {
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    let params;
    if (this.selected_job_id !== '') {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        job_id: this.selected_job_id
      }
    } else if (this.selected_driver_id !== '') {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        driver_id: this.selected_driver_id
      }
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
        end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
        sortOrder: 'desc',
        sortByColumn: 'created_at',

      }
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._walletService.searchFullJob(enc_data)
      .subscribe(dec => {
        this.searchSubmit = false;
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.jobData = result.jobs;
          if (this.jobData.length > 0) {
            for (var i = 0; i < this.jobData.length; ++i) {
              this.jobData[i].detailed_list = [];
              this.jobData[i].showinner = false;
            }
          }
          this.jobDataDataLength = result.count;
        }
        this.loadingIndicator = '';
      });
  }
  public selected_status;
  public selected_status2;
  getExpandeddetails(job, status, status1) {
    job.settlementIds = [];
    for (var i = 0; i < this.jobData.length; ++i) {
      this.jobData[i].detailed_list = [];
      delete this.jobData[i].settlementIds;
      delete this.jobData[i].revertIds;
      this.jobData[i].showinner = false;
    }
    this.selected_status = status;
    this.selected_status2 = status1;
    console.log("selected_status+++" + this.selected_status);
    this.searchSubmit1 = true;
    var params = {};
    if (status1 == '') {
      params = {
        job_id: job._id,
        status: status
      }
    } else {
      params = {
        job_id: job._id,
        status: status,
        sub_status: status1
      }
    }
    console.log("params" + JSON.stringify(params));
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._walletService.getJobDetail(enc_data)
      .subscribe(dec => {
        this.searchSubmit1 = false;
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          let index = this.jobData.findIndex(x => x._id === job._id);
          if (index == 0 && this.selected_status == "BANK_PROCESSING") {
            this.settle_button = true;
            this.checkbox_button = true;
            this.bank_button = false;
          } else if (index == 0 && this.selected_status == "UNPAID" && this.selected_status2 == "topay") {
            this.checkbox_button = true;
            this.bank_button = true;
            this.settle_button = false;
          } else {
            this.checkbox_button = false;
            this.bank_button = false;
            this.settle_button = false;
          }
          this.InnerjobData = result.jobs;
          console.log("inner length" + this.InnerjobData.length);
          if (this.InnerjobData.length > 0) {
            this.haveData = true;
            this.jobData[index].showinner = true;
            this.jobData[index].detailed_list = [];
            for (var i = 0; i < this.InnerjobData.length; ++i) {
              this.InnerjobData[i].settle_amount = this.InnerjobData[i].due_amount.$numberDecimal;
              this.jobData[index].detailed_list.push(this.InnerjobData[i]);
            }
            console.log("walletdetails" + this.jobData[index].detailed_list.length)
          } else {
            this.jobData[index].showinner = true;
            this.haveData = false;
            this.jobData[index].detailed_list = [];
          }
        }
        this.loadingIndicator = '';
      });
  }
  reset() {
    this.selected_status = '';
    this.selected_status2 = '';
    this.showexpanded = false;
    this.selected_job_id = '';
    this.selected_driver_id = '';
    this.creatingcsv = false;
    this.settle_button = false;
    this.checkbox_button = false;
    this.bank_button = false;
    this.driver_id = '';
    this.job_id = '';
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
    this.getFinalisationReports();
  }
  public creatingcsv = false;
  public getconstantsforcsv(job, status) {
    if (this.creatingcsv) {
      this.toastr.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      var params = {
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._orderService.getCSVConstantsWallet(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          let offset = res.data;
          this.createCsv(offset.order_fetch_interval, offset.order_fetch_offset, job, status);
        } else {
          this.creatingcsv = false;
          this.toastr.error("Please try again")
        }
      })
    }
  }
  public progressvalue = 0;
  public k = 1;
  public createCsv(interval_value, offset, job, status) {
    // if (this.payment_type_id != undefined && this.payment_type_id != '') {
    //   this.payment_type_id = this.payment_type_id.id;
    // }
    console.log("selected_status+++" + this.selected_status);
    const params1 = {
      job_id: job._id,
      status: this.selected_status,
      sub_status: this.selected_status2,
      bank_excel: status
    };
    this.getCsvCount(params1, interval_value, offset, job, status)
  }
  getCsvCount(params1, interval_value, offset, job, status) {
    console.log("params+++" + JSON.stringify(params1));
    var encrypted1 = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted1,
      email: this.email
    }
    this._walletService.getDueCount(enc_data).subscribe((dec) => {
      var res: any = this.encDecService.dwt(this.session, dec.data);
      if (dec) {
        if (dec.status == 201) {
          this.creatingcsv = false;
          this.toastr.error('Please try again after some time');
        } else if (dec.status == 200) {
          var count = res.count;
          console.log("count++++++++++++++++++++" + count)
          let res1Array = [];
          //alert("total_orders" + count);
          let i = 0;
          let fetch_status = true;
          let that = this;
          let new_params = {
            offset: 0,
            limit: 0,
            sortOrder: 'desc',
            sortByColumn: 'updated_at',
            job_id: job._id,
            status: this.selected_status,
            sub_status: this.selected_status2,
            bank_excel: status
          };
          if (status == "") {
            that.beginCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value)
          } else {
            that.beginBankCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value)
          }
        }
      }
    })
  }
  beginCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value) {
    that.livetrackingOrder = setInterval(function () {
      if (fetch_status) {
        console.log("maincount++++++++++++++++++++++++++" + that.finalcount);
        console.log("in++++++++++++++++++++++++++" + res1Array.length);
        if (that.finalcount >= count) {
          console.log("end++++++++++++++++++++++++");
          that.creatingcsv = false;
          clearInterval(that.livetrackingOrder);
          let labels = [
            'Job id',
            'Driver',
            'Driver Name',
            'Bank Name',
            'Bank Code',
            'Bank',
            'IBAN',
            'Settled Amount',
            'Total Driver revenue',
            'Total Cash in hand',
            'Previous Due Amount',
            'Due Amount (Total Driver revenue - (Total Cash in hand + Previous Due Amount))',
            'Date',
            'status',
            'Total Salik',
            'Total Sharjah',
            'Total cash order',
            'Total online order',
            'Total POS order',
            'Total Discount',
            'Total Fee collected(with RTA)',
            'Total Fee collected(without RTA)',
            'Trip Id',
            'Trip Date',
            'Trip amount',
            'Online',
            'Payment Mode',
            'Discount amount',
            'Driver revenue amount',
            'Fee collected(including RTA)',
            'Fee collected(excluding RTA)',
            'Salik',
            'Sharjah',
            'Customer Paid',
            'Cybersource ref',
            'Auth code',
            'Previous Reco Trip',
            'To pay',
            'Diff'
          ];
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(res1Array, 'Wallet finalisation-' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        } else {
          fetch_status = false;
          new_params.offset = i;
          new_params.limit = parseInt(offset);
          console.log(JSON.stringify(new_params));
          var encrypted2 = that.encDecService.nwt(that.session, new_params);
          var enc_data2 = {
            data: encrypted2,
            email: that.email
          }
          that._walletService.getJobDetailCSV(enc_data2).subscribe((dec) => {
            if (dec && dec.status == 200) {
              var res: any = that.encDecService.dwt(that.session, dec.data);
              console.log("data+=======================================" + res.jobs.length);
              if (res.jobs.length > 0) {
                for (let j = 0; j < res.jobs.length; j++) {
                  console.log("current_processing+================================================" + j);
                  const csvArray = res.jobs[j];
                  //console.log("detailssssss+++++++++++++++"+JSON.stringify(csvArray));
                  let bankcode;
                  let bankexcelcode;
                  if (csvArray.driver_id.account_details == "Emirates NBD" || csvArray.driver_id.account_details == "Emirates Islamic") {
                    bankcode = "TR";
                  } else {
                    bankcode = "OR";
                  }
                  if (csvArray.driver_id.account_details === "Al Masraf") {
                    bankexcelcode = "ABIN";
                  }
                  if (csvArray.driver_id.account_details === "Abu Dhabi Islamic Bank") {
                    bankexcelcode = "ABDI";
                  }
                  if (csvArray.driver_id.account_details === "United Bank Limited (UBL)") {
                    bankexcelcode = "UNIL";
                  }
                  if (csvArray.driver_id.account_details === "Abu Dhabi Commercial Bank") {
                    bankexcelcode = "ADCB";
                  }
                  if (csvArray.driver_id.account_details === "Mashreq") {
                    bankexcelcode = "BOML";
                  }
                  if (csvArray.driver_id.account_details === "National Bank of Ras Al-Khaimah PJSC (RAKBANK)") {
                    bankexcelcode = "NRAK";
                  }
                  if (csvArray.driver_id.account_details === "Dubai Islamic Bank") {
                    bankexcelcode = "DUIB";
                  }
                  if (csvArray.driver_id.account_details === "Emirates Islamic") {
                    bankexcelcode = "MEBL";
                  }
                  if (csvArray.driver_id.account_details === "Emirates NBD") {
                    bankexcelcode = "EBIL";
                  }
                  //console.log("Additional+++++++++++++++++++++" + JSON.stringify(csvArray));
                  csvArray.pendingamount = 0;
                  if (csvArray.order_details.length > 0) {
                    console.log("current=========================orderdetailspresent" + j);
                    that.finalcount = that.finalcount + 1;
                    if (csvArray.previous_unsettled && csvArray.previous_unsettled.length > 0) {
                      for (let l = 0; l < csvArray.previous_unsettled.length; l++) {
                        //console.log("pending+++++++++++++++++++++++++++inlooopbefore");
                        csvArray.pendingamount = parseFloat(csvArray.pendingamount) + (parseFloat(csvArray.previous_unsettled[l].due_amount.$numberDecimal) - parseFloat((csvArray.previous_unsettled[l].settled_amount ? csvArray.previous_unsettled[l].settled_amount.$numberDecimal : "0")))
                        //console.log("pending+++++++++++++++++++++++++++inlooop" + JSON.stringify(csvArray.pendingamount));
                      }
                    }
                    console.log("current=========================orderdetailspresent1" + j);
                    let to_pay = (parseFloat(csvArray.pendingamount) + parseFloat(csvArray.total_online_order) + parseFloat(csvArray.total_discount)) - (parseFloat(csvArray.total_fee_with_without_rta));
                    let diff = to_pay - parseFloat(csvArray.due_amount.$numberDecimal)
                    res1Array.push({
                      job_id: csvArray.job_id.job_id,
                      driver: csvArray.driver_id ? csvArray.driver_id.emp_id : '',
                      driver_name: csvArray.driver_id ? csvArray.driver_id.name : '',
                      bank_details: csvArray.driver_id ? csvArray.driver_id.account_details : '',
                      bank: bankcode,
                      bank_code: bankexcelcode,
                      iban: csvArray.driver_id ? csvArray.driver_id.iban : '',
                      settled_amount: csvArray.settled_amount ? csvArray.settled_amount.$numberDecimal : 0,
                      total_revenue: csvArray.total_revenue ? csvArray.total_revenue : 0,
                      total_cash_in_hand: csvArray.total_cash_in_hand ? csvArray.total_cash_in_hand : 0,
                      previous: csvArray.pendingamount,
                      amount: csvArray.due_amount.$numberDecimal,
                      date: csvArray.created_at ? csvArray.created_at : '',
                      status: csvArray.status,
                      salik_total: csvArray.total_salik,
                      sharjah_total: csvArray.total_sharjah,
                      total_cash_order: csvArray.total_cash_order,
                      total_online_order: csvArray.total_online_order,
                      total_pos_order: csvArray.total_pos_order,
                      total_discount: csvArray.total_discount,
                      total_fee_with_rta: csvArray.total_fee_with_rta,
                      total_fee_with_without_rta: csvArray.total_fee_with_without_rta,
                      trip_id: '',
                      trip_date: '',
                      trip_amount: csvArray.total_trip_amount,
                      online: csvArray.total_online_order,
                      payment_mode: '',
                      discount: csvArray.total_discount,
                      driver_revenue: csvArray.total_revenue ? csvArray.total_revenue : 0,
                      fee_collected: csvArray.total_fee_with_rta,
                      fee_without_rta: csvArray.total_fee_with_without_rta,
                      salik: csvArray.total_salik,
                      sharjah: csvArray.total_sharjah,
                      customer_paid: csvArray.total_customer_paid,
                      cybersource: '',
                      auth_code: '',
                      previous_reco: '',
                      to_pay: to_pay,
                      diff: parseFloat(parseFloat(to_pay.toString()).toFixed(2)) - parseFloat(csvArray.due_amount.$numberDecimal)
                    });
                    for (let k = 0; k < csvArray.order_details.length; k++) {
                      res1Array.push({
                        job_id: '',
                        driver: '',
                        driver_name: '',
                        bank_details: '',
                        bank: '',
                        bank_code: '',
                        iban: '',
                        settled_amount: '',
                        total_revenue: '',
                        total_cash_in_hand: '',
                        previous: '',
                        amount: '',
                        date: '',
                        status: '',
                        salik_total: '',
                        sharjah_total: '',
                        total_cash_order: '',
                        total_online_order: '',
                        total_pos_order: '',
                        total_discount: '',
                        total_fee_with_rta: '',
                        total_fee_with_without_rta: '',
                        trip_id: csvArray.order_details[k].unique_order_id,
                        trip_date: csvArray.order_details[k].trip_date_time,
                        trip_amount: csvArray.order_details[k].trip_amount.$numberDecimal,
                        online: csvArray.order_details[k].online,
                        payment_mode: csvArray.order_details[k].payment_mode,
                        discount: csvArray.order_details[k].discount_amount,
                        driver_revenue: csvArray.order_details[k].driver_revenue.$numberDecimal,
                        fee_collected: csvArray.order_details[k].fee_collected,
                        fee_without_rta: csvArray.order_details[k].fee_without_rta,
                        salik: csvArray.order_details[k].salik_amount,
                        sharjah: csvArray.order_details[k].sharjah_amount,
                        customer_paid: csvArray.order_details[k].customer_paid,
                        cybersource: csvArray.order_details[k].cybersource_ref ? csvArray.order_details[k].cybersource_ref : '',
                        authcode: csvArray.order_details[k].auth_code ? csvArray.order_details[k].auth_code : '',
                        previous_reco: csvArray.order_details[k].previous ? csvArray.order_details[k].previous : "FALSE",
                        to_pay: '',
                        diff: '',
                      });
                    }
                    for (let r = 0; r < csvArray.previous_order_details.length; r++) {
                      console.log("in");
                      res1Array.push({
                        job_id: "Previous splitup Amount",
                        driver: '',
                        driver_name: '',
                        bank_details: '',
                        bank: '',
                        bank_code: '',
                        iban: '',
                        settled_amount: '',
                        total_revenue: "",
                        total_cash_in_hand: "",
                        previous: '',
                        amount: '',
                        date: '',
                        status: '',
                        salik_total: '',
                        sharjah_total: '',
                        total_cash_order: '',
                        total_online_order: '',
                        total_pos_order: '',
                        total_discount: '',
                        total_fee_with_rta: '',
                        total_fee_with_without_rta: '',
                        trip_id: csvArray.previous_order_details[r].unique_order_id,
                        trip_date: csvArray.order_details[r].trip_date_time,
                        trip_amount: csvArray.previous_order_details[r].trip_amount.$numberDecimal,
                        online: csvArray.previous_order_details[r].online,
                        payment_mode: csvArray.previous_order_details[r].payment_mode,
                        discount: csvArray.previous_order_details[r].discount_amount,
                        driver_revenue: csvArray.previous_order_details[r].driver_revenue.$numberDecimal,
                        fee_collected: csvArray.previous_order_details[r].fee_collected,
                        fee_without_rta: csvArray.previous_order_details[r].fee_without_rta,
                        salik: csvArray.previous_order_details[r].salik_amount,
                        sharjah: csvArray.previous_order_details[r].sharjah_amount,
                        customer_paid: csvArray.previous_order_details[r].customer_paid,
                        cybersource: csvArray.previous_order_details[r].cybersource_ref ? csvArray.previous_order_details[r].cybersource_ref : '',
                        authcode: csvArray.order_details[r].auth_code ? csvArray.order_details[r].auth_code : '',
                        previous_reco: csvArray.order_details[r].previous ? csvArray.order_details[r].previous : "FALSE",
                        to_pay: '',
                        diff: '',
                      });
                    }
                    console.log("current=========================orderdetailspresent5" + j);
                  } else {
                    console.log("current=========================orderdetailsnotpresent" + j);
                    that.finalcount = that.finalcount + 1;
                    if (csvArray.previous_unsettled && csvArray.previous_unsettled.length > 0) {
                      for (let l = 0; l < csvArray.previous_unsettled.length; l++) {
                        csvArray.pendingamount = parseFloat(csvArray.pendingamount) + (parseFloat(csvArray.previous_unsettled[l].due_amount.$numberDecimal)) - (parseFloat((csvArray.previous_unsettled[l].settled_amount ? csvArray.previous_unsettled[l].settled_amount.$numberDecimal : "0")))
                      }
                    }
                    let to_pay = parseFloat(csvArray.pendingamount);
                    let diff = to_pay - parseFloat(csvArray.due_amount.$numberDecimal)
                    res1Array.push({
                      job_id: csvArray.job_id.job_id,
                      driver: csvArray.driver_id ? csvArray.driver_id.emp_id : '',
                      driver_name: csvArray.driver_id ? csvArray.driver_id.name : '',
                      bank_details: csvArray.driver_id ? csvArray.driver_id.account_details : '',
                      bank: bankcode,
                      bank_code: bankexcelcode,
                      iban: csvArray.driver_id ? csvArray.driver_id.iban : '',
                      settled_amount: csvArray.settled_amount ? csvArray.settled_amount.$numberDecimal : 0,
                      total_revenue: csvArray.total_revenue ? csvArray.total_revenue : 0,
                      total_cash_in_hand: csvArray.total_cash_in_hand ? csvArray.total_cash_in_hand : 0,
                      previous: csvArray.pendingamount,
                      amount: csvArray.due_amount.$numberDecimal,
                      date: csvArray.created_at ? csvArray.created_at : '',
                      status: csvArray.status,
                      salik_total: '',
                      sharjah_total: '',
                      total_cash_order: '',
                      total_online_order: '',
                      total_pos_order: '',
                      total_discount: '',
                      total_fee_with_rta: '',
                      total_fee_with_without_rta: '',
                      trip_id: '',
                      trip_date: '',
                      trip_amount: '',
                      online: '',
                      payment_mode: '',
                      discount: '',
                      driver_revenue: '',
                      fee_collected: '',
                      fee_without_rta: '',
                      salik: '',
                      sharjah: '',
                      customer_paid: '',
                      cybersource: '',
                      auth_code: '',
                      to_pay: to_pay,
                      previous_reco: '',
                      diff: parseFloat(parseFloat(to_pay.toString()).toFixed(2)) - parseFloat(csvArray.due_amount.$numberDecimal)
                    });
                    for (let f = 0; f < csvArray.previous_order_details.length; f++) {
                      res1Array.push({
                        job_id: "Previous splitup Amount",
                        driver: '',
                        driver_name: '',
                        bank_details: '',
                        bank: '',
                        bank_code: '',
                        iban: '',
                        settled_amount: '',
                        total_revenue: "",
                        total_cash_in_hand: "",
                        previous: '',
                        amount: '',
                        date: '',
                        status: '',
                        salik_total: '',
                        sharjah_total: '',
                        total_cash_order: '',
                        total_online_order: '',
                        total_pos_order: '',
                        total_discount: '',
                        total_fee_with_rta: '',
                        total_fee_with_without_rta: '',
                        trip_id: csvArray.previous_order_details[f].unique_order_id,
                        trip_date: csvArray.order_details[f].trip_date_time,
                        trip_amount: csvArray.previous_order_details[f].trip_amount.$numberDecimal,
                        online: csvArray.previous_order_details[f].online,
                        payment_mode: csvArray.previous_order_details[f].payment_mode,
                        discount: csvArray.previous_order_details[f].discount_amount,
                        driver_revenue: csvArray.previous_order_details[f].driver_revenue.$numberDecimal,
                        fee_collected: csvArray.previous_order_details[f].fee_collected,
                        fee_without_rta: csvArray.previous_order_details[f].fee_without_rta,
                        salik: csvArray.previous_order_details[f].salik_amount,
                        sharjah: csvArray.previous_order_details[f].sharjah_amount,
                        customer_paid: csvArray.previous_order_details[f].customer_paid,
                        cybersource: csvArray.previous_order_details[f].cybersource_ref ? csvArray.previous_order_details[f].cybersource_ref : '',
                        authcode: csvArray.order_details[f].auth_code ? csvArray.order_details[f].auth_code : '',
                        previous_reco: csvArray.order_details[f].previous ? csvArray.order_details[f].previous : "FALSE",
                        to_pay: '',
                        diff: '',
                      });
                    }
                  }
                  if (j == res.jobs.length - 1) {
                    console.log("endinggggggggggggggggggggggggggggggggggggggggggggg");
                    i = i + parseInt(offset);
                    fetch_status = true;
                  }
                }
              } else {
                that.creatingcsv = false;
                fetch_status = true;
              }
            } else {
              console.log(dec);
              that.toastr.warning('Network reset, csv creation restarted')
              that.createCsv(interval_value, offset);
              res1Array.pop();
              fetch_status = true;
            }
          })
        }
      }
    }, parseInt(interval_value));
  }
  beginBankCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value) {
    that.livetrackingBankOrder = setInterval(function () {
      if (fetch_status) {
        console.log("in++++++++++++++" + res1Array.length);
        if (res1Array.length >= count) {
          console.log("end");
          that.creatingcsv = false;
          clearInterval(that.livetrackingBankOrder);
          let labels = [
            'Bank',
            'Beneficiary Name',
            'Account Currency',
            'IBAN',
            'Bank Code',
            'Bank Name',
            'Description',
            'Driver ID',
            'Amount',
          ];
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(res1Array, 'Bank export-' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        } else {
          fetch_status = false;
          new_params.offset = i;
          new_params.limit = parseInt(offset);
          console.log(JSON.stringify(new_params));
          var encrypted2 = that.encDecService.nwt(that.session, new_params);
          var enc_data2 = {
            data: encrypted2,
            email: that.email
          }
          that._walletService.getJobDetailCSV(enc_data2).subscribe((dec) => {
            if (dec && dec.status == 200) {
              var res: any = that.encDecService.dwt(that.session, dec.data);
              console.log("data+++++" + res.jobs.length)
              if (res.jobs.length > 0) {
                for (let j = 0; j < res.jobs.length; j++) {
                  const csvArray = res.jobs[j];
                  let bankcode;
                  let bankexcelcode;
                  if (csvArray.driver_id.account_details == "Emirates NBD" || csvArray.driver_id.account_details == "Emirates Islamic") {
                    bankcode = "TR";
                  } else {
                    bankcode = "OR";
                  }
                  if (csvArray.driver_id.account_details === "Abu Dhabi Islamic Bank") {
                    bankexcelcode = "ABDI";
                  }
                  if (csvArray.driver_id.account_details === "United Bank Limited (UBL)") {
                    bankexcelcode = "UNIL";
                  }
                  if (csvArray.driver_id.account_details === "Abu Dhabi Commercial Bank") {
                    bankexcelcode = "ADCB";
                  }
                  if (csvArray.driver_id.account_details === "Mashreq") {
                    bankexcelcode = "BOML";
                  }
                  if (csvArray.driver_id.account_details === "National Bank of Ras Al-Khaimah PJSC (RAKBANK)") {
                    bankexcelcode = "NRAK";
                  }
                  if (csvArray.driver_id.account_details === "Dubai Islamic Bank") {
                    bankexcelcode = "DUIB";
                  }
                  if (csvArray.driver_id.account_details === "Emirates Islamic") {
                    bankexcelcode = "MEBL";
                  }
                  if (csvArray.driver_id.account_details === "Emirates NBD") {
                    bankexcelcode = "EBIL";
                  }
                  res1Array.push({
                    bank: bankcode,
                    name: csvArray.driver_id ? csvArray.driver_id.display_name : '',
                    currency: "AED",
                    iban: csvArray.driver_id ? csvArray.driver_id.iban : '',
                    bank_code: bankexcelcode,
                    bank_details: csvArray.driver_id ? csvArray.driver_id.account_details : '',
                    des: 'SALARY BY ORDER DTC-RTA. ID#',
                    driver_id: csvArray.driver_id ? csvArray.driver_id.emp_id : '',
                    amount: csvArray.due_amount ? csvArray.due_amount.$numberDecimal : 0
                  });
                  if (j == res.jobs.length - 1) {
                    i = i + parseInt(offset);
                    fetch_status = true;
                  }
                }
              } else {
                that.creatingcsv = false;
                fetch_status = true;
              }
            } else {
              console.log(dec);
              that.toastr.warning('Network reset, csv creation restarted')
              that.createCsv(interval_value, offset);
              res1Array.pop();
              fetch_status = true;
            }
          })
        }
      }
    }, parseInt(interval_value));
  }
  updateSelectedrevertIds(event, job) {
    console.log("log+++++++++++" + JSON.stringify(job));
    let index = this.jobData.findIndex(x => x._id === job);
    console.log("log+++++++++++" + JSON.stringify(this.jobData[index].revertIds));
    console.log("data++++++++++++++++++++++++" + event.target.name);
    if (event.target.checked) {
      if (this.jobData[index].revertIds == undefined) {
        if (this.jobData[index].settlementIds == undefined) {
          console.log("in no data");
          this.jobData[index].revertIds = [];
          this.jobData[index].revertIds.push(event.target.name);
        }
        else if (this.jobData[index].settlementIds.indexOf(event.target.name) < 0) {
          console.log("in no data2");
          this.jobData[index].revertIds = [];
          this.jobData[index].revertIds.push(event.target.name);
        } else {
          this.toastr.error("only one operation is permitted either revert or settle")
        }
      }
      else if (this.jobData[index].revertIds.indexOf(event.target.name) < 0) {
        console.log("in no data2");
        if (this.jobData[index].settlementIds == undefined) {
          console.log("in no data");
          this.jobData[index].revertIds.push(event.target.name);
        }
        else if (this.jobData[index].settlementIds.indexOf(event.target.name) < 0) {
          console.log("in no data2");
          this.jobData[index].revertIds.push(event.target.name);
        } else {
          this.toastr.error("only one operation is permitted either revert or settle")
        }
      }
    } else {
      if (this.jobData[index].revertIds.indexOf(event.target.name) > -1) {
        this.jobData[index].revertIds.splice(this.jobData[index].revertIds.indexOf(event.target.name), 1);
      }
    }
    console.log("IDs:+++ " + JSON.stringify(this.jobData[index].revertIds));
  }
  updateSelectedsettlementIds(event, job) {
    console.log("data++++++++++++++++++++++++" + event.target.name);
    console.log("log+++++++++++" + JSON.stringify(job));
    let index = this.jobData.findIndex(x => x._id === job);
    console.log("log+++++++++++" + JSON.stringify(this.jobData[index].settlementIds));
    console.log("log+++++++++++" + JSON.stringify(this.jobData[index].revertIds));

    if (event.target.checked) {
      if (this.jobData[index].settlementIds == undefined) {
        console.log("in no data");
        if (this.jobData[index].revertIds == undefined) {
          console.log("in no data");
          this.jobData[index].settlementIds = [];
          this.jobData[index].settlementIds.push(event.target.name);
        }
        else if (this.jobData[index].revertIds.indexOf(event.target.name) < 0) {
          console.log("in no data2");
          this.jobData[index].settlementIds = [];
          this.jobData[index].settlementIds.push(event.target.name);
        } else {
          this.toastr.error("only one operation is permitted either revert or settle")
        }
      }
      else if (this.jobData[index].settlementIds.indexOf(event.target.name) < 0) {
        console.log("in no data2");
        if (this.jobData[index].revertIds == undefined) {
          console.log("in no data");
          this.jobData[index].settlementIds.push(event.target.name);
        }
        else if (this.jobData[index].revertIds.indexOf(event.target.name) < 0) {
          console.log("in no data2");
          this.jobData[index].settlementIds.push(event.target.name);
        } else {
          this.toastr.error("only one operation is permitted either revert or settle")
        }
      }
    } else {
      if (this.jobData[index].settlementIds.indexOf(event.target.name) > -1) {
        this.jobData[index].settlementIds.splice(this.jobData[index].settlementIds.indexOf(event.target.name), 1);
      }
    }
    console.log("IDs:+++ " + JSON.stringify(this.jobData[index].settlementIds));
  }
  revert(job) {
    let allow = true;
    var user = JSON.parse(window.localStorage['adminUser']);
    if (!job.revertIds || job.revertIds.length == 0) {
      this.toastr.warning("Please select drivers to revert");
    } else {
      if (!allow) {

      } else {
        allow = false;
        let new_params = {
          job_id: job._id,
          driver_ids: [],
          revert_ids: job.revertIds ? job.revertIds : [],
          driver_ids_with_amount: [],
          bank_reference: this.bank_reference_number,
          status: "settle",
          user_id: user._id
        }
        var encrypted2 = this.encDecService.nwt(this.session, new_params);
        var enc_data2 = {
          data: encrypted2,
          email: this.email
        }
        this._walletService.settleDrivers(enc_data2).subscribe((dec) => {
          if (dec && dec.status == 200) {
            console.log("success");
            allow = true;
            this.getExpandeddetails(job, this.selected_status, this.selected_status2);
          } else {
            allow = true;
            this.toastr.error("Operation Failed");
          }
        })
      }
    }
  }
  settleDrivers(job) {
    let allow = true;
    var user = JSON.parse(window.localStorage['adminUser']);
    if ((!job.settlementIds || job.settlementIds.length == 0)) {
      this.toastr.warning("Please select drivers to settle");
    } else if (!this.bank_reference_number && (job.settlementIds && job.settlementIds.length > 0)) {
      this.toastr.warning("Please Provide bank reference number")
    } else {
      let driver_ids = [];
      console.log("driverids++++++++++++++" + JSON.stringify(job.settlementIds));
      for (var i = 0; i < job.settlementIds.length; ++i) {
        let index = job.detailed_list.findIndex(x => x.driver_id._id === job.settlementIds[i]);
        if (index > -1) {
          if (!job.detailed_list[index].settle_amount) {
            this.toastr.warning("Amount Feild is mandatory");
            allow = false;
          } else {
            if (driver_ids.findIndex(x => x.driver_id === job.settlementIds[i]) > 0) {
              console.log("already added");
              let ind = driver_ids.findIndex(x => x.driver_id === job.settlementIds[i]);
              driver_ids[ind].settle_amount = job.detailed_list[index].settle_amount;
            } else {
              driver_ids.push({ driver_id: job.settlementIds[i], settle_amount: job.detailed_list[index].settle_amount })
            }
          }
        }
      }
      console.log("final" + JSON.stringify(driver_ids))
      if (!allow) {

      } else {
        allow = false;
        let new_params = {
          job_id: job._id,
          driver_ids: job.settlementIds ? job.settlementIds : [],
          revert_ids: [],
          driver_ids_with_amount: driver_ids,
          bank_reference: this.bank_reference_number,
          status: "settle",
          user_id: user._id
        }
        var encrypted2 = this.encDecService.nwt(this.session, new_params);
        var enc_data2 = {
          data: encrypted2,
          email: this.email
        }
        this._walletService.settleDrivers(enc_data2).subscribe((dec) => {
          if (dec && dec.status == 200) {
            console.log("success");
            allow = true;
            this.getExpandeddetails(job, this.selected_status, this.selected_status2);
          } else {
            allow = true;
            this.toastr.error("Operation Failed");
          }
        })
      }
    }
  }
  selectAlltoBank(job) {
    console.log(JSON.stringify(job));
    let index = this.jobData.findIndex(x => x._id === job._id);
    console.log("index" + index);
    if (this.selectedAll) {
      console.log("select all");
      this.jobData[index].settlementIds = [];
      for (var i = 0; i < this.jobData[index].detailed_list.length; ++i) {
        this.jobData[index].settlementIds.push(this.jobData[index].detailed_list[i].driver_id._id);
      }
    } else {
      this.jobData[index].settlementIds = [];
    }
    console.log("final result===========" + JSON.stringify(this.jobData[index].settlementIds));
  }
  AddAlltoBank(job) {
    console.log(JSON.stringify(job));
    let index = this.jobData.findIndex(x => x._id === job._id);
    console.log("index" + index);
    console.log("log+++++++++++" + JSON.stringify(this.jobData[index].settlementIds));
    this.jobData[index].settlementIds = [];
    for (var i = 0; i < this.jobData[index].detailed_list.length; ++i) {
      this.jobData[index].settlementIds.push(this.jobData[index].detailed_list[i].driver_id._id);
    }
    console.log("log+++++++++++" + JSON.stringify(this.jobData[index].settlementIds));
    let allow = true;
    var user = JSON.parse(window.localStorage['adminUser']);
    console.log("user" + JSON.stringify(user));
    console.log("settle++++" + JSON.stringify(job.settlementIds));
    if (!job.settlementIds || job.settlementIds.length == 0) {
      this.toastr.warning("Please select drivers");
    } else {
      if (!allow) {

      } else {
        allow = false;
        console.log("driverids++++++++++++++" + JSON.stringify(job.settlementIds));
        let new_params = {
          job_id: this.jobData[index]._id,
          driver_ids: this.jobData[index].settlementIds,
          status: "bank",
          user_id: user._id
        }
        var encrypted2 = this.encDecService.nwt(this.session, new_params);
        var enc_data2 = {
          data: encrypted2,
          email: this.email
        }
        this._walletService.settleDrivers(enc_data2).subscribe((dec) => {
          if (dec && dec.status == 200) {
            console.log("success");
            allow = true;
            this.getExpandeddetails(job, this.selected_status, this.selected_status2);
          } else {
            allow = true;
            this.toastr.error("Operation Failed");
          }
        })
      }
    }
  }
  changeDriverstoBank(job) {
    let allow = true;
    var user = JSON.parse(window.localStorage['adminUser']);
    console.log("user" + JSON.stringify(user));
    console.log("settle++++" + JSON.stringify(job.settlementIds));
    if (!job.settlementIds || job.settlementIds.length == 0) {
      this.toastr.warning("Please select drivers");
    } else {
      if (!allow) {

      } else {
        allow = false;
        console.log("driverids++++++++++++++" + JSON.stringify(job.settlementIds));
        let new_params = {
          job_id: job._id,
          driver_ids: job.settlementIds,
          status: "bank",
          user_id: user._id
        }
        var encrypted2 = this.encDecService.nwt(this.session, new_params);
        var enc_data2 = {
          data: encrypted2,
          email: this.email
        }
        this._walletService.settleDrivers(enc_data2).subscribe((dec) => {
          if (dec && dec.status == 200) {
            console.log("success");
            allow = true;
            this.getExpandeddetails(job, this.selected_status, this.selected_status2);
          } else {
            allow = true;
            this.toastr.error("Operation Failed");
          }
        })
      }
    }
  }
  getSplitupData(job, innerjob) {
    if (this.selected_status2 == 'topay') {
      console.log("jobdata++++++" + JSON.stringify(job));
      console.log("innerjob++++" + JSON.stringify(innerjob));
      let new_params = {
        job_id: job._id,
        driver_id: innerjob.driver_id._id,
        start_date: job.report_start_date,
        end_date: job.report_end_date,
      }
      var encrypted2 = this.encDecService.nwt(this.session, new_params);
      var enc_data2 = {
        data: encrypted2,
        email: this.email
      }
      this._walletService.getsplitupData(enc_data2).subscribe((dec) => {
        //console.log("Response++++" + JSON.stringify(dec));
        if (dec && dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          console.log("orderslen++++" + res.orderdata.length)
          let that = this;
          this.dialog.closeAll();
          let data = {
            orderData: res.orderdata,
            jobData: innerjob
          }
          let dialogRef = this.dialog.open(SplitupdataComponent, {
            maxWidth: '90vw',
            maxHeight: '90vh',
            hasBackdrop: true,
            data: data,
            //  scrollStrategy: this.overlay.scrollStrategies.noop()
          });
        } else {
          this.toastr.error(dec.message);
        }
      })
    }
  }

  selectAlltoSettle(job) {
    console.log(JSON.stringify(job));
    let index = this.jobData.findIndex(x => x._id === job._id);
    console.log("index" + index);
    if (this.selectedAll) {
      console.log("select all");
      this.jobData[index].settlementIds = [];
      for (var i = 0; i < this.jobData[index].detailed_list.length; ++i) {
        this.jobData[index].settlementIds.push(this.jobData[index].detailed_list[i].driver_id._id);
      }
    } else {
      this.jobData[index].settlementIds = [];
    }
    console.log("final result===========" + JSON.stringify(this.jobData[index].settlementIds));
  }

}
