import { Component, OnInit,  ViewEncapsulation  } from '@angular/core';
import { Compiler } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class AdminComponent implements OnInit {

  constructor(private _compiler: Compiler) { }

  ngOnInit() {
    console.log("clear cache");
    this._compiler.clearCache();
  }

}
