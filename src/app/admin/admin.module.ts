import { ShukranRestService } from './../common/services/shukran/shukran-rest.service';
import { ShukranService } from './../common/services/shukran/shukran.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NguiMapModule } from '@ngui/map';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminComponent } from './admin.component';
import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from '../app-routing/app-routing.module';
import { HeaderComponent } from './layouts/header/header.component';
import { LoginComponent } from './login/login.component';
import { BrowserModule } from '@angular/platform-browser';
import { UsersService } from '../common/services/user/user.service';
import { UsersRestService } from '../common/services/user/userrest.service';
import { ApiService } from '../common/services/api/api.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { JwtService } from '../common/services/api/jwt.service';
import { AdminService } from '../common/services/admin/admin.service';
import { ProfileComponent } from './profile/profile.component';
import { ChangeProfileComponent } from './change-profile/change-profile.component';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OrderModule } from './order/order.module';
import { AdministrationModule } from './administration/administration.module';
import { FleetmanamentModule } from './fleetmanagement/fleetmanament.module';
import { ShiftmanagementModule } from './shiftmanagement/shiftmanagement.module';
import { CustomerModule } from './customer/customer.module';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { NgDatepickerModule } from 'ng2-datepicker';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule, MatNativeDateModule, MatCardModule, MatSelectModule, MatFormField } from '@angular/material';
import { environment } from '../../environments/environment';
import { AccessControlService } from '../common/services/access-control/access-control.service';
import { GlobalProvider } from "./global";
import { PasswordValidation } from "./password-validation";
import { AclAuthervice } from "./../common/services/access-control/acl-auth.service";
import { AclUrlCheckService } from "./../common/services/access-control/acl-url-check.service"
import { RoleManagementModule } from "./role-management/role-management.module";
import { ToastModule } from 'ng2-toastr';
import { administrationRoutes } from '../admin/administration/administration-routing.module'
import { UserlogService } from "../common/services/userlog/userlog.service";
import { AclFormComponent } from './acl-form/acl-form.component';
import { ImageUploadModule } from 'angular2-image-upload';
import { ZoneRestService } from '../common/services/zones/zonerest.service';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { ProgressBarModule } from "angular-progress-bar"
import { HighchartsChartModule } from 'highcharts-angular';
import { MarketingModule } from './marketing/marketing.module';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { WalletModule } from './wallet/wallet.module';
import { FinanceModule } from './finance/finance.module';
import { PerformanceMonitoringModule } from './performance-monitoring/performance-monitoring.module';
const config: SocketIoConfig = { url: environment.socketUrl, options: {} };
@NgModule({
  imports: [
    CommonModule,
    WalletModule,
    FinanceModule,
    MatInputModule,
    MatNativeDateModule,
    AppRoutingModule,
    OrderModule,
    FleetmanamentModule,
    ShiftmanagementModule,
    FormsModule,
    BrowserModule,
    CustomerModule,
    ReactiveFormsModule,
    AdministrationModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpModule,
    HighchartsChartModule,
    FlashMessagesModule.forRoot(),
    HttpClientModule,
    RouterModule,
    MatCardModule,
    MatAutocompleteModule,
    NgDatepickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ToastModule.forRoot(),
    RoleManagementModule,
    ImageUploadModule,
    ProgressBarModule,
    MarketingModule,
    SocketIoModule.forRoot(config),
    NguiMapModule.forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.map_key + '&libraries=visualization,places,drawing' }),
    MatSelectModule,
    RoundProgressModule,
    PerformanceMonitoringModule
  ],
  declarations: [
    AdminComponent,
    SidebarComponent,
    DashboardComponent,
    HeaderComponent,
    LoginComponent,
    ProfileComponent,
    ChangeProfileComponent,
    AclFormComponent,
  ],
  providers: [
    UsersService,
    UsersRestService,
    ApiService,
    JwtService,
    AdminService,
    AccessControlService,
    GlobalProvider,
    PasswordValidation,
    AclAuthervice,
    AclUrlCheckService,
    UserlogService,
    ZoneRestService,
    ShukranService,
    ShukranRestService
  ],
  exports: [
    MatCardModule
  ]

})
export class AdminModule { }
