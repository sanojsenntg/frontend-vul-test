import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FinanceServicesService } from '../../../common/services/finance/finance-services.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import * as moment from 'moment/moment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { CompanyService } from '../../../common/services/companies/companies.service';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { Router } from '@angular/router';
import { OrderService } from '../../../common/services/order/order.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
@Component({
  selector: 'app-finance-list',
  templateUrl: './finance-list.component.html',
  styleUrls: ['./finance-list.component.css']
})
export class FinanceListComponent implements OnInit {
  public companyIdFilter;
  public jobData = [];
  public tripData = [];
  public detailed_list_len = 0;
  public detailed_list = [];
  public trip_total = "NA";
  public selected_job;
  public revenue_total = "NA";
  public walletDataLength = 0;
  public companyData = [];
  public start_date;
  public pNo = 1;
  public end_date;
  public open_filer = false;
  public companyId: any = [];
  public type_value = 'S';
  pageSize = 10;
  pageNo = 0;
  public is_search = false;
  public jobexists = false;
  public detailed_list_selected = false;
  page = 1;
  email: string;
  session: string;
  public showLoader = true;
  public selectedMoment: any = '';
  public selectedMoment1: any = '';
  public company_id;
  public dtc = false;
  public uaeTime = new Date().toDateString();
  public min;
  public max = new Date().toDateString();
  constructor(private _financeService: FinanceServicesService,
    private router: Router,
    public jwtService: JwtService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    public _orderService: OrderService,
    private _companyservice: CompanyService,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.getCompanies();
    this.company_id = localStorage.getItem('user_company');
    if (this.company_id === '5ce12918aca1bb08d73ca25d' || this.company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm'));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    this.getLastReco();
  }
  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }

  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }
  public getLastReco() {
    let that = this;
    console.log("calling+++" + this.type_value);
    this.showLoader = false;
    const params = {
      company_id: this.companyId,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
    }
    this._financeService.getLastreco(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        console.log(JSON.stringify(data));
        if (data.jobs.length > 0) {
          this.jobexists = true;
          this.jobData = data.jobs;
          this.start_date = this.jobData[0].report_start_date;
          this.end_date = this.jobData[this.jobData.length - 1].report_end_date;
          this.selectedMoment = this.getDate(moment(this.start_date).subtract(4, 'hours').format('YYYY-MM-DD HH:mm:ss'));
          this.selectedMoment1 = this.getDate(moment(this.end_date).subtract(4, 'hours').format('YYYY-MM-DD HH:mm:ss'));
          const params1 = {
            company_id: this.companyId,
            job: data.jobs
          };
          var encrypted1 = this.encDecService.nwt(this.session, params1);
          var enc_data1 = {
            data: encrypted1,
            email: this.email,
          }
          that._financeService.getTripsRevenue(enc_data1).subscribe((dec) => {
            that.showLoader = true;
            if (dec && dec.status == 200) {
              var data: any = this.encDecService.dwt(this.session, dec.data);
              console.log(JSON.stringify(data));
              if (data.data.length > 0) {
                that.tripData = data.data;
                that.trip_total = that.tripData[0].total_trips;
                that.revenue_total = that.tripData[0].driver_revenue.$numberDecimal + " AED";
              } else {
                that.trip_total = "0.00 AED";
                that.revenue_total = "0.00 AED";
              }
            } else {
              that.trip_total = "NA";
              that.revenue_total = "NA";
              that.toastr.error(dec.message);
            }
          });
        } else {
          this.showLoader = true;
          this.jobexists = false;
        }
      } else {
        this.showLoader = true;
        this.jobexists = false;
        this.toastr.error(dec.message);
      }
    });
  }
  public getDetailedList(job) {
    this.selected_job = job;
    console.log("data++++" + JSON.stringify(job));
    this.showLoader = false;
    let that = this;
    const params = {
      company_id: this.companyId,
      job: job,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
    }
    this._financeService.getRecoCount(enc_data).subscribe((dec) => {
      //this.showLoader = true;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        console.log("countData+++" + JSON.stringify(data));
        //this.detailed_list_len=
        if (data.jobs && data.jobs.length > 0) {
          this.detailed_list_len = data.jobs[0].count;
          console.log(this.detailed_list_len);
        }
        const params1 = {
          company_id: this.companyId,
          job: job,
          limit: 10,
          offset: 0
        };
        var encrypted1 = this.encDecService.nwt(this.session, params1);
        var enc_data1 = {
          data: encrypted1,
          email: this.email,
        }
        that._financeService.getRecoDetails(enc_data1).subscribe((dec) => {
          that.showLoader = true;
          if (dec && dec.status == 200) {
            that.detailed_list_selected = true;
            var data: any = this.encDecService.dwt(that.session, dec.data);
            //console.log("job+++" + JSON.stringify(data));
            if (data.jobs && data.jobs.length > 0) {
              that.detailed_list = data.jobs;
            }
            //this.detailed_list=
          } else {
            that.toastr.error(dec.message)
          }
        })
      } else {
        this.toastr.error(dec.message)
      }
    })
  }
  pagingAgent(data) {
    this.showLoader = false;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      company_id: this.companyId,
      job: this.selected_job
    }
    var encrypted1 = this.encDecService.nwt(this.session, params);
    var enc_data1 = {
      data: encrypted1,
      email: this.email,
    }
    this._financeService.getRecoDetails(enc_data1).subscribe((dec) => {
      this.showLoader = true;
      if (dec && dec.status == 200) {
        this.detailed_list_selected = true;
        var data: any = this.encDecService.dwt(this.session, dec.data);
        console.log("job+++" + JSON.stringify(data));
        if (data.jobs && data.jobs.length > 0) {
          this.detailed_list = data.jobs;
        }
        //this.detailed_list=
      } else {
        this.toastr.error(dec.message)
      }
    })
  }
  reset() {
    this.selected_job = "";
    this.detailed_list = [];
    this.detailed_list_len = 0;
    this.showLoader = false;
    this.jobexists = false;
    let company_id = localStorage.getItem('user_company');
    this.companyId = [];
    this.companyId.push(company_id);
    this.getLastReco();
  }
  public searchReco() {
    let that = this;
    console.log("calling+++" + this.type_value);
    this.showLoader = false;
    const params = {
      company_id: this.companyId,
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
    };
    console.log("paramas++++++++++++" + JSON.stringify(params));
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
    }
    this._financeService.searchReco(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        console.log(JSON.stringify(data));
        if (data.jobs.length > 0) {
          this.jobexists = true;
          this.jobData = data.jobs;
          //this.start_date = this.jobData[0].report_start_date;
          //this.end_date = this.jobData[this.jobData.length - 1].report_end_date;
          //this.selectedMoment = this.getDate(moment(this.start_date).subtract(4, 'hours').format('YYYY-MM-DD HH:mm:ss'));
          //this.selectedMoment1 = this.getDate(moment(this.end_date).subtract(4, 'hours').format('YYYY-MM-DD HH:mm:ss'));
          const params1 = {
            company_id: this.companyId,
            job: data.jobs
          };
          var encrypted1 = this.encDecService.nwt(this.session, params1);
          var enc_data1 = {
            data: encrypted1,
            email: this.email,
          }
          that._financeService.getTripsRevenue(enc_data1).subscribe((dec) => {
            that.showLoader = true;
            if (dec && dec.status == 200) {
              var data: any = this.encDecService.dwt(this.session, dec.data);
              console.log(JSON.stringify(data));
              if (data.data.length > 0) {
                that.tripData = data.data;
                that.trip_total = that.tripData[0].total_trips;
                that.revenue_total = that.tripData[0].driver_revenue.$numberDecimal + " AED";
              } else {
                that.trip_total = "0.00 AED";
                that.revenue_total = "0.00 AED";
              }
            } else {
              that.trip_total = "NA";
              that.revenue_total = "NA";
              that.toastr.error(dec.message);
            }
          });
        } else {
          this.showLoader = true;
          this.jobexists = false;
        }
      } else {
        this.showLoader = true;
        this.jobexists = false;
        this.toastr.error(dec.message);
      }
    });
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
        }
      }
    });
  }
  changeCompany(company) {
    this.companyId = [];
    this.companyId.push(this.companyIdFilter);
  }
  public creatingcsv = false;
  getconstantsforcsv(job) {
    if (this.creatingcsv) {
      this.toastr.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      var params = {
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._orderService.getCSVConstantsWallet(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          let offset = res.data;
          this.createCsv(offset.order_fetch_interval, offset.order_fetch_offset, job);
        } else {
          this.creatingcsv = false;
          this.toastr.error("Please try again")
        }
      })
    }
  }
  public progressvalue = 0;
  public k = 1;
  public createCsv(interval_value, offset, job) {
    // if (this.payment_type_id != undefined && this.payment_type_id != '') {
    //   this.payment_type_id = this.payment_type_id.id;
    // }
    const params = {
      company_id: this.companyId,
      job: job,
    };
    this.getCsvCount(params, interval_value, offset, job)
  }
  getCsvCount(params1, interval_value, offset, job) {
    console.log("params+++" + JSON.stringify(params1));
    var encrypted1 = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted1,
      email: this.email
    }
    this._financeService.getRecoCount(enc_data).subscribe((dec) => {
      //this.showLoader = true;
      if (dec) {
        if (dec.status == 201) {
          this.creatingcsv = false;
          this.toastr.error('Please try again after some time');
        } else if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          console.log("countData+++" + JSON.stringify(data));
          //this.detailed_list_len=
          var count = 0;
          if (data.jobs && data.jobs.length > 0) {
            count = data.jobs[0].count;
          }
          console.log("count++++++++++++++++++++" + count)
          let res1Array = [];
          //alert("total_orders" + count);
          let i = 0;
          let fetch_status = true;
          let that = this;
          let new_params = {
            offset: 0,
            limit: 0,
            company_id: this.companyId,
            job: job,
          };
          that.beginCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value, job)
        }
      }
    })
  }
  public finalcount = 0;
  beginCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value, job) {
    that.livetrackingOrder = setInterval(function () {
      if (fetch_status) {
        console.log("maincount++++++++++++++++++++++++++" + that.finalcount);
        console.log("in++++++++++++++++++++++++++" + res1Array.length);
        if (that.finalcount >= count) {
          console.log("end++++++++++++++++++++++++");
          that.creatingcsv = false;
          clearInterval(that.livetrackingOrder);
          let labels = [
            'Job id',
            'Driver',
            'Driver Name',
            'Total Fare',
            'Total Driver revenue',
            'Total Surge',
            'Total cancellation',
            'Total Waiting Time',
            'Total Salik',
            'Total Sharjah',
            'Total Fee collected',
            'Trip Id',
            'Trip Date',
            'Trip amount',
            'Payment Mode',
            'Driver revenue amount',
            'Fee collected',
            'Salik',
            'Sharjah',
          ];
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(res1Array, 'Wallet finalisation-' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        } else {
          fetch_status = false;
          new_params.offset = i;
          new_params.limit = parseInt(offset);
          console.log(JSON.stringify(new_params));
          var encrypted2 = that.encDecService.nwt(that.session, new_params);
          var enc_data2 = {
            data: encrypted2,
            email: that.email
          }
          that._financeService.getRecoDetails(enc_data2).subscribe((dec) => {
            if (dec && dec.status == 200) {
              var res: any = that.encDecService.dwt(that.session, dec.data);
              console.log("data+=======================================" + res.jobs.length);
              if (res.jobs.length > 0) {
                for (let j = 0; j < res.jobs.length; j++) {
                  console.log("current_processing+================================================" + j);
                  const csvArray = res.jobs[j];
                  //console.log("detailssssss+++++++++++++++"+JSON.stringify(csvArray));
                  if (csvArray.order_details.length > 0) {
                    console.log("current=========================orderdetailspresent" + j);
                    that.finalcount = that.finalcount + 1;
                    res1Array.push({
                      job_id: job.job_id,
                      driver: csvArray.driver_details ? csvArray.driver_details.emp_id : '',
                      driver_name: csvArray.driver_details ? csvArray.driver_details.name : csvArray.driver_details.firstname ? csvArray.driver_details.firstname : "",
                      total_trip_amount: csvArray.total_trip_amount ? csvArray.total_trip_amount : 0,
                      total_revenue: csvArray.total_revenue ? csvArray.total_revenue : 0,
                      total_cancellation: csvArray.total_cancellation ? csvArray.total_cancellation : 0,
                      total_surge: csvArray.total_surge ? csvArray.total_surge : 0,
                      total_wait_time: csvArray.total_wait_time ? csvArray.total_wait_time : 0,
                      salik_total: csvArray.total_salik,
                      sharjah_total: csvArray.total_sharjah,
                      total_fee_with_without_rta: csvArray.total_fee_with_without_rta,
                      trip_id: '',
                      trip_date: '',
                      trip_amount: '',
                      payment_mode: '',
                      driver_revenue: '',
                      fee_collected: '',
                      salik: '',
                      sharjah: '',
                    });
                    for (let k = 0; k < csvArray.order_details.length; k++) {
                      res1Array.push({
                        job_id: '',
                        driver: '',
                        driver_name: '',
                        total_trip_amount: '',
                        total_revenue: '',
                        total_cancellation: '',
                        total_surge: '',
                        total_wait_time: '',
                        salik_total: '',
                        sharjah_total: '',
                        total_fee_with_without_rta: '',
                        trip_id: csvArray.order_details[k].unique_order_id,
                        trip_date: csvArray.order_details[k].trip_date_time,
                        trip_amount: csvArray.order_details[k].trip_amount.$numberDecimal,
                        payment_mode: csvArray.order_details[k].payment_mode,
                        driver_revenue: csvArray.order_details[k].driver_revenue.$numberDecimal,
                        fee_without_rta: csvArray.order_details[k].fee_without_rta,
                        salik: csvArray.order_details[k].salik_amount,
                        sharjah: csvArray.order_details[k].sharjah_amount,
                      });
                    }
                    console.log("current=========================orderdetailspresent5" + j);
                  } else {
                    console.log("current=========================orderdetailsnotpresent" + j);
                    that.finalcount = that.finalcount + 1;
                    res1Array.push({
                      job_id: job.job_id,
                      driver: csvArray.driver_revenue ? csvArray.driver_revenue.emp_id : '',
                      driver_name: csvArray.driver_details ? csvArray.driver_details.name : csvArray.driver_details.firstname ? csvArray.driver_details.firstname : "",
                      total_trip_amount: csvArray.total_trip_amount ? csvArray.total_trip_amount : 0,
                      total_revenue: csvArray.total_revenue ? csvArray.total_revenue : 0,
                      total_cancellation: csvArray.total_cancellation ? csvArray.total_cancellation : 0,
                      total_surge: csvArray.total_surge ? csvArray.total_surge : 0,
                      total_wait_time: csvArray.total_wait_time ? csvArray.total_wait_time : 0,
                      salik_total: csvArray.total_salik,
                      sharjah_total: csvArray.total_sharjah,
                      total_fee_with_without_rta: csvArray.total_fee_with_without_rta,
                      trip_id: '',
                      trip_date: '',
                      trip_amount: '',
                      payment_mode: '',
                      driver_revenue: '',
                      fee_collected: '',
                      salik: '',
                      sharjah: '',
                    });
                  }
                  if (j == res.jobs.length - 1) {
                    console.log("endinggggggggggggggggggggggggggggggggggggggggggggg");
                    i = i + parseInt(offset);
                    fetch_status = true;
                  }
                }
              } else {
                that.creatingcsv = false;
                fetch_status = true;
              }
            } else {
              console.log(dec);
              that.toastr.warning('Network reset, csv creation restarted')
              that.createCsv(interval_value, offset);
              res1Array.pop();
              fetch_status = true;
            }
          })
        }
      }
    }, parseInt(interval_value));
  }
}
