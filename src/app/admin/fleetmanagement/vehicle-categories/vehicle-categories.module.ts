import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddVehicleCategoryComponent } from './add-vehicle-category/add-vehicle-category.component';
import { EditVehicleCategoryComponent } from './edit-vehicle-category/edit-vehicle-category.component';
import { ListVehicleCategoryComponent } from './list-vehicle-category/list-vehicle-category.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { VehicleModelsService } from '../../../common/services/vehiclemodels/vehiclemodels.service';
import { VehicleModelsRestService } from '../../../common/services/vehiclemodels/vehiclemodelsrest.service';
import { ImageUploadModule } from 'angular2-image-upload';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    MatSelectModule,
    ImageUploadModule.forRoot(),
    MatTooltipModule
  ],
  declarations: [AddVehicleCategoryComponent, EditVehicleCategoryComponent, ListVehicleCategoryComponent],
  exports: [
    RouterModule
  ],
  providers: [VehicleModelsService, VehicleModelsRestService]
})
export class VehicleCategoriesModule { }
