import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { TariffService } from '../../../../common/services/tariff/tariff.service';
import { PaymentTypeService } from '../../../../common/services/paymenttype/paymenttype.service';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-vehicle-category',
  templateUrl: './add-vehicle-category.component.html',
  styleUrls: ['./add-vehicle-category.component.css']
})
export class AddVehicleCategoryComponent implements OnInit {
  public companyId: any = [];
  public saveForm: FormGroup;
  public spandata = 'no';
  public available_for_status = '2';
  public spanicondata = 'no';
  public tariffRecords;
  public CategoryData;
  public pdata;
  public paymentTypeData;
  email: string;
  session: string;
  invalid: string;
  constructor(private _vehicleModelsService: VehicleModelsService,
    private _paymentTypeService: PaymentTypeService,
    private _tariffService: TariffService,
    public toastr: ToastsManager,
    private router: Router,
    formBuilder: FormBuilder,
    public encDecService: EncDecService,
    vcr: ViewContainerRef,
    public jwtService: JwtService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.saveForm = formBuilder.group({
      name: ['', [
        Validators.required,
      ]],
      short_description: ['', [
        Validators.required,
      ]],
      detailed_description: ['', [
        Validators.required,
      ]],
      seat_count: ['', [
        Validators.required,
      ]],
      fair_breakdown: ['', [
        Validators.required,
      ]],
      vehicle_image: ['', [
        Validators.required,
      ]],
      icon_image: ['', [
        Validators.required,
      ]],
      Priority: ['', [
        Validators.required,
      ]],
      tariffs: ['', [
        Validators.required,
      ]],
      payment_types: ['', [
        Validators.required,
      ]],
      type: ['', [
        Validators.required,
      ]],
      isdefault: ['', [
        Validators.required,
      ]],
      available_for: ['2', [
        Validators.required,
      ]],
      visible_short_menu: ['true', [
        Validators.required,
      ]],
      visible_expand_menu: ['true', [
        Validators.required,
      ]],
      skip_destination: ['true', [
        Validators.required,
      ]],
      block_status: [
        'false', [
          Validators.required,
        ]
      ],
      isDisabled: [
        'false', [
          Validators.required,
        ]
      ],
      schedule_before: [
        '15', [
          Validators.required,
        ]
      ],
      schedule_upto: [
        '10080', [
          Validators.required,
        ]
      ],
      eta_for_schedule_only: [
        '15', [
          Validators.required,
        ]
      ],
      vehicles_count_to_timeout: ['', [
        Validators.required,
      ]],
      distance_to_timeout: ['', [
        Validators.required,
      ]],
      min_vehicle_count: ['', [
        Validators.required,
      ]],
      total_vehicle_count_limit: ['', [
        Validators.required,
      ]],
    });
  }

  ngOnInit() {
    this.getAllTariffs();
    this.getCategories();
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      key: 'customer',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.paymentTypeData = data.paymentTypes;
      }
    });
  }
  public getAllTariffs() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._tariffService.getTariffs(enc_data).then((dec) => {
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.tariffRecords = data.getTariff;
      }
    });
  }
  /**
   * Add vehicle model records
   */
  public addVehicleCategory() {
    if (!this.saveForm.valid) {
      this.toastr.error('All fields are required');
      console.log(this.saveForm)
      this.invalid = 'All fields are required';
      return;
    } else {
      if (typeof this.saveForm.value.vehicles_count_to_timeout !== 'number' || typeof this.saveForm.value.distance_to_timeout !== 'number' || typeof this.saveForm.value.min_vehicle_count !== 'number' || typeof this.saveForm.value.total_vehicle_count_limit !== 'number') {
        this.toastr.error("Please input valid data for Dispatch parameters")
      } else {
        this.invalid = '';
        this.saveForm.controls['eta_for_schedule_only'].enable();
        this.saveForm.controls['schedule_before'].enable();
        this.saveForm.controls['schedule_upto'].enable();
        this.saveForm.controls['Priority'].enable();
        //console.log(this.saveForm);
        const vehicleModel = {
          name: this.saveForm.value.name,
          short_description: this.saveForm.value.short_description,
          detailed_description: this.saveForm.value.detailed_description,
          seat_count: this.saveForm.value.seat_count,
          fair_breakdown: this.saveForm.value.fair_breakdown,
          priority: this.saveForm.value.Priority,
          vehicle_image: this.saveForm.value.vehicle_image,
          icon_image: this.saveForm.value.icon_image,
          tariff: this.saveForm.value.tariffs,
          payment_types: this.saveForm.value.payment_types,
          type: this.saveForm.value.type,
          isdefault: this.saveForm.value.isdefault,
          available_for: this.saveForm.value.available_for,
          visible_short_menu: this.saveForm.value.visible_short_menu,
          visible_expand_menu: this.saveForm.value.visible_expand_menu,
          skip_destination: this.saveForm.value.skip_destination,
          block_status: this.saveForm.value.block_status,
          schedule_before: this.saveForm.value.schedule_before,
          schedule_upto: this.saveForm.value.schedule_upto,
          eta_for_schedule_only: this.saveForm.value.eta_for_schedule_only,
          isDisabled: this.saveForm.value.isDisabled,
          vehicles_count_to_timeout: this.saveForm.value.vehicles_count_to_timeout,
          distance_to_timeout: this.saveForm.value.distance_to_timeout,
          min_vehicle_count: this.saveForm.value.min_vehicle_count,
          total_vehicle_count_limit: this.saveForm.value.total_vehicle_count_limit,
          company_id: this.companyId
        };
        //console.log("params" + JSON.stringify(vehicleModel));
        var encrypted = this.encDecService.nwt(this.session, vehicleModel);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._vehicleModelsService.saveVehicleCategory(enc_data)
          .then((dec) => {
            if (dec) {
              if (dec.status == 200) {
                this.toastr.success('Vehicle category added successfully.');
                this.router.navigate(['/admin/fleet/vehicle-category']);
              }
              else if (dec.status == 203) {
                this.toastr.warning('Vehicle category already exist.');
              }
              else if (dec.status == 205) {
                this.toastr.warning(dec.message);
              }
            }
          }).catch((error) => {
            this.toastr.error('Vehicle category addition failed.');
            //this.router.navigate(['/login']);
          });
      }
    }

  }

  imageFinishedUploading(file: FileHolder) {
    this.saveForm.controls['vehicle_image'].setValue(file.src);

    this.spandata = 'yes';

  }
  imageFinishedIconUploading(file: FileHolder) {
    this.saveForm.controls['icon_image'].setValue(file.src);

    this.spanicondata = 'yes';

  }
  onRemovedIcon(file: FileHolder) {
    this.saveForm.controls['icon_image'].setValue(null);
    this.spanicondata = 'no';
    // do some stuff with the removed file.
  }
  onRemoved(file: FileHolder) {
    this.saveForm.controls['vehicle_image'].setValue(null);
    this.spandata = 'no';
    // do some stuff with the removed file.
  }
  getCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CategoryData = data.vehicleModelsList;
        this.pdata = this.CategoryData.length + 1;
        this.saveForm.controls['Priority'].setValue((this.pdata).toString());
        this.saveForm.controls['Priority'].disable();
      }
    });
  }
  public checkbookingoptions(data) {
    if (data == '0') {
      this.saveForm.controls['eta_for_schedule_only'].setValue("0");
      this.saveForm.controls['eta_for_schedule_only'].disable();
      this.saveForm.controls['schedule_before'].disable();
      this.saveForm.controls['schedule_before'].setValue("0");
      this.saveForm.controls['schedule_upto'].disable();
      this.saveForm.controls['schedule_upto'].setValue("0");
    } else if (data == '1') {
      this.saveForm.controls['eta_for_schedule_only'].setValue("0");
      this.saveForm.controls['eta_for_schedule_only'].disable();
      this.saveForm.controls['schedule_before'].enable();
      this.saveForm.controls['schedule_upto'].enable();
      this.saveForm.controls['schedule_before'].setValue("15");
      this.saveForm.controls['schedule_upto'].setValue("0");
    } else {
      this.saveForm.controls['eta_for_schedule_only'].enable();
      this.saveForm.controls['schedule_before'].enable();
      this.saveForm.controls['schedule_upto'].enable();
      this.saveForm.controls['schedule_upto'].setValue("0");
      this.saveForm.controls['schedule_before'].setValue("15");
      this.saveForm.controls['eta_for_schedule_only'].setValue("20");
    }
  }
}

