import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { ToasterService } from 'angular2-toaster';
import { TariffService } from '../../../../common/services/tariff/tariff.service';
import { PaymentTypeService } from '../../../../common/services/paymenttype/paymenttype.service';
import { Subscription } from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { environment } from '../../../../../environments/environment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-vehicle-category',
  templateUrl: './edit-vehicle-category.component.html',
  styleUrls: ['./edit-vehicle-category.component.css']
})
export class EditVehicleCategoryComponent implements OnInit {
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public available_for;
  public imageUrl = '';
  public iconimageUrl = '';
  public spandata = 'yes';
  public spandataicon = 'yes';
  public tariffRecords;
  public paymentTypeData;
  public companyId: any = [];
  email: string;
  session: string;
  constructor(private _vehicleModelsService: VehicleModelsService,
    private _paymentTypeService: PaymentTypeService,
    private _tariffService: TariffService,
    private route: ActivatedRoute,
    private router: Router,
    formBuilder: FormBuilder,
    public encDecService: EncDecService,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public jwtService: JwtService
  ) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      name: ['', [
        Validators.required,
      ]],
      short_description: ['', [
        Validators.required,
      ]],
      detailed_description: ['', [
        Validators.required,
      ]],
      seat_count: ['', [
        Validators.required,
      ]],
      fair_breakdown: ['', [
        Validators.required,
      ]],
      vehicle_image: [''],
      icon_image: [''],
      Priority: ['', [
        Validators.required,
      ]],
      tariff: ['', [
        Validators.required,
      ]],
      payment_type: ['', [
        Validators.required,
      ]],
      types: ['', [
        Validators.required,
      ]],
      isdefault: ['', [
        Validators.required,
      ]],
      available_for: ['2', [
        Validators.required,
      ]],
      visible_short_menu: ['true', [
        Validators.required,
      ]],
      visible_expand_menu: ['true', [
        Validators.required,
      ]],
      skip_destination: ['true', [
        Validators.required,
      ]],
      block_status: [
        'false', [
          Validators.required,
        ]
      ],
      isDisabled: [
        'false', [
          Validators.required,
        ]
      ],
      schedule_before: [
        '15', [
          Validators.required,
        ]
      ],
      schedule_upto: [
        '10080', [
          Validators.required,
        ]
      ],
      eta_for_schedule_only: [
        '15', [
          Validators.required,
        ]
      ],
      vehicles_count_to_timeout: ['', [
        Validators.required,
      ]],
      distance_to_timeout: ['', [
        Validators.required,
      ]],
      min_vehicle_count: ['', [
        Validators.required,
      ]],
      total_vehicle_count_limit: ['', [
        Validators.required,
      ]],
    });
  }

  ngOnInit() {
    this.getAllTariffs();
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: null,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.paymentTypeData = data.paymentTypes;
      }
    });
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    this.spandata = 'no';
    this.spandataicon = 'no';
    var params1 = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted1 = this.encDecService.nwt(this.session, params1);
    var enc_data1 = {
      data: encrypted1,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategoryById(enc_data1).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        if (res.vehicleModel.category_image) {
          this.imageUrl = environment.imgUrl + '/uploads/vehiclemodels/' + res.vehicleModel.category_image;
        }
        if (res.vehicleModel.icon_image) {
          this.iconimageUrl = environment.imgUrl + '/uploads/vehiclemodels/' + res.vehicleModel.icon_image;
        }
        this.editForm.setValue({
          name: res.vehicleModel.name ? res.vehicleModel.name : '',
          short_description: res.vehicleModel.short_description ? res.vehicleModel.short_description : '',
          detailed_description: res.vehicleModel.detailed_description ? res.vehicleModel.detailed_description : '',
          seat_count: res.vehicleModel.seat_count ? res.vehicleModel.seat_count : '',
          fair_breakdown: res.vehicleModel.fare_breakdown ? res.vehicleModel.fare_breakdown : '',
          Priority: res.vehicleModel.priority ? res.vehicleModel.priority : '',
          vehicle_image: '',
          icon_image: '',
          tariff: res.vehicleModel.tarrifs ? res.vehicleModel.tarrifs : '',
          types: res.vehicleModel.type ? res.vehicleModel.type : '',
          isdefault: res.vehicleModel.isDefault ? res.vehicleModel.isDefault : '',
          payment_type: res.vehicleModel.payment_types ? res.vehicleModel.payment_types : '',
          available_for: res.vehicleModel.available_for ? res.vehicleModel.available_for : '2',
          visible_short_menu: res.vehicleModel.visible_short_menu ? res.vehicleModel.visible_short_menu : 'true',
          visible_expand_menu: res.vehicleModel.visible_expand_menu ? res.vehicleModel.visible_expand_menu : 'true',
          skip_destination: res.vehicleModel.skip_destination ? res.vehicleModel.skip_destination : 'true',
          block_status: res.vehicleModel.block_status ? res.vehicleModel.block_status : 'false',
          schedule_before: res.vehicleModel.schedule_before ? res.vehicleModel.schedule_before : '15',
          schedule_upto: res.vehicleModel.schedule_upto ? res.vehicleModel.schedule_upto : "0",
          eta_for_schedule_only: res.vehicleModel.eta_for_schedule_only ? res.vehicleModel.eta_for_schedule_only : '0',
          vehicles_count_to_timeout: res.vehicleModel.vehicles_count_to_timeout ? parseInt(res.vehicleModel.vehicles_count_to_timeout) : "",
          distance_to_timeout: res.vehicleModel.distance_to_timeout ? parseInt(res.vehicleModel.distance_to_timeout) : "",
          min_vehicle_count: res.vehicleModel.min_vehicle_count ? parseInt(res.vehicleModel.min_vehicle_count) : "",
          total_vehicle_count_limit: res.vehicleModel.total_vehicle_count_limit ? parseInt(res.vehicleModel.total_vehicle_count_limit) : "",
          isDisabled: res.vehicleModel.isDisabled ? res.vehicleModel.isDisabled : 'false',
        });
      }
    }).catch((error) => {
    });
  }
  public getAllTariffs() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._tariffService.getTariffs(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.tariffRecords = data.getTariff;
      }
    });
  }
  /**
  * Update Vehicle model data
  *
  * @param formData
  */

  public updateVehicleCategory() {
    if (this.editForm) {
      if (typeof this.editForm.value.vehicles_count_to_timeout !== 'number' || typeof this.editForm.value.distance_to_timeout !== 'number' || typeof this.editForm.value.min_vehicle_count !== 'number' || typeof this.editForm.value.total_vehicle_count_limit !== 'number') {
        this._toasterService.error("Please input valid data for Dispatch parameters")
      } else {
        this.editForm.controls['eta_for_schedule_only'].enable();
        this.editForm.controls['schedule_before'].enable();
        this.editForm.controls['schedule_upto'].enable();
        const vehiclemodeldata = {
          name: this.editForm.value.name,
          /*number_of_person: this.editForm.value.number_of_person,*/
          short_description: this.editForm.value.short_description,
          detailed_description: this.editForm.value.detailed_description,
          seat_count: this.editForm.value.seat_count,
          fair_breakdown: this.editForm.value.fair_breakdown,
          priority: this.editForm.value.Priority,
          vehicle_image: this.editForm.value.vehicle_image,
          icon_image: this.editForm.value.icon_image,
          tariff: this.editForm.value.tariff,
          payment_types: this.editForm.value.payment_type,
          type: this.editForm.value.types,
          isdefault: this.editForm.value.isdefault,
          available_for: this.editForm.value.available_for,
          visible_short_menu: this.editForm.value.visible_short_menu,
          visible_expand_menu: this.editForm.value.visible_expand_menu,
          skip_destination: this.editForm.value.skip_destination,
          block_status: this.editForm.value.block_status,
          schedule_before: this.editForm.value.schedule_before,
          schedule_upto: this.editForm.value.schedule_upto,
          eta_for_schedule_only: this.editForm.value.eta_for_schedule_only,
          isDisabled: this.editForm.value.isDisabled ? this.editForm.value.isDisabled : 'false',
          vehicles_count_to_timeout: this.editForm.value.vehicles_count_to_timeout,
          distance_to_timeout: this.editForm.value.distance_to_timeout,
          min_vehicle_count: this.editForm.value.min_vehicle_count,
          total_vehicle_count_limit: this.editForm.value.total_vehicle_count_limit,
          company_id: this.companyId,
          _id: this._id
        };
        console.log(vehiclemodeldata);
        var encrypted = this.encDecService.nwt(this.session, vehiclemodeldata);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._vehicleModelsService.updateVehicleCategory(enc_data)
          .then((dec) => {
            console.log(dec);
            if (dec) {
              if (dec.status == 200) {
                this._toasterService.success('Vehicle model updated successfully.');
                this.router.navigate(['/admin/fleet/vehicle-category']);
              } else if (dec.status == 201) {
                this._toasterService.error('Vehicle category updation failed.');
              } else if (dec.status === 203) {
                this._toasterService.warning(dec.message);
              }
            }
          })
          .catch((error) => {
          });
      }

    } else {
      this._toasterService.error('All fields are required.');
      return;
    }
  }

  imageFinishedUploading(file: FileHolder) {
    this.editForm.controls['vehicle_image'].setValue(file.src);
    this.spandata = 'yes';
  }

  onRemoved(file: FileHolder) {
    this.editForm.controls['vehicle_image'].setValue(null);
    this.spandata = 'no';
    // do some stuff with the removed file.
  }
  imageIconFinishedUploading(file: FileHolder) {
    this.editForm.controls['icon_image'].setValue(file.src);
    this.spandata = 'yes';
  }

  onIconRemoved(file: FileHolder) {
    this.editForm.controls['icon_image'].setValue(null);
    this.spandataicon = 'no';
    // do some stuff with the removed file.
  }

  onUploadStateChanged(state: boolean) {
  }
  public checkbookingoptions(data) {
    if (data == '0') {
      this.editForm.controls['eta_for_schedule_only'].setValue("0");
      this.editForm.controls['eta_for_schedule_only'].disable();
      this.editForm.controls['schedule_before'].disable();
      this.editForm.controls['schedule_before'].setValue("0");
      this.editForm.controls['schedule_upto'].disable();
      this.editForm.controls['schedule_upto'].setValue("0");
    } else if (data == '1') {
      this.editForm.controls['eta_for_schedule_only'].setValue("0");
      this.editForm.controls['eta_for_schedule_only'].disable();
      this.editForm.controls['schedule_before'].enable();
      this.editForm.controls['schedule_upto'].enable();
      this.editForm.controls['schedule_before'].setValue("15");
      this.editForm.controls['schedule_upto'].setValue("0");
    } else {
      this.editForm.controls['eta_for_schedule_only'].enable();
      this.editForm.controls['schedule_before'].enable();
      this.editForm.controls['schedule_upto'].enable();
      this.editForm.controls['schedule_upto'].setValue("0");
      this.editForm.controls['schedule_before'].setValue("15");
      this.editForm.controls['eta_for_schedule_only'].setValue("20");
    }
  }
}