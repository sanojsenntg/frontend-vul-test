import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBlockedDevicesComponent } from './edit-blocked-devices.component';

describe('EditBlockedDevicesComponent', () => {
  let component: EditBlockedDevicesComponent;
  let fixture: ComponentFixture<EditBlockedDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBlockedDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBlockedDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
