import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { BlockedDeviceService } from '../../../../common/services/blocked-devices/blocked-devices.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-blocked-devices',
  templateUrl: './edit-blocked-devices.component.html',
  styleUrls: ['./edit-blocked-devices.component.css'],
  providers: [BlockedDeviceService]
})
export class EditBlockedDevicesComponent implements OnInit {

  public serviceData;
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public companyId: any = [];
  session: string;
  email: string;

  constructor(private _blockedDeviceService: BlockedDeviceService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      deviceId: ['', [
        Validators.required
      ]],
      Date_from: ['', [
        Validators.required
      ]],
      Date_to: ['', [
        Validators.required
      ]],
      blocking_notice: ['', [
        Validators.required
      ]],
    });
  }

  public ngOnInit(): void {
    this.editForm.controls['deviceId'].disable();
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._blockedDeviceService.getBlockDevicesById(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        this.editForm.setValue({
          deviceId: res.deviceId ? res.deviceId : '',
          Date_from: res.Date_from ? res.Date_from : '',
          Date_to: res.Date_to ? res.Date_to : '',
          blocking_notice: res.blocking_notice ? res.blocking_notice : '',
        });
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
  }

  public editBlockedDevice() {

    if (!this.editForm.valid) {
      this.toastr.error('All fields are required.');
      return;
    }

    const blockedDeviceData = {
      deviceId: this.editForm.value.deviceId,
      Date_from: this.editForm.value.Date_from,
      Date_to: this.editForm.value.Date_to,
      blocking_notice: this.editForm.value.blocking_notice,
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, blockedDeviceData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._blockedDeviceService.updateBlockedDevice(enc_data)
      .then((dec) => {
        if (dec && dec.status == 200) {
          this.toastr.success('Blocked device Updated successfully.');
          this.router.navigate(['/admin/fleet/blocked-devices']);
        }
      })
      .catch((error) => {
        this.toastr.error('Blocked device Updation cancelled.');
        this.router.navigate(['/login']);
      });
  }
}
