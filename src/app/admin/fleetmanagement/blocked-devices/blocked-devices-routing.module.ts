import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import {ListBlockedDevicesComponent} from './list-blocked-devices/list-blocked-devices.component';
import {AddBlockedDevicesComponent} from './add-blocked-devices/add-blocked-devices.component';
import {EditBlockedDevicesComponent} from './edit-blocked-devices/edit-blocked-devices.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const blockedDeviceRoutes: Routes = [

   { path: '', component: ListBlockedDevicesComponent, canActivate: [AclAuthervice], data: {roles: ["BlockedDevices - List"]}},
   { path: 'add', component: AddBlockedDevicesComponent, canActivate: [AclAuthervice], data: {roles: ["BlockedDevices - Add"]}},
   { path: 'edit/:id', component: EditBlockedDevicesComponent, canActivate: [AclAuthervice], data: {roles: ["BlockedDevices - Edit"]}},

  ];
