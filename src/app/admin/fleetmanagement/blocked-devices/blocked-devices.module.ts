import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { BlockedDeviceService } from '../../../common/services/blocked-devices/blocked-devices.service';
import { BlockedDeviceRestService } from '../../../common/services/blocked-devices/blocked-devicesrest.service';
import { ListBlockedDevicesComponent } from './list-blocked-devices/list-blocked-devices.component';
import { AddBlockedDevicesComponent } from './add-blocked-devices/add-blocked-devices.component';
import { EditBlockedDevicesComponent } from './edit-blocked-devices/edit-blocked-devices.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import {MatTooltipModule} from '@angular/material/tooltip';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    Ng2OrderModule,
    FormsModule,
    MatDatepickerModule,
    MatInputModule,
    MatTooltipModule,
    NguiDatetimePickerModule
        
  ],
        
  declarations: [ListBlockedDevicesComponent, AddBlockedDevicesComponent, EditBlockedDevicesComponent],
   exports: [
    RouterModule
  ],
  providers :[BlockedDeviceService,BlockedDeviceRestService]
})
export class BlockedDevicesModule { }
