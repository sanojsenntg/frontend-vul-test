import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { BlockedDeviceService } from '../../../../common/services/blocked-devices/blocked-devices.service';
import { DeviceService } from '../../../../common/services/devices/devices.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-blocked-devices',
  templateUrl: './add-blocked-devices.component.html',
  styleUrls: ['./add-blocked-devices.component.css'],
  providers: [BlockedDeviceService, JwtService, DeviceService]
})
export class AddBlockedDevicesComponent implements OnInit {
  public companyId: any = [];
  public saveForm: FormGroup;
  private sub: Subscription;
  public _id;
  public blockedDeviceData;
  public blockedDevicesData;
  date1 = null;
  session: string;
  email: string;

  constructor(private _blockedDeviceService: BlockedDeviceService,
    private _deviceService: DeviceService,
    private router: Router,
    public jwtService: JwtService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.saveForm = formBuilder.group({
      deviceId: ['', [
        Validators.required,
      ]],
      Date_from: ['', [
        Validators.required,
      ]],
      Date_to: ['', [
        Validators.required,
      ]],
      blocking_notice: ['', [
        Validators.required,
      ]],
    });
  }
  ngOnInit() {
    this.getDeviceId();
  }

  public addBlockedDevices() {
    if (!this.saveForm.valid) {
      this.toastr.error('All fields are required');
      return;
    }

    const blockedDeviceData = {
      deviceId: this.saveForm.value.deviceId,
      Date_from: this.saveForm.value.Date_from,
      Date_to: this.saveForm.value.Date_to,
      blocking_notice: this.saveForm.value.blocking_notice,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, blockedDeviceData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._blockedDeviceService.addBlockDevices(enc_data)
      .then((dec) => {
        if (dec && dec.status == 200) {
          this.updateDeviceStatus(this.saveForm.value.deviceId)
          this.toastr.success('Blocked device added successfully');
          this.router.navigate(['/admin/fleet/blocked-devices']);
        }
      })
      .catch((error) => {
        this.toastr.error('Blocked device addition failed');
        this.router.navigate(['/login']);
      });
  }

  updateDeviceStatus(value) {
    var params = {
      company_id: this.companyId,
      _id: value
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._deviceService.updateDeviceStatus(enc_data);
  }
  /**
  * Get Device ID for drop down
  *
  */
  public getDeviceId() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._deviceService.getDeviceStatus(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var servicedata: any = this.encDecService.dwt(this.session, dec.data);
        this.blockedDeviceData = servicedata.getStatus;
      }
    });
  }

}
