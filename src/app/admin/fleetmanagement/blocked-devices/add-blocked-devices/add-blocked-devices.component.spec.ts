import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBlockedDevicesComponent } from './add-blocked-devices.component';

describe('AddBlockedDevicesComponent', () => {
  let component: AddBlockedDevicesComponent;
  let fixture: ComponentFixture<AddBlockedDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBlockedDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBlockedDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
