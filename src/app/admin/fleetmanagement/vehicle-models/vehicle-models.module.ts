import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListVehicleModelsComponent } from './list-vehicle-models/list-vehicle-models.component';
import { VehicleModelsService } from '../../../common/services/vehiclemodels/vehiclemodels.service';
import { VehicleModelsRestService } from '../../../common/services/vehiclemodels/vehiclemodelsrest.service';
import { AddVehicleModelComponent } from './add-vehicle-model/add-vehicle-model.component';
import { EditVehicleModelComponent } from './edit-vehicle-model/edit-vehicle-model.component';
import { ImageUploadModule } from 'angular2-image-upload';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule} from '@angular/material/tooltip';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    MatSelectModule,
    ImageUploadModule.forRoot(),
    MatTooltipModule  
  ],
  declarations: [
    ListVehicleModelsComponent,
    AddVehicleModelComponent,
    EditVehicleModelComponent
  ],
  exports: [
    RouterModule
  ],
  providers: [VehicleModelsService, VehicleModelsRestService]
})
export class VehicleModelsModule { }
