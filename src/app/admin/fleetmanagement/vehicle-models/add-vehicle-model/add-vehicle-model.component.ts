import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-admin-add-vehicle-model',
  templateUrl: './add-vehicle-model.component.html',
  styleUrls: ['./add-vehicle-model.component.css']
})
export class AddVehicleModelComponent {
  public saveForm: FormGroup;
  public spandata = 'no';
  public CategoryData;
  public companyId: any = [];
  session: string;
  email: string;
  constructor(private _vehicleModelsService: VehicleModelsService,
    public toastr: ToastsManager,
    private router: Router,
    formBuilder: FormBuilder,
    vcr: ViewContainerRef,
    public encDecService: EncDecService,
    public jwtService: JwtService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.saveForm = formBuilder.group({
      name: ['', [
        Validators.required,

      ]],
      /*number_of_person: ['', [
        Validators.required,

      ]],*/
      vehicle_image: [''],
      vehicle_category: ['']
    });
  }

  ngOnInit() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CategoryData = data.vehicleModelsList;
        }
      }
    });
  }

  /**
   * Add vehicle model records
   */
  public addVehiclemodel() {
    let vehicle_cat_ids = [];
    let vehicle_cat_num_ids = [];
    if (!this.saveForm.valid) {
      // this.toastr.error('All fields are required');
      return;
    } else {
      if (this.saveForm.value.vehicle_category.length > 0) {
        for (var i = 0; i < this.saveForm.value.vehicle_category.length; ++i) {
          vehicle_cat_num_ids.push(this.saveForm.value.vehicle_category[i].unique_category_id)
          vehicle_cat_ids.push(this.saveForm.value.vehicle_category[i]._id)
        }
      }
      const vehicleModel = {
        name: this.saveForm.value.name,
        /*number_of_person: this.saveForm.value.number_of_person,*/
        vehicle_image: this.saveForm.value.vehicle_image,
        vehicle_cat_ids: vehicle_cat_ids,
        vehicle_cat_num_ids: vehicle_cat_num_ids,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, vehicleModel);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._vehicleModelsService.saveVehicleModels(enc_data)
        .then((dec) => {
          vehicle_cat_num_ids = [];
          vehicle_cat_ids = [];
          if (dec) {
            if (dec.status == 200) {
              this.toastr.success('Vehicle model added successfully.');
              this.router.navigate(['/admin/fleet/vehicle-model']);
            }
            else if (dec.status == 203) {
              this.toastr.warning('Vehicle model already exist.');
            }
            else
            this.toastr.error(dec.message)
          }
        }).catch((error) => {
          this.toastr.error('Vehicle model addition failed.');
          this.router.navigate(['/login']);
        });
    }
  }

  imageFinishedUploading(file: FileHolder) {
    this.saveForm.controls['vehicle_image'].setValue(file.src);

    this.spandata = 'yes';

  }

  onRemoved(file: FileHolder) {
    this.saveForm.controls['vehicle_image'].setValue(null);
    this.spandata = 'no';
    // do some stuff with the removed file.
  }
}

