import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { ToasterService } from 'angular2-toaster';
import { Subscription } from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { environment } from '../../../../../environments/environment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-vehicle-model',
  templateUrl: './edit-vehicle-model.component.html',
  styleUrls: ['./edit-vehicle-model.component.css']
})

export class EditVehicleModelComponent implements OnInit {
  public editForm: FormGroup;
  public companyId: any = [];
  private sub: Subscription;
  public _id;
  public imageUrl = '';
  public spandata = 'yes';
  public CategoryData;
  session: string;
  email: string;
  constructor(private _vehicleModelsService: VehicleModelsService,
    private route: ActivatedRoute,
    private router: Router,
    formBuilder: FormBuilder,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public jwtService: JwtService,
    private toastr: ToastsManager,
    public encDecService: EncDecService
  ) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      name: ['', Validators.required],
      /*number_of_person: ['', Validators.required],*/
      vehicle_image: [''],
      vehicle_category: ['']
    });
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CategoryData = data.vehicleModelsList;
        }
      }
    });
    this.spandata = 'no';
    var params1 = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted1 = this.encDecService.nwt(this.session, params1);
    var enc_data1 = {
      data: encrypted1,
      email: this.email
    }
    this._vehicleModelsService.getVehicleModelById(enc_data1).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          if (res.vehicleModel.vehicle_image) {
            this.imageUrl = environment.imgUrl + '/uploads/vehiclemodels/' + res.vehicleModel.vehicle_image;
          }
          this.editForm.setValue({
            name: res.vehicleModel.name ? res.vehicleModel.name : '',
            /*number_of_person: res.vehicleModel.number_of_person ? res.vehicleModel.number_of_person:'',*/
            vehicle_image: '',
            vehicle_category: res.vehicleModel.vehicle_cat_ids ? res.vehicleModel.vehicle_cat_ids : ''
          });
        }
      }
    }).catch((error) => {
    });
  }

  /**
   * Update Vehicle model data
   *
   * @param formData
   */

  public updateVehicleModel() {
    let vehicle_cat_ids = [];
    let vehicle_cat_num_ids = [];
    if (!this.editForm.valid) {
      // this.toastr.error('All fields are required');
      return;
    } else {
      if (this.editForm.value.vehicle_category.length > 0) {
        for (var i = 0; i < this.editForm.value.vehicle_category.length; ++i) {
          vehicle_cat_num_ids.push(this.editForm.value.vehicle_category[i].unique_category_id)
          vehicle_cat_ids.push(this.editForm.value.vehicle_category[i]._id)
        }
      }
      const vehiclemodeldata = {
        name: this.editForm.value.name,
        /*number_of_person: this.editForm.value.number_of_person,*/
        vehicle_cat_ids: vehicle_cat_ids,
        vehicle_cat_num_ids: vehicle_cat_num_ids,
        vehicle_image: this.editForm.value.vehicle_image,
        _id: this._id
      };
      var encrypted = this.encDecService.nwt(this.session, vehiclemodeldata);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._vehicleModelsService.updateVehicleModel(enc_data)
        .then((dec) => {
          if (dec) {
            if (dec.status == 200) {
              this._toasterService.success('Vehicle model updated successfully.');
              this.router.navigate(['/admin/fleet/vehicle-model']);
            } else if (dec.status == 201) {
              this._toasterService.error('Vehicle model updation failed.');
            } else if (dec.status === 203) {
              this._toasterService.warning('Vehicle model Already Exist ');
            }
            else
              this._toasterService.error(dec.message)
          }
        })
        .catch((error) => {
        });
    }
  }

  imageFinishedUploading(file: FileHolder) {
    this.editForm.controls['vehicle_image'].setValue(file.src);
    this.spandata = 'yes';
  }

  onRemoved(file: FileHolder) {
    this.editForm.controls['vehicle_image'].setValue(null);
    this.spandata = 'no';
    // do some stuff with the removed file.
  }

  onUploadStateChanged(state: boolean) {
  }

}

