import { Component, OnInit } from '@angular/core';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-fleet',
  templateUrl: './fleet.component.html',
  styleUrls: ['./fleet.component.css']
})
export class FleetComponent implements OnInit {
  public companyId: any = [];
  public aclCheck;
  email: string;
  session: string;
  constructor(public _aclService: AccessControlService,
    public encDecService: EncDecService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session= localStorage.getItem('Sessiontoken');
    this.email= localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.aclDisplayService();
  }

  ngOnInit() {
  }
  public aclDisplayService() {
    this.aclCheck = JSON.parse(localStorage.getItem('userMenu'));
    // var params = {
    //   company_id: this.companyId
    // }
    // var encrypted = this.encDecService.nwt(this.session, params);
    // var enc_data = {
    //   data: encrypted,
    //   email: this.email
    // }
    // this._aclService.getAclUserMenu(enc_data).then((dec) => {
    //   if(dec){
    //     if(dec.status==200){
    //       var data: any = this.encDecService.dwt(this.session, dec.data);
    //     }
    //   }
    // })

  }
}
