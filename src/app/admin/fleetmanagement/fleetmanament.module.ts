import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { VehiclesModule } from './vehicles/vehicles.module';
import { VehicleModelsModule } from './vehicle-models/vehicle-models.module';
import { VehicleCategoriesModule } from './vehicle-categories/vehicle-categories.module';
import { VehicleCategoriesMessageModule } from './vehicle-categories-messages/vehicle-categories-messages.module';
import { DevicesModule } from './devices/devices.module';
import { BlockedDevicesModule } from './blocked-devices/blocked-devices.module';
import { DriversModule } from './drivers/drivers.module';
import { DriverGroupsModule } from './driver-groups/driver-groups.module';
import { AdditionalServiceModule } from './additional-service/additional-service.module';
import { EditDriverLanguageComponent } from './drivers/list-drivers/dialog/edit-driver-language/edit-driver-language.component';
import { OrderStatusHistoryComponent } from './drivers/list-drivers/dialog/order-status-history/order-status-history';
import { ShiftInfoComponent } from './drivers/list-drivers/dialog/shift-info/shift';
import { ShiftAssignmentComponent } from './drivers/list-drivers/dialog/shift-assignment/shift-assignment';
import { ShiftDetailComponent } from './drivers/list-drivers/dialog/shift-detail/shift-detail.component';
import { EditAdditionalServiceComponent } from './drivers/list-drivers/dialog/edit-additional-service/edit-additional-service.component';
import { CustomerRatingComponent } from './drivers/list-drivers/dialog/customer-rating/customer-rating.component';
import { FleetComponent } from './fleet/fleet.component';
import { environment } from '../../../environments/environment';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { DriverStatsComponent } from './driver-stats/driver-stats.component';
import { ExtDriverModule } from './ext-driver/ext-driver.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { DriverDetailsComponent } from './driver-stats/driver-details/driver-details.component';
import { DriverShiftActivityComponent } from './driver-stats/driver-shift-activity/driver-shift-activity.component';import {MatTableModule} from '@angular/material/table';
import { ListDriverStatsComponent } from './driver-stats/list-driver-stats/list-driver-stats.component';
import { ListDriverAcceptanceComponent } from './driver-stats/list-driver-acceptance/list-driver-acceptance.component';
import { ListDriverAvailabilityComponent } from './driver-stats/list-driver-availability/list-driver-availability.component';
const config: SocketIoConfig = { url: environment.socketUrl, options: {} };


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    VehiclesModule,
    DriversModule,
    VehicleModelsModule,
    VehicleCategoriesModule,
    VehicleCategoriesMessageModule,
    DevicesModule,
    BlockedDevicesModule,
    DriverGroupsModule,
    AdditionalServiceModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule, 
    ExtDriverModule,
    SocketIoModule.forRoot(config),
    MatAutocompleteModule
  ],
  declarations: [
    EditDriverLanguageComponent,
    OrderStatusHistoryComponent,
    ShiftAssignmentComponent,
    ShiftInfoComponent,
    ShiftDetailComponent,
    EditAdditionalServiceComponent,
    CustomerRatingComponent,
    FleetComponent,
    DriverStatsComponent,
    DriverDetailsComponent,
    DriverShiftActivityComponent,
    ListDriverStatsComponent,
    ListDriverAcceptanceComponent,
    ListDriverAvailabilityComponent
  ],
  exports: [
    RouterModule
  ],
  providers: [],
  entryComponents: [
    EditDriverLanguageComponent,
    OrderStatusHistoryComponent,
    ShiftInfoComponent,
    ShiftAssignmentComponent,
    ShiftDetailComponent,
    CustomerRatingComponent,
    EditAdditionalServiceComponent,
    DriverShiftActivityComponent
  ]
})
export class FleetmanamentModule { }
