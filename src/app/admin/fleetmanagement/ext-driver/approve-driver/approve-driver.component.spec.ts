import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveDriverComponent } from './approve-driver.component';

describe('ApproveDriverComponent', () => {
  let component: ApproveDriverComponent;
  let fixture: ComponentFixture<ApproveDriverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveDriverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
