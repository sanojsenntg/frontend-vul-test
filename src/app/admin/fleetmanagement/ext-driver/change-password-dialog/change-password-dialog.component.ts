import { Component, OnInit, Inject } from '@angular/core';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ExtDriverService } from '../../../../common/services/ext-driver/ext-driver.service';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./change-password-dialog.component.css']
})
export class ChangePasswordDialogComponent implements OnInit {
  session: string;
  email: string;
  companyId: any=[];
  isLoading: boolean;
  invalid: string;
  extDriver: any;

  constructor(
    private encDecService: EncDecService,
    public dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dataw: any, public extDriverService: ExtDriverService,public toastr: ToastsManager) {
    const company_id: any = localStorage.getItem('user_company');
    this.session= localStorage.getItem('Sessiontoken');
    this.email= localStorage.getItem('user_email');
    this.companyId.push(company_id); 
    this.extDriver=dataw;
  }
  ngOnInit() {
  }
  submit(data) {
    if(data.password !== data.cpassword){
      this.invalid='Password mismatch';
      return;
    }
    this.isLoading = true;
    this.invalid='';
    var params = {
      on_boarding_driver_id: this.extDriver._id,
      company_id: this.companyId,
      auth_key: this.extDriver.auth_key,
      password:data.password
    }
    console.log(params)
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.extDriverService.updatePassword(enc_data).then((dec) => {
      this.isLoading = false;
      if (dec && dec.status == 200) {
        this.toastr.success('Password changed successfully')
        setTimeout(()=>{this.dialogRef.close();},2000)
      }
      else {
        this.invalid = dec.message ? dec.message : 'Something went wrong';
      }
    })
  }
}
