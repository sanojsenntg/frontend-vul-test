import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproveDriverComponent } from './approve-driver/approve-driver.component';
import { ListDriverComponent } from './list-driver/list-driver.component';

export const extDriverRoute:Routes = [
  { path: 'approve/:id', component : ApproveDriverComponent},
  { path: 'list', component : ListDriverComponent},
];
