import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListDriverComponent } from './list-driver/list-driver.component';
import { ApproveDriverComponent } from './approve-driver/approve-driver.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material';
import { ExtDriverRestService } from '../../../common/services/ext-driver/ext-driverrest.service';
import { ExtDriverService } from '../../../common/services/ext-driver/ext-driver.service';
import { ImageDialogComponent } from './image-dialog/image-dialog.component';
import { MatRadioModule } from '@angular/material/radio';
import { ApproveDialogComponent } from './approve-dialog/approve-dialog.component';
import {MatDialogModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ChangePasswordDialogComponent } from './change-password-dialog/change-password-dialog.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MatCardModule,
    NgbModule,
    MatSelectModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatDialogModule,
    MatCheckboxModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [ExtDriverRestService, ExtDriverService],
  declarations: [ListDriverComponent, ApproveDriverComponent, ImageDialogComponent, ApproveDialogComponent, ChangePasswordDialogComponent],
  entryComponents: [
    ImageDialogComponent,
    ApproveDialogComponent,
    ChangePasswordDialogComponent
  ]
})
export class ExtDriverModule { }
