import { Component, OnInit, ViewContainerRef, OnDestroy } from '@angular/core';
import { DriverService } from '../../../../common/services/driver/driver.service';
import { EditDriverLanguageComponent } from './dialog/edit-driver-language/edit-driver-language.component';
import { OrderStatusHistoryComponent } from './dialog/order-status-history/order-status-history';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeviceService } from '../../../../common/services/devices/devices.service';
import { ToastsManager } from 'ng2-toastr';
import { Overlay } from '@angular/cdk/overlay';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { ShiftInfoComponent } from './dialog/shift-info/shift';
import { ShiftDetailComponent } from './dialog/shift-detail/shift-detail.component';
import { ShiftAssignmentComponent } from './dialog/shift-assignment/shift-assignment';
import { ShiftTimingsService } from '../../../../common/services/shift-timings/shift-timings.service';
import { environment } from '../../../../../environments/environment';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { EditAdditionalServiceComponent } from './dialog/edit-additional-service/edit-additional-service.component';
import { single } from 'rxjs/operator/single';
import * as moment from 'moment/moment';
import { DriverDialogComponent } from '../../../../common/dialog/driver-dialog/driver-dialog.component';
import { CustomerRatingComponent } from './dialog/customer-rating/customer-rating.component';
import { UploadDialogComponent } from './dialog/upload-dialog/upload-dialog.component';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { FormControl } from '@angular/forms';
import { CancelReasonComponent } from '../../../../common/dialog/cancel-reason/cancel-reason.component';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { DriverblockreasonComponent } from '../../../../common/dialog/driverblockreason/driverblockreason.component';
import { Socket } from 'ng-socket-io';
import { EditStatusDialogComponent } from './dialog/edit-status-dialog/edit-status-dialog.component'
import { EditDriverGroupComponent } from './dialog/edit-driver-group/edit-driver-group.component';
import { GlobalService } from '../../../../common/services/global/global.service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { UsersService } from '../../../../common/services/user/user.service';

declare var $;

declare var jsPDF;

declare var templateToPdf;

declare var $;
@Component({
  selector: 'app-list-drivers',
  templateUrl: './list-drivers.component.html',
  styleUrls: ['./list-drivers.component.css']
})

export class ListDriversComponent implements OnInit, OnDestroy {
  queryField: FormControl = new FormControl();
  device: FormControl = new FormControl();
  public companyId: any = [];
  public selectedAll;
  public DriverData;
  public DriverList;
  public driverForm = '';
  public deviceForm = '';
  public company_id = '';
  public pageSize = 10;
  public pageNo = 0;
  public changeStatus;
  public sortOrder = 'asc';
  public key;
  public is_search = false;
  public dropDownData: any = [];
  public avgRatings: any = [];
  public selectedDriver: any = [];
  public DriverLength;
  public searchSubmit = false;
  public change_status;
  public deviceData;
  public keyword;
  public _id;
  public searchValue;
  public driverGroupData;
  public blocked_status = '';
  public driver_status:any = [];
  public ShiftTimingsData;
  public imageurl;
  public selected;
  public searchLoader = false;

  public shift_timing_id: any;
  public driver_groups: any;
  public device_id: any;
  public driver_id: any;
  public companyData;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public aclDriverStatus = true;
  public aclForceLogout = false;
  public aclDriverBlock = true;
  public downloads = false
  public loadingIndicator;
  public pNo = 1;
  public logoutList = [];
  public blockList = [];
  public imageext;
  public apiUrl;
  public adminUser;
  session: string;
  email: string;
  dtc: boolean = false;
  public socket_session_token;
  companyIdFilter: any;
  constructor(
    public _driverService: DriverService,
    public dialog: MatDialog,
    private _devicesService: DeviceService,
    public toastr: ToastsManager,
    public overlay: Overlay,
    private socket: Socket,
    private vcr: ViewContainerRef,
    private _usersService: UsersService,
    private _companyservice: CompanyService,
    private _shiftTimingsService: ShiftTimingsService,
    public _aclService: AccessControlService,
    public jwtService: JwtService,
    private router: Router,
    public encDecService: EncDecService,
    private _global: GlobalService) {
    this.company_id = localStorage.getItem('user_company');
    if (this.company_id === '5ce12918aca1bb08d73ca25d' || this.company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(this.company_id);
    this.toastr.setRootViewContainerRef(vcr);
    this.aclDisplayService();
    this.adminUser = JSON.parse(window.localStorage['adminUser']);
  }
  ngOnInit() {
    this.imageext = window.localStorage['ImageExt'];
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.imageurl = environment.imgUrl;
    this.getCompanies();
    this.getDriverGroups();
    this.getShiftTimings();
    this.refreshBlockList();
    if (this._global.drivers) {
      const data = this._global.drivers;
      this.pageSize = data.limit;
      this.driver_id = data._id;
      this.driverForm = data.driver;
      this.driver_groups = data.driver_groups;
      this.blocked_status = data.blocked_status;
      this.shift_timing_id = data.shift_timing_id;
      this.device_id = data.device_id;
      this.deviceForm = data.devices;
    }
    this.apiUrl = environment.apiUrl;
    /*this.queryField.valueChanges
    .debounceTime(200)
    .distinctUntilChanged().subscribe( queryField =>this._driverService.searchDriver({ 'search_keyword': queryField }).then(data => {
      this.results= data.getDriverss;
    }));*/
    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriver';
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        }
        console.log(params)
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status === 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.DriverList = result.driver;
          this.loadingIndicator = '';
        }
      });
    this.device.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDevice';
    })
    this.device.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {

        var params = {
          offset: 0,
          limit: this.pageSize,
          sortOrder: 'asc',
          sortByColumn: 'unique_device_id',
          search_keyword: query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
          unique_device_id: null
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._devicesService.getDevicesListing(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.deviceData = result.devices;
          this.loadingIndicator = '';
        }
      });
    if (JSON.parse(localStorage.getItem("logoutDrivers")))
      this.logoutList = JSON.parse(localStorage.getItem("logoutDrivers"));
    if (JSON.parse(localStorage.getItem("blockedDrivers")))
      this.blockList = JSON.parse(localStorage.getItem("blockedDrivers"));
  }
  /**
   * Load list of drivers in driver dropdown on page load.
   */
  public aclDisplayService() {
    var params = {
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Drivers - Add") {
            this.aclAdd = true;
          } else if (data.menu[i] == "Drivers - Edit") {
            this.aclEdit = true;
          } else if (data.menu[i] == "Drivers - Delete") {
            this.aclDelete = true;
          } else if (data.menu[i] == "Drivers - Status Change") {
            this.aclDriverStatus = false;
          } else if (data.menu[i] == "Drivers - Force Logout") {
            this.aclForceLogout = true;
          } else if (data.menu[i] == "Drivers - Block") {
            this.aclDriverBlock = false;
          } else if (data.menu[i] == 'Downloads - Fleet Management') {
            this.downloads = true;
          }
        };
      }
    })
  }

  changeDriverStatus(event){
    if(this.driver_status. indexOf("A") != -1){
      this.driver_status.push(null);
    }
    console.log(this.driver_status);
  }
  /**
   * Autocomplete search for driver.
   * @param data
   */
  searchDriver(data) {
    if (typeof data === 'object') {
      this.driver_id = data._id;
    }
  }

  /**
   * Displaying name of driver in autocomplete search for driver dropdown.
   * @param data
   */
  displayFndriver(data): string {
    return data ? data.username : data;
  }

  /**
   * Load list of devices in device dropdown on page load.
   */
  public getDevices() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      unique_device_id: null
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._devicesService.getDevicesListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.deviceData = data.devices;
      }
    })
  }

  /**
   * Autocomplete search on device
   * @param data
   */
  searchDevice(data) {
    if (typeof data === 'object') {
      this.device_id = data._id;
    }
  }

  /**
   * Displaying unique_device_id of device in autocomplete search for device dropdown.
   * @param data
   */
  displayFndevice(data): string {
    return data ? data.unique_device_id : data;
  }

  /**
   * Load list of driver group in driver groups dropdown on page load.
   */
  public getDriverGroups() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriversGroup(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.driverGroupData = data.getDriverGroups;
      }
    });
  }

  /**
   * Autocomplete search on driver group
   * @param data
   */
  searchDriverGroups(data) {
    this.loadingIndicator = 'searchDriverGroups';
    if (typeof data !== 'object') {
      const params = {
        offset: 0,
        limit: 0,
        sortOrder: 'asc',
        sortByColumn: 'name',
        search: data,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.getDriversGroup(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.driverGroupData = data.getDriverGroups;
        }
        this.loadingIndicator = '';
      });
    } else {
      this.driver_groups = data._id;
      this.loadingIndicator = '';
    }
  }

  /**
   * Displaying name of driver group in autocomplete search for driver groups dropdown.
   * @param data
   */
  displayFndriverGroup(data): string {
    return data ? data.name : data;
  }

  /**
   * Load list of shift timing in shifts dropdown on page load.
   */
  public getShiftTimings() {
    const shiftParams = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };

    var encrypted = this.encDecService.nwt(this.session, shiftParams);
    var enc_data = {
      data: encrypted,
      email: this.email
    }

    this._shiftTimingsService.getShiftTiming(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ShiftTimingsData = data.shift_timings;
      }
    });
  }

  /**
   * Autocomplete search on shift timing
   * @param data
   */
  searchShiftTiming(data) {
    if (typeof data !== 'object') {
      this.loadingIndicator = 'searchShiftTiming';
      const shiftParams = {
        offset: 0,
        limit: 0,
        sortOrder: 'desc',
        name: data,
        sortByColumn: 'updated_at',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, shiftParams);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftTimingsService.getShiftTiming(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.ShiftTimingsData = data.shift_timings;
          this.loadingIndicator = '';
        }
      });
    } else {
      this.shift_timing_id = data._id;
    }
  }

  /**
   * Displaying name of shift in autocomplete search for shift dropdown.
   * @param data
   */
  displayFnShiftTiming(data): string {
    return data ? data.name : data;
  }

  /**
   * Select and deselect all records for multiple records deletion.
   * @param id
   */
  selectAll() {
    for (let i = 0; i < this.DriverData.length; i++) {
      if (this.selectedAll === false) {
        this.selectedDriver = [];
      } else {
        this.selectedDriver[i] = this.DriverData[i]._id;
      }
      this.DriverData[i].selected = this.selectedAll;
    }
  }

  /**
   * Select and delselect one or more record to delete.
   * @param driver
   * @param i
   */
  checkIfAllSelected(driver, i) {
    if (driver) {
      this.DriverData[i].checked = !this.DriverData[i].checked;
      if (this.DriverData[i].checked == true) {
        this.selectedDriver.push(driver._id);
      }
      else if (this.DriverData[i].checked == false) {
        this.selectedDriver.splice(i, 1);
      }
    }
  }

  /**
   * Delete array of driver
   */
  deleteSelectedDriver() {
    let selectedDriverData = {
      ids: this.selectedDriver,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    }
    var encrypted = this.encDecService.nwt(this.session, selectedDriverData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.deleteDriverArray(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.toastr.success('Drivers deleted successfully.');
      }
    })
    this.ngOnInit();
  }

  openUploadDialog(): void {
    let dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  /**
   * Open dialog for delete array of drivers.
   */
  openDeleteArrayDialog(): void {
    if (this.selectedDriver.length >= 1) {
      let dialogRef = this.dialog.open(DeleteDialogComponent, {
        width: '450px',
        scrollStrategy: this.overlay.scrollStrategies.noop(),
        data: { text: 'Are you sure you want to remove this record' }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result == true) {
          this.deleteSelectedDriver();
        } else {
        }
      });
    } else {
      this.toastr.warning('Please select atleast one record to delete.')
    }

  }

  /**
   * Open dialog for delete a particular driver by id.
   * @param id
   * @param index
   */
  openDeleteDialog(id, index): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        const data = {
          'driver_id': id,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        };
        var encrypted = this.encDecService.nwt(this.session, data);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._driverService.deleteDriver(enc_data).then((dec) => {
          if (dec && dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.DriverData.splice(index, 1);
            this.toastr.success('Driver deleted successfully.');
          } else {
            this.toastr.warning(dec.message);
          }
        });
      } else {
      }
    });
  }

  /**
   * Edit additional service dialog.
   * @param id
   */
  EditAddService(driverAddData1): void {
    let data = {
      id: driverAddData1,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    }
    let dialogRef = this.dialog.open(EditAdditionalServiceComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.getAllDrivers(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.DriverData = data.driver;
          this.DriverLength = data.count;
        }
      });
    });
  }

  /**
   * Used to generate pdf.
   */
  public pdfGenerator() {
    var options = {
      html: "<div><p>hello der</p></div>",
      fileName: 'howdycolton.pdf',
      filePath: '/assets'
    }
    templateToPdf(options).then(function (resp) {
    })
      .catch(function (err) {
      });
  }

  /**
   * Open dialog for edit driver language of a particular driver.
   * @param driverAddData1
   */
  DriverLanguage(driverAddData1) {
    this.dialog.closeAll();
    let data = {
      id: driverAddData1,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    }
    let dialogRef = this.dialog.open(EditDriverLanguageComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      const params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.getAllDrivers(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.DriverData = data.driver;
          this.DriverLength = data.count;
        }
      });
    });
  }

  /**
   * Open dialog for edit shifts of a particular driver.
   * @param driverData
   */
  shiftAsignment(driverData) {
    this.dialog.closeAll();
    let data = {
      id: driverData,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    }
    let dialogRef = this.dialog.open(ShiftAssignmentComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  /**
   * Open dialog customer rtaings detail of a particular driver.
   * @param driverData
   */
  CustomerRating(driverData) {
    this.dialog.closeAll();
    let dialogRef = this.dialog.open(CustomerRatingComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: driverData
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  /**
   * Open dialog for Showing order status history of a particular driver.
   * @param driver
   */
  showOrderHistory(driver) {
    this.dialog.closeAll();
    let dialogRef = this.dialog.open(OrderStatusHistoryComponent, {
      width: '750px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: driver
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  /**
   * Open dialog for showing online history of a particular driver.
   * @param driver
   */
  shiftInfo(driver) {
    this.dialog.closeAll();
    let dialogRef = this.dialog.open(ShiftInfoComponent, {
      width: '750px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: driver
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  /**
   * Open dialog for showing shift detail  of a particular driver.
   * @param driver
   */
  shiftDetail(driver) {
    this.dialog.closeAll();
    let dialogRef = this.dialog.open(ShiftDetailComponent, {
      width: '800px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: driver
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  /**
   * Change block status of a driver.
   * @param driver
   * @param index
   */
  ChangeBlockStatus(driver, index, type, reason, note) {
    let that = this;
    var user = JSON.parse(window.localStorage['adminUser']);
    if (!user) {
      that.toastr.error("Session Expired..please login again");
    } else {
      let params = {
        driver_id: driver._id,
        block_status: driver.blocked_status,
        email: user.email,
        user_id: user._id,
        priority: user.priority,
        reason: reason,
        note: note,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      }
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this.blockList.push(driver._id);
      this._driverService.ChangeDriverStatus(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            if (type === "block") {
              this.toastr.success('Driver blocked with force logout Operation Initiated');
            } else {
              this.removeBlockedDriverFromLocal(driver._id)
              this.toastr.success('Driver unblocked successfully');
              this.DriverData[index].blocked_status = data.updatedDriverData.blocked_status;
            }
          }
          else if (dec.status === 205) {
            this.toastr.warning(dec.Message);
          }
          else if (dec.status === 203) {
            this.toastr.warning(dec.message);
            //return false;
          }
          else if (dec.status === 201) {
            this.removeBlockedDriverFromLocal(driver._id)
            this.toastr.error('Block Operation Failed');
            //return false;
          } else if (dec.status === 208) {
            this.removeBlockedDriverFromLocal(driver._id)
            this.toastr.success('Driver blocked successfully');
            this.DriverData[index].blocked_status = "1";
            this.DriverData[index].status = 'logout';
            //return false;
          }
          //return true;
        }
      });

    }
  }
  public typeof;
  public DriverstatusChangePopup(id, index, type) {
    //alert(type);
    this.typeof = type;
    let that = this;
    let dialogRef = this.dialog.open(DriverblockreasonComponent, {
      width: '315px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { type: type }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        //alert(type);
        if (this.typeof == 'b') {
          //alert("block");
          let reason = res.cancelreason
          let note = res.cancelText;
          var type = "block"
          var r = this.ChangeBlockStatus(id, index, type, reason, note);
        }
        if (this.typeof == 'f') {
          //alert("forcelogout");
          let reason = res.cancelreason
          let note = res.cancelText;
          var user = JSON.parse(window.localStorage['adminUser']);
          if (!user) {
            that.toastr.error("Session Expired..please login again");
          } else {
            let params = {
              driver_id: id._id,
              email: user.email,
              reason: reason,
              note: note,
              type: this.typeof,
              company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
            }
            var encrypted = this.encDecService.nwt(this.session, params);
            var enc_data = {
              data: encrypted,
              email: this.email
            }
            this.logoutList.push(id._id);
            this._driverService.ForceLogout(enc_data).then((dec) => {
              if (dec) {
                if (dec.status == 200) {
                  that.toastr.success(dec.message);
                }
                if (dec.status === 205) {
                  that.toastr.warning(dec.Message);
                  this.removeDriverFromLogout(id._id)
                } else if (dec.status === 204) {
                  that.toastr.error('Force Logout operation failed');
                  this.removeDriverFromLogout(id._id)
                }
                else if (dec.status === 206) {
                  this.removeDriverFromLogout(id._id)
                  that.toastr.error('Driver is currently on a trip');
                } else if (dec.status === 202) {
                  that.toastr.success('Force Logout Operation initiated');
                }
              }
            });
          }
        }
      }
    });
  }

  /**
   * Open dialog for block a driver.
   * @param id
   * @param index
   */
  openDialogs(id, index): void {
    const dialogRef = this.dialog.open(DriverDialogComponent, {
      width: '450px',
      height: '150px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to block this driver' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        var type = "block"
        let reason = '';
        let note = '';
        var r = this.ChangeBlockStatus(id, index, type, reason, note);
        //this.toastr.success('Driver blocked successfully');
        //alert(r);
        //if (r) {
        //  this.toastr.success('Driver blocked successfully');
        //} else {force
        //  this.toastr.error('Driver is currently on trip');
        // }
      } else {

      }
    });
  }

  /**
   * Open dialog for unblock a driver.
   * @param id
   * @param index
   */
  openDialogToUnblock(id, index): void {
    const dialogRef = this.dialog.open(DriverDialogComponent, {
      width: '450px',
      height: '150px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to Unblock this driver' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        var type = "unblock";
        let reason = "";
        let note = '';
        this.ChangeBlockStatus(id, index, type, reason, note);
        //this.toastr.success('Driver unblocked successfully');
      } else {
      }
    });
  }

  /**
   * Force logout for a driver.
   * @param driver
   * @param index
   */
  ForceLogout(driver, index) {
    var user = JSON.parse(window.localStorage['adminUser']);
    let params = {
      driver_id: driver._id,
      email: user.email,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.ForceLogout(enc_data).then((dec) => {
      if (dec) {
        if (dec.status === 206) {
          this.toastr.error('Driver is currently on a trip');
        } else if (dec.status === 202) {
          this.toastr.success('Force Logout Operation initiated');
        }
      }
    });
  }

  /**
   * Used for search on driver.
   */
  public getdriver() {
    this.pNo = 1;
    this.searchLoader = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      _id: this.driver_id ? this.driver_id : '',
      driver_groups: this.driver_groups ? this.driver_groups._id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      blocked_status: this.blocked_status,
      shift_timings_id: this.shift_timing_id ? this.shift_timing_id._id : '',
      device_id: this.device_id ? this.device_id : '',
      driver_status:this.driver_status
    };
    const params2 = {
      limit: this.pageSize,
      _id: this.driver_id ? this.driver_id : '',
      driver: this.driverForm ? this.driverForm : '',
      driver_groups: this.driver_groups ? this.driver_groups : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      blocked_status: this.blocked_status,
      shift_timings_id: this.shift_timing_id ? this.shift_timing_id : '',
      device_id: this.device_id ? this.device_id : '',
      devices: this.deviceForm ? this.deviceForm : '',
      driver_status:this.driver_status
    };
    this._global.drivers = params2;
    if (
      this._id != '' || this.driver_id != '' || this.driver_groups != '' ||
      this.blocked_status != '' || this.shift_timing_id != '' || this.device_id != ''
    ) {
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      console.log(params);
      this._driverService.getAllDrivers(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.DriverData = data.driver;
          console.log(this.DriverData);
          this.DriverLength = data.count;
          this.is_search = true;
          this.searchLoader = false;
          console.log(data);
        }
      });
    } else {
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.getAllDrivers(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.DriverData = data.driver;
          console.log(this.DriverData);
          this.DriverLength = data.count;
        }
        this.is_search = false;
        this.searchLoader = false;
      });
    }
  }

  /**
  * For refreshing the page.
  */
  public refetchDrivers() {
    if (this.is_search == true) {
      this.searchSubmit = true;
      this.getdriver();
      this.searchSubmit = false;
    }
    else {

      this.ngOnInit();
    }
  }

  /**
   * Reset the search filters
   */
  reset() {
    this.pNo = 1;
    this.searchValue = '';
    this.device_id = '';
    this.shift_timing_id = '';
    this.driver_groups = '';
    this.blocked_status = '';
    this._id = '';
    this.driver_id = '';
    this.DriverData = [];
    this.driverForm = '';
    this.deviceForm = '';
    this.driver_status = [];
    if (this.dtc) {
      this.companyId = this.companyData.slice().map(x => x._id);
      this.companyIdFilter = this.companyId.slice()
      this.companyIdFilter.push('all');
    }
    // $('#myDropdown').ddslick('destroy');
    this.getdriver();
  }
  selectAllCompany() {
    if (this.companyId.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.companyId = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.companyId = [];
      this.companyIdFilter = [];
    }
  }

  /**
   * Sort fuction to sort by a particular field.
   * @param key
   */
  sort(key) {
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    this.searchLoader = true;
    var params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        _id: this.driver_id ? this.driver_id._id : '',
        driver_groups: this.driver_groups ? this.driver_groups._id : '',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        blocked_status: this.blocked_status,
        shift_timings_id: this.shift_timing_id ? this.shift_timing_id._id : '',
        device_id: this.device_id ? this.device_id._id : '',
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
    }

    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getAllDrivers(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.DriverData = data.driver;
        this.DriverLength = data.count;
        this.searchLoader = false;
      }
    });
  }

  /**
   * Pagination for driver module
   * @param data
   */
  pagingAgent(data) {
    this.searchLoader = true;
    this.DriverData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      _id: this.driver_id ? this.driver_id._id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      driver_groups: this.driver_groups ? this.driver_groups._id : '',
      blocked_status: this.blocked_status,
      shift_timings_id: this.shift_timing_id ? this.shift_timing_id : '',
      device_id: this.device_id ? this.device_id._id : '',
      driver_status:this.driver_status
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getAllDrivers(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.DriverData = data.driver;
      }
      this.searchLoader = false;
    });
  }

  /**
   * Generate csv for driver list.
   */
  public createCsv() {
    const params1 = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      _id: this.driver_id ? this.driver_id._id : '',
      driver_groups: this.driver_groups ? this.driver_groups._id : '',
      blocked_status: this.blocked_status,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      shift_timings_id: this.shift_timing_id ? this.shift_timing_id : '',
      fromCSV: true,
      driver_status:this.driver_status
    };
    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    
    this._driverService.getAllDriversCSV(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        let labels = [
          'ID',
          'Name',
          'Surname',
          'OIB',
          'Gender',
          'Marital Status',
          'Birth Date',
          'Nationality',
          //'POS Username',
          //'Email',
          'Company join date',
          'Vehicle plate',
          'Driver Groups',
          'Username',
          'Company',
          //'Rating',
          'Blocked Status',
          'Vehicle Model',
          'Spoken languages',
          //'Additional Services',
          'Device Id',
          'Home Address',
          'Residence',
          'Residence Expiry Date',
          'Phone Number',
          'Display Name',
          'Licence Number',
          'Licence Expiry Date',
          'Driver Status',
          'Created At'
        ];
        let resArray = [];
        for (let i = 0; i < res.driver.length; i++) {
          const csvArray = res.driver[i];

          let groupName = ''
          if (csvArray.driver_groups != '' && csvArray.driver_groups != null) {
            for (let k = 0; k < csvArray.driver_groups.length; k++) {
              if (groupName != '') {
                groupName = groupName + ',' + csvArray.driver_groups[k].name;
              } else {
                groupName = csvArray.driver_groups[k].name;
              }
            }
          }

          let spokenLanguage = ''
          if (csvArray.driver_language_id != '' && csvArray.driver_language_id != null) {
            for (let l = 0; l < csvArray.driver_language_id.length; l++) {
              if (spokenLanguage != '') {
                spokenLanguage = spokenLanguage + ',' + csvArray.driver_language_id[l].driver_language;
              } else {
                spokenLanguage = csvArray.driver_language_id[l].driver_language;
              }
            }
          }
          resArray.push
            ({
              Id: csvArray._id ? csvArray._id : 'N/A',
              name: csvArray.name ? csvArray.name : 'N/A',
              surname: csvArray.surname ? csvArray.surname : 'N/A',
              oib: csvArray.oib ? csvArray.oib : 'N/A',
              gender: csvArray.gender ? csvArray.gender : 'N/A',
              marital_status: csvArray.marital_status ? csvArray.marital_status : 'N/A ',
              birth_date: csvArray.birth_date ? csvArray.birth_date : 'N/A',
              nationality: csvArray.nationality ? csvArray.nationality : 'N/A',
              // POS Username : csvArray.
              // email : csvArray.email ? csvArray.email :'N/A',
              company_join_date: csvArray.company_join_date ? csvArray.company_join_date : 'N/A',
              vehicle_plate: csvArray.vehicle_id != null ? csvArray.vehicle_id.plate_number ? csvArray.vehicle_id.plate_number : 'N/A' : 'N/A',
              driver_groups: groupName != '' ? groupName : 'N/A',
              username: csvArray.username ? csvArray.username : 'N/A',
              company: csvArray.company_id ? csvArray.company_id.company_name : 'N/A ',
              //  rating : ,
              blocked_status: csvArray.blocked_status != null ? csvArray.blocked_status == '1' ? csvArray.blocked_status = 'Blocked' : 'Unblocked' : 'N/A',
              vehicle_id: csvArray.vehicle_id != null ? csvArray.vehicle_id.vehicle_model_id != null ? csvArray.vehicle_id.vehicle_model_id.name ? csvArray.vehicle_id.vehicle_model_id.name : 'NA' : 'N/A' : 'N/A',
              spoken_language: spokenLanguage != '' ? spokenLanguage : 'N/A',
              // additional_services : ,
              device_id: csvArray.device_id ? csvArray.device_id.unique_device_id : 'N/A',
              home_address: csvArray.home_address ? csvArray.home_address : 'N/A',
              residence: csvArray.residence ? csvArray.residence : 'N/A',
              residence_expiry_date: csvArray.residence_expiry_date ? csvArray.residence_expiry_date : 'N/A',
              phone_number: csvArray.phone_number ? csvArray.phone_number : 'N/A',
              display_name: csvArray.display_name ? csvArray.display_name : 'N/A ',
              driver_licence_number: csvArray.driver_licence_number ? csvArray.driver_licence_number : 'N/A',
              driver_licence_expiry_date: csvArray.driver_licence_expiry_date ? csvArray.driver_licence_expiry_date : 'N/A',
              drivr_status: csvArray.status ? csvArray.status : 'N/A',
              created_at: csvArray.created_at
            });
        }
        var options =
        {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: false,
          useBom: true,
          headers: (labels)
        };
        new Angular2Csv(resArray, 'Driver-List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
      }
    })
  }

  /**
   * Generate pdf for list of drivers.
   */
  public generatePDF() {
    const params1 = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      _id: this.driver_id ? this.driver_id._id : '',
      driver_groups: this.driver_groups ? this.driver_groups._id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      blocked_status: this.blocked_status,
      shift_timings_id: this.shift_timing_id ? this.shift_timing_id._id : '',
      driver_status:this.driver_status
    };
    var pdf = new jsPDF('landscape');
    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getAllDrivers(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        let resArray = [];
        for (let i = 0; i < res.driver.length; i++) {
          const pdfArray = res.driver[i];

          let groupName = ''
          if (pdfArray.driver_groups != '' && pdfArray.driver_groups != null) {
            for (let j = 0; j < pdfArray.driver_groups.length; j++) {
              if (groupName != '') {
                groupName = groupName + ',' + pdfArray.driver_groups[j].name;
              } else {
                groupName = pdfArray.driver_groups[j].name;
              }
            }
          }

          let driver_language = ''
          if (pdfArray.driver_language_id != '' && pdfArray.driver_language_id != null) {
            for (let k = 0; k < pdfArray.driver_language_id.length; k++) {
              if (driver_language != '') {
                driver_language = driver_language + ',' + pdfArray.driver_language_id[k].driver_language;
              } else {
                driver_language = pdfArray.driver_language_id[k].driver_language;
              }
            }
          }

          let additional_service = ''
          if (pdfArray.additional_service_id != '' && pdfArray.additional_service_id != null) {
            for (let l = 0; l < pdfArray.additional_service_id.length; l++) {
              if (additional_service != '') {
                additional_service = additional_service + ',' + pdfArray.additional_service_id[l].name;
              } else {
                additional_service = pdfArray.additional_service_id[l].name;
              }
            }
          }

          let personalInfo = 'Name : ' + pdfArray.name;
          personalInfo += "\r" + 'Surname : ' + pdfArray.surname;
          personalInfo += "\r" + 'OIB : ' + pdfArray.oib;
          personalInfo += "\r" + 'Gender : ' + pdfArray.gender;
          personalInfo += "\r" + 'Marital status : ' + pdfArray.marital_status;
          personalInfo += "\r" + 'Nationality : ' + pdfArray.nationality;
          personalInfo += "\r" + 'Register with WASL : ' + pdfArray.register_with_WASL;
          personalInfo += "\r" + 'Driver Groups : ' + groupName;

          let workInfo = 'Username : ' + pdfArray.username;
          workInfo += "\r" + 'Current Device Id : '
          workInfo += (pdfArray.device_id == null) ? '' : pdfArray.device_id._id;
          workInfo += "\r" + 'Company : ' + pdfArray.company_id ? pdfArray.company_id : '';
          workInfo += "\r" + 'Vehicle plate : ';
          workInfo += (pdfArray.vehicle_id == null) ? '' : pdfArray.vehicle_id.plate_number;
          workInfo += "\r" + 'Vehicle model : '
          workInfo += (pdfArray.vehicle_id == null) ? '' : (pdfArray.vehicle_id.vehicle_model_id == null ? '' : pdfArray.vehicle_id.vehicle_model_id.name);
          workInfo += "\r" + 'Last login date : '
          workInfo += (pdfArray.shift_timings_id == null) ? '' : pdfArray.shift_timings_id.start_time;
          workInfo += "\r" + 'Last logout date : '
          workInfo += (pdfArray.shift_timings_id == null) ? '' : pdfArray.shift_timings_id.end_time;

          let contactInfo = 'Home address : ' + pdfArray.home_address;
          contactInfo += "\r" + 'Residence : ' + pdfArray.residence;
          contactInfo += "\r" + 'Residence expiry date : ' + pdfArray.residence_expiry_date;
          contactInfo += "\r" + 'Phone number : ' + pdfArray.phone_number;
          contactInfo += "\r" + 'E-mail : ' + pdfArray.email;
          contactInfo += "\r" + 'VPN : ' + pdfArray.vpn_number;
          contactInfo += "\r" + 'Display name : ' + pdfArray.display_name;
          contactInfo += "\r" + 'Driver licence number : ' + pdfArray.driver_licence_number;
          contactInfo += "\r" + 'Driver licence expiry date : ' + pdfArray.driver_licence_expiry_date;

          let otherInfo = 'Spoken Language : '
          otherInfo += (driver_language == null) ? '' : driver_language;
          otherInfo += "\r" + 'Shift Assignment : '
          otherInfo += (pdfArray.shift_timings_id == null) ? '' : pdfArray.shift_timings_id.name;
          otherInfo += "\r" + 'Additional Service : '
          otherInfo += additional_service;
          otherInfo += "\r" + 'Status : '
          otherInfo += pdfArray.blocked_status ? (pdfArray.blocked_status == '1' ? 'Blocked' : 'Unblocked') : '';

          resArray.push
            ({
              PersonalInfo: personalInfo,
              WorkInfo: workInfo,
              ContactInfo: contactInfo,
              OtherInfo: otherInfo
            });
        }

        var col = [
          { title: "Personal Info", dataKey: "PersonalInfo" },
          { title: "WorkInfo", dataKey: "WorkInfo" },
          { title: "Contact Info", dataKey: "ContactInfo" },
          { title: "Other Info", dataKey: "OtherInfo" }
        ];
        var options = {
          theme: 'striped',
          title: 'bold',
          styles: {
            lineColor: 0,
            halign: 'left',
            valign: 'top',
            overflow: 'linebreak',
            fontStyle: 'bold',
          },
          columnStyles: {
            PersonalInfo: { columnWidth: 60 },
            WorkInfo: { columnWidth: 60 },
            ContactInfo: { columnWidth: 80 },
            OtherInfo: { columnWidth: 70 }
          }
        };
        pdf.text(120, 10, "Driver - List");
        pdf.autoTable(col, resArray, options);
        pdf.save('Driver_list' + moment().format('YYYY-MM-DD_HH_mm_ss'));
      }
    });
  }
  openChangeStatusDialog(driverData): void {
    let dialogRef = this.dialog.open(EditStatusDialogComponent, {
      width: '400px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: driverData
    });
    dialogRef.afterClosed().subscribe(result => {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.searchForDriver(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.DriverData = data.driver;
        }
      });
    });
  }
  ngAfterViewInit() {
    let that = this;
    that.socket.on('driver_force_logout', function (data) {
      if (!data || data == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      }
      data = that.encDecService.sockDwt(that.socket_session_token, data);
      if (Object.keys(data).length !== 0) {
        if (data.data.company_id === window.localStorage['user_company']) {
          if (data) {
            let driverid = data.data.driver_id;
            let logged_out = data.data.shift_end
            if (data.data.block_response === "Trip") {
              that.removeDriverFromLogout(driverid)
              that.toastr.error("Driver cant be force logout now..is on an active trip");
            }
            else if (data.data.block_response === 'RC') {
              that.removeDriverFromLogout(driverid);
              that.toastr.error('Force logout operation failed, Temporary block initiated');
              //return false;
            } else {
              that.removeDriverFromLogout(driverid)
              that.toastr.success("Driver force logout successfully");
              that.DriverData.forEach(driver => {
                if (driver._id === driverid) {
                  driver.status = "logout";
                  if (logged_out != null || logged_out != '') {
                    driver.shift_timings_id.end_time = data.data.shift_end;
                  }
                }
              })
            }
          }
        }
      } else {
        let param_socket = {
          company_id: that.companyId
        }
        let enc_data = that.encDecService.nwt(that.session, param_socket);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: that.email
        }
        that._usersService.getSocketsession(data1).then((res1) => {
          if (res1 && res1.status == 200) {
            var res_data1: any = that.encDecService.dwt(that.session, res1.data);
            if (res1.status == 200) {
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              that.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          }
        })
      }
    });
    that.socket.on('driver_block', function (data) {
      if (!data || data == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      }
      data = that.encDecService.sockDwt(that.socket_session_token, data);
      if (Object.keys(data).length !== 0) {
        if (data.data.company_id === window.localStorage['user_company']) {
          if (data) {
            let driverid = data.data.driver_id;
            let logged_out = data.data.shift_end;
            if (data.data.block_response === "Trip") {
              that.removeBlockedDriverFromLocal(driverid)
              that.toastr.error("Driver cant be blocked now..is on an active trip");
            }
            else if (data.data.block_response === 'RC') {
              that.removeBlockedDriverFromLocal(driverid)
              that.toastr.error('Block operation failed, Temporary block initiated');
            } else {
              that.toastr.success("Driver blocked successfully");
              that.removeBlockedDriverFromLocal(driverid)
              that.DriverData.forEach(driver => {
                if (driver._id === driverid) {
                  driver.blocked_status = "1";
                  driver.status = "logout";
                  if (logged_out != null || logged_out != '') {
                    driver.shift_timings_id.end_time = data.data.shift_end;
                  }
                }
              })
            }
          }
        }
      } else {
        let param_socket = {
          company_id: that.companyId
        }
        let enc_data = that.encDecService.nwt(that.session, param_socket);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: that.email
        }
        that._usersService.getSocketsession(data1).then((res1) => {
          if (res1 && res1.status == 200) {
            var res_data1: any = that.encDecService.dwt(that.session, res1.data);
            if (res1.status == 200) {
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              that.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          }
        })
      }
    });
  }
  DriverGroup(driver) {
    this.dialog.closeAll();
    /*let dialogRef = this.dialog.open(EditDriverGroupComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: driver
    });*/
    let data = {
      id: driver,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    }
    let dialogRef = this.dialog.open(EditDriverGroupComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      const params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.getAllDrivers(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.DriverData = data.driver;
          this.DriverLength = data.count;
        }
      });
    });
  }
  checkIfLogoutPressed(id) {
    let index = this.logoutList.indexOf(id)
    if (index > -1) {
      return true;
    }
    return false;
  }
  checkIfBlockPressed(id) {
    let index = this.blockList.indexOf(id)
    if (index > -1) {
      return true;
    }
    return false;
  }
  removeDriverFromLogout(id) {
    let index = this.logoutList.indexOf(id)
    if (index > -1)
      this.logoutList.splice(index, 1)
  }
  removeBlockedDriverFromLocal(id) {
    let index = this.blockList.indexOf(id)
    if (index > -1) {
      this.blockList.splice(index, 1);
    }
  }
  ngOnDestroy() {
    localStorage.setItem("blockedDrivers", JSON.stringify(this.blockList));
    localStorage.setItem("logoutDrivers", JSON.stringify(this.logoutList));
  }
  public refreshInterval;
  refreshBlockList() {
    clearInterval(this.refreshInterval);
    this.refreshInterval = setInterval(() => {
      this.logoutList = [];
      this.blockList = [];
    }, 60000);
  }
  forceShiftClose(id) {
    let params = {
      driver_id: id,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.ForceShiftClose(enc_data).then((dec) => {
      if (dec && dec.status === 200) {
        this.toastr.success('success');
      }
      else {
        this.toastr.success('some error occurred');
      }
    });
  }
  displayFnCompany(data): string {
    return data ? data.company_name : data;
  }
  changeCompany() {
    this.DriverList = [];
    this.driverGroupData = [];
    this.deviceData = [];
    this.ShiftTimingsData = [];
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
          if (this.dtc && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher'  && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
            this.companyId = data.getCompanies.map(x => x._id);
            this.companyIdFilter = data.getCompanies.map(x => x._id)
            this.companyIdFilter.push('all')
          }
          this.getdriver();
        }
      }
    });
  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.companyId = this.companyIdFilter.slice()
    }
    else {
      this.companyId = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }
}

