import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AdditionalService } from '../../../../../../common/services/additional_service/additional_service.service';
import { DriverService } from '../../../../../../common/services/driver/driver.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-additional-service',
  templateUrl: './edit-additional-service.component.html',
  styleUrls: ['./edit-additional-service.component.css']
})
export class EditAdditionalServiceComponent implements OnInit {
  public driverAddData;
  public serviceGroupLength;
  public serviceGroupData;
  public editForm: FormGroup;
  private sub: Subscription;
  public additionalData;
  public searchSubmit = false;
  public additional_service_id;
  public companyId: any = [];
  public Data;
  public driverAddData1;
  session: string;
  email: string;

  constructor(
    public _additionalService: AdditionalService,
    public _driverService: DriverService,
    public dialogRef: MatDialogRef<EditAdditionalServiceComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private router: Router,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    const company_id: any = data.company_id;
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    company_id.forEach(element => {
      this.companyId.push(element);
    });
    this.driverAddData1 = data.id;
    this.editForm = formBuilder.group({
      additional_service_id: ['']
    });
  }

  public ngOnInit(): void {
    this.driverAddData = this.driverAddData1;
    this.getAdditionalService();
    var params = {
      company_id: this.companyId,
      _id: this.driverAddData._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDrivergroupsById(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        this.Data = res.getDriverss.additional_service_id;
        this.editForm.setValue({
          additional_service_id: this.Data ? this.Data : '',
        });
      }
    })
  }

  public updateDriver() {

    if (!this.editForm.valid) {
      this.toastr.error('All fields are required.');
      return;
    }

    const driverData = {
      additional_service_id: this.editForm.value.additional_service_id ? this.editForm.value.additional_service_id : null,
      company_id: this.companyId,
      _id: this.driverAddData._id
    }
    var encrypted = this.encDecService.nwt(this.session, driverData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.updateAdditionalService(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status === 200) {
            this.toastr.success('Additional service updated successfully.');
            setTimeout(() => {
              this.dialogRef.close();
            }, 1000);
          } else if (dec.status === 201) {
            this.toastr.error('Additional service updation failed.');
          }
          this.router.navigate(['/admin/fleet/drivers']);
        }
      })
      .catch((error) => {
      });
  }

  closePop() {
    this.dialogRef.close();
  }

  public deleteAdditionalService(index, _id) {
    this.searchSubmit = true;
    this.driverAddData.additional_service_id.splice(index, 1);
    var i = this.Data.indexOf(_id);
    this.Data.splice(i, 1);
    this.editForm.controls['additional_service_id'].setValue(this.Data);
  }

  /**
   * get additional service  list
   * @param
   */
  public getAdditionalService() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._additionalService.getaddService(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.serviceGroupData = data.getService;
        this.serviceGroupLength = data.count;
      }
    });
  }
}
