
import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { CustomerRatingService } from '../../../../../../common/services/customer-rating/customerrating.service';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-customer-rating',
  templateUrl: './customer-rating.component.html',
  styleUrls: ['./customer-rating.component.css']
})
export class CustomerRatingComponent implements OnInit {
  public companyId: any = [];
  public driverData;
  public customerRatingData;
  public customerRatingLength;
  session: string;
  email: string;
  constructor(
    public encDecService: EncDecService,
    public dialogRef: MatDialogRef<CustomerRatingComponent>,
    private toastr: ToastsManager,
    private router: Router,
    private _customerRatingService: CustomerRatingService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public jwtService: JwtService) {
    this.driverData = data;
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }
  ngOnInit() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      driver_id: this.driverData._id,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._customerRatingService.getCustomerRatings(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.customerRatingData = data.customerRatingDetails;
        this.customerRatingLength = this.customerRatingData.length;
      }
    });
  }
  closePop() {
    this.dialogRef.close();
  }
}

