import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { OrderService } from '../../../../../../common/services/order/order.service';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-order-status-history',
  templateUrl: './order-status-history.html',
  styleUrls: ['./order-status-history.css'],
})
export class OrderStatusHistoryComponent implements OnInit {

  public driverData;
  public orderHistoryData;
  public orderHistoryLength;
  public companyId: any = [];
  email: string;
  session: string;
  constructor(
    public dialogRef: MatDialogRef<OrderStatusHistoryComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private router: Router,
    private _orderService: OrderService,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public jwtService: JwtService) {
    this.driverData = data;
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }
  ngOnInit() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      driver_id: this.driverData._id,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.orderHistoryByDriverId(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.orderHistoryData = data.getOrderStatusHistory;
        this.orderHistoryLength = this.orderHistoryData.length;
      }
    });
  }
  closePop() {
    this.dialogRef.close();
  }
}
