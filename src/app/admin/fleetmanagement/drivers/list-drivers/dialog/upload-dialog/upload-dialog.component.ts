import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DriverService } from '../../../../../../common/services/driver/driver.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.css']
})
export class UploadDialogComponent implements OnInit {
  public editForm: FormGroup;
  public formData;
  public companyId: any = [];
  private sub: Subscription;
  session: string;
  email: string;
  constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    private _driverService: DriverService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      file: [null, Validators.required]
    });
    this.formData = new FormData();
  }

  ngOnInit() {

  }
  public uploadfile() {
    const formData: FormData = new FormData();
    formData.append('file', this.fileToUpload, this.fileToUpload.name);
    formData['company_id'] = this.companyId;
    var encrypted = this.encDecService.nwt(this.session, formData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.uploadDriverList(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status === 200) {
            var res: any = this.encDecService.dwt(this.session, dec.data);
            this.toastr.success('Drivers added Successfully');
            setTimeout(() => {
              this.dialogRef.close();
              this.router.navigate(['/admin/fleet/drivers']);
            }, 1000);
          } else if (dec.status === 201) {
            this.toastr.error('Duplicate Plate number');
            this.dialogRef.close();
          } else if (dec.status === 202) {
            this.toastr.error('Driver adding failed');
            this.router.navigate(['/admin/fleet/drivers']);
          }
        }
      })
      .catch((error) => {
      });
  }
  closePop() {
    this.dialogRef.close();
  }
  public fileToUpload: File = null;
  handleFileInput(event) {
    this.fileToUpload = event.target.files[0];
    //alert(JSON.stringify(this.fileToUpload));
  }
}


