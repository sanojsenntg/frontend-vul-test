import { Component, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ShiftsService } from '../../../../../../common/services/shifts/shifts.service';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-shift-info',
  templateUrl: 'shift-info.html',
  styleUrls: ['./shift-info.css']
})
export class ShiftInfoComponent {
  public driverData;
  public driver_id;
  public currentPage;
  public pageSize = 5;
  public pageNo = 1;
  public ShiftData;
  public Shiftlength;
  public companyId: any = [];
  email: string;
  session: string;
  constructor(
    public _shiftsService: ShiftsService,
    public dialogRef: MatDialogRef<ShiftInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public toastr: ToastsManager,
    private router: Router,
    public jwtService: JwtService,
    public encDecService: EncDecService
  ) {
    this.driverData = data;
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }

  ngOnInit() {
    this.driver_id = this.driverData._id;
    let params = {
      driver_id: this.driver_id,
      offset: 0,
      limit: 5,
      sortByColumn: '_id',
      sortOrder: 'descending',
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftsService.getshiftinfo(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ShiftData = data.shifts;
        this.Shiftlength = data.count;
      }
    });
  }

  pagingAgent(data) {
    this.currentPage = data;
    this.ShiftData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params_pagingAgent = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      driver_id: this.driver_id,
      order_status: 'all',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params_pagingAgent);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftsService.getshiftinfo(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ShiftData = data.shifts;
      }
    });
  }


  closePop() {
    this.dialogRef.close();
  }
}
