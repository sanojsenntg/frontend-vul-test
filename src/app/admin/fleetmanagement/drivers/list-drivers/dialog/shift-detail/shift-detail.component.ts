import { Component, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ShiftsService } from '../../../../../../common/services/shifts/shifts.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-shift-detail',
  templateUrl: './shift-detail.component.html',
  styleUrls: ['./shift-detail.component.css']
})
export class ShiftDetailComponent implements OnInit {
  public companyId: any = [];
  public driverData;
  public driver_id;
  public currentPage;
  public pageSize = 5;
  public pageNo = 1;
  public ShiftData;
  public Shiftlength;
  session: string;
  email: string;
  constructor(
    public _shiftsService: ShiftsService,
    public dialogRef: MatDialogRef<ShiftDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public toastr: ToastsManager,
    private router: Router,
    public encDecService: EncDecService,
    public jwtService: JwtService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.driverData = data;
  }

  ngOnInit() {
    this.driver_id = this.driverData._id;
    let params = {
      driver_id: this.driver_id,
      offset: 0,
      limit: 5,
      sortByColumn: '_id',
      sortOrder: 'desc',
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftsService.getShiftDetailByDriverId(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ShiftData = data.getShifts;
      }
    });
  }

  /**
   * This function is used to create CSV
   * @param message
   * @param action
   */
  public createCsv() {
    let labels = [
      'Driver name',
      'Vehicle',
      'Vehicle id',
      'Device name',
      'Device id',
      'Company',
      'Occupied time',
      'Free time',
      'First start',
      'Last stop',
      'Orders distance',
      'Total distance',
      'Driver formated',
      'Drive',
      'Bill not printed count',
      'No payment',
      'Free rides',
      'Status',
      'Total',
      'Payed',
      'To pay',
      'Shift duration',
      'Trip full paid by credit',
      'Trip partially paid by credit',
      'Credit total',
      'Cash trip total',
      'Credit',
      'Bill printed',
      'Bill printed count',
      'Bill not printed'
    ];
    let resArray = [];
    resArray.push
      ({
        driver: this.ShiftData.driver_id ? this.ShiftData.driver_id.name : 'N/A',
        vehicleName: this.ShiftData.vehicle_id ? this.ShiftData.vehicle_id.display_name : 'N/A',
        VehicleId: this.ShiftData.vehicle_id ? this.ShiftData.vehicle_id.vehicle_unique_id : 'N/A',
        DeviceName: this.ShiftData.device_id ? this.ShiftData.device_id.device_model : 'N/A',
        DeviceId: this.ShiftData.device_id ? this.ShiftData.device_id.unique_device_id : 'N/A',
        Company: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.company : 'N/A',
        OccupiedTime: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.occupied_time : 'N/A',
        FreeTime: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.free_time : 'N/A',
        FirstStart: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.first_start : 'N/A',
        LastStop: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.last_stop : 'N/A',
        OrdersDistance: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.orders_distance : 'N/A',
        ToatalDistance: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.total_distance : 'N/A',
        DriverFormated: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.driver_formated : 'N/A',
        Drive: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.drive : 'N/A',
        BillNotPrintedCount: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.bill_not_printed_count : 'N/A',
        NoPayment: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.no_payment : 'N/A',
        FreeRides: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.free_rides : 'N/A',
        Status: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.status : 'N/A',
        Toatal: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.total : 'N/A',
        Payed: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.payed : 'N/A',
        ToPay: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.to_pay : 'N/A',
        ShiftDuration: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.shift_duration : 'N/A',
        TripFullPaidByCredit: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.trip_full_paid_by_credit : 'N/A',
        TripPartiallyPaidByCredit: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.trip_partially_paid_by_credit : 'N/A',
        CreditTotal: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.credit_total : 'N/A',
        CashTripTotal: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.cash_trip_total : 'N/A',
        Credit: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.credit : 'N/A',
        BillPrinted: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.bill_printed : 'N/A',
        BillPrintedCount: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.bill_printed_count : 'N/A',
        BillNotPrinted: this.ShiftData.shift_details_id ? this.ShiftData.shift_details_id.bill_not_printed : 'N/A'
      });
    var options =
    {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      headers: (labels)
    };
    new Angular2Csv(resArray, 'Shift - Detail', options);
  }

  closePop() {
    this.dialogRef.close();
  }
}

