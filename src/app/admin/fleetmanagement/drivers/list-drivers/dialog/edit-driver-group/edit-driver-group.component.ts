import { Component, Inject, OnInit, ViewContainerRef } from '@angular/core';
import { DriverService } from "../../../../../../common/services/driver/driver.service";
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-driver-group',
  templateUrl: './edit-driver-group.component.html',
  styleUrls: ['./edit-driver-group.component.css']
})
export class EditDriverGroupComponent implements OnInit {
  public driver_groups;
  public companyId: any = [];
  email: string;
  session: string;
  constructor(
    private _driverService: DriverService,
    formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditDriverGroupComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private router: Router,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    const company_id: any = data.company_id//localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    company_id.forEach(element => {
      this.companyId.push(element);
    });
    this._id = data.id._id;
    this.driverData = data.id;
    this.toastr.setRootViewContainerRef(vcr);
  }
  public driverGroupData;
  public tempdriverGroupData;
  public driverGroupDataLength;
  public driver_groups_id;
  public PositionArray = [];
  public _id;
  public driverData;
  ngOnInit() {
    this.getDriverGroupsForListing();
    this.driver_groups = this.driverData.driver_groups
      ? this.driverData.driver_groups
      : "";
  }
  public getDriverGroupsForListing() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'unique_driver_model_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriversGroup(enc_data).subscribe(dec => {
      var data: any = this.encDecService.dwt(this.session, dec.data);
      if (dec && dec.status == 200) {
        this.driverGroupData = data.getDriverGroups;
        this.tempdriverGroupData = data.getDriverGroups;
        this.driverGroupDataLength = this.tempdriverGroupData;
      }
    });
  }
  closePop() {
    this.dialogRef.close();
  }
  updateDriver() {
    if (!this.driver_groups) {
      this.toastr.error("Please fill in the required field. ");
      return;
    }
    else {
      this.driver_groups_id = "";
      if (this.driver_groups.length > 0) {
        for (var i = 0; i < this.tempdriverGroupData.length; ++i) {
          for (var w = 0; w < this.driver_groups.length; ++w) {
            if (this.tempdriverGroupData[i]._id === this.driver_groups[w]) {
              this.PositionArray.push(i);
            }
          }
          if (i === this.tempdriverGroupData.length - 1) {
            for (let j = this.tempdriverGroupData.length - 1; j >= 0; j--) {
              let index = this.PositionArray.indexOf(j)
              if (index !== -1)
                this.driver_groups_id = this.driver_groups_id + "1";
              else
                this.driver_groups_id = this.driver_groups_id + "0";
            }

          }
        }
      } else {
        for (var i = 0; i < this.tempdriverGroupData.length; ++i) {
          this.driver_groups_id = this.driver_groups_id + "0";
        }
      }
      let param = {
        driver_id: this._id,
        driver_groups: this.driver_groups
          ? this.driver_groups
          : null,
        driver_groups_id: this.driver_groups_id ? "a" + this.driver_groups_id : 0,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, param);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.updateDriverGroup(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          this.toastr.success('Driver group updated successfully.');
          setTimeout(() => {
            this.closePop()
          }, 1000);
        }
      }).catch((error) => {
        this.toastr.error(error);

      });
    }
  }
  compareWithFunc(a, b) {
    return a === b._id;
  }
}
