import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDriverGroupComponent } from './edit-driver-group.component';

describe('EditDriverGroupComponent', () => {
  let component: EditDriverGroupComponent;
  let fixture: ComponentFixture<EditDriverGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDriverGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDriverGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
