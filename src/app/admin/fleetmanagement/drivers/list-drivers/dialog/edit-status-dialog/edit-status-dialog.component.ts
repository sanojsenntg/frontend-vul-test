import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DriverService } from '../../../../../../common/services/driver/driver.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-status-dialog',
  templateUrl: './edit-status-dialog.component.html',
  styleUrls: ['./edit-status-dialog.component.css']
})
export class EditStatusDialogComponent implements OnInit {
  public driverData;
  public driverData1;
  public editForm: FormGroup;
  private sub: Subscription;
  public searchSubmit = false;
  public statusinfo = {
    status: ''
  };
  public Data;
  public vehicleAddData1;
  public companyId: any = [];
  session: string;
  email: string;

  constructor(
    public _DriverService: DriverService,
    public dialogRef: MatDialogRef<EditStatusDialogComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private router: Router,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.driverData = data;
    this.editForm = formBuilder.group({
      status: ['']
    });
  }
  public ngOnInit(): void {
    this.driverData1 = this.driverData;
  }
  closePop() {
    this.dialogRef.close();
  }
  public updateDriverStatus() {
    if (!this.editForm.valid || this.statusinfo.status == undefined || this.statusinfo.status == '') {
      this.toastr.error('Select status to update');
      return;
    }
    var user = JSON.parse(window.localStorage['adminUser']);
    if (!user) {
      this.toastr.error("Session Expired..please login again");
    } else {
      const DriverData = {
        status: this.statusinfo.status ? this.statusinfo.status : 'A',
        email: user.email,
        user_id: user._id,
        priority: user.priority,
        company_id: this.companyId,
        _id: this.driverData1._id
      }
      var encrypted = this.encDecService.nwt(this.session, DriverData);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      //alert(JSON.stringify(this.driverData));
      this._DriverService.updateDriverStatus(enc_data)
        .then((dec) => {
          if (dec) {
            if (dec.status === 200) {
              var res: any = this.encDecService.dwt(this.session, dec.data);
              this.toastr.success('Driver status updated successfully.');
              setTimeout(() => {
                this.dialogRef.close();
              }, 1000);
            } else if (dec.status === 201) {
              this.toastr.error('Driver status updation failed.');
            }
            else if (dec.status === 202) {
              this.toastr.error(dec.message);
            }
            else if (dec.status === 203) {
              this.toastr.warning(dec.message);
            }
            this.router.navigate(['/admin/fleet/drivers']);
          }
        })
        .catch((error) => {
        });
    }
  }
}
