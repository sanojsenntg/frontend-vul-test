import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverService } from '../../../../common/services/driver/driver.service';
import { ToastsManager } from 'ng2-toastr';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { Router } from '@angular/router';
import { FileHolder } from 'angular2-image-upload';
import { ShiftTimingsService } from '../../../../common/services/shift-timings/shift-timings.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.component.html',
  styleUrls: ['./add-driver.component.css']
})
export class AddDriverComponent implements OnInit {
  public dtc = false;
  public is_temp_enable = false;
  public companyId: any = [];
  public maxDate = new Date();
  public DriverForm: FormGroup;
  public VehicleData;
  public driverGroupData;
  public ShiftTimingsData;
  public dateObj = new Date();
  public spandata = 'no';
  public CompanyData;
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',

  };
  public myBirthDatePickerOptions: IMyDpOptions = {
    disableSince: { year: this.dateObj.getUTCFullYear(), month: this.dateObj.getUTCMonth() + 1, day: this.dateObj.getUTCDate() },
    // other options...
    dateFormat: 'yyyy-mm-dd',

  };

  public myDatePickerExpiryOptions: IMyDpOptions = {
    disableUntil: { year: this.dateObj.getUTCFullYear(), month: this.dateObj.getUTCMonth() + 1, day: this.dateObj.getUTCDate() },
    // other options...
    dateFormat: 'yyyy-mm-dd',

  };
  email: string;
  session: string;

  constructor(formBuilder: FormBuilder,
    private _shiftTimingsService: ShiftTimingsService,
    private router: Router,
    private _driverService: DriverService,
    private toastr: ToastsManager,
    private _companyservice: CompanyService,
    private vcr: ViewContainerRef,
    public jwtService: JwtService,
    private _vehicleService: VehicleService,
    public encDecService: EncDecService
  ) {
    this.toastr.setRootViewContainerRef(vcr);
    const company_id: any = localStorage.getItem('user_company');
    if (company_id === '5ce12918aca1bb08d73ca25d' || company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.DriverForm = formBuilder.group({
      username: ['', [
        Validators.required,
      ]],
      password: ['', [
        Validators.required,
      ]],
      name: ['', [
        Validators.required,
      ]],
      surname: ['', []],
      display_name: ['', [
        Validators.required,
      ]],
      email: ['', [
        Validators.required,
      ]],
      oib: ['', []],
      gender: ['', [
        Validators.required,
      ]],
      marital_status: ['Single', [
        Validators.required,
      ]],
      emp_id: ['', [
        Validators.required,
      ]],
      birth_date: ['', [
      ]],
      nationality: ['', [
      ]],
      home_address: ['', [
      ]],
      residence: ['', [
      ]],
      residence_expiry_date: ['', [
      ]],
      company_join_date: ['', [
      ]],
      vehicle_id: ['', [
      ]],
      driver_licence_number: ['', [
      ]],
      driver_licence_expiry_date: ['', [
      ]],
      phone_number: ['', [
        Validators.required, Validators.minLength(9), Validators.maxLength(9)
      ]],
      vpn_number: ['', [
      ]],
      company: [],
      register_with_WASL: ['', [
      ]],
      profile_picture: [''],
      shift_timings_id: [],
      driver_groups: [''],
      company_id: [this.companyId],
      is_temperory: "0",
      account_details: '',
      iban: '',
      permit_number: ['', [
      ]],
      permit_expiry_date: ['', [
      ]],
      emirates_id: ['', [
      ]],
      emirates_id_expiry: ['', [
      ]],
    });
  }
  ngOnInit() {
    this.getDriverGroups();
    let params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.vehicleListing(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.VehicleData = data;
      }
    });
    const Shiftparams = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted1 = this.encDecService.nwt(this.session, Shiftparams);
    var enc_data1 = {
      data: encrypted1,
      email: this.email
    }
    this._shiftTimingsService.getShiftTiming(enc_data1).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ShiftTimingsData = data.shift_timings;
      }
    });
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted2 = this.encDecService.nwt(this.session, param);
    var enc_data2 = {
      data: encrypted2,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data2).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CompanyData = data.getCompanies;
      }
      //this.searchSubmit = false;
    });
  }

  /**
   * To add the driver records
   */
  saveDriver(data, drivergroups) {
    let driver_groups_id = "";
    let PositionArray = [];
    if (data.driver_groups.length > 0) {
      for (var i = 0; i < drivergroups.length; ++i) {
        for (var w = 0; w < data.driver_groups.length; ++w) {
          if (drivergroups[i]._id === data.driver_groups[w]) {
            PositionArray.push(i);
          }
        }
        if (i === drivergroups.length - 1) {
          for (let j = drivergroups.length - 1; j >= 0; j--) {
            let index = PositionArray.indexOf(j)
            if (index !== -1)
              driver_groups_id = driver_groups_id + "1";
            else
              driver_groups_id = driver_groups_id + "0";
          }

        }
      }
    } else {
      for (var i = 0; i < drivergroups.length; ++i) {
        driver_groups_id = driver_groups_id + "0";
      }
    }
    const params = {
      username: data.username,
      password: data.password,
      name: data.name,
      surname: data.surname,
      display_name: data.display_name,
      email: data.email,
      oib: data.oib,
      gender: data.gender,
      marital_status: data.marital_status,
      birth_date: data.birth_date.formatted,
      emp_id: data.emp_id,
      nationality: data.nationality,
      home_address: data.home_address,
      residence: data.residence,
      residence_expiry_date: data.residence_expiry_date?data.residence_expiry_date.formatted?data.residence_expiry_date.formatted:data.residence_expiry_date:'',
      company_join_date: data.company_join_date.formatted,
      driver_licence_number: data.driver_licence_number,
      driver_licence_expiry_date: data.driver_licence_expiry_date.formatted,
      phone_number: data.phone_number,
      vpn_number: data.vpn_number,
      //company: data.company,
      emirates_id : data.emirates_id,
      emirates_id_expiry : data.emirates_id_expiry.formatted,
      vehicle_id: data.vehicle_id ? data.vehicle_id : null,
      register_with_WASL: data.register_with_WASL,
      profile_picture: data.profile_picture,
      driver_groups: data.driver_groups ? data.driver_groups : null,
      shift_timings_id: data.shift_timings_id ? data.shift_timings_id : null,
      driver_groups_id: driver_groups_id ? "a" + driver_groups_id : 0,
      company: data.company ? [data.company] : this.companyId,
      is_temperory: data.is_temperory ? data.is_temperory : "0",
      account_details: data.account_details ? data.account_details : "",
      iban: data.iban ? data.iban : '',
      permit_number: data.permit_number,
      permit_expiry_date: data.permit_expiry_date?data.permit_expiry_date.formatted?data.permit_expiry_date.formatted:data.permit_expiry_date:'',
    };
    return params;
  }
  addDriver() {
    if (!this.DriverForm.valid) {
      this.toastr.error('Please fill in the required field. ');
      return;
    }
    else {
      if (this.DriverForm.value.is_temperory == "1" && (!this.DriverForm.value.account_details || !this.DriverForm.value.iban)) {
        this.toastr.error('Account Details required for freelance Drivers');
        return;
      }
      var params = this.saveDriver(this.DriverForm.value, this.driverGroupData);
      params['company_id'] = this.companyId;
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.saveDriver(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 203) {
            this.toastr.warning('Driver is already registered with same username.');
          }
          else if (dec.status == 205) {
            this.toastr.warning('Driver is already registered with same phone number.');
          }
          else if (dec.status == 206) {
            this.toastr.warning('Driver is already registered with same email.');
          }
          else if (dec.status == 207) {
            this.toastr.warning('Driver is already registered with Employee ID');
          }
          else if (dec.status == 200) {
            this.toastr.success('Driver Added successfully.');
            setTimeout(() => {
              this.router.navigate(['/admin/fleet/drivers']);
            }, 1000);
          }
          else {
            this.toastr.error(dec.message);
          }
        }
      });
    }

  }
  imageFinishedUploading(file: FileHolder) {
    this.DriverForm.controls['profile_picture'].setValue(file.src);
    this.spandata = 'yes';
  }

  onRemoved(file: FileHolder) {
    // this.EditDriverForm.value.profile_picture = '';
    this.DriverForm.controls['profile_picture'].setValue(null);
    this.spandata = 'no';
    // do some stuff with the removed file.
  }

  onUploadStateChanged(state: boolean) {
  }


  /**
  * Function To Get Driver Groups For Dropdown
  *
  */
  public getDriverGroups() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'unique_driver_model_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriversGroup(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.driverGroupData = data.getDriverGroups;
      }
    });
  }
  public checkfreelance(data) {
    if (data == "1") {
      this.is_temp_enable = true;
    } else {
      this.is_temp_enable = false;
    }
  }
}
