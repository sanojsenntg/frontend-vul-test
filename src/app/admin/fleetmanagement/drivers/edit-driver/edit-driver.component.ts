import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs/Subscription";
import { DriverService } from "../../../../common/services/driver/driver.service";
import { DriverGroupService } from "../../../../common/services/driver_groups/driver_groups.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { VehicleService } from "../../../../common/services/vehicles/vehicle.service";
import { ToastsManager } from "ng2-toastr";
import { IMyDpOptions } from "mydatepicker";
import { FileHolder } from "angular2-image-upload";
import { environment } from "../../../../../environments/environment";
import { JwtService } from "../../../../common/services/api/jwt.service";
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { EncDecService } from "../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service";


@Component({
  selector: "app-edit-driver",
  templateUrl: "./edit-driver.component.html",
  styleUrls: ["./edit-driver.component.css"]
})
export class EditDriverComponent implements OnInit {
  public dtc = false;
  public companyId: any = [];
  public EditDriverForm: FormGroup;
  private sub: Subscription;
  public _id;
  public DriverData;
  public driverGroupData;
  public tempdriverGroupData;
  public PositionArray = [];
  public CompanyData;
  public driverGroupDataLength;
  public driver_groups_id;
  public VehicleData;
  public editDriverdata;
  public imageUrl = "";
  public spandata = "no";
  public dateObj = new Date();
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: "yyyy-mm-dd"
  };
  public myBirthDatePickerOptions: IMyDpOptions = {
    disableSince: {
      year: this.dateObj.getUTCFullYear(),
      month: this.dateObj.getUTCMonth() + 1,
      day: this.dateObj.getUTCDate()
    },

    // other options...
    dateFormat: "yyyy-mm-dd"
  };

  public myDatePickerExpiryOptions: IMyDpOptions = {
    disableUntil: {
      year: this.dateObj.getUTCFullYear(),
      month: this.dateObj.getUTCMonth() + 1,
      day: this.dateObj.getUTCDate()
    },

    // other options...
    dateFormat: "yyyy-mm-dd"
  };
  session: string;
  email: string;
  message: string;

  constructor(
    formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private _driverService: DriverService,
    public jwtService: JwtService,
    private _companyservice: CompanyService,
    private _driverGroupService: DriverGroupService,
    private _vehicleService: VehicleService,
    public encDecService: EncDecService
  ) {
    const company_id: any = localStorage.getItem('user_company');
    if (company_id === '5ce12918aca1bb08d73ca25d' || company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.toastr.setRootViewContainerRef(vcr);
    this.EditDriverForm = formBuilder.group({
      username: ["", [Validators.required]],
      password: [""],
      name: ["", [Validators.required]],
      surname: ["", []],
      display_name: ["", [Validators.required]],
      email: ["", [Validators.required]],
      oib: ["", []],
      gender: ["", [Validators.required]],
      marital_status: ["Single"],
      birth_date: ["", []],
      nationality: ["", []],
      home_address: ["", []],
      residence: ["", []],
      residence_expiry_date: ["", []],
      company_join_date: ["", []],
      vehicle_id: ["", []],
      driver_licence_number: ["", []],
      driver_licence_expiry_date: ["", []],
      phone_number: [
        "",
        [Validators.required, Validators.minLength(9), Validators.maxLength(9)]
      ],
      vpn_number: ["", []],
      company: ["", []],
      register_with_WASL: ["", []],
      profile_picture: [""],
      driver_groups: [""],
      permit_number: [""],
      emp_id: ["", [Validators.required]],
      permit_expiry_date: [""],
      emirates_id:[""],
      emirates_id_expiry:[""]
    });
  }

  ngOnInit() {
    this.spandata = "no";
    this.getDriverGroupsForListing();
    this.sub = this.route.params.subscribe(params => {
      this._id = params["id"];
    });
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CompanyData = data.getCompanies;
      }
      //this.searchSubmit = false;
    });
    const params = {
      offset: 0,
      limit: 0,
      driver_id: this._id,
      sortOrder: "desc",
      sortByColumn: "_id"
    };
    var id = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted1 = this.encDecService.nwt(this.session, id);
    var enc_data1 = {
      data: encrypted1,
      email: this.email
    }
    this._driverService.getDrivergroupsById(enc_data1).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.DriverData = data.getDriverss;
        let country = this.DriverData.phone_number ? this.DriverData.phone_number.substr(0, 4) : '';
        let phone = '';
        if (country == '+971')
          phone = this.DriverData.phone_number.substr(4);
        if (this.DriverData.profile_picture) {
          this.imageUrl = environment.apiUrl + this.DriverData.profile_picture + window.localStorage['ImageExt'];
        }
        this.EditDriverForm.setValue({
          username: this.DriverData.username ? this.DriverData.username : "",
          password: "",
          name: this.DriverData.name ? this.DriverData.name : "",
          surname: this.DriverData.surname ? this.DriverData.surname : "",
          display_name: this.DriverData.display_name
            ? this.DriverData.display_name
            : "",
          email: this.DriverData.email ? this.DriverData.email : "",
          oib: this.DriverData.oib ? this.DriverData.oib : "",
          gender: this.DriverData.gender ? this.DriverData.gender : "",
          marital_status: this.DriverData.marital_status
            ? this.DriverData.marital_status
            : "",
          birth_date: this.dateformat(this.DriverData.birth_date),
          nationality: this.DriverData.nationality
            ? this.DriverData.nationality
            : "",
          home_address: this.DriverData.home_address
            ? this.DriverData.home_address
            : "",
          residence: this.DriverData.residence ? this.DriverData.residence : "",
          residence_expiry_date: this.dateformat(
            this.DriverData.residence_expiry_date
          ),
          company_join_date: this.dateformat(this.DriverData.company_join_date),
          vehicle_id: this.DriverData.vehicle_id
            ? this.DriverData.vehicle_id
            : "",
          driver_licence_number: this.DriverData.driver_licence_number
            ? this.DriverData.driver_licence_number
            : "",
          driver_licence_expiry_date: this.dateformat(
            this.DriverData.driver_licence_expiry_date
          ),
          phone_number: phone,
          vpn_number: this.DriverData.vpn_number
            ? this.DriverData.vpn_number
            : "",
          company: this.DriverData.company_id ? this.DriverData.company_id : "",
          driver_groups: this.DriverData.driver_groups
            ? this.DriverData.driver_groups
            : "",
          register_with_WASL: this.DriverData.register_with_WASL
            ? this.DriverData.register_with_WASL
            : "",
          profile_picture: "",
          emp_id: this.DriverData.emp_id,
          permit_expiry_date: this.dateformat(
            this.DriverData.permit_expiry_date
          ),
          permit_number: this.DriverData.permit_number
            ? this.DriverData.permit_number
            : "",
          emirates_id: this.DriverData.emirates_id
            ? this.DriverData.emirates_id
            : "",
          emirates_id_expiry: this.DriverData.emirates_id_expiry
            ? this.dateformat(this.DriverData.emirates_id_expiry)
            : "",
        });
        setTimeout(() => { this.EditDriverForm.patchValue({ username: this.DriverData.username ? this.DriverData.username : "" }) }, 2000)
      }
    });
    const vehicleparams = {
      offset: 0,
      limit: 0,
      sortOrder: "desc",
      sortByColumn: "_id",
      _id: this._id,
      company_id: this.companyId
    };
    var encrypted2 = this.encDecService.nwt(this.session, vehicleparams);
    var enc_data2 = {
      data: encrypted2,
      email: this.email
    }
    this._vehicleService.vehicleListing(enc_data2).subscribe(dec => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.VehicleData = data;
      }
    });
  }

  public dateformat(date) {
    if (date) {
      const dateObj = new Date(date);
      const month = dateObj.getUTCMonth() + 1;
      const day = dateObj.getUTCDate();
      const year = dateObj.getUTCFullYear();
      const dateObject = { date: { year: year, month: month, day: day } };
      return dateObject;
    } else {
      return "";
    }
  }

  /**
   * To update the driver records
   */
  public updateFlag = false;
  public updateDriver() {
    this.updateFlag = true;
    this.message = '';
    if (!this.EditDriverForm.valid) {
      this.updateFlag = false;
      this.toastr.error("Please fill in the required field. ");
      
      this.message = this.findInvalidControls()+" is not valid";
      return;
    } else {
      this.driver_groups_id = "";
      if (this.EditDriverForm.value.driver_groups.length > 0) {
        for (var i = 0; i < this.tempdriverGroupData.length; ++i) {
          for (var w = 0; w < this.EditDriverForm.value.driver_groups.length; ++w) {
            if (this.tempdriverGroupData[i]._id === this.EditDriverForm.value.driver_groups[w]) {
              this.PositionArray.push(i);
            }
          }
          if (i === this.tempdriverGroupData.length - 1) {
            for (let j = this.tempdriverGroupData.length - 1; j >= 0; j--) {
              let index = this.PositionArray.indexOf(j)
              if (index !== -1)
                this.driver_groups_id = this.driver_groups_id + "1";
              else
                this.driver_groups_id = this.driver_groups_id + "0";
            }

          }
        }
      } else {
        for (var i = 0; i < this.tempdriverGroupData.length; ++i) {
          this.driver_groups_id = this.driver_groups_id + "0";
        }
      }
      this.editDriverdata = {
        driver_id: this._id,
        username: this.EditDriverForm.value.username,
        name: this.EditDriverForm.value.name,
        surname: this.EditDriverForm.value.surname,
        display_name: this.EditDriverForm.value.display_name,
        password:this.EditDriverForm.value.password,
        email: this.EditDriverForm.value.email,
        oib: this.EditDriverForm.value.oib,
        gender: this.EditDriverForm.value.gender,
        marital_status: this.EditDriverForm.value.marital_status,
        birth_date: this.EditDriverForm.value.birth_date.formatted
          ? this.EditDriverForm.value.birth_date.formatted
          : this.DriverData.birth_date,
        nationality: this.EditDriverForm.value.nationality,
        home_address: this.EditDriverForm.value.home_address,
        residence: this.EditDriverForm.value.residence,
        residence_expiry_date: this.EditDriverForm.value.residence_expiry_date
          .formatted
          ? this.EditDriverForm.value.residence_expiry_date.formatted
          : this.DriverData.residence_expiry_date,
        company_join_date: this.EditDriverForm.value.company_join_date.formatted
          ? this.EditDriverForm.value.company_join_date.formatted
          : this.DriverData.company_join_date,
        vehicle_id: this.EditDriverForm.value.vehicle_id
          ? this.EditDriverForm.value.vehicle_id
          : null,
        driver_licence_number: this.EditDriverForm.value.driver_licence_number,
        driver_licence_expiry_date: this.EditDriverForm.value
          .driver_licence_expiry_date.formatted
          ? this.EditDriverForm.value.driver_licence_expiry_date.formatted
          : this.DriverData.driver_licence_expiry_date,
        phone_number: this.EditDriverForm.value.phone_number,
        vpn_number: this.EditDriverForm.value.vpn_number,
        company: this.EditDriverForm.value.company,
        driver_groups: this.EditDriverForm.value.driver_groups
          ? this.EditDriverForm.value.driver_groups
          : null,
        register_with_WASL: this.EditDriverForm.value.register_with_WASL,
        profile_picture: this.EditDriverForm.value.profile_picture,
        emp_id: this.EditDriverForm.value.emp_id,
        driver_groups_id: this.driver_groups_id ? "a" + this.driver_groups_id : 0,
        company_id: this.companyId,
        permit_number: this.EditDriverForm.value.permit_number,
        permit_expiry_date: this.EditDriverForm.value.permit_expiry_date
          .formatted
          ? this.EditDriverForm.value.permit_expiry_date.formatted
          : this.DriverData.permit_expiry_date,
        emirates_id:this.EditDriverForm.value.emirates_id ? this.EditDriverForm.value.emirates_id : null,
        emirates_id_expiry:this.EditDriverForm.value.emirates_id_expiry ? this.EditDriverForm.value.emirates_id_expiry.formatted : null,
      };
      var encrypted = this.encDecService.nwt(this.session, this.editDriverdata);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverService.updateDriver(enc_data).then((dec) => {
        if (dec) {
          this.message = dec.message;
          if (dec.status === 203) {
            this.toastr.warning('Driver is already registered with same username.');
          } else if (dec.status === 205) {
            this.toastr.warning('Driver is already registered with same phone number.');
          }
          else if (dec.status === 206) {
            this.toastr.warning('Driver is already registered with same email.');
          }
          else {
            this.toastr.success('Driver updated successfully.');
            setTimeout(() => {
              this.router.navigate(['/admin/fleet/drivers']);
            }, 1000);
          }
        }
        this.updateFlag = false;
      }).catch((error) => {
        this.toastr.error(error);

      });
    }
  }

  imageFinishedUploading(file: FileHolder) {
    this.EditDriverForm.controls["profile_picture"].setValue(file.src);
    this.spandata = "yes";
  }

  onRemoved(file: FileHolder) {
    this.EditDriverForm.value.profile_picture = null;
    this.EditDriverForm.controls["profile_picture"].setValue("");
    this.EditDriverForm.controls["profile_picture"].setValue(null);
    this.spandata = "no";
    // do some stuff with the removed file.
  }

  onUploadStateChanged(state: boolean) {
  }

  /**
   * Function To Get Driver Groups For Dropdown
   *
   */
  public getDriverGroupsForListing() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'unique_driver_model_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriversGroup(enc_data).subscribe(dec => {
      var data: any = this.encDecService.dwt(this.session, dec.data);
      if (dec && dec.status == 200) {
        this.driverGroupData = data.getDriverGroups;
        this.tempdriverGroupData = data.getDriverGroups;
        this.driverGroupDataLength = this.tempdriverGroupData;
      }
    });
  }
  public findInvalidControls() {
    const invalid = [];
    const controls = this.EditDriverForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }
}
