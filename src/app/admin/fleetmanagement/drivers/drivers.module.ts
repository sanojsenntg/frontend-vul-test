import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';
import { ListDriversComponent } from './list-drivers/list-drivers.component';
import { AddDriverComponent } from './add-driver/add-driver.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditDriverComponent } from './edit-driver/edit-driver.component';
import { ImageUploadModule } from 'angular2-image-upload';
import { DriverService } from '../../../common/services/driver/driver.service';
import { MatSelectModule } from '@angular/material/select';
import { ShiftsService } from '../../../common/services/shifts/shifts.service';
import { ShiftsrestService } from '../../../common/services/shifts/shiftsrest.service';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import { DriverDialogComponent } from '../../../common/dialog/driver-dialog/driver-dialog.component';

import { CustomerRatingService } from '../../../common/services/customer-rating/customerrating.service';
import { CustomerRatingRestService } from '../../../common/services/customer-rating/customerratingrest.service';
import { StarRatingModule } from 'angular-star-rating';
import { RatingModule } from "ngx-rating";
import { UploadDialogComponent } from './list-drivers/dialog/upload-dialog/upload-dialog.component';
import { EditStatusDialogComponent } from './list-drivers/dialog/edit-status-dialog/edit-status-dialog.component';
import { EditDriverGroupComponent } from './list-drivers/dialog/edit-driver-group/edit-driver-group.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MyDatePickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule,
    MatTooltipModule,
    NgbModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatInputModule,
    MatNativeDateModule,
    ImageUploadModule.forRoot(),
    StarRatingModule.forRoot(),
    MatSelectModule,
    MatDialogModule,
    RatingModule
  ],
  declarations: [ListDriversComponent, AddDriverComponent, EditDriverComponent, DriverDialogComponent, UploadDialogComponent, EditStatusDialogComponent, EditDriverGroupComponent],
  providers: [DriverService, ShiftsService, ShiftsrestService, CustomerRatingService, CustomerRatingRestService],
  exports: [MatSelectModule, MatTooltipModule, MatDialogModule],
  entryComponents: [DriverDialogComponent, UploadDialogComponent, EditStatusDialogComponent, EditDriverGroupComponent],

})
export class DriversModule { }
