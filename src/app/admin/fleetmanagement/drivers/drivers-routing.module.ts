import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListDriversComponent } from './list-drivers/list-drivers.component';
import { AddDriverComponent } from './add-driver/add-driver.component';
import {EditDevicesComponent} from '../devices/edit-devices/edit-devices.component';
import {EditDriverComponent} from './edit-driver/edit-driver.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const driversRoutes: Routes = [
  { path: '', component : ListDriversComponent, canActivate: [AclAuthervice], data: {roles: ["Drivers - List"]}},
  { path: 'add', component : AddDriverComponent, canActivate: [AclAuthervice], data: {roles: ["Drivers - Add"]}},
  { path: 'edit/:id', component: EditDriverComponent, canActivate: [AclAuthervice], data: {roles: ["Drivers - Edit"]}}

];
