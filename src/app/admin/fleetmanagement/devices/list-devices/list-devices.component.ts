import { Component, OnInit, ViewContainerRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceService } from '../../../../common/services/devices/devices.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { EditDriverComponent } from '../dialog/edit-driver/edit-driver.component';
import { EditVehicleComponent } from '../dialog/edit-vehicle/edit-vehicle.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { ToastsManager } from 'ng2-toastr';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { DeviceDialogComponent } from '../../../../common/dialog/device-dialog/device-dialog.component'
import { DriverService } from '../../../../common/services/driver/driver.service'
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service'
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { DeviceblockreasonComponent } from '../../../../common/dialog/deviceblockreason/deviceblockreason.component'
import * as moment from 'moment/moment';
import { Overlay } from '@angular/cdk/overlay';
import { FormControl } from '@angular/forms';
import { Socket } from 'ng-socket-io';
import { environment } from '../../../../../environments/environment';
import { GlobalService } from '../../../../common/services/global/global.service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { UsersService } from '../../../../common/services/user/user.service';

declare var jsPDF;

@Component({
  selector: 'app-list-devices',
  templateUrl: './list-devices.component.html',
  styleUrls: ['./list-devices.component.css']
})

export class ListDevicesComponent implements OnInit, OnDestroy {
  public companyId: any = [];
  queryField: FormControl = new FormControl();
  device: FormControl = new FormControl();
  driver_Id: FormControl = new FormControl();
  vehicle_side_no: FormControl = new FormControl();
  vehicle_plate_no: FormControl = new FormControl();
  imei_no: FormControl = new FormControl();
  company: FormControl = new FormControl();
  key: string = '';
  reverse: boolean = false;
  sortOrder = 'asc';
  public keyword;
  public changeStatus;
  del = false;
  pageSize = 10;
  public is_search = false;
  public searchSubmit = false;
  public pageNo = 0;
  totalPage = [1];
  public _id;
  public company_id = '';
  public deviceSearch;
  deviceLength;
  pNo = 1
  public socket_session_token;
  public deviceData;
  public deviceData2;
  public deviceData3;
  public companyData;
  public isBlocked_status = '';
  public unique_device_id: any = '';
  public deviceForm = '';
  public driverForm = '';
  public driverIdForm = '';
  public sideForm = '';
  public plateForm = '';
  public imeiForm = '';
  public selectedAll;
  public selectedDevice: any = [];
  public selectedDeviceData;
  public deviceDataPDF;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public aclEditDriver = false;
  public aclEditVehicle = false;
  public aclTempBlock = false;
  public aclTPermBlock = false;
  public downloads = false;
  public loadingIndicator;
  public blockedDevices = [];
  public imageext;
  public imageurl = '';
  public apiUrl;
  email: string;
  session: string;
  dtc: boolean = false;
  company_id_list: any = [];
  appVersion: any = [];
  appVersionArr: any = [{ version: 'V 2.20', _id: '31' }, { version: 'V 2.19', _id: '30' }, { version: 'V 2.18', _id: '29' }, { version: 'V 2.17', _id: '28' }, { version: 'V 2.16', _id: '27' }, { version: 'V 2.15', _id: '22' }, { version: 'V 2.14', _id: '21' }, { version: 'V 2.13', _id: '20' }, { version: 'V 2.12', _id: '19' }, { version: 'V 2.11', _id: '18' }, { version: 'V 2.10', _id: '17' }]
  companyIdFilter: any;
  //public pBlockedDevices=[];

  constructor(private _devicesService: DeviceService,
    private router: Router,
    public jwtService: JwtService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    private _driverService: DriverService,
    private _usersService: UsersService,
    private _vehichleService: VehicleService,
    private _companyservice: CompanyService,
    private socket: Socket,
    vcr: ViewContainerRef,
    public _aclService: AccessControlService,
    public encDecService: EncDecService,
    public _global: GlobalService) {
    const company_id: any = localStorage.getItem('user_company');
    if (company_id === '5ce12918aca1bb08d73ca25d' || company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.company_id_list.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.toastr.setRootViewContainerRef(vcr);
  }
  ngOnInit() {
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.imageext = window.localStorage['ImageExt'];
    this.imageurl = environment.imgUrl;
    this.apiUrl = environment.apiUrl;
    if (this._global.devices) {
      const data = this._global.devices;
      this.pageSize = data.limit;
      this.isBlocked_status = data.isBlocked_status;
      this.unique_device_id = data.unique_device_id;
      this.deviceForm = data.device;
      this.driver_id = data.driver_id;
      this.driverForm = data.driver;
      this.driverIdForm = data.driverIdForm;
      this.uniquedriverid = data.unique_driver_id;
      this.vehichle_side_no = data.vehichle_side_no;
      this.sideForm = data.side;
      this.vehichle_plate_no = data.vehichlePlateData;
      this.plateForm = data.plate;
      //this.getDevices();
    }
    else
      //this.getDeviceListing();
      this.getDevicesFoePDF();
    this.getDriver();
    this.getCompanies();
    this.aclDisplayService();
    this.refreshBlockList();
    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriver'
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
          limit: 10,
          'search_keyword': query
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status == 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.driverData = result.driver;
          }
        }
        this.loadingIndicator = '';
      });
    this.device.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDevice';
    })
    this.device.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
          limit: 10,
          offset: 0,
          sortOrder: 'asc',
          sortByColumn: 'unique_device_id',
          search_keyword: query,
          unique_device_id: ''
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._devicesService.getDevicesListing(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.deviceData2 = result.devices;
          }
        }
        this.loadingIndicator = '';
      });
    this.company.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {

        var params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
          offset: 0,
          limit: 10,
          sortOrder: 'asc',
          sortByColumn: 'unique_company_id',
          keyword: query
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._companyservice.getCompanyListing(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.companyData = result.getCompanies;
          }
        }
        this.loadingIndicator = '';
      });
    this.driver_Id.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriverId';
    })
    this.driver_Id.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {

        var params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
          limit: 10,
          'search_keyword_id': query
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.driverIdData = result.driver;
          }
        }
        this.loadingIndicator = '';
      });

    this.vehicle_side_no.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchVehichleSidenumber';
    })
    this.vehicle_side_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {

        var params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          'searchsidenumber': query
        }
        //console.log(params);
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._vehichleService.getVehicleListingAdmin(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            //      console.log(JSON.stringify(result));
            this.vehichleSideData = result.result;
            //      console.log(JSON.stringify(this.vehichleSideData));
          }
        }
        this.loadingIndicator = '';
      });
    this.vehicle_plate_no.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchVehichlePlateNumber';
    })
    this.vehicle_plate_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          'searchplate': query,
          company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
        }
        //console.log(params);
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._vehichleService.getVehicleListingAdmin(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status == 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.vehichlePlateData = result.result;
          }
        }
        this.loadingIndicator = '';
      });
    this.imei_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
          limit: 10,
          offset: 0,
          sortOrder: 'asc',
          sortByColumn: 'unique_device_id',
          imei: query,
          unique_device_id: ''
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._devicesService.getDevicesListing(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.deviceData3 = result.devices;
          }
        }
        this.loadingIndicator = '';
      });
    if (JSON.parse(localStorage.getItem("blockedDevices")))
      this.blockedDevices = JSON.parse(localStorage.getItem("blockedDevices"));
    // if(JSON.parse(localStorage.getItem("PBlockedDevices")))
    // this.pBlockedDevices=JSON.parse(localStorage.getItem("PBlockedDevices"));
  }

  public aclDisplayService() {
    var params = {
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Device - Edit") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Device - Delete") {
              this.aclDelete = true;
            } else if (data.menu[i] == "Drivers - Edit") {
              this.aclEditDriver = true;
            } else if (data.menu[i] == "Device - Permanent Block") {
              this.aclTPermBlock = true;
            } else if (data.menu[i] == "Device - Temporary Block") {
              this.aclTempBlock = true;
            } else if (data.menu[i] == "Vehicles - Edit") {
              this.aclEditVehicle = true;
            } else if (data.menu[i] == 'Downloads - Fleet Management') {
              this.downloads = true;
            }
          };
        }
      }
    })
  }
  public getDeviceListing() {
    this.is_search = false;
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
      unique_device_id: null
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._devicesService.getDevicesListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.deviceData = data.devices;
          this.deviceSearch = data.devices;
          this.deviceLength = data.totalCount;
        }
      }
      this.searchSubmit = false;
      this.del = false;
    });
  }

  public getDevicesFoePDF() {
    this.is_search = false;
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      unique_device_id: null,
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._devicesService.getDevicesListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.deviceDataPDF = data.devices;
        }
      }
    });
  }

  /**
   * Select and deselect checkbox for multiple records deletion
   * @param id
   */
  selectAll() {
    for (let i = 0; i < this.deviceData.length; i++) {
      if (this.selectedAll === false) {
        this.selectedDevice = [];
      } else {
        this.selectedDevice[i] = this.deviceData[i]._id;
      }
      this.deviceData[i].selected = this.selectedAll;
    }
  }

  checkIfAllSelected(device, i) {
    if (device) {
      this.deviceData[i].checked = !this.deviceData[i].checked;
      if (this.deviceData[i].checked == true) {
        this.selectedDevice.push(device._id);
      }
      else if (this.deviceData[i].checked == false) {
        this.selectedDevice.splice(i, 1);
      }
    }
  }

  /**
   * Delete array of driver
   */
  deleteSelectedDevice() {
    this.selectedDeviceData = {
      ids: this.selectedDevice
    }
    this._devicesService.deleteDeviceArray(this.selectedDeviceData).then((data) => {
    })
    this.ngOnInit();
  }

  /**
  * Confirmation popup for deleting one or more device record
  */
  openDeleteArrayDialog(): void {
    if (this.selectedDevice.length >= 1) {
      let dialogRef = this.dialog.open(DeleteDialogComponent, {
        width: '450px',
        scrollStrategy: this.overlay.scrollStrategies.noop(),
        data: { text: 'Are you sure you want to remove this record' }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result == true) {
          this.del = true;
          this.deleteSelectedDevice();

        } else {
        }
      });
    } else {
      this.toastr.warning('Please select atleast one record to delete.');
    }
  }

  /**
  * Confirmation popup for deleting particular device record
  * @param id
  */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deleteDevice(id);
      } else {
      }
    });
  }

  /**
   * Delete driver record
   * @param id
   */
  Driver(id) {
    this.dialog.closeAll();
    let data = {
      id: id,
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
    }
    let dialogRef = this.dialog.open(EditDriverComponent, {
      width: '800px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getDevices();
    });
  }

  /**
   * Delete vehicle records
   * @param id
   */
  Vehicle(id) {
    this.dialog.closeAll();
    let data = {
      id: id,
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
    }
    let dialogRef = this.dialog.open(EditVehicleComponent, {
      width: '600px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getDevices();
    });
  }

  /**
   * Delete device records
   * @param id
   */
  public deleteDevice(id) {
    this.searchSubmit = true;
    var params = {
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._devicesService.updatedeviceForDeletion(enc_data).then((dec) => {
      if (dec) {
        if (dec.status === 203) {
          this.toastr.warning('There is shift assign to this device, so you cannot delete it.');
          this.searchSubmit = false;
        } else if (dec.status === 200) {
          this.deviceData.splice(id, 1);
          this.searchSubmit = false;
          this.refresh();
          this.toastr.success('Device Deleted successfully');
        } else if (dec.status === 201) {
          this.toastr.error('Device updation failed');
          this.searchSubmit = false;
        }
        else {
          this.toastr.error(dec.message)
          this.searchSubmit = false;
        }
      }
    });
  }

  /**
  * Pagination for device module
  * @param data
  */
  pagingAgent(data) {
    this.searchSubmit = true;
    this.deviceData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    this.pNo = data;
    let params;
    params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      isBlocked_status: this.isBlocked_status ? this.isBlocked_status : '',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
      unique_device_id: this.unique_device_id ? this.unique_device_id : '',
      driver_id: this.driver_id ? this.driver_id : '',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      app_version_code: this.appVersion.length != 0 ? this.appVersion : []
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.deviceData = [];
    this.del = true;
    this._devicesService.searchForDevices(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.deviceData = data.devices;
          this.deviceLength = data.totalCount;
        }
      }
      this.searchSubmit = false;
    });
  }

  /**
   * For sorting device records
   * @param key
   */
  sort(key) {
    this.searchSubmit = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
    this.deviceData = [];
    var params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        isBlocked_status: this.isBlocked_status ? this.isBlocked_status : '',
        unique_device_id: this.unique_device_id ? this.unique_device_id : '',
        driver_id: this.driver_id ? this.driver_id : '',
        unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
        company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
        vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
        vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
        app_version_code: this.appVersion.length != 0 ? this.appVersion : []
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
      }
    }
    this.deviceData = [];
    //  if (this.isBlocked_status != "" || this.unique_device_id != "") {
    // this.deviceData = [];
    this.del = true;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    console.log(params);
    this._devicesService.searchForDevices(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.deviceData = data.devices;
          this.deviceLength = data.totalCount;
        }
      }
      this.searchSubmit = false;
    });
  }

  /**
   * For searching device records
   */
  public getDevices() {
    this.pNo = 1
    this.searchSubmit = true;
    this.is_search = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      isBlocked_status: this.isBlocked_status ? this.isBlocked_status : '',
      unique_device_id: this.unique_device_id ? this.unique_device_id : '',
      driver_id: this.driver_id ? this.driver_id : '',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      app_version_code: this.appVersion.length != 0 ? this.appVersion : []
    };
    const params2 = {
      limit: this.pageSize,
      isBlocked_status: this.isBlocked_status ? this.isBlocked_status : '',
      unique_device_id: this.unique_device_id ? this.unique_device_id : '',
      device: this.deviceForm ? this.deviceForm : '',
      driver_id: this.driver_id ? this.driver_id : '',
      driver: this.driverForm ? this.driverForm : '',
      driverIdForm: this.driverIdForm ? this.driverIdForm : '',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
      side: this.sideForm ? this.sideForm : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      plate: this.plateForm ? this.plateForm : '',
      app_version_code: this.appVersion.length != 0 ? this.appVersion : []
    }
    this._global.devices = params2;
    this.deviceData = [];
    this.del = true;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._devicesService.searchForDevices(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.deviceData = data.devices;
          this.deviceLength = data.totalCount;
        }
      }
      this.searchSubmit = false;
    });
  }

  /**
   * Autocomplete search on device filter
   * @param data
   */
  searchDevice(data) {
    if (typeof data === 'object') {
      this.unique_device_id = data.unique_device_id;
      this.getDevices();
    }
    else {
      this.unique_device_id = '';
    }
  }
  displayFnDevice(data): string {
    return data ? data.unique_device_id : data;
  }
  searchCompany(data) {
    if (typeof data === 'object') {
      this.company_id = data._id;
      this.getDevices();
    }
    else {
      this.company_id = '';
    }
  }
  displayFnCompany(data): string {
    return data ? data.company_name : data;
  }
  /**
   * To refresh device records showing in the grid
   */
  refresh() {
    if (this.is_search == true) {
      this.getDevices();
    }
    else {
      this.del = true;
      this.ngOnInit();
    }
  }

  /**
   * To reset the search filters and reload the device records
   */
  reset() {
    this.unique_device_id = '';
    this.isBlocked_status = '';
    this.driver_id = '';
    this.company_id = '';
    this.uniquedriverid = '';
    this.driverIdData = [];
    this.vehichle_plate_no = '';
    this.vehichle_side_no = '';
    this.is_search = false;
    this.del = true;
    this.pNo = 1;
    this.deviceForm = '';
    this.driverForm = '';
    this.driverIdForm = '';
    this.imeiForm = '';
    this.sideForm = '';
    this.plateForm = '';
    this.pageSize = 10;
    if (this.dtc) {
      this.company_id_list = this.companyData.slice().map(x => x._id);
      this.companyIdFilter = this.company_id_list.slice()
      this.companyIdFilter.push('all')
    }
    this.appVersion = [];
    this.getDevices();
  }

  /**
   * To change the blocked status of device
   * @param device
   * @param index
   */
  ChangeBlockStatus(device, index, type, reason, note) {
    this.blockedDevices.push(device.unique_device_id);
    this.refreshBlockList();
    let that = this;
    if (device.isBlocked_status == '1') {
      that.changeStatus = '2';
    } else if (device.isBlocked_status == '4') {
      that.changeStatus = '4';
    } else {
      that.changeStatus = '1';
    }
    var user = JSON.parse(window.localStorage['adminUser']);
    if (!user) {
      that.toastr.error("Session Expired..please login again");
    } else {
      const params = {
        isBlocked_status: that.changeStatus,
        email: user.email,
        reason: reason,
        note: note,
        user_id: user._id,
        priority: user.priority,
        company_id: that.company_id_list.length > 0 ? that.company_id_list : [that.company_id],
        _id: device._id
      }
      var encrypted = this.encDecService.nwt(that.session, params);
      var enc_data = {
        data: encrypted,
        email: that.email
      }
      that._devicesService.ChangeBlockStatus(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 205) {
            //aleready
            that.toastr.success(dec.message);
          } else if (dec.status == 202) {
            //initiated
            //alert("inprogress")
            that.toastr.success(dec.message);
          } else if (dec.status == 203) {
            //No permission  
            that.removeDevicesFromLocal(device.unique_device_id)
            //alert("inprogress")
            that.toastr.warning(dec.message);
          } else if (dec.status == 204) {
            that.removeDevicesFromLocal(device.unique_device_id)
            that.toastr.error(dec.message);
          } else if (dec.status == 200) {
            var data: any = that.encDecService.dwt(that.session, dec.data);
            //console.log(JSON.stringify(data));
            if (data) {
              that.removeDevicesFromLocal(device.unique_device_id)
              that.deviceData[index].isBlocked_status = data.updatedBlockedStatus.isBlocked_status;
              that.deviceData[index].note = data.updatedBlockedStatus.note;
              that.deviceData[index].reason = data.updatedBlockedStatus.reason;
              that.toastr.success(dec.message);
            }
          }
        }
        //this.deviceData[index].isBlocked_status = data.updatedBlockedStatus.isBlocked_status;
      });
    }
  }

  ChangePermanantBlockStatus(device, index, reason, note) {
    // this.pBlockedDevices.push(device.unique_device_id)  
    let that = this;
    this.changeStatus = '3';
    var user = JSON.parse(window.localStorage['adminUser']);
    if (!user) {
      that.toastr.error("Session Expired..please login again");
    } else {
      const params = {
        isBlocked_status: this.changeStatus,
        email: user.email,
        reason: reason,
        note: note,
        company_id: that.company_id_list.length > 0 ? that.company_id_list : [that.company_id],
        _id: device._id
      }
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: that.email
      }
      that._devicesService.ChangeBlockStatus(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 201) {
            // this.removePBDevicesFromLocal(device.unique_device_id)
            that.toastr.error("Device blocked operation failed");
          } else if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            //this.removePBDevicesFromLocal(device.unique_device_id)
            that.toastr.success("Device blocked permanently");
            that.deviceData[index].isBlocked_status = data.updatedBlockedStatus.isBlocked_status;
            that.deviceData[index].note = data.updatedBlockedStatus.note;
            that.deviceData[index].reason = data.updatedBlockedStatus.reason;
          }
        }
      });
    }
  }

  /**
   * To create csv of the device records showing in the grid
   */
  public createCsv() {
    const params1 = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      isBlocked_status: this.isBlocked_status ? this.isBlocked_status : '',
      unique_device_id: this.unique_device_id ? this.unique_device_id : '',
      driver_id: this.driver_id ? this.driver_id : '',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      fromCSV: true,
      app_version_code: this.appVersion.length != 0 ? this.appVersion : [],
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    console.log(params1);
    this._devicesService.searchForDevices(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          let labels = [
            'ID',
            'IMEI',
            'Credit Card',
            'Side Number'
          ];
          let resArray = [];
          for (let i = 0; i < res.devices.length; i++) {
            const csvArray = res.devices[i];
            resArray.push
              ({
                id: csvArray.unique_device_id ? csvArray.unique_device_id : 'N/A',
                imei: csvArray.imei ? csvArray.imei : 'NA',
                credit_card: csvArray.vehicle_id[0] ? csvArray.vehicle_id[0].credit_card ? csvArray.vehicle_id[0].credit_card : 'NA' : 'NA',
                side_number: csvArray.vehicle_id[0] ? csvArray.vehicle_id[0].display_name : 'NA',
              });
          }
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(resArray, 'Device-List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        }
      }
    })
  }

  /**
   * To generate the pdf of device records showing in the grid
   */
  public generatePDF() {
    const params1 = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      isBlocked_status: this.isBlocked_status ? this.isBlocked_status : '',
      unique_device_id: this.unique_device_id ? this.unique_device_id : '',
      driver_id: this.driver_id ? this.driver_id : '',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
      app_version_code: this.appVersion.length != 0 ? this.appVersion : []
    };
    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    console.log(params1);
    this._devicesService.searchForDevices(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          let resArray = [];
          for (let i = 0; i < res.devices.length; i++) {
            const pdfArray = res.devices[i];
            resArray.push({
              id: pdfArray.unique_device_id ? pdfArray.unique_device_id : 'N/A',
              device_model: pdfArray.device_model ? pdfArray.device_model : 'N/A',
              ud_id: pdfArray.ud_id ? pdfArray.ud_id : 'N/A',
              company: pdfArray.company ? pdfArray.company : 'Dubai Taxi',
              manufacturer: pdfArray.manufacturer ? pdfArray.manufacturer : 'N/A ',
              isBlocked_status: pdfArray.isBlocked_status != null ? pdfArray.isBlocked_status == '1' ? pdfArray.isBlocked_status = 'Blocked' : 'Unblocked' : 'N/A',

            });
          }
          var doc = new jsPDF();
          var col = [
            { title: "Id", dataKey: "id" },
            { title: "Device Model", dataKey: "device_model" },
            { title: "Ud Id", dataKey: "ud_id" },
            { title: "Company", dataKey: "company" },
            { title: "Manufacturer", dataKey: "manufacturer" },
            { title: "Block Status", dataKey: "isBlocked_status" },
          ];
          var options = {
            styles: { // Defaul style
              lineWidth: 0.01,
              lineColor: 0,
              fillStyle: 'DF',
              halign: 'center',
              valign: 'middle',
              columnWidth: 'auto',
              overflow: 'linebreak'
            },
          };
          doc.text(90, 10, "Device - List");
          doc.autoTable(col, resArray, options);
          doc.save('Device_list' + moment().format('YYYY-MM-DD_HH_mm_ss'));
        }
      }
    })
  }

  /**
   * Confirmation popup for blocking particular device record
   * @param id
   * @param index
   */
  openDialogs(id, index, type): void {
    const dialogRef = this.dialog.open(DeviceDialogComponent, {
      width: '450px',
      height: '150px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to block this device' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        let reason = "";
        let note = "";
        this.ChangeBlockStatus(id, index, type, reason, note);
        //this.toastr.success('Device block Operation initiated');
      } else {
      }
    });
  }
  public type;
  public DevicestatusChangePopup(id, index, type) {
    this.type = type;
    let dialogRef = this.dialog.open(DeviceblockreasonComponent, {
      width: '315px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { type: type }
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        if (this.type === "t") {
          let reason = res.cancelreason;
          let note = res.cancelText;
          this.del = true;
          this.ChangeBlockStatus(id, index, type, reason, note);
        } else {
          this.del = true;
          let reason = res.cancelreason;
          let note = res.cancelText;
          this.ChangePermanantBlockStatus(id, index, reason, note);
        }
      }
    });
  }

  /**
   * Confirmation popup for blocking particular device record
   * @param id
   * @param index
   */
  openPermanantDeleteDialog(id, index): void {
    const dialogRef = this.dialog.open(DeviceDialogComponent, {
      width: '450px',
      height: '150px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'All the data of the device will be lost including trip details.Are you sure you want to block this device permanantly' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        let reason = '';
        let note = ''
        this.ChangePermanantBlockStatus(id, index, reason, note);
        //this.toastr.success('Device blocked Permenantly');
      } else {
      }
    });
  }
  public PDF() {
    const elementToPrint = document.getElementsByClassName('Html2PDFdiv'); //The html element to become a pdf
    const pdf = new jsPDF('landscape');
    pdf.addHTML(elementToPrint, () => {
      pdf.save('Device_List.pdf');
    });
  }

  /**
   * Confirmation popup for unblocking particular device record
   * @param id
   * @param index
   */
  openDialogToUnblock(id, index, type): void {
    const dialogRef = this.dialog.open(DeviceDialogComponent, {
      width: '450px',
      height: '150px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to Unblock this device' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        let reason = '';
        let note = '';
        this.ChangeBlockStatus(id, index, type, reason, note);
        //this.toastr.success('Device unblock Operation initiated');
      } else {
      }
    });
  }
  /**
   * Get driver for drop down
   *
   */
  public driverData;
  public driverIdData;
  public driver_id;
  public uniquedriverid;
  public vehichlePlateData;
  public vehichleSideData;
  public getDriver() {
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getAllDrivers(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var Driverdata: any = this.encDecService.dwt(this.session, dec.data);
          this.driverData = Driverdata.driver;
          this.driverIdData = Driverdata.driver;
        }
      }
    });
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
          if (this.dtc && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher'  && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
            this.company_id_list = data.getCompanies.map(x => x._id);
            this.companyIdFilter = data.getCompanies.map(x => x._id)
            this.companyIdFilter.push('all')
          }
          if (this._global.devices) {
            this.getDevices()
          }
          else {
            this.getDeviceListing()
          }
        }
      }
    });
  }

  /**
   * Autocomplete search on driver drop down
   * @param data 
   */
  searchDriver(data) {
    if (typeof data === 'object') {
      this.driver_id = data._id;
    }
    else {
      this.driver_id = '';
    }
  }
  displayFnDriver(data): string {
    return data ? data.name : data;
  }
  searchDriverId(data) {
    if (typeof data === 'object') {
      this.uniquedriverid = data._id;
    }
    else {
      this.uniquedriverid = '';
    }
  }
  displayFnDriverId(data): string {
    return data ? data.emp_id : data;
  }
  /**
       * Get Vehichle Side and Plate data for drop down
       *
       */
  public getVehichleData() {
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      role: 'admin',
      company_id: this.company_id_list.length > 0 ? this.company_id_list : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehichleService.getVehicleListingAdmin(enc_data).subscribe(dec => {
      if (dec) {
        if (dec.status == 200) {
          var Vehichledata: any = this.encDecService.dwt(this.session, dec.data);
          this.vehichlePlateData = Vehichledata;
          this.vehichleSideData = Vehichledata;
        }
      }
    });
  }
  public vehichle_plate_no;
  searchVehichlePlateNumber(data) {
    if (typeof data === 'object') {
      this.vehichle_plate_no = data._id;
    }
    else {
      this.vehichle_plate_no = '';
    }
  }
  public vehichle_side_no;
  searchVehichleSidenumber(data) {
    if (typeof data === 'object') {
      this.vehichle_side_no = data._id;
    }
    else {
      this.vehichle_side_no = '';
    }
  }
  displayFnVehichleSide(data): string {
    return data ? data.vehicle_identity_number : data;
  }
  displayFnVehichlePlate(data): string {
    return data ? data.plate_number : data;
  }
  displayFnIMEI(data): string {
    return data ? data.imei : data;
  }
  ngAfterViewInit() {
    let that = this;
    that.socket.on('device_block', function (data) {
      if (!data || data == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      }
      data = that.encDecService.sockDwt(that.socket_session_token, data);
      //console.log(data);
      if (Object.keys(data).length !== 0) {
        if (data.data.company_id === window.localStorage['user_company']) {
          if (data) {
            let device_id = data.data.device_id;
            let devicedata = data.device;
            if (data.data.block_response === "Trip") {
              that.toastr.error("Device cant be blocked now..is on an active trip");
              if (devicedata.unique_device_id)
                that.removeDevicesFromLocal(devicedata.unique_device_id)
            } else {
              that.toastr.success("Device " + devicedata.unique_device_id + " blocked successfully");
              that.removeDevicesFromLocal(devicedata.unique_device_id)
              that.deviceData.forEach(device => {
                if (device._id === device_id) {
                  device.isBlocked_status = "1";
                  device.note = devicedata.note;
                  device.reason = devicedata.reason;
                }
              })
            }
          }
        }
      } else {
        let param_socket = {
          company_id: that.companyId
        }
        let enc_data = that.encDecService.nwt(that.session, param_socket);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: that.email
        }
        that._usersService.getSocketsession(data1).then((res1) => {
          //console.log(JSON.stringify(res1));
          if (res1 && res1.status == 200) {
            var res_data1: any = that.encDecService.dwt(this.session, res1.data);
            if (res1.status == 200) {
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              that.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          }
        })
      }
    });
    that.socket.on('device_unblock', function (data) {
      if (!data || data == '' || localStorage.getItem('socketFlag') == '0') {
        return;
      }
      data = that.encDecService.sockDwt(that.socket_session_token, data);
      //console.log(data);
      if (Object.keys(data).length !== 0) {
        if (data.data.company_id === window.localStorage['user_company']) {
          if (data) {
            let device_id = data.data.device_id;
            let devicedata = data.device;
            that.toastr.success("Device " + devicedata.unique_device_id + " unblocked successfully");
            that.removeDevicesFromLocal(devicedata.unique_device_id)
            that.deviceData.forEach(device => {
              if (device._id === device_id) {
                device.isBlocked_status = "2";
              }
            })
          }
        }
      } else {
        let param_socket = {
          company_id: that.companyId
        }
        let enc_data = that.encDecService.nwt(that.session, param_socket);
        //alert(this.dispatcher_id.email)
        let data1 = {
          data: enc_data,
          email: that.email
        }
        that._usersService.getSocketsession(data1).then((res1) => {
          //console.log(JSON.stringify(res1));
          if (res1 && res1.status == 200) {
            var res_data1: any = that.encDecService.dwt(that.session, res1.data);
            if (res1.status == 200) {
              window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
              that.socket_session_token = window.localStorage['SocketSessiontoken'];
            }
          }
        })
      }
    });
  }
  checkIfPressed(id) {
    let index = this.blockedDevices.indexOf(id)
    if (index > -1) {
      return true;
    }
    return false;
  }
  // checkIfPBPressed(id) {
  //   let index = this.pBlockedDevices.indexOf(id)
  //   if (index > -1) {
  //   return true;
  //   }
  //   return false;
  // }
  removeDevicesFromLocal(id) {
    let index = this.blockedDevices.indexOf(id);
    if (index > -1) {
      this.blockedDevices.splice(index, 1);
    }
  }
  // removePBDevicesFromLocal(id) {
  //   let index = this.pBlockedDevices.indexOf(id)
  //   if (index > -1) {
  //     this.pBlockedDevices.splice(index, 1);
  //   }
  // }
  ngOnDestroy() {
    //localStorage.setItem("PBlockedDevices", JSON.stringify(this.pBlockedDevices));
    localStorage.setItem("blockedDevices", JSON.stringify(this.blockedDevices));
  }
  public refreshInterval;
  refreshBlockList() {
    clearInterval(this.refreshInterval);
    this.refreshInterval = setInterval(() => {
      this.blockedDevices = [];
    }, 60000);
  }
  changeCompany() {
    this.deviceData2 = [];
    this.driverData = [];
    this.driverIdData = [];
    this.vehichleSideData = [];
    this.vehichlePlateData = [];
  }
  selectAllCompany() {
    if (this.company_id_list.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.company_id_list = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.company_id_list = [];
      this.companyIdFilter = [];
    }
  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.company_id_list = this.companyIdFilter.slice()
    }
    else {
      this.company_id_list = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }
}

