import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { DeviceService } from '../../../../common/services/devices/devices.service';
import { ToastsManager } from 'ng2-toastr';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-devices',
  templateUrl: './edit-devices.component.html',
  styleUrls: ['./edit-devices.component.css'],
  providers: [DeviceService]
})
export class EditDevicesComponent implements OnInit {
  public companyId: any = [];
  public serviceData;
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public CompanyData;
  email: string;
  session: string;
  constructor(private _deviceService: DeviceService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private _companyservice: CompanyService,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.toastr.setRootViewContainerRef(vcr);
    this.editForm = formBuilder.group({
      ud_id: ['', [
        Validators.required,
        Validators.maxLength(25)]],
      device_model: ['', [
        Validators.required,
        Validators.maxLength(30)]],
      manufacturer: ['', [Validators.required,
      Validators.maxLength(20)]],
      company: ["", []],
    });
  }

  public ngOnInit(): void {
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      //this.CompanyLength = data.count;
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CompanyData = data.getCompanies;
        }
      }
      //this.searchSubmit = false;
    });
    var params1 = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted1 = this.encDecService.nwt(this.session, params1);
    var enc_data1 = {
      data: encrypted1,
      email: this.email
    }
    this._deviceService.getDevicesById(enc_data1).then((dec) => {
      var res: any = this.encDecService.dwt(this.session, dec.data);
      if (dec) {
        if (dec.status == 200) {
          this.editForm.setValue({
            ud_id: res.getDevices.ud_id ? res.getDevices.ud_id : '',
            device_model: res.getDevices.device_model ? res.getDevices.device_model : '',
            manufacturer: res.getDevices.manufacturer ? res.getDevices.manufacturer : '',
            company: res.getDevices.company_id ? res.getDevices.company_id : "",
          });
        }
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
  }

  public updateDeviceDetails() {

    if (!this.editForm.valid) {
      this.toastr.error('All fields are required');
      return;
    }

    const deviceData = {
      ud_id: this.editForm.value.ud_id,
      device_model: this.editForm.value.device_model,
      manufacturer: this.editForm.value.manufacturer,
      company: this.editForm.value.company,
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, deviceData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._deviceService.updateDevice(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            this.toastr.success('Device updated successfully');
            this.router.navigate(['/admin/fleet/devices']);
          }
          else
            this.toastr.error(dec.message)
        }
      })
      .catch((error) => {
        this.toastr.error('Device updation cancelled');
        this.router.navigate(['/login']);
      });
  }

}
