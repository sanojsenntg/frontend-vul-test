import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent } from '@angular/material';
import { DriverService } from '../../../../../common/services/driver/driver.service';
import { VehicleService } from '../../../../../common/services/vehicles/vehicle.service';
import { DeviceService } from '../../../../../common/services/devices/devices.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JwtService } from '../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-vehicle',
  templateUrl: './edit-vehicle.component.html',
  styleUrls: ['./edit-vehicle.component.css']
})
export class EditVehicleComponent implements OnInit {
  public editForm: FormGroup;
  selectedValue: any;
  public companyId: any = [];
  public vehicleData;
  public vehiclesData;
  public vehicle_id;
  public deviceData;
  private sub: Subscription;
  public id;
  public Data;
  public searchSubmit = false;
  public deviceAddData;
  email: string;
  session: string;
  invalid: string;

  constructor(public _vehicleService: VehicleService,
    public _deviceService: DeviceService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EditVehicleComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private route: ActivatedRoute,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    private router: Router, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.toastr.setRootViewContainerRef(vcr);
    this.deviceData = data.id;
    //const company_id: any = localStorage.getItem('user_company');
    const company_id: any = data.company_id
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    company_id.forEach(element => {
      this.companyId.push(element);
    });
    this.editForm = formBuilder.group({
      vehicle_id: ['']
    });
  }

  public ngOnInit(): void {
    this.deviceAddData = this.deviceData;
    this.getVehicles();
    this.vehicleFilter = this.deviceData.vehicle_id;
    for (let i = 0; i < this.vehicleFilter.length; i++) {
      this.vehicleList[i] = this.vehicleFilter[i]._id;
    }
    var params = {
      company_id: this.companyId,
      _id: this.deviceAddData._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._deviceService.getDevicesById(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var datas: any = this.encDecService.dwt(this.session, dec.data);
          this.vehicleData = datas.getDevices.vehicle_id;
          this.editForm.setValue({
            vehicle_id: datas.getDevices.vehicle_id ? datas.getDevices.vehicle_id : '',
          });
        }
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
    /*this.vehicleCtrl.valueChanges
    .debounceTime(500)
    .distinctUntilChanged()
    .switchMap((query) =>  this._vehicleService.getVehicleListingAdmin({
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      'searchsidenumber': query
    }))
    .subscribe( result => { if (result.status === 400) { return; } else {   this.vehicleData = result; }
  });*/
    this.vehicleCtrl.valueChanges
      .debounceTime(500).subscribe(query => {
        this.vehicleCtrlVal(query);
      })
  }
  vehicleCtrlVal(query) {
    var params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      'searchsidenumber': query,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.getVehicleListingAdmin(enc_data)
      .subscribe(dec => {
        if (dec) {
          if (dec.status == 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.vehiclesData = result.result;
          }
        }
      });
  }

  updateVehicles() {
    this.invalid = '';
    if (!this.editForm.valid) {
      this.toastr.error('All fields are required.');
      this.invalid = 'All fields are required.';
      return;
    }
    else {
      const params = {
        vehicle_id: this.vehicleList ? this.vehicleList : null,
        company_id: this.companyId,
        _id: this.deviceAddData._id
      }
      console.log(params)
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._deviceService.updateVehicle(enc_data)
        .then((dec) => {
          console.log(dec)
          if (dec) {
            if (dec.status == 200) {
              var res: any = this.encDecService.dwt(this.session, dec.data);
              console.log(res)
              if (res.busyvehicle !== 1) {
                this.toastr.success('Vehicle updated successfully.');
                setTimeout(() => {
                  this.dialogRef.close();
                }, 1000);
              }
              else if (res.busyvehicle === 1) {
                this.toastr.warning('Vehicle is busy');
                this.invalid = 'Vehicle is busy';
                setTimeout(() => {
                  this.dialogRef.close();
                }, 1000);
              }
            } else if (dec.status === 201) {
              this.toastr.error('Vehicle updation failed.');
              this.invalid = 'Vehicle updation failed.';
            }
            else if (dec.status === 202) {
              this.toastr.error('Cant assign multiple vehicles to same device.');
              this.invalid='Cant assign multiple vehicles to same device.';
            }
            else {
              this.toastr.warning(dec.message)
              this.invalid=dec.message;
            }
            // else if (dec.status === 208) {
            //   var device_data = res.updatedVehicle;
            //   var did = '';
            //   for (var i = 0; i < device_data.length; ++i) {
            //     did = did + (device_data[i].unique_device_id).toString() + ',';
            //   }
            //   this.toastr.error('Vehicle Already assigned to ' + did);
            // }
          }
        })
        .catch((error) => {
          this.toastr.error('Vehicle updation failed.');
          this.invalid='Vehicle updation failed.';
        });
    }
  }

  closePop() {
    this.dialogRef.close();
  }

  public getVehicles() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.getAllVehicleListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          this.vehiclesData = res.getAllVehicle;
        }
      }
    });
  }
  /*
    public deleteVehicles(index, _id) {
      this.searchSubmit = true;
      this.deviceAddData.vehicle_id.splice(index, 1);
      var i = this.vehicleData.indexOf(_id);
      this.vehicleData.splice(i, 1);
      this.editForm.controls['vehicle_id'].setValue(this.vehicleData);
    }
  */
  vehicleCtrl: FormControl = new FormControl();
  public vehicleFilter = [];
  public vehicleList = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  vehicleIdselected(event: MatAutocompleteSelectedEvent): void {
    this.invalid = '';
    if (this.vehicleList.indexOf(event.option.value._id) > -1) {
      return
    }
    else {
      this.vehicleFilter = [event.option.value];
      this.vehicleList = [event.option.value._id];
    }
  }
  removeVehicle(fruit): void {
    this.invalid = '';
    const index = this.vehicleFilter.indexOf(fruit);
    if (index >= 0) {
      this.vehicleFilter.splice(index, 1);
      this.vehicleList.splice(index, 1)
    }
  }
  displayFnVehicleId(data): string {
    return data ? '' : '';
  }
}
