import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent } from '@angular/material';
import { DriverService } from '../../../../../common/services/driver/driver.service';
import { DeviceService } from '../../../../../common/services/devices/devices.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment/moment';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { DeleteDialogComponent } from '../../../../../common/dialog/delete-dialog/delete-dialog.component';
import { JwtService } from '../../../../../common/services/api/jwt.service';
import { Overlay } from '@angular/cdk/overlay';
import { EncDecService } from '../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-driver',
  templateUrl: './edit-driver.component.html',
  styleUrls: ['./edit-driver.component.css']
})
export class EditDriverComponent implements OnInit {

  public companyId: any = [];
  public editForm: FormGroup;
  selectedValue: any;
  public driverData;
  public max = new Date();
  public deviceAddData;
  public driversData;
  public driver_id;
  public deviceData;
  public temp_shift_driver;
  public id;
  private sub: Subscription;
  public searchSubmit = false;
  public tempShiftData = '';
  public selectedMoment = '';
  public selectedMoment1 = '';
  email: string;
  session: string;
  constructor(public _driverService: DriverService,
    public _deviceService: DeviceService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public overlay: Overlay,
    public dialogRef: MatDialogRef<EditDriverComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    public dialog: MatDialog,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    private router: Router, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.toastr.setRootViewContainerRef(vcr);
    this.deviceData = data.id;
    //const company_id: any = localStorage.getItem('user_company');
    const company_id: any = data.company_id;
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    company_id.forEach(element => {
      this.companyId.push(element);
    });
    this.editForm = formBuilder.group({
      driver_id: ['']
    });
  }

  ngOnInit() {
    this.deviceAddData = this.deviceData;
    this.selectedMoment = this.getDate(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    this.selectedMoment1 = this.getDate1(moment().add(12, 'hours').format('YYYY-MM-DD HH:mm'));
    this.getDrivers();
    if (this.deviceAddData.temp_shift_id && this.deviceAddData.temp_shift_id !== null) {
      this.getTempshiftDetails(this.deviceAddData.temp_shift_id);
    }
    this.driversFilter = this.deviceData.driver_id;
    for (let i = 0; i < this.driversFilter.length; i++) {
      this.driverList[i] = this.driversFilter[i]._id;
    }
    /*this._deviceService.getDevicesById(this.deviceAddData._id).then((datas) => {
      this.driverData = datas.getDevices.driver_id;
      this.editForm.setValue({
        driver_id: datas.getDevices.driver_id ? datas.getDevices.driver_id : '',
      });
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });*/
    this.driverCtrl.valueChanges
      .debounceTime(500).subscribe((data) => {
        this.driverCtroler(data)
      })

  }
  driverCtroler(query) {
    var params = {
      limit: 10,
      'search_keyword': query,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.searchForDriver(enc_data)
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.driversData = result.driver
          }
        }
      });
  }
  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }

  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }
  updateDriver() {
    if (this.driverList1.length > 0) {
      let dialogRef = this.dialog.open(DeleteDialogComponent, {
        width: '450px',
        scrollStrategy: this.overlay.scrollStrategies.noop(),
        data: { text: 'Standby Driver field is not empty.Are you sure you want to proceed without adding standby driver', type: 'confirm' }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result != true) {
        } else {
          if (!this.editForm.valid) {
            this.toastr.error('All fields are required.');
            return;
          }
          else {
            const params = {
              company_id: this.companyId,
              driver_id: this.driverList ? this.driverList : null,
              _id: this.deviceAddData._id
            }
            var encrypted = this.encDecService.nwt(this.session, params);
            var enc_data = {
              data: encrypted,
              email: this.email
            }
            this._deviceService.updateDriver(enc_data)
              .then((dec) => {
                if (dec) {
                  if (dec.status === 200) {
                    var res: any = this.encDecService.dwt(this.session, dec.data);
                    if (res.busydriver !== 1) {
                      this.toastr.success('Driver updated successfully.');
                      setTimeout(() => {
                        this.dialogRef.close();
                      }, 1000);
                    } else if (res.busydriver === 1) {
                      this.toastr.warning('Driver is busy');
                      setTimeout(() => {
                        this.dialogRef.close();
                      }, 1000);
                    }
                  } else if (dec.status === 201) {
                    this.toastr.error('Driver updation Failed..Please try Again');
                    setTimeout(() => {
                      this.dialogRef.close();
                    }, 1000);
                  }
                }
              })
              .catch((error) => {
                setTimeout(() => {
                  this.dialogRef.close();
                }, 1000);
                this.toastr.error('Driver updation failed.');
              });
          }
        }
      })
    } else {
      if (!this.editForm.valid) {
        this.toastr.error('All fields are required.');
        return;
      }
      else {
        const Driver = {
          driver_id: this.driverList ? this.driverList : null,
          company_id: this.companyId,
          _id: this.deviceAddData._id
        }
        var encrypted = this.encDecService.nwt(this.session, Driver);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._deviceService.updateDriver(enc_data)
          .then((dec) => {
            if (dec) {
              if (dec.status == 200) {
                var res: any = this.encDecService.dwt(this.session, dec.data);
                if (res.busydriver !== 1) {
                  this.toastr.success('Driver updated successfully.');
                  setTimeout(() => {
                    this.dialogRef.close();
                  }, 1000);
                } else if (res.busydriver === 1) {
                  this.toastr.warning('Driver is busy');
                  setTimeout(() => {
                    this.dialogRef.close();
                  }, 1000);
                } else if (dec.status === 201) {
                  this.toastr.error('Driver updation Failed..Please try Again');
                  setTimeout(() => {
                    this.dialogRef.close();
                  }, 1000);
                }
              }
            }
          })
          .catch((error) => {
            setTimeout(() => {
              this.dialogRef.close();
            }, 1000);
            this.toastr.error('Driver updation failed.');
          });
      }
    }
  }

  closePop() {
    this.dialogRef.close();
  }
  public getTempshiftDetails(temp_shift_id) {
    const params = {
      temp_shift_id: temp_shift_id,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._deviceService.searchForTempshift(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var tempdata: any = this.encDecService.dwt(this.session, dec.data);
          this.tempShiftData = tempdata.tempShiftData;
        }
      }
    });
  }
  public getDrivers() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.searchForDriver(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data1: any = this.encDecService.dwt(this.session, dec.data);
          this.driversData = data1.driver;
        }
      }
    });
  }
  /*
    public deleteDrivers(index, _id) {
      this.searchSubmit = true;
      this.deviceAddData.driver_id.splice(index, 1);
      var i = this.driverData.indexOf(_id);
      this.driverData.splice(i, 1);
      this.editForm.controls['driver_id'].setValue(this.driverData);
    }
  */
  driverCtrl: FormControl = new FormControl();
  public driversFilter = [];
  public driversFilter1 = [];
  public driverList = [];
  public driverList1 = [];
  public additional_info;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  driverIdselected(event: MatAutocompleteSelectedEvent): void {
    if (this.driverList.indexOf(event.option.value._id) > -1) {
      return
    }
    else {
      this.driversFilter.push(event.option.value);
      this.driverList.push(event.option.value._id);
    }
  }
  removeDriver(fruit): void {
    const index = this.driversFilter.indexOf(fruit);
    if (index >= 0) {
      this.driversFilter.splice(index, 1);
      this.driverList.splice(index, 1)
    }
  }
  driverIdselected1(event: MatAutocompleteSelectedEvent): void {
    if (this.driverList1.indexOf(event.option.value._id) > -1) {
      return
    } else if (this.driverList.indexOf(event.option.value._id) > -1) {
      this.toastr.error('Driver is already permenant');
      return
    }
    else {
      if (this.driverList1.length >= 1) {
        this.toastr.error('Only one Driver can be added')
      } else {
        this.driversFilter1.push(event.option.value);
        this.driverList1.push(event.option.value._id);
      }
    }
  }
  removeDriver1(driver): void {
    const index = this.driversFilter1.indexOf(driver);
    if (index >= 0) {
      this.driversFilter1.splice(index, 1);
      this.driverList1.splice(index, 1)
    }
  }
  displayFnDriverId(data): string {
    return data ? '' : '';
  }
  openDialog(): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        const params = {
          deviceData: this.deviceAddData,
          company_id: this.companyId
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._deviceService.deleteTempshift(enc_data).subscribe((dec) => {
          if (dec) {
            if (dec.status == 201) {
              this.toastr.error('Failed to delete standby driver')
            } else if (dec.status == 200) {
              var deleteresponse: any = this.encDecService.dwt(this.session, dec.data);
              this.deviceAddData = deleteresponse.data;
              this.tempShiftData = '';
              this.driversFilter1 = [];
              this.driverList1 = [];
              this.additional_info = '';
            }
          }
        });
      }
    });
  }
  addTempShift() {
    var shiftstart = moment(this.selectedMoment);
    var shiftend = moment(this.selectedMoment1);
    let diff = shiftend.diff(shiftstart, 'hours');
    if (diff < 2) {
      this.toastr.error('Shift duration should be minimum of 2 hours')
    }
    else if (this.driverList1.length == 0) {
      this.toastr.error('Please select a driver')
    } else if (this.selectedMoment == '' || this.selectedMoment1 == '') {
      this.toastr.error('Invalid Shift start and end times')
    } else {
      var user = JSON.parse(window.localStorage['adminUser']);
      if (!user) {
        this.toastr.error("Session Expired..please login again");
      } else {
        const params = {
          deviceData: this.deviceAddData,
          user_id: user._id,
          from_time: moment(this.selectedMoment).format('YYYY-MM-DD HH:mm'),
          to_time: moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm'),
          driver_id: this.driverList1[0],
          additinal_data: this.additional_info,
          company_id: this.companyId
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._deviceService.addTempshift(enc_data).subscribe((dec) => {
            if (dec && dec.status == 200) {
              var addresponse: any = this.encDecService.dwt(this.session, dec.data);
              this.tempShiftData = '';
              this.driversFilter1 = [];
              this.driverList1 = [];
              this.additional_info = '';
              this.deviceAddData = addresponse.data;
              if (this.deviceAddData.temp_shift_id && this.deviceAddData.temp_shift_id !== null) {
                this.getTempshiftDetails(this.deviceAddData.temp_shift_id);
              }
            } else {
              this.toastr.error('Failed to add Standby driver')
            }
        });
      }
    }

  }
  getMax(date) {
    return this.getDate1(moment(date).add(12, 'hours').format('YYYY-MM-DD HH:mm'))
  }
}
