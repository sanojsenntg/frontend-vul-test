
// Import Required Dependencies and/or Modules
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr';
import { CompanyService } from '../../../../common/services/companies/companies.service';
// Import required Services
import { JwtService } from '../../../../common/services/api/jwt.service';
import { DeviceService } from '../../../../common/services/devices/devices.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-add-devices',
  templateUrl: './add-devices.component.html',
  styleUrls: ['./add-devices.component.css'],
  providers: [DeviceService, JwtService]
})
export class AddDevicesComponent implements OnInit {
  public saveForm: FormGroup;
  private sub: Subscription;
  public companyId: any = [];
  public ud_id;
  public CompanyData;
  email: string;
  session: string;
  constructor(private _deviceService: DeviceService,
    private router: Router,
    public encDecService: EncDecService,
    public jwtService: JwtService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public toastr: ToastsManager,
    private _companyservice: CompanyService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.toastr.setRootViewContainerRef(vcr);
    this.saveForm = formBuilder.group({
      ud_id: ['', [
        Validators.required,
        Validators.maxLength(20)
      ]],
      device_model: ['', [
        Validators.required,
        Validators.maxLength(20)
      ]],
      manufacturer: ['', [
        Validators.required,
        Validators.maxLength(20)
      ]],
      company: ['', [
      ]],
    });
  }
  ngOnInit() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CompanyData = data.getCompanies;
        }
      }
      //this.searchSubmit = false;
    });
  }
  public addDevice() {

    if (!this.saveForm.valid) {
      this.toastr.error('All fields are required');
      return;
    }
    const devicedata = {
      device_model: this.saveForm.value.device_model,
      ud_id: this.saveForm.value.ud_id,
      company: this.saveForm.value.company,
      manufacturer: this.saveForm.value.manufacturer,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, devicedata);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._deviceService.addDevices(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            this.toastr.success('Device added successfully');
            this.router.navigate(['/admin/fleet/devices']);
          } else {
            this.toastr.error('Device addition failed');
          }
        }
      })
      .catch((error) => {
        this.toastr.error('Device addition failed');
        this.router.navigate(['/login']);
      });
  }
}
