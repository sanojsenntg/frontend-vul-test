import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import {ListDevicesComponent} from './list-devices/list-devices.component';
import {AddDevicesComponent} from './add-devices/add-devices.component';
import {EditDevicesComponent} from './edit-devices/edit-devices.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const deviceRoutes: Routes = [
  { path: '', component: ListDevicesComponent, canActivate: [AclAuthervice], data: {roles: ["Devices - List"]}},
  { path: 'add', component: AddDevicesComponent, canActivate: [AclAuthervice], data: {roles: ["Devices - Add"]}},
  { path: 'edit/:id', component: EditDevicesComponent, canActivate: [AclAuthervice], data: {roles: ["Device - Edit"]}}
  ];
