import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { DeviceService } from '../../../common/services/devices/devices.service';
import { DeviceRestService } from '../../../common/services/devices/devicesrest.service';
import { ListDevicesComponent } from './list-devices/list-devices.component';
import { AddDevicesComponent } from './add-devices/add-devices.component';
import { EditDevicesComponent } from './edit-devices/edit-devices.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import { EditDriverComponent } from './dialog/edit-driver/edit-driver.component';
import { EditVehicleComponent } from './dialog/edit-vehicle/edit-vehicle.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule, MatNativeDateModule,MatChipsModule,MatIconModule } from '@angular/material';
import { DeviceDialogComponent} from '../../../common/dialog/device-dialog/device-dialog.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { GlobalService } from '../../../common/services/global/global.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    Ng2OrderModule,
    FormsModule,
    MatTooltipModule,
    MatDialogModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatChipsModule,
    MatIconModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule 
  ],
  declarations: [ 
    ListDevicesComponent, 
    AddDevicesComponent, 
    EditDevicesComponent, 
    EditDriverComponent, 
    EditVehicleComponent,
    DeviceDialogComponent
  ],
   exports: [
    RouterModule,
    MatDialogModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ],
  providers :[
    GlobalService,
    DeviceService,
    DeviceRestService,
    { provide: MAT_DIALOG_DATA,useValue: {}}
  ],
  entryComponents : [ 
    EditDriverComponent,
    EditVehicleComponent,
    DeviceDialogComponent 
  ],
  
})
export class DevicesModule { }

