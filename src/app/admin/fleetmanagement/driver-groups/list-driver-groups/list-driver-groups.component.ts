import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { DriverGroupService } from '../../../../common/services/driver_groups/driver_groups.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-driver-groups',
  templateUrl: './list-driver-groups.component.html',
  styleUrls: ['./list-driver-groups.component.css']
})

export class ListDriverGroupsComponent implements OnInit {
  public companyId: any = [];
  del = false;
  public searchSubmit = false;
  public _id;
  sortOrder = 'asc';
  pageSize = 10;
  pageNo = 0;
  public is_search = false;
  driverGroupLength;
  page = 1;
  public name;
  key: string = '';
  reverse: boolean = false;
  public keyword;
  public driverGroupData;
  email: string;
  session: string;

  constructor(private _driverGroupService: DriverGroupService,
    private router: Router,
    public jwtService: JwtService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
  }

  ngOnInit() {
    this.getDriverGroupListing();
  }

  public getDriverGroupListing() {
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
      search: ''
    };
    this.is_search = false;
    this.del = true;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
    }
    this._driverGroupService.getDriversGroup(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.driverGroupData = data.getDriverGroups;
        this.driverGroupLength = data.count;
      }
      this.searchSubmit = false;
      this.del = false;
    });
  }

  /**
   * Confirmation popup for deleting particular driver group record
   * @param id 
   */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '350px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this data' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.delete(id);
      } else {
      }
    });
  }

  /**
   * Delete driver group from view
   * @param id 
   */
  public delete(id) {
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverGroupService.updateDriverGroupForDeletion(enc_data)
      .then((dec) => {
        if (dec && dec.status === 200) {
          this.refresh();
          this.driverGroupData.splice(id, 1);
          this.toastr.success('Driver Group deleted successfully.');
        }
      })
  }

  /**
   * Sorting the driver groups records
   * @param key  
   */
  sort(key) {
    this.searchSubmit = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.sortOrder,
      sortByColumn: this.key,
      company_id: this.companyId,
      search: this.keyword
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverGroupService.getDriversGroup(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.driverGroupData = data.getDriverGroups;
        this.driverGroupLength = data.count;
      }
      this.searchSubmit = false;
    });
  }

  /**
   * Pagination for driver groups module
   * @param data 
   */
  pagingAgent(data) {
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverGroupService.getDriversGroup(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.driverGroupData = data.getDriverGroups;
      }
      this.searchSubmit = false;
    });
  }

  /**
   * For searching the driver groups records
   * @param keyword 
   */
  public getDriverGroup(keyword) {
    this.searchSubmit = true;
    this.keyword = keyword;


    if (this.keyword) {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        search: this.keyword,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this.del = true;
      this._driverGroupService.getDriversGroup(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.driverGroupData = data.getDriverGroups;
          this.driverGroupLength = data.count;
        }
        this.searchSubmit = false;
        this.del = false;
        this.is_search = true;
      });
    } else {
      this.del = true;
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        search: '',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._driverGroupService.getDriversGroup(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.driverGroupData = data.getDriverGroups;
          this.driverGroupLength = data.count;
        }
        this.searchSubmit = false;
        this.del = false;
        this.is_search = false;
      });
    }
  }

  /**
   * To refresh driver group records showing in the grid
   */
  refresh() {
    if (this.is_search == true) {
      this.searchSubmit = true;
      this.searchSubmit = false;
    }
    else {
      this.del = true;
      this.ngOnInit();
    }
  }

  /**
   * To reset the search filters and reload the driver group records
   */
  reset() {
    this.keyword = '';
    this.name = '';
    this.ngOnInit();
  }

}
