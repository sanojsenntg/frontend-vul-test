import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDriverGroupsComponent } from './add-driver-groups.component';

describe('AddDriverGroupsComponent', () => {
  let component: AddDriverGroupsComponent;
  let fixture: ComponentFixture<AddDriverGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDriverGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDriverGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
