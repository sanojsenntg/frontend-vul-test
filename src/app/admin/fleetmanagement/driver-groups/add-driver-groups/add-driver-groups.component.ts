import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr';

import { JwtService } from '../../../../common/services/api/jwt.service';
import { DriverGroupService } from '../../../../common/services/driver_groups/driver_groups.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-driver-groups',
  templateUrl: './add-driver-groups.component.html',
  styleUrls: ['./add-driver-groups.component.css'],
  providers: [DriverGroupService, JwtService]
})
export class AddDriverGroupsComponent implements OnInit {
  public companyId: any = [];
  public saveForm: FormGroup;
  private sub: Subscription;
  public _id;
  email: string;
  session: string;
  constructor(private _driverGroupService: DriverGroupService,
    private router: Router,
    public jwtService: JwtService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.saveForm = formBuilder.group({
      name: ['', [
        Validators.required,
      ]],
      contact_name: ['', [
        Validators.required,
      ]],
      contact_surname: ['', [
        Validators.required,
      ]],
      street: ['', [
        Validators.required,
      ]],
      house_number: ['', [
        Validators.required,
      ]],
      city: ['', [
        Validators.required,
      ]],
      company: ['', [
      ]],
    });
  }

  ngOnInit() {
  }

  /**
   * Add the driver groups records
   */
  public addDriverGroups() {
    if (!this.saveForm.valid) {
      // this.toastr.error('All fields are required');
      return;
    }
    const driverGroupData = {
      name: this.saveForm.value.name,
      contact_name: this.saveForm.value.contact_name,
      company: this.saveForm.value.company,
      contact_surname: this.saveForm.value.contact_surname,
      street: this.saveForm.value.street,
      city: this.saveForm.value.city,
      house_number: this.saveForm.value.house_number,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, driverGroupData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverGroupService.addDriverGroup(enc_data)
      .then((dec) => {
        if (dec && dec.status == 200) {
          this.toastr.success('Driver Group added successfully');
          this.router.navigate(['/admin/fleet/driver-groups']);
        }
      })
      .catch((error) => {
        this.toastr.error('Driver Group addition failed');
        this.router.navigate(['/login']);
      });
  }

}
