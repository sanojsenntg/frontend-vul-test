import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import {ListDriverGroupsComponent} from './list-driver-groups/list-driver-groups.component';
import { AddDriverGroupsComponent } from './add-driver-groups/add-driver-groups.component';
import { EditDriverGroupsComponent } from './edit-driver-groups/edit-driver-groups.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const driverGroupsRoutes: Routes = [
  
    { path: '', component: ListDriverGroupsComponent, canActivate: [AclAuthervice], data: {roles: ["Driver Groups - List"]}},
    { path: 'add', component: AddDriverGroupsComponent, canActivate: [AclAuthervice], data: {roles: ["Driver Groups - Add"]}},
    { path: 'edit/:id', component: EditDriverGroupsComponent, canActivate: [AclAuthervice], data: {roles: ["Driver Groups - Edit"]}},
      
    ];