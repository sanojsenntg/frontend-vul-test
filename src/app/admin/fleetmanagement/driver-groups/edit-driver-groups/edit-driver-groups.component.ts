import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PaymentService } from '../../../../common/services/payment/payment.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { DriverGroupService } from '../../../../common/services/driver_groups/driver_groups.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-driver-groups',
  templateUrl: './edit-driver-groups.component.html',
  styleUrls: ['./edit-driver-groups.component.css']
})
export class EditDriverGroupsComponent implements OnInit {

  public companyId: any = [];
  public serviceData;
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  session: string;
  email: string;
  constructor(private _paymentService: PaymentService,
    private _driverGroupService: DriverGroupService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      name: ['', [
        Validators.required,

      ]],
      contact_name: ['', [
        Validators.required,

      ]],
      contact_surname: ['', [
        Validators.required,

      ]],
      street: ['', [
        Validators.required,

      ]],
      house_number: ['', [
        Validators.required,

      ]],
      city: ['', [
        Validators.required
      ]],
    });
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    })
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverGroupService.getById(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        res=res.SingleDetail;
        this.editForm.setValue({
          name: res.name ? res.name : '',
          contact_name: res.contact_name ? res.contact_name : '',
          contact_surname: res.contact_surname ? res.contact_surname : '',
          street: res.street ? res.street : '',
          house_number: res.house_number ? res.house_number : '',
          city: res.city ? res.city : '',
        });
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });

  }

  /**
   * Update the driver groups records
   */
  public updateDriverGroup() {
    if (!this.editForm.valid) {
      // this.toastr.error('All fields are required');
      return;
    }
    const driverGroupData = {
      name: this.editForm.value.name,
      contact_name: this.editForm.value.contact_name,
      contact_surname: this.editForm.value.contact_surname,
      street: this.editForm.value.street,
      house_number: this.editForm.value.house_number,
      city: this.editForm.value.city,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, driverGroupData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverGroupService.updateDriverGroupDetails(enc_data)
      .then((dec) => {
        if (dec && dec.status == 200) {
          this.toastr.success('Driver Group Updated successfully.');
          this.router.navigate(['/admin/fleet/driver-groups']);
        }
      })
      .catch((error) => {
        this.toastr.error('Driver Group updation cancelled');
      });
  }
}
