import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDriverGroupsComponent } from './edit-driver-groups.component';

describe('EditDriverGroupsComponent', () => {
  let component: EditDriverGroupsComponent;
  let fixture: ComponentFixture<EditDriverGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDriverGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDriverGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
