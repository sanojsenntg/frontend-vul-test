import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { IMyDpOptions } from 'mydatepicker';
import * as moment from 'moment/moment';
import { Categories } from '../../../../common/services/categories/categories.service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.component.html',
  styleUrls: ['./add-vehicle.component.css']
})
export class AddVehicleComponent implements OnInit {
  public companyId: any = [];
  public minDate = new Date();
  public vehicleModelData: any;
  public data;
  public categoryData;
  public categoriesData;
  public CompanyData;
  public dateObj = new Date();
  public dtc = false;
  public myDatePickerExpiryOptions: IMyDpOptions = {
    disableUntil: { year: this.dateObj.getUTCFullYear(), month: this.dateObj.getUTCMonth() + 1, day: this.dateObj.getUTCDate() },
    // other options...
    dateFormat: 'yyyy-mm-dd',

  };
  public insurance_expiration_date;
  public vehicle: any = {
    registration_number: '',
    licence_number: '',
    display_name: '',
    vehicle_identity_number: '',
    insurance_expiration_date: '',
    vehicle_model: '',
    vehicle_model_id: '',
    category_id: '',
    sub_category_id: '',
    plate_left_text: '',
    plate_middle_text: '',
    plate_right_text: '',
    plate_number: '',
    plate_type: '',
    register_with_wasl: '',
    send_to_rta: '',
    in_maintenance: '',
    credit_card: '',
    company: '',
    vehicle_color:'',
    company_id: '',
    vehicle_policy_number:'',
    vehicle_expiry_date:''
  };
  email: string;
  session: string;

  constructor(private _toasterService: ToastsManager,
    private _categoryService: Categories,
    private router: Router,
    private _vehicleService: VehicleService,
    private _vehicleModelsService: VehicleModelsService,
    private _companyservice: CompanyService,
    vcr: ViewContainerRef,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    private toastr: ToastsManager) {
    this._toasterService.setRootViewContainerRef(vcr);
    const company_id: any = localStorage.getItem('user_company');
    if (company_id === '5ce12918aca1bb08d73ca25d' || company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.vehicle = {
      registration_number: '',
      licence_number: '',
      display_name: '',
      vehicle_identity_number: '',
      insurance_expiration_date: '',
      vehicle_model: '',
      vehicle_model_id: '',
      category_id: '',
      sub_category_id: '',
      plate_left_text: '',
      plate_middle_text: '',
      plate_right_text: '',
      plate_number: '',
      plate_type: '',
      register_with_wasl: '',
      send_to_rta: '',
      in_maintenance: '',
      credit_card: '',
      company: '',
      vehicle_color:'',
      vehicle_policy_number:'',
      vehicle_expiry_date:'',
      company_id: this.companyId
    };
  }

  ngOnInit() {
    this.getCategories();
    this.getVehicleModels();
    const params = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CompanyData = data.getCompanies;
      }
      //this.searchSubmit = false;
    });
  }

  /**
   * Save Vehicle data
   * @param formData
   */
  onSubmit(formData): void {
    if (formData.valid) {
      if (typeof this.vehicle.insurance_expiration_date == 'object') {
        this.vehicle.insurance_expiration_date = this.vehicle.insurance_expiration_date.formatted;
      }
      if (typeof this.vehicle.vehicle_expiry_date == 'object') {
        this.vehicle.vehicle_expiry_date = this.vehicle.vehicle_expiry_date.formatted;
      }
      this.vehicle.plate_number = this.vehicle.plate_left_text + ' ' + this.vehicle.plate_middle_text + ' ' + this.vehicle.plate_right_text;
      this.vehicle.insurance_expiration_date = this.vehicle.insurance_expiration_date ? this.vehicle.insurance_expiration_date : '';

      var encrypted = this.encDecService.nwt(this.session, this.vehicle);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._vehicleService.saveVehicles(enc_data)
        .then((dec) => {
          if (dec) {
            if (dec.status == 200) {
              this._toasterService.success('Vehicles added successfully.');
              this.router.navigate(['/admin/fleet/vehicles']);
            } else if (dec.status == 201) {
              this._toasterService.error('Vehicle addition failed.');
            } else if (dec.status == 203) {
              this._toasterService.warning('Registration number already exist.');
            } else if (dec.status == 205) {
              this._toasterService.warning('Licence number already exist');
            } else if (dec.status == 202) {
              this._toasterService.warning('Vehicle identity number already exist');
            } else if (dec.status == 206) {
              this._toasterService.warning('Plate number already exist.');
            }
          }
        })
        .catch((error) => {
        });
    } else {
      this._toasterService.error('All Fields are required');
      return;
    }
  }

  /**
   * Get Vehicle Model for drop down
   *
   */
  public getVehicleModels() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleModels(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleModelData = data.vehicleModelsList;
      }
    });
  }

  public getCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'category',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._categoryService.getCategories(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var servicedata: any = this.encDecService.dwt(this.session, dec.data);
        this.categoriesData = servicedata.getCategory;
      }
    });
  }

  public ShowSubCategories(categoryId) {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'sub_category',
      category_id: categoryId,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.getCategories(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var servicedata: any = this.encDecService.dwt(this.session, dec.data);
        this.categoryData = servicedata.getSubCategory;
      }
    });
  }
}




