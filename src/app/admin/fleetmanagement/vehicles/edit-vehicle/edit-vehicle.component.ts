import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { IMyDpOptions } from 'mydatepicker';
import * as moment from 'moment/moment';
import { Categories } from '../../../../common/services/categories/categories.service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-vehicle',
  templateUrl: './edit-vehicle.component.html',
  styleUrls: ['./edit-vehicle.component.css']
})
export class EditVehicleComponent implements OnInit {
  public dtc=false;
  public companyId: any = [];
  public minDate = new Date();
  public editForm: FormGroup;
  private sub: Subscription;
  public categoryData;
  public categoriesData;
  public _id;
  public partnerData;
  public tariffData;
  public vehicleModelData;
  public dateObj = new Date();
  public CompanyData;

  public myDatePickerExpiryOptions: IMyDpOptions = {
    disableUntil: { year: this.dateObj.getUTCFullYear(), month: this.dateObj.getUTCMonth() + 1, day: this.dateObj.getUTCDate() },
    // other options...
    dateFormat: 'yyyy-mm-dd',

  };
  email: string;
  session: string;

  constructor(private _vehicleService: VehicleService,
    private _vehicleModelsService: VehicleModelsService,
    formBuilder: FormBuilder,
    public encDecService: EncDecService,
    private _categoryService: Categories,
    private route: ActivatedRoute,
    private router: Router,
    private _companyservice: CompanyService,
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    public jwtService: JwtService) {
    this.toastr.setRootViewContainerRef(vcr);
    const company_id: any = localStorage.getItem('user_company');
    if (company_id === '5ce12918aca1bb08d73ca25d' || company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      registration_number: ['', [Validators.required]],
      licence_number: ['', [Validators.required]],
      display_name: ['', [Validators.required]],
      vehicle_identity_number: ['', [Validators.required]],
      insurance_expiration_date: [''],
      vehicle_model: ['', [Validators.required]],
      plate_left_text: ['', [Validators.required]],
      category_id: [''],
      sub_category_id: [''],
      plate_middle_text: ['', [Validators.required]],
      plate_right_text: ['', [Validators.required]],
      plate_number:[''],
      plate_type: [''],
      register_with_wasl: [''],
      send_to_rta: [''],
      in_maintenance: ['', [Validators.required]],
      credit_card: [''],
      vehicle_color:['',[Validators.required]],
      company: [''],
      vehicle_policy_number:[''],
      vehicle_expiry_date:['']
    });

    this.editForm.controls['plate_left_text'].valueChanges.subscribe(data => {
      var plate_number = this.editForm.controls['plate_left_text'].value + ' ' + this.editForm.controls['plate_middle_text'].value + ' ' + this.editForm.controls['plate_right_text'].value;
      this.editForm.controls['plate_number'].setValue(plate_number);
    });
    this.editForm.controls['plate_middle_text'].valueChanges.subscribe(data => {
      var plate_number = this.editForm.controls['plate_left_text'].value + ' ' + this.editForm.controls['plate_middle_text'].value + ' ' + this.editForm.controls['plate_right_text'].value;
      this.editForm.controls['plate_number'].setValue(plate_number);
    });
    this.editForm.controls['plate_right_text'].valueChanges.subscribe(data => {
      var plate_number = this.editForm.controls['plate_left_text'].value + ' ' + this.editForm.controls['plate_middle_text'].value + ' ' + this.editForm.controls['plate_right_text'].value;
      this.editForm.controls['plate_number'].setValue(plate_number);
    });


  }
  public ngOnInit(): void {
    this.getVehicleModels();
    this.getCategories();
    const params = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at'
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CompanyData = data.getCompanies;
      }
      //this.searchSubmit = false;
    });

    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params1 = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted1 = this.encDecService.nwt(this.session, params1);
    var enc_data1 = {
      data: encrypted1,
      email: this.email
    }
    this._vehicleService.getById(enc_data1).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        res=res.SingleDetail;
        this.editForm.setValue({
          registration_number: res.registration_number ? res.registration_number : '',
          licence_number: res.licence_number ? res.licence_number : '',
          display_name: res.display_name ? res.display_name : '',
          vehicle_identity_number: res.vehicle_identity_number ? res.vehicle_identity_number : '',
          insurance_expiration_date: res.insurance_expiration_date ? this.dateformat(res.insurance_expiration_date) : '',
          vehicle_model: res.vehicle_model ? res.vehicle_model : '',
          category_id: res.category_id ? res.category_id : '',
          sub_category_id: res.sub_category_id ? res.sub_category_id : '',
          plate_left_text: res.plate_left_text ? res.plate_left_text : '',
          plate_middle_text: res.plate_middle_text ? res.plate_middle_text : '',
          plate_right_text: res.plate_right_text ? res.plate_right_text : '',
          plate_number: res.plate_number ? res.plate_number : '',
          plate_type: res.plate_type ? res.plate_type : '',
          register_with_wasl: res.register_with_wasl ? res.register_with_wasl : '',
          send_to_rta: res.send_to_rta ? res.send_to_rta : '',
          in_maintenance: res.in_maintenance ? res.in_maintenance : '',
          credit_card: res.credit_card ? res.credit_card : '',
          vehicle_color : res.vehicle_color ? res.vehicle_color : '',
          company: res.company_id ? res.company_id : '',
          vehicle_policy_number:res.vehicle_policy_number ? res.vehicle_policy_number : '',
          vehicle_expiry_date: res.vehicle_expiry_date ? this.dateformat(res.vehicle_expiry_date) : ''
        });
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
  }

  /**
   * Update vehicle records
   */
  public updateVehicle() {
    if (!this.editForm.valid) {
      return;
    }
    const vehicleData = {
      registration_number: this.editForm.value.registration_number ? this.editForm.value.registration_number : null,
      licence_number: this.editForm.value.licence_number ? this.editForm.value.licence_number : null,
      display_name: this.editForm.value.display_name ? this.editForm.value.display_name : null,
      vehicle_identity_number: this.editForm.value.vehicle_identity_number ? this.editForm.value.vehicle_identity_number : null,
      insurance_expiration_date: this.editForm.value.insurance_expiration_date ? this.editForm.value.insurance_expiration_date.formatted : null,
      vehicle_model: this.editForm.value.vehicle_model ? this.editForm.value.vehicle_model : null,
      plate_left_text: this.editForm.value.plate_left_text ? this.editForm.value.plate_left_text : null,
      plate_middle_text: this.editForm.value.plate_middle_text ? this.editForm.value.plate_middle_text : null,
      plate_right_text: this.editForm.value.plate_right_text ? this.editForm.value.plate_right_text : null,
      plate_number: this.editForm.value.plate_number ? this.editForm.value.plate_number : null,
      plate_type: this.editForm.value.plate_type ? this.editForm.value.plate_type : null,
      category_id: this.editForm.value.category_id ? this.editForm.value.category_id : null,
      sub_category_id: this.editForm.value.sub_category_id ? this.editForm.value.sub_category_id : null,
      register_with_wasl: this.editForm.value.register_with_wasl ? this.editForm.value.register_with_wasl : null,
      send_to_rta: this.editForm.value.send_to_rta ? this.editForm.value.send_to_rta : null,
      in_maintenance: this.editForm.value.in_maintenance ? this.editForm.value.in_maintenance : "false",
      credit_card: this.editForm.value.credit_card ? this.editForm.value.credit_card : '',
      company: this.editForm.value.company ? this.editForm.value.company : null,
      vehicle_color: this.editForm.value.vehicle_color ? this.editForm.value.vehicle_color : null,
      vehicle_policy_number:this.editForm.value.vehicle_policy_number ? this.editForm.value.vehicle_policy_number : null,
      vehicle_expiry_date:this.editForm.value.vehicle_expiry_date ? this.editForm.value.vehicle_expiry_date.formatted : null,
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, vehicleData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.updateVehicleDetails(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            this.toastr.success('Vehicles updated successfully.');
            this.router.navigate(['/admin/fleet/vehicles']);
          } else if (dec.status == 201) {
            this.toastr.error('Vehicles updation failed.');
          } else if (dec.status === 203) {
            this.toastr.warning('Registration number already exist. ');
          } else if (dec.status === 205) {
            this.toastr.warning('Licence number already exist.');
          } else if (dec.status === 202) {
            this.toastr.warning('Vehicle identity number already exist.');
          } else if (dec.status === 206) {
            this.toastr.warning('Plate number already exist.');
          }
        }
      })
      .catch((error) => {
      });
  }

  public dateformat(date) {
    if (date) {
      const dateObj = new Date(date);
      const month = dateObj.getUTCMonth() + 1;
      const day = dateObj.getUTCDate();
      const year = dateObj.getUTCFullYear();
      const dateObject = { date: { year: year, month: month, day: day } };
      return (dateObject);
    } else {
      return '';
    }

  }
  /**
  * Get Vehicle model Services for drop down
  *
  */
  public getVehicleModels() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleModels(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleModelData = data.vehicleModelsList;
      }
    });
  }

  public getCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'category',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._categoryService.getCategories(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var servicedata: any = this.encDecService.dwt(this.session, dec.data);
        this.categoriesData = servicedata.getCategory;
      }
    });
  }

  public ShowSubCategories(categoryId) {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'sub_category',
      category_id: categoryId,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.getCategories(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var servicedata: any = this.encDecService.dwt(this.session, dec.data);
        this.categoryData = servicedata.getSubCategory;
      }
    });
  }

}
