import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { EditDialogComponent } from './dialog/edit-dialog/edit-dialog.component';
import { EditStatusDialogComponent } from './dialog/edit-status-dialog/edit-status-dialog.component'
import { UploadDialogComponent } from './dialog/upload-dialog/upload-dialog.component';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { MatSelectModule } from '@angular/material/select';
import * as moment from 'moment/moment';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { environment } from '../../../../../environments/environment';
import { FormControl } from '@angular/forms';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

declare var jsPDF;

declare var templateToPdf;
@Component({
  selector: 'app-admin-list-vehicle',
  templateUrl: './list-vehicle.component.html',
  styleUrls: ['./list-vehicle.component.css']
})
export class ListVehicleComponent implements OnInit {
  queryField: FormControl = new FormControl();
  company: FormControl = new FormControl();
  public vehicleForm = '';
  public company_id = '';
  public companyData;
  public vehicleData;
  key: string = '';
  searchValue: string = '';
  reverse: boolean = false;
  pageSize = 10;
  public selectedMoment = '';
  pageNo = 0;
  totalPage = [1];
  public vehiclesListing;
  public _id;
  pNo = 1;
  sortOrder = 'asc';
  vehicleLength;
  public showLoader = false;
  public vehicle_model_id: any;
  public vehicle_unique_id: any;
  public insurance_expiration_date = '';
  public vehicleModelData;
  public vehicle_id;
  public is_search = false;
  del = false;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public aclVehicleStatus = true;
  public downloads = false;
  public loadingIndicator;
  public companyId: any = [];

  public imageurl;
  session: string;
  email: string;
  dtc: boolean = false;
  companyIdFilter: any;
  vehicleStatus=[];
  constructor(
    private router: Router,
    public encDecService: EncDecService,
    private _vehicleService: VehicleService,
    public dialog: MatDialog,
    public overlay: Overlay,
    private _vehicleModelsService: VehicleModelsService,
    private _toasterservice: ToastsManager,
    private _companyservice: CompanyService,
    public _aclService: AccessControlService,
    vcr: ViewContainerRef,
    public toastr: ToastsManager,
    public jwtService: JwtService
  ) {
    this.company_id = localStorage.getItem('user_company');
    if (this.company_id === '5ce12918aca1bb08d73ca25d' || this.company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(this.company_id);
    this._toasterservice.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.imageurl = environment.imgUrl;
    this.getCompanies()
    this.getVehicleModels();
    this.aclDisplayService();
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.is_search = false;
    this.del = true;
    this._vehicleService.getAllVehicleListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleData = data.getAllVehicle;
        this.vehicleLength = data.count;
      }
      this.del = false;
    });
    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchVehicle';
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'vehicle_unique_id',
          'search_keyword': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._vehicleService.searchVehicle(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status === 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.vehiclesListing = result.searchVehicle;
        }
        this.loadingIndicator = '';
      });
    this.company.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'asc',
          sortByColumn: 'unique_company_id',
          keyword: query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        };
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._companyservice.getCompanyListing(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status === 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = result.getCompanies;
        }
        this.loadingIndicator = '';
      });

  }

  displayFnCompany(data): string {
    return data ? data.company_name : data;
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Vehicles - Edit") {
            this.aclEdit = true;
          } else if (data.menu[i] == "Vehicles - Delete") {
            this.aclDelete = true;
          } else if (data.menu[i] == "Vehicles - Status Change") {
            this.aclVehicleStatus = false;
          } else if (data.menu[i] == "Vehicles - Add") {
            this.aclAdd = true;
          } else if (data.menu[i] == 'Downloads - Fleet Management') {
            this.downloads = true;
          }
        };
      }
    })
  }
  /**
   * Load list of vehicle models in vehicle models dropdown on page load.
   */
  public getVehicleModels() {
    let params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: ''
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleModels(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleModelData = data.vehicleModelsList;
      }
    });
  }

  /**
   * Autocomplete search for vehicle model.
   * @param data
   */
  searchVehicleModel(data) {
    this.loadingIndicator = 'searchVehicleModel';
    if (typeof data !== 'object') {
      let params = {
        offset: 0,
        limit: 0,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        'search': data,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._vehicleModelsService.getVehicleModels(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.vehicleModelData = data.vehicleModelsList;
        }
        this.loadingIndicator = '';
      });
    } else {
      this.vehicle_model_id = data._id;
      this.loadingIndicator = '';
    }
  }

  /**
   * Displaying name of vehicle in autocomplete search for vehicle model dropdown.
   * @param data
   */
  displayFnVehicleModel(data): string {
    return data ? data.name : data;
  }

  /**
   * Load list of vehicles in vehicle dropdown on page load.
   */
  public getVehicles() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.getAllVehicleListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehiclesListing = data.getAllVehicle;
      }
    });
  }

  /**
   * Autocomplete search for vehicles.
   * @param data
   */
  searchVehicle(data) {
    if (typeof data === 'object') {
      this.vehicle_id = data._id;
      this.loadingIndicator = '';
    }
    else {
      this.vehicle_id = '';
      this.loadingIndicator = '';
    }
  }

  /**
   * Displaying licence_number of vehicle in autocomplete search for vehicle dropdown.
   * @param data
   */
  displayFnVehicles(data): string {
    return data ? data.licence_number : data;
  }

  /**
   * Used for search on vehicles.
   */
  public searchVehicles() {
    this.pNo = 1
    this.showLoader = true;
    this.is_search = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      vehicle_id: this.vehicle_id ? this.vehicle_id : '',
      vehicle_model_id: this.vehicle_model_id ? this.vehicle_model_id._id : '',
      insurance_expiration_date: this.insurance_expiration_date ? this.insurance_expiration_date : '',
      vehicle_status: this.vehicleStatus.length>0?this.vehicleStatus:[]
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._vehicleService.searchVehicle(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleData = data.searchVehicle;
        this.vehicleLength = data.count;
      }
      this.showLoader = false;
    });

  }

  /**
   * Open dialog for delete a particular vehicle by id.
   * @param id
   */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '600px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this data' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deleteVehicle(id);
      }
    });
  }

  /**
   * Fuction to delete a vehicle by id.
   * @param id
   */
  public deleteVehicle(id) {
    this.showLoader = true;
    var params = {
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.updateVehicleForDeletion(enc_data).then((dec) => {
      if (dec) {
        if (dec.status === 200) {
          this.searchVehicles();
          this.vehicleData.splice(id, 1);
          this._toasterservice.success('Vehicle deleted successfully.');
        } else if (dec.status === 201) {
        } else if (dec.status === 203) {
          this._toasterservice.warning('There is shift assign to this vehicle, so you cannot delete it.');
        }
      }
      this.showLoader = false;
    });
  }
  openUploadDialog(): void {
    let dialogRef = this.dialog.open(UploadDialogComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openChangeStatusDialog(vehicleAddData1): void {
    let dialogRef = this.dialog.open(EditStatusDialogComponent, {
      width: '400px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: vehicleAddData1
    });
    dialogRef.afterClosed().subscribe(result => {
      this.searchVehicles();
    });
  }
  /**
   * Open edit additional service dialog.
   * @param id
   */
  openEditDialog(vehicleAddData1): void {
    let dialogRef = this.dialog.open(EditDialogComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: vehicleAddData1
    });
    dialogRef.afterClosed().subscribe(result => {
      this.searchVehicles()
    });
  }
  /**
   * Reset the search filters and refresh the page.
   */
  reset() {
    this.vehicle_unique_id = '';
    this.vehicle_model_id = '';
    this.insurance_expiration_date = '';
    this.vehicle_id = '';
    this.del = true;
    this.vehicleData = [];
    this.vehicle_model_id = ''
    this.vehicleForm = '';
    this.pNo = 1;
    this.vehicleStatus=[];
    if (this.dtc) {
      this.companyId = this.companyData.slice().map(x => x._id);
      this.companyIdFilter = this.companyId.slice()
      this.companyIdFilter.push('all')
    }
    this.ngOnInit();
  }
  selectAllCompany() {
    if (this.companyId.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.companyId = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.companyId = [];
      this.companyIdFilter = [];
    }
  }

  /**
   * Refresh the page.
   */
  refresh() {
    if (this.is_search == true) {
      this.searchVehicles();
    } else {
      this.vehicleData = [];
      this.ngOnInit();
    }
  }

  /**
   *Used in pagination.
   * @param data
   */
  pagingAgent(data) {
    this.vehicleData = [];
    this.showLoader = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    var params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        vehicle_id: this.vehicle_id ? this.vehicle_id : '',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        vehicle_model_id: this.vehicle_model_id ? this.vehicle_model_id._id : '',
        insurance_expiration_date: this.insurance_expiration_date ? this.insurance_expiration_date : '',
        vehicle_status: this.vehicleStatus.length>0?this.vehicleStatus:[]
      };

    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._vehicleService.searchVehicle(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleData = data.searchVehicle;
        this.vehicleLength = data.count;
      }
      this.showLoader = false;
      this.is_search = true;
    });
  }

  /**
   * Used for sorting all records.
   * @param key
   */
  sort(key) {
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
    this.vehicleData = [];
    var params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        vehicle_id: this.vehicle_id ? this.vehicle_id : '',
        vehicle_model_id: this.vehicle_model_id ? this.vehicle_model_id._id : '',
        insurance_expiration_date: this.insurance_expiration_date ? this.insurance_expiration_date : '',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        vehicle_status: this.vehicleStatus.length>0?this.vehicleStatus:[]
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        vehicle_status: this.vehicleStatus.length>0?this.vehicleStatus:[]
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._vehicleService.searchVehicle(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleData = data.searchVehicle;
        this.vehicleLength = data.count;
      }
      this.showLoader = false;
    });
  }

  public createCsv() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      vehicle_id: this.vehicle_id ? this.vehicle_id : '',
      vehicle_model_id: this.vehicle_model_id ? this.vehicle_model_id._id : '',
      insurance_expiration_date: this.insurance_expiration_date ? this.insurance_expiration_date : '',
      fromCSV: true,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      vehicle_status: this.vehicleStatus.length>0?this.vehicleStatus:[]
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.searchVehicle(enc_data).then((dec) => {
      //this.showLoader = true;
      //this.vehicleData = data.searchVehicle;
      //this.vehicleLength = data.count;
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        let labels = [
          'Registration No',
          'License No',
          'Display Name',
          'Vehicle Model',
          'Insurance Expiry Date',
          'Plate Number',
          'Plate Type',
          'Vehicle Identity Number',
          'Register With WASL',
          'Send To RTA',
          'Maintenance Status',
          'Created At',
          'Credit Card Machine',
          'Vehicle status'
        ];
        let resArray = [];

        for (let i = 0; i < res.searchVehicle.length; i++) {
          const csvArray = res.searchVehicle[i];

          resArray.push
            ({
              registration_number: csvArray.registration_number ? csvArray.registration_number : 'N/A',
              license_number: csvArray.license_number ? csvArray.license_number : 'N/A',
              display_name: csvArray.display_name ? csvArray.display_name : 'N/A',
              vehicle_model: csvArray.vehicle_model && csvArray.vehicle_model.name ? csvArray.vehicle_model.name : 'N/A',
              insuarance_expiration_date: csvArray.insuarance_expiration_date ? csvArray.insuarance_expiration_date : 'N/A ',
              plate_number: csvArray.plate_number ? csvArray.plate_number : 'N/A',
              plate_type: csvArray.plate_type ? csvArray.plate_type : 'N/A',
              vehicle_identity_number: csvArray.vehicle_identity_number ? csvArray.vehicle_identity_number : 'N/A',
              register_with_wasl: csvArray.register_with_wasl ? csvArray.register_with_wasl : 'N/A',
              send_to_rta: csvArray.send_to_rta ? csvArray.send_to_rta : 'N/A',
              maintenance_status: csvArray.maintenance_status ? csvArray.maintenance_status : 'N/A',
              created_at: csvArray.created_at ? csvArray.created_at : 'N/A ',
              credit_card: csvArray.credit_card ? csvArray.credit_card : 'Not Added',
              vehicle_status: csvArray.vehicle_status ? csvArray.vehicle_status : 'N/A'
            });
        }
        var options =
        {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: false,
          useBom: true,
          headers: (labels)
        };
        new Angular2Csv(resArray, 'Vehicle-List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
      }
    })
  }

  public generatePDF() {
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      vehicle_id: this.vehicle_id ? this.vehicle_id : '',
      vehicle_model_id: this.vehicle_model_id ? this.vehicle_model_id._id : '',
      insurance_expiration_date: this.insurance_expiration_date ? this.insurance_expiration_date : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      vehicle_status: this.vehicleStatus.length>0?this.vehicleStatus:[]
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.searchVehicle(enc_data).then((dec) => {
      if (dec && dec.status == 200) {


        var res: any = this.encDecService.dwt(this.session, dec.data);
        //this.showLoader = true;
        //this.vehicleData = data.searchVehicle;
        //this.vehicleLength = data.count;
        let labels = [
          'ID',
          'Registration No',
          'License No',
          'Display Name',
          'Vehicle Model',
          'Insurance Expiry Date',
          'Plate Number',
          'Plate Type',
          'Vehicle Identity Number',
          'Register With WASL',
          'Send To RTA',
          'Maintenance Status',
          'Additional Service',
          'Created At'
        ];
        let resArray = [];

        for (let i = 0; i < res.searchVehicle.length; i++) {
          const csvArray = res.searchVehicle[i];

          resArray.push
            ({
              Id: csvArray._id ? csvArray._id : 'N/A',
              registration_number: csvArray.registration_number ? csvArray.registration_number : 'N/A',
              license_number: csvArray.license_number ? csvArray.license_number : 'N/A',
              display_name: csvArray.display_name ? csvArray.display_name : 'N/A',
              vehicle_model: csvArray.vehicle_model ? csvArray.vehicle_model : 'N/A',
              insuarance_expiration_date: csvArray.insuarance_expiration_date ? csvArray.insuarance_expiration_date : 'N/A ',
              plate_number: csvArray.plate_number ? csvArray.plate_number : 'N/A',
              plate_type: csvArray.plate_type ? csvArray.plate_type : 'N/A',
              vehicle_identity_number: csvArray.vehicle_identity_number ? csvArray.vehicle_identity_number : 'N/A',
              register_with_wasl: csvArray.register_with_wasl ? csvArray.register_with_wasl : 'N/A',
              category: csvArray.category_id ? csvArray.category_id.category : 'N/A',
              sub_category: csvArray.sub_category_id ? csvArray.sub_category_id.sub_category : 'N/A',
              send_to_rta: csvArray.send_to_rta ? csvArray.send_to_rta : 'N/A',
              maintenance_status: csvArray.maintenance_status ? csvArray.maintenance_status : 'N/A',
              additional_service: csvArray.additional_service_id ? csvArray.additional_service_id : 'N/A',
              created_at: csvArray.created_at ? csvArray.created_at : 'N/A '
            });
        }
        //var doc = new jsPDF();
        var doc = new jsPDF('landscape');
        var col = [
          { title: "Id", dataKey: "Id" },
          { title: "Registraion Number", dataKey: "registration_number" },
          { title: "Licence Number", dataKey: "license_number" },
          { title: "Display Name", dataKey: "display_name" },
          { title: "Vehicle Model", dataKey: "vehicle_model" },
          { title: "Insurance Expiration date", dataKey: "insuarance_expiration_date" },
          { title: "Plate Number", dataKey: "plate_number" },
          { title: "Plate Type", dataKey: "plate_type" },
          { title: "Vehicle Identity Number", dataKey: "vehicle_identity_number" },
          { title: "Register With WASL", dataKey: "register_with_wasl" },
          { title: "Category", dataKey: "category" },
          { title: "Sub Category", dataKey: "sub_category" },
          { title: "Send to RTA", dataKey: "send_to_rta" },
          { title: "Maintanance Status", dataKey: "maintenance_status" },
          { title: "Additonal Service", dataKey: "additional_service" },
          { title: "Created At", dataKey: "created_at" }
        ];
        var options = {
          styles: { // Defaul style
            lineWidth: 0.01,
            lineColor: 0,
            fillStyle: 'DF',
            halign: 'center',
            valign: 'middle',
            columnWidth: 'auto',
            overflow: 'linebreak'
          },
        };
        doc.text(90, 10, "List - Vehicle");
        doc.autoTable(col, resArray, options);
        doc.save('List-Vehicle' + moment().format('YYYY-MM-DD_HH_mm_ss') + '.pdf');
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
  }
  changeCompany() {
    this.vehicleModelData = [];
    this.vehiclesListing = [];
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
          if (this.dtc && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher'  && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
            this.companyId = data.getCompanies.map(x => x._id);
            this.companyIdFilter = data.getCompanies.map(x => x._id)
            this.companyIdFilter.push('all')
          }
          this.getVehicles();
        }
      }
    });
  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.companyId = this.companyIdFilter.slice()
    }
    else {
      this.companyId = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }
}
