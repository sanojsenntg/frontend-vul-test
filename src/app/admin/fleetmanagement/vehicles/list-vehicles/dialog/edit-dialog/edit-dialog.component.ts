import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AdditionalService } from '../../../../../../common/services/additional_service/additional_service.service';
import { VehicleService } from '../../../../../../common/services/vehicles/vehicle.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-dialog.component',
  templateUrl: 'edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.css']
})
export class EditDialogComponent implements OnInit {
  public vehicleAddData;
  public serviceGroupLength;
  public serviceGroupData;
  public editForm: FormGroup;
  private sub: Subscription;
  public additionalData;
  public searchSubmit = false;
  public additional_service_id;
  public Data;
  public vehicleAddData1;
  public companyId: any = [];
  email: string;
  session: string;

  constructor(
    public _additionalService: AdditionalService,
    public _vehicleService: VehicleService,
    public dialogRef: MatDialogRef<EditDialogComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private router: Router,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.vehicleAddData1 = data;
    this.editForm = formBuilder.group({
      additional_service_id: ['']
    });
  }

  public ngOnInit(): void {
    this.vehicleAddData = this.vehicleAddData1;
    this.getAdditionalService();

    var params = {
      company_id: this.companyId,
      _id: this.vehicleAddData._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.getById(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        this.Data = res.additional_service_id;
        this.editForm.setValue({
          additional_service_id: res.additional_service_id ? res.additional_service_id : '',
        });
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
  }

  public updateVehicle() {

    if (!this.editForm.valid) {
      this.toastr.error('All fields are required.');
      return;
    }

    const vehicleData = {
      additional_service_id: this.editForm.value.additional_service_id ? this.editForm.value.additional_service_id : null,
      company_id: this.companyId,
      _id: this.vehicleAddData._id
    }
    var encrypted = this.encDecService.nwt(this.session, vehicleData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }

    this._vehicleService.updateAdditionalService(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status === 200) {
            this.toastr.success('Additional service updated successfully.');
            setTimeout(() => {
              this.dialogRef.close();
            }, 1000);
          } else if (dec.status === 201) {
            this.toastr.error('Additional service updation failed.');
          }
          this.router.navigate(['/admin/fleet/vehicles']);
        }
      })
      .catch((error) => {
      });
  }

  closePop() {
    this.dialogRef.close();
  }

  public deleteAdditionalService(index, _id) {
    this.searchSubmit = true;
    this.vehicleAddData.additional_service_id.splice(index, 1);
    var i = this.Data.indexOf(_id);
    this.Data.splice(i, 1);
    this.editForm.controls['additional_service_id'].setValue(this.Data);
  }

  /**
   * get additional service  list
   * @param
   */
  public getAdditionalService() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._additionalService.getaddService(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.serviceGroupData = data.getService;
        this.serviceGroupLength = data.count;
      }
    });
  }
}
