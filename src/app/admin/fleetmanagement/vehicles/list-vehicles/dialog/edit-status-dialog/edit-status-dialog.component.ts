import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VehicleService } from '../../../../../../common/services/vehicles/vehicle.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-status-dialog',
  templateUrl: './edit-status-dialog.component.html',
  styleUrls: ['./edit-status-dialog.component.css']
})
export class EditStatusDialogComponent implements OnInit {
  public vehicleAddData;
  public editForm: FormGroup;
  private sub: Subscription;
  public searchSubmit = false;
  public companyId: any = [];
  public statusinfo = {
    status: ''
  };
  public Data;
  public vehicleAddData1;
  email: string;
  session: string;
  note='';

  constructor(
    public _vehicleService: VehicleService,
    public dialogRef: MatDialogRef<EditStatusDialogComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private router: Router,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.vehicleAddData1 = data;
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      status: ['']
    });
  }
  public ngOnInit(): void {
    this.vehicleAddData = this.vehicleAddData1;
  }
  closePop() {
    this.dialogRef.close();
  }
  public updateVehicleStatus() {
    if (!this.editForm.valid || this.statusinfo.status == undefined || this.statusinfo.status == '') {
      this.toastr.error('Select status to update');
      return;
    }
    var user = JSON.parse(window.localStorage['adminUser']);
    if (!user) {
      this.toastr.error("Session Expired..please login again");
    } else {
      const vehicleData = {
        status: this.statusinfo.status ? this.statusinfo.status : 'A',
        email: user.email,
        user_id: user._id,
        priority: user.priority,
        company_id: this.companyId,
        _id: this.vehicleAddData._id,
        note: this.note
      }
      var encrypted = this.encDecService.nwt(this.session, vehicleData);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      //alert(JSON.stringify(vehicleData));
      this._vehicleService.updateVehicleStatus(enc_data)
        .then((dec) => {
          if (dec) {
            if (dec.status === 200) {
              this.toastr.success('Vehicle status updated successfully.');
              setTimeout(() => {
                this.dialogRef.close();
              }, 1000);
            } else if (dec.status === 201) {
              this.toastr.error('Vehicle status updation failed.');
            }
            else if (dec.status === 202) {
              this.toastr.error(dec.message);
            }
            else if (dec.status === 203) {
              this.toastr.warning(dec.message);
            }
            this.router.navigate(['/admin/fleet/vehicles']);
          }
        })
        .catch((error) => {
        });
    }
  }
}
