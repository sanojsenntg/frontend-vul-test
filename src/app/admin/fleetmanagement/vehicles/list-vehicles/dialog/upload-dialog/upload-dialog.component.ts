import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VehicleService } from '../../../../../../common/services/vehicles/vehicle.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.css']
})
export class UploadDialogComponent implements OnInit {
  public editForm: FormGroup;
  public formData;
  public companyId: any = [];
  private sub: Subscription;
  email: string;
  session: string;
  constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public jwtService: JwtService,
    private _vehicleService: VehicleService,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email= localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      file: [null, Validators.required]
    });
    this.formData = new FormData();
  }

  ngOnInit() {

  }
  public uploadfile() {
    const formData: FormData = new FormData();
    formData.append('file', this.fileToUpload, this.fileToUpload.name);
    formData['company_id'] = this.companyId;
    var encrypted = this.encDecService.nwt(this.session, formData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleService.uploadVehicleList(enc_data)
      .then((dec) => {
        if (dec) {
        if (dec.status === 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          this.toastr.success('Vehicles added Successfully');
          setTimeout(() => {
            this.dialogRef.close();
            this.router.navigate(['/admin/fleet/vehicles']);
          }, 1000);
        } else if (dec.status === 201) {
          //alert(JSON.stringify(res));
          this.toastr.success('Vehicles added Successfully');
          let failed_plate = "";
          for (var i = 0; i < res.data.length; ++i) {
            failed_plate = failed_plate + "," + res.data[0].vehicle_identity_number;
          }
          this.toastr.error('Duplicate Plate number' + failed_plate);
          this.dialogRef.close();
        } else if (dec.status === 202) {
          this.toastr.error('Vehichle adding failed');
          this.router.navigate(['/admin/fleet/vehicles']);
        }}
      })
      .catch((error) => {
      });
  }
  closePop() {
    this.dialogRef.close();
  }
  public fileToUpload: File = null;
  handleFileInput(event) {
    this.fileToUpload = event.target.files[0];
    //alert(JSON.stringify(this.fileToUpload));
  }
}


