import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListVehicleComponent } from './list-vehicles/list-vehicle.component';
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component';
import {EditVehicleComponent} from './edit-vehicle/edit-vehicle.component';


export const vehiclesRoutes: Routes = [
  { path: '', component: ListVehicleComponent},
  { path: 'add', component: AddVehicleComponent},
  { path: 'edit/:id', component: EditVehicleComponent},

];
