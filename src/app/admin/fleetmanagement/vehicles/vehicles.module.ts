import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListVehicleComponent } from './list-vehicles/list-vehicle.component';
import { VehicleService } from '../../../common/services/vehicles/vehicle.service';
import { VehicleRestService } from '../../../common/services/vehicles/vehiclerest.service';
import { AddVehicleComponent } from './add-vehicle/add-vehicle.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { EditVehicleComponent } from './edit-vehicle/edit-vehicle.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EditDialogComponent } from './list-vehicles/dialog/edit-dialog/edit-dialog.component'
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatNativeDateModule } from '@angular/material';
import { MyDatePickerModule } from 'mydatepicker';
import { UploadDialogComponent } from './list-vehicles/dialog/upload-dialog/upload-dialog.component';
import { EditStatusDialogComponent } from './list-vehicles/dialog/edit-status-dialog/edit-status-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatDatepickerModule,
    MatInputModule,
    MatTooltipModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MyDatePickerModule
  ],
  declarations: [
    ListVehicleComponent,
    AddVehicleComponent,
    EditVehicleComponent,
    EditDialogComponent,
    UploadDialogComponent,
    EditStatusDialogComponent

  ],
  exports: [
    RouterModule,
    MatSelectModule
  ],
  providers: [VehicleService, VehicleRestService],
  entryComponents: [EditDialogComponent, UploadDialogComponent,EditStatusDialogComponent]
})
export class VehiclesModule { }
