import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { ToastsManager } from 'ng2-toastr';
import { Overlay } from '@angular/cdk/overlay';
import { environment } from '../../../../../environments/environment';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-list-vehicle-category-messages',
  templateUrl: './list-vehicle-category-messages.component.html',
  styleUrls: ['./list-vehicle-category-messages.component.css']
})
export class ListVehicleCategoryMessageComponent implements OnInit {
  public companyId: any = [];
  public CategoryData;
  public pageSize = 10;
  public pageNo = 0;
  public vehicleCategoryLength;
  public name;
  del = false;
  public is_search = false;
  searchValue;
  sortOrder = 'asc';
  public searchLoader = false;
  public keyword;
  key: string = '';
  public imageUrl;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  email: string;
  session: string;

  constructor(private router: Router,
    private _vehicleModelsService: VehicleModelsService,
    public overlay: Overlay,
    public dialog: MatDialog,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public _aclService: AccessControlService,
    public jwtService: JwtService,
    public encDecService: EncDecService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }

  ngOnInit() {
    this.aclDisplayService();
    this.is_search = false;
    this.imageUrl = environment.imgUrl;
    this.searchLoader = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._vehicleModelsService.getVehicleCategoriesMessages(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CategoryData = data.messageList;
        this.vehicleCategoryLength = data.totalCount;
      }
      this.searchLoader = false;
      this.del = false;
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        //console.log("Data"+JSON.stringify(data));
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Vehicle Models - Add") {
            this.aclAdd = true;
          } else if (data.menu[i] == "Vehicle Models - Edit") {
            this.aclEdit = true;
          } else if (data.menu[i] == "Vehicle Models - Delete") {
            this.aclDelete = true;
          }
        };
      }
    })
  }
  /**
   * Confirmation popup for deleting particular vehicle model record
   * @param id
   */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this data' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deleteVehicleCategoryMessage(id);
      } else {
      }
    });
  }

  /**
   * Delete vehicle model from view
   * @param id
   */
  public deleteVehicleCategoryMessage(id) {
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.updateVehicleCategoryMessageForDeletion(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status === 200) {
            this.refetchVehicleModel();
            this.CategoryData.splice(id, 1);
            this._toasterService.success('Message deleted successfully.');
          }
        }
      });
  }

  /**
  * To refresh vehicle model records showing in the grid
  */
  public refetchVehicleModel() {
    if (this.is_search == true) {
      this.searchLoader = true;
      this.del = true;
      this.searchLoader = false;
    }
    else {
      this.del = true;
      this.ngOnInit();
    }
  }

  /**
   * To reset the search filters and reload the vehicle model records
   */
  public reset() {
    this.name = '';
    this.ngOnInit();
  }

  /**
   * Pagination for vehicle model module
   * @param data
   */
  pagingAgent(data) {
    this.searchLoader = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategoriesMessages(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CategoryData = data.messageList;
        this.vehicleCategoryLength = data.totalCount;
      }
      this.searchLoader = false;
    });
  }
}
