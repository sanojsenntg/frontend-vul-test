import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { ListVehicleCategoryMessageComponent } from './list-vehicle-category-messages/list-vehicle-category-messages.component';
import { AddVehicleCategoryMessageComponent } from './add-vehicle-category-messages/add-vehicle-category-messages.component';
import { EditVehicleCategoryMessageComponent } from './edit-vehicle-category-messages/edit-vehicle-category.component-messages';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const vehicleCategoryMessageRoutes: Routes = [
  { path: '', component: ListVehicleCategoryMessageComponent, canActivate: [AclAuthervice], data: { roles: ["Vehicle Models - List"] } },
  { path: 'add', component: AddVehicleCategoryMessageComponent, canActivate: [AclAuthervice], data: { roles: ["Vehicle Models - Add"] } },
  { path: 'edit/:id', component: EditVehicleCategoryMessageComponent, canActivate: [AclAuthervice], data: { roles: ["Vehicle Models - Edit"] } }
];
