import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { ToasterService } from 'angular2-toaster';
import { TariffService } from '../../../../common/services/tariff/tariff.service';
import { PaymentTypeService } from '../../../../common/services/paymenttype/paymenttype.service';
import { Subscription } from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { environment } from '../../../../../environments/environment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-vehicle-category-messages',
  templateUrl: './edit-vehicle-category-messages.component.html',
  styleUrls: ['./edit-vehicle-category-messages.component.css']
})
export class EditVehicleCategoryMessageComponent implements OnInit {
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public imageUrl = '';
  public iconimageUrl = '';
  public spandata = 'yes';
  public spandataicon = 'yes';
  public tariffRecords;
  public paymentTypeData;
  public CategoryData;
  public companyId: any = [];
  email: string;
  session: string;
  constructor(private _vehicleModelsService: VehicleModelsService,
    private _paymentTypeService: PaymentTypeService,
    private _tariffService: TariffService,
    private route: ActivatedRoute,
    private router: Router,
    formBuilder: FormBuilder,
    public encDecService: EncDecService,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public jwtService: JwtService
  ) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      message: ['', [
        Validators.required,
      ]],
      category_id: ['', [
        Validators.required,
      ]],
    });
  }

  ngOnInit() {
    this.getCategories();
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    this.spandata = 'no';
    this.spandataicon = 'no';
    var params1 = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted1 = this.encDecService.nwt(this.session, params1);
    var enc_data1 = {
      data: encrypted1,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategoryMessageById(enc_data1).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        this.editForm.setValue({
          message: res.vehicleModel.message ? res.vehicleModel.message : '',
          category_id: res.vehicleModel.category_id ? res.vehicleModel.category_id : '',
        });
      }
    }).catch((error) => {
    });
  }
  /**
  * Update Vehicle model data
  *
  * @param formData
  */

  public updateVehicleCategory() {
    if (this.editForm) {
      const vehiclemodeldata = {
        name: this.editForm.value.message,
        /*number_of_person: this.editForm.value.number_of_person,*/
        category_id: this.editForm.value.category_id,
        company_id: this.companyId,
        _id: this._id
      };
      console.log(vehiclemodeldata);
      var encrypted = this.encDecService.nwt(this.session, vehiclemodeldata);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._vehicleModelsService.updateVehicleCategoryMessages(enc_data)
        .then((dec) => {
          console.log(dec);
          if (dec) {
            if (dec.status == 200) {
              this._toasterService.success('Message updated successfully.');
              this.router.navigate(['/admin/fleet/vehicle-category-messages']);
            } else if (dec.status == 201) {
              this._toasterService.error('Message updation failed.');
            }
          }
        })
        .catch((error) => {
        });
    } else {
      this._toasterService.error('All fields are required.');
      return;
    }
  }
  getCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CategoryData = data.vehicleModelsList;
      }
    });
  }
}