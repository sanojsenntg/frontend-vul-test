import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { TariffService } from '../../../../common/services/tariff/tariff.service';
import { PaymentTypeService } from '../../../../common/services/paymenttype/paymenttype.service';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-vehicle-category-messages',
  templateUrl: './add-vehicle-category-messages.component.html',
  styleUrls: ['./add-vehicle-category-messages.component.css']
})
export class AddVehicleCategoryMessageComponent implements OnInit {
  public companyId: any = [];
  public saveForm: FormGroup;
  public spandata = 'no';
  public available_for_status = '2';
  public spanicondata = 'no';
  public tariffRecords;
  public CategoryData;
  public pdata;
  public paymentTypeData;
  email: string;
  session: string;
  constructor(private _vehicleModelsService: VehicleModelsService,
    private _paymentTypeService: PaymentTypeService,
    private _tariffService: TariffService,
    public toastr: ToastsManager,
    private router: Router,
    formBuilder: FormBuilder,
    public encDecService: EncDecService,
    vcr: ViewContainerRef,
    public jwtService: JwtService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.saveForm = formBuilder.group({
      message: ['', [
        Validators.required,
      ]],
      category_id: ['', [
        Validators.required,
      ]]
    });
  }

  ngOnInit() {
    this.getCategories();
  }
  /**
   * Add vehicle model records
   */
  public addVehicleCategoryMessage() {
    if (!this.saveForm.valid) {
      this.toastr.error('All fields are required');
      return;
    }
    //console.log(this.saveForm);
    const vehicleModel = {
      message: this.saveForm.value.message,
      category_id: this.saveForm.value.category_id,
      company_id: this.companyId
    };
    //console.log("params" + JSON.stringify(vehicleModel));
    var encrypted = this.encDecService.nwt(this.session, vehicleModel);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.saveVehicleCategoryMessages(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            this.toastr.success('Message added successfully.');
            this.router.navigate(['/admin/fleet/vehicle-category-messages']);
          }
        }
      }).catch((error) => {
        this.toastr.error('Message addition failed.');
        //this.router.navigate(['/login']);
      });
  }

  getCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CategoryData = data.vehicleModelsList;
      }
    });
  }
}

