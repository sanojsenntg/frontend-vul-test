import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListServiceComponent } from './list-service/list-service.component';
import { EditServiceComponent } from './edit-service/edit-service.component';
import { AddServiceComponent } from './add-service/add-service.component';
import {AdditionalService} from '../../../common/services/additional_service/additional_service.service';
import {AdditionalrestService} from '../../../common/services/additional_service/additional_servicerest.service';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MatSelectModule,
    MatTooltipModule
  ],
  declarations: [
    ListServiceComponent,
     EditServiceComponent,
      AddServiceComponent
    ],
    exports: [
      RouterModule
    ],
    providers: [ AdditionalService, AdditionalrestService]

})

export class AdditionalServiceModule { }
