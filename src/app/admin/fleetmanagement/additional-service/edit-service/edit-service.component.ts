import { Component, OnInit } from '@angular/core';
import { AdditionalService } from '../../../../common/services/additional_service/additional_service.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-service',
  templateUrl: './edit-service.component.html',
  styleUrls: ['./edit-service.component.css']
})
export class EditServiceComponent implements OnInit {
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public companyId: any = [];
  session: string;
  email: string;

  constructor(private _additional_Service: AdditionalService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    private _toasterservice: ToastsManager) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      name: ['', [Validators.required]],
      company_id: ['', [Validators.required]],
      service_type: ['', [Validators.required]],
    });
  }
  public ngOnInit(): void {
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._additional_Service.getServiceById(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        res=res.SingleDetail;
        this.editForm.setValue({
          name: res.name ? res.name : '',
          company_id: res.company_id ? res.company_id : '',
          service_type: res.service_type ? res.service_type : '',
        });
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
  }

  /**
   *  To update the additional service records
   */
  public updateService() {
    const serviceData = {
      name: this.editForm.value.name ? this.editForm.value.name : null,
      company_id: this.companyId,
      service_type: this.editForm.value.service_type ? this.editForm.value.service_type : null,
      _id: this._id
    }
    if (!this.editForm.valid) {
      return;
    }
    else {
      var encrypted = this.encDecService.nwt(this.session, serviceData);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._additional_Service.updateServiceGroup(enc_data)
        .then((dec) => {
          if (dec) {
            if (dec.status === 200) {
              this._toasterservice.success('Additional Service Updated successfully.');
              this.router.navigate(['/admin/fleet/servicegroup']);
            } else if (dec.status === 201) {
              this._toasterservice.error('Additional Service updation failed.');
            } else if (dec.status === 203) {
              this._toasterservice.warning(' Service name already exist.');
            }
          }
        })
    }
  }
}