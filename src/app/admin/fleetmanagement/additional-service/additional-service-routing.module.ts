import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListServiceComponent } from './list-service/list-service.component';
import { EditServiceComponent } from './edit-service/edit-service.component';
import { AddServiceComponent } from './add-service/add-service.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const additionalServiceRoutes: Routes = [
  { path: '', component: ListServiceComponent, canActivate: [AclAuthervice], data: {roles: ["Additional Services - List"]}},
  { path: 'add', component: AddServiceComponent, canActivate: [AclAuthervice], data: {roles: ["Additional Services - Add"]}},
  { path: 'edit/:id', component: EditServiceComponent, canActivate: [AclAuthervice], data: {roles: ["Additional Services - Edit"]}}
];
