import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { AdditionalService } from '../../../../common/services/additional_service/additional_service.service';
import * as moment from 'moment/moment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.css']
})
export class AddServiceComponent {
  public companyId: any = [];
  public serviceGroup: any = {
    name: '',
    service_type: ''
  };
  email: string;
  session: string;

  constructor(private _toasterService: ToastsManager,
    formBuilder: FormBuilder,
    private router: Router,
    private _additional_Service: AdditionalService,
    vcr: ViewContainerRef,
    public encDecService: EncDecService,
    public jwtService: JwtService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }


  /**
   * Save Additional services data 
   * 
   */
  onSubmit(formData): void {
    if (!formData.valid) {
      // this._toasterService.error( 'All Fields are required');
    } else {
      this.serviceGroup['company_id'] = this.companyId;
      var encrypted = this.encDecService.nwt(this.session, this.serviceGroup);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._additional_Service.saveServiceGroup(enc_data)
        .then((dec) => {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          if (dec && dec.status == 200) {
            if (dec.status === 200) {
              this._toasterService.success('Additional Service added successfully.');
              this.router.navigate(['/admin/fleet/servicegroup']);
            } else if (dec.status === 201) {
              this._toasterService.error('Additional Service add failed.');
            } else if (dec.status === 203) {
              this._toasterService.warning(' Service name already exist.');
            }
          }
        })

    }
  }
}