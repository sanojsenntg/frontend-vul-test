import { Component, OnInit } from '@angular/core';
import * as moment from 'moment/moment';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { OrderService } from '../../../../common/services/order/order.service';
import { ActivatedRoute } from '@angular/router';
import { DriverService } from '../../../../common/services/driver/driver.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-driver-details',
  templateUrl: './driver-details.component.html',
  styleUrls: ['./driver-details.component.css']
})
export class DriverDetailsComponent implements OnInit {
  searchLoader: boolean;
  orderData: any = [];
  pageNo: number;
  selectedMoment1: any;
  selectedMoment: any;
  pageSize: any = 5;
  session: string;
  email: string;
  companyId: any = [];
  orderLength: any;
  driver_id: any;
  driverDetails: any = [];
  imageext: any;
  apiUrl: any;
  public max = new Date();
  ratingData: any[];
  ratingDataLength: any;
  searchLoader2: boolean;
  pageNo2: number;

  constructor(private encDecService: EncDecService, private _orderService: OrderService,
    private route: ActivatedRoute, private _driverService: DriverService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.imageext = window.localStorage['ImageExt'];
    this.apiUrl = environment.apiUrl;
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.driver_id = params['driver_id'];
      this.getOrders();
    });
    this.getDriver();
  }
  getDriver() {
    let params = {
      limit: 10,
      _id: this.driver_id,
      company_id: this.companyId
    }
    let enc_data = this.encDecService.nwt(this.session, params);
    let data1 = {
      data: enc_data,
      email: this.email
    }
    this._driverService.getDrivergroupsById(data1).then(result => {
      if (result && result.status === 200) {
        result = this.encDecService.dwt(this.session, result.data);
        this.driverDetails = result.getDriverss;
        //this.selectDriverData2 = result.driver;
        //this.loadingIndicator = "";
      }
    });
  }
  public getOrders() {
    this.searchLoader = true;
    this.orderData = [];
    this.pageNo = 1;
    let params;
    this.getTripRatings();
    params = {
      company_id: this.companyId,
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      //unique_order_id: this.unique_order_id ? this.unique_order_id : '',
      startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      driver_id: [this.driver_id],
      //generate_by: this.generated_by ? this.generated_by : '',
      //issues: this.issue_type
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getFlaggedTrips(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.orderData = data.data;
        this.orderLength = data.count;
        console.log(this.orderLength)
      }
      this.searchLoader = false;
    });
  }
  reset() {
    this.pageNo = 1;
    this.selectedMoment = '';
    this.selectedMoment1 = '';
    this.ngOnInit();
  }
  pagingAgent(data) {
    this.searchLoader = true;
    //this.orderData = [];
    //this.orderData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      company_id: this.companyId,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      //unique_order_id: this.unique_order_id ? this.unique_order_id : '',
      startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      driver_id: [this.driver_id],
      //generate_by: this.generated_by ? this.generated_by : '',
      //issues: this.issue_type
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getFlaggedTrips(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.orderData = data.data;
        this.orderLength = data.count;
      }
      this.searchLoader = false;
    });
  }
  
  public getTripRatings() {
    this.searchLoader2 = true;
    this.ratingData = [];
    this.pageNo2 = 1;
    let params;
    params = {
      company_id: this.companyId,
      offset: 0,
      limit: this.pageSize,
      sortOrder: -1,
      sortByColumn: 'updated_at',
      //unique_order_id: this.unique_order_id ? this.unique_order_id : '',
      startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      driver_id: this.driver_id,
      //generate_by: this.generated_by ? this.generated_by : '',
      //issues: this.issue_type
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getTripRating(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ratingData = data.data;
        this.ratingDataLength = data.count;
      }
      this.searchLoader2 = false;
    });
  }
  tripRatingPagingAgent(data) {
    this.searchLoader2 = true;
    //this.orderData = [];
    this.pageNo2 = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo2,
      limit: this.pageSize,
      company_id: this.companyId,
      sortOrder: -1,
      sortByColumn: 'updated_at',
      startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      driver_id: this.driver_id
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getTripRating(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ratingData = data.data;
        this.ratingDataLength = data.count;
      }
      this.searchLoader2 = false;
    });
  }
}
