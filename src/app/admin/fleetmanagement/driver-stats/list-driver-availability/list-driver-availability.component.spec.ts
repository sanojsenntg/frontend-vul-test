import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDriverAvailabilityComponent } from './list-driver-availability.component';

describe('ListDriverAvailabilityComponent', () => {
  let component: ListDriverAvailabilityComponent;
  let fixture: ComponentFixture<ListDriverAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDriverAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDriverAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
