import { Component, OnInit } from '@angular/core';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { DriverService } from '../../../../common/services/driver/driver.service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { Angular2Csv } from 'angular2-csv';
import * as moment from 'moment/moment';
import { FormControl } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { GlobalService } from '../../../../common/services/global/global.service';
import { MatDialog } from '@angular/material';
import { DriverShiftActivityComponent } from '../driver-shift-activity/driver-shift-activity.component';

@Component({
  selector: 'app-list-driver-stats',
  templateUrl: './list-driver-stats.component.html',
  styleUrls: ['./list-driver-stats.component.css']
})
export class ListDriverStatsComponent implements OnInit {
  session: string;
  email: string;
  companyId: any = [];
  company_id: string;
  dtc: boolean;
  searchLoader: boolean;
  pNo: number;
  pageSize = 10;
  sortByColumn = '';
  sortOrder = -1;
  DriverData: any;
  DriverLength: any;
  pageNo: number;
  rating = '';
  companyData: any;
  driver = [];
  alldriverData: any;
  companyIdFilter: any;
  imageext: any;
  apiUrl: any;
  constructor(public encDecService: EncDecService,
    public dialog: MatDialog,
    public _driverService: DriverService,
    private _companyservice: CompanyService,
    public _global: GlobalService) {
    this.company_id = localStorage.getItem('user_company');
    if (this.company_id === '5ce12918aca1bb08d73ca25d' || this.company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(this.company_id);
    this.imageext = window.localStorage['ImageExt'];
    this.apiUrl = environment.apiUrl;
  }
  queryFieldDriver: FormControl = new FormControl();

  ngOnInit() {
    if (this._global.driverStats) {
      this.sortOrder = this._global.driverStats.sortOrder;
      this.sortByColumn = this._global.driverStats.sortByColumn;
      this.rating = this._global.driverStats.startRating;
      this.driver = this._global.driverStats.username;
    }
    if (this.dtc)
      this.getCompanies();
    else
      this.getdriver();
    this.queryFieldDriver.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          offset: 0,
          limit: 10,
          search_keyword: query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        }
        let enc_data = this.encDecService.nwt(this.session, params);
        let data1 = {
          data: enc_data,
          email: this.email
        }
        return this._driverService.searchForDriver(data1)
      }).subscribe(result => {
        if (result && result.status === 200) {
          result = this.encDecService.dwt(this.session, result.data);
          this.alldriverData = result.driver;
        }
      })
  }
  public getdriver() {
    this.pNo = 1;
    this.searchLoader = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: this.sortOrder ? this.sortOrder : -1,
      sortByColumn: this.sortByColumn,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      startRating: this.rating ? this.rating : '',
      endRating: this.rating ? parseInt(this.rating) + 1 == 5 ? (6).toString() : (parseInt(this.rating) + 1).toString() : '',
      username: this.driver.length > 0 ? this.driver.map(x => x.username) : ''
    };
    this._global.driverStats = {
      sortOrder: this.sortOrder ? this.sortOrder : -1,
      sortByColumn: this.sortByColumn,
      startRating: this.rating ? this.rating : '',
      username: this.driver.length > 0 ? this.driver : []
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriverStats(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.DriverData = data.driver;
        this.DriverLength = data.count;
        console.log(this.DriverData)
      }
      this.searchLoader = false;
    });
  }
  pagingAgent(data) {
    this.searchLoader = true;
    this.DriverData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.sortOrder ? this.sortOrder : -1,
      sortByColumn: this.sortByColumn,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      startRating: this.rating ? this.rating : '',
      endRating: this.rating ? parseInt(this.rating) + 1 == 5 ? (6).toString() : (parseInt(this.rating) + 1).toString() : '',
      username: this.driver.length > 0 ? this.driver.map(x => x.username) : ''
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriverStats(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.DriverData = data.driver;
        this.DriverLength = data.count;
      }
      this.searchLoader = false;
    });
  }
  reset() {
    this.sortOrder = -1;
    this.sortByColumn = '';
    this.rating = '';
    this.driver = [];
    if (this.dtc) {
      this.companyId = this.companyData.slice().map(x => x._id);
      this.companyIdFilter = this.companyId.slice()
      this.companyIdFilter.push('all');
    }
    this.getdriver()
  }
  public getCompanies() {
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
          if (this.dtc && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
            this.companyId = data.getCompanies.map(x => x._id);
            this.companyIdFilter = data.getCompanies.map(x => x._id)
            this.companyIdFilter.push('all')
          }
          this.getdriver();
        }
      }
    });
  }
  public createCsv() {
    if (this.searchLoader)
      return;
    this.searchLoader = true;
    const params1 = {
      offset: 0,
      limit: this.DriverLength,
      sortOrder: this.sortOrder ? this.sortOrder : -1,
      sortByColumn: this.sortByColumn,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      startRating: this.rating ? this.rating : '',
      endRating: this.rating ? parseInt(this.rating) + 1 == 5 ? (6).toString() : (parseInt(this.rating) + 1).toString() : '',
      username: this.driver.length > 0 ? this.driver.map(x => x.username) : '',
    };
    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriverStats(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        let labels = [
          'ID',
          'Driver Type',
          'Real Time Status',
          'Status',
          'Rating',
          'Trips completed',
          'Acceptance rate'
        ];
        let resArray = [];
        for (let i = 0; i < res.driver.length; i++) {
          const csvArray = res.driver[i];
          resArray.push
            ({
              Id: csvArray.username ? csvArray.username : 'N/A',
              driver_type: csvArray.is_temperory && csvArray.is_temperory == "1" ? 'Freelance driver' : 'Company driver',
              real_time_status: csvArray.device_location && csvArray.device_location.length > 0 ? this.realTimeStatus(csvArray.device_location[0]) : 'Logged out',
              status: csvArray.status ? csvArray.status : 'N/A',
              rating: csvArray.avg_ratings ? csvArray.avg_ratings.$numberDecimal : 'N/A',
              trips_completed: csvArray.completed_trips ? csvArray.completed_trips : 'N/A',
              acceptance_rate: csvArray.acceptance_rate ? csvArray.acceptance_rate.$numberDecimal : 'N/A'
            });
        }
        var options =
        {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: false,
          useBom: true,
          headers: (labels)
        };
        new Angular2Csv(resArray, 'Driver-List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        this.searchLoader = false;
      }
      else {
        this.searchLoader = false;
      }
    });
  }
  searchDriver(data) {
    if (typeof data === "object") {
      if (this.driver.findIndex(x => x._id == data._id) == -1)
        this.driver.push(data);
    }
  }
  removeMultiSelect(data) {
    if (typeof data === "object") {
      let index = this.driver.findIndex(x => x._id == data._id)
      if (index !== -1)
        this.driver.splice(index, 1);
    }
  }
  displayFnModel(data): string {
    return "";
  }
  selectAllCompany() {
    if (this.companyId.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.companyId = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.companyId = [];
      this.companyIdFilter = [];
    }
  }
  changeCompany() {
    this.alldriverData = [];
  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.companyId = this.companyIdFilter.slice()
    }
    else {
      this.companyId = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }
  realTimeStatus(data) {
    data = data.vehicle;
    //  "vehicle_ids.vehicle_status": {$nin: ["M", "AC", "D"]}};
    if (data.occupied_status == '1' && data.online_status == '0' && data.in_maintenance == 'false' && data.vehicle_status !== 'M' && data.vehicle_status !== 'AC' && data.vehicle_status !== 'D')
      return 'Paused';
    else if (data.booked_status == '1' && data.occupied_status == '1' && data.online_status == '1' && data.in_maintenance == 'false' && data.vehicle_status !== 'M' && data.vehicle_status !== 'AC' && data.vehicle_status !== 'D')
      return 'Booked';
    else if (data.booked_status == '0' && data.occupied_status == '1' && data.online_status == '1' && data.in_maintenance == 'false' && data.vehicle_status !== 'M' && data.vehicle_status !== 'AC' && data.vehicle_status !== 'D')
      return 'Free';
    else
      return '';
  }
  getDriverShiftStats(driverId, username): void {
    this.dialog.open(DriverShiftActivityComponent, {
      width: '700px',
      hasBackdrop: true,
      data: { driver_id: driverId, username: username }
    });
  }
}
