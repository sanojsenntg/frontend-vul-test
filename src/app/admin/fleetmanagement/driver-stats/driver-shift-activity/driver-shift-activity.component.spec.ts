import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverShiftActivityComponent } from './driver-shift-activity.component';

describe('DriverShiftActivityComponent', () => {
  let component: DriverShiftActivityComponent;
  let fixture: ComponentFixture<DriverShiftActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverShiftActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverShiftActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
