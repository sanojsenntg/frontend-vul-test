import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../common/services/api/api.service';
import { environment } from '../../../environments/environment';
import { EncDecService } from '../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


const apiUrl = environment.apiUrl;
@Component({
  selector: 'app-acl-form',
  templateUrl: './acl-form.component.html',
  styleUrls: ['./acl-form.component.css']
})
export class AclFormComponent implements OnInit {
  companyId:any = [];
  public acl: any = {
    menu_name: '',
    site_url: '',
    level:'',
    is_main_menu:'',
    title:'',
    parent_id: ''
  };

  constructor(public apiService:ApiService,
    public encDecService:EncDecService) { 
    this.companyId.push(localStorage.getItem('user_company'))
  }

  ngOnInit() {
  }
  public onSubmit(data){
    console.log(this.acl);
    this.acl['company_id'] = this.companyId;
    var encrypted = this.encDecService.nwt(localStorage.getItem('Sessiontoken'),this.acl);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.apiService.post(apiUrl + '/access_control/addMenu', enc_data )
        .subscribe((res) => {
          console.log(res)
        });
  }
}
