import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../common/services/order/order.service';
import { VehicleService } from '../../common/services/vehicles/vehicle.service';
import * as moment from 'moment/moment';
import { filter } from 'rxjs/operator/filter';
import { JwtService } from '../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { UserIdleService } from 'angular-user-idle';
import { Router } from '@angular/router';
import * as Highcharts from 'highcharts';
import { EncDecService } from '../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../common/services/companies/companies.service';
import { AccessControlService } from '../../common/services/access-control/access-control.service';
import { Angular2Csv } from 'angular2-csv';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public showLoader;
  public orderData;
  public totalOrders = 0;
  public vehicleLogged = 0;
  public vehicleLoggedWithout = 0;
  public totalVehicles = 0;
  public fleetUtilization = 0;
  public session_token;
  public useremail;
  public company_details;
  public usercompany;
  public dataRefresher;
  public startDate;
  public endDate;
  public max = new Date();
  public selectedMoment;
  public orderLength;
  public loginUserData;
  public date;
  public filter_time: String = '1';
  public revenue_time: String = '1';
  public tripCountTime: String = '1';
  public fleetTime: String = '1';
  public loginTime: String = '1';
  public logoutTime: String = '1';
  public switchTime: String = '1';
  public cAppVehicles: String = '1';
  public order_detail_data;
  public interval;
  public subscripttimeout;
  public subscripttimestart;
  public split_up = 2;
  public revenueStart;
  public revenueEnd;
  public tripStart;
  public tripEnd;
  public loginStart;
  public loginEnd;
  public logoutStart;
  public logoutEnd;
  public fleetStart;
  public fleetEnd;

  Highcharts = Highcharts;
  Highcharts1 = Highcharts;
  fleet = Highcharts;
  login = Highcharts;
  logout = Highcharts;
  cAppHighChart = Highcharts;

  chartOptions = {
    title: {
      text: '',
      align: 'center',
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: {
        text: 'Values'
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      data: []
    }
    ],

  };
  chartOptions1 = {
    title: {
      text: '',
      align: 'center',
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: {
        text: 'Values'
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      data: []
    },
    ],

  };
  fleetOptions = {
    title: {
      text: '',
      align: 'center',
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: {
        text: 'Values'
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      data: []
    },
    ],
  };
  loginOptions = {
    title: {
      text: '',
      align: 'center',
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: {
        text: 'Values'
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      data: []
    },
    ],
  };
  logoutOptions = {
    title: {
      text: '',
      align: 'center',
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: {
        text: 'Values'
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      data: []
    },
    ],
  };
  cAppOptions = {
    title: {
      text: '',
      align: 'center',
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      title: {
        text: 'Values'
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      data: []
    },
    ],
  };
  type: string = '2';
  public hike: any = {};
  uaeTime: string | number | Date;
  public companyId = [];
  public companyData;
  public disabledCompany = false;
  public company_id_list: any = [localStorage.getItem('user_company')];
  customerStatsLoading: boolean;
  companyIdFilter: any;
  customerVehicleStats: any = { free: 0, freelance_in_trip: 0, dtc_in_trip: 0, total_booked: 0 };
  aclMenu = {
    overallStats: false,
    customerStats: false,
    customerVehicleStats: false,
    vehicleStats: false,
    graphs: false,
    downloads: false
  };
  createCsvFlag: boolean;
  constructor(private _vehicleService: VehicleService, private _EncDecService: EncDecService,
    private _orderService: OrderService, private _jwtService: JwtService, public toastr: ToastsManager, private userIdle: UserIdleService, private router: Router, private _companyservice: CompanyService, private _aclService: AccessControlService) {
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.subscripttimestart = this.userIdle.onTimerStart().subscribe(count => { });

    // Start watch when time is up.
    this.subscripttimeout = this.userIdle.onTimeout().subscribe(() => {
      clearInterval(this.interval);
      this.userIdle.stopWatching();
      this._jwtService.destroyDispatcherToken();
      this._jwtService.destroyAdminToken();
      this.router.navigate(['/admin/login']);
    });
  }
  ngOnInit() {
    this.companyId.push(window.localStorage['user_company']);
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.usercompany = window.localStorage['user_company'];
    this.company_details = JSON.parse(window.localStorage['companydata']);
    var company = window.localStorage['user_company'];
    if (company == '5ce12918aca1bb08d73ca25d' || company == '5cd982667a64fe51e5d3f7a0') {
      this.disabledCompany = true;
    }

    document.addEventListener("click", this.restart.bind(this));
    document.addEventListener("mousemove", this.restart.bind(this));
    document.addEventListener("mousedown", this.restart.bind(this));
    document.addEventListener("keypress", this.restart.bind(this));
    document.addEventListener("DOMMouseScroll", this.restart.bind(this));
    document.addEventListener("mousewheel", this.restart.bind(this));
    document.addEventListener("touchmove", this.restart.bind(this));
    document.addEventListener("MSPointerMove", this.restart.bind(this));
    this.startDate = this.getDate(moment().tz('Asia/Dubai').subtract(1, 'days').format("YYYY-MM-DD HH:mm"));
    this.endDate = this.getDate1(moment(new Date()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm'));
    this.type = '2';
    this.loginUserData = this._jwtService.getAdminUser();
    let that = this
    setInterval(function () {
      that.uaeTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Dubai" });
    }, 1000);
    if (this.disabledCompany)
      this.getCompanies()
    this.aclDisplayService();
  }
  stop() {
    this.userIdle.stopWatching();
  }
  restart() {
    this.userIdle.resetTimer();
  }
  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }
  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }
  refreshData() {
    this.interval = setInterval(() => {
      this.date_selected("all", "1");
      if (this.aclMenu.vehicleStats)
        this.getVehicleStatus();
      if (this.aclMenu.customerVehicleStats)
        this.getCustomerAppVehicleStatus();
      //Passing the false flag would prevent page reset to 1 and hinder user interaction
    }, 1000000);
  }
  ngOnDestroy() {
    this.subscripttimestart.unsubscribe();
    this.subscripttimeout.unsubscribe();
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
  date_selected(event, item) {
    this.switchTime = item;
    if (this.switchTime) {
      switch (this.switchTime) {
        case '1':
          // 24 hour
          this.type = '2';
          this.startDate = this.getDate(moment().tz('Asia/Dubai').subtract(1, 'days').format("YYYY-MM-DD HH:mm"));
          this.endDate = this.getDate1(moment(new Date()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm'));
          this.split_up = 2;
          break;
        case '2':
          // last 1 hour
          this.type = '1';
          this.startDate = this.getDate(moment().tz('Asia/Dubai').subtract(60, 'm').format("YYYY-MM-DD HH:mm"));
          this.endDate = this.getDate1(moment(new Date()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm'));
          this.split_up = 1
          break;
        case '4':
          // last week
          this.type = '3';
          this.startDate = this.getDate(moment().tz('Asia/Dubai').subtract(7, 'days').format("YYYY-MM-DD HH:mm"));
          this.endDate = this.getDate1(moment(new Date()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm'));
          this.split_up = 3;
          break;
        case '5':
          // last month
          this.type = '4';
          this.startDate = this.getDate(moment().tz('Asia/Dubai').subtract(30, 'days').format("YYYY-MM-DD HH:mm"));
          this.endDate = this.getDate1(moment(new Date()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm'));
          this.split_up = 4;

          break;
        default:
          this.type = '5';
          this.startDate = this.getDate(moment().tz('Asia/Dubai').subtract(1, 'days').format("YYYY-MM-DD HH:mm"));
          this.endDate = this.getDate1(moment(new Date()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm'));
          break;
      }
      if (event == "revenue") {
        if (this.aclMenu.graphs)
          this.dashChartRevenue();
      } else if (event == "trip") {
        if (this.aclMenu.graphs)
          this.dashChartTrip();
      } else if (event == "fleet") {
        if (this.aclMenu.graphs)
          this.fleetChart();
      } else if (event == "login") {
        if (this.aclMenu.graphs)
          this.loginChart();
      } else if (event == "logout") {
        if (this.aclMenu.graphs)
          this.logoutChart();
      } else if (event == "appVehicles") {
        if (this.aclMenu.graphs)
          this.cAppVehicleChart();
      }
    }
  }
  /**
   *  start time timepicker
   */
  checkShiftstartTime() {
    this.endDate = '';
  }
  public getCompanies() {
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.usercompany
    };
    var encrypted = this._EncDecService.nwt(this.session_token, param);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.companyData = data.getCompanies;
          if (this.disabledCompany && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
            this.companyId = data.getCompanies.map(x => x._id);
            this.companyIdFilter = data.getCompanies.map(x => x._id)
            this.companyIdFilter.push('all')
          }
        }
      }
    });
  }
  cAppStart;
  cAppEnd;
  public dateDetection(event) {
    if (event == "revenue") {
      this.startDate = this.revenueStart;
      this.endDate = this.revenueEnd;
      this.split_up = 4;
      this.dashChartRevenue();
    } else if (event == "trip") {
      this.startDate = this.tripStart;
      this.endDate = this.tripEnd;
      this.split_up = 4;
      if (this.aclMenu.graphs)
        this.dashChartTrip();
    } else if (event == "fleet") {
      this.startDate = this.fleetStart;
      this.endDate = this.fleetEnd;
      this.split_up = 4;
      if (this.aclMenu.graphs)
        this.fleetChart();
    } else if (event == "login") {
      this.startDate = this.loginStart;
      this.endDate = this.loginEnd;
      this.split_up = 4;
      if (this.aclMenu.graphs)
        this.loginChart();
    } else if (event == "logout") {
      this.startDate = this.logoutStart;
      this.endDate = this.logoutEnd;
      this.split_up = 4;
      if (this.aclMenu.graphs)
        this.logoutChart();
    } else if (event == "cAppVehicles") {
      this.startDate = this.cAppStart;
      this.endDate = this.cAppEnd;
      this.split_up = 4;
      if (this.aclMenu.graphs)
        this.cAppVehicleChart();
    }
  }
  /**
   *  end time timepicker
   */
  setDate() {
    /*const params = {
      offset: 0,
      start_date: this.startDate ?  moment(this.startDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.endDate ?  moment(this.endDate).format('YYYY-MM-DD HH:mm') : '',
    };
    this._orderService.searchOrder(params).then((data) => {
      });
      */
  }
  public order_driven;
  public shift_driven;
  public shift_duration;
  public total_salik;
  public total_revenue;
  public customerStats: any = {
    customer_order: 0,
    customer_cash: 0,
    customer_card: 0,
    avg_rating: 0,
    customer_eta: 0,
    customer_cancel: 0,
    order_timed_out: 0,
    driver_acceptance_rate: []
  };
  getOrdercounts() {
    if (this.company_id_list.length == 0) {
      this.company_id_list.push(localStorage.getItem('user_company'))
      this.companyId = this.company_id_list;
    }
    if (this.aclMenu.overallStats)
      this.getAllorderCount();
    if (this.aclMenu.customerStats)
      this.getAllorderCount2();
    if (this.aclMenu.vehicleStats)
      this.getVehicleStatus();
    if (this.aclMenu.customerVehicleStats)
      this.getCustomerAppVehicleStatus();
    // this.dashChartTrip();
    // this.dashChartRevenue();
    // this.fleetChart();
    // this.loginChart();
    // this.logoutChart();
  }
  getAllorderCount() {
    this.showLoader = true;
    var params = {
      start_date: this.startDate ? moment(this.startDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.endDate ? moment(this.endDate).format('YYYY-MM-DD HH:mm') : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany],
      type: this.type
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getAllOrderCount(encrypted).then((dec) => {
      this.showLoader = false;
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.countdata);
          data.countdata = data;
          this.totalOrders = data.countdata.total_orders;
          this.order_driven = data.countdata.order_distance;
          this.shift_driven = data.countdata.total_shift_km;
          this.shift_duration = data.countdata.total_shift_duration;
          this.total_salik = data.countdata.total_salik;
          this.total_revenue = data.countdata.total_revenue;
          // if (this.endDate !== '') {
          //   this.hike={};
          //   this.hike['totalOrders'] = (((data.countdata.total_orders / (data.countdata.pre_total_orders != 0 ? data.countdata.pre_total_orders : 1)) * 100) - 100).toFixed(2);
          //   this.hike['shift_driven'] = (((parseFloat(data.countdata.order_distance) / (parseFloat(data.countdata.pre_order_distance) != 0 ? parseFloat(data.countdata.pre_order_distance) : 1)) * 100) - 100).toFixed(2);
          //   this.hike['totalOrders'] = (((parseFloat(data.countdata.total_shift_km) / (parseFloat(data.countdata.pre_total_shift_km) != 0 ? parseFloat(data.countdata.pre_total_shift_km) : 1)) * 100) - 100).toFixed(2);
          //   this.hike['shift_duration'] = (((parseFloat(data.countdata.total_shift_duration) / (parseFloat(data.countdata.pre_total_shift_duration) != 0 ? parseFloat(data.countdata.pre_total_shift_duration) : 1)) * 100) - 100).toFixed(2);
          //   this.hike['total_salik'] = (((parseFloat(data.countdata.total_salik) / (parseFloat(data.countdata.pre_total_salik) != 0 ? parseFloat(data.countdata.pre_total_salik) : 1)) * 100) - 100).toFixed(2);
          //   this.hike['total_revenue'] = (((parseFloat(data.countdata.total_revenue) / (parseFloat(data.countdata.pre_total_revenue) != 0 ? parseFloat(data.countdata.pre_total_revenue) : 1)) * 100) - 100).toFixed(2);
          // } else {
          //   this.hike = undefined
          // }
        }
        else {
          this.totalOrders = 0;
          this.order_driven = 0;
          this.shift_driven = 0;
          this.shift_duration = 0;
          this.total_salik = 0;
          this.total_revenue = 0;
          this.hike = {
            totalOrders: 0,
            order_driven: 0,
            shift_driven: 0,
            shift_duration: 0,
            total_salik: 0,
            total_revenue: 0,
          }
        }
      }
    });

  }
  getAllorderCount2() {
    this.customerStatsLoading = true;
    var params = {
      start_date: this.startDate ? moment(this.startDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.endDate ? moment(this.endDate).format('YYYY-MM-DD HH:mm') : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany],
      type: this.type
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getAllOrderCount2(encrypted).then((dec) => {
      this.customerStatsLoading = false;
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.countdata);
          data.countdata = data;
          this.customerStats = data.countdata;
          // this.customerStats.driver_acceptance=0;
          if (this.endDate !== '') {
            this.hike = {};
            this.hike['customer_order'] = (((data.countdata.customer_order / (data.countdata.pre_customer_order != 0 ? data.countdata.pre_customer_order : 1)) * 100) - 100).toFixed(2);
            this.hike['customer_cash'] = (((data.countdata.customer_cash / (data.countdata.pre_customer_cash != 0 ? data.countdata.pre_customer_cash : 1)) * 100) - 100).toFixed(2);
            this.hike['customer_card'] = (((data.countdata.customer_card / (data.countdata.pre_customer_card != 0 ? data.countdata.pre_customer_card : 1)) * 100) - 100).toFixed(2);
            this.hike['avg_rating'] = (((data.countdata.avg_rating / (data.countdata.pre_avg_rating != 0 ? data.countdata.pre_avg_rating : 1)) * 100) - 100).toFixed(2);
            this.hike['customer_eta'] = (((parseFloat(data.countdata.customer_eta) / (parseFloat(data.countdata.pre_customer_eta) != 0 ? parseFloat(data.countdata.pre_customer_eta) : 1)) * 100) - 100).toFixed(2);
            this.hike['customer_cancel'] = (((parseFloat(data.countdata.customer_cancel) / (parseFloat(data.countdata.pre_customer_cancel) != 0 ? parseFloat(data.countdata.pre_customer_cancel) : 1)) * 100) - 100).toFixed(2);
            this.hike['order_timed_out'] = (((data.countdata.order_timed_out / (data.countdata.pre_order_timed_out != 0 ? data.countdata.pre_order_timed_out : 1)) * 100) - 100).toFixed(2);
            this.hike['customer_actual_eta'] = (((parseFloat(data.countdata.customer_actual_eta) / (parseFloat(data.countdata.pre_customer_actual_eta) != 0 ? parseFloat(data.countdata.pre_customer_actual_eta) : 1)) * 100) - 100).toFixed(2);
            this.hike['driver_acceptance_rate'] = data.countdata.driver_acceptance_rate;
            for (let i = 0; i < this.customerStats.driver_acceptance_rate.length; i++) {
              let index = this.customerStats.pre_driver_acceptance_rate.findIndex(x => x._id == this.customerStats.driver_acceptance_rate[i]._id);
              if (index > -1)
                this.customerStats.driver_acceptance_rate[i].hike = (((parseFloat(this.customerStats.driver_acceptance_rate[i].driver_acceptance) / (parseFloat(this.customerStats.pre_driver_acceptance_rate[index].driver_acceptance) != 0 ? parseFloat(this.customerStats.pre_driver_acceptance_rate[index].driver_acceptance) : 1)) * 100) - 100).toFixed(2)//this.customerStats.pre_driver_acceptance_rate[index].driver_acceptance;
              //this.customerStats.driver_acceptance_rate.hike=this.customerStats.pre_driver_acceptance_rate[index].driver_acceptance;
              else
                this.customerStats.driver_acceptance_rate[i].hike = 0;
            }
            // this.hike['driver_acceptance']=0;
            // this.customerStats.pre_driver_acceptance_rate.forEach(element => {
            //   this.hike['driver_acceptance']+=parseFloat(element.driver_acceptance);
            // });
          } else {
            this.hike = undefined
          }
        }
        else {
          this.customerStats = {
            customer_order: 0,
            customer_cash: 0,
            customer_card: 0,
            avg_rating: 0,
            customer_eta: 0,
            customer_cancel: 0,
            order_timed_out: 0,
            customer_actual_eta: 0,
            driver_acceptance_rate: []
          };
          this.hike = {
            customer_order: 0,
            customer_cash: 0,
            customer_card: 0,
            avg_rating: 0,
            customer_eta: 0,
            customer_cancel: 0,
            order_timed_out: 0,
            customer_actual_eta: 0,
            driver_acceptance_rate: []
          }
        }
      }
    });
  }
  public getVehicleStatus() {
    let params = {
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    this._vehicleService.getVehiclesCountforDashboard(encrypted).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var vehicledata: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.vehicleLogged = vehicledata.vehicles.logged_vehicle_count;
          this.vehicleLoggedWithout = vehicledata.vehicles.offline_count;
          this.totalVehicles = vehicledata.vehicles.total_vehicle;
          this.fleetUtilization = (this.vehicleLogged / (this.vehicleLogged + this.vehicleLoggedWithout)) * 100;
        }
      }
    });
  }
  getCustomerAppVehicleStatus() {
    var params = {
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
    }
    let enc_data = this._EncDecService.nwt(this.session_token, params);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    this.customerVehicleStats.free = 0;
    this.customerVehicleStats.freelance_in_trip = 0;
    this.customerVehicleStats.dtc_in_trip = 0;
    this.customerVehicleStats.total_booked = 0;
    this._orderService.getCustomerAppVehicleStats(encrypted).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          this.customerVehicleStats.free = data.data.free_vehicles;
          for (let element of data.data.in_trip_vehicles) {
            if (element._id == "1")
              this.customerVehicleStats.freelance_in_trip = element.count;
            else
              this.customerVehicleStats.dtc_in_trip = element.count;
          }
          this.customerVehicleStats.total_booked = parseInt(this.customerVehicleStats.dtc_in_trip) + parseInt(this.customerVehicleStats.freelance_in_trip) + parseInt(this.customerVehicleStats.free);
        }
      }
    });
  }
  public dashChartTrip() {
    var dashparms = {
      start_date: this.startDate ? moment(this.startDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.endDate ? moment(this.endDate).format('YYYY-MM-DD HH:mm') : '',
      split_up: this.split_up,
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
    }
    let enc_data = this._EncDecService.nwt(this.session_token, dashparms);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getDashChart(encrypted).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          let aggrDate = [];
          let aggcount = [];
          for (let i = 0; i < data.dateCategory.length; i++) {
            var flag = 0;
            for (let j = 0; j < data.aggregateData.length; j++) {
              if (data.aggregateData[j]._id == data.dateCategory[i]) {
                flag = 1
                aggrDate.push(data.aggregateData[j]._id);
                aggcount.push(parseInt(data.aggregateData[j].count));
              }
            }
            if (flag == 0) {
              aggrDate.push(data.dateCategory[i]);
              aggcount.push(0);
            }
          }
          aggrDate.pop();
          aggcount.pop();
          this.showLoader = false;
          this.chartOptions1 = {
            title: {
              text: '',
              align: 'center',
            },
            xAxis: {
              categories: aggrDate
            },
            yAxis: {
              title: {
                text: 'Count'
              }
            },
            credits: {
              enabled: false
            },
            series: [{
              name: 'Trip',
              data: aggcount
            },
            ]
          };
        }
      }
    });
  }

  public dashChartRevenue() {
    var dashparms = {
      start_date: this.startDate ? moment(this.startDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.endDate ? moment(this.endDate).format('YYYY-MM-DD HH:mm') : '',
      split_up: this.split_up,
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
    }
    let enc_data = this._EncDecService.nwt(this.session_token, dashparms);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOfflineChart(encrypted).then((data) => {
    })
    this._orderService.getDashChart(encrypted).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          let aggrDate = [];
          let aggvalue = [];
          for (let i = 0; i < data.dateCategory.length; i++) {
            var flag = 0;
            for (let j = 0; j < data.aggregateData.length; j++) {
              if (data.aggregateData[j]._id == data.dateCategory[i]) {
                flag = 1;
                aggrDate.push(data.aggregateData[j]._id);
                aggvalue.push(parseFloat(data.aggregateData[j].total_cost.$numberDecimal));
              }
            }
            if (flag == 0) {
              aggrDate.push(data.dateCategory[i]);
              aggvalue.push(0);
            }
          }
          this.showLoader = false;
          aggrDate.pop();
          aggvalue.pop();
          this.chartOptions = {
            title: {
              text: '',
              align: 'center',
            },
            xAxis: {
              categories: aggrDate
            },
            yAxis: {
              title: {
                text: 'Amt'
              }
            },
            credits: {
              enabled: false
            },
            series: [
              {
                name: 'Revenue',
                data: aggvalue
              }
            ]
          };
        }
      }
    });
  }

  public fleetChart() {
    var dashparms = {
      start_date: this.startDate ? moment(this.startDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.endDate ? moment(this.endDate).format('YYYY-MM-DD HH:mm') : '',
      split_up: this.split_up,
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
    }
    let enc_data = this._EncDecService.nwt(this.session_token, dashparms);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOfflineChart(encrypted).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          let aggrDate = [];
          let aggvalue = [];
          for (let i = 0; i < data.dateCategory.length; i++) {
            var flag = 0;
            for (let j = 0; j < data.aggregateData.length; j++) {
              if (data.aggregateData[j]._id == data.dateCategory[i]) {
                flag = 1;
                aggrDate.push(data.aggregateData[j]._id);
                aggvalue.push(parseFloat(data.aggregateData[j].fleetpercent));
              }
            }
            if (flag == 0) {
              aggrDate.push(data.dateCategory[i]);
              aggvalue.push(0);
            }
          }
          aggrDate.pop();
          aggvalue.pop();
          this.showLoader = false;
          this.fleetOptions = {
            title: {
              text: '',
              align: 'center',
            },
            xAxis: {
              categories: aggrDate
            },
            yAxis: {
              title: {
                text: '%'
              }
            },
            credits: {
              enabled: false
            },
            series: [
              {
                name: 'Fleet',
                data: aggvalue
              }
            ]
          };
        }
      }
    });
  }

  public loginChart() {
    var dashparms = {
      start_date: this.startDate ? moment(this.startDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.endDate ? moment(this.endDate).format('YYYY-MM-DD HH:mm') : '',
      split_up: this.split_up,
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
    }
    let enc_data = this._EncDecService.nwt(this.session_token, dashparms);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOfflineChart(encrypted).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          let aggrDate = [];
          let aggvalue = [];
          for (let i = 0; i < data.dateCategory.length; i++) {
            var flag = 0;
            for (let j = 0; j < data.aggregateData.length; j++) {
              if (data.aggregateData[j]._id == data.dateCategory[i]) {
                flag = 1;
                aggrDate.push(data.aggregateData[j]._id);
                aggvalue.push(parseInt(data.aggregateData[j].login));
              }
            }
            if (flag == 0) {
              aggrDate.push(data.dateCategory[i]);
              aggvalue.push(0);
            }
          }
          aggrDate.pop();
          aggvalue.pop();
          this.showLoader = false;
          this.loginOptions = {
            title: {
              text: '',
              align: 'center',
            },
            xAxis: {
              categories: aggrDate
            },
            yAxis: {
              title: {
                text: 'Count'
              }
            },
            credits: {
              enabled: false
            },
            series: [
              {
                name: 'Login',
                data: aggvalue
              }
            ]
          };
        }
      }
    });
  }

  public logoutChart() {
    var dashparms = {
      start_date: this.startDate ? moment(this.startDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.endDate ? moment(this.endDate).format('YYYY-MM-DD HH:mm') : '',
      split_up: this.split_up,
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
    }
    let enc_data = this._EncDecService.nwt(this.session_token, dashparms);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    this._orderService.getOfflineChart(encrypted).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          let aggrDate = [];
          let aggvalue = [];
          for (let i = 0; i < data.dateCategory.length; i++) {
            var flag = 0;
            for (let j = 0; j < data.aggregateData.length; j++) {
              if (data.aggregateData[j]._id == data.dateCategory[i]) {
                flag = 1;
                aggrDate.push(data.aggregateData[j]._id);
                aggvalue.push(parseInt(data.aggregateData[j].logout));
              }
            }
            if (flag == 0) {
              aggrDate.push(data.dateCategory[i]);
              aggvalue.push(0);
            }
          }
          aggrDate.pop();
          aggvalue.pop();
          this.showLoader = false;
          this.logoutOptions = {
            title: {
              text: '',
              align: 'center',
            },
            xAxis: {
              categories: aggrDate
            },
            yAxis: {
              title: {
                text: 'Count'
              }
            },
            credits: {
              enabled: false
            },
            series: [
              {
                name: 'Logout',
                data: aggvalue
              }
            ]
          };
        }
      }
    });
  }
  public cAppVehicleChart() {
    var dashparms = {
      start_date: this.startDate ? moment(this.startDate).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.endDate ? moment(this.endDate).format('YYYY-MM-DD HH:mm') : '',
      split_up: this.split_up,
      company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
    }
    let enc_data = this._EncDecService.nwt(this.session_token, dashparms);
    let encrypted = {
      data: enc_data,
      email: this.useremail
    }
    console.log(dashparms);
    this._orderService.getappVehicleChart(encrypted).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this._EncDecService.dwt(this.session_token, dec.data);
          console.log(data);
          let aggrDate = [];
          let aggvalue = [];
          for (let i = 0; i < data.dateCategory.length; i++) {
            var flag = 0;
            for (let j = 0; j < data.aggregateData.length; j++) {
              if (data.aggregateData[j]._id == data.dateCategory[i]) {
                flag = 1;
                aggrDate.push(data.aggregateData[j]._id);
                aggvalue.push(parseInt(data.aggregateData[j].login));
              }
            }
            if (flag == 0) {
              aggrDate.push(data.dateCategory[i]);
              aggvalue.push(0);
            }
          }
          aggrDate.pop();
          aggvalue.pop();
          this.showLoader = false;
          this.cAppOptions = {
            title: {
              text: '',
              align: 'center',
            },
            xAxis: {
              categories: aggrDate
            },
            yAxis: {
              title: {
                text: 'Count'
              }
            },
            credits: {
              enabled: false
            },
            series: [
              {
                name: 'Customer App vehicles',
                data: aggvalue
              }
            ]
          };
          console.log(this.cAppOptions);
        }
      }
    });
  }
  convertToMinutes(minutes) {
    if (minutes > 0) {
      let time = minutes.split(".");
      let decimal = parseInt(time[1])
      if (decimal > 0)
        if ((decimal / 100) * 60 < 10)
          return time[0] + '.0' + ((decimal / 100) * 60).toFixed(0);
        else
          return time[0] + '.' + ((decimal / 100) * 60).toFixed(0);
      else
        return minutes;
    }
    else
      return minutes;
  }
  public getClass(a) {
    return parseInt(a) >= 0 ? 'green' : 'red';
  }
  public getImage(i) {
    return parseInt(i) >= 0 ? 'assets/img/stats_icons/graph_up.png' : 'assets/img/stats_icons/graph_down.png';
  }
  public getIcon(i) {
    return parseInt(i) >= 0 ? 'fa-caret-up' : 'fa-caret-down';
  }
  public pastTime() {
    let date1 = new Date(this.startDate).getTime();
    let date2 = new Date(this.endDate).getTime();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";

    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      let postFix = Math.round(var1) == 1 ? ' second' : ' seconds';
      output += Math.round(var1) + postFix;
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      let postFix = Math.round(var1) == 1 ? ' minute' : ' minutes';
      output += Math.round(var1) + postFix;
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      let postFix = Math.round(var1) == 1 ? ' hour' : ' hours';
      output += Math.round(var1) + postFix;
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      let postFix = Math.round(var1) == 1 ? ' day' : ' days';
      output += Math.round(var1) + postFix;
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      let postFix = Math.round(var1) == 1 ? ' month' : ' months';
      output += Math.round(var1) + postFix;
    }
    return hoursDiff < 0 ? '0 secs' : output;
  }
  target(a, b) {//calculates the percentage of a in b
    if (a == 0)
      a = 1;
    if (b == 0)
      b = 1;
    return Math.floor(((a / b) * 100)) >= 100 ? 100 : Math.floor(((a / b) * 100));
  }
  targetColor(value, flag) {
    if (flag)
      return value < 100 ? '#FF0000' : '#00FF00';
    return value > 5 ? '#FF0000' : '#00FF00';
  }
  selectAllCompany() {
    if (this.companyId.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.companyId = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.companyId = [];
      this.companyIdFilter = [];
    }
  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.companyId = this.companyIdFilter.slice()
    }
    else {
      this.companyId = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }
  public aclDisplayService() {
    let data = JSON.parse(localStorage.getItem('userMenu'));
    for (let i = 0; i < data.length; i++) {
      if (data[i] == "Customer app stats") {
        this.aclMenu.customerStats = true;
      } else if (data[i] == "Customer app vehicle stats") {
        this.aclMenu.customerVehicleStats = true;
      } else if (data[i] == "Graphs") {
        this.aclMenu.graphs = true;
      } else if (data[i] == "Overall stats") {
        this.aclMenu.overallStats = true;
      } else if (data[i] == "Vehicle stats") {
        this.aclMenu.vehicleStats = true;
      }
      if (data[i] == "Dashboard - Downloads")
        this.aclMenu.downloads = true
    };
    if (this.aclMenu.overallStats)
      this.getAllorderCount();
    if (this.aclMenu.customerStats)
      this.getAllorderCount2();
    if (this.aclMenu.customerVehicleStats)
      this.getCustomerAppVehicleStatus();
    if (this.aclMenu.vehicleStats)
      this.getVehicleStatus();
    if (this.aclMenu.graphs) {
      this.dashChartTrip();
      this.dashChartRevenue();
      this.fleetChart();
      this.loginChart();
      this.logoutChart();
      this.cAppVehicleChart();
    }
  }

  public createCsv(id) {
    if (this.createCsvFlag) {
      this.toastr.warning('CSV creation already started');
      return;
    }
    let title = 'Vehicle-List';
    let params;
    switch (id) {
      case 1://Free vehicles
        params = {
          company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
        }
        break;
      case 5://Logged In
        title = 'Logged-in-vehicle';
        params = {
          keyword: 3,
          offset: 0,
          limit: 0,
          sortOrder: 'desc',
          sortByColumn: '_id',
          company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
        }
        break;
      case 6://Logged out
        title = 'Logged-out-vehicle';
        params = {
          keyword: 2,
          offset: 0,
          limit: 0,
          sortOrder: 'desc',
          sortByColumn: '_id',
          company_id: this.companyId.length > 0 ? this.companyId : [this.usercompany]
        }
        break;
      default:
        if (this.customerStatsLoading) {
          this.toastr.warning('Stats is loading please try again');
          return;
        }
        let labels = [
          'Stats (' + moment().tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') + ')',
          'Current value',
          this.hike ? 'Variation (Percentage) (past ' + this.pastTime() + ')' : ''
        ];
        console.log(this.hike)
        let resArray = [
          { Status: 'Total Completed Trips', C_value: this.customerStats.customer_order, Variation: this.hike ? this.hike.customer_order : '' },
          { Status: 'Paid through cash', C_value: this.customerStats.customer_cash + ' AED', Variation: this.hike ? this.hike.customer_cash : '' },
          { Status: 'Paid through online', C_value: this.customerStats.customer_card + ' AED', Variation: this.hike ? this.hike.customer_card : '' },
          { Status: 'Average Rating', C_value: this.customerStats.avg_rating, Variation: this.hike ? this.hike.avg_rating : '' },
          { Status: 'Average system ETA', C_value: this.convertToMinutes(this.customerStats.customer_eta) + ' Minutes', Variation: this.hike ? this.hike.customer_eta : '' },
          { Status: 'Average actual ETA', C_value: this.convertToMinutes(this.customerStats.customer_actual_eta) + ' Minutes', Variation: this.hike ? this.hike.customer_actual_eta : '' },
          { Status: 'Order Cancelled', C_value: this.customerStats.customer_cancel + ' %', Variation: this.hike ? this.hike.customer_cancel : '' },
          { Status: 'Order timed out', C_value: this.customerStats.order_timed_out + ' %', Variation: this.hike ? this.hike.order_timed_out : '' }
        ];
        this.customerStats.driver_acceptance_rate.forEach(element => {
          resArray.push({ Status: 'Driver acceptance rate (' + element._id + ')', C_value: parseFloat(element.driver_acceptance).toFixed(2) + ' %', Variation: element.hike ? parseFloat(element.hike).toFixed(2) : '' })
        });
        var options =
        {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: false,
          useBom: true,
          headers: (labels)
        };
        new Angular2Csv(resArray, 'Customer-stats' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        return;
    }
    this.createCsvFlag = true;
    var encrypted = this._EncDecService.nwt(this.session_token, params);
    var enc_data = {
      data: encrypted,
      email: this.useremail
    }
    let that = this;
    var setData = function (res, title) {
      let labels = [
        'Registration No',
        'License No',
        'Display Name',
        'Vehicle Model',
        'Insurance Expiry Date',
        'Plate Number',
        'Plate Type',
        'Vehicle Identity Number',
        'Register With WASL',
        'Send To RTA',
        'Maintenance Status',
        'Created At',
        'Credit Card Machine',
        'Vehicle status'
      ];
      let resArray = [];
      for (let i = 0; i < res.length; i++) {
        const csvArray = res[i];
        resArray.push
          ({
            registration_number: csvArray.registration_number ? csvArray.registration_number : 'N/A',
            license_number: csvArray.license_number ? csvArray.license_number : 'N/A',
            display_name: csvArray.display_name ? csvArray.display_name : 'N/A',
            vehicle_model: csvArray.vehicle_model && csvArray.vehicle_model.name ? csvArray.vehicle_model.name : 'N/A',
            insuarance_expiration_date: csvArray.insuarance_expiration_date ? csvArray.insuarance_expiration_date : 'N/A ',
            plate_number: csvArray.plate_number ? csvArray.plate_number : 'N/A',
            plate_type: csvArray.plate_type ? csvArray.plate_type : 'N/A',
            vehicle_identity_number: csvArray.vehicle_identity_number ? csvArray.vehicle_identity_number : 'N/A',
            register_with_wasl: csvArray.register_with_wasl ? csvArray.register_with_wasl : 'N/A',
            send_to_rta: csvArray.send_to_rta ? csvArray.send_to_rta : 'N/A',
            maintenance_status: csvArray.maintenance_status ? csvArray.maintenance_status : 'N/A',
            created_at: csvArray.created_at ? csvArray.created_at : 'N/A ',
            credit_card: csvArray.credit_card ? csvArray.credit_card : 'Not Added',
            vehicle_status: csvArray.vehicle_status ? csvArray.vehicle_status : 'N/A'
          });
      }
      var options =
      {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: false,
        useBom: true,
        headers: (labels)
      };
      new Angular2Csv(resArray, title + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
    }
    if (id != 1)
      this._vehicleService.getStatusBasedVehicles(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var res: any = that._EncDecService.dwt(that.session_token, dec.data);
          setData(res.vehicles, title);
        }
        that.createCsvFlag = false;
      })
    else
      this._vehicleService.customerAppVehicleList(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var res: any = that._EncDecService.dwt(that.session_token, dec.data);
          setData(res.freeVehicles, 'Customer-App-Free-vehicles');
          for (let element of res.in_trip_vehicles) {
            if (element._id == "1")
              setData(element.data, 'Customer-App-Freelance-booked-vehicles');
            else
              setData(element.data, 'Customer-App-DTC-booked-vehicles');
          }
        }
        that.createCsvFlag = false;
      })
  }
}
