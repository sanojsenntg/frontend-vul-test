import { Component, OnInit } from '@angular/core';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-performance-monitoring',
  templateUrl: './performance-monitoring.component.html',
  styleUrls: ['./performance-monitoring.component.css']
})
export class PerformanceMonitoringComponent implements OnInit {

  public aclCheck;
  constructor(public _aclService: AccessControlService, private route: ActivatedRoute,
    public encDecService: EncDecService,
    private router: Router) {
    this.aclDisplayService();
    this.aclDisplayService();
  }

  ngOnInit() {
  }
  public aclDisplayService() {
    this.aclCheck = JSON.parse(localStorage.getItem('userMenu'));
    // const company_id: any = localStorage.getItem('user_company');
    // const session: any = localStorage.getItem('Sessiontoken');
    // const email: any = localStorage.getItem('user_email');
    // var params = {
    //   company_id: [company_id]
    // }
    // var encrypted = this.encDecService.nwt(session, params);
    // var enc_data = {
    //   data: encrypted,
    //   email: email
    // }
    // this._aclService.getAclUserMenu(enc_data).then((dec) => {
    //   if (dec && dec.status == 200) {
    //     var data: any = this.encDecService.dwt(session, dec.data);
    //     this.aclCheck = data.menu;
    //   }
    // })

  }

}
