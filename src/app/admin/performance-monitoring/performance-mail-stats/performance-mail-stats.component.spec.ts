import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerformanceMailStatsComponent } from './performance-mail-stats.component';

describe('PerformanceMailStatsComponent', () => {
  let component: PerformanceMailStatsComponent;
  let fixture: ComponentFixture<PerformanceMailStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerformanceMailStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerformanceMailStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
