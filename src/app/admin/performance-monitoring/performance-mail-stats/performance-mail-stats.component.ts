import { Component, OnInit } from '@angular/core';
import * as moment from 'moment/moment';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { OrderService } from '../../../common/services/order/order.service';
import { ToastsManager } from 'ng2-toastr';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
  selector: 'app-performance-mail-stats',
  templateUrl: './performance-mail-stats.component.html',
  styleUrls: ['./performance-mail-stats.component.css']
})
export class PerformanceMailStatsComponent implements OnInit {
  searchSubmit: boolean;
  companyId: any = [];
  email: string;
  session: string;
  public startDate;
  public endDate;
  public max = new Date();
  performanceStats: any = [];
  createCsvFlag: boolean;
  october = '2019-10-01';
  constructor(
    private _orderService: OrderService,
    public encDecService: EncDecService,
    public toastr: ToastsManager) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }
  dateFilter = (d: Date): boolean => {
    const minutes = d.getMinutes();
    return minutes % 20 == 0;
  }

  ngOnInit() {
    var coeff = 1000 * 60 * 20;
    var date = new Date();
    this.endDate = new Date(Math.round(date.getTime() / coeff) * coeff);
    this.startDate = moment().startOf('day').format();
    this.getEmailStats()
  }
  getEmailStats() {
    this.searchSubmit = true;
    const params = {
      start_date: this.startDate ? moment(this.startDate).format('YYYY-MM-DD HH:mm') : '2019-10-01 00:00',
      end_date: this.endDate ? moment(this.endDate).format('YYYY-MM-DD HH:mm') : '2019-10-10 00:00',
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getMailStats(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.performanceStats.completed_trips = 0;
        this.performanceStats.customer_cancelled = 0;
        this.performanceStats.dispatcher_cancelled = 0;
        this.performanceStats.order_timed_out = 0;
        this.performanceStats.rejected_by_driver = 0;
        this.performanceStats.driver_cancelled = 0;

        this.performanceStats.dtc_driver_acceptance_rate = []

        this.performanceStats.customer_cancel_count = 0;
        this.performanceStats.system_eta_count = 0;
        this.performanceStats.order_rejected_drivers_count = 0;
        this.performanceStats.order_timed_out_count = 0;
        this.performanceStats.low_rating_count = 0;
        this.performanceStats.timedout_drivers_count = 0;
        this.performanceStats.scheduled_case_count = 0;

        this.performanceStats.customer_cancel_data = [];
        this.performanceStats.system_eta_data = [];
        this.performanceStats.order_rejected_drivers_data = [];
        this.performanceStats.order_timed_out_data = [];
        this.performanceStats.low_rating_data = [];
        this.performanceStats.timedout_drivers_data = [];
        this.performanceStats.scheduled_case_data = [];

        this.performanceStats.freelance_revenue = 0;

        this.performanceStats.temp_completed_trips = 0;
        this.performanceStats.temp_driver_cancelled = 0;
        this.performanceStats.temp_customer_cancelled = 0;
        this.performanceStats.temp_dispatcher_cancelled = 0;
        this.performanceStats.temp_order_timed_out = 0;
        this.performanceStats.temp_rejected_by_driver = 0;
        this.performanceStats.temp_driver_cancelled = 0;

        this.performanceStats.temp_dtc_driver_acceptance_rate = [];

        let avg_rating = 0;
        let temp_avg_rating = 0;
        this._orderService.getMailPauseStats(enc_data).then((dec2) => {
          console.log(dec2);
          if (dec2 && dec2.status == 200) {
            var data2: any = this.encDecService.dwt(this.session, dec2.data);
            console.log(data2);
            this.performanceStats.pausing_meter = data2.data;
            data.data.forEach(element => {
              this.performanceStats.completed_trips += element.completed_trips;
              this.performanceStats.customer_cancelled += element.customer_cancelled;
              this.performanceStats.dispatcher_cancelled += element.dispatcher_cancelled;
              this.performanceStats.order_timed_out += element.order_timed_out;
              this.performanceStats.rejected_by_driver += element.rejected_by_driver;
              this.performanceStats.driver_cancelled += element.driver_cancelled;
              element.dtc_driver_acceptance_rate.forEach(element2 => {
                if (this.performanceStats.dtc_driver_acceptance_rate[element2._id])
                  this.performanceStats.dtc_driver_acceptance_rate[element2._id] += element2.driver_acceptance;
                else
                  this.performanceStats.dtc_driver_acceptance_rate[element2._id] = 10;
              });

              this.performanceStats.customer_cancel_count += element.customer_cancel_count;
              this.performanceStats.system_eta_count += element.system_eta_count;
              this.performanceStats.order_rejected_drivers_count += element.order_rejected_drivers_count;
              this.performanceStats.order_timed_out_count += element.order_timed_out_count;
              this.performanceStats.low_rating_count += element.low_rating_count;
              this.performanceStats.timedout_drivers_count += element.timedout_drivers_count;
              this.performanceStats.scheduled_case_count += element.scheduled_case_count;

              this.performanceStats.customer_cancel_data = this.performanceStats.customer_cancel_data.concat(element.customer_cancel_data);
              this.performanceStats.system_eta_data = this.performanceStats.system_eta_data.concat(element.system_eta_data);
              this.performanceStats.order_rejected_drivers_data = this.performanceStats.order_rejected_drivers_data.concat(element.order_rejected_drivers_data);
              this.performanceStats.order_timed_out_data = this.performanceStats.order_timed_out_data.concat(element.order_timed_out_data);
              this.performanceStats.low_rating_data = this.performanceStats.low_rating_data.concat(element.low_rating_data);
              this.performanceStats.timedout_drivers_data = this.performanceStats.timedout_drivers_data.concat(element.timedout_drivers_data);
              this.performanceStats.scheduled_case_data = this.performanceStats.scheduled_case_data.concat(element.scheduled_case_data);

              this.performanceStats.freelance_revenue += parseFloat(element.freelance_revenue.$numberDecimal);
              this.performanceStats.temp_completed_trips += element.temp_completed_trips;
              this.performanceStats.temp_customer_cancelled += element.temp_customer_cancelled;
              this.performanceStats.temp_dispatcher_cancelled += element.temp_dispatcher_cancelled;
              this.performanceStats.temp_order_timed_out += element.temp_order_timed_out;
              this.performanceStats.temp_rejected_by_driver += element.temp_rejected_by_driver;
              this.performanceStats.temp_driver_cancelled += element.temp_driver_cancelled;

              element.temp_driver_acceptance_rate.forEach(element2 => {
                if (this.performanceStats.temp_dtc_driver_acceptance_rate[element2._id])
                  this.performanceStats.temp_dtc_driver_acceptance_rate[element2._id] += element2.temp_driver_acceptance;
                else
                  this.performanceStats.temp_dtc_driver_acceptance_rate[element2._id] = 10;
              });

              avg_rating += parseFloat(element.avg_rating.$numberDecimal);
              temp_avg_rating += parseFloat(element.temp_avg_rating.$numberDecimal);
            });
            setTimeout(() => {
              this.performanceStats.avg_rating = avg_rating / data.data.length;
              this.performanceStats.temp_avg_rating = temp_avg_rating / data.data.length;
              this.searchSubmit = false;
              console.log(this.performanceStats)
            }, 1000)
          }
          else {
            this.toastr.error(dec ? dec.message : 'something went wrong');
            this.searchSubmit = false;
          }
        });
      }
      else {
        this.toastr.error(dec ? dec.message : 'something went wrong');
        this.searchSubmit = false;
      }
    });
  }
  public createCsv(data, id) {
    this.createCsvFlag = true;
    let labels;
    let title;
    switch (id) {
      case 1:
        title = 'Orders which got cancelled due to drivers not accepting or accepting very late';
        labels = [
          'Unique order id',
          'Order status',
          'Driver username',
          'Driver phone number',
          'Driver E-mail',
          'Vehicle Category',
          'Driver Accepted time',
          'Accepted driver to customer distance(in KM)',
          'Accepted driver to customer duration(in minutes)',
          'Created at',
          'Updated at',
          'Scheduled trip',
          'Scheduled date',
          'Order dispatching time',
        ];
        break;
      case 2:
        title = 'No of order where driver arrived late to pickup the customer';
        labels = [
          'Unique order id',
          'Order status',
          'System ETA (in minutes)',
          'Driver accept ETA (in minutes)',
          'Actual ETA (in minutes)',
          'Difference between actual ETA and system ETA (in minutes)',
          'Difference between actual ETA and driver accept ETA (in minutes)',
          'Pickup location',
          'Drop location',
          'Driver username',
          'Driver name',
          'Driver phone number',
          'Vehicle Category',
          'Order created at'
        ];
        break;
      case 3:
        title = 'No of drivers rejecting orders sent to them';
        labels = [
          'Rejected count',
          'Driver username',
          'Driver name',
          'Driver phone number',
          'Rejected order Ids'
        ];
        break;
      case 4:
        title = 'Orders which got timed out as no driver accepted';
        labels = [
          'Unique order id',
          'Order status',
          'Pickup location',
          'Pickup coordinates',
          'Drop location',
          'Drop coordinates',
          'Vehicle category',
          'System ETA (in minutes)',
          'Order created date',
        ];
        break;
      case 5:
        title = 'No of orders which had very poor rating from customer';
        labels = [
          'Unique order id',
          'Driver username',
          'Driver name',
          'Driver phone number',
          'Vehicle category',
          'Rating given to customer',
          'Comment given to customer',
          'Rating given to driver',
          'Comment given to driver',
          'Order created date'
        ];
        break;
      case 6:
        title = "No of flagged drivers who didn't respond to our request"
        labels = [
          'Non responsive count',
          'Driver username',
          'Driver name',
          'Driver phone number',
          'Non responsive orders'
        ];
        break;
      case 7:
        title = 'Scheduled orders which got cancelled or timed out without getting a vehicle'
        labels = [
          'Unique order id',
          'Order status',
          'Pickup location',
          'Pickup coordinates',
          'Drop location',
          'Drop coordinates',
          'Vehicle category',
          'Created at',
          'Updated at',
          'Scheduled Date',
          'Schedule Before',
          'Order dispatching time',
          'Driver username',
          'Driver name',
          'Driver phone number',
          'Difference between order dispatching and updated at (ms)'
        ];
        break;
      case 8:
        title = 'Scheduled orders which got cancelled or timed out without getting a vehicle'
        labels = [
          'Driver username',
          'Driver name',
          'Driver phone number',
          'Total Pause Time (Hrs)',
          'Pause details'
        ];
        break;
      default:
        title = '';
        labels = [];
        break;
    }
    let resArray = [];
    for (let i = 0; i < data.length; i++) {
      const csvArray = data[i];
      switch (id) {
        case 1:
          resArray.push({
            'Unique_order_id': csvArray["Unique Order Id"],
            'Order_status': csvArray["Order Status"],
            'Driver_username': csvArray["Driver - Username"],
            'Driver_phone number': csvArray["Driver - Phone No"],
            'Driver_E-mail': csvArray["Driver Email"],
            'Vehicle category': csvArray["Vehicle Category"],
            'Driver_Accepted_time': csvArray["Driver Accept Time"],
            'Accepted_driver_to_customer_distance(in_KM)': csvArray["Accepted Driver To Customer Distance (in kms)"],
            'Accepted_driver_to_customer_duration(in_minutes)': csvArray["Accepted Driver To Customer Duration (in minutes)"],
            'Created_at': csvArray["Created At"],
            'Updated_at': csvArray["Updated At"],
            'Scheduled_trip': csvArray["Schedule Trip"],
            'Scheduled_date': csvArray["Scheduled Date"],
            'Order_dispatching_time': csvArray["Order Dipatching Time"],
          });
          break;
        case 2:
          resArray.push({
            'Unique order id': csvArray["Unique Order Id"],
            'Order status': csvArray["Order Status"],
            'System ETA (in minutes)': csvArray["System ETA (in minutes)"] ? csvArray["System ETA (in minutes)"].$numberDecimal : '',
            'Driver Accept ETA': csvArray["Driver Accept ETA"] ? csvArray["Driver Accept ETA"].$numberDecimal : '',
            'Actual ETA (in minutes)': csvArray["Actual ETA (in minutes)"],
            'Difference B/w Actual and System (in minutes)': csvArray["Difference B/w Actual and System (in minutes)"] ? csvArray["Difference B/w Actual and System (in minutes)"].$numberDecimal : '',
            'Difference B/w Actual and Driver Accept ETA (in minutes)': csvArray["Difference B/w Actual and Driver Accept ETA (in minutes)"] ? csvArray["Difference B/w Actual and Driver Accept ETA (in minutes)"].$numberDecimal : '',
            'Pick Up Location': csvArray["Pick Up Location"],
            'Drop Location': csvArray["Drop Location"],
            'Driver username': csvArray["Driver - Username"],
            'Driver - Name': csvArray["Driver - Name"],
            'Driver phone number': csvArray["Driver - Phone No"],
            'Vehicle category': csvArray["Vehicle Category"],
            'Order Created Date': csvArray["Order Created Date"],
          });
          break;
        case 3:
          //let rejectedOrders=csvArray["Reject Order Ids"].map(x=>x.)
          resArray.push({
            'Rejected Count': csvArray["Rejected Count"],
            'Driver username': csvArray["Driver - Username"],
            'Driver - Name': csvArray["Driver - Name"],
            'Driver phone number': csvArray["Driver - Phone No"],
            'Reject Order Ids': JSON.stringify(csvArray["Reject Order Ids"])
          })
          break;
        case 4:
          resArray.push({
            'Unique Order Id': csvArray["Unique Order Id"],
            'Order Status': csvArray["Order Status"],
            'Pick Up Location': csvArray["Pick Up Location"],
            'Pick Up - Cordinates': JSON.stringify(csvArray["Pick Up - Cordinates"]),
            'Drop Location': csvArray["Drop Location"],
            'Drop - Cordinates': JSON.stringify(csvArray["Drop - Cordinates"]),
            'Vehicle Category': csvArray["Vehicle Category"],
            'System ETA (in minutes)': csvArray["System ETA (in minutes)"] ? csvArray["System ETA (in minutes)"].$numberDecimal : '',
            'Order Created Date': csvArray["Order Created Date"]
          })
          break;
        case 5:
          resArray.push({
            'Unique Order Id': csvArray["Unique Order Id"],
            'Driver - username': csvArray["Driver - username"],
            'Driver - Name': csvArray["Driver - Name"],
            'Driver phone number': csvArray["Driver - Phone No"],
            'Vehicle category': csvArray["Vehicle Category"],
            'Rating Given To Customer By Driver': csvArray["Rating Given To Customer By Driver"],
            'Comments Given To Customer By Driver': csvArray["Comments Given To Customer By Driver"],
            'Rating Given To Driver By Customer': csvArray["Rating Given To Driver By Customer"],
            'Comments Given To Driver By Customer': csvArray["Comments Given To Driver By Customer"],
            'Order Created Date': csvArray["Order Created Date"]
          })
          break;
        case 6:
          resArray.push({
            'Non Responsive Count': csvArray["Non Responsive Count"],
            'Driver username': csvArray["Driver - Username"],
            'Driver - Name': csvArray["Driver - Name"],
            'Driver phone number': csvArray["Driver - Phone No"],
            'Non Responsive Order Ids': JSON.stringify(csvArray["Non Responsive Order Ids"])
          })
          break;
        case 7:
          resArray.push({
            'Unique order id': csvArray["Unique Order Id"],
            'Order status': csvArray["Order Status"],
            'Pick Up Location': csvArray["Pick Up Location"],
            'Pick Up - Cordinates': csvArray["Pick Up - Cordinates"],
            'Drop Location': csvArray["Drop Location"],
            'Drop - Cordinates': csvArray["Drop - Cordinates"],
            'Vehicle category': csvArray["Vehicle Category"],
            'Created at': csvArray["Created At"],
            'Updated at': csvArray["Updated At"],
            'Scheduled Date': csvArray["Scheduled Date"],
            'Schedule Before': csvArray["Schedule Before"],
            'Order dispatching time': csvArray["Order Dispatching Time"],
            'Driver username': csvArray["Driver - Username"],
            'Driver - Name': csvArray["Driver - Name"],
            'Driver phone number': csvArray["Driver - Phone No"],
            'Difference between order dispatching and updated at (ms)': csvArray["Diff B/w Order Dipatching And Updated At (ms)"]

          });
          break;
        case 8:
          resArray.push({
            'Driver username': csvArray["Driver - Username"],
            'Driver - Name': csvArray["Driver - Name"],
            'Driver phone number': csvArray["Driver - Phone No"],
            'Total - Pause time': JSON.stringify(csvArray["Pause Details"])
          })
          break;
        default:
          resArray = []
          break;
      }
    }
    var options =
    {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: false,
      useBom: true,
      headers: (labels)
    };
    this.createCsvFlag = false;
    new Angular2Csv(resArray, title + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
  }
  resetMinutes(data, id) {
    var coeff = 1000 * 60 * 20;
    var date = new Date(data);
    if (id == 1)
      this.startDate = new Date(Math.round(date.getTime() / coeff) * coeff);
    else
      this.endDate = new Date(Math.round(date.getTime() / coeff) * coeff);
  }
}
