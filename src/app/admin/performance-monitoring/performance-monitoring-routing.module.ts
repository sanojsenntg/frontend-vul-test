import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerformanceMonitoringComponent } from './performance-monitoring/performance-monitoring.component';
import { AclAuthervice } from './../../common/services/access-control/acl-auth.service';
import { PerformanceMailStatsComponent } from './performance-mail-stats/performance-mail-stats.component'

const routes: Routes = [];

export const PerformanceMonitoringRoutes: Routes = [
  { path: '', component: PerformanceMonitoringComponent, canActivate: [AclAuthervice], data: { roles: ["performance-monitoring"] } },
  { path: 'mail-stats', component: PerformanceMailStatsComponent, canActivate: [AclAuthervice], data: { roles: ["performance-mail-stats"] } },
]
