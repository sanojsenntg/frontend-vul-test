import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoverageareaService } from '../../../../common/services/coveragearea/coveragearea.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { CoverageAreaModule } from '../coverage-area.module';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-list-coverage-area',
  templateUrl: './list-coverage-area.component.html',
  styleUrls: ['./list-coverage-area.component.css']
})
export class ListCoverageAreaComponent implements OnInit {
  public coverageAreaData;
  public itemsPerPage = 10;
  public pageNo = 0;
  public coverageAreaDataLength;
  public is_search = false;
  public searchLoader = false;
  public sortOrder = 'asc';
  public keyword = '';
  key: string = '';
  public coveragearea_name = '';
  public companyId = [];
  public session

  constructor(private _CoverageareaService: CoverageareaService,
    private router: Router,
    public dialog: MatDialog,
    public overlay: Overlay,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public jwtService: JwtService,
    public encDecService:EncDecService) { 
    }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');

    this.searchLoader = true;
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._CoverageareaService.getCoverageAreas(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.coverageAreaData = data.coverageArea;
          this.coverageAreaDataLength = data.totalCount;
          this.searchLoader = false;
        }
      }
    });
  }
  /**
     * Confirmation popup for deleting particular payment zone record
     * @param id 
     */
  public deleteCoverageArea(id) {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        var params = {
          company_id: this.companyId,
          _id:id
        }
        var encrypted = this.encDecService.nwt(this.session,params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._CoverageareaService.updateDeletedStatus(enc_data)
          .then((dec) => {
            if (dec.status === 200) {
              this.refetchCoverageZone();
              this.coverageAreaData.splice(id, 1);
              this._toasterService.success('Coverage Area deleted successfully.');
            } else if (dec.status === 201) {
            }
          });
      } else {
      }
    });
  }

  /**
  * To refresh payment zone records showing in the grid
  */
  public refetchCoverageZone() {
    this.getCoverageArea(this.keyword);
  }

  /**
   * To reset the search filters and reload the payment zone records
   */
  public reset() {
    this.keyword = '';
    this.coveragearea_name = '';
    this.ngOnInit();
  }

  /**
   * For sorting payment zone records 
   * @param key 
   */
  sort(key) {
    this.searchLoader = true;
    this.key = key;
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._CoverageareaService.getCoverageAreas(enc_data).then((dec) => {
      this.searchLoader = false;
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.coverageAreaData = data.coverageArea; 
        }
      }else{
        console.log(dec.message)
      }

    });
  }

  /**
   * Pagination for payment zone module
   * @param data 
   */
  pagingAgent(data) {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._CoverageareaService.getCoverageAreas(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.coverageAreaData = data.coverageArea;
        }
      }else{
        console.log(dec.message)
      }
    
    });
  }

  /**
   * For searching payment zone records 
   * @param keyword 
   */
  public getCoverageArea(keyword) {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._CoverageareaService.getCoverageAreas(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.coverageAreaData = data.coverageArea;
          this.coverageAreaDataLength = data.totalCount;
          this.searchLoader = false;
          this.is_search = true;
        }
      }else{
        console.log(dec.message)
      }
    });
  }
}
