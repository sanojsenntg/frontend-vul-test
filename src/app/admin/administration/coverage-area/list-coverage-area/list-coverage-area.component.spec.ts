import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCoverageAreaComponent } from './list-coverage-area.component';

describe('ListCoverageAreaComponent', () => {
  let component: ListCoverageAreaComponent;
  let fixture: ComponentFixture<ListCoverageAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCoverageAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCoverageAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
