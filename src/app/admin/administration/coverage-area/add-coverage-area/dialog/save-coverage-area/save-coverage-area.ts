import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverService } from '../../../../../../common/services/driver/driver.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { CoverageareaService } from '../../../../../../common/services/coveragearea/coveragearea.service';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-save-coverage-area',
  templateUrl: './save-coverage-area.html',
  styleUrls: ['./save-coverage-area.css'],
})

export class SaveCoverageAreaComponent implements OnInit {

  public addCoverageArea: FormGroup;
  public CoverageArea;
  public CoverageAreaData;
  public companyId;
  public session;
  constructor(
    public jwtService: JwtService,
    private _coverageareaService: CoverageareaService,
    public dialogRef: MatDialogRef<SaveCoverageAreaComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private formBuilder: FormBuilder,
    private router: Router,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.CoverageArea = data;
    this.addCoverageArea = formBuilder.group({
      company: [''],
      type: [''],
      bounds: [''],
      name: ['', [
        Validators.required
      ]]
    });
  }

  ngOnInit() {
    this.companyId = localStorage.getItem('user_company')
    this.session = localStorage.getItem('Sessiontoken');

    const params = {
      offset: 0,
      limit: 100,
      sortOrder: 'desc',
      sortByColumn: 'updated_at'
    };
  }
  saveCoverageArea() {
    if (!this.addCoverageArea.valid) {
      return;
    }
    this.addCoverageArea.value.type = this.CoverageArea.type;
    this.addCoverageArea.value.bounds = this.CoverageArea.bounds;
    var params = {
      company: this.addCoverageArea.value.company,
      type: this.addCoverageArea.value.type,
      bounds: this.addCoverageArea.value.bounds,
      name: this.addCoverageArea.value.name,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._coverageareaService.saveCoverageArea(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          this.toastr.success('Coverage Area saved successfully.');
          this.dialogRef.close(true);
          this.router.navigate['admin/administration/coverage-area']
        }
      }
    });
  }
  closePop() {
    this.dialogRef.close(false);
  }
}
