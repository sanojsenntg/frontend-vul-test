import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCoverageAreaComponent } from './add-coverage-area.component';

describe('AddCoverageAreaComponent', () => {
  let component: AddCoverageAreaComponent;
  let fixture: ComponentFixture<AddCoverageAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCoverageAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCoverageAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
