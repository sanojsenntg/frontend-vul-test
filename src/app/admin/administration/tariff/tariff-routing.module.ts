import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListTariffComponent } from './list-tariff/list-tariff.component';
import { AddTariffComponent } from './add-tariff/add-tariff.component';
import { EditTariffComponent } from './edit-tariff/edit-tariff.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const tariffRoutes: Routes = [
  	{path: '', component: ListTariffComponent, canActivate: [AclAuthervice], data: {roles: ["Tariffs - List"]}},
  	{path: 'add-tariff', component: AddTariffComponent, canActivate: [AclAuthervice], data: {roles: ["Tariffs - Add"]}},
	{path: 'edit-tariff/:id', component: EditTariffComponent, canActivate: [AclAuthervice], data: {roles: ["Tariffs - Edit"]}},
];


  














