import { Component, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { TariffService } from '../../../../../../common/services/tariff/tariff.service';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
	selector: 'zones',
	templateUrl: 'zones.html',
	styleUrls: ['zones.css']
})
export class ZonesComponent {
	public tariffData;
	public jsondata;
	public coordinatesdata = [];
	public cordinates = [];
	public name;
	public tariffId;
	public cordinateslength;
	public editForm: FormGroup;
	public errormessage = 0;
	public companyId: any = [];
	public session;
	constructor(
		private fb: FormBuilder,
		public _toasterService: ToastsManager,
		private router: Router,
		public encDecService: EncDecService,
		private _tariffService: TariffService,
		public dialogRef: MatDialogRef<ZonesComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
	) {
		this.tariffId = data._id;

	}

	ngOnInit() {
		//console.log(this.tariffData);
		this.companyId.push( localStorage.getItem('user_company'))
		this.session = localStorage.getItem('Sessiontoken');
	
		var params = {
			company_id: this.companyId,
			_id: this.tariffId
		}
		var encrypted = this.encDecService.nwt(this.session, params);
		var enc_data = {
			data: encrypted,
			email: localStorage.getItem('user_email')
		}
		this._tariffService.getTariffById(enc_data)
			.then((dec) => {
				if (dec) {
					if (dec.status == 200) {
						var res: any = this.encDecService.dwt(this.session, dec.data);
						if (res.zone != null) {
							this.tariffData = JSON.parse(res.zone);
							if (this.tariffData.coordinates) {
								this.name = this.tariffData.name ? this.tariffData.name : "";
								this.tariffData = this.tariffData.coordinates;
								if (this.tariffData.length > 0) {
									for (var i = 0; i < this.tariffData.length; i++) {
										this.cordinates = this.tariffData[i].split(',');
										this.coordinatesdata.push(this.cordinates);
									}
									this.cordinateslength = this.coordinatesdata.length;
									console.log('after ', this.coordinatesdata);
								} else {
									this.cordinates = ["", ""];
									this.coordinatesdata.push(this.cordinates);
									this.cordinateslength = this.coordinatesdata.length;
								}
							} else {
								this.cordinates = ["", ""];
								this.coordinatesdata.push(this.cordinates);
								this.cordinateslength = this.coordinatesdata.length;
							}
						} else {
							this.cordinates = ["", ""];
							this.coordinatesdata.push(this.cordinates);
							this.cordinateslength = this.coordinatesdata.length;
						}
					}
				} else {
					console.log(dec.message)
				}

			});
	}

	/**
	 * Save Shift Timings
	 *
	 * @param formData
	 */
	public updateTariffCordinates() {
		if (this.name != "") {
			var status = 0;
			if (this.coordinatesdata.length > 0) {
				for (var i = 0; i < this.coordinatesdata.length; i++) {
					if (this.coordinatesdata[i][0] == "") {
						status = 1;
					}
					if (this.coordinatesdata[i][1] == "") {
						status = 1;
					}
				}
			}

			if (status == 1) {
				this.errormessage = 1;
			} else {
				const params = {
					id: this.tariffId,
					name: this.name,
					coordinates: this.coordinatesdata,
					company_id: this.companyId
				};
				var encrypted = this.encDecService.nwt(this.session, params);
				var enc_data = {
					data: encrypted,
					email: localStorage.getItem('user_email')
				}
				this._tariffService.updateTariffCoordinates(enc_data)
					.then((dec) => {
						if (dec) {
							if (dec.status === 200) {
								this._toasterService.success('Tariff updated successfully.');
								this.router.navigate(['/admin/administration/tariffs']);
							}
							else if (dec.status === 201) {
								this._toasterService.error('Tariff updation failed.');
							}
							this.dialogRef.close();
						} else {
							console.log(dec.message)
						}

					})
					.catch((error) => {
						this.router.navigate(['/login']);
					});
			}

		} else {
			this.errormessage = 1;
		}
	}

	public addnewcoordinates(i) {
		this.errormessage = 0;
		this.cordinates = ["", ""];
		this.coordinatesdata.push(this.cordinates);
		this.cordinateslength = this.coordinatesdata.length;
	}

	public removecoordinates(k) {
		this.errormessage = 0;
		var corddata = [];
		for (var i = 0; i < this.coordinatesdata.length; i++) {
			if (i != k) {
				corddata.push(this.coordinatesdata[i]);
			}
		}
		this.coordinatesdata = corddata;
		this.cordinateslength = this.coordinatesdata.length;
	}

	closePop() {
		this.dialogRef.close();
	}

}
