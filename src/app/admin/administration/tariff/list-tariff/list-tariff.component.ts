import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TariffService } from '../../../../common/services/tariff/tariff.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { AdditionalService } from '../../../../common/services/additional_service/additional_service.service';
import { ZonesComponent } from './dialog/zones/zones';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-list-tariff',
  templateUrl: './list-tariff.component.html',
  styleUrls: ['./list-tariff.component.css']
})

export class ListTariffComponent implements OnInit {
  public companyId: any = [];
  public tariffData;
  public is_search = false;
  pageSize = 10;
  pageNo = 0;
  totalPage = [5];
  tariffLength;
  public tariffRecords;
  public name;
  public showLoader = false;
  del = false;
  searchValue;
  public serviceGroupData;
  public additional_service_id: any;
  public tariff_type = '';
  public is_peek_tariff = '';
  sortOrder = 'asc';
  public keyword;
  key: string = '';
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public session;
  constructor(
    private _tariffService: TariffService,
    private router: Router,
    public jwtService: JwtService,
    public dialog: MatDialog,
    private _additional_Service: AdditionalService,
    private route: ActivatedRoute,
    public overlay: Overlay,
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    public encDecService: EncDecService,
    public _aclService: AccessControlService) {
    this.toastr.setRootViewContainerRef(vcr);

  }

  /**
   * Function which runs first when the page loads
   */
  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.aclDisplayService();
    this.getAllTariffs();
    this.getServices();
    this.is_search = false;
    this.showLoader = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
      search: ''
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._tariffService.getTariffs(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.tariffData = data.getTariff;
          this.tariffLength = data.totalCount;
          this.showLoader = false;
        } else {
          console.log(dec.message)
        }
      }

    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Tariffs - Add") {
              this.aclAdd = true;
            } else if (data.menu[i] == "Tariffs - Edit") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Tariffs - Delete") {
              this.aclDelete = true;
            }
          };
        }
      } else {
        console.log(dec.message)
      }
    })
  }
  /**
   * Confirmation popup for deleting particular tariff record
   * @param id 
   */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '350px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this data' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        // this.del = true;
        this.deleteTariff(id);
      } else {
      }
    });
  }

  /**
   * Delete tariff from view
   * @param id 
   */
  public deleteTariff(id) {
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._tariffService.updateTariffForDeletion(enc_data)
      .then((res) => {
        if (res.status === 200) {
          this.refetchTariff();
          this.tariffData.splice(id, 1);
          this.toastr.success('Tariff deleted successfully.');
        } else if (res.status === 201) {
        } else if (res.status === 203) {
          this.toastr.warning('This tariff is assigned to order so you cannot delete it.');
        }
      })
  }

  /**
   * To refresh tariff records showing in the grid
   */
  public refetchTariff() {
    this.getTariffsForSearch();
  }

  /**
   * To reset the search filters and reload the tariff records
   */
  public reset() {
    this.name = '';
    this.tariff_type = '';
    this.is_peek_tariff = '';
    this.additional_service_id = '';
    this.ngOnInit();
  }

  /**
   * Pagination for tariff module
   * @param data 
   */
  pagingAgent(data) {
    this.showLoader = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    var params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        additional_service_id: this.additional_service_id ? this.additional_service_id._id : '',
        tariff_type: this.tariff_type ? this.tariff_type : '',
        is_peek_tariff: this.is_peek_tariff ? this.is_peek_tariff : '',
        name: this.name ? this.name : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._tariffService.searchingTariff(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.tariffData = data.tariffs;
          this.tariffLength = data.count;
        }
      } else {
        console.log(dec.message)
      }
      this.showLoader = false;
    });
  }

  /**
   * For searching tariff records (old)
   * @param keyword 
   */
  public getTariff(keyword) {
    this.showLoader = true;
    this.keyword = keyword;

    if (this.keyword) {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId,
        search: this.keyword
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._tariffService.getTariffs(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.tariffData = data.getTariff;
            this.tariffLength = data.totalCount;
            this.showLoader = false;
            this.is_search = true;
          }
        } else {
          console.log(dec.message)
        }
      });
    } else {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId,
        search: ''
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._tariffService.getTariffs(params).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.tariffData = data.getTariff;
            this.tariffLength = data.totalCount;
            this.showLoader = false;
            this.is_search = false;
          }
        } else {
          console.log(dec.message)
        }

      });
    }
  }

  /**
   * For sorting tariff records 
   * @param key 
   */
  sort(key) {
    this.showLoader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    if (this.is_search) {
      var params;
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        additional_service_id: this.additional_service_id ? this.additional_service_id._id : '',
        tariff_type: this.tariff_type ? this.tariff_type : '',
        is_peek_tariff: this.is_peek_tariff ? this.is_peek_tariff : '',
        name: this.name ? this.name : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._tariffService.searchingTariff(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.tariffData = data.tariffs;
          this.tariffLength = data.count;
        }
      } else {
        console.log(dec.message)
      }
      this.showLoader = false;
    });


    // if (this.additional_service_id != '' || this.tariff_type != '' || this.is_peek_tariff != '' || this.name != ''){
    //   this._tariffService.searchingTariff(params).then((data) => {
    //     this.tariffData = data.tariffs;
    //     this.tariffLength = data.count; 
    //     this.showLoader = false;
    //   });
    // } else {
    //   this._tariffService.getTariffs(null, params).then((data) => {
    //     this.tariffData = data.getTariff;
    //     this.tariffLength = data.totalCount;

    //   });
    // }
  }

  /**
   * For searching tariff records (Latest)
   */
  public getTariffsForSearch() {
    this.showLoader = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      additional_service_id: this.additional_service_id ? this.additional_service_id._id : '',
      tariff_type: this.tariff_type ? this.tariff_type : '',
      is_peek_tariff: this.is_peek_tariff ? this.is_peek_tariff : '',
      name: this.name ? this.name : '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._tariffService.searchingTariff(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.tariffData = data.tariffs;
          this.tariffLength = data.count;
          this.is_search = true;
        }
      } else {
        console.log(dec.message)
      }
      this.showLoader = false;
    });

  }

  /**
   * Get Additional services for dropdown
   *
   */
  public getServices() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._additional_Service.getaddService(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.serviceGroupData = data.getService;
        }
      } else {
        console.log(dec.message)
      }
    });
  }

  /**
   * Autocomplete on additional service filter
   * @param data 
   */
  public searchAddService(data) {
    if (typeof data !== 'object') {
      let params = {
        offset: 0,
        limit: 0,
        sortOrder: 'desc',
        sortByColumn: '_id',
        'name': data,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._additional_Service.getaddService(enc_data).subscribe((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.serviceGroupData = data.getService;
          }
        } else {
          console.log(dec.message)
        }

      });
    } else {
      this.additional_service_id = data.additional_service_id;
    }
  }

  displayFnAddService(data): string {
    return data ? data.name : data;
  }

  /**
   * Get tariff list for dropdown
   */
  public getAllTariffs() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._tariffService.getTariffs(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.tariffRecords = data.getTariff;
        }
      } else {
        console.log(dec.message)
      }

    });
  }

  /**
   * Autocomplete search on tariff filter
   * @param data 
   */
  public advanceSearchForTariff(data) {
    if (typeof data !== 'object') {
      let params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: '_id',
        'tariff_type': data,
        search: '',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._tariffService.getTariffs(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.tariffRecords = data.getTariff;
          }
        } else {
          console.log(dec.message)
        }

      });
    } else {
      this.tariff_type = data._id;
    }
  }
  displayFnTariff(data): string {
    return data ? data.tariff_type : data;
  }
  /**
   * Open dialog for edit coordinates of particular tariff.
   * @param driverData
   */

  zones(tariffdata) {
    this.dialog.closeAll();
    let dialogRef = this.dialog.open(ZonesComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: tariffdata
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
