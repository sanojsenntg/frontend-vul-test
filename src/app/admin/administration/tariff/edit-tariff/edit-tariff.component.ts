import { Component, OnInit, ViewContainerRef, AfterViewInit } from '@angular/core';
import { TariffService } from '../../../../common/services/tariff/tariff.service';
import { AdditionalService } from '../../../../common/services/additional_service/additional_service.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { AmazingTimePickerService } from 'amazing-time-picker';
import * as moment from 'moment';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { AdditionalSplitupComponent } from '../../../../common/dialog/additional-splitup/additional-splitup.component';
import { Overlay } from '@angular/cdk/overlay';

@Component({
  selector: 'app-edit-tariff',
  templateUrl: './edit-tariff.component.html',
  styleUrls: ['./edit-tariff.component.css']
})
export class EditTariffComponent implements OnInit, AfterViewInit {
  public companyId: any = [];
  selectedValue: any;
  peekDayList = [
    { value: 'Monday', viewValue: 'Monday' },
    { value: 'Tuesday', viewValue: 'Tuesday' },
    { value: 'Wednesday', viewValue: 'Wednesday' },
    { value: 'Thursday', viewValue: 'Thursday' },
    { value: 'Friday', viewValue: 'Friday' },
    { value: 'Saturday', viewValue: 'Saturday' },
    { value: 'Sunday', viewValue: 'Sunday' }
  ];
  public modelsFlag = false;
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public serviceData;
  public showpeek = false;
  public showerror;
  public PeekendError;
  public vehicle_models;
  public modelFilterList = [];
  public is_general = 0;
  public CompanyData = [];
  public additional_splitup_data = [];
  tariff_types = ['HOURLY_RATE', 'FIX_RATE', 'GPS_TAXIMETER'];
  session;
  constructor(private _tariffService: TariffService,
    private _additional_service: AdditionalService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _vehicleModelsService: VehicleModelsService,
    private router: Router,
    public jwtService: JwtService,
    private _companyservice: CompanyService,
    private _toasterService: ToastsManager,
    public overlay: Overlay,
    public dialog: MatDialog,
    private atp: AmazingTimePickerService,
    public encDecService: EncDecService,
    private vcr: ViewContainerRef) {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this._toasterService.setRootViewContainerRef(vcr);
    this.editForm = formBuilder.group({
      name: ['', [Validators.required]],
      tariff_type: ['', [Validators.required]],
      vat: ['', [Validators.required]],
      unit_price: [''],
      partner_only: [''],
      price_with_vat: ['', [Validators.required]],
      additional_service_id: [''],
      is_peek_tariff: ['', [Validators.required]],
      start_price: ['', [Validators.required]],
      price_per_distance: ['', [Validators.required]],
      standing_price: ['', [Validators.required]],
      denomination: ['', [Validators.required]],
      included_in_start: [''],
      distance_charge_unit: ['', [Validators.required]],
      standing_speed_limit: ['', [Validators.required]],
      percentage_multiplier: [''],
      minimum_fare: ['', [Validators.required]],
      peek_days: [''],
      peek_time_start: [''],
      peek_time_end: [''],
      is_general: ['', [Validators.required]],
      companies: ['', [Validators.required]],
      is_for_driver_list: ['-1', []],
    });
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: ''
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._vehicleModelsService.getVehicleModels(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.vehicle_models = data.vehicleModelsList;
        } else {
          console.log(dec.message)
        }
      }
    });
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CompanyData = data.getCompanies;
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  public ngOnInit(): void {
    //this.editForm.controls['company'].disable();
    //this.getVehicleModels();
    //this.getadditionalservice();
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
  }
  ngAfterViewInit(): void {
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._tariffService.getTariffById(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var item: any = this.encDecService.dwt(this.session, dec.data);
          var res = item.SingleDetail;
          console.log(res);
          if (res.is_peek_tariff == '1') {
            this.showpeek = true;
            var string = "peekDayList";
            this.selectedValue = res.peek_days.split(',');
          } else {
            this.showpeek = false;
          }
          console.log(res)
          this.modelsFlag = true;
          this.editForm.patchValue({
            name: res.name ? res.name : '',
            company: res.company ? res.company : '',
            tariff_type: res.tariff_type ? res.tariff_type : '',
            vat: res.vat ? res.vat : '',
            unit_price: res.unit_price ? res.unit_price : '',
            partner_only: res.partner_only ? res.partner_only : '',
            price_with_vat: res.price_with_vat ? res.price_with_vat : '',
            additional_service_id: res.additional_service_id ? res.additional_service_id._id : '',
            is_peek_tariff: res.is_peek_tariff ? res.is_peek_tariff : '',
            start_price: res.start_price ? res.start_price : '',
            price_per_distance: res.price_per_distance ? res.price_per_distance : '',
            standing_price: res.standing_price ? res.standing_price : '',
            denomination: res.denomination ? res.denomination : '',
            included_in_start: res.included_in_start ? res.included_in_start : '',
            distance_charge_unit: res.distance_charge_unit ? res.distance_charge_unit : '',
            standing_speed_limit: res.standing_speed_limit ? res.standing_speed_limit : '',
            percentage_multiplier: res.percentage_multiplier ? res.percentage_multiplier : '',
            minimum_fare: res.minimum_fare ? res.minimum_fare : '',
            peek_days: res.peek_days ? res.peek_days.split(',') : '',
            peek_time_start: res.peek_time_start ? res.peek_time_start : '',
            peek_time_end: res.peek_time_end ? res.peek_time_end : '',
            is_general: res.is_general ? res.is_general : '',
            companies: res.company_list ? res.company_list : '',
            is_for_driver_list: res.is_for_driver_list ? res.is_for_driver_list : '-1',
          });
          this.additional_splitup_data = res.additional_splitup ? res.additional_splitup : [];
          if (res.tariff_type == 'FIX_RATE') {
            this.checkTariff();
          }
          if (res.vehicle_model_id.length > 0) {
            for (var i = 0; i < res.vehicle_model_id.length; i++) {
              this.modelFilterList.push(res.vehicle_model_id[i]._id);
              if (i == (res.vehicle_model_id.length - 1)) {
                this.models = res.vehicle_model_id;
              }
            }
          }
          if (this.vehicle_models.length > 0 && this.modelFilterList.length > 0)
            var index = this.vehicle_models.length;
          while (index--) {
            for (var k = 0; k < this.modelFilterList.length; k++) {
              if (this.vehicle_models[index]._id == this.modelFilterList[k]) {
                this.vehicle_models.splice(index, 1);
              }
            }
          }
        }
      } else {
        console.log(dec.message)
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
  }

  public checkPeekStartTime() {
    const ShiftpeekTimePicker = this.atp.open();
    ShiftpeekTimePicker.afterClose().subscribe(time => {
      //console.log(time);
      this.editForm.controls['peek_time_start'].setValue(time);
      this.editForm.controls['peek_time_end'].setValue('');
      this.showerror = false;
    });
  }

  public checkPeekEndTime() {
    const ShiftpeekEndPicker = this.atp.open();
    ShiftpeekEndPicker.afterClose().subscribe(time => {
      //console.log(time);
      this.editForm.controls['peek_time_end'].setValue(time);
      // console.log('gfh>>', this.editForm.value.peek_time_start);
      if (moment(this.editForm.value.peek_time_end, 'HH:mm:ss') < moment(this.editForm.value.peek_time_start, 'HH:mm:ss')) {
        //console.log("Start Time is less than end time");
        this.PeekendError = 'The end time should be greater than start time.';
        this.showerror = true;
      } else {
        this.showerror = false;
      }
    });

  }

  /**
   * To update the tariff records
   * 
   */
  public updateTariff() {
    if (!this.editForm.valid || this.models.length == 0) {
      //console.log(this.editForm)
      this._toasterService.error('All fields are required.');
      //const er=this.findInvalidControls()
      return;
    } else {
      console.log(this.additional_splitup_data.length);
      if (this.editForm.value.is_for_driver_list !== '-1' && this.additional_splitup_data.length == 0) {
        this._toasterService.error('Split up fields are required');
      } else if (this.editForm.value.is_for_driver_list == '-1' && this.additional_splitup_data.length > 0) {
        this._toasterService.error('Select split up applicable if split up is selected');
      } else {
        console.log(this.editForm.value.companies);
        const tariffdata = {
          name: this.editForm.value.name,
          tariff_type: this.editForm.value.tariff_type,
          vat: this.editForm.value.vat,
          unit_price: this.editForm.value.unit_price,
          partner_only: this.editForm.value.partner_only,
          price_with_vat: this.editForm.value.price_with_vat,
          additional_service_id: this.editForm.value.additional_service_id ? this.editForm.value.additional_service_id : null,
          is_peek_tariff: this.editForm.value.is_peek_tariff,
          start_price: this.editForm.value.start_price,
          price_per_distance: this.editForm.value.price_per_distance,
          standing_price: this.editForm.value.standing_price,
          denomination: this.editForm.value.denomination,
          included_in_start: this.editForm.value.included_in_start,
          distance_charge_unit: this.editForm.value.distance_charge_unit,
          standing_speed_limit: this.editForm.value.standing_speed_limit,
          percentage_multiplier: this.editForm.value.percentage_multiplier,
          minimum_fare: this.editForm.value.minimum_fare,
          peek_days: this.editForm.value.peek_days,
          peek_time_start: this.editForm.value.peek_time_start,
          peek_time_end: this.editForm.value.peek_time_end,
          vehicle_model: this.modelFilterList,
          is_general: this.editForm.value.is_general,
          companies: this.editForm.value.companies,
          company_id: localStorage.getItem('user_company'),
          is_for_driver_list: this.editForm.value.is_for_driver_list,
          additional_splitup: this.additional_splitup_data ? this.additional_splitup_data : [],
          _id: this._id
        };
        console.log(tariffdata)
        var encrypted = this.encDecService.nwt(this.session, tariffdata);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._tariffService.updateTariff(enc_data)
          .then((dec) => {
            if (dec) {
              if (dec.status === 200) {
                this._toasterService.success('Tariff updated successfully.');
                setTimeout(() => {
                  this.router.navigate(['/admin/administration/tariffs']);
                }, 1000);
              }
              else if (dec.status === 201) {
                this._toasterService.error('Tariff updation failed.');
              }
              else if (dec.status === 204) {
                var res: any = this.encDecService.dwt(this.session, dec.data);
                let gModels = '';
                for (var i = 0; i < res.vehicle_models.length; i++)
                  gModels += res.vehicle_models[i] + ',';
                this._toasterService.error('Vehicle model already has a general tariff for these vehicles, ' + gModels);
              }
              else {
                this._toasterService.error(dec.message);
              }
            }

          })
          .catch((error) => {
            this.router.navigate(['/login']);
          });
      }
    }
  }
  public findInvalidControls() {
    const invalid = [];
    const controls = this.editForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }
  public ShowPeekTime(data) {
    if (data.target.value == '1') {
      this.showpeek = true;
      this.editForm.controls['peek_days'].setValidators(Validators.required);
      this.editForm.controls['peek_time_start'].setValidators(Validators.required);
      this.editForm.controls['peek_time_end'].setValidators(Validators.required);
    } else {
      this.showpeek = false;
      this.showerror = false;
      this.editForm.controls['peek_days'].setValidators([]);
      this.editForm.controls['peek_time_start'].setValidators([]);
      this.editForm.controls['peek_time_end'].setValidators([]);
    }
  }

  /**
   *  Get Additional service For Dropdown
   * 
   */

  public getadditionalservice() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._additional_service.get(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.serviceData = data;
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  /* public getVehicleModels() {
     const params = {
       offset: 0,
       sortOrder: 'desc',
       sortByColumn: 'updated_at'
     };
     this._vehicleModelsService.getVehicleModels(null, params).then((data) => {
       this.vehicle_models = data.vehicleModelsList;
     });
   }*/
  visible = true;
  selectable = true;
  removable = true;
  modelCtrl = new FormControl();
  public models: any[] = [];

  remove(fruit): void {
    const index = this.models.indexOf(fruit);
    const index2 = this.modelFilterList.indexOf(fruit._id);
    if (index2 >= 0)
      this.modelFilterList.splice(index2, 1);
    if (index >= 0) {
      this.models.splice(index, 1);
      this.vehicle_models.push(fruit)
    }
    console.log(this.modelFilterList)
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.models.push(event.option.value);
    this.modelFilterList.push(event.option.value._id);
    const index = this.vehicle_models.indexOf(event.option.value);
    this.vehicle_models.splice(index, 1);
    this.modelCtrl.setValue(null);
    console.log(this.modelFilterList)
  }
  displayFnModels(data): string {
    return data ? '' : '';
  }
  public hideFields = false;
  checkTariff() {
    console.log(this.editForm.value.tariff_type)
    if (this.editForm.value.tariff_type == 'FIX_RATE') {
      this.editForm.controls["start_price"].setValue(0);
      this.editForm.controls["price_per_distance"].setValue(0);
      this.editForm.controls["standing_price"].setValue(0);
      this.editForm.controls["denomination"].setValue(0);
      this.editForm.controls["included_in_start"].setValue(0);
      this.editForm.controls["distance_charge_unit"].setValue(0);
      this.editForm.controls["standing_speed_limit"].setValue(0);
      this.editForm.controls["minimum_fare"].setValue(0);
      this.hideFields = true;
    }
    else {
      this.hideFields = false;
    }
  }
  AdditionalSplitup() {
    this.dialog.closeAll();
    let data = {
      ops: "edit",
      splitup: this.additional_splitup_data ? this.additional_splitup_data : [],
      company_id: this.companyId.length > 0 ? this.companyId : [this.companyId]
    }
    let dialogRef = this.dialog.open(AdditionalSplitupComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("Result++++" + JSON.stringify(result));
      this.additional_splitup_data = result;
    });
  }
}

























