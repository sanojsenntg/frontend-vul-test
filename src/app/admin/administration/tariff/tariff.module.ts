import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTariffComponent } from './list-tariff/list-tariff.component';
import { TariffService } from '../../../common/services/tariff/tariff.service';
import { TariffRestService } from '../../../common/services/tariff/tariffrest.service';
import { AddTariffComponent } from './add-tariff/add-tariff.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditTariffComponent } from './edit-tariff/edit-tariff.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule, MatChipsModule } from '@angular/material';
import { ZonesComponent } from './list-tariff/dialog/zones/zones';
import { AdditionalSplitupComponent } from '../../../common/dialog/additional-splitup/additional-splitup.component';

@NgModule({
   imports: [
      CommonModule,
      FormsModule,
      RouterModule,
      ReactiveFormsModule,
      NgbModule,
      MatSelectModule,
      MatAutocompleteModule,
      Ng2OrderModule,
      AmazingTimePickerModule,
      MatTooltipModule,
      MatChipsModule,
      MatIconModule
   ],
   declarations: [
      ListTariffComponent,
      AddTariffComponent,
      EditTariffComponent,
      ZonesComponent
   ],
   exports: [
      RouterModule,
      MatSelectModule
   ],
   entryComponents: [ZonesComponent, AdditionalSplitupComponent
   ],
   providers: [
      TariffService, TariffRestService]
})

export class TariffModule { }





