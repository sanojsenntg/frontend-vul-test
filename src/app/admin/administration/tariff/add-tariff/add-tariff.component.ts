import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { TariffService } from '../../../../common/services/tariff/tariff.service';
import { AdditionalService } from '../../../../common/services/additional_service/additional_service.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import * as moment from 'moment';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { AdditionalSplitupComponent } from '../../../../common/dialog/additional-splitup/additional-splitup.component';
import { Overlay } from '@angular/cdk/overlay';

@Component({
  selector: 'app-add-tariff',
  templateUrl: './add-tariff.component.html',
  styleUrls: ['./add-tariff.component.css'],

})
export class AddTariffComponent implements OnInit {
  selectedValue: string;
  public companyId: any = [];
  peekDayList = [
    { value: 'Monday', viewValue: 'Monday' },
    { value: 'Tuesday', viewValue: 'Tuesday' },
    { value: 'Wednesday', viewValue: 'Wednesday' },
    { value: 'Thursday', viewValue: 'Thursday' },
    { value: 'Friday', viewValue: 'Friday' },
    { value: 'Saturday', viewValue: 'Saturday' },
    { value: 'Sunday', viewValue: 'Sunday' }
  ];

  private sub: Subscription;
  public _id;
  public serviceData;
  public showpeek = false;
  tariff_types = ['HOURLY_RATE', 'FIX_RATE', 'GPS_TAXIMETER'];
  public Tariff: any = {
    name: '',
    tariff_type: '',
    vat: '',
    unit_price: 0,
    partner_only: '0',
    price_with_vat: '',
    additional_service_id: 0,
    is_peek_tariff: '',
    start_price: '',
    price_per_distance: '',
    standing_price: '',
    denomination: '',
    included_in_start: '',
    distance_charge_unit: '',
    standing_speed_limit: '',
    percentage_multiplier: 0,
    minimum_fare: '',
    peek_days: '',
    peek_time_start: '',
    peek_time_end: '',
    vehicle_model: [],
    companies: [],
    is_general: '0',
    is_for_driver_list: '-1',
    additional_splitup: []
  };
  public PeekendError;
  public showerror;
  public vehicle_models = [];
  public hideFields = false;
  public CompanyData = [];
  session
  constructor(private _tariffService: TariffService,
    private _additional_service: AdditionalService,
    private _vehicleModelsService: VehicleModelsService,
    private _companyservice: CompanyService,
    private route: ActivatedRoute,
    private router: Router,
    public overlay: Overlay,
    public dialog: MatDialog,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    private atp: AmazingTimePickerService,
    public encDecService: EncDecService
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  public ngOnInit(): void {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');

    this.getadditionalservice();
    this.getVehicleModels();
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CompanyData = data.getCompanies;
        }
      } else {
        console.log(dec.message)
      }
    });
  }

  /**
     * To add the tariff records
     *
     */
  onSubmit(formData): void {
    console.log(this.Tariff.companies);
    if (formData.valid && this.models.length > 0) {
      if (this.Tariff.is_for_driver_list !== '-1' && this.Tariff.additional_splitup.length == 0) {
        this.toastr.error('Split up fields are required');
      } else if (this.Tariff.is_for_driver_list == '-1' && this.Tariff.additional_splitup.length > 0) {
        this.toastr.error('Select split up applicable if split up is selected');
      } else {
        this.Tariff['company_id'] = localStorage.getItem('user_company');
        var encrypted = this.encDecService.nwt(this.session, this.Tariff);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._tariffService.addTariff(enc_data)
          .then((dec) => {
            if (dec) {
              if (dec.status === 200) {
                this.toastr.success('Tariff added successfully');
                setTimeout(() => {
                  this.router.navigate(['/admin/administration/tariffs']);
                }, 1000);
              } else if (dec.status === 201) {
                console.log(this.Tariff)
                this.toastr.error('Tariff addition failed.');
              } else if (dec.status === 204) {
                var res: any = this.encDecService.dwt(this.session, dec.data);
                let gModels = '';
                for (var i = 0; i < res.vehicle_models.length; i++)
                  gModels += res.vehicle_models[i] + ',';
                this.toastr.error('Vehicle model already has a general tariff for these vehicles, ' + gModels);
              }
              else {
              }
            }
          })
          .catch((error) => {
            console.log(error)
          });
      }
    } else {
      return;
    }
  }

  public checkPeekStartTime() {
    const ShiftpeekTimePicker = this.atp.open();
    ShiftpeekTimePicker.afterClose().subscribe(time => {
      this.Tariff.peek_time_start = time;
      this.Tariff.peek_time_end = '';
      this.showerror = false;
    });
  }

  public checkPeekEndTime() {
    const ShiftpeekEndPicker = this.atp.open();
    ShiftpeekEndPicker.afterClose().subscribe(time => {
      this.Tariff.peek_time_end = time;
      if (moment(this.Tariff.peek_time_end, 'HH:mm:ss') < moment(this.Tariff.peek_time_start, 'HH:mm:ss')) {
        this.PeekendError = 'Peek end time should be greater than Peek start time.';
        this.showerror = true;
      } else {
        this.showerror = false;
      }
    });
  }

  /**
   * To show field boxes on the basis of selection of is peek tariff field
   * @param data 
   */
  public ShowPeekTime(data) {
    if (data.target.value == '1') {
      this.showpeek = true;
    } else {
      this.showpeek = false;
      this.showerror = false;
      this.Tariff.peek_time_start = '';
      this.Tariff.peek_time_end = '';
    }
  }

  /**
  * To get Additional service For Dropdown
  *
  */
  public getadditionalservice() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._additional_service.getaddService(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.serviceData = data.getService;
        } else {
          console.log(dec.message)
        }
      }
    });
  }
  public getVehicleModels() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
      search: null
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._vehicleModelsService.getVehicleModels(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.vehicle_models = data.vehicleModelsList;
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  visible = true;
  selectable = true;
  removable = true;
  modelCtrl = new FormControl();
  models: string[] = [];

  remove(fruit): void {
    const index = this.models.indexOf(fruit);
    if (index >= 0) {
      this.Tariff.vehicle_model.splice(index, 1);
      this.models.splice(index, 1);
      this.vehicle_models.push(fruit)
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.models.push(event.option.value);
    this.Tariff.vehicle_model.push(event.option.value._id);
    const index = this.vehicle_models.indexOf(event.option.value);
    this.vehicle_models.splice(index, 1);
    this.modelCtrl.setValue(null);
    console.log(this.Tariff)
  }
  displayFnModels(data): string {
    return data ? '' : '';
  }
  checkTariff() {
    if (this.Tariff.tariff_type == 'FIX_RATE') {
      this.hideFields = true;
      this.Tariff.is_peek_tariff = '0';
      this.Tariff.start_price = '0';
      this.Tariff.price_per_distance = '0';
      this.Tariff.standing_price = '0';
      this.Tariff.denomination = '0';
      this.Tariff.included_in_start = '0';
      this.Tariff.distance_charge_unit = '0';
      this.Tariff.standing_speed_limit = '0';
      this.Tariff.minimum_fare = '0';
      this.showpeek = false;
      this.showerror = false;
      this.Tariff.peek_time_start = '';
      this.Tariff.peek_time_end = '';
      return true;
    }
    else {
      this.hideFields = false;
    }
    return false;
  }
  AdditionalSplitup() {
    this.dialog.closeAll();
    let data = {
      ops: "add",
      splitup: this.Tariff.additional_splitup,
      company_id: this.companyId.length > 0 ? this.companyId : [this.companyId]
    }
    let dialogRef = this.dialog.open(AdditionalSplitupComponent, {
      width: '700px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("Result++++" + JSON.stringify(result));
      this.Tariff.additional_splitup = result;
    });
  }
}