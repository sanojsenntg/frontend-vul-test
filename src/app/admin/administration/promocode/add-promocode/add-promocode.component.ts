import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { NG_VALIDATORS, AbstractControl, ValidatorFn, FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { PromocodeService } from '../../../../common/services/promocode/promocode.service';
import { CustomerService } from '../../../../common/services/customer/customer.service';
import { FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Numbervalidator } from '../../../../common/directive/numberdirective';
import * as moment from 'moment/moment';
import { ZoneService } from '../../../../common/services/zones/zone.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { PaymentTypeService } from '../../../../common/services/paymenttype/paymenttype.service';
@Component({
  selector: 'app-add-promocode',
  templateUrl: './add-promocode.component.html',
  styleUrls: ['./add-promocode.component.css']
})
export class AddPromocodeComponent implements OnInit {
  public max = new Date();
  public customerData;
  public CategoryData;
  public zones;
  public paymentTypeData;
  public zoneLength;
  public companyId: any = [];
  public promoCodeModel: any = {
    promo_name: '',
    promo_code: '',
    promo_type: 0,
    description: '',
    valid_from_date: '',
    valid_to_date: '',
    valid_from_time: '',
    valid_to_time: '',
    usage_count: 1,
    total_usage_count: '',
    customer_group_tag: [],
    //promo_valid_for_customers_tag: '',
    applicable_for_app_category: [],
    applicable_for_payment_types: [],
    maximum_discount_amount_upto: '',
    percentage_discount: '',
    min_order_amount: '',
    max_order_amount: '',
    pickup_applicable_zones: [],
    drop_applicable_zones: [],
    pickup_applicable_polygons: [],
    drop_applicable_polygons: [],
    is_auto_applicable: false,
    refno: '',
    company_id: localStorage.getItem('user_company'),
    promo_match_pick_or_drop: false
  };
  session;
  constructor(private _promocodeService: PromocodeService,
    private _customerService: CustomerService,
    private _paymentTypeService: PaymentTypeService,
    formBuilder: FormBuilder,
    private router: Router,
    private _vehicleModelsService: VehicleModelsService,
    public _zoneService: ZoneService,
    private _toasterService: ToastsManager,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    this._toasterService.setRootViewContainerRef(vcr);
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
  }


  public ngOnInit(): void {
    this.getCustomers();
    this.zoneLoad();
    this.getCategories();
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      key: 'customer',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.paymentTypeData = data.paymentTypes;
      }
    });
  }
  resetEndtime() {
    this.promoCodeModel.valid_to_date = '';
  }
  public zoneLoad() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._zoneService.getZones(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.zones = data.zones
          this.zoneLength = data.count;;
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  /**
  * Save promocode data
  *
  * @param formData
  */
  addPromoCode(formData): void {
    if (formData.valid) {
      if (this.promoCodeModel.maximum_discount_amount_upto == '0') {
        this._toasterService.error('Maximum amount cant be zero');
      } else if (this.promoCodeModel.percentage_discount == '0') {
        this._toasterService.error('Discount percentage cant be zero');
      } else if (this.promoCodeModel.min_order_amount == '0') {
        this._toasterService.error('Minimum order value cant be zero');
      } else if (this.promoCodeModel.max_order_amount == '0') {
        this._toasterService.error('Maximum order amount cant be zero');
      } else if (this.promoCodeModel.usage_count == '0') {
        this._toasterService.error('Customer usage cant be zero');
      } else if (this.promoCodeModel.total_usage_count == '0') {
        this._toasterService.error('Total usage cant be zero');
      }
      else if (this.promoCodeModel.valid_from_date == '') {
        this._toasterService.error('Please select a valid from date');
      } else if (this.promoCodeModel.valid_to_date == '') {
        this._toasterService.error('Please select a valid to date.');
      } else if (this.promoCodeModel.valid_from_time != '' && this.promoCodeModel.valid_to_time == '') {
        this._toasterService.error('Please select a valid to time');
      } else if (this.promoCodeModel.valid_to_time != '' && this.promoCodeModel.valid_from_time == '') {
        this._toasterService.error('Please select a valid from time.');
      } else if (this.promoCodeModel.promo_type == '1' && this.promoCodeModel.customer_group_tag.length == 0) {
        this._toasterService.error('Please select a customer group.');
      }
      else {
        this.promoCodeModel.valid_from_date = moment(this.promoCodeModel.valid_from_date).format('YYYY-MM-DD HH:mm:ss');
        this.promoCodeModel.valid_to_date = moment(this.promoCodeModel.valid_to_date).format('YYYY-MM-DD HH:mm:ss');
        if (this.promoCodeModel.valid_from_time != '') {
          this.promoCodeModel.valid_from_time = moment(this.promoCodeModel.valid_from_time).format('HH:mm:ss');
        }
        if (this.promoCodeModel.valid_to_time != '') {
          this.promoCodeModel.valid_to_time = moment(this.promoCodeModel.valid_to_time).format('HH:mm:ss');
        }
        if (this.promoCodeModel.pickup_applicable_zones.length > 0) {
          for (var i = 0; i < this.promoCodeModel.pickup_applicable_zones.length; ++i) {
            let index = this.zones.findIndex(x => x._id == this.promoCodeModel.pickup_applicable_zones[i]);
            if (index > -1) {
              this.promoCodeModel.pickup_applicable_polygons.push(this.zones[index].loc.coordinates[0]);
            }
          }
        }
        if (this.promoCodeModel.drop_applicable_zones.length > 0) {
          for (var j = 0; j < this.promoCodeModel.drop_applicable_zones.length; ++j) {
            let index1 = this.zones.findIndex(y => y._id == this.promoCodeModel.drop_applicable_zones[j]);
            if (index1 > -1) {
              this.promoCodeModel.drop_applicable_polygons.push(this.zones[index1].loc.coordinates[0]);
            }
          }
        }
        if (this.promoCodeModel.applicable_for_app_category.length == 0) {
          for (var j = 0; j < this.CategoryData.length; ++j) {
            this.promoCodeModel.applicable_for_app_category.push(this.CategoryData[j]._id);
          }
        }
        if (this.promoCodeModel.applicable_for_payment_types.length == 0) {
          for (var j = 0; j < this.paymentTypeData.length; ++j) {
            this.promoCodeModel.applicable_for_payment_types.push(this.paymentTypeData[j]._id);
          }
        }
        console.log(JSON.stringify(this.promoCodeModel));
        var encrypted = this.encDecService.nwt(this.session, this.promoCodeModel);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._promocodeService.addPromocode(enc_data)
          .then((dec) => {
            console.log(this.promoCodeModel);
            console.log(dec);
            if (dec.status === 200) {
              this._toasterService.success('Promocode added successfully.');
              this.router.navigate(['/admin/administration/promo-code']);
            } else if (dec.status === 201) {
              this._toasterService.error('Promocode addition failed.');
            } else if (dec.status === 203) {
              this._toasterService.warning('Promocode already exist.');
            } else {
              console.log(dec.message)
            }
          })
          .catch((error) => {
            this._toasterService.error('Promocode addition failed.');
          });
      }
    } else {
      this._toasterService.error('Please fill all fields');
      console.log(this.findInvalidControls(formData))
      console.log(formData)
      return;
    }
  }
  public getCustomers() {
    const params = {
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.fetchCustomerGroups(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.customerData = data.customergroup;
        } else {
          console.log(dec.message)
        }
      }
    });
  }
  public findInvalidControls(formData) {
    const invalid = [];
    const controls = formData.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }
  getCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CategoryData = data.vehicleModelsList;
      }
    });
  }

}
