import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListPromocodeComponent } from './list-promocode/list-promocode.component';
import { AddPromocodeComponent } from './add-promocode/add-promocode.component';
import { EditPromocodeComponent } from './edit-promocode/edit-promocode.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const PromocodeRoutes: Routes = [
     { path: '', component: ListPromocodeComponent, canActivate: [AclAuthervice], data: {roles: ["Promocodes"]}},
     { path: 'add', component: AddPromocodeComponent, canActivate: [AclAuthervice], data: {roles: ["Promocodes -Add"]}},
     { path: 'edit/:id' , component: EditPromocodeComponent, canActivate: [AclAuthervice], data: {roles: ["Promocodes -Edit"]}}
     
  ];
