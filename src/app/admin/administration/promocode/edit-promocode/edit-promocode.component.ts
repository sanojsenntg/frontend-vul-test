
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { PromocodeService } from '../../../../common/services/promocode/promocode.service';
import { ToastsManager } from 'ng2-toastr';
import { ZoneService } from '../../../../common/services/zones/zone.service';
import * as moment from 'moment/moment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { CustomerService } from '../../../../common/services/customer/customer.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { PaymentTypeService } from '../../../../common/services/paymenttype/paymenttype.service';

@Component({
  selector: 'app-edit-promocode',
  templateUrl: './edit-promocode.component.html',
  styleUrls: ['./edit-promocode.component.css']
})
export class EditPromocodeComponent implements OnInit {
  public companyId: any = [];
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public CategoryData;
  public paymentTypeData;
  public promoCodeModel: any = {
    promo_name: '',
    promo_code: '',
    promo_type: 0,
    description: '',
    valid_from_date: '',
    valid_to_date: '',
    valid_from_time: '',
    valid_to_time: '',
    usage_count: 1,
    total_usage_count: '',
    customer_group_tag: [],
    //promo_valid_for_customers_tag: '',
    applicable_for_app_category: [],
    applicable_for_payment_types: [],
    maximum_discount_amount_upto: '',
    percentage_discount: '',
    min_order_amount: '',
    max_order_amount: '',
    pickup_applicable_zones: [],
    drop_applicable_zones: [],
    pickup_applicable_polygons: [],
    drop_applicable_polygons: [],
    is_auto_applicable: false,
    refno: '',
    company_id: localStorage.getItem('user_company'),
    promo_match_pick_or_drop: false
  };
  public zones;
  public initialfromdate;
  public initialtodate;
  public initialfromtime;
  public initialtotime;
  public zoneLength;
  public max = new Date();
  customerData: any;
  session;
  constructor(private _promocodeService: PromocodeService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public jwtService: JwtService,
    private _paymentTypeService: PaymentTypeService,
    private _vehicleModelsService: VehicleModelsService,
    public _zoneService: ZoneService,
    public encDecService: EncDecService,
    private _toasterService: ToastsManager,
    private _customerService: CustomerService,
    private vcr: ViewContainerRef) {
  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.initialfromdate = true;
    this.initialtodate = true;
    this.initialfromtime = true;
    this.initialtotime = true;
    this.zoneLoad();
    this.getCustomers();
    this.getCategories();
    const params1 = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      key: 'customer',
      company_id: this.companyId
    };
    var encrypted1 = this.encDecService.nwt(this.session, params1);
    var enc_data1 = {
      data: encrypted1,
      email: localStorage.getItem('user_email')
    }
    this._paymentTypeService.getPaymentTypes(enc_data1).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.paymentTypeData = data.paymentTypes;
      }
    });
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._promocodeService.getPromocodeById(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        this.promoCodeModel.promo_name = res.PromocodeById.promo_name ? res.PromocodeById.promo_name : '';
        this.promoCodeModel.promo_code = res.PromocodeById.promo_code ? res.PromocodeById.promo_code : '';
        this.promoCodeModel.description = res.PromocodeById.description ? res.PromocodeById.description : '';
        this.promoCodeModel.discount = res.PromocodeById.discount ? res.PromocodeById.discount : '';
        this.promoCodeModel.promo_type = res.PromocodeById.promo_type ? res.PromocodeById.promo_type : '0';
        this.promoCodeModel.valid_from_date = res.PromocodeById.valid_from_date;
        this.promoCodeModel.valid_to_date = res.PromocodeById.valid_to_date;
        this.promoCodeModel.valid_from_time = res.PromocodeById.valid_from_time;
        this.promoCodeModel.valid_to_time = res.PromocodeById.valid_to_time;
        this.promoCodeModel.usage_count = res.PromocodeById.usage_count;
        this.promoCodeModel.total_usage_count = res.PromocodeById.total_usage_count;
        this.promoCodeModel.maximum_discount_amount_upto = res.PromocodeById.maximum_discount_amount_upto != -1 ? res.PromocodeById.maximum_discount_amount_upto : '';
        this.promoCodeModel.percentage_discount = res.PromocodeById.percentage_discount;
        this.promoCodeModel.min_order_amount = res.PromocodeById.min_order_amount != -1 ? res.PromocodeById.min_order_amount : '';
        this.promoCodeModel.max_order_amount = res.PromocodeById.max_order_amount != -1 ? res.PromocodeById.max_order_amount : '';
        this.promoCodeModel.pickup_applicable_zones = res.PromocodeById.pickup_applicable_zones;
        this.promoCodeModel.drop_applicable_zones = res.PromocodeById.drop_applicable_zones;
        this.promoCodeModel.refno = res.PromocodeById.promo_ref;
        this.promoCodeModel.customer_group_tag = res.promo_valid_for_customers_tag;
        this.promoCodeModel.applicable_for_app_category = res.PromocodeById.applicable_for_app_category;
        this.promoCodeModel.applicable_for_payment_types = res.PromocodeById.applicable_for_payment_types;
        this.promoCodeModel.is_auto_applicable = res.PromocodeById.is_auto_applicable;
        this.promoCodeModel.promo_match_pick_or_drop = res.PromocodeById.promo_match_pick_or_drop;
      }
    })
  }
  resetfromdate() {
    this.initialfromdate = false;
  }
  resettodate() {
    this.initialtodate = false;
  }
  resetfromtime() {
    this.initialfromtime = false;
    this.promoCodeModel.valid_from_time = '';
  }
  resettotime() {
    this.initialtotime = false;
    this.promoCodeModel.valid_to_time = '';
  }
  resetEndtime() {
    this.promoCodeModel.valid_to_date = '';
    this.initialtodate = false;
  }
  /**
   * To update the predefined message records
   */
  public updatePromocode(formData): void {
    console.log(this.promoCodeModel);
    if (this.promoCodeModel.maximum_discount_amount_upto == '0') {
      this._toasterService.error('Maximum amount cant be zero');
    } else if (this.promoCodeModel.percentage_discount == '0') {
      this._toasterService.error('Discount percentage cant be zero');
    } else if (this.promoCodeModel.min_order_amount == '0') {
      this._toasterService.error('Minimum order value cant be zero');
    } else if (this.promoCodeModel.max_order_amount == '0') {
      this._toasterService.error('Maximum order amount cant be zero');
    } else if (this.promoCodeModel.usage_count == '0') {
      this._toasterService.error('Customer usage cant be zero');
    } else if (this.promoCodeModel.total_usage_count == '0') {
      this._toasterService.error('Total usage cant be zero');
    }
    else if (this.promoCodeModel.valid_from_date == '') {
      this._toasterService.error('Please select a valid from date');
    } else if (this.promoCodeModel.valid_to_date == '') {
      this._toasterService.error('Please select a valid to date.');
    } else if (this.promoCodeModel.valid_from_time != '' && this.promoCodeModel.valid_to_time == '') {
      this._toasterService.error('Please select a valid to time');
    } else if (this.promoCodeModel.valid_to_time != '' && this.promoCodeModel.valid_from_time == '') {
      this._toasterService.error('Please select a valid from time.');
    } else if (this.promoCodeModel.promo_type == '1' && this.promoCodeModel.customer_group_tag.length == 0) {
      this._toasterService.error('Please select a customer group.');
    }
    else {
      this.promoCodeModel.valid_from_date = moment(this.promoCodeModel.valid_from_date).format('YYYY-MM-DD HH:mm:ss');
      this.promoCodeModel.valid_to_date = moment(this.promoCodeModel.valid_to_date).format('YYYY-MM-DD HH:mm:ss');
      if (this.promoCodeModel.valid_from_time != '' && !this.initialtotime) {
        this.promoCodeModel.valid_from_time = moment(this.promoCodeModel.valid_from_time).format('HH:mm:ss');
      }
      if (this.promoCodeModel.valid_to_time != '' && !this.initialfromtime) {
        this.promoCodeModel.valid_to_time = moment(this.promoCodeModel.valid_to_time).format('HH:mm:ss');
      }
      if (this.promoCodeModel.pickup_applicable_zones.length > 0) {
        for (var i = 0; i < this.promoCodeModel.pickup_applicable_zones.length; ++i) {
          let index = this.zones.findIndex(x => x._id == this.promoCodeModel.pickup_applicable_zones[i]);
          if (index > -1) {
            this.promoCodeModel.pickup_applicable_polygons.push(this.zones[index].loc.coordinates[0]);
          }
        }
      }
      if (this.promoCodeModel.drop_applicable_zones.length > 0) {
        for (var j = 0; j < this.promoCodeModel.drop_applicable_zones.length; ++j) {
          let index1 = this.zones.findIndex(y => y._id == this.promoCodeModel.drop_applicable_zones[j]);
          if (index1 > -1) {
            this.promoCodeModel.drop_applicable_polygons.push(this.zones[index1].loc.coordinates[0]);
          }
        }
      }
      if (this.promoCodeModel.applicable_for_app_category.length == 0) {
        for (var j = 0; j < this.CategoryData.length; ++j) {
          this.promoCodeModel.applicable_for_app_category.push(this.CategoryData[j]._id);
        }
      }
      if (this.promoCodeModel.applicable_for_payment_types.length == 0) {
        for (var j = 0; j < this.paymentTypeData.length; ++j) {
          this.promoCodeModel.applicable_for_payment_types.push(this.paymentTypeData[j]._id);
        }
      }
      console.log(JSON.stringify(this.promoCodeModel));
      this.promoCodeModel['_id'] = this._id;
      var encrypted = this.encDecService.nwt(this.session, this.promoCodeModel);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._promocodeService.updatePromocodeById(enc_data).then((dec) => {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        if (dec.status === 200) {
          this._toasterService.success('Promocode updated successfully.');
          setTimeout(() => {
            this.router.navigate(['/admin/administration/promo-code']);
          }, 1000);
        } else if (dec.status === 201) {
          this._toasterService.error('Promocode updation failed.');
        } else if (dec.status === 203) {
          this._toasterService.error('Promocode usage count already passed limit.current count :' + res.data);
        }
      })
        .catch((error) => {
          this._toasterService.error('Promocode updation failed.');
        });
    }
  }
  public zoneLoad() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at'
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._zoneService.getZones(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.zones = data.zones
          this.zoneLength = data.count;;
          console.log(this.zones)
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  public getCustomers() {
    const params = {
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.fetchCustomerGroups(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.customerData = data.customergroup;
        } else {
          console.log(dec.message)
        }
      }
    });
  }
  getCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CategoryData = data.vehicleModelsList;
      }
    });
  }
}







