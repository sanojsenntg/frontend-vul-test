import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AddPromocodeComponent } from './add-promocode/add-promocode.component';
import { ListPromocodeComponent } from './list-promocode/list-promocode.component';

import { PromocoderestService } from './../../../common/services/promocode/promocoderest.service';
import { PromocodeService } from './../../../common/services/promocode/promocode.service';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EditPromocodeComponent } from './edit-promocode/edit-promocode.component';
import { Numbervalidator } from './../../../common/directive/numberdirective';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {  MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule} from '@angular/material';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSelectModule,
    MatTooltipModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSlideToggleModule
  ],
  declarations: [AddPromocodeComponent, ListPromocodeComponent, EditPromocodeComponent, Numbervalidator],
  providers: [PromocodeService, PromocoderestService]
})
export class PromocodeModule { }
