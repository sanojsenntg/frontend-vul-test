import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { ToastsManager } from 'ng2-toastr';
import * as moment from 'moment/moment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { PartnerService } from '../../../../common/services/partners/partner.service';
import { FileHolder } from 'angular2-image-upload';
import { environment } from '../../../../../environments/environment';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-companies',
  templateUrl: './edit-companies.component.html',
  styleUrls: ['./edit-companies.component.css']
})
export class EditCompaniesComponent implements OnInit {
  public editForm: FormGroup;
  public companyId = [];
  public partnersData;
  private sub: Subscription;
  public _id;
  public spandata = 'yes';
  public new_image = false;
  public old_image;
  public session;
  public companyModel: any = {
    company_name: '',
    description: '',
    partners: [],
    payment: [],
    company_image: '',
    account_details: '',
    iban_number: '',
    commision: '',
    _id: '',
    company_type:1
  };
  public companyMail = localStorage.getItem('user_email');
  public imageext;
  public apiUrl;
  constructor(private _companyService: CompanyService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private _partnerService: PartnerService,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public jwtService: JwtService, public encDecService: EncDecService) {
  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'));
    this.session = localStorage.getItem('Sessiontoken');
    this.old_image = '';
    this.apiUrl = environment.apiUrl;
    this.imageext = window.localStorage['ImageExt'];
    this.getPartners();
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
      var data = {
        _id: params['id']
      }
      var encrypted = this.encDecService.nwt(this.session, data);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._companyService.getCompanyById(enc_data).then((dec) => {
        if (dec) {
           if (dec.status == 200) {
            var res: any = this.encDecService.dwt(this.session, dec.data);
            this.companyModel.company_name = res.CompanyById.company_name ? res.CompanyById.company_name : '';
            this.companyModel.description = res.CompanyById.description ? res.CompanyById.description : '';
            this.companyModel.partners = res.CompanyById.partners ? res.CompanyById.partners : [];
            this.companyModel.payment = res.CompanyById.payment_types ? res.CompanyById.payment_types : [];
            this.companyModel.account_details = res.CompanyById.account_details ? res.CompanyById.account_details : '';
            this.companyModel.iban_number = res.CompanyById.iban ? res.CompanyById.iban : '';
            this.companyModel.commision = res.CompanyById.commision ? res.CompanyById.commision : 0;
            this.old_image = res.CompanyById.company_image ? res.CompanyById.company_image : '';
            this.companyModel = res.CompanyById.company_type ? res.CompanyById.company_type : '';
          }
        }
      })
    });
  }
  public getPartners() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };

    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._partnerService.getPartner(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.partnersData = data.getPartners;
        }
      } else {
        console.log(dec.message)
      }

    });
  }
  public updateCompany(formData): void {
    console.log(this.companyModel);
    if (this.new_image) {

    } else {
      this.companyModel.company_image = '';
    }
    if (this.companyModel.payment.length > 0) {
      this.companyModel['_id'] = this._id
      var encrypted = this.encDecService.nwt(this.session, this.companyModel);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      console.log(this.companyModel)
      this._companyService.updateCompanyById(enc_data).then((dec) => {
        console.log(dec)
        if (dec) {
          if (dec.status === 200) {
            this._toasterService.success('Company updated successfully.');
            setTimeout(() => {
              this.router.navigate(['/admin/administration/companies']);
            }, 1000);
          } else {
            this._toasterService.error('Company updation failed.');
          }
        }
      })
        .catch((error) => {
          this._toasterService.error('Company updation failed.');
        });
    } else {
      this._toasterService.error("Must select a payment type");
    }
  }
  imageFinishedUploading(file: FileHolder) {
    this.companyModel.company_image = file.src;
    this.new_image = true;
    this.spandata = 'yes';
    // console.log(JSON.stringify(file.serverResponse));
  }

  onRemoved(file: FileHolder) {
    this.companyModel.company_image = '';
    this.spandata = 'no';
    this.new_image = false;
    // do some stuff with the removed file.
  }

  onUploadStateChanged(state: boolean) {
    console.log(JSON.stringify(state));
  }
}
