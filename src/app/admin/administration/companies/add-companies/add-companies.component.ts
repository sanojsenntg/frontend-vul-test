import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { NG_VALIDATORS, AbstractControl, ValidatorFn, FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Numbervalidator } from '../../../../common/directive/numberdirective';
import * as moment from 'moment/moment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { PartnerService } from '../../../../common/services/partners/partner.service';
import { FileHolder } from 'angular2-image-upload';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-companies',
  templateUrl: './add-companies.component.html',
  styleUrls: ['./add-companies.component.css']
})
export class AddCompaniesComponent implements OnInit {
  public partnersData;
  public spandata = 'no';
  public companyId = [];
  public companyModel: any = {
    company_name: '',
    description: '',
    company_image: '',
    partners: [],
    payment: [],
    account_details: '',
    iban_number: '',
    commision: 0,
    company_type: 1
  };
  public session;
  public companyMail

  constructor(private _companyservice: CompanyService,
    formBuilder: FormBuilder,
    private router: Router,
    private _toasterService: ToastsManager,
    private _partnerService: PartnerService,
    vcr: ViewContainerRef,
    public jwtService: JwtService,
    public encDecService: EncDecService) {
    this._toasterService.setRootViewContainerRef(vcr);

  }

  public ngOnInit(): void {
    this.companyMail = localStorage.getItem('user_email');
    this.companyId.push(localStorage.getItem('user_company'));
    this.session = localStorage.getItem('Sessiontoken');
    this.getPartners();
  }
  /**
  * Save promocode data
  *
  * @param formData
  */
  public getPartners() {
    const params = {
      offset: 0,
      //limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._partnerService.getPartner(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.partnersData = data.getPartners;
        }
      } else {
        console.log(dec.message)
      }

    });
  }
  addCompany(formData): void {
    if (formData.valid) {
      console.log(JSON.stringify(this.companyModel));
      if (this.companyModel.payment.length > 0) {
        var params = {
          company: formData.company,
          company_name: formData.value.company_name,
          description: formData.value.description,
          company_image: formData.value.company_image,
          partners: formData.value.partners,
          payment: formData.value.payment,
          account_details: formData.value.account_details,
          commision: formData.value.commision,
          iban_number: formData.value.iban_number,
          company_type: formData.value.company_type
        };
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        console.log(params);
        console.log(formData);
        this._companyservice.addCompany(enc_data)
          .then((dec) => {
            console.log(dec)
            if (dec) {
              if (dec.status === 200) {
                this._toasterService.success('Company added successfully.');
                this.router.navigate(['/admin/administration/companies']);
              } else if (dec.status === 201) {
                this._toasterService.error('Company addition failed.');
              } else if (dec.status === 203) {
                this._toasterService.warning('Company already exist.');
              }
            }
            else {
              this._toasterService.error('Company addition failed.');
            }
          })
          .catch((error) => {
            this._toasterService.error('Company addition failed.');
          });
      } else {
        this._toasterService.error("Payment type must be selected")
      }
    } else {
      console.log("invalid date" + formData)
      return;
    }
  }
  imageFinishedUploading(file: FileHolder) {
    console.log('state', file);
    this.companyModel.company_image = file.src;
    this.spandata = 'yes';
    //console.log(JSON.stringify(file.serverResponse));

  }

  onRemoved(file: FileHolder) {
    console.log(JSON.stringify(file));
    this.companyModel.company_image = '';
    this.spandata = 'no';
    // do some stuff with the removed file.
  }
}
