import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { AddCompaniesComponent } from './add-companies/add-companies.component';
import { EditCompaniesComponent } from './edit-companies/edit-companies.component';
import { ListCompaniesComponent } from './list-companies/list-companies.component';
import { ImageUploadModule } from 'angular2-image-upload';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { Numbervalidator } from './../../../common/directive/numberdirective';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CompanyService } from './../../../common/services/companies/companies.service';
import { CompanyrestService } from './../../../common/services/companies/companiesrest.service';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSelectModule,
    ImageUploadModule,
    MatTooltipModule
  ],
  providers: [CompanyService, CompanyrestService],
  declarations: [AddCompaniesComponent, EditCompaniesComponent, ListCompaniesComponent]
})
export class CompaniesModule { }
