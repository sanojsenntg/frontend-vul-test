import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyService } from '../../../../common/services/companies/companies.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { environment } from '../../../../../environments/environment';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-companies',
  templateUrl: './list-companies.component.html',
  styleUrls: ['./list-companies.component.css']
})
export class ListCompaniesComponent implements OnInit {
  key: string = '';
  itemsPerPage = 10;
  pageNo = 0;
  public is_search = false;
  public _id;
  public searchSubmit = false;
  sortOrder = 'asc';
  public keyword;
  public CompanyLength;
  public CompanyData;
  public searchLoader = false;
  public del = false;
  public code = '';
  public imageext;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public apiUrl;
  public companyId = [];
  public companyMail = localStorage.getItem('user_email');
  public session;
  constructor(
    private router: Router,
    public jwtService: JwtService,
    public dialog: MatDialog,
    public overlay: Overlay,
    public toastr: ToastsManager,
    public _CompanyService: CompanyService,
    public _aclService: AccessControlService,
    public encDecService: EncDecService) {
    this.companyMail = localStorage.getItem('user_email');
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.aclDisplayService();
  }

  ngOnInit() {
    this.apiUrl = environment.apiUrl;
    this.imageext = window.localStorage['ImageExt'];
    this.searchSubmit = true;
    var params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._CompanyService.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CompanyLength = data.count;
          this.CompanyData = data.getCompanies;
        }
      }
      this.searchSubmit = false;
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Companies-Add") {
              this.aclAdd = true;
            } else if (data.menu[i] == "Companies-Edit") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Companies-Delete") {
              this.aclDelete = true;
            }
          };
        }
      } else {
        console.log(dec.message)
      }
    })
  }
  public refreshCompany() {
    if (this.is_search == true) {
      this.getCompanies(this.keyword);
    }
    else {
      this.ngOnInit();
    }
  }

  public reset() {
    this.is_search = false;
    this.code = '';
    this.ngOnInit();
  }

  /**
  * Confirmation popup for deleting particular predefined message record
  * @param id 
  */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deleteCompany(id);
      } else {
      }
    });
  }

  /**
   * Delete predefined message record from view
   * @param id 
   */
  public deleteCompany(id) {
    var params = {
      _id: id,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._CompanyService.updateDeletedStatus(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          this.refreshCompany();
          this.CompanyData.splice(id, 1);
          this.toastr.success('Company deleted successfully.');
        } else if (dec.status == 201) {
          this.toastr.success('Company deletion failed.');
        }
      } else {
      }
    });
  }

  /**
   * For sorting predefined meesage records 
   * @param key 
   */
  sort(key) {
    this.searchLoader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    let params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        keyword: this.keyword ? this.keyword : '',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);

    }
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._CompanyService.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CompanyLength = data.count;
          this.CompanyData = data.getCompanies;
          this.searchSubmit = false;
        }
      } else {
        console.log(dec.message)
      }

    });
  }

  /**
  * Pagination for predefined message module
  * @param data 
  */
  pagingAgent(data) {
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    let params;
    if (this.is_search == true) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        keyword: this.keyword ? this.keyword : '',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
    }
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._CompanyService.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CompanyData = data.getCompanies;
        }
      } else {
        console.log(dec.message)
      }
    });
  }

  /**
   * For searching payment extra records
   * @param keyword 
   */
  public getCompanies(keyword) {
    this.is_search = true;
    this.searchSubmit = true;
    this.keyword = keyword;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      keyword: this.keyword,
      company_id: this.companyId
    };

    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._CompanyService.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CompanyLength = data.count;
          this.CompanyData = data.getCompanies;
          this.searchSubmit = false;
        }
      } else {
        console.log(dec.message)
      }

    });
  }

}
