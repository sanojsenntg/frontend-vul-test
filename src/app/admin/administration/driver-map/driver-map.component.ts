import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { ZoneService } from '../../../common/services/zones/zone.service';
import { ToastsManager } from 'ng2-toastr';
import { VehicleService } from '../../../common/services/vehicles/vehicle.service';
import { VehicleModelsService } from '../../../common/services/vehiclemodels/vehiclemodels.service';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-driver-map',
  templateUrl: './driver-map.component.html',
  styleUrls: ['./driver-map.component.css']
})
export class DriverMapComponent implements OnInit {

  title = 'zone';
  positions = [];
  marker;
  map;
  timer;
  count = 0;
  cardata:any;
  carPos;
  center = '25.2048,55.2708';
  radius:number = 0;
  zoom:number = 13
  carCord = [];
  markerArr = [];
  name = new FormControl(3000);
  companyId = [];
  session;
  allOrderIddata;
  orderId;
  assignedDevices;
  searchSubmit = false;
  vehicleModelData;
  vehcleCatId;
  booksByStoreID;
  vehicleCategory;
  tripId;
  t4Selection;
  iwMarker;
  checkDate;
  orderCtrl: FormControl = new FormControl();
  constructor(public toastr: ToastsManager, public encDecService: EncDecService, public zoneSevice:ZoneService,  private _vehicleService: VehicleService,public _vehicleModelsService:VehicleModelsService){
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
  }
  
  ngOnInit(){
    this.getVehicleCategList();
    this.name.valueChanges.subscribe(
      name => {
        this.radius = name;
        if(this.radius == 3000){
          this.zoom = 13
        }else if(this.radius == 6000){
          this.zoom = 12
        }else{
          this.zoom = 11
        }
      }
    ); 
  }
 
  getVehicleCategList() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleCategory = data.vehicleModelsList;
      }
    });
  }
  public driverList() {
    this.searchSubmit = true;
    this.radius = 0;
    this.positions = [];
    this.markerHide();
    const params = {
      unique_order_id:this.orderCtrl.value,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }

    this.zoneSevice.driverMaping(enc_data).then((dec) => {
      this.searchSubmit = false;
      if(dec){
        if(dec.status == 200){
          this.tripId = this.orderCtrl.value;
          this.carCord = [];
          this.radius = 3000;
          var data: any = this.encDecService.dwt(this.session,dec.data);
          console.log(data);
          this.center = data.data[0].customer_selected_pickup_lat_long;
          this.assignedDevices = data.data[0].assigned_drivers;
          this.carPos = data.data[0].deviceLocationHistory[0].device_location_data;
          this.positions.push(data.data[0].customer_selected_pickup_lat_long);
          this.vehcleCatId = data.data[0].vehicle_category_ids;
          this.checkDate = moment(data.data[0].deviceLocationHistory[0].created_at).utc().format('YYYY-MM-DD HH:mm:ss');
          let t4Ind = this.vehicleCategory.findIndex(x=> x.unique_category_id == data.data[0].t4[0] )
          if(t4Ind > -1 ){
            this.t4Selection = this.vehicleCategory[t4Ind]
          }
          for(let i=0; i<this.carPos.length; i++){
            if(this.carPos[i].driver_id){ 
              let index = this.assignedDevices.findIndex(x=>
                x.device_id == this.carPos[i].device_id)
              if(index > -1) {
                this.carCord.push({assigned:'assigned_vehicle',latLng:new google.maps.LatLng(this.carPos[i].location.coordinates[1],this.carPos[i].location.coordinates[0]), vehicle_id:this.carPos[i].vehicle_id,date:this.checkDate, trip_status: this.carPos[i].trip_status, vehicle_details:this.carPos[i]})
              }else if(this.carPos[i].t4[0] == data.data[0].t4[0]){
                var d1:any = moment(this.carPos[i].updated_at).format('YYYY-MM-DD HH:mm:ss');
                var d2:any = this.checkDate;
                var duration = moment.duration(moment(d2).diff(d1));
               if(duration.asMinutes() > 3){
                this.carCord.push({assigned:'offline',latLng: new google.maps.LatLng(this.carPos[i].location.coordinates[1],this.carPos[i].location.coordinates[0]), vehicle_id:this.carPos[i].vehicle_id, trip_status: this.carPos[i].trip_status !=''? this.carPos[i].trip_status : 'Offline Vehicle', date:this.checkDate, vehicle_details:this.carPos[i] })
               }else{
                this.carCord.push({assigned:'online',latLng: new google.maps.LatLng(this.carPos[i].location.coordinates[1],this.carPos[i].location.coordinates[0]), vehicle_id:this.carPos[i].vehicle_id, trip_status: this.carPos[i].trip_status !=''? this.carPos[i].trip_status : 'Free Vehicle', date:this.checkDate, vehicle_details:this.carPos[i]})
               }
                
              }
            }
          }
          this.searchSubmit = false;
          this.pushMark();
        }else if(dec.status == 203){
          this.toastr.error('Not a customer app order !');
        }else{
          this.toastr.error('No such order found !');
        }
      }
    });
  
  }

  

  onMapReady(map) {
    this.map = map; 
  }
  public infoWindow;
  pushMark(){
    for(let i=0; i<this.carCord.length;i++){
      if(this.carCord[i].assigned == 'assigned_vehicle'){
        var custIcon = 'assets/img/map/almost_free40.png'
      }else if(this.carCord[i].assigned == 'offline'){
        var custIcon = 'assets/img/map/paused(break)20.png'
      }else if(this.carCord[i].assigned == 'online' && this.carCord[i].trip_status == 'trip_started'){
        var custIcon = 'assets/img/map/accepted20.png'
      }else if(this.carCord[i].assigned == 'online' && this.carCord[i].trip_status == 'Free Vehicle'){
        var custIcon = 'assets/img/map/signed_out20.png'
      }else if(this.carCord[i].assigned == 'online' && this.carCord[i].trip_status != ''){
        var custIcon = 'assets/img/map/accepted20.png'
      }else{
        var custIcon = 'assets/img/map/signed_out20.png'
      }
      var marker = new google.maps.Marker({
       position: this.carCord[i].latLng,
       map: this.map,
       icon: custIcon
     });
     this.markerArr.push(marker);
     marker.addListener('click',// this.showArrays);
     () => {
      console.log(this.carCord[i]);
      this.showArrays(marker,this.carCord[i]);
    });
    }
    this.infoWindow = new google.maps.InfoWindow();
  }
  public markerHide(){
    for(let i=0; i<this.markerArr.length;i++){
      this.markerArr[i].setMap(null);
    }
  }
  public showArrays(event,cor){
    const params = {
      _id: cor.vehicle_id,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._vehicleService.getById(enc_data).then(dec => {
      var data: any = this.encDecService.dwt(this.session, dec.data);
      data['status'] = cor.trip_status =='' ? "Free Vehicle":cor.trip_status;
      console.log(data);
      var info = '<div id="content">'+
                  '<div id="siteNotice">'+
                  '<h5>'+data.SingleDetail.display_name +' </h5>'+
                  '</div>'+
                  '<h6> Vehicle Status: '+ data.status+'</h6>'+
                  '<div id="bodyContent">'+
                  '</div>'+
                  '</div>';
      this.infoWindow.setContent(info)
      this.infoWindow.setPosition(cor.latLng);
      this.infoWindow.open(this.map);
    })
  }
  onIdle(event) {
  }
  onMarkerInit(marker) {
    this.iwMarker = marker;
    marker.nguiMapComponent.openInfoWindow("iw", marker);
  }
  onMapClick(event) {
  }
  clicked({target: marker}) {
    marker.nguiMapComponent.openInfoWindow('iw', marker);
  }
  hideMarkerInfo() {
  }
  public loopinFn(data){
    let locHistory = data.data[0].deviceLocationHistory;
    setInterval(()=>{
      var i=0;
      if(i<locHistory.length){
        for(var k=0; k<locHistory[i].device_location_data.length; k++){
          if(locHistory[i].device_location_data[k].driver_id && locHistory[i+1].device_location_data[k].driver_id){
            var mark1 = [];
            let mark2 = [];
            var posLat = locHistory[i].device_location_data[k].latitude;
            var posLng = locHistory[i].device_location_data[k].longitude;
            var tarLat = locHistory[i+1].device_location_data[k].latitude;
            var tarLng = locHistory[i+1].device_location_data[k].longitude;
            mark1.push(posLat,posLng);
            mark2.push(tarLat,tarLng);
          }
        }
        i = i+1;
      }else{
        i = 0;
      }
    },300)
  }

  Wait3(i) {
    return new Promise(r => setTimeout(r, i * 30));
  }

  slideMarker(marker, target, count) {
    let steps = 20;
    let delay = 20;
    var i = 0;
    var position = [marker[0], marker[1]];
    let deltaLat = (target[0] - position[0]) / steps;
    let deltaLng = (target[1] - position[1]) / steps;
    var rotation = Math.atan2(deltaLng, deltaLat) * (180 / Math.PI);
    var img = new Image();
    img.src = 'assets/img/map/almost_free20.png';
    img.onload = () => {
      let chain = Promise.resolve();
      let icon;
      chain
        .then(() => {
          icon = new RotateIcon({ img: img, pixel: 20 })
            .setRotation({ deg: rotation })
            .getUrl();
        })
        .then(() => this.Wait3(count * 3))
        .then(() => {
          this.marker.setIcon(icon);
          var moveMarker = function () {
            position[0] += deltaLat;
            position[1] += deltaLng;
            var newPosition = new google.maps.LatLng(position[0], position[1]);
            this.marker.setPosition(newPosition);
            if (i != steps) {
              i++;
              setTimeout(moveMarker, delay);
            }
          };
          moveMarker();
        });
    };
  }
  
}

class RotateIcon {
  options: any;
  img: HTMLImageElement;
  context: CanvasRenderingContext2D;
  canvas: any;

  constructor(options) {
    this.options = options;
    this.img = options.img.cloneNode();
    this.options.width = options.pixel;
    this.options.height = options.pixel;
    let canvas = document.createElement("canvas");
    canvas.width = this.options.width;
    canvas.height = this.options.height;
    this.context = canvas.getContext("2d");
    this.canvas = canvas;
  }

  setRotation(options) {
    let angle = options.deg ? (options.deg * Math.PI) / 180 : options.rad;
    let centerX = this.options.width / 2;
    let centerY = this.options.height / 2;
    this.context.clearRect(0, 0, this.options.width, this.options.height);
    this.context.save();
    this.context.translate(centerX, centerY);
    this.context.rotate(angle);
    this.context.translate(-centerX, -centerY);
    this.context.drawImage(this.img, 0, 0);
    this.context.restore();
    return this;
  }
  getUrl() {
    return this.canvas.toDataURL("image/png");
  }
}
