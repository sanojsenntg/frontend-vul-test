import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { ListRolesComponent } from './list-roles/list-roles.component';
import { AddRolesComponent } from './add-roles/add-roles.component';

import { RolesrestService } from './../../../common/services/roles/rolesrest.service';
import { RolesService } from './../../../common/services/roles/roles.service';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EditRolesComponent } from './edit-roles/edit-roles.component';
//import { Numbervalidator }  from './../../../common/directive/numberdirective';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    MatSelectModule, 
    MatTooltipModule
  ],
  declarations: [ ListRolesComponent,AddRolesComponent,EditRolesComponent],
  providers:[RolesService,RolesrestService]
})
export class RolesModule { }
