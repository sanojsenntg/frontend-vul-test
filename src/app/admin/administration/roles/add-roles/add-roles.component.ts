import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { NG_VALIDATORS, AbstractControl, ValidatorFn, FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { RolesService } from '../../../../common/services/roles/roles.service';
import { FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Numbervalidator } from '../../../../common/directive/numberdirective';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
@Component({
  selector: 'app-add-roles',
  templateUrl: './add-roles.component.html',
  styleUrls: ['./add-roles.component.css']
})
export class AddRolesComponent implements OnInit {
  public companyId: any = [];
  public max = 100;
  public usersModel: any = {
    firstname: '',
    lastname: '',
    email: '',
    phonenumber: '',
    password: ''
  };
  session;
  constructor(private _rolesService: RolesService,
    formBuilder: FormBuilder,
    private router: Router,
    private _toasterService: ToastsManager,
    public jwtService: JwtService,
    public encDecService: EncDecService
  ) {
  }

  public ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    let userstoredata = JSON.parse(window.localStorage['adminUser']);
    if (userstoredata.role == "1") {
    } else {
      this.router.navigate(['/admin/dashboard']);
      return false;
    }
  }

  /**
  * Save promocode data
  *
  * @param formData
  */
  addUsers(formData): void {
    if (formData.valid) {
      this.usersModel['company_id'] = localStorage.getItem('user_company');
      var encrypted = this.encDecService.nwt(this.session, this.usersModel);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._rolesService.addUsers(enc_data)
        .then((dec) => {
          if (dec && dec.status === 200) {
            this._toasterService.success('User added successfully.');
            this.router.navigate(['/admin/administration/roles']);
          } else if (dec && dec.status === 203) {
            this._toasterService.warning('User is already exist.');
          } else
            this._toasterService.error('User addition failed.');
        })
    } else {
      return;
    }
  }


}
