
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { RolesService } from '../../../../common/services/roles/roles.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-roles',
  templateUrl: './edit-roles.component.html',
  styleUrls: ['./edit-roles.component.css']
})
export class EditRolesComponent implements OnInit {

  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public companyId: any = [];
  session;
  constructor(private _rolesService: RolesService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public encDecService: EncDecService,
    public jwtService: JwtService) {
    this.editForm = formBuilder.group({
      firstname: ['', [
        Validators.required]],
      lastname: ['', [
        Validators.required]],
      email: ['', [
        Validators.required]],
      password: ['', [
        Validators.required]],
      phonenumber: ['', [
        Validators.required]],
      role: ['', [
        Validators.required,
      ]]

    });
  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    let userstoredata = JSON.parse(window.localStorage['adminUser']);
    if (userstoredata.role == "1") {
    } else {
      this.router.navigate(['/admin/dashboard']);
      return false;
    }

    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._rolesService.getUserById(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          this.editForm.setValue({
            firstname: res.userById.firstname ? res.userById.firstname : '',
            lastname: res.userById.lastname ? res.userById.lastname : '',
            email: res.userById.email ? res.userById.email : '',
            password: '',
            phonenumber: res.userById.phone_number ? res.userById.phone_number : '',
            role: res.userById.role ? res.userById.role : '',
          });
        } else {
          console.log(dec.message)
        }
      }
    })
  }

  /**
   * To update the predefined message records
   */
  public updateUser() {
    if (!this.editForm.valid) {

      this._toasterService.error('All Fields are required.');

    } else {
      const promocodeData = {
        firstname: this.editForm.value.firstname,
        lastname: this.editForm.value.lastname,
        email: this.editForm.value.email,
        password: this.editForm.value.password,
        phonenumber: this.editForm.value.phonenumber,
        role: this.editForm.value.role,
        company_id: localStorage.getItem('user_company'),
        _id: this._id
      }
      var encrypted = this.encDecService.nwt(this.session, promocodeData);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._rolesService.updateuserById(enc_data)
        .then((dec) => {
          if (dec.status === 200) {
            this._toasterService.success('User updated successfully.');
            this.router.navigate(['/admin/administration/roles']);
          } else if (dec.status === 201) {
            this._toasterService.error('User updation failed.');
          } else if (dec.status === 203) {
            this._toasterService.warning('User is already exist.');
          }
        })
        .catch((error) => {

          this._toasterService.error('User updation failed.');
        });
    }
  }
}







