import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPromocodeComponent } from './list-promocode.component';

describe('ListPromocodeComponent', () => {
  let component: ListPromocodeComponent;
  let fixture: ComponentFixture<ListPromocodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPromocodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPromocodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
