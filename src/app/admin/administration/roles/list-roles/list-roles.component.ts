import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { RolesService } from '../../../../common/services/roles/roles.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-list-roles',
  templateUrl: './list-roles.component.html',
  styleUrls: ['./list-roles.component.css'],
  providers: [RolesService]
})

export class ListRolesComponent implements OnInit {
  public companyId: any = [];
  key: string = '';
  itemsPerPage = 10;
  pageNo = 0;
  public is_search = false;
  public _id;
  public searchSubmit = false;
  sortOrder = 'asc';
  public keyword;
  public usersLength;
  public usersData;
  public searchLoader = false;
  public del = false;
  public code = '';
  public session;

  constructor(
    private router: Router,
    public jwtService: JwtService,
    public dialog: MatDialog,
    public overlay: Overlay,
    public toastr: ToastsManager,
    public _rolesService: RolesService,
    public encDecService: EncDecService
  ) {
  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    //console.log('list role',window.localStorage['adminUser']);
    let userstoredata = JSON.parse(window.localStorage['adminUser']);
    if (userstoredata.role == "1") {
    } else {
      this.router.navigate(['/admin/dashboard']);
      return false;
    }

    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._rolesService.getUsersListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.usersLength = data.count;
          for (var k = 0; k < data.getListAllRolesUsers.length; k++) {
            if (data.getListAllRolesUsers[k].role == '2') {
              data.getListAllRolesUsers[k].role_name = "dispatcher";
            } else if (data.getListAllRolesUsers[k].role == '3') {
              data.getListAllRolesUsers[k].role_name = "admin";
            } else if (data.getListAllRolesUsers[k].role == '4') {
              data.getListAllRolesUsers[k].role_name = "fleetmanager";
            }
          }
          this.usersData = data.getListAllRolesUsers;
        } else {
          console.log(dec.message)
        }
      }
      this.searchSubmit = false;
    });
  }

  public refreshPromocode() {
    if (this.is_search == true) {
      this.getUers(this.keyword);
    }
    else {
      this.ngOnInit();
    }
  }

  /**
  * Confirmation popup for deleting particular predefined message record
  * @param id 
  */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deleteUser(id);
      } else {
      }
    });
  }

  /**
   * Delete predefined message record from view
   * @param id 
   */
  public deleteUser(id) {
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._rolesService.updateDeletedStatus(enc_data).then((dec) => {
      if (dec) {
        if (res.status === 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          this.refreshPromocode();
          this.usersData.splice(id, 1);
          this.toastr.success('Promocode deleted successfully.');
        } else if (res.status === 201) {
          this.toastr.success('Promocode deletion failed.');
        }
      } else {
        console.log(dec.message)
      }
    });
  }

  public reset() {
    this.is_search = false;
    this.code = '';
    this.ngOnInit();
  }

  /**
   * For searching role users
   * @param keyword 
   */
  public getUers(keyword) {
    this.is_search = true;
    this.searchSubmit = true;
    this.keyword = keyword;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      keyword: this.keyword,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._rolesService.getUsersListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.usersLength = data.count;
          for (var k = 0; k < data.getListAllRolesUsers.length; k++) {
            if (data.getListAllRolesUsers[k].role == '2') {
              data.getListAllRolesUsers[k].role_name = "dispatcher";
            } else if (data.getListAllRolesUsers[k].role == '3') {
              data.getListAllRolesUsers[k].role_name = "admin";
            } else if (data.getListAllRolesUsers[k].role == '4') {
              data.getListAllRolesUsers[k].role_name = "fleetmanager";
            }
          }
          this.usersData = data.getListAllRolesUsers;
        }
      } else {
        console.log(dec.message)
      }
      this.searchSubmit = false;
    });
  }

  /**
 * Pagination for predefined message module
 * @param data 
 */
  pagingAgent(data) {
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    let params;
    if (this.is_search == true) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : '_id',
        keyword: this.keyword ? this.keyword : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : '_id',
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._rolesService.getUsersListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.usersLength = data.count;
          for (var k = 0; k < data.getListAllRolesUsers.length; k++) {
            if (data.getListAllRolesUsers[k].role == '2') {
              data.getListAllRolesUsers[k].role_name = "dispatcher";
            } else if (data.getListAllRolesUsers[k].role == '3') {
              data.getListAllRolesUsers[k].role_name = "admin";
            } else if (data.getListAllRolesUsers[k].role == '4') {
              data.getListAllRolesUsers[k].role_name = "fleetmanager";
            }
          }
          this.usersData = data.getListAllRolesUsers;
        }
      } else {
        console.log(dec.message)
      }

    });
  }

  /**
   * For sorting predefined meesage records 
   * @param key 
   */
  sort(key) {
    this.searchLoader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    let params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        keyword: this.keyword ? this.keyword : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._rolesService.getUsersListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.usersLength = data.count;
          for (var k = 0; k < data.getListAllRolesUsers.length; k++) {
            if (data.getListAllRolesUsers[k].role == '2') {
              data.getListAllRolesUsers[k].role_name = "dispatcher";
            } else if (data.getListAllRolesUsers[k].role == '3') {
              data.getListAllRolesUsers[k].role_name = "admin";
            } else if (data.getListAllRolesUsers[k].role == '4') {
              data.getListAllRolesUsers[k].role_name = "fleetmanager";
            }
          }
          this.usersData = data.getListAllRolesUsers;
        }
      } else {
        console.log(dec.message)
      }
      this.searchSubmit = false;
    });
  }


}
