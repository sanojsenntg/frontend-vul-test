import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';

import { PredefinedmessageService } from './../../../common/services/predefinedmessage/predefinedmessage.service';
import { PredefinedmessagerestService } from './../../../common/services/predefinedmessage/predefinedmessagerest.service';
import { MatSelectModule } from '@angular/material/select';
import { ListPredefinedMessagesComponent } from './list-predefined-messages/list-predefined-messages.component';
import { AddPredefinedMessagesComponent } from './add-predefined-messages/add-predefined-messages.component';
import { EditPredefinedMessagesComponent } from './edit-predefined-messages/edit-predefined-messages.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    Ng2OrderModule,
    NgbModule,
    MatTooltipModule,
    MatSelectModule
  ],
  declarations: [ 
    ListPredefinedMessagesComponent,
    AddPredefinedMessagesComponent,
    EditPredefinedMessagesComponent
  ],
  providers: [ PredefinedmessageService, PredefinedmessagerestService ]
})
export class PredefinedMessagesModule { }
