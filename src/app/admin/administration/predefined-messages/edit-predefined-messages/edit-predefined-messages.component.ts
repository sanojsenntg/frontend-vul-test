import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { PredefinedmessageService } from '../../../../common/services/predefinedmessage/predefinedmessage.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-predefined-messages',
  templateUrl: './edit-predefined-messages.component.html',
  styleUrls: ['./edit-predefined-messages.component.css']
})
export class EditPredefinedMessagesComponent implements OnInit {
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public companyId = [];
  public session;
  constructor(private _predefinedmessageService: PredefinedmessageService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public jwtService: JwtService,
    private _toasterService: ToastsManager,
    public encDecService:EncDecService,
    private vcr: ViewContainerRef) {
    this.editForm = formBuilder.group({
      company: ['', [
        Validators.required]],
      message_type: ['', [
        Validators.required]],
      message_content: ['', [
        Validators.required]]
    });
  }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');

    this.editForm.controls['company'].disable();
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    const params = {
      company_id: this.companyId,
      _id:this._id
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._predefinedmessageService.getPredefinedMessagesById(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var res:any = this.encDecService.dwt(this.session,dec.data);
          this.editForm.setValue({
            company: res.PredefinedMessage.company ? res.PredefinedMessage.company : '',
            message_type: res.PredefinedMessage.message_type ? res.PredefinedMessage.message_type : '',
            message_content: res.PredefinedMessage.message_content ? res.PredefinedMessage.message_content : ''
          });
        }else{
          console.log(dec.message)
        }
      }
      });
  }

  /**
   * To update the predefined message records
   */
  public updatePredefinedMessages() {
    if (!this.editForm.valid) {
      return;
    }
    const predefinedMessagesData = {
      message_type: this.editForm.value.message_type,
      message_content: this.editForm.value.message_content,
      _id: this._id,
      created_at: Date.now(),
      company_id:localStorage.getItem('user_company')
    }
    var encrypted = this.encDecService.nwt(this.session,predefinedMessagesData);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
    }
    console.log(predefinedMessagesData);
    this._predefinedmessageService.updatePredefinedMessages(enc_data).then((dec) => {
        if (dec.status === 200) {
          this._toasterService.success('Predefined messages updated successfully.');
          this.router.navigate(['/admin/administration/predefined-messages']);
        } else if (dec.status === 201) {
          this._toasterService.error('Predefined messages updation failed.');

        }
        this.router.navigate(['/admin/administration/predefined-messages']);
      })
      .catch((error) => {
        this.router.navigate(['/login']);
      });
  }
}






