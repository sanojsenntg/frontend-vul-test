import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PredefinedmessageService } from '../../../../common/services/predefinedmessage/predefinedmessage.service';
import { FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-predefined-messages',
  templateUrl: './add-predefined-messages.component.html',
  styleUrls: ['./add-predefined-messages.component.css']
})
export class AddPredefinedMessagesComponent implements OnInit {
  public companyId = [];
  public predefinedMessageModel: any = {
    company: '',
    message_type: '',
    message_content: '',
    company_id: localStorage.getItem('user_company')
  };
  session;

  constructor(private _predefinedmessageService: PredefinedmessageService,
    formBuilder: FormBuilder,
    private router: Router,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public encDecService:EncDecService,
    public jwtService: JwtService) { 
    }

  public ngOnInit(): void {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
  }

  /**
  * Save predefined message data
  *
  * @param formData
  */
  addPredefinedMessage(formData): void {
    if (formData.valid) {
    
      var encrypted = this.encDecService.nwt(this.session,this.predefinedMessageModel);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._predefinedmessageService.savePredefinedMessages(enc_data)
        .then((dec) => {
          if (dec && dec.status === 200) {
            this._toasterService.success('Predefined message added successfully.');
            this.router.navigate(['/admin/administration/predefined-messages']);
          } else{
            this._toasterService.error('Predefined message addition failed.');
          }
        })
    } else {
      return;
    }
  }
}
