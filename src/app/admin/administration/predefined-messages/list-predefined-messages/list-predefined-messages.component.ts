import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PredefinedmessageService } from '../../../../common/services/predefinedmessage/predefinedmessage.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-predefined-messages',
  templateUrl: './list-predefined-messages.component.html',
  styleUrls: ['./list-predefined-messages.component.css']
})

export class ListPredefinedMessagesComponent implements OnInit {
  companyId: any = [];
  public predefinedMessageData;
  public id;
  public is_search = false;
  public message_content = '';
  itemsPerPage = 10;
  pageNo = 0;
  sortOrder = 'asc';
  key = '';
  del = false;
  predefinedMessageLength;
  searchLoader = false;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  session;
  constructor(private _predefinedmessageService: PredefinedmessageService,
    private router: Router,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    public dialog: MatDialog,
    public overlay: Overlay,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public encDecService: EncDecService,
    public _aclService: AccessControlService) {

  }

  /**
   * Function which runs first when the page loads
   */
  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.aclDisplayService();
    this.is_search = false;
    this.searchLoader = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._predefinedmessageService.getPredefinedMessages(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.predefinedMessageData = data.predefineMessages;
        this.predefinedMessageLength = data.totalCount;
      }
      this.searchLoader = false;
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      var data: any = this.encDecService.dwt(this.session, dec.data);
      for (let i = 0; i < data.menu.length; i++) {
        if (data.menu[i] == "Predefined Messages - Add") {
          this.aclAdd = true;
        } else if (data.menu[i] == "Predefined Messages - Edit") {
          this.aclEdit = true;
        } else if (data.menu[i] == "Predefined Messages - Delete") {
          this.aclDelete = true;
        }
      };
    })
  }
  /**
  * Confirmation popup for deleting particular predefined message record
  * @param id 
  */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deletePredefinedMessage(id);
      } else {
      }
    });
  }

  /**
   * Delete predefined message record from view
   * @param id 
   */
  public deletePredefinedMessage(id) {
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._predefinedmessageService.updateDeletedStatus(enc_data)
      .then((dec) => {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        if (dec.status === 200) {
          this.refetchPredefinedMessages();
          this.predefinedMessageData.splice(id, 1);
          this._toasterService.success('Predefined message deleted successfully.');
        } else if (dec.status === 201) {
        }
      });
  }

  /**
   * To refresh predefined message records showing in the grid
   */
  public refetchPredefinedMessages() {
    if (this.is_search == true) {
      this.searchMessageContent();
    }
    else {
      this.ngOnInit();
    }
  }

  /**
   * To reset the search filters and reload the predefined message records
   */
  public reset() {
    this.is_search = false;
    this.message_content = '';
    this.ngOnInit();
  }

  /**
   * For sorting predefined meesage records 
   * @param key 
   */
  sort(key) {
    this.searchLoader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    let params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        message_content: this.message_content ? this.message_content : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._predefinedmessageService.searchPredefinedMessages(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.predefinedMessageData = data.searchPreMessages;
          this.predefinedMessageLength = data.totalCount;
        }
      } else {
        console.log(dec.message)
      }
      this.searchLoader = false;
    });
  }

  /**
  * Pagination for predefined message module
  * @param data 
  */
  pagingAgent(data) {
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    let params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        message_content: this.message_content ? this.message_content : '',
        company_id: this.companyId
      };

    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._predefinedmessageService.getPredefinedMessages(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.predefinedMessageData = data.predefineMessages;
        }
      } else {
        console.log(dec.message)
      }
    });
  }

  /**
  * For searching predefined message records
  */
  public searchMessageContent() {
    this.searchLoader = true;
    this.is_search = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      message_content: this.message_content ? this.message_content : '',
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._predefinedmessageService.searchPredefinedMessages(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.predefinedMessageData = data.searchPreMessages;
          this.predefinedMessageLength = data.totalCount;
        }
      } else {
        console.log(dec.message)
      }
      this.searchLoader = false;
    });
  }
}
