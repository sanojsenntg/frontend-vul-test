import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniZoneEditComponent } from './mini-zone-edit.component';

describe('MiniZoneEditComponent', () => {
  let component: MiniZoneEditComponent;
  let fixture: ComponentFixture<MiniZoneEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiniZoneEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiniZoneEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
