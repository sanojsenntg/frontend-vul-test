import { ElementRef, NgZone, OnInit, ViewChild, Component, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { DrawingManager } from '@ngui/map';
import { NguiMapComponent } from '@ngui/map';
import { MatSelectModule } from '@angular/material/select';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ZoneService } from '../../../../common/services/zones/zone.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

declare var $;

@Component({
  selector: 'app-mini-zone-edit',
  templateUrl: './mini-zone-edit.component.html',
  styleUrls: ['./mini-zone-edit.component.css']
})
export class MiniZoneEditComponent implements OnInit {
  public companyId = [];
  public center:any ='dubai';
  public drawFigureType;
  public recanglebounds: any = {};
  public polygonbounds: any = [];
  public triangleCode = [];
  public triangleCoords = [];
  public polyCoord;
  data = [];
  map;
  shape;
  public sub;
  public _id;
  onFigureCompletePopup = false;
  public paymentData;
  public allPaymentZone;
  public zone_id;
  selectedOverlay: any;
  public zonesave: any = false;
  public markName;
  public session;
  @ViewChild(DrawingManager) drawingManager: DrawingManager;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    public overlay: Overlay,
    private route: ActivatedRoute,
    public _zoneService: ZoneService,
    public toastr: ToastsManager, 
    public jwtService: JwtService,
    public encDecService:EncDecService
  ) {
    
  }


  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
      this.zone_id = { 
        "zone_id": this._id, 
         company_id: this.companyId
        }
    });
    var encrypted = this.encDecService.nwt(this.session,this.zone_id );
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._zoneService.detailSubZones(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.markName =  data.data.name;
          this.polyCoord = data.data.loc.coordinates[0];
          for (let i = 0; i < this.polyCoord.length - 1; i++) {
            var polyItem = this.polyCoord[i];
            var polygon_obj = { lat: parseFloat(polyItem[1]), lng: parseFloat(polyItem[0]) };
            this.triangleCode.push(polygon_obj);
          }
          this.triangleCoords.push(this.triangleCode);
          this.center = this.triangleCoords[0][0];
          this.markerCenter();
          this.drawPolygon();
        }
      }
    });

    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', event => {
        if (event.type !== google.maps.drawing.OverlayType.MARKER) {
          dm.setDrawingMode(null);
          google.maps.event.addListener(event.overlay, 'click', e => {
            this.selectedOverlay = event.overlay;
            this.selectedOverlay.setEditable(true);
          });
          this.drawFigureType = event.type;
          this.selectedOverlay = event.overlay;
          this.onFigureCompletePopup = true;
          this.dialog.closeAll();
        }
      });
    });
    setTimeout(() => { }, 1000);

    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', event => {
        google.maps.event.addListener(event.overlay, 'click', e => {
          this.selectedOverlay = event.overlay;
          this.selectedOverlay.setEditable(true);
          var mapLength = this.selectedOverlay.getPath().getLength();
          this.zonesave = true;
          console.log(mapLength);
        });
      });
    });
  }

  
  onMapReady(map) {
    this.map = map;
    this.markerCenter();
  }
  aoiPoly
  drawPolygon(){
    console.log(this.triangleCoords);
    this.aoiPoly = new google.maps.Polygon({ 
      paths: this.triangleCoords, 
      strokeColor: '#BCDCF9', 
      strokeOpacity: 1, 
      strokeWeight: 2, 
      fillColor: '#BCDCF9', 
      fillOpacity: 0.6, 
      clickable:true,
      editable:true
    }); 
    this.aoiPoly.setMap(this.map);
    google.maps.event.addListener(this.aoiPoly, 'click', function (event) {
      console.log(this.aoiPoly.getPath())
      var polygonBounds:any =this.aoiPoly.getPath();
      var bounds = [];
      for (var i = 0; i < polygonBounds.length; i++) {
            var point = {
              lat: polygonBounds.getAt(i).lat(),
              lng: polygonBounds.getAt(i).lng()
            };
            bounds.push(point);
       }
       console.log(bounds);
    });
  }
  public butonClk(){
    var polygonBounds:any = this.aoiPoly.getPath();
    var bounds = [];
    for (var i = 0; i < polygonBounds.length; i++) {
      var point = {
        lat: polygonBounds.getAt(i).lat(),
        lng: polygonBounds.getAt(i).lng()
      };
      bounds.push(point);
    }
    console.log(bounds);
  }
  public markerCenter(){
    var bounds = new google.maps.LatLngBounds();
    if(this.triangleCoords){
      for (var j = 0; j < this.triangleCoords[0].length; j++) {
        bounds.extend(this.triangleCoords[0][j]);
      }
      var image = 'data:image/svg+xml;charset=utf-8,%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%0D%0A%09%20width%3D%22122px%22%20height%3D%2253px%22%20viewBox%3D%220%200%20243%20106%22%20enable-background%3D%22new%200%200%20243%20106%22%20xml%3Aspace%3D%22preserve%22%3E%0D%0A%3Cg%3E%0D%0A%09%3Cdefs%3E%0D%0A%09%09%3Crect%20id%3D%22SVGID_1_%22%20width%3D%22243%22%20height%3D%22106%22%2F%3E%0D%0A%09%3C%2Fdefs%3E%0D%0A%09%3CclipPath%20id%3D%22SVGID_2_%22%3E%0D%0A%09%09%3Cuse%20xlink%3Ahref%3D%22%23SVGID_1_%22%20%20overflow%3D%22visible%22%2F%3E%0D%0A%09%3C%2FclipPath%3E%0D%0A%09%3Cg%20clip-path%3D%22url%28%23SVGID_2_%29%22%3E%0D%0A%09%09%3Cg%3E%0D%0A%09%09%09%3Cpath%20fill%3D%22%23FFFFFF%22%20d%3D%22M239%2C22.52v38.55c0%2C9.68-8.95%2C17.52-20%2C17.52h-74.15L125.5%2C104l-21.35-25.41H24%0D%0A%09%09%09%09c-11.05%2C0-20-7.84-20-17.52V22.52C4%2C12.84%2C12.95%2C5%2C24%2C5h195C230.05%2C5%2C239%2C12.84%2C239%2C22.52z%22%2F%3E%0D%0A%09%09%3C%2Fg%3E%0D%0A%09%09%3Cpath%20fill%3D%22%23FFFFFF%22%20d%3D%22M122%2C52%22%2F%3E%0D%0A%09%3C%2Fg%3E%0D%0A%3C%2Fg%3E%0D%0A%3Crect%20x%3D%225%22%20y%3D%2229.833%22%20fill%3D%22none%22%20width%3D%22234%22%20height%3D%2234%22%2F%3E%0D%0A%3Ctext%20transform%3D%22matrix%281%200%200%201%2028.9766%2055.6094%29%22%20font-family%3D%22%27ArialMT%27%22%20font-size%3D%2236%22%3E'+ this.markName +'%3C%2Ftext%3E%0D%0A%3C%2Fsvg%3E' ;
      var myLatlng = bounds.getCenter();
      var marker = new google.maps.Marker({
        position: myLatlng,
        map: this.map,
        icon: image,
        title: this.markName
      });
      marker.setMap(this.map);
      this.center = myLatlng
    }    
  }

  public zoneSave(event) {
   
  }

  deleteSelectedOverlay() {
    if (this.selectedOverlay) {
      this.selectedOverlay.setMap(null);
      delete this.selectedOverlay;
    }
  }
}
