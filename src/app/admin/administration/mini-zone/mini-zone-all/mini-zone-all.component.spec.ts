import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniZoneAllComponent } from './mini-zone-all.component';

describe('MiniZoneAllComponent', () => {
  let component: MiniZoneAllComponent;
  let fixture: ComponentFixture<MiniZoneAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiniZoneAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiniZoneAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
