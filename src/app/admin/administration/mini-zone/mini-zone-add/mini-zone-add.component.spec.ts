import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniZoneAddComponent } from './mini-zone-add.component';

describe('MiniZoneAddComponent', () => {
  let component: MiniZoneAddComponent;
  let fixture: ComponentFixture<MiniZoneAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiniZoneAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiniZoneAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
