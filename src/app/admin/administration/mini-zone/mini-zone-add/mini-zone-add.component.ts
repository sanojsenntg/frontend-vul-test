
import { ElementRef, NgZone, OnInit, ViewChild, Component, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { DrawingManager } from '@ngui/map';
import { NguiMapComponent } from '@ngui/map';
import { MatSelectModule } from '@angular/material/select';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { ZoneService } from '../../../../common/services/zones/zone.service';
import { ZoneSavingComponent } from '../../../../common/dialog/zone-saving/zone-saving.component';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
declare var $;

@Component({
  selector: 'app-mini-zone-add',
  templateUrl: './mini-zone-add.component.html',
  styleUrls: ['./mini-zone-add.component.css']
})
export class MiniZoneAddComponent implements OnInit {
  public companyId = [];
  public center: any;
  public drawFigureType;
  public recanglebounds: any = {};
  public polygonbounds: any = [];
  triangleCoords = [];
  markCord = [];
  data = [];
  map;
  shape;
  onFigureCompletePopup = false;
  public paymentzone_id;
  public direction: any = {
    origin: 'penn station, new york, ny',
    destination: '260 Broadway New York NY 10007',
    travelMode: 'WALKING'
  };
  public paymentData;
  public allPaymentZone;
  public zonesave: any = false;
  public deleteZone: any = false;
  selectedOverlay: any;
  public zoneName = [];
  public session;

  @ViewChild(DrawingManager) drawingManager: DrawingManager;

  constructor(
    public dialog: MatDialog,
    public overlay: Overlay,
    private router: Router,
    private _zoneService: ZoneService,
    public toastr: ToastsManager, 
    public jwtService: JwtService,
    public encDecService:EncDecService
  ) {
  }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');

    this.center = 'dubai';
    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', event => {
        if (event.type !== google.maps.drawing.OverlayType.MARKER) {
          dm.setDrawingMode(null);
          google.maps.event.addListener(event.overlay, 'click', e => {
            this.selectedOverlay = event.overlay;
            this.selectedOverlay.setEditable(true);
          });
          this.drawFigureType = event.type;
          this.selectedOverlay = event.overlay;
          this.onFigureCompletePopup = true;
          this.dialog.closeAll();
          this.zonesave = true;
          this.deleteZone = true;
          this.zoneSave()
        }
      });
    });
    setTimeout(() => { }, 1000);
    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', event => {
        google.maps.event.addListener(event.overlay, 'click', e => {
          this.deleteZone = true;
          this.zonesave = true;
          this.selectedOverlay = event.overlay;
        });
      });
    });

    this.getPolygons();
  }
  public getPolygons(){
    const params = {
      offset: 0,
      limit: 100,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._zoneService.getSubZones(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          var zoneTemp = []
          for(let i=0; i<data.zones.length; i++){
            this.zoneName.push(data.zones[i].name);
            for(let j=0; j<data.zones[i].loc.coordinates[0].length;j++){
              zoneTemp.push({lat:parseFloat(data.zones[i].loc.coordinates[0][j][1]), lng:parseFloat(data.zones[i].loc.coordinates[0][j][0])})
            }
            this.triangleCoords.push(zoneTemp);
            this.initialMark(zoneTemp, data.zones[i].name).then(
              () =>{
                zoneTemp = [];
              },
              () =>{
                this.initialMark(zoneTemp, data.zones[i].name);
              },
            );
            zoneTemp = [];
          }
        }
      }
    });
  }

  public deleteOverlay() {
    this.deleteSelectedOverlay();
    this.zonesave = false;
    this.deleteZone = false;
  }

  public initialMark(data, name) {
    var promise;
    return promise = new Promise((resolve, reject) => {
      var bounds = new google.maps.LatLngBounds();
      
      data.forEach(function(element,index) {
        if(index < data.length){
          bounds.extend(element)
        }
      });
      var image = 'data:image/svg+xml;charset=utf-8,%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%0D%0A%09%20width%3D%22122px%22%20height%3D%2253px%22%20viewBox%3D%220%200%20243%20106%22%20enable-background%3D%22new%200%200%20243%20106%22%20xml%3Aspace%3D%22preserve%22%3E%0D%0A%3Cg%3E%0D%0A%09%3Cdefs%3E%0D%0A%09%09%3Crect%20id%3D%22SVGID_1_%22%20width%3D%22243%22%20height%3D%22106%22%2F%3E%0D%0A%09%3C%2Fdefs%3E%0D%0A%09%3CclipPath%20id%3D%22SVGID_2_%22%3E%0D%0A%09%09%3Cuse%20xlink%3Ahref%3D%22%23SVGID_1_%22%20%20overflow%3D%22visible%22%2F%3E%0D%0A%09%3C%2FclipPath%3E%0D%0A%09%3Cg%20clip-path%3D%22url%28%23SVGID_2_%29%22%3E%0D%0A%09%09%3Cg%3E%0D%0A%09%09%09%3Cpath%20fill%3D%22%23FFFFFF%22%20d%3D%22M239%2C22.52v38.55c0%2C9.68-8.95%2C17.52-20%2C17.52h-74.15L125.5%2C104l-21.35-25.41H24%0D%0A%09%09%09%09c-11.05%2C0-20-7.84-20-17.52V22.52C4%2C12.84%2C12.95%2C5%2C24%2C5h195C230.05%2C5%2C239%2C12.84%2C239%2C22.52z%22%2F%3E%0D%0A%09%09%3C%2Fg%3E%0D%0A%09%09%3Cpath%20fill%3D%22%23FFFFFF%22%20d%3D%22M122%2C52%22%2F%3E%0D%0A%09%3C%2Fg%3E%0D%0A%3C%2Fg%3E%0D%0A%3Crect%20x%3D%225%22%20y%3D%2229.833%22%20fill%3D%22none%22%20width%3D%22234%22%20height%3D%2234%22%2F%3E%0D%0A%3Ctext%20transform%3D%22matrix%281%200%200%201%2028.9766%2055.6094%29%22%20font-family%3D%22%27ArialMT%27%22%20font-size%3D%2236%22%3E'+ name+'%3C%2Ftext%3E%0D%0A%3C%2Fsvg%3E' ;

      var myLatlng = bounds.getCenter();
      var marker = new google.maps.Marker({
        position: myLatlng,
        map: this.map,
        title: name,
        icon: image
      });
      marker.setMap(this.map);
    });
  }
  

  public markerSet(data){
    var bounds = new google.maps.LatLngBounds();
    for (var j = 0; j < data.coordinates.length-1; j++) {
      var item = {lat: data.coordinates[j][1],lng: data.coordinates[j][0]}
      bounds.extend(item);
    }
    var image = 'data:image/svg+xml;charset=utf-8,%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%0D%0A%09%20width%3D%22122px%22%20height%3D%2253px%22%20viewBox%3D%220%200%20243%20106%22%20enable-background%3D%22new%200%200%20243%20106%22%20xml%3Aspace%3D%22preserve%22%3E%0D%0A%3Cg%3E%0D%0A%09%3Cdefs%3E%0D%0A%09%09%3Crect%20id%3D%22SVGID_1_%22%20width%3D%22243%22%20height%3D%22106%22%2F%3E%0D%0A%09%3C%2Fdefs%3E%0D%0A%09%3CclipPath%20id%3D%22SVGID_2_%22%3E%0D%0A%09%09%3Cuse%20xlink%3Ahref%3D%22%23SVGID_1_%22%20%20overflow%3D%22visible%22%2F%3E%0D%0A%09%3C%2FclipPath%3E%0D%0A%09%3Cg%20clip-path%3D%22url%28%23SVGID_2_%29%22%3E%0D%0A%09%09%3Cg%3E%0D%0A%09%09%09%3Cpath%20fill%3D%22%23FFFFFF%22%20d%3D%22M239%2C22.52v38.55c0%2C9.68-8.95%2C17.52-20%2C17.52h-74.15L125.5%2C104l-21.35-25.41H24%0D%0A%09%09%09%09c-11.05%2C0-20-7.84-20-17.52V22.52C4%2C12.84%2C12.95%2C5%2C24%2C5h195C230.05%2C5%2C239%2C12.84%2C239%2C22.52z%22%2F%3E%0D%0A%09%09%3C%2Fg%3E%0D%0A%09%09%3Cpath%20fill%3D%22%23FFFFFF%22%20d%3D%22M122%2C52%22%2F%3E%0D%0A%09%3C%2Fg%3E%0D%0A%3C%2Fg%3E%0D%0A%3Crect%20x%3D%225%22%20y%3D%2229.833%22%20fill%3D%22none%22%20width%3D%22234%22%20height%3D%2234%22%2F%3E%0D%0A%3Ctext%20transform%3D%22matrix%281%200%200%201%2028.9766%2055.6094%29%22%20font-family%3D%22%27ArialMT%27%22%20font-size%3D%2236%22%3E'+ data.name+'%3C%2Ftext%3E%0D%0A%3C%2Fsvg%3E' ;

    var myLatlng = bounds.getCenter();
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: this.map,
      icon: image,
      title: data.name
    });
    marker.setMap(this.map);
    this.markCord=[];
  }
  
  public zoneSave() {
    this.polygonbounds = [];
    if (this.drawFigureType == "polygon") {
      for (var i = 0; i < this.selectedOverlay.getPath().getLength(); i++) {
        console.log(this.selectedOverlay.getPath().getAt(i).toUrlValue(6)) + "<br>";
        var polygon = this.selectedOverlay.getPath().getAt(i).toUrlValue(6);
        var splits = polygon.split(",");
        var polygon_obj = [parseFloat(splits[1]), parseFloat(splits[0])];
        var markItem = {lat:parseFloat(splits[0]),lng: parseFloat(splits[1])}
        this.markCord.push(markItem);
        this.polygonbounds.push(polygon_obj);
      }
      this.polygonbounds.push(this.polygonbounds[0]);
      let dialogRef = this.dialog.open(ZoneSavingComponent, {
        width: '600px',
        scrollStrategy: this.overlay.scrollStrategies.noop(),
        data: { 'type': 'polygon', bounds: this.polygonbounds }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          var params = { 
            name: result, 
            coordinates: this.polygonbounds,
            company_id:localStorage.getItem('user_company')
          }
          var encrypted = this.encDecService.nwt(this.session,params);
          var enc_data = {
            data: encrypted,
            email: localStorage.getItem('user_email')
          }
          this._zoneService.saveSubZone(enc_data).then((dec) => {
            if (dec.status == 200) {
              this.toastr.success('zone succesfully saved..');
              this.markerSet(params);
            } else if (dec.status == 203) {
              this.toastr.success('zone already exist..');
            }
          });
        }
      });
    }
  }

  onMapReady(map) {
    this.map = map;
    this.getPolygons();
  }

  deleteSelectedOverlay() {
    if (this.selectedOverlay) {
      this.selectedOverlay.setMap(null);
      delete this.selectedOverlay;
    }
  }
}
