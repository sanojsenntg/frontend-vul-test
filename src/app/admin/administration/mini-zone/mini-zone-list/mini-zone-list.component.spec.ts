import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniZoneListComponent } from './mini-zone-list.component';

describe('MiniZoneListComponent', () => {
  let component: MiniZoneListComponent;
  let fixture: ComponentFixture<MiniZoneListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiniZoneListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiniZoneListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
