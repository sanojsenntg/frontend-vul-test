import { Component, OnInit } from '@angular/core';
import { MatDialog,MatAutocompleteSelectedEvent, MatChipInputEvent} from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ZoneService } from '../../../../common/services/zones/zone.service';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { RenameSubzoneComponent } from '../../../../common/dialog/rename-subzone/rename-subzone.component';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-mini-zone-list',
  templateUrl: './mini-zone-list.component.html',
  styleUrls: ['./mini-zone-list.component.css']
})
export class MiniZoneListComponent implements OnInit {
  public zones;
  public sortOrder = 'desc';
  public pageSize = 100;
  public showLoader = false;
  public zoneLength;
  public pageNo = 0;
  public key: string = '';
  public searchBar;
  public aclAdd=false;
  public aclEdit=false;
  public aclDelete=false;
  public companyId = [];
  public session;

  constructor(
    private router: Router,
    public _zoneService:ZoneService,
    public dialog: MatDialog,
    public overlay: Overlay,
    public _aclService: AccessControlService,
    public toastr: ToastsManager, 
    public jwtService: JwtService,
    public encDecService:EncDecService
  ) {
   }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.zoneLoad();
    this.aclDisplayService()
  }
  public aclDisplayService(){
    var params = {  
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          for(let i=0; i<data.menu.length; i++){
                if(data.menu[i]=="Dubai Major Zone - Add"){
                  this.aclAdd = true;
                }else if (data.menu[i]=="Dubai Major Zone - Edit"){
                  this.aclEdit = true;
                }else if (data.menu[i]=="Dubai Major Zone - Delete"){
                  this.aclDelete = true;
                }
            };
        }
      }else{
        console.log(dec.message)
      }
   
    })
  }

  public zoneLoad(){
    this.showLoader = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._zoneService.getSubZones(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.zones = data.zones
          this.zoneLength = data.count;
          this.showLoader = false;
        }
      }
    });
  }
  public deleteZone(data){
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        var params = {
          'zone_id': data,
          company_id: this.companyId
        };
        var encrypted = this.encDecService.nwt(this.session,params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._zoneService.deleteSubZones(enc_data).then((dec) => {
          if(dec){
            if(dec.status == 200){
              this.zoneLoad();
            }
          }else{
            console.log(dec.message)
          }
          
        });
      } else {
      }
    });
    
  }
  public searchZone(){
    console.log(this.searchBar);
    var params;
        params = {
          offset: 0,
          limit: this.pageSize,
          sortOrder: 'desc',
          sortByColumn: 'updated_at',
          search: this.searchBar,
          company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._zoneService.getSubZones(enc_data).then((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec);
            this.zones = data.zones;
            this.showLoader = false;
          }
        }else{
          console.log(dec.message)
        }
       
      });
    
  }
  
  pagingAgent(data) {
    this.showLoader = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    var params;
        params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._zoneService.getSubZones(enc_data).then((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec);
            this.zones = data.zones;
            this.showLoader = false;
          }
        }else{
          console.log(dec.message)
        }
      
      });
  }
  renameSubZones(event){
    let dialogRef = this.dialog.open(RenameSubzoneComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: event
    });
    dialogRef.afterClosed().subscribe(result => {
      this.zoneLoad();
    });
  }
}
