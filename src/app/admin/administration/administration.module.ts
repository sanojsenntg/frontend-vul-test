import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { PaymentExtraModule } from './payment-extra/payment-extra.module';
import { PaymentTypeModule } from './payment-type/payment-type.module';
import { VehicleCategoriesModule } from './sub-categories/vehicle-categories.module';
import { VehicleSubCategoriesModule } from './vehicle_category/vehicle-sub-category.module';
import { PaymentZoneModule } from './payment-zone/payment-zone.module';
import { PromocodeTransactionModule } from './promo-transactions/promocode-transaction.module';
import { PredefinedMessagesModule } from './predefined-messages/predefined-messages.module';
import { MessageHistoryModule } from './message-history/message-history.module';
import { PartnersModule } from './partners/partners.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImageUploadModule } from 'angular2-image-upload';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { TariffModule } from './tariff/tariff.module';
import { MatSelectModule } from '@angular/material/select';
import { SavePaymentZoneComponent } from './payment-zone/add-payment-zone/dialog/save-payment-zone/save-payment-zone';
import { PromocodeModule } from './promocode/promocode.module';
import { AdministrationComponent } from './administration/administration.component';
import { FaqModule } from './faq/faq.module';
import { SocialSettingsModule } from './social-settings/social-settings.module';
import { ContactusCategoryModule } from './contactus-category/contactus-category.module';
import { ZonesComponent } from '../administration/zones/zones/zones.component';
import { RolesModule } from './roles/roles.module';
import { ListCoverageAreaComponent } from './coverage-area/list-coverage-area/list-coverage-area.component';
import { AddCoverageAreaComponent } from './coverage-area/add-coverage-area/add-coverage-area.component';
import { SaveCoverageAreaComponent } from './coverage-area/add-coverage-area/dialog/save-coverage-area/save-coverage-area';
import { NguiMapModule } from '@ngui/map';
import { environment } from '../../../environments/environment';
import { MatDialogModule, MatChipsModule } from '@angular/material';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DestinationModule } from './destination/destination.module';
import { ZoneModule } from './zones/zone.module';
import { ZoneDialogComponent } from './zones/zone-actions/zone-dialog/zone-dialog.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MiniZoneModule } from './mini-zone/mini-zone.module';
import { PromoHistoryModule } from './promo-history/promo-history.module';
import { OrderCorrectionComponent } from './order-correction/order-correction.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { TwoDigitDecimaNumberDirective } from '../../common/directive/pricevalidationdirective';
import { CompaniesModule } from '../../admin/administration/companies/companies.module';
import { AdministrationSubmenusComponent } from './administration-submenus/administration-submenus.component';
import { ZendeskComponent } from './zendesk/zendesk.component'
import { ProgressBarModule } from "angular-progress-bar";
import { ZendeskCommentsComponent } from './zendesk/zendesk-comments/zendesk-comments.component';
import { DriverMapComponent } from './driver-map/driver-map.component';
import { TicketManagementModule } from './ticket-management/ticket-management.module';
import { MultiZonesModule } from './multi-zones/multi-zones.module';

@NgModule({
  imports: [
    MatTooltipModule,
    MatDialogModule,
    CommonModule,
    MatChipsModule,
    PaymentExtraModule,
    ReactiveFormsModule,
    RouterModule,
    PaymentTypeModule,
    PaymentZoneModule,
    CompaniesModule,
    PredefinedMessagesModule,
    PromocodeTransactionModule,
    MessageHistoryModule,
    VehicleCategoriesModule,
    PartnersModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ImageUploadModule,
    NgbModule,
    NguiMapModule.forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.map_key + '&libraries=visualization,places,drawing' }),
    Ng2OrderModule,
    VehicleSubCategoriesModule,
    FormsModule,
    MatSelectModule,
    TariffModule,
    PromocodeModule,
    FaqModule,
    SocialSettingsModule,
    ContactusCategoryModule,
    RolesModule,
    DestinationModule,
    ZoneModule,
    MiniZoneModule,
    PromoHistoryModule,
    MatAutocompleteModule,
    ProgressBarModule,
    TicketManagementModule,
    MultiZonesModule
  ],
  declarations: [SavePaymentZoneComponent, AdministrationComponent, ListCoverageAreaComponent, AddCoverageAreaComponent, SaveCoverageAreaComponent, ZoneDialogComponent, OrderCorrectionComponent, TwoDigitDecimaNumberDirective, AdministrationSubmenusComponent, ZendeskComponent, ZendeskCommentsComponent, DriverMapComponent],
  entryComponents: [ZonesComponent, SavePaymentZoneComponent, ListCoverageAreaComponent, AddCoverageAreaComponent, SaveCoverageAreaComponent, ZoneDialogComponent],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } },
  ],
  exports: [
    RouterModule
  ]
})
export class AdministrationModule { }
