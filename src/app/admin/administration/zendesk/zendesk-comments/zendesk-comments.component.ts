import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CustomerService } from '../../../../common/services/customer/customer.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-zendesk-comments',
  templateUrl: './zendesk-comments.component.html',
  styleUrls: ['./zendesk-comments.component.css']
})
export class ZendeskCommentsComponent implements OnInit {
  id: any;
  session: string;
  companyId: any=[];
  isLoading: boolean;
  invalid: string;
  commentList: any=[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,private _customerService:CustomerService,
  public encDecService: EncDecService) {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
   }

  ngOnInit() {
    this.isLoading = true;
    this.invalid='Loading...';
    let params = {
      ticket_id:this.data.id
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.zendeskComments(enc_data).then((dec) => {
      this.isLoading= false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        console.log(data)
        this.commentList = data.data;
      }
      else {
        this.invalid = dec ? dec.message : 'Something went wrong'
      }
    }) 
  }
  format(data){
    return data.replace(/(\r\n|\n|\r)/gm, " <br /> ")
  }
}
