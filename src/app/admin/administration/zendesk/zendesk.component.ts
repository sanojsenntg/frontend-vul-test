import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../common/services/customer/customer.service';
import * as moment from 'moment/moment';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { ToastsManager } from 'ng2-toastr';
import { Angular2Csv } from 'angular2-csv';
import { ZendeskCommentsComponent } from './zendesk-comments/zendesk-comments.component'
import { MatDialog } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { Router } from '@angular/router';
@Component({
  selector: 'app-zendesk',
  templateUrl: './zendesk.component.html',
  styleUrls: ['./zendesk.component.css']
})
export class ZendeskComponent implements OnInit {

  public startDate: any=new Date(Date.now() - 604800000);
  public endDate: any=new Date(Date.now());
  max = new Date(Date.now());
  companyId: any = [];
  session: string;
  invalid: any;
  searchSubmit: boolean;
  offset = 0;

  ticketList: any = [];
  pageNo: number = 0;
  pageSize: number = 10;
  ticketLength: any;
  creatingcsv: any;
  csvInterval: NodeJS.Timer;
  progressvalue: number = 0;
  sort_order='-1';
  sort_by_column='created_at';
  status;
  pNo: any = 1;
  tagList: any;
  ticketId;
  ticketIdList: any=[];
  tags: any;
  statusList: any;
  constructor(
    private router: Router,
    public dialog: MatDialog,
    private _customerService: CustomerService,
    public encDecService: EncDecService,
    public toastr: ToastsManager,
    public overlay: Overlay) {

    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
  }

  ngOnInit() {
    this.getTickets();
    this.getTags();
    this.getStatus();
  }
  getTags() {
    let params = {
      offset: 0,
      limit: this.pageSize,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.zendeskTags(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.tagList = data.data;
        this.tagList = this.tagList.sort();
      }
    })
  }
  getTickets() {
    this.pNo = 1;
    this.searchSubmit = true;
    this.invalid = '';
    let params = {
      offset: 0,
      limit: this.pageSize,
      start_time: this.ticketIdList.length==0?this.ticketId?'':this.startDate ? moment(this.startDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') : moment(Date.now() - 86400000).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss'):'',
      end_time: this.ticketIdList.length==0?this.ticketId?'':this.endDate ? moment(this.endDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') : moment(Date.now()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss'):'',
      status: this.status ? this.status : [],
      ticket_id: this.ticketIdList.length>0?this.ticketIdList:this.ticketId?[this.ticketId]:[],
      tags: this.tags?this.tags:[],
      sortOrder: this.sort_order ? this.sort_order : '-1',
      sortByColumn: this.sort_by_column ? this.sort_by_column : 'created_at'
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.zendeskTickets(enc_data).then((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ticketList = data.data;
        this.ticketLength = data.count;
      }
      else {
        this.invalid = dec ? dec.message : 'Something went wrong'
      }
    })
  }
  getStatus() {
    var encrypted = this.encDecService.nwt(this.session, {});
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.zendeskStatus(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.statusList = data.data;
        this.statusList = this.statusList.sort();
      }
    })
  }
  pagingAgent(data) {
    this.pNo = data
    window.scrollTo(0, 200);
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      start_time: this.ticketIdList.length==0?this.ticketId?'':this.startDate ? moment(this.startDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') : moment(Date.now() - 86400000).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss'):'',
      end_time: this.ticketIdList.length==0?this.ticketId?'':this.endDate ? moment(this.endDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') : moment(Date.now()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss'):'',
      status: this.status ? this.status : [],
      ticket_id: this.ticketIdList.length>10?this.ticketIdList:[],
      tags: this.tags?this.tags:[],
      sortOrder: this.sort_order ? this.sort_order : '-1',
      sortByColumn: this.sort_by_column ? this.sort_by_column : 'created_at'
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.zendeskTickets(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.ticketList = data.data;
      }
      this.searchSubmit = false;
    });
  }
  public getconstantsforcsv() {
    if (this.creatingcsv) {
      this.toastr.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      this.createCsv(2000, 100, 0);
    }
  }
  public createCsv(interval_value, offset, resetCount) {
    var count = this.ticketLength;
    let res1Array = [];
    let i = 0;
    let fetch_status = true;
    let that = this;
    this.csvInterval = setInterval(function () {
      if (fetch_status) {
        if (res1Array.length >= count) {
          that.progressvalue = 100;
          that.creatingcsv = false;
          clearInterval(that.csvInterval);
          let labels = [
            'ID',
            'Subject',
            'Description',
            'Status',
            'Tags',
            'Created date',
            'Updated date',
            'comments'
          ];
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(res1Array, 'List-Order' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
          that.progressvalue = 0;
        } else {
          that.creatingcsv = true;
          fetch_status = false;
          let diff = count - i;
          let perc = diff / count;
          let remaining = 100 * perc;
          that.progressvalue = 100 - remaining;
          let new_params;
          new_params = {
            offset: i,
            //limit: res1Array.length>=40?10:parseInt(offset),
            limit: parseInt(offset),
            start_time: that.ticketIdList.length==0?that.ticketId?'':that.startDate ? moment(that.startDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') : moment(Date.now() - 86400000).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss'):'',
            end_time: that.ticketIdList.length==0?that.ticketId?'':that.endDate ? moment(that.endDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') : moment(Date.now()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss'):'',
            status: that.status ? that.status : [],
            sortOrder: that.sort_order ? that.sort_order : '-1',
            ticket_id: that.ticketIdList.length>0?that.ticketIdList:that.ticketId?[that.ticketId]:[],
            tags: that.tags?that.tags:[],
            sortByColumn: that.sort_by_column ? that.sort_by_column : 'created_at',
          };
          let enc_data = that.encDecService.nwt(that.session, new_params);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: localStorage.getItem('user_email')
          }
          that._customerService.zendeskTickets(data1).then((res) => {
            if (res && res.status == 200) {
              res = that.encDecService.dwt(that.session, res.data);
              if (res.data.length > 0) {
                for (let j = 0; j < res.data.length; j++) {
                  const csvArray = res.data[j];
                  let tags = '';
                  for (let k = 0; k < csvArray.tags.length; k++) {
                    tags += csvArray.tags[k];
                    tags += csvArray.tags.length !== 1 && csvArray.tags.length !== k + 1 ? ', ' : '';
                  }
                  res1Array.push({
                    id: csvArray.id ? csvArray.id : 'N/A',
                    subject: csvArray.subject ? csvArray.subject : 'N/A',
                    description: csvArray.description ? csvArray.description : 'N/A',
                    status: csvArray.status ? csvArray.status : 'N/A',
                    tags: tags,
                    created_date: csvArray.created_at ? moment(csvArray.created_at).format('YYYY-MM-DD HH:mm:ss') : 'N/A',
                    updated_date: csvArray.updated_at ? moment(csvArray.updated_at).format('YYYY-MM-DD HH:mm:ss') : 'N/A',
                  });
                  if(csvArray.comment_count>0){
                    let k=0;
                    if(csvArray.comments[0] && csvArray.comments[0].comments)
                    csvArray.comments[0].comments.forEach(element => {
                      k++;
                      res1Array[res1Array.length-1]['comment '+k]=element.plain_body;
                    })
                  }
                  else{
                    res1Array[res1Array.length-1]['comment']='N/A'
                  }
                  if (j == res.data.length - 1) {
                    i = i + parseInt(offset);
                    fetch_status = true;
                  }
                }
              }
            }
          }).catch((Error) => {
            console.log(Error)
            that.toastr.warning('Network reset, csv creation restarted');
            resetCount++;
            if (resetCount <= 3)
              that.createCsv(interval_value, offset, resetCount);
            else{
              that.toastr.error('Error, Csv creation terminated');
              that.creatingcsv = false;
              clearInterval(that.csvInterval);
            }
          })

        }
      }
    }, parseInt(interval_value));
  }
  reset() {
    this.startDate = new Date(Date.now() - 86400000);
    this.endDate = new Date();
    this.pNo = 1;
    this.status=[];
    this.ticketIdList=[];
    this.tags=[];
    this.ticketId='';
    this.sort_by_column='created_at';
    this.sort_order='-1';
    this.getTickets();
  }
  viewComment(id) {
    let data = {
      id: id,
    }
    this.dialog.open(ZendeskCommentsComponent, {
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      width: "450px",
      data: data,
      hasBackdrop: true
    });
    // dialogRef.afterClosed().subscribe(res => {

    // })
  }
  searchId(data) {
    if (data.keyCode==13) {
      if (this.ticketIdList.indexOf(data.target.value)==-1)
          this.ticketIdList.push(data.target.value);
      this.ticketId='';
    }
  }
  removeMultiSelect(id) {
    let index = this.ticketIdList.indexOf(id);
    this.ticketIdList.splice(index, 1);
  }
  goToLink(url: string){
    if(url)
    window.open('/admin/customer/detail/'+url);
  }
  goToZen(url){
    window.open('https://dtcapp.zendesk.com/agent/tickets/'+url)
  }
}
