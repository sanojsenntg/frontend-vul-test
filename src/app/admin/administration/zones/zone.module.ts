import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NguiMapModule } from '@ngui/map';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ZonesComponent } from './zones/zones.component';
import { ZoneMenuComponent } from './zone-menu/zone-menu.component';
import { ZoneListComponent } from './zone-actions/zone-list/zone-list.component';
import { ZoneAddComponent } from './zone-actions/zone-add/zone-add.component';
import { ZoneEditComponent } from './zone-actions/zone-edit/zone-edit.component';
import { ZoneService } from '../../../common/services/zones/zone.service';
import { ZoneMultipleComponent } from './zone-actions/zone-multiple/zone-multiple.component';

@NgModule({
  imports: [
    CommonModule,
    NguiMapModule,
    RouterModule,
    NgbModule
  ],
  declarations: [
    ZonesComponent,
    ZoneMenuComponent,
    ZoneListComponent, 
    ZoneAddComponent, 
    ZoneEditComponent,
    ZoneMultipleComponent
  ],
  providers:[ZoneService],
  exports: [
    RouterModule
  ]
})
export class ZoneModule { }
