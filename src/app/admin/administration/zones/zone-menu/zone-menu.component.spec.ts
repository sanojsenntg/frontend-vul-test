import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneMenuComponent } from './zone-menu.component';

describe('ZoneMenuComponent', () => {
  let component: ZoneMenuComponent;
  let fixture: ComponentFixture<ZoneMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
