import { Component, OnInit } from '@angular/core';
import { ZoneService } from '../../../../../common/services/zones/zone.service';
import { DeleteDialogComponent } from '../../../../../common/dialog/delete-dialog/delete-dialog.component';
import { MatDialog, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { AccessControlService } from '../../../../../common/services/access-control/access-control.service';
import { Router } from '@angular/router';
import { JwtService } from '../../../../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-zone-list',
  templateUrl: './zone-list.component.html',
  styleUrls: ['./zone-list.component.css']
})
export class ZoneListComponent implements OnInit {
  public companyId: any = [];
  public zones;
  public sortOrder = 'desc';
  public pageSize = 10;
  public showLoader = false;
  public zoneLength;
  public pageNo = 0;
  public key: string = '';
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public session;
  constructor(private router: Router,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    public _zoneService: ZoneService,
    public dialog: MatDialog,
    public overlay: Overlay,
    public _aclService: AccessControlService,
    public encDecService: EncDecService
  ) {
  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.zoneLoad();
    this.aclDisplayService();
  }

  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Zone-Add") {
              this.aclAdd = true;
            } else if (data.menu[i] == "Zone-Edit") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Zone-Delete") {
              this.aclDelete = true;
            }
          };
        }
      } else {
        console.log(dec.message)
      }
    })
  }

  public zoneLoad() {
    this.showLoader = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._zoneService.getZones(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.zones = data.zones
          this.zoneLength = data.count;
          this.showLoader = false;
        } else {
          console.log(dec.message)
        }
      }
    });
  }
  public deleteZone(data) {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        var params = { 'zone_id': data, company_id: this.companyId };
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._zoneService.deleteZones(enc_data).then((dec) => {
          if (dec.status == 200) {
            this.zoneLoad();
          }
        });
      } else {
      }
    });

  }

  pagingAgent(data) {
    this.showLoader = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    var params;
    params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._zoneService.getZones(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.zones = data.zones
        }
      } else {
        console.log(dec.message)
      }
      this.showLoader = false;
    });
  }
}
