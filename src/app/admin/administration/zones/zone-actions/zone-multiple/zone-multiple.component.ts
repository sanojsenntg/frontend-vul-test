
import { ElementRef, NgZone, OnInit, ViewChild, Component, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { DrawingManager } from '@ngui/map';
import { NguiMapComponent } from '@ngui/map';
import { MatSelectModule } from '@angular/material/select';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { PaymentZoneService } from '../../../../../common/services/paymentzone/paymentzone.service';
import { Overlay } from '@angular/cdk/overlay';
import { ZoneDialogComponent } from '../zone-dialog/zone-dialog.component';
import { ZoneService } from '../../../../../common/services/zones/zone.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
declare var $;

@Component({
  selector: 'app-zone-multiple',
  templateUrl: './zone-multiple.component.html',
  styleUrls: ['./zone-multiple.component.css']
})
export class ZoneMultipleComponent implements OnInit {
  public center: any;
  public drawFigureType;
  public recanglebounds: any = {};
  public polygonbounds: any = [];
  public polygonArray = [];
  triangleCoords = [];
  data = [];
  map;
  shape;
  onFigureCompletePopup = false;
  public paymentzone_id;
  public direction: any = {
    origin: 'penn station, new york, ny',
    destination: '260 Broadway New York NY 10007',
    travelMode: 'WALKING'
  };
  public paymentData;
  public allPaymentZone;
  public zonesave: any = false;
  public deleteZone: any = false;
  selectedOverlay: any;
  public companyId: any = [];
  public session;
  @ViewChild(DrawingManager) drawingManager: DrawingManager;

  constructor(
    public dialog: MatDialog,
    public overlay: Overlay,
    private route: ActivatedRoute,
    private router: Router,
    private _zoneService: ZoneService,
    public toastr: ToastsManager,
    public jwtService: JwtService,
    public encDecService: EncDecService
  ) {
  }


  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.center= "dubai";
    this.zoneNaming();
    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', event => {
        if (event.type !== google.maps.drawing.OverlayType.MARKER) {
          dm.setDrawingMode(null);
          google.maps.event.addListener(event.overlay, 'click', e => {
            this.selectedOverlay = event.overlay;
            this.selectedOverlay.setEditable(true);
          });

          this.drawFigureType = event.type;
          this.selectedOverlay = event.overlay;
          this.onFigureCompletePopup = true;
          this.dialog.closeAll();
          this.zonesave = true;
          this.deleteZone = true;
        }
      });
    });
    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', event => {
        google.maps.event.addListener(event.overlay, 'click', e => {
          this.deleteZone = true;
          this.zonesave = true;
          this.selectedOverlay = event.overlay;
        });
      });
    });
  }

  public deleteOverlay() {
    this.deleteSelectedOverlay();
    this.zonesave = false;
    this.deleteZone = false;
  }
  zoneNaming(){
    let dialogRef = this.dialog.open(ZoneDialogComponent, {
      width: '600px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { 'type': 'polygon', bounds: this.polygonbounds }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        var params = {
          name: result.name,
          pin_zone: result.pin_zone,
          coordinates: this.polygonbounds,
          company_id: localStorage.getItem('user_company')
        }
        console.log(params);
      }
    });
  }
  saveApi(result){
    var params = {
      name: result.name,
      pin_zone: result.pin_zone,
      coordinates: this.polygonbounds,
      company_id: localStorage.getItem('user_company')
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._zoneService.saveZone(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          this.toastr.success('zone succesfully saved..');
          this.router.navigate(['/admin/administration/zones/zonelist']);
        } else if (dec.status == 203) {
          this.toastr.success('zone already exist..');
        } else {
          console.log(dec.message)
        }
      }
    });
  }
  public zoneSave() {
    this.polygonbounds = [];
    if (this.drawFigureType == "polygon") {
      for (var i = 0; i < this.selectedOverlay.getPath().getLength(); i++) {
        console.log(this.selectedOverlay.getPath().getAt(i).toUrlValue(6)) + "<br>";
        var polygon = this.selectedOverlay.getPath().getAt(i).toUrlValue(6);
        var splits = polygon.split(",");
        var polygon_obj = [parseFloat(splits[0]), parseFloat(splits[1])];
        this.polygonbounds.push(polygon_obj);
      }
      this.polygonbounds.push(this.polygonbounds[0]);
      console.log(this.polygonbounds);
    }
  }

  onMapReady(map) {
    this.map = map;
  }

  deleteSelectedOverlay() {
    if (this.selectedOverlay) {
      this.selectedOverlay.setMap(null);
      delete this.selectedOverlay;
    }
  }
}
