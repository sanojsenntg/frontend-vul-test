
import { ElementRef, NgZone, OnInit, ViewChild, Component, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { DrawingManager } from '@ngui/map';
import { NguiMapComponent } from '@ngui/map';
import { MatSelectModule } from '@angular/material/select';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ZoneService } from '../../../../../common/services/zones/zone.service';
import { JwtService } from '../../../../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

declare var $;

@Component({
  selector: 'app-zone-edit',
  templateUrl: './zone-edit.component.html',
  styleUrls: ['./zone-edit.component.css']
})

export class ZoneEditComponent implements OnInit {
  public companyId: any = [];
  public center: any;
  public drawFigureType;
  public recanglebounds: any = {};
  public polygonbounds: any = [];
  public triangleCode = [];
  public triangleCoords = [];
  public polyCoord;
  data = [];
  map;
  shape;
  public sub;
  public _id;
  onFigureCompletePopup = false;
  public paymentData;
  public allPaymentZone;
  public zone_id;
  selectedOverlay: any;
  public zonesave: any = false;
  public session;
  @ViewChild(DrawingManager) drawingManager: DrawingManager;

  constructor(private router: Router,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    public dialog: MatDialog,
    public overlay: Overlay,
    private route: ActivatedRoute,
    public _zoneService: ZoneService,
    public encDecService: EncDecService
  ) {
  }


  ngOnInit() {
    this.center = 'dubai';
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
      console.log(this._id)
      this.zone_id = {
        "zone_id": this._id,
        company_id: this.companyId
      }
    });
    var encrypted = this.encDecService.nwt(this.session, this.zone_id);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._zoneService.getZoneDetails(enc_data).then((dec) => {

      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.polyCoord = data.data.loc.coordinates[0];
          for (let i = 0; i < this.polyCoord.length - 1; i++) {
            var polyItem = this.polyCoord[i];
            var polygon_obj = { lat: parseFloat(polyItem[0]), lng: parseFloat(polyItem[1]) };
            this.triangleCode.push(polygon_obj);
          }
          this.triangleCoords.push(this.triangleCode);
          this.center = this.triangleCoords[0][0];
        } else {
          console.log(dec.message)
        }
      }
    });

    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', event => {
        if (event.type !== google.maps.drawing.OverlayType.MARKER) {
          dm.setDrawingMode(null);
          google.maps.event.addListener(event.overlay, 'click', e => {
            this.selectedOverlay = event.overlay;
            this.selectedOverlay.setEditable(true);
          });
          this.drawFigureType = event.type;
          this.selectedOverlay = event.overlay;
          this.onFigureCompletePopup = true;
          this.dialog.closeAll();
        }
      });
    });
    setTimeout(() => { }, 1000);

    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', event => {
        google.maps.event.addListener(event.overlay, 'click', e => {
          this.selectedOverlay = event.overlay;
          this.selectedOverlay.setEditable(true);
          var mapLength = this.selectedOverlay.getPath().getLength();
          this.zonesave = true;
          console.log(mapLength);
        });
      });
    });

  }
  onMapReady(map) {
    this.map = map;
  }

  public zoneSave(event) {
    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', even => {
        console.log(even)
      })
    })
    this.polygonbounds = [];
    for (var i = 0; i < this.selectedOverlay.getPath().getLength(); i++) {
      console.log(this.selectedOverlay.getPath().getAt(i).toUrlValue(6)) + "<br>";
      var polygon = this.selectedOverlay.getPath().getAt(i).toUrlValue(6);
      var splits = polygon.split(",");
      var polygon_obj = [parseFloat(splits[1]), parseFloat(splits[0])];
      this.polygonbounds.push(polygon_obj);
    }
    var params = { name: 'dsfjf', coordinates: this.polygonbounds }
    console.log(this.polygonbounds);
    // this._zoneService.saveZone(params).then((data) => {
    //   console.log(data)
    //   this.zonesave = false;
    // });
  }

  deleteSelectedOverlay() {
    if (this.selectedOverlay) {
      this.selectedOverlay.setMap(null);
      delete this.selectedOverlay;
    }
  }
}