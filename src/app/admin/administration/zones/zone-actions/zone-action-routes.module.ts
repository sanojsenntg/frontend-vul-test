import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ZoneListComponent } from './zone-list/zone-list.component';
import { ZoneAddComponent } from './zone-add/zone-add.component';
import { ZoneEditComponent } from './zone-edit/zone-edit.component';
import { AclAuthervice } from '../../../../common/services/access-control/acl-auth.service';
import { ZoneMultipleComponent } from './zone-multiple/zone-multiple.component';
export const ZoneActions:Routes =[
    {path:'', component:ZoneListComponent, canActivate: [AclAuthervice], data: {roles: ["Zone-List"]}},
    {path:'add', component:ZoneAddComponent, canActivate: [AclAuthervice], data: {roles: ["Zone-Add"]}},
    {path:'edit/:id', component:ZoneEditComponent, canActivate: [AclAuthervice], data: {roles: ["Zone-Edit"]}},
    {path:'multiple', component:ZoneMultipleComponent, canActivate:[AclAuthervice],data: {roles:["Zone-Add"]}}
] 

