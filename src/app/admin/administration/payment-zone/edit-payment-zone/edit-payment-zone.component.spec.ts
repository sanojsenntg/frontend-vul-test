import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPaymentZoneComponent } from './edit-payment-zone.component';

describe('EditPaymentZoneComponent', () => {
  let component: EditPaymentZoneComponent;
  let fixture: ComponentFixture<EditPaymentZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPaymentZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPaymentZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
