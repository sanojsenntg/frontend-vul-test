import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PaymentZoneService } from '../../../../common/services/paymentzone/paymentzone.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-payment-zone',
  templateUrl: './edit-payment-zone.component.html',
  styleUrls: ['./edit-payment-zone.component.css']
})
export class EditPaymentZoneComponent implements OnInit {
  public center: any;
  public companyId = [];
  public session;
  public paymentzone_id;
  public triangleCoords = [];
  public map;
  public aoiPoly;
  constructor(private route: ActivatedRoute, private _paymentZoneService: PaymentZoneService, public encDecService:EncDecService) { 
    this.route.params.subscribe((params) => {
      this.paymentzone_id = params['id'];
    });
  }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.center = 'The Dubai Mall';
  }

  onMapReady(map) {
    this.map = map;
    this.getPolgon();
  }
  getPolgon(){
    if (this.paymentzone_id) {
      const params = {
        company_id: this.companyId,
        _id:this.paymentzone_id
      }
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentZoneService.getPaymentZoneById(enc_data).then((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec.data);
            console.log(data);
            let paymentzone = data.PaymentZoneById;
            if (paymentzone.type == 'polygon') {
              this.triangleCoords[0] = paymentzone.bounds;
              console.log(this.triangleCoords[0]);
              this.center = this.triangleCoords[0][0];
              this.drawPolygon();
            }
          }
        }
      });
    }
  }
  drawPolygon(){
    this.aoiPoly = new google.maps.Polygon({ 
      paths: this.triangleCoords, 
      strokeColor: '#3fa4ff', 
      strokeOpacity: 0.8, 
      strokeWeight: 2, 
      fillColor: '#3fa4ff', 
      fillOpacity: 0.35, 
      clickable:true,
      editable:true
    }); 

    this.aoiPoly.setMap(this.map);
    google.maps.event.addListener(this.aoiPoly, 'click', function (event) {
      console.log(this.aoiPoly)
      console.log(event)
      console.log(event.getPath())
      var polygonBounds:any =this.aoiPoly.getPath();
      var bounds = [];
      for (var i = 0; i < polygonBounds.length; i++) {
            var point = {
              lat: polygonBounds.getAt(i).lat(),
              lng: polygonBounds.getAt(i).lng()
            };
            bounds.push(point);
       }
       console.log(bounds);
    });
  }
  public butonClk(){
    var polygonBounds:any = this.aoiPoly.getPath();
    var bounds = [];
    for (var i = 0; i < polygonBounds.length; i++) {
      var point = {
        "lat": polygonBounds.getAt(i).lat(),
        "lng": polygonBounds.getAt(i).lng()
      };
      bounds.push(point);
    }
    console.log(bounds);
  }
}
