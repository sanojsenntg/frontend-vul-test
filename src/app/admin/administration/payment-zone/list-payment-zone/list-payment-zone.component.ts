
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PaymentZoneService } from '../../../../common/services/paymentzone/paymentzone.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-list-payment-zone',
  templateUrl: './list-payment-zone.component.html',
  styleUrls: ['./list-payment-zone.component.css']
})

export class ListPaymentZoneComponent implements OnInit {
  public companyId = [];
  public paymentZoneData;
  public itemsPerPage = 10;
  public pageNo = 0;
  public paymentZoneLength;
  public is_search = false;
  public searchLoader = false;
  public payment_zone;
  public sortOrder = 'asc';
  public keyword = '';
  key: string = '';
  public zone_type = '';
  public paymentzone_name = '';
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public session;
  constructor(private _paymentZoneService: PaymentZoneService,
    private router: Router,
    public dialog: MatDialog,
    public overlay: Overlay,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public _aclService: AccessControlService,
    public jwtService: JwtService,
    public encDecService: EncDecService) {
    this.companyId.push(localStorage.getItem('user_company'));
    this.session = localStorage.getItem('Sessiontoken');
  }
  ngOnInit() {
    this.aclDisplayService();
    this.searchLoader = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: null,
      company_id: this.companyId,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentZoneService.getPaymentZones(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.paymentZoneData = data.paymentZone;
          this.paymentZoneLength = data.totalCount;
          this.searchLoader = false;
        } else {
          console.log(dec.message)
        }
      }
    });
  }
  public aclDisplayService() {
    const params = {
      company_id: this.companyId,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Drive through payment zones / Tolls -Add") {
              this.aclAdd = true;
            } else if (data.menu[i] == "Drive through payment zones / Tolls - View") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Drive through payment zones / Tolls - Delete") {
              this.aclDelete = true;
            }
          };
        }
      } else {
        console.log(dec.message)
      }

    })
  }
  /**
   * Confirmation popup for deleting particular payment zone record
   * @param id 
   */
  public deletePaymentZone(id) {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        const params = {
          company_id: this.companyId,
          _id: id
        };
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._paymentZoneService.updateDeletedStatus(enc_data)
          .then((dec) => {
            if (dec.status === 200) {
              this.refetchPaymentZone();
              this.paymentZoneData.splice(id, 1);
              this._toasterService.success('Payment Zone deleted successfully.');
            } else if (dec.status === 201) {
            }
          });
      } else {
      }
    });
  }

  /**
  * To refresh payment zone records showing in the grid
  */
  public refetchPaymentZone() {
    this.getPaymentZone(this.keyword);
  }

  /**
   * To reset the search filters and reload the payment zone records
   */
  public reset() {
    this.keyword = '';
    this.zone_type = '';
    this.paymentzone_name = '';
    this.ngOnInit();
  }

  /**
   * For sorting payment zone records 
   * @param key 
   */
  sort(key) {
    this.searchLoader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.sortOrder,
      sortByColumn: this.key,
      search: this.keyword,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentZoneService.getPaymentZones(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.paymentZoneData = data.paymentZone;
          this.searchLoader = false;
        }
      } else {
        console.log(dec.message)
      }

    });
  }

  /**
   * Pagination for payment zone module
   * @param data 
   */
  pagingAgent(data) {
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentZoneService.getPaymentZones(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.paymentZoneData = data.paymentZone;
        }
      } else {
        console.log(dec.message)
      }
    });
  }

  /**
   * For searching payment zone records 
   * @param keyword 
   */
  public getPaymentZone(keyword) {
    this.searchLoader = true;
    this.keyword = keyword;
    if (this.keyword) {
      const params = {
        offset: 0,
        limit: this.itemsPerPage,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId,
        search: this.keyword
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentZoneService.getPaymentZones(enc_data).then((dec) => {
        this.searchLoader = false;
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.paymentZoneData = data.paymentZone;
            this.paymentZoneLength = data.totalCount;
          } else {
            console.log(dec.message)
          }
        }

      });
    } else {
      const params = {
        offset: 0,
        limit: this.itemsPerPage,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentZoneService.getPaymentZones(enc_data).then((dec) => {
        this.searchLoader = false;
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.paymentZoneData = data.paymentZone;
            this.paymentZoneLength = data.totalCount;
          }else {
            console.log(dec.message)
          }
        } 
      });
    }
  }
}
