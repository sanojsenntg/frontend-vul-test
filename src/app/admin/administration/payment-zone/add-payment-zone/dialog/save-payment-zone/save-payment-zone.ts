import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverService } from '../../../../../../common/services/driver/driver.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { PaymentService } from '../../../../../../common/services/payment/payment.service';
import { PaymentZoneService } from '../../../../../../common/services/paymentzone/paymentzone.service';
import { JwtService } from '../../../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-save-payment-zone',
  templateUrl: './save-payment-zone.html',
  styleUrls: ['./save-payment-zone.css'],
})

export class SavePaymentZoneComponent implements OnInit {
    public addPaymentZone: FormGroup;
    public paymentZone;
    public paymentData;
    public companyId = localStorage.getItem('user_company');
    session;
    constructor(
    private _paymentZoneService: PaymentZoneService,
    private _paymentService: PaymentService,
    public dialogRef: MatDialogRef<SavePaymentZoneComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    public jwtService: JwtService,
    private formBuilder: FormBuilder,
    private router: Router,
    public encDecService:EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.paymentZone = data;
    this.companyId = localStorage.getItem('user_company');
    this.addPaymentZone = formBuilder.group({
        payment_extra: [''],
        company:[''],
        zone_type:[''],
        area_type:[''],
        type:[''],
        bounds:[''],
        rate:['',[
          Validators.required
        ]],
        name:['',[
          Validators.required
        ]],
        company_id: localStorage.getItem('user_company')
      });
  }

  ngOnInit() {
    this.companyId = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    const params = {
        offset: 0 ,
        limit:100,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        search:'',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentService.getPaymentListing(enc_data).subscribe((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec.data);
            this.paymentData = data.getPayment;
          }
        }
      });
  }

  savePaymentZone() {
    if (!this.addPaymentZone.valid) {
      return;
    }
    this.addPaymentZone.value.type = this.paymentZone.type;
    this.addPaymentZone.value.bounds = this.paymentZone.bounds;
    var encrypted = this.encDecService.nwt(this.session,this.addPaymentZone.value);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
    this._paymentZoneService.savePaymentType(enc_data).then((dec)=>{
    if(dec.status == 200) {
      this.toastr.success('Payment zone saved successfully.');
      this.router.navigate(['/admin/administration/payment-zone']);
      this.dialogRef.close(true);
    }
    });
  }
  closePop() {
    this.dialogRef.close(false);
  }


}
