import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPaymentZoneComponent } from './add-payment-zone.component';

describe('AddPaymentZoneComponent', () => {
  let component: AddPaymentZoneComponent;
  let fixture: ComponentFixture<AddPaymentZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPaymentZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPaymentZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
