import { ElementRef, NgZone, OnInit, ViewChild, Component, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { DrawingManager } from '@ngui/map';
import { NguiMapComponent } from '@ngui/map';
import { MatSelectModule } from '@angular/material/select';
import { SavePaymentZoneComponent } from './dialog/save-payment-zone/save-payment-zone';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { PaymentZoneService } from '../../../../common/services/paymentzone/paymentzone.service';

import { Overlay } from '@angular/cdk/overlay';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-add-payment-zone',
  templateUrl: './add-payment-zone.component.html',
  styleUrls: ['./add-payment-zone.component.css']
})
export class AddPaymentZoneComponent implements OnInit {
  public center: any;
  public companyId = [];
  public drawFigureType;
  public recanglebounds: any = {};
  public polygonbounds: any = [];
  triangleCoords = [];
  data = [];
  map;
  shape;
  onFigureCompletePopup = false;
  public paymentzone_id;
  public direction: any = {
    origin: 'penn station, new york, ny',
    destination: '260 Broadway New York NY 10007',
    travelMode: 'WALKING'
  };
  public paymentData;
  public allPaymentZone;
  public session;
  selectedOverlay: any;
  @ViewChild(DrawingManager) drawingManager: DrawingManager;

  constructor(
    public dialog: MatDialog,
    public overlay: Overlay,
    private route: ActivatedRoute,
    private _paymentZoneService: PaymentZoneService,
    public jwtService: JwtService,
    private router: Router,
    private _toasterService: ToastsManager,
    public encDecService:EncDecService
  ) {
  }


  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.center = 'The Dubai Mall';
    this.route.params.subscribe((params) => {
      this.paymentzone_id = params['id'];
      if (this.paymentzone_id) {
        const params = {
          company_id: this.companyId,
          _id:this.paymentzone_id
        }
        var encrypted = this.encDecService.nwt(this.session,params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._paymentZoneService.getPaymentZoneById(enc_data).then((dec) => {
          if(dec){
            if(dec.status == 200){
              var data:any = this.encDecService.dwt(this.session,dec.data);
              let paymentzone = data.PaymentZoneById;
              if (paymentzone.type == 'polygon') {
                this.triangleCoords[0] = paymentzone.bounds;
                console.log(this.triangleCoords[0]);
                this.center = this.triangleCoords[0][0];
              } else {
                setTimeout(() => {
                  let rectangle = new google.maps.Rectangle({
                    strokeColor: '#57ACF9',
                    strokeOpacity: 1,
                    strokeWeight: 3,
                    fillColor: '#BCDCF9',
                    fillOpacity: 0.75,
                    map: this.map,
                    bounds: paymentzone.bounds[0]
                  });
                  console.log('bounds', paymentzone.bounds[0])
                  this.center = { lat: paymentzone.bounds[0].north, lng: paymentzone.bounds[0].east };
                }, 1000);
              }
            }
          }
        });
      } else {
        console.log('AA');
        const params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'updated_at',
          company_id: this.companyId,
          search:null,
        };
        var encrypted = this.encDecService.nwt(this.session,params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._paymentZoneService.getPaymentZones(enc_data).then((dec) => {
          if(dec){
            if(dec.status == 200){
              var data:any = this.encDecService.dwt(this.session,dec.data);
              this.allPaymentZone = data.paymentZone;
              if (this.allPaymentZone) {
    
                this.allPaymentZone.forEach((zones, index) => {
                  console.log(zones, index);
                  if (zones.type == 'polygon') {
                    this.triangleCoords.push(zones.bounds);
                  } else {
                    setTimeout(() => {
                      let rectangle = new google.maps.Rectangle({
                        strokeColor: '#57ACF9',
                        strokeOpacity: 1,
                        strokeWeight: 3,
                        fillColor: '#BCDCF9',
                        fillOpacity: 0.75,
                        map: this.map,
                        bounds: zones.bounds[0]
                      });
                    }, 1000);
                  }
                });
              }
            }
          }else{
            console.log(dec.message)
          }
        })
      }
    });

    this.drawingManager['initialized$'].subscribe(dm => {
      google.maps.event.addListener(dm, 'overlaycomplete', event => {
        if (event.type !== google.maps.drawing.OverlayType.MARKER) {
          dm.setDrawingMode(null);
          google.maps.event.addListener(event.overlay, 'click', e => {
            this.selectedOverlay = event.overlay;
            this.selectedOverlay.setEditable(true);
          });

          this.drawFigureType = event.type;
          this.selectedOverlay = event.overlay;
          this.onFigureCompletePopup = true;
          this.dialog.closeAll();

          if (this.drawFigureType == "polygon") {
            for (var i = 0; i < this.selectedOverlay.getPath().getLength(); i++) {
              console.log(this.selectedOverlay.getPath().getAt(i).toUrlValue(6)) + "<br>";
              var polygon = this.selectedOverlay.getPath().getAt(i).toUrlValue(6);
              var splits = polygon.split(",");
              var polygon_obj = { lat: parseFloat(splits[0]), lng: parseFloat(splits[1]) };
              this.polygonbounds.push(polygon_obj);
              console.log(this.polygonbounds.push)
            }
            // let dialogRef = this.dialog.open(SavePaymentZoneComponent, {
            //   width: '600px',
            //   scrollStrategy: this.overlay.scrollStrategies.noop(),
            //   data: { 'type': 'polygon', bounds: this.polygonbounds }
            // });
            // dialogRef.afterClosed().subscribe(result => {
            //   var that = this;
            //   if (result) {
            //   } else {
            //     that.deleteSelectedOverlay();
            //   }
            // });
          } else {
            var bounds = this.selectedOverlay.getBounds();
            var aNorth = parseFloat(bounds.getNorthEast().lat());
            var aEast = parseFloat(bounds.getNorthEast().lng());
            var aSouth = parseFloat(bounds.getSouthWest().lat());
            var aWest = parseFloat(bounds.getSouthWest().lng());
            console.log(aNorth);
            console.log(aSouth);
            console.log(aEast);
            console.log(aWest);
            this.recanglebounds = {
              north: aNorth,
              south: aSouth,
              east: aEast,
              west: aWest
            };
            // let dialogRef = this.dialog.open(SavePaymentZoneComponent, {
            //   width: '600px',
            //   scrollStrategy: this.overlay.scrollStrategies.noop(),
            //   data: { 'type': 'rectangle', bounds: this.recanglebounds }
            // });
            // dialogRef.afterClosed().subscribe(result => {
            //   var that = this;
            //   if (result) {

            //   } else {
            //     that.deleteSelectedOverlay();
            //   }
            // });
          }
        }
      });
    });
    setTimeout(() => { }, 1000);
  }
  deleteSelectedOverlay() {
    if (this.selectedOverlay) {
      this.selectedOverlay.setMap(null);
      delete this.selectedOverlay;
    }
  }
  onMapReady(map) {
    this.map = map;
  }
  addPolygon() {
    this.triangleCoords[1] = [
      { lat: 25.567826, lng: 55.677585 },
      { lat: 25.484797, lng: 56.155491 },
      { lat: 25.211761, lng: 55.882206 },
      { lat: 25.426518, lng: 55.766849 },
      { lat: 25.389304, lng: 55.532016 }
    ];
    this.data.push(this.triangleCoords);
  }
  addRectangle() {
    let rectangle = new google.maps.Rectangle({
      strokeColor: '#57ACF9',
      strokeOpacity: 1,
      strokeWeight: 3,
      fillColor: '#BCDCF9',
      fillOpacity: 0.75,
      map: this.map,
      bounds: {
        north: 25.28671103683655,
        south: 25.169934709279143,
        east: 55.81863572363284,
        west: 55.49041917089846
      }
    });
    google.maps.event.addListener(rectangle, 'click', function (event) {
      console.log(event);
      rectangle.setMap(null);
    });
  }
  close_popup() {
    this.onFigureCompletePopup = false;
  }
}
