import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { Subscription } from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { Categories } from '../../../../common/services/categories/categories.service';
import { environment } from '../../../../../environments/environment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-sub-categories',
  templateUrl: './edit-sub-categories.component.html',
  styleUrls: ['./edit-sub-categories.component.css']
})
export class EditVehicleCategoriesComponent implements OnInit {
  public editForm: FormGroup;
  private sub: Subscription;
  public data;
  public _id;
  public imageUrl = '';
  public categoriesData;
  public spandata = 'yes';
  public companyId: any = [];
  public session;
  constructor(private _categoryService: Categories,
    private route: ActivatedRoute,
    private router: Router,
    formBuilder: FormBuilder,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public jwtService: JwtService, public encDecService: EncDecService) {
    this.editForm = formBuilder.group({
      category_id: ['', Validators.required],
      sub_category: ['', Validators.required],
      persons: ['', Validators.required],
      rate: ['', Validators.required],
      description: ['', Validators.required],
      image: ['']
    });
  }
  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.getCategory();
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    this.spandata = 'no';
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoryService.getSubCategoryById(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          this.data = res;
          if (this.data.image) {
            this.imageUrl = environment.imgUrl + '/uploads/vehiclemodels/' + this.data.image;
          }
          this.editForm.setValue({
            category_id: res.category_id ? res.category_id._id : '',
            sub_category: res.sub_category ? res.sub_category : '',
            persons: res.persons ? res.persons : '',
            description: res.description ? res.description : '',
            rate: res.rate ? res.rate : '',
            image: ''
          });
        }
      } else {
        console.log(dec.message)
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {

      }
      console.log(error);
    });

  }

  /**
   * Update vehicle categories data
   *
   * @param formData
   */

  public updateVehicleCategories() {
    if (this.editForm.valid) {
      const vehicleCategoriesData = {
        category_id: this.editForm.value.category_id,
        sub_category: this.editForm.value.sub_category,
        persons: this.editForm.value.persons,
        rate: this.editForm.value.rate,
        description: this.editForm.value.description,
        image: this.editForm.value.image,
        company_id: localStorage.getItem('user_company'),
        _id: this._id
      };
      var encrypted = this.encDecService.nwt(this.session, vehicleCategoriesData);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._categoryService.updateCategories(enc_data)
        .then((dec) => {
          if (dec.status == 200) {
            this._toasterService.success('Vehicle sub category updated successfully.');
            this.router.navigate(['/admin/administration/vehicle-sub-categories']);
          } else if (dec.status == 201) {
            this._toasterService.error('Vehicle sub category updation failed.');
          }
          else {
            this._toasterService.warning('Sub category name already exist');
          }
        })
        .catch((error) => {
          console.log('error', error);
        });
    } else {
      return;
    }
  }

  imageFinishedUploading(file: FileHolder) {
    this.editForm.controls['image'].setValue(file.src);
    this.spandata = 'yes';

  }

  onRemoved(file: FileHolder) {
    this.editForm.controls['image'].setValue(null);
    this.spandata = 'no';

  }

  onUploadStateChanged(state: boolean) {

  }

  /**
   * Function To get categories For Dropdown
   *
   */
  public getCategory() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoryService.getCategories(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.categoriesData = data.getCategory;
        }
        else {
          console.log(dec.message)
        }
      }
    });
  }
}
