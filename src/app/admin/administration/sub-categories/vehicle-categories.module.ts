import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Categories } from './../../../common/services/categories/categories.service';
import { CategoriesrestService } from './../../../common/services/categories/categoriesrest.service';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListVehicleCategoriesComponent } from '../sub-categories/list-sub-categories/list-sub-categories.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { ImageUploadModule } from 'angular2-image-upload';
import { AddVehicleCategoriesComponent } from './add-sub-categories/add-sub-categories.component';
import { EditVehicleCategoriesComponent } from './edit-sub-categories/edit-sub-categories.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    Ng2OrderModule,
    MatTooltipModule,
    ImageUploadModule,
    MatAutocompleteModule,
    MatSelectModule
  ],
  declarations: [ 
    ListVehicleCategoriesComponent, AddVehicleCategoriesComponent, EditVehicleCategoriesComponent, 
   
  ],
  providers: [ 
    Categories,CategoriesrestService
  ],
  exports: [
    RouterModule
  ],
})
export class VehicleCategoriesModule { }
