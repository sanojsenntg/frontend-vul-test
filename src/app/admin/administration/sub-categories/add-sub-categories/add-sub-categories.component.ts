import { Component, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Categories } from '../../../../common/services/categories/categories.service';

import { ToastsManager } from 'ng2-toastr';
import { ActivatedRoute } from '@angular/router';
import { FileHolder } from 'angular2-image-upload';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { environment } from '../../../../../environments/environment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-sub-categories',
  templateUrl: './add-sub-categories.component.html',
  styleUrls: ['./add-sub-categories.component.css']
})
export class AddVehicleCategoriesComponent {
  public companyId: any = [];
  public VehicleData;
  public driverGroupData;

  private sub: Subscription;
  public ShiftTimingsData;
  public categoriesData;
  public _id;
  public dateObj = new Date();
  public spandata = 'no';
  public saveForm: FormGroup;

  public data;
  public imageUrl = '';
  public session;

  constructor(private _categoryService: Categories,
    private _toasterService: ToastsManager,
    private router: Router,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private vcr: ViewContainerRef,
    public encDecService: EncDecService,
    public jwtService: JwtService) {

    this.saveForm = formBuilder.group({
      category_id: ['', [
        Validators.required,

      ]],
      sub_category: ['', [
        Validators.required,

      ]],
      image: ['', Validators.required,],
      persons: ['', [
        Validators.required,

      ]],
      description: ['', [
        Validators.required,

      ]],
      rate: ['', [
        Validators.required,

      ]],
    });
  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.getCategories();
  }

  /**
    * Save vehicle categories
    */
  addVehicleCategory() {

    if (!this.saveForm.valid) {
      return;
    }
    const vehicleModel = {
      category_id: this.saveForm.value.category_id,
      sub_category: this.saveForm.value.sub_category,
      image: this.saveForm.value.image,
      persons: this.saveForm.value.persons,
      description: this.saveForm.value.description,
      rate: this.saveForm.value.rate,
      company_id: localStorage.getItem('user_company')
    };
    var encrypted = this.encDecService.nwt(this.session, vehicleModel);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoryService.addCategories(enc_data)
      .then((dec) => {
        if (dec.status == 200) {
          this._toasterService.success('Vehicle sub category added successfully.');
          this.router.navigate(['/admin/administration/vehicle-sub-categories']);
        }
        else if (dec.status === 201) {
          this._toasterService.error('Vehicle sub category addition failed.');
        }
        else {
          this._toasterService.warning('Sub category name already exist');
        }
      }).catch((error) => {
      });

  }

  imageFinishedUploading(file: FileHolder) {
    this.saveForm.controls['image'].setValue(file.src);
    this.spandata = 'yes';
  }

  onRemoved(file: FileHolder) {
    this.saveForm.controls['image'].setValue(null);
    this.spandata = 'no';
  }

  onUploadStateChanged(state: boolean) {

  }


  public getCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'category',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoryService.getCategories(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var servicedata: any = this.encDecService.dwt(this.session, dec.data);
          this.categoriesData = servicedata.getCategory;
        } else {
          console.log(dec.message)
        }
      }

    });
  }
}
