import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVehicleCategoriesComponent } from './add-vehicle-categories.component';

describe('AddVehicleCategoriesComponent', () => {
  let component: AddVehicleCategoriesComponent;
  let fixture: ComponentFixture<AddVehicleCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVehicleCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVehicleCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
