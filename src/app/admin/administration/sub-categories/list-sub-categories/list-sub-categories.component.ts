import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Categories } from '../../../../common/services/categories/categories.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { environment } from '../../../../../environments/environment';
import { Subscription } from 'rxjs/Subscription';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-sub-categories',
  templateUrl: './list-sub-categories.component.html',
  styleUrls: ['./list-sub-categories.component.css']
})
export class ListVehicleCategoriesComponent implements OnInit {
  key: string = '';
  public companyId: any = [];
  public categoryDatas;
  public sub_category_id;
  public sub_category = '';
  public categoryData;
  public Datas;
  public category_id: any;
  private sub: Subscription;
  public id;
  public i;
  public Data;
  public _id = '';
  itemsPerPage = 10;
  pageNo = 0;
  categoryLength;
  public payment_type;
  public is_search = false;
  public imageUrl;
  searchValue;
  sortOrder = 'asc';
  public keyword = '';
  public searchLoader = false;
  del = false;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public session
  constructor(private _categoriesService: Categories,
    private router: Router,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    public overlay: Overlay,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public _aclService: AccessControlService,
    public jwtService: JwtService,
    public encDecService: EncDecService) {

  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.aclDisplayService();
    this.getCategoryName();
    this.imageUrl = environment.imgUrl;
    this.is_search = false;
    this.searchLoader = true;

    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.get(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.categoryDatas = data.getSubCategories;
          this.categoryLength = data.count;
        }
      } else {
        console.log(dec.message)
      }
      this.searchLoader = false;
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Vehicle Sub Categories - Add") {
              this.aclAdd = true;
            } else if (data.menu[i] == "Vehicle Sub Categories - Edit") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Vehicle Sub Categories - Delete") {
              this.aclDelete = true;
            }
          };
        }
      } else {
        console.log(dec.message)
      }
    })
  }
  /**
    * Confirmation popup for deleting particular vehicle categories record
    * @param id 
    */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '350px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this data' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.delete(id);
      } else {
      }
    });
  }

  /**
   * Delete vehicle categories from view
   * @param id 
   */
  public delete(id) {
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.updateCategoryForDeletion(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            this.refresh();
            this.categoryDatas.splice(id, 1);
            this._toasterService.success('Vehicle sub category deleted successfully.');
          }
        } else {
          console.log(dec.message)
        }
      })
  }

  /**
  * For searching category records
  */
  public searchRecords() {

    this.searchLoader = true;
    this.is_search = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      sub_category: this.sub_category ? this.sub_category : '',
      category_id: this.category_id ? this.category_id._id : '',
      company_id: this.companyId,
    };

    this.del = true;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.get(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.categoryDatas = data.getSubCategories
          this.categoryLength = data.count;
          this.searchLoader = false;
          this.del = false;
        }
      } else {
        console.log(dec.message)
      }
    });
  }

  /**
   * Load list of sub category name in drop down on page load.
   */
  public getSubCategoryName() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.get(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.Datas = data.getSubCategories;
        }
      } else {
        console.log(dec.message)
      }
    });
  }

  /**
  * Load list of category name in drop down on page load.
  */
  public getCategoryName() {
    let params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.getCategories(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.Data = data.getCategory;
        } else {
          console.log(dec.message)
        }
      }
    });
  }


  /**
   * Autocomplete search for category name
   * @param data
   */
  searchByCategoryName(data) {
    if (typeof data !== 'object') {
      let params = {
        offset: 0,
        limit: 0,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        'category_id': data
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._categoriesService.searchCategoyByName(enc_data).subscribe((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.Data = data.searchResults;
          }
        } else {
          console.log(dec.message)
        }

      });
    } else {
      this.category_id = data._id;
    }
  }

  /**
   * Displaying name of category in autocomplete search for category dropdown.
   * @param data
   */
  displayFnCategories(data): string {
    return data ? data.category : data;
  }

  /**
  * To refresh the page
  */
  refresh() {
    if (this.is_search == true) {
      this.searchRecords();
    }
    else {
      this.ngOnInit();
    }
  }

  /**
   * Reset the search filters and refresh the page.
   */
  reset() {
    this.del = true;
    this.category_id = '';
    this.sub_category = '';
    this.ngOnInit();
  }

  /**
   * For sorting vehicle categories records 
   * @param key 
   */
  sort(key) {
    this.searchLoader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
    var params;
    if (this.is_search == true) {
      params = {
        offset: 0,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        sub_category: this.sub_category ? this.sub_category : '',
        category_id: this.category_id ? this.category_id._id : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: 0,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId
      }
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.get(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.categoryDatas = data.getSubCategories;
        }
      } else {
        console.log(dec.message)
      }
      this.searchLoader = false;
    });
  }

  /**
   * Pagination for vehicle categories module
   * @param data 
   */
  pagingAgent(data) {
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.get(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.categoryDatas = data.getSubCategories;
        }
      } else {
        console.log(dec.message)
      }

    });
  }
}
