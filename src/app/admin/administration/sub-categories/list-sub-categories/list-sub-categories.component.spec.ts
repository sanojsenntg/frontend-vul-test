import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListVehicleCategoriesComponent } from './list-vehicle-categories.component';

describe('ListVehicleCategoriesComponent', () => {
  let component: ListVehicleCategoriesComponent;
  let fixture: ComponentFixture<ListVehicleCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListVehicleCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListVehicleCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
