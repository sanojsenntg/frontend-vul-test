import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../../common/services/customer/customer.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-ticket-edit',
  templateUrl: './ticket-edit.component.html',
  styleUrls: ['./ticket-edit.component.css']
})
export class TicketEditComponent implements OnInit {

  companyId = [];
  session;
  showerror = false;
  uniqueId;
  uniqeIdRecived = '';
  showLoader = false;
  tagData: any = [];
  orderSearch = false;
  searchForm: FormGroup;
  zenDeskData: any;
  zendeskId;

  constructor(public ticketService: CustomerService, public encDecService: EncDecService, public formBuilder: FormBuilder, public toastr: ToastsManager,public route: ActivatedRoute, public router:Router) {
    this.route.params.subscribe( params => this.zendeskId = params.id );
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.searchForm = this.formBuilder.group({
      unique_order_id: [{ value: '', disabled: true }],
      zendesk_id: [{ value: '', disabled: true }],
      start_date: [{ value: '', disabled: true }],
      end_date: [{ value: '', disabled: true }],
      email: [{ value: '', disabled: true }],
      firstname: [{ value: '', disabled: true }],
      phone_number: [{ value: '', disabled: true }],
      order_status: [{ value: '', disabled: true }],
      created_at: [{ value: '', disabled: true }],
      updated_at: [{ value: '', disabled: true }],
      complaint: ['', [Validators.required]],
      issue_identified: ['', [Validators.required]],
      operations_response: ['', [Validators.required]],
      action_for_oper_resp: ['', [Validators.required]],
      customer_feedback: ['', [Validators.required]],
      action_taked: ['', [Validators.required]],
      driver_name: [{ value: '', disabled: true }],
      description: [{ value: '', disabled: true }],
      driver_id: [{ value: '', disabled: true }]
    });
  }
  ngOnInit() {
    this.ticketFind();
  }
  saveUser(formData) {
    if(!formData.valid){
      return;
    }
    var params = {
      zendeskEvaluationId:this.zenDeskData._id,
      zendesk_id: this.zenDeskData.zendesk_id,
      zen_id: this.searchForm.get('zendesk_id').value,
      order_id: this.zenDeskData.order_id,
      unique_order_id: this.zenDeskData.unique_order_id,
      status: this.searchForm.get('order_status').value,
      tags: this.zenDeskData.tags,
      order_created_date: this.searchForm.get('created_at').value,
      order_updated_date: this.searchForm.get('updated_at').value,
      order_start_time: this.searchForm.get('start_date').value,
      order_end_time: this.searchForm.get('end_date').value,
      customer_name: this.searchForm.get('firstname').value,
      customer_phone_number: this.zenDeskData.customer_phone_number,
      country_code: this.zenDeskData.country_code,
      customer_email: this.searchForm.get('email').value,
      customer_id: this.zenDeskData.customer_id,
      driver_name: this.searchForm.get('driver_name').value,
      driver_username: this.searchForm.get('driver_id').value,
      driver_id: this.zenDeskData.driver_id,
      complaint: this.searchForm.get('description').value,
      issue_by_sup_team: this.searchForm.get('issue_identified').value,
      oper_resp: this.searchForm.get('operations_response').value,
      action_for_oper_resp: this.searchForm.get('action_for_oper_resp').value,
      feedback_from_cust: this.searchForm.get('customer_feedback').value,
      action_for_feedback_from_cust: this.searchForm.get('action_taked').value,
      company_id: this.companyId
    };
    console.log(params);
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.ticketService.zendeskEdit(enc_data).then((dec) => {
      if(dec.status == 200){
        this.toastr.success('Ticket Edit Successfull.')
        this.router.navigate(['/admin/administration/ticket']);
      }else{
        this.toastr.error(dec.message);
      }
    })
  }
  ticketFind() {
    this.showLoader = true;
    const params = {
      id:this.zendeskId,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.ticketService.zendeskId(enc_data).then((dec) => {
      this.showLoader = false;
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.zenDeskData = data.data;
        console.log(this.zenDeskData);
        this.searchForm.setValue({
          start_date: this.zenDeskData.order_start_time,
          end_date: this.zenDeskData.order_end_time,
          email: this.zenDeskData.customer_email,
          firstname: this.zenDeskData.customer_name,
          phone_number: '+' +this.zenDeskData.customer_country_code + '-' + this.zenDeskData.customer_phone_number,
          order_status: this.zenDeskData.status,
          created_at: this.zenDeskData.order_created_date,
          updated_at: this.zenDeskData.order_updated_date,
          unique_order_id: this.zenDeskData.unique_order_id,
          zendesk_id: this.zenDeskData.zen_id,
          driver_name: this.zenDeskData.driver_name,
          complaint: this.zenDeskData.complaint,
          issue_identified: this.zenDeskData.issue_by_sup_team,
          operations_response: this.zenDeskData.oper_resp,
          customer_feedback: this.zenDeskData.feedback_from_cust,
          action_taked: this.zenDeskData.action_for_feedback_from_cust,
          description: this.zenDeskData.complaint,
          driver_id: this.zenDeskData.driver_username,
          action_for_oper_resp: this.zenDeskData.action_for_oper_resp
        });
        this.tagData = this.zenDeskData.tags;
      }
    })
  }

}
