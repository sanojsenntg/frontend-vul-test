import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../../common/services/customer/customer.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Router} from "@angular/router";


@Component({
  selector: 'app-ticket-add',
  templateUrl: './ticket-add.component.html',
  styleUrls: ['./ticket-add.component.css']
})
export class TicketAddComponent implements OnInit {
  public ticket: any = {
    zendesk_id: '',
    unique_order_id: ''
  }
  companyId = [];
  session;
  showerror = false;
  uniqueId;
  uniqeIdRecived = '';
  showLoader = false;
  tagData: any = [];
  orderSearch = false;
  searchForm: FormGroup;
  zenDeskData:any;

  constructor(public ticketService: CustomerService, public encDecService: EncDecService, public formBuilder: FormBuilder, public toastr: ToastsManager, public router:Router) {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.searchForm = this.formBuilder.group({
      unique_order_id: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(9)]],
      zendesk_id: ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(4)]],
      start_date: [{ value: '', disabled: true }],
      end_date: [{ value: '', disabled: true }],
      email: [{ value: '', disabled: true }],
      firstname: [{ value: '', disabled: true }],
      phone_number: [{ value: '', disabled: true }],
      order_status: [{ value: '', disabled: true }],
      created_at: [{ value: '', disabled: true }],
      updated_at: [{ value: '', disabled: true }],
      complaint: ['', [Validators.required]],
      issue_identified: ['', [Validators.required]],
      operations_response: ['', [Validators.required]],
      action_for_oper_resp: ['', [Validators.required]],
      customer_feedback: ['', [Validators.required]],
      action_taked: ['', [Validators.required]],
      driver_name: [{ value: '', disabled: true }],
      description: [{ value: '', disabled: true }],
      driver_id: [{ value: '', disabled: true }]
    });
  }
  ngOnInit() {
    this.onChanges();
  }
  onChanges(): void {
    this.searchForm.get('unique_order_id').valueChanges.debounceTime(500).subscribe(val => {
      this.uniqueId = val;
      var zendeskId = this.searchForm.controls['zendesk_id'].value;
      if (zendeskId && this.uniqueId.length >= 9) {
        this.ticketFind(this.uniqueId, zendeskId);
      }
    });
    this.searchForm.get('zendesk_id').valueChanges.debounceTime(500).subscribe(val => {
      this.uniqueId = this.searchForm.controls['unique_order_id'].value;
      var zendeskId = val;
      if (this.uniqueId && zendeskId.length >= 4) {
        this.ticketFind(this.uniqueId, zendeskId);
      }
    });
  }
  saveUser(formData) {
    console.log(formData.valid)
    console.log(formData)
    if(!formData.valid){
      return;
    }
    var params = {
      zendesk_id: this.zenDeskData.zendeskData._id,
      zen_id: this.searchForm.get('zendesk_id').value,
      order_id: this.zenDeskData.orderData._id,
      unique_order_id: this.zenDeskData.orderData.unique_order_id,
      status: this.searchForm.get('order_status').value,
      tags: this.zenDeskData.zendeskData.tags,
      order_created_date: this.searchForm.get('created_at').value,
      order_updated_date: this.searchForm.get('updated_at').value,
      order_start_time: this.searchForm.get('start_date').value,
      order_end_time: this.searchForm.get('end_date').value,
      customer_name: this.searchForm.get('firstname').value,
      customer_phone_number: this.zenDeskData.orderData.user_id.phone_number,
      country_code: this.zenDeskData.orderData.user_id.country_code,
      customer_email: this.searchForm.get('email').value,
      customer_id: this.zenDeskData.orderData.user_id._id,
      driver_name: this.searchForm.get('driver_name').value,
      driver_username: this.searchForm.get('driver_id').value,
      driver_id: this.zenDeskData.orderData.driver_id._id,
      complaint: this.searchForm.get('description').value,
      issue_by_sup_team: this.searchForm.get('issue_identified').value,
      oper_resp: this.searchForm.get('operations_response').value,
      action_for_oper_resp: this.searchForm.get('action_for_oper_resp').value,
      feedback_from_cust: this.searchForm.get('customer_feedback').value,
      action_for_feedback_from_cust: this.searchForm.get('action_taked').value,
      company_id: this.companyId
    };
    console.log(params);
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.ticketService.zendeskAdd(enc_data).then((dec) => {
      if(dec.status == 200){
        this.toastr.success('Ticket added successfully.');
        this.router.navigate(['/admin/administration/ticket']);
      }else{
        this.toastr.error(dec.message);
      }
    })

  }
  ticketFind(uniqueId, zendeskId) {
    if (this.uniqeIdRecived == this.uniqueId) {
      return;
    }
    this.showLoader = true;
    const params = {
      "unique_order_id": uniqueId,
      "zendesk_id": zendeskId,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.ticketService.zendeskDetails(enc_data).then((dec) => {
      this.showLoader = false;
      if (dec.status == 200) {
        this.orderSearch = true;
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.zenDeskData = data;
        this.searchForm.setValue({
          start_date: data.orderData.start_time,
          end_date: data.orderData.end_time,
          email: data.orderData.user_id.email,
          firstname: data.orderData.user_id.firstname,
          phone_number: '+' + data.orderData.user_id.country_code + '-' + data.orderData.user_id.phone_number,
          order_status: data.orderData.order_status,
          created_at: data.orderData.created_at,
          updated_at: data.orderData.updated_at,
          unique_order_id: data.orderData.unique_order_id,
          zendesk_id: data.zendeskData.id,
          driver_name: data.orderData.driver_id.display_name,
          complaint: data.zendeskData.description,
          issue_identified: '',
          operations_response: '',
          customer_feedback: '',
          action_taked: '',
          description: data.zendeskData.description,
          driver_id: data.orderData.driver_id.username,
          action_for_oper_resp:''
        });
        this.tagData = data.zendeskData.tags;
        this.uniqeIdRecived = this.searchForm.controls['unique_order_id'].value;
      } else {

        this.orderSearch = false;
        this.toastr.error(dec.message);
        this.searchForm.setValue({
          start_date: '',
          end_date: '',
          email: '',
          firstname: '',
          phone_number: '',
          order_status: '',
          created_at: '',
          updated_at: '',
          unique_order_id: this.searchForm.controls['unique_order_id'].value,
          zendesk_id: this.searchForm.controls['zendesk_id'].value,
          complaint: '',
          driver_name: '',
          issue_identified: '',
          operations_response: '',
          customer_feedback: '',
          action_taked: '',
          description: '',
          driver_id: '',
          action_for_oper_resp:''
        });
        this.uniqeIdRecived = this.searchForm.controls['unique_order_id'].value;
      }
    })
  }

}
