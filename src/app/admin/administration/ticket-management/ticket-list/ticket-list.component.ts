import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from '../../../../common/services/customer/customer.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as moment from 'moment/moment';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { Overlay } from '@angular/cdk/overlay';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})
export class TicketListComponent implements OnInit {
  public startDate: any=new Date(Date.now() - 604800000);
  public endDate: any=new Date(Date.now());
  max = new Date(Date.now());
  companyId = [];
  session;
  zendeskList;
  pageSize = 10;
  pageNo = 0;
  listCount;
  del = false;
  zendeskId;
  uniqueId;
  phone;
  driverId
  searchSubmit = false;
  params;
  aclAdd;
  aclEdit;
  aclDelete;

  constructor(public ticketService:CustomerService, public encDecService: EncDecService, public router:Router, public overlay:Overlay, public dialog:MatDialog, public _aclService:AccessControlService) {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.aclDisplayService();
   }

  ngOnInit() {
    this.searchOrder()
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Zendesk Ticket - Management -Add") {
              this.aclAdd = true;
            } else if (data.menu[i] == "Zendesk Ticket - Management -Edit") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Zendesk Ticket - Management -Delete") {
              this.aclDelete = true;
            }
          };
        }
      } else {
        console.log(dec.message)
      }
    })
  }
  searchReset(){
    this.zendeskId = null;
    this.startDate = new Date(Date.now() - 604800000);
    this.endDate = new Date(Date.now());
    this.phone = null;
    this.driverId = null;
    this.searchSubmit = true;
    
    const params = {
      zen_id:this.zendeskId,
      unique_order_id:this.uniqueId,
      startDate:this.startDate,
      endDate:this.endDate,
      offset: 0,
      phone:this.phone,
      driver_id: this.driverId,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.ticketService.zendeskList(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.zendeskList = data.data;
        this.listCount = data.count;
      }
    })
  }
  searchRefresh(){
    this.searchSubmit = true;
    var encrypted = this.encDecService.nwt(this.session, this.params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.ticketService.zendeskList(enc_data).then((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.zendeskList = data.data;
        this.listCount = data.count;
      }
    })
  }
  searchOrder(){  
    this.searchSubmit = true;
    this.params = {
      zen_id:this.zendeskId,
      unique_order_id:this.uniqueId,
      startDate:this.startDate ? moment(this.startDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') :'',
      endDate:this.endDate ? moment(this.endDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') :'',
      phone:this.phone ? this.phone : '',
      driver_id: this.driverId ? this.driverId : '',
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, this.params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.ticketService.zendeskList(enc_data).then((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.zendeskList = data.data;
        this.listCount = data.count;
      }
    })
  }
  deleteOrder(event){
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        var params = {
          zendeskEvaluationId:event.zendesk_id,
          company_id: this.companyId
        };
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this.ticketService.zendeskDelete(enc_data).then((dec) => {
          this.searchSubmit = false;
          this.searchOrder();
        })
      } else {
      }
    });
  }
  pagingAgent(data) {
    this.pageNo = (data * this.pageSize) - this.pageSize;
    var params = {
      zen_id:this.zendeskId,
      unique_order_id:this.uniqueId,
      startDate:this.startDate ? moment(this.startDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') :'',
      endDate:this.endDate ? moment(this.endDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') :'',
      phone:this.phone ? this.phone : '',
      driver_id: this.driverId ? this.driverId : '',
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.ticketService.zendeskList(enc_data).then((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.zendeskList = data.data;
        this.listCount = data.count;
      }
    })
  }

  public createCsv() {
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.ticketService.zendeskList(enc_data).then((dec) => {
      //this.showLoader = true;
      //this.vehicleData = data.searchVehicle;
      //this.vehicleLength = data.count;
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        let labels = [
          'Ticket No',
          'Order No',
          'Order Created Date',
          'Customer Name',
          'Customer Email',
          'Customer Phone',
          'Driver Name',
          'Driver Id',
          'Order End Time',
          'Complaint',
          'Issue by sup team',
          'Operator response',
          'Feedback from customer',
          'Action for feedback from customer',
          'Order Updated At',
        ];
        let resArray = [];

        for (let i = 0; i < res.data.length; i++) {
          const csvArray = res.data[i];

          resArray.push
            ({
              zen_id: csvArray.zen_id ? csvArray.zen_id : 'N/A',
              unique_order_id: csvArray.unique_order_id ? csvArray.unique_order_id : 'N/A',
              order_start_time: csvArray.order_start_time ? csvArray.order_start_time : 'N/A',
              customer_name: csvArray.customer_name ? csvArray.customer_name : 'N/A',
              customer_email: csvArray.customer_email ? csvArray.customer_email : 'N/A ',
              customer_phone: csvArray.customer_phone ? csvArray.customer_phone : 'N/A',
              driver_name: csvArray.driver_name ? csvArray.driver_name : 'N/A',
              driver_username: csvArray.driver_username ? csvArray.driver_username : 'N/A',
              order_end_time: csvArray.order_end_time ? csvArray.order_end_time : 'N/A',
              complaint: csvArray.complaint ? csvArray.complaint : 'N/A',
              issue_by_sup_team: csvArray.issue_by_sup_team ? csvArray.issue_by_sup_team : 'N/A',
              oper_resp: csvArray.oper_resp ? csvArray.oper_resp : 'N/A ',
              feedback_from_cust: csvArray.feedback_from_cust ? csvArray.feedback_from_cust : 'Not Added',
              action_for_feedback_from_cust: csvArray.action_for_feedback_from_cust ? csvArray.action_for_feedback_from_cust : 'N/A',
              updated_at: csvArray.updated_at ? csvArray.updated_at : 'Not Added'
            });
        }
        var options =
        {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: false,
          useBom: true,
          headers: (labels)
        };
        new Angular2Csv(resArray, 'Ticket-List', options);
      }
    })
  }
}
