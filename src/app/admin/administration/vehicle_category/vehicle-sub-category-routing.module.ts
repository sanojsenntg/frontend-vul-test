import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListVehicleSubCategoryComponent } from '../vehicle_category/list-vehicles-category/list-vehicles-category.component';
import { AddVehicleSubCategoryComponent } from './add-vehicles-category/add-vehicles-category.component';
import { EditVehicleSubCategoryComponent } from './edit-vehicles-category/edit-vehicles-category.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const vehicleSubCategoryRoutes: Routes = [
     { path: '', component: ListVehicleSubCategoryComponent, canActivate: [AclAuthervice], data: {roles: ["Vehicle Categories - List"]}},
     { path: 'add', component: AddVehicleSubCategoryComponent, canActivate: [AclAuthervice], data: {roles: ["Vehicle Categories - Add"]}},
     { path: 'edit/:id', component: EditVehicleSubCategoryComponent, canActivate: [AclAuthervice], data: {roles: ["Vehicle Categories - Edit"]}},
    
  ];
