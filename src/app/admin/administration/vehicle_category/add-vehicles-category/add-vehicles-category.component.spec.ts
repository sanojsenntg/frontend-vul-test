import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVehicleSubCategoryComponent } from './add-vehicle-sub-category.component';

describe('AddVehicleSubCategoryComponent', () => {
  let component: AddVehicleSubCategoryComponent;
  let fixture: ComponentFixture<AddVehicleSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVehicleSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVehicleSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
