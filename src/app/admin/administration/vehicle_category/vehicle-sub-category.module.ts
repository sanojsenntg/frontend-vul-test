import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VehicleCategoryService } from './../../../common/services/vehicle-category/vehicle-category.service';
import { VehicleCategoryRestService } from './../../../common/services/vehicle-category/vehicle-categoryrest.service';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListVehicleCategoriesComponent } from '../sub-categories/list-sub-categories/list-sub-categories.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { ImageUploadModule } from 'angular2-image-upload';
import { ListVehicleSubCategoryComponent } from '../vehicle_category/list-vehicles-category/list-vehicles-category.component';
import { AddVehicleSubCategoryComponent } from './add-vehicles-category/add-vehicles-category.component';
import { EditVehicleSubCategoryComponent } from './edit-vehicles-category/edit-vehicles-category.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    Ng2OrderModule,
    MatTooltipModule,
    ImageUploadModule,
    MatSelectModule
  ],
  declarations: [ 
    ListVehicleSubCategoryComponent, AddVehicleSubCategoryComponent, EditVehicleSubCategoryComponent
  ],
  providers: [ 
    VehicleCategoryService,VehicleCategoryRestService
  ],
  exports: [
    RouterModule
  ],
})
export class VehicleSubCategoriesModule { }
