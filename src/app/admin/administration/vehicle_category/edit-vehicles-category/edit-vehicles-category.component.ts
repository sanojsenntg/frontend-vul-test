import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { Subscription } from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { VehicleCategoryService } from '../../../../common/services/vehicle-category/vehicle-category.service';
import { environment } from '../../../../../environments/environment';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-vehicles-category',
  templateUrl: './edit-vehicles-category.component.html',
  styleUrls: ['./edit-vehicles-category.component.css']
})
export class EditVehicleSubCategoryComponent implements OnInit {
  public editForm: FormGroup;
  private sub: Subscription;
  public data;
  public _id;
  public imageUrl = '';
  public categoriesData;
  public spandata = 'yes';
  public companyId: any = [];
  public session;
  
  constructor(private _categoryService: VehicleCategoryService,
    private route: ActivatedRoute,
    private router: Router,
    formBuilder: FormBuilder,
    private _toasterService: ToastsManager,
    public encDecService: EncDecService,
    private vcr: ViewContainerRef) {
    this.editForm = formBuilder.group({
      category: ['', Validators.required],
      image: ['']
    });
  }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.spandata = 'no';
    this._categoryService.getVehicleCategoryById(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var res:any = this.encDecService.dwt(this.session,dec.data);
          this.data = res;
          if (this.data.image) {
            this.imageUrl = environment.imgUrl + '/uploads/vehiclemodels/' + this.data.image;
          }
          this.editForm.setValue({
            category: res.category ? res.category : '',
            image: ''
          });
        }
      }else{
        console.log(dec.message)
      }
    
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {

      }
      console.log(error);
    });
  }

  /**
   * Update vehicle categories data
   *
   */

  public updateVehicleCategories() {
    if (this.editForm.valid) {
      const vehicleCategoriesData = {
        category: this.editForm.value.category,
        image: this.editForm.value.image,
        company_id: localStorage.getItem('user_company'),
        _id:this._id
      };
      var encrypted = this.encDecService.nwt(this.session,vehicleCategoriesData);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._categoryService.updateVehicleCategory(enc_data)
        .then((dec) => {
          if (dec.status == 200) {
            this._toasterService.success('Vehicle category updated successfully.');
            this.router.navigate(['/admin/administration/vehicle-categories']);
          } else if (dec.status == 201) {
            this._toasterService.error('Vehicle category updation failed.');
          }
          else {
            this._toasterService.warning('Category name already exist');
          }
        })
        .catch((error) => {
          console.log('error', error);
        });
    } else {
      return;
    }
  }

  imageFinishedUploading(file: FileHolder) {
    this.editForm.controls['image'].setValue(file.src);
    this.spandata = 'yes';

  }

  onRemoved(file: FileHolder) {
    this.editForm.controls['image'].setValue(null);
    this.spandata = 'no';

  }

  onUploadStateChanged(state: boolean) {

  }


}
