import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { VehicleCategoryService } from '../../../../common/services/vehicle-category/vehicle-category.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { environment } from '../../../../../environments/environment';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-list-vehicles-category',
  templateUrl: './list-vehicles-category.component.html',
  styleUrls: ['./list-vehicles-category.component.css']
})
export class ListVehicleSubCategoryComponent implements OnInit {
  key: string = '';
  public categoryData;
  public id;
  public i;
  itemsPerPage = 10;
  pageNo = 0;
  categoryLength;
  public payment_type;
  public is_search = false;
  public imageUrl;
  searchValue;
  sortOrder = 'asc';
  public keyword = '';
  public searchLoader = false;
  del = false;
  public category = '';
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public companyId: any = [];
  public session;
  constructor(
    public jwtService: JwtService,
    public toastr: ToastsManager,
    private _categoriesService: VehicleCategoryService,
    private router: Router,
    public dialog: MatDialog,
    public overlay: Overlay,
    private _toasterService: ToastsManager,
    public encDecService: EncDecService,
    private vcr: ViewContainerRef,
    public _aclService: AccessControlService) {

  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.aclDisplayService();
    this.imageUrl = environment.imgUrl;
    this.is_search = false;
    this.searchLoader = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.getVehicleCategories(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.categoryData = data.getCategory;
          this.categoryLength = data.count;
        } else {
          console.log(dec.message)
        }
      }
      this.searchLoader = false;
    });
  }

  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Vehicle Categories - Add") {
              this.aclAdd = true;
            } else if (data.menu[i] == "Vehicle Categories - Edit") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Vehicle Categories - Delete") {
              this.aclDelete = true;
            }
          };
        }
      } else {
        console.log(dec.message)
      }
    })
  }

  public searchCategories() {
    this.searchLoader = true;
    this.is_search = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      category: this.category ? this.category : '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.del = true;
    this._categoriesService.getVehicleCategories(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.categoryData = data.getCategory
          this.categoryLength = data.count;
          this.searchLoader = false;
          this.del = false;
        }
      } else {
        console.log(dec.message)
      }

    });
  }

  /**
  * To refresh the page
  */
  refresh() {
    if (this.is_search == true) {
      this.searchCategories();
    } else {
      this.ngOnInit();
    }
  }

  /**
   * Reset the search filters and refresh the page.
   */
  reset() {
    this.del = true;
    this.category = '';
    this.ngOnInit();
  }


  /**
   * For sorting vehicle categories records 
   * @param key 
   */
  sort(key) {
    this.searchLoader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
    var params;
    if (this.is_search == true) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        category: this.category ? this.category : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.getVehicleCategories(enc_data).then((dec) => {
      this.searchLoader = false;
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.categoryData = data.getCategory;
        }
      } else {
        console.log(dec.message)
      }
    });
  }

  /**
   * Pagination for vehicle categories module
   * @param data 
   */
  pagingAgent(data) {
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.getVehicleCategories(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.categoryData = data.getCategory;
        }
      } else {
        console.log(dec.message)
      }
    });
  }


  /**
    * Confirmation popup for deleting particular vehicle categories record
    * @param id 
    */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '350px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this data' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.delete(id);
      } else {
      }
    });
  }

  /**
   * Delete vehicle categories from view
   * @param id 
   */
  public delete(id) {
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._categoriesService.updateVehicleCategoryForDeletion(enc_data)
      .then((dec) => {
        if (dec.status === 200) {
          this.refresh();
          this.categoryData.splice(id, 1);
          this._toasterService.success('Vehicle category deleted successfully.');
        }
        else if (dec.status === 201) {
        }
      })
  }

}
