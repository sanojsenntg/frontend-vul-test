import {Component, Inject, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { } from 'googlemaps';
import { DrawingManager } from '@ngui/map';

@Component({
  selector: 'app-destination-map',
  templateUrl: './destination-map.component.html',
  styleUrls: ['./destination-map.component.css']
})
export class DestinationMapComponent {
  public center;
  public positions=[];
  map;
  @ViewChild(DrawingManager) drawingManager: DrawingManager;
  constructor(
    public dialogRef: MatDialogRef<DestinationMapComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.center={'lat': parseFloat(data.lat), 'lng': parseFloat(data.lng)};
      this.positions.push([data.lat, data.lng]);
  }
  onMapReady(map) {
    console.log('map', map);
    console.log('markers', map.markers);  // to get all markers as an array 
  }
  onIdle(event) {
    console.log('map', event.target);
  }
}
