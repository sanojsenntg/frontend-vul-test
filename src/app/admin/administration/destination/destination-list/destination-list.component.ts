import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PoiService } from '../../../../common/services/poi/poi.service';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { DestinationMapComponent } from '../destination-map/destination-map.component';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-destination-list',
  templateUrl: './destination-list.component.html',
  styleUrls: ['./destination-list.component.css']
})
export class DestinationListComponent implements OnInit {
  session;
  constructor(public _poiService: PoiService,
    public dialog: MatDialog,
    public overlay: Overlay,
    public toastr: ToastsManager,
    public _aclService: AccessControlService,
    private router: Router,
    public jwtService: JwtService,
    public encDecService: EncDecService) {
  }
  public searchSubmit = false;
  public poiItems;
  public tlo;
  public poi;
  key: string = '';
  public faqData;
  public faqLength;
  itemsPerPage = 10;
  pageNo = 0;
  sortOrder = 'desc';
  public showloader;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public companyId = [];

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.poiList();
    this.aclDisplayService();
  }

  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Point Of Interest - List") {
              this.aclAdd = true;
            } else if (data.menu[i] == "Point Of Interest - Edit") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Point Of Interest - Delete") {
              this.aclDelete = true;
            }
          };
        }
      }else{
        console.log(dec.message)
      }
    
    })
  }

  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.poi = {
          "poi_id": id,
          company_id: this.companyId
        };
        var encrypted = this.encDecService.nwt(this.session, this.poi);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._poiService.poiDelete(enc_data).then((dec) => {
          if(dec){
            if(dec.status == 200){
              this.poiList();
            }
          }else{
            console.log(dec.message)
          }
        });
      } else {
      }
    });
  }
  openMap(lat, lng): void {
    let dialogRef = this.dialog.open(DestinationMapComponent, {
      width: '650px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { lat: lat, lng: lng }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
      } else {
      }
    });
  }


  public poiList() {
    this.searchSubmit = true;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._poiService.poiList(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.faqLength = data.count;
          this.poiItems = data.poi;
          this.searchSubmit = false;
        }
      }
    });
  }
  pagingAgent(data) {
    this.searchSubmit = true;
    this.faqData = [];
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    var params;
    params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._poiService.poiList(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.showloader = false;
          this.faqLength = data.count;
          this.poiItems = data.poi;
          this.searchSubmit = false;
        }
      }else{
        console.log(dec.message)
      }
  
    });
  }
}


