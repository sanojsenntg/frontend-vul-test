import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatDialogModule } from '@angular/material/dialog';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DestinationListComponent } from './destination-list/destination-list.component';
import { DestinationAddComponent } from './destination-add/destination-add.component';
import { DestinationMapComponent } from './destination-map/destination-map.component';
import { NguiMapModule } from '@ngui/map';
import { DestinationEditComponent } from './destination-edit/destination-edit.component';


@NgModule({
  imports: [
    MatDialogModule,
    CommonModule,
    RouterModule,
    NgbModule,
    MatSelectModule,
    MatTooltipModule,
    FormsModule, 
    ReactiveFormsModule,
    NguiMapModule
  ],
  declarations: [DestinationListComponent, DestinationAddComponent, DestinationEditComponent, DestinationMapComponent],
  entryComponents: [ DestinationMapComponent ],
})
export class DestinationModule { }
