import { Component, OnInit,ViewChild,ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NG_VALIDATORS,AbstractControl, ValidatorFn , FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { DirectionsRenderer } from '@ngui/map';
import { DrawingManager } from '@ngui/map';
import { environment } from '../../../../../environments/environment';
import { PoiService } from '../../../../common/services/poi/poi.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-destination-add',
  templateUrl: './destination-add.component.html',
  styleUrls: ['./destination-add.component.css']
})
export class DestinationAddComponent implements OnInit {
  public companyId;
  public destinationModel: any = {
    name: '',
    company: 'DubaiTaxi',
    status: '',
    priority:'',
    is_favorite:'',
    xcoordinate:'',
    ycoordinate:'',
    company_id: this.companyId
  };
  
  public statusItems = ['Ok', 'Not ready'];
  public priorityItems = ['Yes','No'];
  public favoriteItems = ['Yes','No'];
  public positions = [];
  map;
  public lat;
  public lang;
  name:string;
  place:any;
  center: any;
  position:any
  public address: any = {};
  public session;

  @ViewChild(DrawingManager) drawingManager: DrawingManager;
  constructor(  
    private ref: ChangeDetectorRef,
    formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastsManager,
    public _poiService : PoiService,
    public jwtService: JwtService,
    public encDecService:EncDecService
  ) {
    this.center = { "lat": 25.18869519067745, "lng": 55.281769128125006 };
   }

  ngOnInit() {
    this.companyId = localStorage.getItem('user_company')
    this.session = localStorage.getItem('Sessiontoken');
  }
  onMapReady(map) {
    console.log('map', map);
    console.log('markers', map.markers);  // to get all markers as an array 
  }
  onIdle(event) {
    console.log('map', event.target);
  }
  
  onMapClick(event) {
    this.positions = [];
    this.lat = event.latLng.lat();
    this.lang = event.latLng.lng();
    this.destinationModel.xcoordinate = event.latLng.lat();
    this.destinationModel.ycoordinate = event.latLng.lng();
    this.positions.push([this.lat, this.lang]);
    console.log(this.positions);
  }
  addPoiData(formData): void {
   
    if (formData.valid) {
      if(this.destinationModel.priority=="Yes"){
        this.destinationModel.priority=1;
      }else{
        this.destinationModel.priority=0;
      }
      if(this.destinationModel.status=="Ok"){
        this.destinationModel.status=1;
      }else{
        this.destinationModel.status=0;
      }
      if(this.destinationModel.is_favorite=="Yes"){
        this.destinationModel.is_favorite=1;
      }else{
        this.destinationModel.is_favorite=0;
      }
      var encrypted = this.encDecService.nwt(this.session,this.destinationModel);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._poiService.poiAdd(enc_data).then((dec)=>{
        if(dec){
          if(dec.status == 200){
            this.router.navigate(['/admin/administration/destination']); 
            this.toastr.success('Poi added successfully.');
          }
        }
      });
    }
  };
  public placeChanged(place) {
    this.place = place;
    this.center = place.geometry.location;
    this.address = place.formatted_address;
    let lng = place.geometry.location.lng();
    let lat = place.geometry.location.lat();
    this.position=[lat,lng];
  }
  
}
