import { Component, OnInit,ViewChild,ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NG_VALIDATORS,AbstractControl, ValidatorFn , FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { } from 'googlemaps';
import { DirectionsRenderer } from '@ngui/map';
import { DrawingManager } from '@ngui/map';
import { environment } from '../../../../../environments/environment';
import { PoiService } from '../../../../common/services/poi/poi.service';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-destination-edit',
  templateUrl: './destination-edit.component.html',
  styleUrls: ['./destination-edit.component.css']
})
export class DestinationEditComponent implements OnInit {
  public companyId = [];
  public destinationModel: any = {
    poi_id:'',
    name: '',
    company: 'DubaiTaxi',
    status: '',
    priority:'',
    is_favorite:'',
    xcoordinate:'',
    ycoordinate:'',
    company_id: localStorage.getItem('user_company')
  };
  private sub: Subscription;
  public statusItems = ['Ok', 'Not ready'];
  public priorityItems = ['Yes','No'];
  public favoriteItems = ['Yes','No'];
  public positions = [];
  public lat;
  public lang;
  name:string;
  place:any;
  position:any
  public address: any = {};
  public id;
  public poiData;
  public session;
  public center ={'lat': 25.186831117106856, 'lng': 55.30314096955567};


  @ViewChild(DrawingManager) drawingManager: DrawingManager;
  constructor(  
    private ref: ChangeDetectorRef,
    formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastsManager,
    public _poiService : PoiService,
    private route: ActivatedRoute,
    public encDecService:EncDecService
  ) {
  }
  

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');

    this.sub = this.route.params.subscribe((params) => {
      this.id = params['id'];
      console.log(this.id);
      var Params = {
        poi_id:this.id,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session,Params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._poiService.poiDetails(enc_data).then((dec)=>{
        if(dec){
          if(dec.status == 200){
            var res:any = this.encDecService.dwt(this.session,dec.data);
            console.log(res);
            this.poiData = res.data;
            this.center={'lat': parseFloat(this.poiData.xcoordinate), 'lng': parseFloat(this.poiData.ycoordinate)}; 
            console.log(this.center);
            this.destinationModel.poi_id = this.poiData._id;
            this.destinationModel.name = this.poiData.name;
            this.destinationModel.company = this.poiData.company;
            this.destinationModel.xcoordinate = this.poiData.xcoordinate;
            this.destinationModel.ycoordinate = this.poiData.ycoordinate;
            if(this.poiData.priority==1){
              this.destinationModel.priority="Yes";
            }else{
              this.destinationModel.priority="No";
            }
            if(this.poiData.status==1){
              this.destinationModel.status="Ok";
            }else{
              this.destinationModel.status="Not Ready";
            }
            if(this.poiData.is_favorite==1){
              this.destinationModel.is_favorite="Yes";
            }else{
              this.destinationModel.is_favorite="No";
            }
            this.positions.push([this.poiData.xcoordinate, this.poiData.ycoordinate]);
          }
        }else{
          console.log(dec.message)
        }
  
      });
    });
    
  }

  onMapReady(map) {
    console.log('map', map);
    console.log('markers', map.markers);  // to get all markers as an array 
  }

  onIdle(event) {
    console.log('map', event.target);
  }
  
  onMapClick(event) {
    this.positions = [];
    this.lat = event.latLng.lat();
    this.lang = event.latLng.lng();
    this.destinationModel.xcoordinate = event.latLng.lat();
    this.destinationModel.ycoordinate = event.latLng.lng();
    this.positions.push([this.lat, this.lang]);
  }
  addPoiData(formData): void {
   
    if (formData.valid) {
      if(this.destinationModel.priority=="Yes"){
        this.destinationModel.priority=1;
      }else{
        this.destinationModel.priority=0;
      }
      if(this.destinationModel.status=="Ok"){
        this.destinationModel.status=1;
      }else{
        this.destinationModel.status=0;
      }
      if(this.destinationModel.is_favorite=="Yes"){
        this.destinationModel.is_favorite=1;
      }else{
        this.destinationModel.is_favorite=0;
      }
      var encrypted = this.encDecService.nwt(this.session,this.destinationModel);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._poiService.poiUpdate(enc_data).then((dec)=>{
        if(dec){
          if(dec.status == 200){
            this.router.navigate(['/admin/administration/destination']); 
            this.toastr.success('Poi updated successfully.');
          }
        }else{
          console.log(dec.message)
        }
      });
    }
  };
  public placeChanged(place) {
    this.place = place;
    this.center = place.geometry.location;
    this.address = place.formatted_address;
    let lng = place.geometry.location.lng();
    let lat = place.geometry.location.lat();
    this.position=[lat,lng];
  }
}
