import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { DestinationListComponent } from './destination-list/destination-list.component';
import { DestinationAddComponent } from './destination-add/destination-add.component';
import { DestinationEditComponent } from './destination-edit/destination-edit.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const DestinationRoutes: Routes = [
  { path: '', component: DestinationListComponent, canActivate: [AclAuthervice], data: {roles: ["Point Of Interest - List"]}},
  { path: 'add-destination', component: DestinationAddComponent, canActivate: [AclAuthervice], data: {roles: ["Point Of Interest - Add"]}},
  { path: 'edit-destination/:id', component: DestinationEditComponent, canActivate: [AclAuthervice], data: {roles: ["Point Of Interest - Edit"]}}
];
