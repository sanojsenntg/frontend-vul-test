import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { paymentRoutes } from './payment-extra/payment-extra-routing.module';
import { paymentTypeRoutes } from './payment-type/payment-type-routing.module';
import { tariffRoutes } from './tariff/tariff-routing.module';
import { PredefinedMessagesRoutes } from './predefined-messages/predefined-messages-routing.module';
import { MessageHistoryRoutes } from './message-history/message-history-routing.module';
import { paymentZoneRoutes } from './payment-zone/payment-zone-routing.module';
import { partnersRoutes } from './partners/partners-routing.module';
import { vehicleSubCategoryRoutes } from './vehicle_category/vehicle-sub-category-routing.module';
import { vehicleCategoryRoutes } from './sub-categories/vehicle-categories-routing.module';
import { PromocodeRoutes } from './promocode/promocode-routing.module';
import { RolesRoutes } from './roles/roles-routing.module';
import { AdministrationComponent } from './administration/administration.component';
import { faqRoutes } from './faq/faq-routing.module'
import { socialSettingsRoutes } from './social-settings/social-settings-routing.module';
import { contactusCategoryRoutes } from './contactus-category/contactus-category-routing.module'
import { coverageAreaRoutes } from './coverage-area/coverage-area-routing.module'
import { AclAuthervice } from '../../common/services/access-control/acl-auth.service';
import { zoneRoutes } from './zones/zone-routing.module';
import { DestinationRoutes } from './destination/destination-routing.module';
import { PromocodeTransactionRoutes } from './promo-transactions/promocode-transaction-routing.module';
import { MiniZoneRoutes } from './mini-zone/min-zone.routing.module';
import { PromoHistoryComponent } from './promo-history/promo-history/promo-history.component';
import { OrderCorrectionComponent } from './order-correction/order-correction.component';
import { CompanyRoutes } from './companies/companies-routing.module';
import { AdministrationSubmenusComponent } from './administration-submenus/administration-submenus.component';
import { ZendeskComponent } from './zendesk/zendesk.component';
import { ZendeskCommentsComponent } from './zendesk/zendesk-comments/zendesk-comments.component';
import { DriverMapComponent } from './driver-map/driver-map.component';
import { ticketRoutes } from './ticket-management/ticket-management-routing.module';
import { MultiZoneRoutes } from './multi-zones/multi-zones-routing.module';


export const administrationRoutes: Routes = [
      { path: '', component: AdministrationComponent },
      { path: 'payment-extra', children: paymentRoutes, canActivate: [AclAuthervice], data: { roles: ['Payment Extra List'] } },
      { path: 'payment-type', children: paymentTypeRoutes, canActivate: [AclAuthervice], data: { roles: ['Payment Type List'] } },
      { path: 'tariffs', children: tariffRoutes, canActivate: [AclAuthervice], data: { roles: ['Tariffs - List'] } },
      { path: 'predefined-messages', children: PredefinedMessagesRoutes, canActivate: [AclAuthervice], data: { roles: ['Predefined Messages - List'] } },
      { path: 'message-history', children: MessageHistoryRoutes, canActivate: [AclAuthervice], data: { roles: ["Message History"] } },
      { path: 'payment-zone', children: paymentZoneRoutes, canActivate: [AclAuthervice], data: { roles: ['Drive through payment zones / Tolls'] } },
      { path: 'partners', children: partnersRoutes, canActivate: [AclAuthervice], data: { roles: ['Partners List'] } },
      { path: 'vehicle-categories', children: vehicleSubCategoryRoutes, canActivate: [AclAuthervice], data: { roles: ["Vehicle Categories - List"] } },
      { path: 'vehicle-sub-categories', children: vehicleCategoryRoutes, canActivate: [AclAuthervice], data: { roles: ["Vehicle Sub Categories - List"] } },
      { path: 'promo-code', children: PromocodeRoutes, canActivate: [AclAuthervice], data: { roles: ['Promocodes'] } },
      { path: 'faq', children: faqRoutes, canActivate: [AclAuthervice], data: { roles: ['FAQ - List'] } },
      { path: 'social-settings', children: socialSettingsRoutes, canActivate: [AclAuthervice], data: { roles: ['Social Settings - List'] } },
      { path: 'contactus-category', children: contactusCategoryRoutes, canActivate: [AclAuthervice], data: { roles: ['Contact us Category - List'] } },
      { path: 'zones', children: zoneRoutes },
      { path: 'destination', children: DestinationRoutes },
      { path: 'promo-transaction', children: PromocodeTransactionRoutes },
      { path: 'mini-zone', children: MiniZoneRoutes },
      { path: 'promo-history', component: PromoHistoryComponent },
      { path: 'order-edit', component: OrderCorrectionComponent },
      { path: 'companies', children: CompanyRoutes, canActivate: [AclAuthervice], data: { roles: ['Companies-List'] } },
      { path: 'submenu/:submenu',component: AdministrationSubmenusComponent},
      { path: 'zendesk', component: ZendeskComponent, canActivate: [AclAuthervice], data: { roles: ['Zendesk ticket - List'] } },
      { path: 'zendesk-comments/:id', component: ZendeskCommentsComponent, canActivate: [AclAuthervice], data: { roles: ['Zendesk ticket - List'] } },
      { path: 'driver-map', component: DriverMapComponent,canActivate: [AclAuthervice], data: { roles: ['Driver Map'] } },
      { path: 'ticket', children:ticketRoutes},
      { path: 'multi-zone', children:MultiZoneRoutes}
];
