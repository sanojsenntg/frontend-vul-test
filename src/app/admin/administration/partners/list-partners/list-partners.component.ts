import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PartnerService } from '../../../../common/services/partners/partner.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


 @Component({
  selector: 'app-list-partners',
  templateUrl: './list-partners.component.html',
  styleUrls: ['./list-partners.component.css']
 })
 
 export class ListPartnersComponent implements OnInit {
  public companyId = [];
  public key: string = '';
  public partnersData;
  public partner;
  public itemsPerPage = 10;
  public pageNo = 0;
  public is_search = false;
  public partnerLength;
  public sortOrder = 'desc';
  public searchLoader = false;
  public name;
  public aclAdd=false;
  public aclEdit=false;
  public aclDelete=false;
  public loadingIndicator;
  public session;
  
  constructor(private _partnerService: PartnerService,
   public dialog: MatDialog,
   public overlay: Overlay,
   private _toasterService: ToastsManager,
   private vcr: ViewContainerRef,
   public _aclService : AccessControlService,
   private router: Router,
   public jwtService: JwtService,
   public encDecService:EncDecService) { 
   }
 
  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.getPartner();
    this.getPartnerData();
    this.aclDisplayService();
  }
  public aclDisplayService(){
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          for(let i=0; i<data.menu.length; i++){
                if(data.menu[i]=="Partners -Create"){
                  this.aclAdd = true;
                }else if (data.menu[i]=="Partners -Edit"){
                  this.aclEdit = true;
                }else if (data.menu[i]=="Partners - Delete"){
                  this.aclDelete = true;
                }
            };
        }
      }else{
        console.log(dec.message)
      }
    })
  }
  /**
   * Load list of partner in partner dropdown on page load.
   */
  public getPartner() {
   const params = {
    offset: 0,
    limit: 0,
    sortOrder: 'desc',
    sortByColumn: 'updated_at',
    company_id: this.companyId
   };
   var encrypted = this.encDecService.nwt(this.session,params);
   var enc_data = {
     data: encrypted,
     email: localStorage.getItem('user_email')
   }
   this._partnerService.getPartner(enc_data).then((dec) => {
    if(dec){
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.partner = data.getPartners;
      }
    }
   });
 
  }
 
  /** 
   * get Partner data
  */
  public getPartnerData() {
   const params = {
    offset: this.pageNo,
    limit: this.itemsPerPage,
    sortOrder: 'desc',
    sortByColumn: 'updated_at',
    company_id: this.companyId
   };
   var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
    }
   this.searchLoader = true;
   this._partnerService.getPartner(enc_data).then((dec) => {
    if(dec){
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.partnersData = data.getPartners;
        this.partnerLength = data.totalCount;
        this.searchLoader = false;
      }
    }else{
      console.log(dec.message)
    }
    
   });
  }
 
  /**
   * open delete dialog
   * @param id 
   */
  openDialog(id): void {
   let dialogRef = this.dialog.open(DeleteDialogComponent, {
    width: '450px',
    scrollStrategy: this.overlay.scrollStrategies.noop(),
    data: {
     text: 'Are you sure you want to remove this record'
    }
   });
 
   dialogRef.afterClosed().subscribe(result => {
    if (result == true) {
     this.deletePartners(id);
    } else {
 
    }
   });
  }
 
  /**
   * 
   * @param id Delete partner by id
   */
  public deletePartners(id) {
    var params = {
      _id:id,
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
   this._partnerService.updateDeletedStatus(enc_data).then((dec) => {
    if (dec.status === 200) {
     this.partnersData.splice(id, 1);
     console.log(this.partnersData);
     if(this.partnersData.length == 0) {
        this.refetchPartners();
    }
     this._toasterService.success('Partner deleted successfully.');
    } else if (dec.status === 201) {} else if (dec.status === 203) {
     this._toasterService.warning('Partner is assigned to order so you cannot delete.');
    }
   });
  }
 
  /** 
   * refresh partners
  */
  public refetchPartners() {
   this.searchLoader = true;
   if (this.is_search) {
    const params = {
     offset: 0,
     limit: this.itemsPerPage,
     sortOrder: 'desc',
     sortByColumn: 'updated_at',
     name: this.name,
     company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
    this._partnerService.getPartner(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.partnersData = data.getPartners;
          this.partnerLength = data.totalCount;
          this.searchLoader = false;
        }
      }else{
        console.log(dec.message)
      }
    });
   } else {
    this.getPartnerData();
   }
  }
 
  /** 
   * Reset filters
  */
  public reset() {
   this.is_search = false;
   this.name = '';
   this.getPartnerData();
  }
 
  /**
   * Sorting
   * @param key 
   */
  sort(key) {
   this.key = key;
   if (this.sortOrder == 'desc') {
    this.sortOrder = 'asc';
   } else {
    this.sortOrder = 'desc';
   }
 
   if (this.is_search == true) {
    var params;
    params = {
     offset: this.pageNo,
     limit: this.itemsPerPage,
     sortOrder: this.sortOrder,
     name: this.name,
     sortByColumn: this.key,
     company_id: this.companyId,
    };
   } else {
    params = {
     offset: this.pageNo,
     limit: this.itemsPerPage,
     sortOrder: this.sortOrder,
     sortByColumn: this.key,
     company_id: this.companyId,
    };
   }
   var encrypted = this.encDecService.nwt(this.session,params);
   var enc_data = {
     data: encrypted,
     email: localStorage.getItem('user_email')
   }
   this._partnerService.getPartner(enc_data).then((dec) => {
    this.searchLoader = true;
    if(dec){
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.partnersData = data.getPartners;
        this.partnerLength = data.totalCount;
        this.searchLoader = false;
      }
    }else{
      console.log(dec.message)
    }

   });
  }
 
  /**
   * 
   * @param data pagination
   */
  pagingAgent(data) {
   this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
   let params;
   if (this.is_search == true) {
    params = {
     offset: this.pageNo,
     limit: this.itemsPerPage,
     sortOrder: this.key ? this.sortOrder : 'desc',
     sortByColumn: this.key ? this.key : 'updated_at',
     name: this.name,
     company_id: this.companyId
    };
   } else {
    params = {
     offset: this.pageNo,
     limit: this.itemsPerPage,
     sortOrder: this.key ? this.sortOrder : 'desc',
     sortByColumn: this.key ? this.key : 'updated_at',
     company_id: this.companyId,
    };
   }
   var encrypted = this.encDecService.nwt(this.session,params);
   var enc_data = {
     data: encrypted,
     email: localStorage.getItem('user_email')
   }
   this._partnerService.getPartner(enc_data).then((dec) => {
    if(dec){
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.partnersData = data.getPartners;
      }
    }else{
      console.log(dec.message)
    }
   });
  }
 
  /** 
   * Patner search by name
  */
  public getPartnerForSearch() {
   this.searchLoader = true;
   this.is_search = true;
   const params = {
    offset: 0,
    limit: this.itemsPerPage,
    sortOrder: 'desc',
    sortByColumn: 'updated_at',
    name: this.name,
    company_id: this.companyId
   };
   var encrypted = this.encDecService.nwt(this.session,params);
   var enc_data = {
    data: encrypted,
    email: localStorage.getItem('user_email')
  }
   this._partnerService.getPartner(enc_data).then((dec) => {
    if(dec){
      this.searchLoader = false;
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.partnersData = data.getPartners;
        this.partnerLength = data.totalCount;
      }
    }else{
      console.log(dec.message)
    }

   });
  }
 
  /**
   * Autocomplete search on partner
   * @param data
   */
  searchPartner(data) {
    this.loadingIndicator = 'searchPartner'
   if (typeof data !== 'object') {
    const params = {
     offset: 0,
     limit: 0,
     sortOrder: 'asc',
     sortByColumn: 'name',
     'name': data,
     company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
    this._partnerService.getPartner(enc_data).then((dec) => {
      this.loadingIndicator = '';
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.partner = data.getPartners;
          this.partnerLength = data.totalCount;
        }
      }else{
        console.log(dec.message)
      }
     
    })
   }
  }
 
  displayFndpartner(data): string {
   return data ? data : data;
  }
 
 }
