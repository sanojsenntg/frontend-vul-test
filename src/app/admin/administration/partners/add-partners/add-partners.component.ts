import { Component, ViewContainerRef,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PartnerService } from '../../../../common/services/partners/partner.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-add-partners',
  templateUrl: './add-partners.component.html',
  styleUrls: ['./add-partners.component.css']
})

export class AddPartnersComponent {
  public session;
  public companyId = localStorage.getItem('user_company');
  partnerModel: any = {
  name: '',
  address:'',
  house_number:'',
  postal_code: '',
  city: '',
  phone_number:'',
  invoice_email:'',
  reference_number:'',
  invoice_company_name:'',
  invoice_enabled:'' ,
  invoice_with_appendix:'',
  invoice_address:'' ,
  invoice_house_number:'',
  invoice_postal_code:'',
  invoice_city:'',
  smartphone_partner:'',
  country:'' ,
  contact_person:'',
  mail_add_contact:'',
  select_type: '',
  invoice_generated_for:'',
  web_blocking_period:'',
  web_number_of_packages:'',
  web_number_of_persons:'',
  default_export_type:'',
  ziping_of_invoice_enabled:'',
  sending_via_email_enabled:'',
  uid:'',
  trip_id_mandatory:'',
  kiosk_partner:'',
  password:'',
  confirmPassword:'',
  commission:'',
  available_for:'',
  company_id: this.companyId
  }
  kioskPass = false;
  confirmError = true;
  status=false;
 
  constructor(private _partnerService: PartnerService,
    private router: Router,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    public encDecService:EncDecService) { 
      this.session = localStorage.getItem('Sessiontoken');
      this.companyId = localStorage.getItem('user_company');
    }

  /**
   * Save partner data
   *
   * @param formData
   */
  ngOnInit(){
  }

  cpClicked(){
    console.log('clicked');
  }
  kioskControl(){
    console.log(this.partnerModel.kiosk)
    if(this.partnerModel.kiosk == 'true'){
      this.kioskPass = true;
    }else{
      this.kioskPass = false;
    }
  }
  passControl(){
    if(this.partnerModel.confirmPassword !=''){
      if(this.partnerModel.password == this.partnerModel.confirmPassword){
        this.confirmError = true;
      }else{
        this.confirmError = false;
      }
    }
  }
  addPartner(formData): void {
    if (formData.valid) {
      var encrypted = this.encDecService.nwt(this.session,this.partnerModel);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._partnerService.savePartner(enc_data)
        .then((res) => {
          if (res.status == 200) {
            this.toastr.success('Partner added successfully.');
            this.router.navigate(['/admin/administration/partners']);
          } else if (res.status == 201) {
            this.toastr.error(res.message);
          } else {
            this.toastr.error('partner adding failed.');
          }
        })
    } else {
      // this._toasterService.error('Please fill in the required fields.');
      return;
    }
  }
}


