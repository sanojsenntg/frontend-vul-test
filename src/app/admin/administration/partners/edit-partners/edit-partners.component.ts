import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { PartnerService } from '../../../../common/services/partners/partner.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-partners',
  templateUrl: './edit-partners.component.html',
  styleUrls: ['./edit-partners.component.css']
})
export class EditPartnersComponent implements OnInit {
  public editForm: FormGroup;
  public _id;
  private sub: Subscription;
  public session;
  public companyId = localStorage.getItem('user_company');
  partnerModel: any = {
    name: '',
    address: '',
    house_number: '',
    postal_code: '',
    city: '',
    phone_number: '',
    invoice_email: '',
    reference_number: '',
    invoice_company_name: '',
    invoice_enabled: '',
    invoice_with_appendix: '',
    invoice_address: '',
    invoice_house_number: '',
    invoice_postal_code: '',
    invoice_city: '',
    smartphone_partner: '',
    country: '',
    contact_person: '',
    mail_add_contact: '',
    select_type: '',
    invoice_generated_for: '',
    web_blocking_period: '',
    web_number_of_packages: '',
    web_number_of_persons: '',
    default_export_type: '',
    ziping_of_invoice_enabled: '',
    sending_via_email_enabled: '',
    uid: '',
    trip_id_mandatory: '',
    kiosk_partner: '',
    commission:'',
    available_for:''
  }


  constructor(private _partnerService: PartnerService,
    formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public encDecService: EncDecService
  ) {
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId = localStorage.getItem('user_company');
    this.editForm = formBuilder.group({
      name: ['', [
        Validators.required]],
      address: [''],
      house_number: [''],
      postal_code: [''],
      city: [''],
      phone_number: [''],
      invoice_email: [''],
      reference_number: [''],
      invoice_company_name: [''],
      invoice_enabled: [''],
      invoice_with_appendix: [''],
      invoice_address: [''],
      invoice_house_number: [''],
      invoice_postal_code: [''],
      invoice_city: [''],
      smartphone_partner: [''],
      country: [''],
      contact_person: [''],
      mail_add_contact: [''],
      select_type: [''],
      invoice_generated_for: [''],
      web_blocking_period: [''],
      web_number_of_packages: ['', [
        Validators.required]],
      web_number_of_persons: ['', [
        Validators.required]],
      default_export_type: [''],
      ziping_of_invoice_enabled: [''],
      sending_via_email_enabled: [''],
      trip_id_mandatory: [''],
      uid: [''],
      kiosk_partner: [''],
      available_for:[''],
      commission:['', [
        Validators.required]]
    });
  }

  /**
   * Set the values in the field boxes when the page completely loads 
   */
  public ngOnInit(): void {
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      _id: this._id,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: window.localStorage.getItem('user_email')
    }
    this._partnerService.getPartnerById(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          console.log(res);
          this.editForm.setValue({
            name: res.partnerById.name ? res.partnerById.name : '',
            address: res.partnerById.address ? res.partnerById.address : '',
            house_number: res.partnerById.house_number ? res.partnerById.house_number : '',
            postal_code: res.partnerById.postal_code ? res.partnerById.postal_code : '',
            city: res.partnerById.city ? res.partnerById.city : '',
            phone_number: res.partnerById.phone_number ? res.partnerById.phone_number : '',
            invoice_email: res.partnerById.invoice_email ? res.partnerById.invoice_email : '',
            reference_number: res.partnerById.reference_number ? res.partnerById.reference_number : '',
            invoice_company_name: res.partnerById.invoice_company_name ? res.partnerById.invoice_company_name : '',
            invoice_enabled: res.partnerById.invoice_enabled ? res.partnerById.invoice_enabled : '',
            invoice_with_appendix: res.partnerById.invoice_with_appendix ? res.partnerById.invoice_with_appendix : '',
            invoice_address: res.partnerById.invoice_address ? res.partnerById.invoice_address : '',
            invoice_house_number: res.partnerById.invoice_house_number ? res.partnerById.invoice_house_number : '',
            invoice_postal_code: res.partnerById.invoice_postal_code ? res.partnerById.invoice_postal_code : '',
            invoice_city: res.partnerById.invoice_city ? res.partnerById.invoice_city : '',
            smartphone_partner: res.partnerById.smartphone_partner ? res.partnerById.smartphone_partner : '',
            country: res.partnerById.country ? res.partnerById.country : '',
            contact_person: res.partnerById.contact_person ? res.partnerById.contact_person : '',
            mail_add_contact: res.partnerById.mail_add_contact ? res.partnerById.mail_add_contact : '',
            select_type: res.partnerById.select_type ? res.partnerById.select_type : '',
            invoice_generated_for: res.partnerById.invoice_generated_for ? res.partnerById.invoice_generated_for : '',
            web_blocking_period: res.partnerById.web_blocking_period ? res.partnerById.web_blocking_period : '',
            web_number_of_packages: res.partnerById.web_number_of_packages ? res.partnerById.web_number_of_packages : '',
            web_number_of_persons: res.partnerById.web_number_of_persons ? res.partnerById.web_number_of_persons : '',
            default_export_type: res.partnerById.default_export_type ? res.partnerById.default_export_type : '',
            ziping_of_invoice_enabled: res.partnerById.ziping_of_invoice_enabled ? res.partnerById.ziping_of_invoice_enabled : '',
            sending_via_email_enabled: res.partnerById.sending_via_email_enabled ? res.partnerById.sending_via_email_enabled : '',
            trip_id_mandatory: res.partnerById.trip_id_mandatory ? res.partnerById.trip_id_mandatory : '',
            uid: res.partnerById.uid ? res.partnerById.uid : '',
            available_for: res.partnerById.available_for ? res.partnerById.available_for : '2',
            kiosk_partner: res.partnerById.kiosk_partner == 'true' ? 'true' : 'false',
            commission: res.partnerById.commission ? res.partnerById.commission.$numberDecimal : '',
          });
        }
      } else {
        console.log(dec.message);
      }
      console.log(this.editForm.value.kiosk_partner);
    })
  }

  /**
   * To update the partner data
   */
  public updatePartners(event: Event) {
    console.log(this.editForm);
    if (this.editForm.valid) {
      var params = {
        name: this.editForm.value.name,
        address: this.editForm.value.address,
        house_number: this.editForm.value.house_number,
        postal_code: this.editForm.value.postal_code,
        city: this.editForm.value.city,
        phone_number: this.editForm.value.phone_number,
        invoice_email: this.editForm.value.invoice_email,
        reference_number: this.editForm.value.reference_number,
        invoice_company_name: this.editForm.value.invoice_company_name,
        invoice_enabled: this.editForm.value.invoice_enabled,
        invoice_with_appendix: this.editForm.value.invoice_with_appendix,
        invoice_address: this.editForm.value.invoice_address,
        invoice_house_number: this.editForm.value.invoice_house_number,
        invoice_postal_code: this.editForm.value.invoice_postal_code,
        invoice_city: this.editForm.value.invoice_city,
        country: this.editForm.value.country,
        smartphone_partner: this.editForm.value.smartphone_partner,
        contact_person: this.editForm.value.contact_person,
        mail_add_contact: this.editForm.value.mail_add_contact,
        select_type: this.editForm.value.select_type,
        invoice_generated_for: this.editForm.value.invoice_generated_for,
        web_blocking_period: this.editForm.value.web_blocking_period,
        web_number_of_packages: this.editForm.value.web_number_of_packages,
        web_number_of_persons: this.editForm.value.web_number_of_persons,
        default_export_type: this.editForm.value.default_export_type,
        ziping_of_invoice_enabled: this.editForm.value.ziping_of_invoice_enabled,
        sending_via_email_enabled: this.editForm.value.sending_via_email_enabled,
        trip_id_mandatory: this.editForm.value.trip_id_mandatory,
        uid: this.editForm.value.uid,
        kiosk_partner: this.editForm.value.kiosk_partner,
        company_id: this.companyId,
        available_for: this.editForm.value.available_for,
        _id: this._id,
        commission: this.editForm.value.commission
      };
      console.log(params);
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: window.localStorage.getItem('user_email')
      };
      this._partnerService.updatePartner(enc_data)
        .then((res) => {
          console.log(res);
          if (res.status === 200) {
            this._toasterService.success('Partner updated successfully.');
            this.router.navigate(['/admin/administration/partners']);
          } else if (res.status === 201) {
            this._toasterService.error('Partner updation failed.');
          }
        })
        .catch((error) => {
        });
    } else {
      this._toasterService.error('Please fill all the required Fields');
    }
  }
}
