import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../../../common/services/message/message.service';
import * as moment from 'moment/moment';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-list-message-history',
  templateUrl: './list-message-history.component.html',
  styleUrls: ['./list-message-history.component.css']
})

export class ListMessageHistoryComponent implements OnInit {
  key: string = '';
  public messageHistoryData;
  itemsPerPage = 10;
  pageNo = 0;
  messageHistoryLength;
  public is_search = false;
  sortOrder = 'desc';
  public showloader;
  public message = '';
  public selectedMoment = '';
  public companyId = [];
  session;
  constructor(private _messageService: MessageService,
    public toastr: ToastsManager,
    private router: Router,
    public jwtService: JwtService,
    public encDecService:EncDecService
  ) {
   }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');

    this.is_search = false;
    this.showloader = true;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._messageService.getMessageHistory(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.showloader = false;
          this.messageHistoryData = data.msgData;
          this.messageHistoryLength = data.count;
        }
      }
    });
  }

  /**
  * For sorting message records 
  * @param key 
  */
  sort(key) {
    this.showloader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
   
   
    var params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        message: this.message ? this.message : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._messageService.getAllMessageHistories(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.messageHistoryData = data.AllMessageHistory;
          this.messageHistoryLength = data.totalCount;
          this.showloader = false;
        }
      }else{
        console.log(dec.message)
      }
  
      /*this.is_search = true;*/
    });
  }

  /**
   * Pagination for message history module
   * @param data 
   */
  pagingAgent(data) {
    this.showloader = true;
    this.messageHistoryData = [];
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    var params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        message: this.message ? this.message : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._messageService.getAllMessageHistories(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.showloader = false;
          this.messageHistoryData = data.AllMessageHistory;
        }
      }else{
        console.log(dec.message)
      }
   
    });
  }


  /**
   * For searching message history records 
   */
  public searchMessage() {
    this.showloader = true;
    this.is_search = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      message: this.message ? this.message : '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._messageService.getAllMessageHistories(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.messageHistoryData = data.AllMessageHistory;
          this.messageHistoryLength = data.totalCount;
          this.showloader = false;
        }
      }else{
        console.log(dec.message)
      }
     
    });
  }

  /**
   * To reset the search filters and reload the message records 
   */
  public reset() {
    this.message = '';
    this.ngOnInit();
  }

  /**
   * To refresh message history records showing in the grid
   */
  public refresh() {
    if (this.is_search == true) {
      this.searchMessage();
    } else {
      this.ngOnInit();
    }
  }

}

