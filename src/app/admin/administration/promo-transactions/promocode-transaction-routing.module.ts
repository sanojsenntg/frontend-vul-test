import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { PromoTransactionListComponent } from './promo-transaction-list/promo-transaction-list.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const PromocodeTransactionRoutes: Routes = [
   { path: '', component: PromoTransactionListComponent, canActivate: [AclAuthervice], data: { roles: ["Promocodes"] } },
];
