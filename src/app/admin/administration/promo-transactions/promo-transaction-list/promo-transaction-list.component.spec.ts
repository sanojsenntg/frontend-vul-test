import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoTransactionListComponent } from './promo-transaction-list.component';

describe('PromoTransactionListComponent', () => {
  let component: PromoTransactionListComponent;
  let fixture: ComponentFixture<PromoTransactionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoTransactionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoTransactionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
