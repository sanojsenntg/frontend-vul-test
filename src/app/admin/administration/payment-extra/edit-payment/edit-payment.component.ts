import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PaymentService } from '../../../../common/services/payment/payment.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { AdditionalService } from '../../../../common/services/additional_service/additional_service.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-payment',
  templateUrl: './edit-payment.component.html',
  styleUrls: ['./edit-payment.component.css'],
  providers: [PaymentService, AdditionalService]
})
export class EditPaymentComponent implements OnInit {
  public companyId = [];
  public serviceData;
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  session;
  payment_extra_types = ['Toll', 'Cancellation_Fee', 'Reservation_Fee', 'FIRSTCOMER_DISCOUNT', 'Others'];
  companyData = ['Dubai Taxi'];
  constructor(private _paymentService: PaymentService,
    private _additionalService: AdditionalService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    public encDecService:EncDecService,
    vcr: ViewContainerRef) {
    this.editForm = formBuilder.group({
      payment_extra: ['', [Validators.required]
      ],
      default_amount: ['', [Validators.required]
      ],
      vat: [''],
      additional_service_id: [''],
      payment_extra_type: ['', [Validators.required]],
      is_amount_percantage: ['', [Validators.required]],
      editable: ['', [Validators.required]],
      locked: ['', [Validators.required]],
      price_with_vat: ['', [Validators.required]],
      multi_applicable: ['', [Validators.required]],
      visible_on_app: ['', [Validators.required]]
    });
  }

  /**
   * To set the payment extra field values in text fields 
   */
  public ngOnInit(): void {
    this.getAdditionalServices();
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      _id:this._id,
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentService.getById(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var res:any = this.encDecService.dwt(this.session,dec.data);
          this.editForm.setValue({
            payment_extra: res.payment_extra ? res.payment_extra : '',
            default_amount: res.default_amount ? res.default_amount : '',
            vat: res.vat ? res.vat : '',
            additional_service_id: res.additional_service_id ? res.additional_service_id : '',
            payment_extra_type: res.payment_extra_type ? res.payment_extra_type : '',
            is_amount_percantage: res.is_amount_percantage ? res.is_amount_percantage : '',
            editable: res.editable ? res.editable : '',
            locked: res.locked ? res.locked : '',
            price_with_vat: res.price_with_vat ? res.price_with_vat : '',
            multi_applicable: res.multi_applicable ? res.multi_applicable : '',
            visible_on_app: res.visible_on_app ? res.visible_on_app : '',
          });
        }
      }else{
        console.log(dec.message)
      }
    
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }

    });
  }

  /**
   * To update the payment extra details
   */
  public updatePaymentDetails() {
    if (!this.editForm.valid) {
      return;
    }
    const paymentData = {
      payment_extra: this.editForm.value.payment_extra,
      default_amount: this.editForm.value.default_amount,
      vat: this.editForm.value.vat,
      additional_service_id: this.editForm.value.additional_service_id ? this.editForm.value.additional_service_id : null,
      payment_extra_type: this.editForm.value.payment_extra_type,
      is_amount_percantage: this.editForm.value.is_amount_percantage,
      editable: this.editForm.value.editable,
      locked: this.editForm.value.locked,
      price_with_vat: this.editForm.value.price_with_vat,
      multi_applicable: this.editForm.value.multi_applicable,
      visible_on_app: this.editForm.value.visible_on_app,
      company_id: localStorage.getItem('user_company'),
      _id:this._id
    }
    var encrypted = this.encDecService.nwt(this.session,paymentData);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
    }
    this._paymentService.updatePaymentDetails(enc_data)
      .then((response) => {
        this.toastr.success('Payment extra updated successfully.');
        this.router.navigate(['/admin/administration/payment-extra']);
      })
      .catch((error) => {
        this.toastr.error('Payment extra updation cancelled.');
        this.router.navigate(['/login']);
      });
  }

  /**
   * Get Additional Services for drop down
   *
   */
  public getAdditionalServices() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
    }
    this._additionalService.get(enc_data).subscribe((dec) => {
      if(dec){
        if(dec.status == 200){
          var servicedata:any = this.encDecService.dwt(this.session,dec);
          this.serviceData = servicedata;
        }
      }
    });
  }
}
