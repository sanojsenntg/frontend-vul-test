import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentService } from '../../../../common/services/payment/payment.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-list-payment',
  templateUrl: './list-payment.component.html',
  styleUrls: ['./list-payment.component.css']
})
export class ListPaymentComponent implements OnInit {
  session;
  public companyId = [];
  key: string = '';
  reverse: boolean = false;
  searchValue: string = '';
  pageSize = 10;
  pageNo = 0;
  del = false;
  totalPage = [1];
  public is_search = false;
  pages = 8;
  public _id;
  public paymentData;
  public payment_extra;
  searchDataPayments = [];
  paymentLength;
  public searchSubmit = false;
  page = 1;
  sortOrder = 'asc';
  public keyword;
  public aclAdd=false;
  public aclEdit=false;
  public aclDelete=false;

  constructor(private _paymentService: PaymentService,
    private router: Router,
    public jwtService: JwtService,
    public dialog: MatDialog,
    public overlay: Overlay,
    public toastr: ToastsManager,
    vcr: ViewContainerRef,
    public _aclService : AccessControlService,
    public encDecService:EncDecService
  ) { 
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
  }

  /**
  * Function which runs first when the page loads
  */
  ngOnInit() {
    this.aclDisplayService();
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentService.getPaymentListing(enc_data).subscribe((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.paymentLength = data.count;
          this.paymentData = data.getPayment;
          this.searchSubmit = false;
        }
      }
    });
  }
  public aclDisplayService(){
    var params = {
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          for(let i=0; i<data.menu.length; i++){
               if(data.menu[i]=='Payment Extra - Add'){
                 this.aclAdd = true;
               }else if (data.menu[i]=='Payment Extra - Edit'){
                 this.aclEdit = true;
               }else if (data.menu[i]=='Payment Extra - Delete'){
                 this.aclDelete = true;
               }
           };
        }
      }else{
        console.log(dec.message)
      }

    })
  }
  /**
  * Confirmation popup for deleting particular payment record
  * @param id 
  */
  openDialog(id): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      height: '150px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deletePaymentExtras(id);
        this.toastr.success('Payment extra deleted successfully.');
      } else {

      }
    });
  }

  /**
   * Delete payment extra from view
   * @param id 
   */
  public deletePaymentExtras(id) {
    this.searchSubmit = true;
    var params = {
      _id:id,
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentService.updatePaymentForDeletion(enc_data)
      .then((dec) => {
        if(dec){
          if(dec.status == 200){
            this.refetchPaymentExtra();
            this.paymentData.splice(id, 1);
            this.searchSubmit = false;
          }
        }else{
          console.log(dec.message)
        }
    
      });
  }

  /**
   * To refresh payment extra records showing in the grid
   */
  public refetchPaymentExtra() {
      this.getPayments(this.keyword);

  }

  /**
   * To reset the search filters and reload the payment extra records
   */
  public reset() {
    this.is_search =false;
    this.payment_extra = '';
    this.keyword = '';
    this.ngOnInit();
  }

  /**
   * For sorting payment records 
   * @param key 
   */
  sort(key) {
    this.searchSubmit = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
    
    
    
    if(this.keyword) {
      const params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        search: this.keyword,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentService.getPaymentListing(enc_data).subscribe((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec.data);
            this.paymentData = data.getPayment;
            this.searchSubmit = false;
          }
        }else{
          console.log(dec.message)
        }
      });
    }else {
      const params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        search: '',
        company_id: this.companyId,
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentService.getPaymentListing(params).subscribe((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec.data);
            this.paymentData = data.getPayment;
            this.searchSubmit = false;
          }
        }else{
          console.log(dec.message)
        }
      });
    }
  }

  /**
  * Pagination for payment extra module
  * @param data 
  */
  pagingAgent(data) {
    this.searchSubmit = true;
    this.paymentData = [];
    this.searchDataPayments = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
   
    if(this.keyword) {
      const params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId,
        search:this.keyword
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
    this._paymentService.getPaymentListing(enc_data).subscribe((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.paymentData = data.getPayment;
          this.searchSubmit = false;
        }
      }else{
        console.log(dec.message)
      }
      
    });
  }else {
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      company_id: this.companyId,
      search:''
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentService.getPaymentListing(enc_data).subscribe((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.paymentData = data.getPayment;
          this.searchSubmit = false;
        }
      }else{
        console.log(dec.message)
      }
    });

  }
  }

  /**
   * For searching payment extra records
   * @param keyword 
   */
  public getPayments(keyword) {
    this.searchSubmit = true;
    this.keyword = keyword;
    
   
    if (this.keyword) {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        search:this.keyword
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentService.getPaymentListing(enc_data).subscribe((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec.data);
            this.paymentData = data.getPayment;
            this.paymentLength = data.count;
            this.searchSubmit = false;
          }
        }else{
          console.log(dec.message)
        }
      });
    } else {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        search:''
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentService.getPaymentListing(enc_data).subscribe((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec.data);
            this.paymentData = data.getPayment;
            this.paymentLength = data.count;
            this.searchSubmit = false;
          }
        }else{
          console.log(dec.message)
        }
      });
    }
  }
}
