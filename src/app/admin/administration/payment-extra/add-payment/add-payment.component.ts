
// Import Required Dependencies and/or Modules

import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

// Importing Required Files

import { PaymentService } from '../../../../common/services/payment/payment.service';
import { AdditionalService } from '../../../../common/services/additional_service/additional_service.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-payment',
  templateUrl: './add-payment.component.html',
  styleUrls: ['./add-payment.component.css'],
  providers: [PaymentService, JwtService, AdditionalService]
})
export class AddPaymentComponent implements OnInit {
  public companyId = [];
  public payment: any = {
    payment_extra: '',
    default_amount: '',
    vat: '',
    company: '',
    additional_service_id: '',
    payment_extra_type: '',
    is_amount_percantage: '',
    editable: '',
    locked: '',
    price_with_vat: '',
    multi_applicable: '',
    visible_on_app: '',
    company_id: localStorage.getItem('user_company')
  };
  session;

  public saveForm: FormGroup;
  private sub: Subscription;
  public _id;
  public showAmount = false;
  public showerror;
  public event;
  public serviceData;
  visible_on_app = new FormControl();
  is_amount_percantage = new FormControl();
  editable = new FormControl();
  locked = new FormControl();
  multi_applicable = new FormControl();
  price_with_vat = new FormControl();
  payment_extra_type = new FormControl();
  additional_service = new FormControl();
  company = new FormControl();
  payment_extra_types = ['Toll', 'Cancellation_Fee', 'Reservation_Fee', 'FirstComer_Discount', 'Others'];
  companyData = 'Dubai Taxi';
  constructor(private _paymentService: PaymentService,
    private _additionalService: AdditionalService,
    private router: Router,
    public jwtService: JwtService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public toastr: ToastsManager,
    public encDecService:EncDecService,
    vcr: ViewContainerRef) {
  }

  /**
  * Function which runs first when the page loads completely
  */
  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.getAdditionalServices();
  }

  /**
   * To add the payment extra records
   * @param formData 
   */
  onSubmit(formData): void {
    if (formData.valid) {
      var encrypted = this.encDecService.nwt(this.session,formData);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentService.addPayments(enc_data)
        .then((dec) => {
          if (dec.status === 200) {
            this.toastr.success('Payment extra added successfully.');
            this.router.navigate(['/admin/administration/payment-extra']);
          } else if (dec.status === 201) {
            this.toastr.error('Payment extra addition failed.');
          }
        })
        .catch((error) => {
        });
    } else {
      
    }
  }

  /**
   * Get Additional Services for drop down
   *
   */
  public getAdditionalServices() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'name',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._additionalService.get(enc_data).subscribe((dec) => {
      if(dec){
        if(dec.status == 200){
          var servicedata:any = this.encDecService.dwt(this.session,dec.data);
          this.serviceData = servicedata;
        }
      }
    });
  }
}
