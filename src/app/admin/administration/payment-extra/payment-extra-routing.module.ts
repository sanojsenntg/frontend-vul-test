
import { NgModule } from '@angular/core';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import {AddPaymentComponent} from './add-payment/add-payment.component';
import {ListPaymentComponent} from './list-payment/list-payment.component';
import {EditPaymentComponent} from './edit-payment/edit-payment.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';
export const paymentRoutes: Routes = [
  { path: '', component: ListPaymentComponent},
  { path: 'add', component: AddPaymentComponent , canActivate: [AclAuthervice], data: {roles: ["Payment Extra - Add"]}},
  { path: 'edit/:id', component: EditPaymentComponent, canActivate: [AclAuthervice], data: {roles: ["Payment Extra - Edit"]} },
  ];
