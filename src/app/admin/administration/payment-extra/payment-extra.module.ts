import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddPaymentComponent } from './add-payment/add-payment.component';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListPaymentComponent } from './list-payment/list-payment.component';
import { EditPaymentComponent } from './edit-payment/edit-payment.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { MatTooltipModule} from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select'

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    Ng2OrderModule,
    FormsModule,
    MatTooltipModule,
    MatSelectModule
  ],
  declarations: [AddPaymentComponent, ListPaymentComponent, EditPaymentComponent],
  exports: [
    RouterModule
  ]
})
export class PaymentExtraModule { }
