import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoHistoryComponent } from './promo-history.component';

describe('PromoHistoryComponent', () => {
  let component: PromoHistoryComponent;
  let fixture: ComponentFixture<PromoHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
