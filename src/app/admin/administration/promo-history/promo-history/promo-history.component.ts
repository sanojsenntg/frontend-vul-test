import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { PromocodeService } from '../../../../common/services/promocode/promocode.service';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { CustomerService } from '../../../../common/services/customer/customer.service';
import * as moment from 'moment/moment';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../../common/services/companies/companies.service';

@Component({
  selector: 'app-promo-history',
  templateUrl: './promo-history.component.html',
  styleUrls: ['./promo-history.component.css']
})
export class PromoHistoryComponent implements OnInit {
  public companyId = [];
  itemsPerPage = 10;
  pageNo = 0;
  public searchSubmit = false;
  sortOrder = 'asc';
  public searchLoader = false;
  public selectedMoment: any = "";
  public selectedMoment1: any = "";
  public promo_code = new FormControl;
  public promo_list = [];
  public promo_chips = [];
  public max = new Date();
  public pNo = 1;
  public promoCodeLength;
  public promoCodeData: any = [];
  public promo_type
  public user_name: any = '';
  user_phone: FormControl = new FormControl();
  public customerData = [];
  public customerGroups = [];
  public customer_group_tag;
  public promoLength;
  public unlimited = false;
  public promoDetails: any = {
    total_trips: 0,
    total_users: 0,
    total_usage_alloted: 0,
    total_utilisation: 0,
    total_with_promo: 0,
    total_discount: 0
  };
  session;
  dtc: boolean;
  companyData: any;
  companyIdFilter = [];
  constructor(
    private _companyservice: CompanyService,
    private router: Router,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    public _promoCodeService: PromocodeService,
    public _aclService: AccessControlService,
    private _customerService: CustomerService,
    public encDecService: EncDecService) {
    this.selectedMoment = new Date(Date.now() - 2592000000);
    this.selectedMoment1 = new Date(Date.now());
  }
  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    if (localStorage.getItem('user_company') === '5ce12918aca1bb08d73ca25d' || localStorage.getItem('user_company') === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
      this.getCompanies();
    }
    else
      this.getPromoHistory();
    this.getCustomerGroups();
    this.promo_code
      .valueChanges.debounceTime(200)
      .subscribe(data => {
        //console.log('driverlist', data);
        if (typeof data === "string") {
          const params = {
            offset: 0,
            limit: this.itemsPerPage,
            sortOrder: 'desc',
            sortByColumn: 'updated_at',
            keyword: data,
            promo_type: this.promo_type ? this.promo_type : '',
            company_id: this.companyId
          };
          var encrypted = this.encDecService.nwt(this.session, params);
          var enc_data = {
            data: encrypted,
            email: localStorage.getItem('user_email')
          }
          this._promoCodeService.getPromocodeListing(enc_data).then((dec) => {
            if (dec) {
              if (dec.status == 200) {
                var data: any = this.encDecService.dwt(this.session, dec.data);
                this.promo_list = data.getPromocodes;
              }
            } else {
              console.log(dec.message)
            }
          });
        }
      });
    this.user_phone.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          search: query,
          limit: 10,
          company_id: this.companyId
        };
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        return this._customerService.getCustomerByPhone(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.customerData = data.result;
        }
      });
  }
  getPromoHistory() {
    const promo_ids = this.promo_chips.map(x => x._id);
    this.searchSubmit = true;
    this.unlimited = false;
    this.promoDetails = {
      total_trips: 0,
      total_users: 0,
      total_usage_alloted: 0,
      total_utilisation: 0,
      total_with_promo: 0,
      total_discount: 0
    };
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      promo_type: this.promo_type ? this.promo_type : '',
      promo_group: this.customer_group_tag ? this.customer_group_tag : '',
      promo_code: promo_ids.length > 0 ? promo_ids : [],
      customer_id: this.user_name ? this.user_name._id : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      company_id: this.companyId.length > 0 ? this.companyId : this.companyId
    };
    console.log(params)
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }

    this._promoCodeService.getPromocodeHistory(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.searchSubmit = false;
          this.promoCodeData = data.promoData;
          this.promoLength = data.count;
          data.fullPromoData.forEach(promo => {
            this.promoDetails.total_trips += promo.no_of_trips;
            this.promoDetails.total_users += promo.total_unique_users;
            this.promoDetails.total_usage_alloted += parseFloat(promo.total_usage_allovance);
            this.promoDetails.total_utilisation += parseFloat(promo.a008_amt.$numberDecimal);
            this.promoDetails.total_with_promo += parseFloat(promo.total_cost.$numberDecimal);
            this.promoDetails.total_discount += parseFloat(promo.a008_amt.$numberDecimal);
            if (promo.total_usage_allovance < 0)
              this.unlimited = true;
          });
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  public reset() {
    this.selectedMoment = new Date(Date.now() - 2592000000);
    this.selectedMoment1 = new Date(Date.now());
    this.promo_list = [];
    this.promo_chips = [];
    this.user_name = ''
    this.promo_type = ''
    this.customer_group_tag = '';
    this.companyIdFilter = [];
    this.ngOnInit();
  }
  pagingAgent(data) {
    this.pNo = data;
    this.searchSubmit = true;
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    const promo_ids = this.promo_chips.map(x => x._id);
    this.searchSubmit = true;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      promo_type: this.promo_type ? this.promo_type : '',
      promo_group: this.customer_group_tag ? this.customer_group_tag : '',
      promo_code: promo_ids.length > 0 ? promo_ids : [],
      customer_id: this.user_name ? this.user_name._id : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      company_id: this.companyId.length > 0 ? this.companyId : this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._promoCodeService.getPromocodeHistory(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.searchSubmit = false;
          this.promoCodeData = data.promoData;
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  pChipselected(event) {
    if (typeof event == "object" && event !== null)
      if (this.promo_chips.indexOf(event) > -1) {
        return;
      } else {
        this.promo_chips.push(event);
        this.promo_code.setValue(null);
      }
  }
  removePChip(chip): void {
    const index = this.promo_chips.indexOf(chip);
    if (index >= 0) {
      this.promo_chips.splice(index, 1);
    }
  }
  displayFnPromo(data): String {
    return data ? data.promo_name : '';
  }
  clearEndTime() {
    this.selectedMoment1 = "";
  }
  displayFnCustomerPhone(data): string {
    return data ? data.phone_number : data;
  }
  public getCustomerGroups() {
    const params = {
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.fetchCustomerGroups(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.customerGroups = data.customergroup;
        } else {
          console.log(dec.message)
        }
      }
    });
  }
  getPercentage(a, b) {
    let c = (parseFloat(a)) + parseFloat(b);
    return (((parseFloat(b)) / (c)) * 100).toFixed(2);
  }
  public getCompanies() {
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.companyData = data.getCompanies;
        if (this.dtc) {
          this.companyId = data.getCompanies.map(x => x._id);
          this.companyIdFilter = data.getCompanies.map(x => x._id)
          this.companyIdFilter.push('all')
        }
        this.getPromoHistory()
      }
    });

  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.companyId = this.companyIdFilter.slice()
    }
    else {
      this.companyId = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }
  selectAllCompany() {
    if (this.companyId.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.companyId = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.companyId = [];
      this.companyIdFilter = [];
    }
  }
}
