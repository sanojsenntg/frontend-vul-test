import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PaymentTypeService } from '../../../../common/services/paymenttype/paymenttype.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-payment-type',
  templateUrl: './list-payment-type.component.html',
  styleUrls: ['./list-payment-type.component.css'],
})

export class ListPaymentTypeComponent implements OnInit {
  session;
  key: string = '';
  public paymentTypeData;
  public id;
  public i;
  itemsPerPage = 10;
  pageNo = 0;
  paymentTypeLength;
  public payment_type;
  public is_search = false;
  searchValue;
  sortOrder = 'asc';
  public keyword = '';
  public searchLoader = false;
  del = false;
  public aclAdd=false;
  public aclEdit=false;
  public aclDelete=false;
  public companyId = [];

  constructor(private _paymentTypeService: PaymentTypeService,
    private router: Router,
    public dialog: MatDialog,
    public overlay: Overlay,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public _aclService : AccessControlService,
    private toastr: ToastsManager,
    public encDecService:EncDecService,
    public jwtService: JwtService) { 
      this.companyId.push( localStorage.getItem('user_company'))
      this.session = localStorage.getItem('Sessiontoken');
    }

  /**
   * Function which runs first when the page loads
   */
  ngOnInit() {
    this.aclDisplayService();
    this.is_search = false;
    this.searchLoader = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
      search:''
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
      if(dec){
        this.searchLoader = false;
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.paymentTypeData = data.paymentTypes;
          this.paymentTypeLength = data.totalCount;
        }
      }
    });
  }

  /**
  * Confirmation popup for deleting particular payment type record
  * @param id 
  */
  public aclDisplayService(){
    var params = {
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          for(let i=0; i<data.menu.length; i++){
                if(data.menu[i]=='Payment Extra - Add'){
                  this.aclAdd = true;
                }else if (data.menu[i]=='Payment Extra - Edit'){
                  this.aclEdit = true;
                }else if (data.menu[i]=='Payment Extra - Delete'){
                  this.aclDelete = true;
                }
            };
        }
      }else{
        console.log(dec.message)
      }
 
    })
  }
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deletePaymentType(id);
      } else {
      }
    });
  }

  /**
   * Delete payment type from view
   * @param id 
   */
  public deletePaymentType(id) {
    var params = {
      _id: id,
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentTypeService.updateDeletedStatus(enc_data)
      .then((dec) => {
        if (dec.status === 200) {
          this.refetchPaymentType();
          this.paymentTypeData.splice(id, 1);
          this._toasterService.success('Payment type deleted successfully.');
        } else if (dec.status === 201) {
        }
      });
  }

  /**
   * To refresh payment type records showing in the grid
   */
  public refetchPaymentType() {
      this.getPaymentTypes(this.keyword);
  }

  /**
   * To reset the search filters and reload the payment type records
   */
  public reset() {
    this.keyword = '';
    this.payment_type='';
    this.ngOnInit();
  }

  /**
   * For sorting payment type records 
   * @param key 
   */
  sort(key) {
    this.searchLoader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.sortOrder,
      sortByColumn: this.key,
      company_id: this.companyId,
      search: this.keyword
    };
    var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
    this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.paymentTypeData = data.paymentTypes;
          this.searchLoader = false;
        }
      }else{
        console.log(dec.message)
      }
    
    });
  }

  /**
   * Pagination for payment type module
   * @param data 
   */
  pagingAgent(data) {
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      search: this.keyword
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.paymentTypeData = data.paymentTypes;
        }
      }else{
        console.log(dec.message)
      }
    });
  }

  /**
  * For searching payment type records 
  * @param keyword 
  */
  public getPaymentTypes(keyword) {
    this.searchLoader = true;
    this.keyword = keyword;
   
    
    if (this.keyword) {
      const params = {
        offset: 0,
        limit: this.itemsPerPage,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId,
        search:this.keyword
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
        if(dec){
          this.searchLoader = false;
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec.data);
            this.paymentTypeData = data.paymentTypes;
            this.paymentTypeLength = data.totalCount;
          }
        }else{
          console.log(dec.message)
        }
      
      });
    } else {
      const params = {
        offset: 0,
        limit: this.itemsPerPage,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId,
        search:''
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session,dec.data);
            this.paymentTypeData = data.paymentTypes;
            this.paymentTypeLength = data.totalCount;
            this.searchLoader = false;
            this.is_search = false;
          }
        }else{
          console.log(dec.message)
        }
      });
    }
  }
}
