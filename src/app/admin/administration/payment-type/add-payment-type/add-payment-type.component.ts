import { Component, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentTypeService } from '../../../../common/services/paymenttype/paymenttype.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-add-payment-type',
  templateUrl: './add-payment-type.component.html',
  styleUrls: ['./add-payment-type.component.css']
})
export class AddPaymentTypeComponent {
  public companyId = [];
  public paymentTypeModel: any = {
    company: '',
    payment_type: '',
    available_for: '',
    is_default_payment: '',
    company_id: this.companyId
  };
  session;
  constructor(private _paymentTypeService: PaymentTypeService,
    private _toasterService: ToastsManager,
    private router: Router,
    private vcr: ViewContainerRef,
    public encDecService:EncDecService,
    public jwtService: JwtService) { 
      this.companyId.push( localStorage.getItem('user_company'))
      this.session = localStorage.getItem('Sessiontoken');
    }

  /**
  * Save payment type data
  *
  * @param formData
  */
  addPaymentType(formData): void {
    if (formData.valid) {
      var encrypted = this.encDecService.nwt(this.session,this.paymentTypeModel);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._paymentTypeService.savePaymentType(enc_data)
        .then((dec) => {
          if (dec && dec.status === 200) {
            this._toasterService.success('Payment type added successfully.');
            this.router.navigate(['/admin/administration/payment-type']);
          } else {
            this._toasterService.error('Payment type addition failed.');
          }
        })
    } else {
      return;
    }
  }
}

