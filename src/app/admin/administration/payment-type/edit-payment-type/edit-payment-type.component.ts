import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { PaymentTypeService } from '../../../../common/services/paymenttype/paymenttype.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-payment-type',
  templateUrl: './edit-payment-type.component.html',
  styleUrls: ['./edit-payment-type.component.css']
})
export class EditPaymentTypeComponent implements OnInit {
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public companyId = [];
  session;
  constructor(private _paymentTypeService: PaymentTypeService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public encDecService:EncDecService,
    public jwtService: JwtService) {
    this.editForm = formBuilder.group({
      company: ['', [Validators.required]],
      payment_type: ['', [Validators.required]],
      available_for: ['', [Validators.required]],
      is_default_payment: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');

    this.editForm.controls['company'].disable();
    this.editForm.controls['payment_type'].disable();
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      _id: this._id,
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentTypeService.getPaymentTypeById(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var res:any = this.encDecService.dwt(this.session,dec.data);
          this.editForm.setValue({
            company: res.PaymentTypeById.company ? res.PaymentTypeById.company : '',
            payment_type: res.PaymentTypeById.payment_type ? res.PaymentTypeById.payment_type : '',
            available_for: res.PaymentTypeById.available_for ? res.PaymentTypeById.available_for : '',
            is_default_payment: res.PaymentTypeById.is_default_payment ? res.PaymentTypeById.is_default_payment : '',
          });
        }
      }
     
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
  }

  /**
   * Update payment type records
   */
  public updatePaymentType() {
    if (!this.editForm.valid) {
      return;
    }
    const paymentTypeData = {
      _id:this._id,
      available_for: this.editForm.value.available_for,
      is_default_payment: this.editForm.value.is_default_payment
    }
    var encrypted = this.encDecService.nwt(this.session,paymentTypeData);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._paymentTypeService.updatePaymentType(enc_data).then((dec) => {
        if (dec.status === 200) {
          this._toasterService.success('Payment type updated successfully.');
          this.router.navigate(['/admin/administration/payment-type']);
        } else if (dec.status === 201) {
          this._toasterService.error('Payment type updation failed.');

        }
        this.router.navigate(['/admin/administration/payment-type']);
      })
      .catch((error) => {
        this.router.navigate(['/login']);
      });
  }
}





