import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentTypeService } from './../../../common/services/paymenttype/paymenttype.service';
import { PaymentTypeRestService } from './../../../common/services/paymenttype/paymenttyperest.service';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListPaymentTypeComponent } from './list-payment-type/list-payment-type.component';
import { AddPaymentTypeComponent } from './add-payment-type/add-payment-type.component';
import { EditPaymentTypeComponent } from './edit-payment-type/edit-payment-type.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    Ng2OrderModule,
    MatTooltipModule,
    MatSelectModule,
    MatDialogModule
  ],
  declarations: [ 
    ListPaymentTypeComponent,
    AddPaymentTypeComponent,
    EditPaymentTypeComponent,
  ],
  providers: [ 
    PaymentTypeService,  
    PaymentTypeRestService
  ],
  exports: [
    RouterModule
  ],
})
export class PaymentTypeModule { }
