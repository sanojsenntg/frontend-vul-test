import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-social-status-dialog',
  templateUrl: './social-status-dialog.component.html',
  styleUrls: ['./social-status-dialog.component.css']
})
export class SocialStatusDialogComponent  {

  constructor(public dialogRef: MatDialogRef<SocialStatusDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
