
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListSocialSettingsComponent } from './list-social-settings/list-social-settings.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const socialSettingsRoutes: Routes = [
     { path: '', component: ListSocialSettingsComponent, canActivate: [AclAuthervice], data: {roles: ["Social Settings - List"]}} 
];
