import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatDialogModule } from '@angular/material/dialog';

import { SocialStatusDialogComponent } from './dialog/social-status-dialog/social-status-dialog.component'
import { SocialSettingsService } from '../../../common/services/social-settings/social-settings.service'
import { SocialSettingsrestService } from '../../../common/services/social-settings/social-settingsrest.service';
import { ListSocialSettingsComponent } from './list-social-settings/list-social-settings.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    MatDialogModule
  ],
  declarations: [ 
    ListSocialSettingsComponent ,
    SocialStatusDialogComponent
  ],
  providers: [
    SocialSettingsService,
    SocialSettingsrestService
  ],
  entryComponents: [ 
    SocialStatusDialogComponent 
  ]
})
export class SocialSettingsModule { }
