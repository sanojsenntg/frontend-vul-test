import { Component, OnInit } from '@angular/core';
import { SocialSettingsService } from '../../../../common/services/social-settings/social-settings.service';
import * as moment from 'moment/moment';
import { SocialStatusDialogComponent } from '../dialog/social-status-dialog/social-status-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { Router } from '@angular/router';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-social-settings',
  templateUrl: './list-social-settings.component.html',
  styleUrls: ['./list-social-settings.component.css']
})
export class ListSocialSettingsComponent implements OnInit {
  key: string = '';
  public socialSettingsData;
  public socialSettingsLength;
  itemsPerPage = 10;
  pageNo = 0;
  sortOrder = 'desc';
  public showloader;
  public changeStatus;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public companyId: any = [];
  public session;
  constructor(private _socialSettingsService: SocialSettingsService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    public _aclService: AccessControlService,
    private router: Router,
    public encDecService: EncDecService,
    public jwtService: JwtService) {
      this.companyId.push( localStorage.getItem('user_company'))
      this.session = localStorage.getItem('Sessiontoken');
  }
  ngOnInit() {
    this.aclDisplayService();
    this.showloader = true;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._socialSettingsService.getAllSocialSettings(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.socialSettingsData = data.getSocialSettings;
          this.socialSettingsLength = data.count;
        }else{
          console.log(dec.message)
        }
      }
      this.showloader = false;
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Social Settings - Edit") {
              this.aclAdd = true;
            }
          };
        }
      }else{
        console.log(dec.message)
      }
     
    })
  }
  /**
   * Open dialog for block a driver.
   * @param id
   * @param index
   */
  openDialogToDisable(id, index): void {
    const dialogRef = this.dialog.open(SocialStatusDialogComponent, {
      width: '450px',
      height: '150px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to disable this social setting' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.ChangeSocialStatus(id, index);
        this.toastr.success('Social setting disabled successfully.');
      } else {

      }
    });
  }

  /**
   * To change status of social settings
   * @param socialSettings
   * @param index
   */
  ChangeSocialStatus(socialSettings, index) {
    if (socialSettings.status == '1') {
      this.changeStatus = '0';
    } else {
      this.changeStatus = '1';
    }
    const params = {
      status: this.changeStatus,
      company_id: this.companyId,
      _id: socialSettings._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email'),
    }
    this._socialSettingsService.updateSocialStatus(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data: any = this.encDecService.dwt(this.session, dec.data);
          if (data) {
            this.socialSettingsData[index].status = data.updatedSocialStatus.status;
          }
        }
      }else{
        console.log(dec.message)
      }
    });
  }

  /**
   * Open dialog for enable a social settings.
   * @param id
   * @param index
   */
  openDialogToEnable(id, index): void {
    const dialogRef = this.dialog.open(SocialStatusDialogComponent, {
      width: '450px',
      height: '150px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to enable this social setting' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.ChangeSocialStatus(id, index);
        this.toastr.success('Social setting enabled successfully.');
      } else {

      }
    });
  }


  /**
  * For sorting social settings records 
  * @param key 
  */
  sort(key) {
    this.showloader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    var params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.sortOrder,
      sortByColumn: this.key,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._socialSettingsService.getAllSocialSettings(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){ 
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.socialSettingsData = data.getSocialSettings;
          this.socialSettingsLength = data.count;
        }
      }else{
        console.log(dec.message)
      }
      this.showloader = false;
    });
  }

  /**
   * Used for pagination
   * @param data 
   */
  pagingAgent(data) {
    this.showloader = true;
    this.socialSettingsData = [];
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    var params;
    params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._socialSettingsService.getAllSocialSettings(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.socialSettingsData = data.getSocialSettings;
        }
      }else{
        console.log(dec.message)
      }
      this.showloader = false;
    });
  }

}



