import { Component, OnInit } from '@angular/core';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {
  public session;
  public companyId:any = [];
  public aclCheck;
  public shown = {
    'promo': false,
    'partners': false,
    'payment': false,
    'vehicles': false,
    'zones': false,
    'other': false
  }
  constructor(
    public _aclService:AccessControlService,
    public encDecService:EncDecService) { 
  }
  ngOnInit() {
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
    this.aclDisplayService();
  }
  public aclDisplayService(){
    this.aclCheck = JSON.parse(localStorage.getItem('userMenu'));
    // var params = {
    //   company_id: this.companyId
    // }
    // var encrypted = this.encDecService.nwt(this.session,params);
    // var enc_data = {
    //   data: encrypted,
    //   email: localStorage.getItem('user_email')
    // }
    // this._aclService.getAclUserMenu(enc_data).then((dec) => {
    //   if(dec){
    //     if(dec.status==200){
    //       var data:any = this.encDecService.dwt(this.session,dec.data);
    //       this.aclCheck = data.menu;
    //     }
    //   }else{
    //     console.log(dec.message)
    //   }
    // })

  }
  show(data) {
    let list = [];
    if (!this.aclCheck)
      return false;
    let i = 0;
    switch (data) {
      case 'other':
        list = ['Zendesk ticket - List','Predefined Messages - List', 'Message History', 'FAQ - List', 'Social Settings - List', 'Contact us Category - List', 'Point Of Interest - List', 'Administration-edit-order'];
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'tariff':
        list = ['Tariffs - List']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'zones':
        list = ['Zone-List', 'Dubai Major Zone - List', 'Drive through payment zones / Tolls','Heat Map'];
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'vehicles':
        list = ['Vehicle Categories - List', 'Vehicle Sub Categories - List'];
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'partners':
        list = ['Partners List']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'payment':
        list = ['Payment Extra List', 'Payment Type List']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'promo':
        list = ['Promocodes', 'Promocode history']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'messages':
        list = ['Predefined Messages - List', 'Message History']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'companies':
        list = ['Companies-List']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
    }

  }


}
