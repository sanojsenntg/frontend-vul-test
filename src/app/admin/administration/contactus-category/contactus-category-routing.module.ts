
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { ListContactusCategoryComponent } from './list-contactus-category/list-contactus-category.component';
import { AddContactusCategoryComponent } from './add-contactus-category/add-contactus-category.component'
import { EditContactusCategoryComponent } from './edit-contactus-category/edit-contactus-category.component'
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service'

export const contactusCategoryRoutes: Routes = [
     { path: '', component: ListContactusCategoryComponent},
     { path: 'add', component: AddContactusCategoryComponent , canActivate: [AclAuthervice], data: {roles: ["Contact us Category - Add"]}},
     { path: 'edit/:id', component: EditContactusCategoryComponent , canActivate: [AclAuthervice], data: {roles: ["Contact us Category - Edit"]}}
];
