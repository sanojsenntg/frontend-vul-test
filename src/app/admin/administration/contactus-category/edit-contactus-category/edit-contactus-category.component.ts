import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { ContactusCategoryService } from '../../../../common/services/contactus-category/contactus-category.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';



@Component({
  selector: 'app-edit-contactus-category',
  templateUrl: './edit-contactus-category.component.html',
  styleUrls: ['./edit-contactus-category.component.css']
})
export class EditContactusCategoryComponent implements OnInit {

  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public companyId = [];
  public session;
  constructor(
          private _contactusCategoryService: ContactusCategoryService,
          private formBuilder: FormBuilder,
          private route: ActivatedRoute,
          private router: Router,
          private _toasterService: ToastsManager,
          public encDecService:EncDecService,
          public jwtService: JwtService ) {
            this.editForm = formBuilder.group({
              category: ['', [Validators.required]]
            });
          }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      _id:this._id,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._contactusCategoryService.getContactusCategoryById(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var res:any = this.encDecService.dwt(this.session,dec.data);
          this.editForm.setValue({
            category: res.ContactusCategoryById.category ? res.ContactusCategoryById.category : ''
          });
        }
      }else{
        console.log(dec?dec.message:'Something went wrong')
      }
    })
  }

  /**
   * Update contactus category records
   */
  public updateContactusCategory() {
    if (!this.editForm.valid) {
      return;
    }
    const params = {
      _id:this._id,
      category: this.editForm.value.category,
      companyId:this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._contactusCategoryService.UpdateContactusCategory(enc_data).then((dec) => {
      if (dec.status === 200) {
        this._toasterService.success('Contactus category updated successfully.');
        this.router.navigate(['/admin/administration/contactus-category']);
      } else if (dec.status === 201) {
        this._toasterService.error('Contactus category updation failed.');
      }
      this.router.navigate(['/admin/administration/contactus-category']);
    })
  }
}







