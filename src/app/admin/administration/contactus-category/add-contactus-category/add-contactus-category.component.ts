import { Component, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ContactusCategoryService } from '../../../../common/services/contactus-category/contactus-category.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-contactus-category',
  templateUrl: './add-contactus-category.component.html',
  styleUrls: ['./add-contactus-category.component.css']
})
export class AddContactusCategoryComponent {
  public companyId = [];
  public session;
  public contactusCategoryModel: any = {
    category: ''
  };
  constructor(
    private _contactusCategoryService: ContactusCategoryService,
    private _toasterService: ToastsManager,
    private router: Router,
    public jwtService: JwtService,
    public encDecService: EncDecService
  ) {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
  }

  /**
  * Save contactus category data
  *
  * @param formData
  */
  addContactusCategory(formData): void {
    if (formData.valid) {
      var params = {
        category: this.contactusCategoryModel.category,
        company_id: this.companyId
      }
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._contactusCategoryService.AddNewContactusCategory(enc_data)
        .then((dec) => {
          if (dec && dec.status === 200) {
            this._toasterService.success('Contactus category added successfully.');
            this.router.navigate(['/admin/administration/contactus-category']);
          } else {
            this._toasterService.error('Contactus category addition failed.');
          }
        })
    }
  }
}

