import { Component, OnInit } from '@angular/core';

import { Overlay } from '@angular/cdk/overlay';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';

import { ContactusCategoryService } from '../../../../common/services/contactus-category/contactus-category.service';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-contactus-category',
  templateUrl: './list-contactus-category.component.html',
  styleUrls: ['./list-contactus-category.component.css']
})
export class ListContactusCategoryComponent implements OnInit {

  key: string = '';
  public contactusCategoryData;
  public contactusCategoryLength;
  itemsPerPage = 10;
  pageNo = 0;
  sortOrder = 'desc';
  public showloader;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  companyId=[];
  public session;

  constructor(
    private router: Router,
    public jwtService: JwtService,
    private _contactusCategoryService: ContactusCategoryService,
    public overlay: Overlay,
    public dialog: MatDialog,
    public _toasterService: ToastsManager,
    public _aclService: AccessControlService,
    public encDecService:EncDecService) {
  }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.aclDisplayService();
    this.showloader = true;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._contactusCategoryService.getAllContactusCategory(enc_data).then((dec) => {
      this.showloader = false;
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.contactusCategoryData = data.getCategory;
          this.contactusCategoryLength = data.count;
        }
      }else{
        console.log(dec?dec.message:'Something went wrong')
      }
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == "Contact us Category - Add") {
              this.aclAdd = true;
            } else if (data.menu[i] == "Contact us Category - Edit") {
              this.aclEdit = true;
            } else if (data.menu[i] == "Contact us Category - Delete") {
              this.aclDelete = true;
            }
          };
        }
      }else{
        console.log(dec.message)
      }

    })
  }
  /**
  * Open dialog for deleting a particular contactus category record by id
  * @param id 
  */
  openDeleteDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.deleteSocialSetting(id);
      } else {
      }
    });
  }

  /**
   * Soft delete contactus category by id
   * @param id 
   */
  public deleteSocialSetting(id) {
    var params = {
      _id: id,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._contactusCategoryService.updateDeleteStatus(enc_data)
      .then((dec) => {
        if(dec){
          if(dec.status == 200){
            var res:any = this.encDecService.dwt(this.session,dec.data);
            this.ngOnInit();
            this.contactusCategoryData.splice(id, 1);
            this._toasterService.success('Contactus category deleted successfully.');
          }
        }else{
          console.log(dec.message)
        }
      });
  }

  /**
  * For sorting contactus category records 
  * @param key 
  */
  sort(key) {
    this.showloader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
    var params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.sortOrder,
      sortByColumn: this.key,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._contactusCategoryService.getAllContactusCategory(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.contactusCategoryData = data.getCategory;
          this.contactusCategoryLength = data.count;
          this.showloader = false;
        }
      }else{
        console.log(dec.message)
      }
    });
  }

  /**
   * Used for pagination in contactus category
   * @param data 
   */
  pagingAgent(data) {
    this.showloader = true;
    this.contactusCategoryData = [];
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    var params;
    params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._contactusCategoryService.getAllContactusCategory(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.showloader = false;
          this.contactusCategoryData = data.getCategory;
        }
      }else{
        console.log(dec.message)
      }
    });
  }

}



