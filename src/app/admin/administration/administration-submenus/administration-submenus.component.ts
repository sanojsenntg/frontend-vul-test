import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-administration-submenus',
  templateUrl: './administration-submenus.component.html',
  styleUrls: ['./administration-submenus.component.css']
})
export class AdministrationSubmenusComponent implements OnInit {
  public menuName = '';
  public aclCheck;
  public submenus: any = [];
  companyId: any = [];
  public session;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public _aclService: AccessControlService,
    public encDecService: EncDecService) {
      
  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'));
    this.session = localStorage.getItem('Sessiontoken');
    this.aclDisplayService();
    this.submenus = [];
    this.route.params.subscribe((params) => {
      if (params['submenu']) {
        this.submenus.promo = false;
        this.submenus.partner = false;
        this.submenus.payment = false;
        this.submenus.vehicle = false;
        this.submenus.zone = false;
        this.submenus.other = false;
        this.submenus.messages = false;
        switch (params['submenu']) {
          case 'promo':
            this.menuName = 'Promo Management';
            this.submenus.promo = true;
            break;
          case 'partners':
            this.menuName = 'Partners';
            this.submenus.partner = true;
            break;
          case 'payment':
            this.menuName = 'Payment';
            this.submenus.payment = true;
            break;
          case 'vehicles':
            this.menuName = 'Vehicles';
            this.submenus.vehicle = true;
            break;
          case 'zones':
            this.menuName = 'Zones';
            this.submenus.zone = true;
            break;
          // case 'tariff':
          //     this.menuName = 'Tariff';
          //     this.submenus.vehicle = true;
          //     break;
          case 'other':
            this.menuName = 'Administration - Others';
            this.submenus.other = true;
            break;
          case 'messages':
            this.menuName = 'Messages';
            this.submenus.messages = true;
            break;
          default:
            this.router.navigate(['admin'])
        }
      }
      else {
        this.router.navigate(['admin'])
      }
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.aclCheck = data.menu;
        }
      }else{
        console.log(dec.message)
      }
    })
  }
}