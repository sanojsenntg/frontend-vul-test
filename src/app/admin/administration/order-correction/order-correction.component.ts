import { Component, OnInit, ViewContainerRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { JwtService } from '../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';
import * as moment from 'moment/moment';
import { Overlay } from '@angular/cdk/overlay';
import { FormControl } from '@angular/forms';
import { Socket } from 'ng-socket-io';
import { environment } from '../../../../environments/environment.prod';
import { OrderService } from '../../../common/services/order/order.service';
import { jsonize } from '@ngui/map/dist/services/util';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { PartnerService } from '../../../common/services/partners/partner.service';
import { UserIdleService } from 'angular-user-idle';
import { DeleteDialogComponent } from '../../../common/dialog/delete-dialog/delete-dialog.component';
import { TwoDigitDecimaNumberDirective } from '../../../common/directive/pricevalidationdirective'
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-order-correction',
  templateUrl: './order-correction.component.html',
  styleUrls: ['./order-correction.component.css']
})
export class OrderCorrectionComponent implements OnInit {
  public companyId = [];
  public shift_id = '';
  public allOrderIddata;
  public loadingIndicator;
  public OrderData;
  public orderform;
  public otpresponse;
  public orderlist = false;
  public searchLoader = false;
  public otp;
  public ordersArray = [];
  public partnersData;
  public login_status = false;
  public selected_shift_refernce;
  public subscripttimestart;
  public subscripttimeout;
  session;
  orderCtrl: FormControl = new FormControl();
  constructor(private router: Router, public dialog: MatDialog, private userIdle: UserIdleService, public overlay: Overlay, private _orderService: OrderService, private _partnerService: PartnerService, public toastr: ToastsManager, vcr: ViewContainerRef, public jwtService: JwtService, public encDecService: EncDecService) {
    this.toastr.setRootViewContainerRef(vcr);
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.subscripttimestart = this.userIdle.onTimerStart().subscribe(count => console.log(count));

    // Start watch when time is up.
    this.subscripttimeout = this.userIdle.onTimeout().subscribe(() => {
      this.userIdle.stopWatching();
      this.router.navigate(['/admin']);

    });
  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.sendOTP()
    this.getOrderId();
    this.getPartners();
    this.orderCtrl.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        this.newFn(query);
      });
  }
  newFn(query) {
    var params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'unique_shift_detail_id',
      search_keyword: query,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._orderService.getAllUniqueShiftId(enc_data).then(dec => {
      if (dec) {
        if (dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.allOrderIddata = result.getOrders;
          this.loadingIndicator = '';
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  public getPartners() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._partnerService.getPartner(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.partnersData = data.getPartners;
        }
      } else {
        console.log(dec.message)
      }

    });
  }
  updateOrders() {
    // checking added
    let pricecheck = true;
    for (var i = 0; i < this.OrderData.length; ++i) {
      if (this.OrderData[i].price === '' || this.OrderData[i].price === null || this.OrderData[i].price.charAt(0) === '.' || this.OrderData[i].price.charAt(this.OrderData[i].price.length - 1) === '.') {
        pricecheck = false;
      }
    }
    if (pricecheck) {
      let dialogRef = this.dialog.open(DeleteDialogComponent, {
        width: '450px',
        scrollStrategy: this.overlay.scrollStrategies.noop(),
        data: { text: 'Are you sure you want to edit the shift with reference number ' + this.selected_shift_refernce, type: 'confirm' }
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result != true) {
        } else {
          console.log("updated Orders" + JSON.stringify(this.OrderData));
          var user = JSON.parse(window.localStorage['adminUser']);
          console.log(user);
          if (!user) {
            this.toastr.error("Session Expired..please login again");
          } else {
            this.searchLoader = true;
            const params = {
              orders: this.OrderData,
              email: user.email,
              edited_by: user._id,
              unique_shift_detail_id: this.selected_shift_refernce,
              company_id: localStorage.getItem('user_company')
            };
            var encrypted = this.encDecService.nwt(this.session, params);
            var enc_data = {
              data: encrypted,
              email: localStorage.getItem('user_email')
            }
            this._orderService.updateOrderAfterEdit(enc_data).then((dec) => {
              if (dec.status == 200) {
                this.searchLoader = false;
                this.OrderData = [];
                this.shift_id = "";
                this.selected_shift_refernce = "";
                this.orderlist = false;
                this.toastr.success("Order Updated Successfully");
              } else {
                this.searchLoader = false;
                this.toastr.error("Failed to update Orders");
              }
            });

          }
        }
      })
    } else {
      this.toastr.error("Please enter a valid order amount");
    }
  }
  sendOTP() {
    var user = JSON.parse(window.localStorage['adminUser']);
    if (!user) {
      this.toastr.error("Session Expired..please login again");
    } else {
      const params = {
        email: user.email,
        edited_by: user._id,
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._orderService.sendEditOTP(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          this.toastr.success('OTP has been sent to registered number');
        } else {
          this.toastr.error('Failed to send OTP');
        }
      });
    }
  }
  displayFnOrderId(data): string {
    return data ? data.unique_shift_detail_id : data;
  }
  getOrderId() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'unique_shift_detail_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._orderService.getAllUniqueShiftId(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.allOrderIddata = data.getOrders;
        }
      } else {
        console.log(dec.message)
      }
    });
    //this.allOrderIddata=[100041116,100041169,100041168,250493,112334];

  }
  orderIdselected(event: MatAutocompleteSelectedEvent): void {
    console.log("Selected");
    this.orderform = event.option.value.unique_shift_detail_id;
    this.shift_id = event.option.value.unique_shift_detail_id;
    this.getOrderById(this.shift_id);
  }
  public getOrderById(order_id) {
    console.log("order_id" + order_id);
    this.selected_shift_refernce = order_id;
    var params = {
      _id: order_id,
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._orderService.getOrderByUniqueShiftId(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == "201") {
          this.toastr.error("internal server error occured");
        }
        else if (dec.status == "202") {
          this.toastr.error(dec.message);
        } else if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.OrderData = data.getOrders;
          if (this.OrderData.length > 0) {
            this.orderlist = true;
          } else {
            this.orderlist = false;
          }
        }
      }
    });
  }
  reset() {
    this.shift_id = "";
    this.selected_shift_refernce = "";
    this.orderlist = false;
    this.OrderData = [];
  }
  public selectedMoment;
  public currentdatetime;
  public selectedMoment1;
  verifyOTP() {
    var user = JSON.parse(window.localStorage['adminUser']);
    console.log(user);
    if (!user) {
      this.toastr.error("Session Expired..please login again");
    } else {
      const params = {
        email: user.email,
        edited_by: user._id,
        otp: this.otp,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._orderService.verifyOTP(enc_data).then((dec) => {
        if (dec.status == 200) {
          this.toastr.success('OTP Verified Successfully');
          this.login_status = true;
        } else {
          this.toastr.error(dec.message);
        }
      });
    }
  }
  cancel() {
    this.router.navigate(['/admin']);
  }
}
