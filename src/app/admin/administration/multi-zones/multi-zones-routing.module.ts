import { Routes, RouterModule } from '@angular/router';
import { PetrolZonesComponent } from './petrol-zones/petrol-zones.component';
import { HighwayZonesComponent } from './highway-zones/highway-zones.component';

export const MultiZoneRoutes: Routes = [
  {path:'', component:PetrolZonesComponent},
  {path:'highway', component:HighwayZonesComponent}
];

