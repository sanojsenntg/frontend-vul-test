import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetrolZonesComponent } from './petrol-zones.component';

describe('PetrolZonesComponent', () => {
  let component: PetrolZonesComponent;
  let fixture: ComponentFixture<PetrolZonesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetrolZonesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetrolZonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
