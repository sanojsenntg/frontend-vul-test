import { OnInit, ViewChild, Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DrawingManager } from '@ngui/map';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ZoneService } from '../../../../common/services/zones/zone.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { ToastrService } from 'ngx-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-highway-zones',
  templateUrl: './highway-zones.component.html',
  styleUrls: ['./highway-zones.component.css']
})
export class HighwayZonesComponent implements OnInit {
    public companyId = [];
    public center: any = 'dubai';
    public drawFigureType;
    public recanglebounds: any = {};
    public polygonbounds: any = [];
    public triangleCode = [];
    public triangleCoords = [];
    public polyCoord;
    data = [];
    map;
    shape;
    public sub;
    public _id;
    onFigureCompletePopup = false;
    public paymentData;
    public allPaymentZone;
    public zone_id;
    selectedOverlay: any;
    public zonesave: any = false;
    public markName;
    public session;
    public aoiPoly;
    public zonePrev;
    public countZones = [];
    public flag = true;
    public zoneName = 'highway_zone_';
    public count = 0;
    public index: any = false;
    public mapData = [[{ lat: 25.000622323292763, lng: 55.26896276981595 },
    { lat: 25.047286630899915, lng: 55.32320776493314 },
    { lat: 24.977594732600036, lng: 55.40079870731595 },
    { lat: 24.923431791327047, lng: 55.328014283487825 }], [
      { lat: 25.067813303297388, lng: 55.35754004032376 },
      { lat: 24.999377698951573, lng: 55.43581762821439 },
      { lat: 25.057239386283754, lng: 55.50722876102689 }
    ]]
    @ViewChild(DrawingManager) drawingManager: DrawingManager;
  
    constructor(
      private router: Router,
      public dialog: MatDialog,
      public overlay: Overlay,
      private route: ActivatedRoute,
      public _zoneService: ZoneService,
      public toastr: ToastrService,
      public jwtService: JwtService,
      public encDecService: EncDecService
    ) {
      this.sub = this.route.params.subscribe((params) => {
        this._id = params['id'];
        this.zone_id = {
          "zone_id": this._id,
          company_id: this.companyId
        }
      });
    }
  
  
    ngOnInit() {
      this.companyId.push(localStorage.getItem('user_company'))
      this.session = localStorage.getItem('Sessiontoken');
    }
    getZone() {
      var params = {
        company_id:localStorage.getItem('user_company')
      }
      var encrypted = this.encDecService.nwt(this.session, params );
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._zoneService.listHighwayZone(enc_data).then((dec) => {
        if(dec){
          if(dec.status == 200){
            var data:any = this.encDecService.dwt(this.session, dec.data );
            console.log(data);
            var zoneData = data.zones;
            this.count = data.zones_count;
            var center = zoneData[zoneData.length-1].loc.coordinates[0][0];
            this.center = {lat: center[1], lng:center[0]};
            for(var j=0; j<zoneData.length; j++){
              var coordinates = zoneData[j].loc.coordinates[0];
              var bounds = [];
              this.triangleCoords = [];
              for(var i=0; i<coordinates.length; i++){
                this.triangleCoords.push({lat:coordinates[i][1], lng:coordinates[i][0]})
              } 
              var customZones = {
                zone_name:  zoneData[j].name,
                zone: zoneData[j].loc.coordinates[0]
              }
              this.countZones.push(customZones);
              this.drawPolygon();
            }
          }
        }
      });
    }
  
    onMapReady(map) {
      this.map = map;
      this.drawingManager['initialized$'].subscribe(dm => {
        google.maps.event.addListener(dm, 'overlaycomplete', event => {
          this.selectedOverlay = event.overlay;
          var polygonBounds: any = this.selectedOverlay.getPath();
          var bounds = [];
          for (var i = 0; i < polygonBounds.length; i++) {
            var point = [polygonBounds.getAt(i).lng(), polygonBounds.getAt(i).lat()]
            bounds.push(point);
          }
          bounds.push([polygonBounds.getAt(0).lng(), polygonBounds.getAt(0).lat()])
          if (!this.flag) {
            this.zonePrev.setEditable(false);
          }
  
          this.selectedOverlay.setEditable(true);
          var customZones = {
            zone_name: this.zoneName + this.count++,
            zone: bounds
          }
          this.countZones.push(customZones);
          this.savePaths(this.selectedOverlay);
          if (event.type !== google.maps.drawing.OverlayType.MARKER) {
            dm.setDrawingMode(null);
            google.maps.event.addListener(event.overlay, 'click', e => {
              this.selectedOverlay = event.overlay;
              this.savePaths(this.selectedOverlay);
              this.selectedOverlay.setEditable(true);
            });
            this.drawFigureType = event.type;
            this.selectedOverlay = event.overlay;
            this.onFigureCompletePopup = true;
            this.dialog.closeAll();
          }
        });
      });
      var myLatLng = {lat: 25.2048, lng: 55.2708};
      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map
      });
      google.maps.event.addListener(marker, 'click', (event)=> {
        marker.setPosition(null);
      });
      marker.setPosition(null);
      var pickup_input: any = document.getElementById('pickup-input');
      var autocomplete2 = new google.maps.places.Autocomplete(pickup_input);
      autocomplete2.setComponentRestrictions(
        { 'country': ['ae'] });
      google.maps.event.addListener(autocomplete2, 'place_changed',(event)=> {
        var place = autocomplete2.getPlace();
        var myLatLng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
        map.panTo(myLatLng);
        marker.setPosition(myLatLng);
        map.setZoom(18);
      });
      this.getZone();
    }
    drawPolygon() {
      var aoiPoly = new google.maps.Polygon({
        paths: this.triangleCoords,
        strokeColor: '#3e87bd',
        strokeOpacity: 1,
        strokeWeight: 2,
        fillColor: '#3e87bd',
        fillOpacity: 0.6,
        clickable: true,
        editable: false
      });
      aoiPoly.setMap(this.map);
      this.aoiPoly = aoiPoly;
      google.maps.event.addListener(aoiPoly, "click", (event) => {
        this.savePaths(aoiPoly);
        aoiPoly.setEditable(true);
      });
    }
    public savePaths(event) {
      if (!this.flag) {
        this.zonePrev.setEditable(false);
      }
      this.flag = false;
      this.zonePrev = event;
  
      var polygonBounds: any = event.getPath();
      var bounds = [];
      for (var i = 0; i < polygonBounds.length; i++) {
        var point = [polygonBounds.getAt(i).lng(), polygonBounds.getAt(i).lat()]
        bounds.push(point);
      }
      var len = bounds.length;
      if(bounds[0][0] != bounds[len-1][0]){
        bounds.push([polygonBounds.getAt(0).lng(), polygonBounds.getAt(0).lat()]);
      }
      if (bounds.length > 0) {
        this.index = this.countZones.findIndex((item, ind) => {
          if (bounds[0][0] == item.zone[0][0] && bounds[0][1] == item.zone[0][1]) return true;
        });
        this.countZones[this.index].zone = bounds;
      }
  
    }
    public butonClk() {
      var polygonBounds: any = this.aoiPoly.getPath();
      var bounds = [];
      for (var i = 0; i < polygonBounds.length; i++) {
        var point = {
          lat: polygonBounds.getAt(i).lat(),
          lng: polygonBounds.getAt(i).lng()
        };
        bounds.push(point);
      }
    }
  
    public zoneSave() {
      var zones = [];
      this.countZones.forEach((event) => {
        var item = {
          "loc": {
            "coordinates": [event.zone],
            "type": "Polygon"
          },
          "name": event.zone_name
        }
        zones.push(item);
      })
      var params = { 
        name: 'highway_zones', 
        zones: zones,
        zones_count:this.count,
        company_id:localStorage.getItem('user_company')
      }
      console.log(params);
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._zoneService.saveHighwayZone(enc_data).then((dec) => {
        console.log(dec);
        if(dec && dec.status == 200){
          var data:any = this.encDecService.dwt(this.session, dec.data);
          console.log(data.data.length);
          this.toastr.success('Current Zones Count : ' + data.data.length, 'Zones Saved Successfully..')
        } else {
          this.toastr.error(dec.message)
        }
      });
    }
  
    deleteSelectedOverlay() {
      if (this.zonePrev) {
        this.zonePrev.setMap(null);
        if (this.index || this.index == 0) {
          this.countZones.splice(this.index, 1);
        }
      }
    }
  }