import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighwayZonesComponent } from './highway-zones.component';

describe('HighwayZonesComponent', () => {
  let component: HighwayZonesComponent;
  let fixture: ComponentFixture<HighwayZonesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighwayZonesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighwayZonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
