import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NguiMapModule } from '@ngui/map';
import { PetrolZonesComponent } from './petrol-zones/petrol-zones.component';
import { HighwayZonesComponent } from './highway-zones/highway-zones.component';
import { ToastrModule } from 'ngx-toastr';
@NgModule({
  imports: [
    CommonModule,
    NguiMapModule,
    ToastrModule.forRoot()
  ],
  declarations: [PetrolZonesComponent, HighwayZonesComponent]
})
export class MultiZonesModule { }
