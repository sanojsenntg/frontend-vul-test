import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTooltipModule } from '@angular/material/tooltip';

import { FaqService } from '../../../common/services/faq/faq.service'
import { FaqRestService } from '../../../common/services/faq/faqrest.service';
import { FaqListComponent } from './faq-list/faq-list.component';
import { AddFaqComponent } from './add-faq/add-faq.component';
import { EditFaqComponent } from './edit-faq/edit-faq.component'

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    FaqListComponent, 
    AddFaqComponent, 
    EditFaqComponent
  ],
  providers: [ 
    FaqService,  
    FaqRestService
  ],
})
export class FaqModule { }
