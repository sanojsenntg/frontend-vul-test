import { Component, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FaqService } from '../../../../common/services/faq/faq.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.css']
})
export class AddFaqComponent {

  public companyId = localStorage.getItem('user_company');;
  public faqModel: any = {
    question: '',
    answer: '',
    company_id: this.companyId
  };
  session;
  constructor(private _faqService: FaqService,
    private _toasterService: ToastsManager,
    private router: Router,
    public encDecService: EncDecService,
    public jwtService: JwtService) {
    this.companyId = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
  }

  /**
  * Save FAQ data
  *
  * @param formData
  */
  addFAQ(formData): void {
    if (formData.valid) {
      var encrypted = this.encDecService.nwt(this.session, this.faqModel);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._faqService.AddNewFaq(enc_data)
        .then((dec) => {
          if (dec && dec.status === 200) {
            this._toasterService.success('FAQ added successfully.');
            this.router.navigate(['/admin/administration/faq']);
          } else {
            this._toasterService.error('FAQ addition failed.');
          }
        });
    } else {
      return;
    }
  }
}

