import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { FaqService } from '../../../../common/services/faq/faq.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-edit-faq',
  templateUrl: './edit-faq.component.html',
  styleUrls: ['./edit-faq.component.css']
})
export class EditFaqComponent implements OnInit {

  public companyId = [];
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public session;

  constructor(
          private _faqService: FaqService,
          private formBuilder: FormBuilder,
          private route: ActivatedRoute,
          private router: Router,
          private _toasterService: ToastsManager,
          public jwtService: JwtService,
          public encDecService:EncDecService ) {
            this.editForm = formBuilder.group({
              question: ['', [Validators.required]],
              answer: ['', [Validators.required]]
            });
          }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');

    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params ={
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._faqService.getFaqById(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var res:any = this.encDecService.dwt(this.session,dec.data);
          this.editForm.setValue({
            question: res.QuestionAnswersById.question ? res.QuestionAnswersById.question : '',
            answer: res.QuestionAnswersById.answer ? res.QuestionAnswersById.answer : ''
          });
        }
      }
    })
  }

  /**
   * Update FAQ records
   */
  public updateFAQ() {
    if (!this.editForm.valid) {
      return;
    }
    const faqData = {
      question: this.editForm.value.question,
      answer: this.editForm.value.answer,
      _id:this._id,
      company_id: localStorage.getItem('user_company')
    }
    var encrypted = this.encDecService.nwt(this.session,faqData);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._faqService.UpdateFaq(enc_data).then((dec) => {
      if (dec.status === 200) {
        this._toasterService.success('FAQ updated successfully.');
        this.router.navigate(['/admin/administration/faq']);
      } else if (dec.status === 201) {
        this._toasterService.error('FAQ updation failed.');
      }
      this.router.navigate(['/admin/administration/faq']);
    })
  }
}






