
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { FaqListComponent } from './faq-list/faq-list.component';
import { AddFaqComponent } from './add-faq/add-faq.component'
import { EditFaqComponent } from './edit-faq/edit-faq.component';
import { AclAuthervice } from '../../../common/services/access-control/acl-auth.service';

export const faqRoutes: Routes = [
     { path: '', component: FaqListComponent, canActivate: [AclAuthervice], data: {roles: ["FAQ - List"]}},
     { path: 'add', component: AddFaqComponent, canActivate: [AclAuthervice], data: {roles: ["FAQ - Add"]} },
     { path: 'edit/:id', component: EditFaqComponent, canActivate: [AclAuthervice], data: {roles: ["FAQ - Edit"]} }
];
