
import { Component, OnInit } from '@angular/core';

import { Overlay } from '@angular/cdk/overlay';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';

import { FaqService } from '../../../../common/services/faq/faq.service';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component'
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { Router } from '@angular/router';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-faq-list',
  templateUrl: './faq-list.component.html',
  styleUrls: ['./faq-list.component.css']
})
export class FaqListComponent implements OnInit {
  session;
  key: string = '';
  public faqData;
  public faqLength;
  itemsPerPage = 10;
  pageNo = 0; 
  sortOrder = 'desc';
  public showloader;
  public aclAdd=false;
  public aclEdit=false;
  public aclDelete=false;
  public companyId = [];
  constructor(  private _faqService: FaqService,
                public overlay : Overlay,
                public dialog : MatDialog,
                public _toasterService : ToastsManager,
                public _aclService : AccessControlService,
                private router: Router,
                public jwtService: JwtService,
                public encDecService:EncDecService) { 
               
                }

  ngOnInit() {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.aclDisplayService();
    this.showloader = true;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._faqService.getAllFaq(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.showloader = false;
          this.faqData = data.getQuestionAnswers;
          this.faqLength = data.count;
        }
      }
    });
  }
  public aclDisplayService(){
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
    var data:any = this.encDecService.dwt(this.session,dec.data);
    if(dec){
      if(dec.status == 200){
        for(let i=0; i<data.menu.length; i++){
          if(data.menu[i]=="FAQ - Add"){
            this.aclAdd = true;
          }else if (data.menu[i]=="FAQ - Edit"){
            this.aclEdit = true;
          }else if (data.menu[i]=="FAQ - Delete"){
            this.aclDelete = true;
          }
      };
      }
    }else{
      console.log(dec.message)
    }
  
    })
  }
  /**
  * Open dialog for deleting a particular FAQ record by id
  * @param id 
  */
  openDeleteDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.deleteSocialSetting(id);
      } else {
      }
    });
  }

  /**
   * Soft delete FAQ by id
   * @param id 
   */
  public deleteSocialSetting(id) {
    var params = {
      _id:id,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._faqService.updateDeleteStatus(enc_data)
      .then((dec) => {
        if (dec.status === 200) {
          this.ngOnInit();
          this.faqData.splice(id, 1);
          this._toasterService.success('FAQ deleted successfully.');
        } else if (dec.status === 201) {
        }
      });
  }

  /**
  * For sorting FAQ records 
  * @param key 
  */
  sort(key) {
    this.showloader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }  
    var params = {
          offset: this.pageNo,
          limit: this.itemsPerPage,
          sortOrder: this.sortOrder,
          sortByColumn: this.key,
          company_id: this.companyId
        };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._faqService.getAllFaq(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.faqData = data.getQuestionAnswers;
          this.faqLength = data.count;
          this.showloader = false;
        }
      }else{
        console.log(dec.message)
      }
    });
  }

  /**
   * Used for pagination in FAQ
   * @param data 
   */
  pagingAgent(data) {
    this.showloader = true;
    this.faqData = [];
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    var params;
        params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId
      };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._faqService.getAllFaq(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.showloader = false;
          this.faqData = data.getQuestionAnswers;
        }
      }else{
        console.log(dec.message)
      }
    });
  }

}


