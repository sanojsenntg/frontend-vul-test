import { Component, OnInit } from '@angular/core';
import { JwtService } from '../../common/services/api/jwt.service';
import { UsersService } from '../../common/services/user/user.service';
import { AccessControlService } from './../../common/services/access-control/access-control.service';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { EncDecService } from '../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public company_id;
  public session;
  public email;
  private loginUser;
  public userData;
  public profile_picture;
  public spandata = 'no';
  public companyId: any = [];

  constructor(private jwtService: JwtService,
    private _usersService: UsersService,
    public _aclService: AccessControlService,
    public encDecService: EncDecService,
    public toastr: ToastsManager) {
    //this.companyId.push(company_id);
  }
  ngOnInit() {
    this.company_id = window.localStorage.getItem('user_company');
    this.session = window.localStorage.getItem('Sessiontoken');
    this.email = window.localStorage.getItem('user_email');
    this.loginUser = this.jwtService.getAdminUser();
    this.companyId.push(localStorage.getItem('user_company'));
    var params = {
      _id: this.loginUser._id,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._usersService.getById(enc_data).then((dec) => {
      var userData: any = this.encDecService.dwt(this.session, dec.data);
      //console.log(userData);
      this.userData = userData.users[0];
    });
  }
  updateUser() {
    const params = {
      user_id: this.loginUser._id,
      profile_picture: this.profile_picture,
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.updateAclUsers(enc_data).then((dec) => {
      var res: any = this.encDecService.dwt(this.session, dec.data);
      if (dec.status == 200) {
        this.toastr.success('User successfully Updated');
        document.getElementById('img').nodeValue = '';
      }
      else {
        this.toastr.error("Some error has occured");
      }
    })
  }
  imageFinishedUploading(file: FileHolder) {
    this.profile_picture = file.src;
    this.spandata = 'yes';

    // console.log(JSON.stringify(file.serverResponse));
  }
  onUploadStateChanged(state: boolean) {
    console.log(JSON.stringify(state));
  }
  onRemoved(file: FileHolder) {
    console.log('file remove by click cross icon');
    this.profile_picture = null;
    this.spandata = 'no';
    // do some stuff with the removed file.
  }
}
