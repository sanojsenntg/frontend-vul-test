import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { CustomerService } from '../../../common/services/customer/customer.service';
import { MatDialog, MatAutocompleteSelectedEvent } from "@angular/material";
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import * as moment from "moment/moment";
@Component({
  selector: 'app-detail-customer',
  templateUrl: './detail-customer.component.html',
  styleUrls: ['./detail-customer.component.css']
})
export class DetailCustomerComponent implements OnInit {

  public customerId;
  public customerDetails;
  public userOrders;
  public pageNo = 0;
  public pageSize = 10;
  public sortOrder;
  public orderLength;
  public orderTotal;
  public searchSubmit = false;
  public lastDrop;
  public customerDevices: any = '';
  key: string = '';
  public promocodes: any = '';
  public customeRatings;
  statuses: string[] = [];
  public enablePromo = false;
  public companyId: any = [];
  session: string;
  email: string;
  completedPromoTrips:any ='';
  constructor(private router: Router, private route: ActivatedRoute, public _CustomerService: CustomerService, public _aclService: AccessControlService,
    public encDecService: EncDecService,
    private _toasterservice: ToastsManager,
    public jwtService: JwtService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.customerId = route.snapshot.params.id;
  }

  ngOnInit() {
    this.searchSubmit = true;
    var getCust = {
      customer_id: this.customerId,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, getCust);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    console.log(getCust);
    this._CustomerService.getCustomerById(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.customerDetails = data.data[0];
        if (this.customerDetails.customer_devices[0]) {
          this.customerDevices = this.customerDetails.customer_devices;
        }
        if (this.customerDetails.customer_ratings) {
          this.customeRatings = this.customerDetails.customer_ratings.rating.$numberDecimal
        }
        if (this.customerDetails.promocodes) {
          this.promocodes = this.customerDetails.promocodes;
        }if(this.customerDetails.completed_promo_trips){
          this.completedPromoTrips = this.customerDetails.completed_promo_trips;
        }
      }
    })
    const params = {
      offset: '0',
      limit: '10',
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      customer_id: this.customerId,
      company_id: this.companyId
    };
    var encryptedParams = this.encDecService.nwt(this.session, params);
    var enc_data_params = {
      data: encryptedParams,
      email: this.email
    }
    this._CustomerService.getCustomerTrips(enc_data_params).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.userOrders = data.getOrder;
        console.log(this.userOrders);
        this.orderLength = data.count;
        this.orderTotal = data.count;
        if (data.getOrder[0]) {
          this.lastDrop = data.getOrder[0].pickup_location;
        }
      }
      this.searchSubmit = false;
    })
    this.aclDisplayService();
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if(dec && dec.status==200){
      var data: any = this.encDecService.dwt(this.session, dec.data);
      console.log(data)
      for (let i = 0; i < data.menu.length; i++) {
        if (data.menu[i] == "Customer Details - Promocode") {
          this.enablePromo = true;
          return;
        }
      }}
    })
  }
  pagingAgent(data) {
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    var params;
    if (this.pageNo == 0) {
      params = {
        offset: '0',
        limit: '10',
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        customer_id: this.customerId,
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        customer_id: this.customerId,
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._CustomerService.getCustomerTrips(enc_data).then((dec) => {
      if(dec && dec.status==200){
      var data: any = this.encDecService.dwt(this.session, dec.data);
      this.userOrders = data.getOrder;
      this.orderLength = data.count;
      this.orderTotal = data.count;
    }
      this.searchSubmit = false;
    })
  }
  public blockDevice(event){
    if(event.is_blocked == "0"){
      var blocked_status = "1"
    }else{
      var blocked_status = "0"
    }
    var params = {
      customer_id: event.customer_id,
      is_blocked:blocked_status,
      device_token:event.device_token,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._CustomerService.blockCustomer(enc_data).then((data)=>{
      if(data.status == 200){
        this.ngOnInit();
      }else{
      }
    })
  }
  formatDate(date){
    return moment(date).format('dddd MMMM Do YYYY, h:mm:ss a')
  }
}
