import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { FormControl } from '@angular/forms';
import { CustomerService } from '../../../common/services/customer/customer.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-in-app-notification',
  templateUrl: './in-app-notification.component.html',
  styleUrls: ['./in-app-notification.component.css']
})
export class InAppNotificationComponent implements OnInit {
  companyId: any=[];
  session: string;
  customerCtrl=new FormControl;
  customerData: any=[];
  customer_id=''

  constructor(private toast: ToastsManager,private vcr: ViewContainerRef,private _customerService: CustomerService,public encDecService: EncDecService) { 
    this.toast.setRootViewContainerRef(vcr);
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
  }
  

  ngOnInit() {
    this.customerCtrl.valueChanges.debounceTime(500).distinctUntilChanged()
      .switchMap((data) => {
        let params = {
          search: typeof data !== 'object' ? data.trim().substr(0, 1) == '+' ? data.substr(1) : data.trim() : '',
          limit:10,
          company_id: this.companyId
        };
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        return this._customerService.getCustomerByPhone(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.customerData = data.result;
        }
      });
  }
  displayFnCustomerPhone(data): string {
    return data ? data.firstname : data;
  }
  notification(data){
    console.log(data)
  }

}
