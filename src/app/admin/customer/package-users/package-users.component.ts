import { CustomerService } from './../../../common/services/customer/customer.service';
import { PackageService } from './../../../common/services/customer_packages/customer_packages.service';
import { EncDecService } from './../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { Angular2Csv } from 'angular2-csv';
import { ToastsManager } from 'ng2-toastr';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import * as moment from 'moment/moment';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-package-users',
  templateUrl: './package-users.component.html',
  styleUrls: ['./package-users.component.css']
})
export class PackageUsersComponent implements OnInit {
  customerForm: FormControl = new FormControl();
  packageForm: FormControl = new FormControl();
  loadingIndicator;
  public customerFormModel = '';
  public packageFormModel = '';
  pageSize = 10;
  pageNo = 0;
  currentPage = 1;
  public endDate: any;
  public startDate: any;
  start;
  end;
  creatingcsv: any;
  csvInterval: NodeJS.Timer;
  progressvalue: number = 0;
  public loader = false;
  public search;
  public session;
  public companyId:any = [];
  public package_id:any;
  public customer_id:any;
  public packageUsersData;
  public packageUsersCount;
  customerData: any;
  packageListData: any;
  status: any;
  constructor(
    private _packageService: PackageService, 
    private router: Router,
    private EncDecService: EncDecService,
    private Toaster: ToastsManager,
    private CustomerService: CustomerService
    ) {
      this.session = localStorage.getItem('Sessiontoken');
      this.companyId.push(localStorage.getItem('user_company'));
      this.start = ''
      this.end = ''
      this.startDate = ''
      this.endDate =  ''
     }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.getPackageUsers();

    this.customerForm.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchCustomer';
    })
    this.customerForm.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((data) => {
        var params = {
          limit: 10,
          search: typeof data !== 'object' && data ? data.trim().substr(0, 1) == '+' ? data.substr(1) : data.trim() : '',
          company_id: this.companyId.length > 0 ? this.companyId : [this.companyId],
        }
        var encrypted = this.EncDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        return this.CustomerService.getCustomerByPhone(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.EncDecService.dwt(this.session, dec.data);
          this.customerData = result.result;
          this.loadingIndicator = '';
        }
      });
    
    this.packageForm.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchPackage';
    })
    this.packageForm.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((data) => {
        const params = {
          offset: 0,
          limit: 10,
          search:  typeof data !== 'object' ? data.trim() : '',
          sortOrder: 'desc',
          sortByColumn: 'updated_at',
          company_id: this.companyId,
        };
        var encrypted = this.EncDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        return this._packageService.getPackageList(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.EncDecService.dwt(this.session, dec.data);
          this.packageListData = result.packageData;
          this.loadingIndicator = '';
        }
      });
  }

  public getPackageUsers() {
    this.loader = true;
    const params = {
      company_id: this.companyId,
      offset: 0,
      limit: this.pageSize,
      sortOrder:  -1,
      status: this.status ? this.status : [],
      sortByColumn: 'purchased_on',
      package_id: this.package_id ? [this.package_id] : [],
      customer_id: this.customer_id ? [this.customer_id] : [],
      startDate: this.search || this.package_id ? '' : this.startDate,
      endDate: this.search || this.package_id ? '' : this.endDate ,
    };
    var encrypted = this.EncDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    
    this._packageService.packageUsers(enc_data).then((dec) => {
      this.loader = false;
      if (dec && dec.status == 200) {
        var data: any = this.EncDecService.dwt(this.session, dec.data);
        this.packageUsersData = data.data;
        this.packageUsersCount = data.count;
      }else{
        this.Toaster.error(dec.message);
      }
    });
  }

  pagingAgent(data) {
    window.scrollTo(0, 0);
    this.currentPage = data;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    this.loader = true;
    const params = {
      company_id: this.companyId,
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder:  -1,
      sortByColumn: 'purchased_on',
      status: this.status ? this.status : [],
      package_id: this.package_id ? [this.package_id] : [],
      customer_id: this.customer_id ? [this.customer_id] : [],
      startDate: this.customer_id || this.package_id ? '' : this.startDate,
      endDate: this.customer_id || this.package_id ? '' : this.endDate ,
    };
    var encrypted = this.EncDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._packageService.packageUsers(enc_data).then((dec) => {
      this.loader = false;
      if (dec && dec.status == 200) {
        var data: any = this.EncDecService.dwt(this.session, dec.data);
        this.packageUsersData = data.data;
        this.packageUsersCount = data.count;
      }else{
        this.Toaster.error(dec.message);
      }
    });
  }

  dateCalc() {
    this.startDate = this.start ? (moment(this.start).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '';
    this.endDate = this.end ? (moment(this.end).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '';
  }

  displayFnCxPhn(data): string {
    return data ? data.phone_number : data;
  }

  displayFnPkName(data): string {
    return data ? data.package_name : data;
  }

  searchCustomer(data){
    if (typeof data === 'object') {
      this.customer_id = data._id;
    }
  }
  
  searchPackage(data){
    if (typeof data === 'object') {
      this.package_id = data._id;
    }
  }

  reset() {
    this.clearField();
    this.ngOnInit();
  }

  clearField() {
    this.currentPage = 1;
    this.search = '';
    this.package_id = '';
    this.customer_id = '';
    this.customerFormModel = '';
    this.packageFormModel = '';
    this.startDate ='';
    this.endDate =  '';
    this.start = '';
    this.end = '';
    this.status = [];
  }

  goToLink(url: string){
    if(url)
      window.open('/admin/customer/detail/'+url);
  }

  getconstantsforcsv(){
    if (this.creatingcsv) {
      this.Toaster.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      this.createCsv(2000, 100, 0);
    }
  }

  public createCsv(interval_value, offset, resetCount) {
    var count = this.packageUsersCount;
    let res1Array = [];
    let i = 0;
    let fetch_status = true;
    let that = this;
    this.csvInterval = setInterval(function () {
      if (fetch_status) {
        if (res1Array.length >= count) {
          that.progressvalue = 100;
          that.creatingcsv = false;
          clearInterval(that.csvInterval);
          let labels = [
            'Package Id',
            'Package Name',
            'First Name',
            'Last Name',
            'Country Code',
            'Phone Number',
            'Promo Code',
            'Package Status',
            'Auto Renewable',
            'Purchased Date',
            'Updated Date'
          ];

          var options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(res1Array, 'Package-customers' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
          that.progressvalue = 0;
        } else {
          that.creatingcsv = true;
          fetch_status = false;
          let diff = count - i;
          let perc = diff / count;
          let remaining = 100 * perc;
          that.progressvalue = 100 - remaining;
          let new_params;
          new_params = {
            offset: i,
            limit: parseInt(offset),
            sortOrder: -1, 
            sortByColumn: 'purchased_on',
            package_id: that.package_id ? [that.package_id] : [],
            customer_id: that.customer_id ? [that.customer_id] : [],
            status: that.status ? that.status : [],
            startDate: that.customer_id || that.package_id ? '' : that.startDate,
            endDate: that.customer_id || that.package_id ? '' : that.endDate ,
            company_id: that.companyId,
          };
          let enc_data = that.EncDecService.nwt(that.session, new_params);
          let data1 = {
            data: enc_data,
            email: localStorage.getItem('user_email')
          }
          that._packageService.packageUsers(data1).then((res) => {
            if (res && res.status == 200) {
              res = that.EncDecService.dwt(that.session, res.data);
              if (res.data.length > 0) {
                for (let j = 0; j < res.data.length; j++) {
                  const csvArray = res.data[j];
                  res1Array.push({
                    package_id: csvArray.unique_package_id ? csvArray.unique_package_id : 'N/A',
                    package_name: csvArray.package_id && csvArray.package_id.package_name ? csvArray.package_id.package_name : 'N/A',
                    first_name: csvArray.customer_id && csvArray.customer_id.firstname ? csvArray.customer_id.firstname : 'N/A',
                    last_name: csvArray.customer_id && csvArray.customer_id.last_name ? csvArray.customer_id.last_name : 'N/A',
                    country_code: csvArray.customer_id && csvArray.customer_id.country_code ? csvArray.customer_id.country_code : 'N/A',
                    phone_number: csvArray.customer_id && csvArray.customer_id.phone_number ? csvArray.customer_id.phone_number : 'N/A',
                    promo_code: csvArray.created_promo_id && csvArray.created_promo_id.promo_name ? csvArray.created_promo_id.promo_name : 'N/A',
                    package_status: csvArray.status && csvArray.status == '0'  ? 'Active' : csvArray.status == '1' ? 'Expired' : 'Cancelled',
                    auto_renewable: csvArray.is_auto_renewable,
                    purchased_date: csvArray.purchased_on ? moment(csvArray.purchased_on).format('YYYY-MM-DD HH:mm:ss') : 'N/A',
                    updated_at: csvArray.updated_at ? moment(csvArray.updated_at).format('YYYY-MM-DD HH:mm:ss') : 'N/A',
                  });
                  if (j == res.data.length - 1) {
                    i = i + parseInt(offset);
                    fetch_status = true;
                  }
                }
              }
            }
          }).catch((Error) => {
            console.log(Error)
            that.Toaster.warning('Network reset, csv creation restarted');
            resetCount++;
            if (resetCount <= 3)
              that.createCsv(interval_value, offset, resetCount);
            else{
              that.Toaster.error('Error, Csv creation terminated');
              that.creatingcsv = false;
              clearInterval(that.csvInterval);
            }
          })
        }
      }
    }, parseInt(interval_value));
  }

  getColor(status:any){
    return {"0": 'green', '1': 'red', '2': 'blue'}[status]
  }
}

