import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPackagesComponent } from './list-packages/list-packages.component';
import { AddPackagesComponent } from './add-packages/add-packages.component';
import { EditPackagesComponent } from './edit-packages/edit-packages.component';
import { RouterModule, Routes, PreloadAllModules, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { NgDatepickerModule } from 'ng2-datepicker';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MatTooltipModule, MatIconModule, MatChipsModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { PackageService } from '../../../common/services/customer_packages/customer_packages.service';
import { CustomerPackageRestService } from '../../../common/services/customer_packages/cusomter_packages-rest.service';
import { ImageUploadModule } from 'angular2-image-upload';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    MatSelectModule,
    NgDatepickerModule,
    ImageUploadModule.forRoot(),
    OwlDateTimeModule,
    MatTooltipModule, MatIconModule, MatChipsModule,
    MatAutocompleteModule
  ],
  declarations: [ListPackagesComponent, AddPackagesComponent, EditPackagesComponent],
  providers: [PackageService, CustomerPackageRestService]
})
export class CustomerPackagesModule { }
