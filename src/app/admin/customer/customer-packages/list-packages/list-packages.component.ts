import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { WalletServicesService } from '../../../../common/services/wallet/wallet-services.service';
import { JwtService } from '../../../../common/services/api/jwt.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { Router } from '@angular/router';
import { PackageService } from '../../../../common/services/customer_packages/customer_packages.service'
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { environment } from '../../../../../environments/environment';
@Component({
  selector: 'app-list-packages',
  templateUrl: './list-packages.component.html',
  styleUrls: ['./list-packages.component.css']
})
export class ListPackagesComponent implements OnInit {
  public companyId: any = [];
  pageSize = 10;
  public imageUrl;
  pageNo = 0;
  public is_search = false;
  page = 1;
  email: string;
  session: string;
  public packageData = [];
  public packageDatalength = 0;
  public searchSubmit = false;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  constructor(private router: Router,
    public jwtService: JwtService,
    public _packageService: PackageService,
    public overlay: Overlay,
    public _aclService: AccessControlService,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    vcr: ViewContainerRef) {
    this.imageUrl = environment.imgUrl;
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.toastr.setRootViewContainerRef(vcr)
  }
  ngOnInit() {
    this.aclDisplayService();
    this.getPackageListing();
  }
  public getPackageListing() {
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
    }
    this._packageService.getPackageList(enc_data).subscribe((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        //console.log(JSON.stringify(data));
        this.packageData = data.packageData;
        this.packageDatalength = data.count;
      } else {
        this.toastr.error(dec.message);
      }
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        //console.log("Data"+JSON.stringify(data));
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Packages -Add") {
            this.aclAdd = true;
          } else if (data.menu[i] == "Packages -Edit") {
            this.aclEdit = true;
          } else if (data.menu[i] == "Packages -Delete") {
            this.aclDelete = true;
          }
        };
      }
    })
  }
  pagingAgent(data) {
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._packageService.getPackageList(enc_data).subscribe((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.packageData = data.packageData;
        this.packageDatalength = data.count;
      } else if (dec.status != 200) {
        this.toastr.error(dec.message);
      }
    });
  }
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this data' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.deletePackage(id);
      } else {
      }
    });
  }
  public deletePackage(id) {
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._packageService.packageForDeletion(enc_data)
      .subscribe((dec) => {
        if (dec) {
          if (dec.status === 200) {
            this.getPackageListing();
            this.packageData.splice(id, 1);
            this.toastr.success('Package deleted successfully.');
          }
          else if (dec.status === 201) {
            this.toastr.show(dec.message);
          }
        }
      });
  }
}
