import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleModelsService } from '../../../../common/services/vehiclemodels/vehiclemodels.service';
import { PackageService } from '../../../../common/services/customer_packages/customer_packages.service';
import { ToastsManager } from 'ng2-toastr';
import { FileHolder } from 'angular2-image-upload';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
@Component({
  selector: 'app-add-packages',
  templateUrl: './add-packages.component.html',
  styleUrls: ['./add-packages.component.css']
})
export class AddPackagesComponent implements OnInit {

  public companyId: any = [];
  public saveForm: FormGroup;
  public spandata = 'no';
  public spanicondata = 'no';
  public CategoryData;
  public pdata;
  public km_type = false;
  public ride_type = true;
  public vehicleCatData;
  email: string;
  session: string;
  constructor(private _vehicleModelsService: VehicleModelsService,
    private _packageService: PackageService,
    public toastr: ToastsManager,
    private router: Router,
    formBuilder: FormBuilder,
    public encDecService: EncDecService,
    vcr: ViewContainerRef,
    public jwtService: JwtService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.saveForm = formBuilder.group({
      package_name: ['', [
        Validators.required,
      ]],
      package_description: ['', [
        Validators.required,
      ]],
      ride_count: [0, [
        Validators.required,
      ]],
      total_kms: [0, [
        Validators.required,
      ]],
      maximum_discount_amount_upto: '',
      percentage_discount: '',
      vehicle_image: ['', [
        Validators.required,
      ]],
      allowed_categories: ['', [
        Validators.required,
      ]],
      package_cost: [0, [
        Validators.required,
      ]],
      validity: [0, [
        Validators.required,
      ]],
      package_type: ['ride', [
        Validators.required,
      ]],
    });
  }

  ngOnInit() {
    this.getCategories();
  }
  /**
   * Add vehicle model records
   */
  public addPackage() {
    if (!this.saveForm.valid) {
      this.toastr.error('All fields are required');
      return;
    } else {
      if (this.saveForm.value.package_type == "km" && this.saveForm.value.total_kms == 0) {
        this.toastr.error('Please enter kms');
      } else if (this.saveForm.value.package_type == "ride" && this.saveForm.value.ride_count == 0) {
        this.toastr.error('Please specify number of rides');
      } else if (this.saveForm.value.package_type == "ride" && this.saveForm.value.percentage_discount == 0) {
        this.toastr.error('Discount percentage cant be zero');
      } else if (this.saveForm.value.package_cost == 0) {
        this.toastr.error('Please enter a valid cost');
      } else {
        const vehicleModel = {
          package_name: this.saveForm.value.package_name,
          package_description: this.saveForm.value.package_description,
          ride_count: this.saveForm.value.ride_count,
          total_kms: this.saveForm.value.total_kms,
          vehicle_image: this.saveForm.value.vehicle_image,
          allowed_categories: this.saveForm.value.allowed_categories,
          package_cost: this.saveForm.value.package_cost,
          maximum_discount_amount_upto: this.saveForm.value.maximum_discount_amount_upto,
          percentage_discount: this.saveForm.value.percentage_discount,
          validity: this.saveForm.value.validity,
          package_type: this.saveForm.value.package_type,
          company_id: this.companyId
        };
        //console.log("params" + JSON.stringify(vehicleModel));
        var encrypted = this.encDecService.nwt(this.session, vehicleModel);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._packageService.savePackage(enc_data)
          .subscribe((dec) => {
            if (dec) {
              console.log(JSON.stringify(dec));
              if (dec.status == 200) {
                this.toastr.success('Package added successfully.');
                this.router.navigate(['/admin/customer/packages']);
              }
              else if (dec.status == 203) {
                this.toastr.warning('Package already exist.');
              }
              else {
                this.toastr.warning(dec.message);
              }
            }
          })
      }
    }
  }

  imageFinishedUploading(file: FileHolder) {
    this.saveForm.controls['vehicle_image'].setValue(file.src);
    this.spandata = 'yes';

  }
  onRemoved(file: FileHolder) {
    this.saveForm.controls['vehicle_image'].setValue(null);
    this.spandata = 'no';
    // do some stuff with the removed file.
  }
  getCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.CategoryData = data.vehicleModelsList;
      }
    });
  }
  setpackageType(data) {
    if (data == 'km') {
      this.km_type = true;
      this.ride_type = false;
    } else {
      this.km_type = false;
      this.ride_type = true;
    }
  }
}