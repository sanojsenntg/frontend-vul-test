import { Component, OnInit, ViewContainerRef,ViewChild } from '@angular/core';
import { CustomerService } from '../../../common/services/customer/customer.service';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../common/services/api/jwt.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { FormControl } from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete} from '@angular/material';

@Component({
  selector: 'app-push-notification',
  templateUrl: './push-notification.component.html',
  styleUrls: ['./push-notification.component.css']
})

export class PushNotificationComponent implements OnInit {
  statuses: string[] = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[];
  fruitCtrl = new FormControl();
  filteredFruits;
  allFruits: string[] = [];
  pageSize = 10;
  current_order_status = [];
  customers;
  public pushNotification = {
    device_type: '',
    title: '',
    text: ''
  }
  public session;
  public companyId:any = [];
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  constructor(public apiService: CustomerService, private toast: ToastsManager, private router: Router,
    private vcr: ViewContainerRef, public _customerService:CustomerService,
    public encDecService:EncDecService) {
    this.toast.setRootViewContainerRef(vcr);
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
  }

  ngOnInit() {
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: -1,
      sortByColumn: 'created_date',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.fetchCustomer(enc_data).then((dec) => {
      var data:any = this.encDecService.dwt(this.session,dec.data);
      this.filteredFruits = data.customers;
    });
  }
  searchCustomer(event){
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: -1,
      sortByColumn: 'created_date',
      search: this.customers,
      platform:this.pushNotification.device_type,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.fetchCustomer(enc_data).then((dec) => {
      var data:any = this.encDecService.dwt(this.session,dec.data);
      this.filteredFruits = data.customers;
    });
  }
  public notification(event) {
    if(this.pushNotification.text == '' || this.pushNotification.title == ''){
      return;
    }
    var params = {
      device_type: this.pushNotification.device_type,
      text: this.pushNotification.text,
      title: this.pushNotification.title,
      customerIds: this.current_order_status,
      company_id: this.companyId
    }
    console.log(params);
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.apiService.notificationServ(enc_data).then((dec) => {
      if (dec.status == 200) {
        this.toast.success('Push Notifications sent successfully');
        setTimeout(() => {
          this.router.navigate(['/admin/customer'])
        }, 1000);
      } else {
        this.toast.error('Some error has occurred');
      }
    })
  }


  displayFnOrderId(data): string {
    return data ? "" : "";
  }

  remove(fruit: string): void {
    const index = this.statuses.indexOf(fruit);
      if (index >= 0) {
        this.statuses.splice(index, 1);
        this.current_order_status.splice(index, 1);
      }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.statuses.indexOf(event.option.viewValue) > -1) {
      return;
    } else {
      this.statuses.push(event.option.value.firstname);
      this.current_order_status.push(event.option.value._id);
      this.fruitCtrl.setValue(null);
    }
  }

}
