import { Component, OnInit, ViewContainerRef,ViewChild } from '@angular/core';
import { CustomerService } from '../../../common/services/customer/customer.service';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { JwtService } from '../../../common/services/api/jwt.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { FormControl } from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete} from '@angular/material';
import { FileHolder } from 'angular2-image-upload';
import * as moment from 'moment/moment';
import { PromocodeService } from '../../../common/services/promocode/promocode.service';

@Component({
  selector: 'app-app-notification',
  templateUrl: './app-notification.component.html',
  styleUrls: ['./app-notification.component.css']
})

export class AppNotificationComponent implements OnInit {
  statuses: string[] = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[];
  fruitCtrl = new FormControl();
  filteredFruits;
  allFruits: string[] = [];
  pageSize = 10;
  current_order_status = [];
  customers;
  public pushNotification = {
    type:'',
    device_type: '',
    device_version:'',
    customer_spec:'',
    customer_group:'',
    customer_single:'',
    title: '',
    text: '',
    image: '',
    link:'',
    futureCustomers:0,
    notification_expiry:'',
    promocode:''
  }
  public promoId;
  public oSVersion;
  public company_id_list = [];
  public session;
  public spandata;
  public companyId:any = [];
  public submit = false;
  public promo_list;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  constructor(public apiService: CustomerService, private toast: ToastsManager, private router: Router,
    private vcr: ViewContainerRef, public _customerService:CustomerService,
    public encDecService:EncDecService, public _promoCodeService: PromocodeService) {
    this.toast.setRootViewContainerRef(vcr);
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
  }

  ngOnInit() {
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: -1,
      sortByColumn: 'created_date',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.fetchCustomer(enc_data).then((dec) => {
      var data:any = this.encDecService.dwt(this.session,dec.data);
      this.filteredFruits = data.customers;
    });
  }
  searchCustomer(event){
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: -1,
      sortByColumn: 'created_date',
      search: this.customers,
      platform:this.pushNotification.device_type,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.fetchCustomer(enc_data).then((dec) => {
      var data:any = this.encDecService.dwt(this.session,dec.data);
      this.filteredFruits = data.customers;
    });
  }
  searchPromo(event){
    console.log(event);
    const params = {
      offset: 0,
      limit: 5,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      keyword: event,
      promo_type: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._promoCodeService.getPromocodeListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.promo_list = data.getPromocodes;
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  displayFnDevice(data): string {
    return data ? data.promo_code : data;
  }
  promoSelected(event){
    this.promoId = event.option.value._id;
  }
  public notification(event) {
    this.submit = true; 
    if(this.pushNotification.type =='customer'){
      this.pushNotification.customer_spec == 'customer'
    }
    if(this.pushNotification.text == '' || this.pushNotification.title == '' || this.pushNotification.type=='' && (this.pushNotification.device_type =='' || this.pushNotification.customer_spec == '')){
      return;
    }
    var params = {
      type: this.pushNotification.type, 
      device_type:this.pushNotification.device_type ? this.pushNotification.device_type : '',
      os_version:this.company_id_list.length !=0 ? this.company_id_list: [],
      text: this.pushNotification.text,
      title: this.pushNotification.title,
      notification_type: this.current_order_status.length != 0 ? 'selected' : 'all',
      customerIds: this.current_order_status.length != 0 ? this.current_order_status : '',
      image_url:this.pushNotification.image,
      link:this.pushNotification.link,
      futureCustomers:this.current_order_status.length == 0 ?this.pushNotification.futureCustomers:'0',
      notification_expiry:moment(this.pushNotification.notification_expiry).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm"),
      promo_code_id:this.pushNotification.promocode !='' ? this.promoId : '',
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.apiService.appNotification(enc_data).then((dec) => {
      if (dec.status == 200) {
        this.toast.success('App Notifications sent successfully');
        setTimeout(() => {
          this.router.navigate(['/admin/customer'])
        }, 1000);
      } else {
        this.toast.error('Some error has occurred');
      }
    })
  }


  displayFnOrderId(data): string {
    return data ? "" : "";
  }

  remove(fruit: string): void {
    const index = this.statuses.indexOf(fruit);
      if (index >= 0) {
        this.statuses.splice(index, 1);
        this.current_order_status.splice(index, 1);
      }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.statuses.indexOf(event.option.viewValue) > -1) {
      return;
    } else {
      this.statuses.push(event.option.value.firstname);
      this.current_order_status.push(event.option.value._id);
      this.fruitCtrl.setValue(null);
    }
  }

  imageFinishedUploading(file: FileHolder) {
    this.pushNotification.image = file.src;
    this.spandata = 'yes';
  }

  onRemoved(file: FileHolder) {
    this.pushNotification.image = '';
    this.spandata = 'no';
  }
  deviceCustomer(event){
  }
  setCompanyList(){
  }
  osVersion(){
    var params = {
      device_type:this.pushNotification.device_type,
      company_id: this.companyId,
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.apiService.appDeviceVersion(enc_data).then((dec)=>{
      if(dec && dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.oSVersion = data.data;
      }else{
        console.log(dec.message)
      }
    })
  }
}
