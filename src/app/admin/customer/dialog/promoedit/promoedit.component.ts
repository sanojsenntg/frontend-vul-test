import { Component, OnInit, ViewChild, Inject, ViewContainerRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent } from '@angular/material';
import { PromocodeService } from '../../../../common/services/promocode/promocode.service';
import { MatSelectModule } from '@angular/material/select';
import { ToastsManager } from 'ng2-toastr';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment/moment';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Overlay } from '@angular/cdk/overlay';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
@Component({
  selector: 'app-promoedit',
  templateUrl: './promoedit.component.html',
  styleUrls: ['./promoedit.component.css']
})
export class PromoeditComponent implements OnInit {
  public editForm: FormGroup;
  selectedValue: any;
  public max = new Date();
  public PromoAddData;
  public PromosData;
  public driver_id;
  public deviceData;
  private sub: Subscription;
  public searchSubmit = false;
  public selectedMoment = '';
  public selectedMoment1 = '';
  public companyId: any = [];
  email: string;
  session: string;
  constructor(public _promoservice: PromocodeService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public overlay: Overlay,
    public dialogRef: MatDialogRef<PromoeditComponent>,
    private toastr: ToastsManager,
    private vcr: ViewContainerRef,
    public dialog: MatDialog,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    private router: Router, @Inject(MAT_DIALOG_DATA) public data: any) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.toastr.setRootViewContainerRef(vcr);
    this.PromoAddData = data;
    this.editForm = formBuilder.group({
      driver_id: ['']
    });
  }

  ngOnInit() {
    this.selectedMoment = this.getDate(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    this.selectedMoment1 = this.getDate1(moment().add(12, 'hours').format('YYYY-MM-DD HH:mm'));
    this.getPromos();
    this.promoFilter = this.PromoAddData.applicable_promo_codes;
    for (let i = 0; i < this.promoFilter.length; i++) {
      this.promoList[i] = this.promoFilter[i]._id;
    }
    /*this._deviceService.getDevicesById(this.deviceAddData._id).then((datas) => {
      this.driverData = datas.getDevices.driver_id;
      this.editForm.setValue({
        driver_id: datas.getDevices.driver_id ? datas.getDevices.driver_id : '',
      });
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
      console.log(error);
    });*/
    this.driverCtrl.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.companyId
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._promoservice.getindividualPromo(enc_data)
      }).subscribe(dec => {
        var result: any = this.encDecService.dwt(this.session, dec.data);
        if (result.status === 400) { return; } else { this.PromosData = result.promo; }
      })
  }
  
  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }

  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }
  updatePromoForCustomer() {
    if (!this.editForm.valid) {
      this.toastr.error('All fields are required.');
      return;
    }
    else {
      const params = {
        applicable_promo_codes: this.promoList.length > 0 ? this.promoList : [],
        company_id: this.companyId,
        _id: this.PromoAddData._id
      }
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._promoservice.updateCustomerPromo(enc_data)
        .then((dec) => {
          if (dec && dec.status === 200) {
            this.toastr.success('Customer promo updated successfully.');
            setTimeout(() => {
              this.dialogRef.close();
            }, 1000);
          } else {
            this.toastr.error('Promo updation Failed..Please try Again');
            setTimeout(() => {
              this.dialogRef.close();
            }, 1000);
          }
        })
        .catch((error) => {
          setTimeout(() => {
            this.dialogRef.close();
          }, 1000);
          this.toastr.error('Driver updation failed.');
        });
    }
  }

  closePop() {
    this.dialogRef.close();
  }
  public getPromos() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._promoservice.getindividualPromo(enc_data).subscribe(dec => {
      if (dec && dec.status == 200) {
        var data1: any = this.encDecService.dwt(this.session, dec.data);
        this.PromosData = data1.promo;
      }
    });
  }
  driverCtrl: FormControl = new FormControl();
  public promoFilter = [];
  public promoList = [];
  public additional_info;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  promoIdselected(event: MatAutocompleteSelectedEvent): void {
    if (this.promoList.indexOf(event.option.value._id) > -1) {
      return
    }
    else {
      this.promoFilter.push(event.option.value);
      this.promoList.push(event.option.value._id);
    }
  }
  removePromo(fruit): void {
    const index = this.promoFilter.indexOf(fruit);
    if (index >= 0) {
      this.promoFilter.splice(index, 1);
      this.promoList.splice(index, 1)
    }
  }
  displayFnDriverId(data): string {
    return data ? '' : '';
  }

}
