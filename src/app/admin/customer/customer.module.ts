import { ShukranModule } from './shukran/shukran.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCustomerComponent } from './list-customer/list-customer.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { DetailCustomerComponent } from './detail-customer/detail-customer.component';
import { NgDatepickerModule } from 'ng2-datepicker';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule, MatIconModule, MatChipsModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';
import { PushNotificationComponent } from './push-notification/push-notification.component';
import { PromoeditComponent } from './dialog/promoedit/promoedit.component';
import { EmailNotificationComponent } from '../customer/email-notification/email-notification.component'
import { SmsNotificationComponent } from '../customer/sms-notification/sms-notification.component';
import { AppNotificationComponent } from './app-notification/app-notification.component'
import { ImageUploadModule } from 'angular2-image-upload';
import { InAppNotificationComponent } from './in-app-notification/in-app-notification.component';
import { DispatcherCustomerComponent } from './dispatcher-customer/dispatcher-customer.component';
import { EditDispatcherCustomerComponent } from './dialog/edit-dispatcher-customer/edit-dispatcher-customer.component';
import { CustomerPackagesModule } from './customer-packages/customer-packages.module';
import { PackageUsersComponent } from './package-users/package-users.component';

@NgModule({
  imports: [
    ImageUploadModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    CustomerPackagesModule,
    NgbModule,
    Ng2OrderModule,
    FormsModule,
    MatSelectModule,
    NgDatepickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatTooltipModule,
    MatIconModule,
    MatChipsModule,
    MatAutocompleteModule,
    ShukranModule
  ],
  declarations: [ListCustomerComponent, DetailCustomerComponent, PromoeditComponent, PushNotificationComponent, CustomerDashboardComponent, EmailNotificationComponent, SmsNotificationComponent, AppNotificationComponent, InAppNotificationComponent, DispatcherCustomerComponent, EditDispatcherCustomerComponent, PackageUsersComponent],
  entryComponents: [
    PromoeditComponent,
    EditDispatcherCustomerComponent
  ]
})
export class CustomerModule { }
