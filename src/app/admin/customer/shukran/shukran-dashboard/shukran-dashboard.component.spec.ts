import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShukranDashboardComponent } from './shukran-dashboard.component';

describe('ShukranDashboardComponent', () => {
  let component: ShukranDashboardComponent;
  let fixture: ComponentFixture<ShukranDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShukranDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShukranDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
