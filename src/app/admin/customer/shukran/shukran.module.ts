import { ProgressBarModule } from 'angular-progress-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule, MatInput, MatInputModule, MatFormField, MatLabel, MatTooltipModule, MatIconModule, MatChipsModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShukranListComponent } from './shukran-list/shukran-list.component';
import { ShukranTransactionsComponent } from './shukran-transactions/shukran-transactions.component';
import { ShukranDashboardComponent } from './shukran-dashboard/shukran-dashboard.component';
import { NgDatepickerModule } from 'ng2-datepicker';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgDatepickerModule,
    MatTooltipModule,
    MatIconModule,
    MatChipsModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    ProgressBarModule
  ],
  declarations: [ShukranListComponent, ShukranTransactionsComponent, ShukranDashboardComponent]
})
export class ShukranModule { }
