import { Angular2Csv } from 'angular2-csv';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from './../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { ShukranService } from './../../../../common/services/shukran/shukran.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import * as moment from 'moment/moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shukran-transactions',
  templateUrl: './shukran-transactions.component.html',
  styleUrls: ['./shukran-transactions.component.css']
})
export class ShukranTransactionsComponent implements OnInit {
  pageSize = 10;
  pageNo = 0;
  currentPage = 1;
  public endDate: any;
  public startDate: any;
  start;
  end;
  creatingcsv: any;
  csvInterval: NodeJS.Timer;
  progressvalue: number = 0;
  public loader = false;
  public search;
  public session;
  public companyId:any = [];
  public unique_order_id:any;
  public shukranTransactionData;
  public shukranTransactionCount;
  constructor(
    private _shukranService: ShukranService, 
    private router: Router,
    private EncDecService: EncDecService,
    private Toaster: ToastsManager
    ) {
      this.session = localStorage.getItem('Sessiontoken');
      this.companyId.push(localStorage.getItem('user_company'));
      this.start = new Date(Date.now() - (86400000 * 7));
      this.end = new Date(Date.now());
      this.startDate = moment(Date.now() - (86400000 * 7)).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm");
      this.endDate =  moment().tz('Asia/Dubai').format("YYYY-MM-DD HH:mm");
     }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.getShukranTransactions();
  }

  public getShukranTransactions() {
    this.loader = true;
    const params = {
      company_id: this.companyId,
      offset: 0,
      limit: this.pageSize,
      sortOrder:  -1,
      sortByColumn: 'cr_at',
      unique_order_id: this.unique_order_id ? [this.unique_order_id] : [],
      search: this.search ? this.search : '',
      startDate: this.search || this.unique_order_id ? '' : this.startDate,
      endDate: this.search || this.unique_order_id ? '' : this.endDate ,
    };
    var encrypted = this.EncDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    
    this._shukranService.getShukranTransactions(enc_data).then((dec) => {
      this.loader = false;
      if (dec && dec.status == 200) {
        var data: any = this.EncDecService.dwt(this.session, dec.data);
        this.shukranTransactionData = data.data;
        this.shukranTransactionCount = data.count;
      }else{
        this.Toaster.error(dec.message);
      }
    });
  }

  pagingAgent(data) {
    window.scrollTo(0, 0);
    this.currentPage = data;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    this.loader = true;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: -1, 
      sortByColumn: 'cr_at',
      unique_order_id: this.unique_order_id ? [this.unique_order_id] : [],
      search: this.search ? this.search : '',
      startDate: this.search || this.unique_order_id ? '' : this.startDate,
      endDate: this.search || this.unique_order_id ? '' : this.endDate ,
      company_id: this.companyId
    };
    var encrypted = this.EncDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._shukranService.getShukranTransactions(enc_data).then((dec) => {
      this.loader = false;
      if (dec && dec.status == 200) {
        var data: any = this.EncDecService.dwt(this.session, dec.data);
        this.shukranTransactionData = data.data;
        this.shukranTransactionCount = data.count;
      }else{
        this.Toaster.error(dec.message);
      }
    });
  }

  dateCalc() {
    this.startDate = this.start ? (moment(this.start).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '';
    this.endDate = this.end ? (moment(this.end).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '';
  }

  reset() {
    this.clearField();
    this.ngOnInit();
  }

  clearField() {
    this.currentPage = 1;
    this.search = '';
    this.unique_order_id = '';
    this.startDate = moment(Date.now() - (86400000 * 7)).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm");
    this.endDate =  moment().tz('Asia/Dubai').format("YYYY-MM-DD HH:mm");
    this.start = new Date(Date.now() - (86400000 * 7))
    this.end = new Date(Date.now())
  }

  goToLink(url: string){
    if(url)
      window.open('/admin/customer/detail/'+url);
  }

  getconstantsforcsv(){
    if (this.creatingcsv) {
      this.Toaster.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      this.createCsv(2000, 100, 0);
    }
  }

  public createCsv(interval_value, offset, resetCount) {
    var count = this.shukranTransactionCount;
    let res1Array = [];
    let i = 0;
    let fetch_status = true;
    let that = this;
    this.csvInterval = setInterval(function () {
      if (fetch_status) {
        if (res1Array.length >= count) {
          that.progressvalue = 100;
          that.creatingcsv = false;
          clearInterval(that.csvInterval);
          let labels = [
            'Shukran Number',
            'First Name',
            'Last Name',
            'Country Code',
            'Phone Number',
            'Unique Order Id',
            'Price',
            'Shukran Points',
            'Transaction Status',
            'Created Date'
          ];

          var options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(res1Array, 'Shukran-transactions' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
          that.progressvalue = 0;
        } else {
          that.creatingcsv = true;
          fetch_status = false;
          let diff = count - i;
          let perc = diff / count;
          let remaining = 100 * perc;
          that.progressvalue = 100 - remaining;
          let new_params;
          new_params = {
            offset: i,
            limit: parseInt(offset),
            sortOrder: -1, 
            sortByColumn: 'cr_at',
            unique_order_id: this.unique_order_id ? [this.unique_order_id] : [],
            search: this.search ? this.search : '',
            startDate: this.search || this.unique_order_id ? '' : this.startDate,
            endDate: this.search || this.unique_order_id ? '' : this.endDate ,
            company_id: this.companyId
          };
          let enc_data = that.EncDecService.nwt(that.session, new_params);
          let data1 = {
            data: enc_data,
            email: localStorage.getItem('user_email')
          }
          that._shukranService.getShukranTransactions(data1).then((res) => {
            if (res && res.status == 200) {
              res = that.EncDecService.dwt(that.session, res.data);
              if (res.data.length > 0) {
                for (let j = 0; j < res.data.length; j++) {
                  const csvArray = res.data[j];
                  res1Array.push({
                    shukran_number: csvArray.shukran_number ? csvArray.shukran_number : 'N/A',
                    first_name: csvArray.customer_id && csvArray.customer_id.firstname ? csvArray.customer_id.firstname : 'N/A',
                    last_name: csvArray.customer_id && csvArray.customer_id.last_name ? csvArray.customer_id.last_name : 'N/A',
                    country_code: csvArray.customer_id && csvArray.customer_id.country_code ? csvArray.customer_id.country_code : 'N/A',
                    phone_number: csvArray.customer_id && csvArray.customer_id.phone_number ? csvArray.customer_id.phone_number : 'N/A',
                    unique_order_id: csvArray.unique_order_id ? csvArray.unique_order_id : 'N/A',
                    price: csvArray.price ? csvArray.price : 'N/A',
                    shukran_points: csvArray.shukran_points ? csvArray.shukran_points : 'N/A',
                    transaction_status: csvArray.parsed_res && csvArray.parsed_res.initiateTransferResponse && csvArray.parsed_res.initiateTransferResponse.message ? csvArray.parsed_res.initiateTransferResponse.message : 'Some Error Occured',
                    created_at: csvArray.cr_at ? moment(csvArray.cr_at).format('YYYY-MM-DD HH:mm:ss') : 'N/A',
                  });
                  if (j == res.data.length - 1) {
                    i = i + parseInt(offset);
                    fetch_status = true;
                  }
                }
              }
            }
          }).catch((Error) => {
            console.log(Error)
            that.Toaster.warning('Network reset, csv creation restarted');
            resetCount++;
            if (resetCount <= 3)
              that.createCsv(interval_value, offset, resetCount);
            else{
              that.Toaster.error('Error, Csv creation terminated');
              that.creatingcsv = false;
              clearInterval(that.csvInterval);
            }
          })

        }
      }
    }, parseInt(interval_value));
  }
}
