import { Angular2Csv } from 'angular2-csv';
import { CustomerService } from './../../../../common/services/customer/customer.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from './../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { ShukranService } from './../../../../common/services/shukran/shukran.service';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import * as moment from 'moment/moment';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete} from '@angular/material';

@Component({
  selector: 'app-shukran-list',
  templateUrl: './shukran-list.component.html',
  styleUrls: ['./shukran-list.component.css']
})
export class ShukranListComponent implements OnInit {
  pageSize = 10;
  pageNo = 0;
  currentPage = 1;
  public endDate: any;
  public startDate: any;
  start;
  end;
  customers;
  creatingcsv: any;
  csvInterval: NodeJS.Timer;
  progressvalue: number = 0;
  public customerIdsArray: string[] = [];
  public customerIds = [];
  formCtrl = new FormControl();
  fetchCustomerData;
  removable = true;
  public loader = false;
  public session;
  public companyId:any = [];
  public customer_id = '';
  public shukranCustomerData;
  public shukranCustomerCount;
  
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  constructor(
    private _shukranService: ShukranService, 
    private _customerService: CustomerService,
    private router: Router,
    private EncDecService: EncDecService,
    private Toaster: ToastsManager
    ) {
      this.session = localStorage.getItem('Sessiontoken');
      this.companyId.push(localStorage.getItem('user_company'));
      this.start = '';
      this.end = '';
      this.startDate = '';
      this.endDate =  '';
     }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.getShukranCustomers();
  }
  
  public getShukranCustomers() {
    this.loader = true;
    const params = {
      company_id: this.companyId,
      offset: 0,
      limit: this.pageSize,
      sortOrder:  -1,
      sortByColumn: 'created_at',
      customer_id: this.customerIds.length ? this.customerIds : [],
      startDate: this.customerIds.length ? '' : this.startDate,
      endDate: this.customerIds.length ? '' : this.endDate ,
    };
    var encrypted = this.EncDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    
    this._shukranService.getShukranCustomers(enc_data).then((dec) => {
      this.loader = false
      if (dec && dec.status == 200) {
        var data: any = this.EncDecService.dwt(this.session, dec.data);
        this.shukranCustomerData = data.data;
        this.shukranCustomerCount = data.count;
      }else{
        this.Toaster.error(dec.message);
      }
    });
  }

  pagingAgent(data) {
    window.scrollTo(0, 0);
    this.currentPage = data;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    this.loader = true;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      customer_id: this.customerIds.length ? this.customerIds : [],
      sortOrder: -1, 
      sortByColumn: 'created_at',
      startDate: this.customerIds.length ? '' : this.startDate,
      endDate: this.customerIds.length ? '' : this.endDate ,
      company_id: this.companyId
    };
    var encrypted = this.EncDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._shukranService.getShukranCustomers(enc_data).then((dec) => {
      this.loader = false;
      if (dec && dec.status == 200) {
        var data: any = this.EncDecService.dwt(this.session, dec.data);
        this.shukranCustomerData = data.data;
        this.shukranCustomerCount = data.count;
      }else{
        this.Toaster.error(dec.message);
      }
    });
  }

  dateCalc() {
    this.startDate = this.start ? (moment(this.start).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '';
    this.endDate = this.end ? (moment(this.end).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '';
  }

  reset() {
    this.clearField();
    this.ngOnInit();
  }

  clearField() {
    this.currentPage = 1;
    this.customerIds = [];
    this.customerIdsArray = [];
    this.startDate = '';
    this.endDate = '';
    this.start = '';
    this.end = '';
  }

  goToLink(url: string){
    if(url)
      window.open('/admin/customer/detail/'+url);
  }

  remove(customer_id: string): void {
    const index = this.customerIdsArray.length && this.customerIdsArray.indexOf(customer_id);
      if (index >= 0) {
        this.customerIdsArray.splice(index, 1);
        this.customerIds.splice(index, 1);
      }
  }

  searchCustomer(event){
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: -1,
      sortByColumn: 'created_date',
      search: this.customers,
      company_id: this.companyId
    };
    var encrypted = this.EncDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._customerService.fetchCustomer(enc_data).then((dec) => {
      var data:any = this.EncDecService.dwt(this.session,dec.data);
      if (dec && dec.status == 200) {
        var data: any = this.EncDecService.dwt(this.session, dec.data);
        this.fetchCustomerData = data.customers;
      }else{
        this.Toaster.error(dec.message);
      }
    });
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.customerIdsArray.length && this.customerIdsArray.indexOf(event.option.viewValue) > -1) {
      return;
    } else {
      this.customerIdsArray.push(event.option.value.firstname);
      this.customerIds.push(event.option.value._id);
      this.formCtrl.setValue(null);
    }
  }

  getconstantsforcsv(){
    if (this.creatingcsv) {
      this.Toaster.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      this.createCsv(2000, 100, 0);
    }
  }

  public createCsv(interval_value, offset, resetCount) {
    var count = this.shukranCustomerCount;
    let res1Array = [];
    let i = 0;
    let fetch_status = true;
    let that = this;
    this.csvInterval = setInterval(function () {
      if (fetch_status) {
        if (res1Array.length >= count) {
          that.progressvalue = 100;
          that.creatingcsv = false;
          clearInterval(that.csvInterval);
          let labels = [
            'Shukran Number',
            'First Name',
            'Last Name',
            'Country Code',
            'Phone Number',
            'Created Date'
          ];
          var options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(res1Array, 'Shukran-customers' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
          that.progressvalue = 0;
        } else {
          that.creatingcsv = true;
          fetch_status = false;
          let diff = count - i;
          let perc = diff / count;
          let remaining = 100 * perc;
          that.progressvalue = 100 - remaining;
          let new_params;
          new_params = {
            offset: i,
            limit: parseInt(offset),
            customer_id: that.customerIds.length ? that.customerIds : [],
            sortOrder: -1, 
            sortByColumn: 'created_at',
            startDate: that.customerIds.length ? '' : that.startDate,
            endDate: that.customerIds.length ? '' : that.endDate ,
            company_id: that.companyId
          };
          let enc_data = that.EncDecService.nwt(that.session, new_params);
          let data1 = {
            data: enc_data,
            email: localStorage.getItem('user_email')
          }
          that._shukranService.getShukranCustomers(data1).then((res) => {
            if (res && res.status == 200) {
              res = that.EncDecService.dwt(that.session, res.data);
              if (res.data.length > 0) {
                for (let j = 0; j < res.data.length; j++) {
                  const csvArray = res.data[j];
                  
                  res1Array.push({
                    shukran_number: csvArray.shukran_number ? csvArray.shukran_number : 'N/A',
                    first_name: csvArray.first_name ? csvArray.first_name : 'N/A',
                    last_name: csvArray.last_name ? csvArray.last_name : 'N/A',
                    country_code: csvArray.country_code ? csvArray.country_code : 'N/A',
                    phone_number: csvArray.phone_number ? csvArray.phone_number : 'N/A',
                    created_at: csvArray.created_at ? moment(csvArray.created_at).format('YYYY-MM-DD HH:mm:ss') : 'N/A',
                  });
                  if (j == res.data.length - 1) {
                    i = i + parseInt(offset);
                    fetch_status = true;
                  }
                }
              }
            }
          }).catch((Error) => {
            console.log(Error)
            that.Toaster.warning('Network reset, csv creation restarted');
            resetCount++;
            if (resetCount <= 3)
              that.createCsv(interval_value, offset, resetCount);
            else{
              that.Toaster.error('Error, Csv creation terminated');
              that.creatingcsv = false;
              clearInterval(that.csvInterval);
            }
          })

        }
      }
    }, parseInt(interval_value));
  }
}
