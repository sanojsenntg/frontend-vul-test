import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShukranListComponent } from './shukran-list.component';

describe('ShukranListComponent', () => {
  let component: ShukranListComponent;
  let fixture: ComponentFixture<ShukranListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShukranListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShukranListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
