import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../../../common/services/customer/customer.service';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { GlobalService } from '../../../common/services/global/global.service';
import { EditDispatcherCustomerComponent } from '../dialog/edit-dispatcher-customer/edit-dispatcher-customer.component';
import * as moment from "moment/moment";
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-dispatcher-customer',
  templateUrl: './dispatcher-customer.component.html',
  styleUrls: ['./dispatcher-customer.component.css']
})
export class DispatcherCustomerComponent implements OnInit {
  pageSize = 10;
  public searchSubmit = false;
  public customerData;
  public customerLength;
  pageNo = 0;
  public is_search = false;
  public companyId: any = [];
  sortOrder = 'asc';
  public aclBtn = true;
  public name;
  public phone;
  public startDate: any;
  public endDate: any;
  public sortBy = '';
  public max = new Date();
  session: string;
  email2: any;

  constructor(private router: Router,
    public dialog: MatDialog,
    private _customerService: CustomerService,
    public _aclService: AccessControlService,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    private _global: GlobalService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email2 = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.startDate = new Date(Date.now() - 2592000000);
    this.endDate = new Date(Date.now());
    this.aclDisplayService();
    let sData = this._global.dispatcher_customers;
    if (sData) {
      this.pageSize = sData.limit;
      this.sortBy = sData.sortOrder;
      this.name = sData.search;
      this.startDate = sData.startDate ? new Date(sData.startDate) : new Date(Date.now() - 2592000000);
      this.endDate = sData.endDate ? new Date(sData.endDate) : new Date(Date.now());
    }
  }

  ngOnInit() {
    this.getCustomers();
  }

  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email2
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      var data: any = this.encDecService.dwt(this.session, dec.data);
      for (let i = 0; i < data.menu.length; i++) {
        if (data.menu[i] == "Dispatcher-Edit Customer")
          this.aclBtn = false;
      }
    })

  }
  /**
    * Pagination for customer module
    * @param data 
    */
  pagingAgent(data) {
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.sortBy == '1' ? 1 : -1,
      search: this.name ? this.name : '',
      startDate: this.startDate ? (moment(this.startDate).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '',
      endDate: this.endDate ? (moment(this.endDate).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '',
      company_id: this.companyId,
      phone:this.phone,
      firstname: this.name
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email2
    }
    this.customerData=[]
    this._customerService.dispatcherCustomers(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.customerData = data.data;
        this.customerLength = data.count;
      }
      this.searchSubmit = false;
    });
  }
  clearEndDate(){
    this.endDate='';
  }
  public getCustomers() {
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: this.sortBy == '1' ? 1 : -1,
      sortByColumn: 'created_at',
      search: this.name ? this.name : '',
      startDate: this.startDate ? (moment(this.startDate).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '',
      endDate: this.endDate ? (moment(this.endDate).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '',
      company_id: this.companyId,
      phone:this.phone,
      firstname: this.name
    };
    this._global.dispatcher_customers = params;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email2
    }
    this.customerData=[];
    this.customerLength=0;
    console.log(params)
    console.log(enc_data)
    this._customerService.dispatcherCustomers(enc_data).then((dec2) => {
      if (dec2 && dec2.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec2.data);
        this.customerLength = data.count;
        this.customerData = data.data;
      }
      this.searchSubmit = false;
    });
  }

  /**
   * To refresh customer records showing in the grid
   */
  refresh() {
    if (this.is_search == true) {
      this.getCustomers();
      this.clearField();
    }
    else {
      this.clearField();
      this.ngOnInit();
    }
  }

  clearField() {
    this.phone = null;
    this.name = null;
    this.sortBy = '';
    this.startDate = new Date(Date.now() - 86400000);
    this.endDate = new Date(Date.now());
  }
  /**
  * To reset the search filters and reload the customer records
  */
  reset() {
    this.clearField();
    this.ngOnInit();
  }

  customerDetails(data) {
    this.router.navigate(['/admin/customer/detail', data._id]);
  }
  editCustomer(data){
    let dialogRef = this.dialog.open(EditDispatcherCustomerComponent, {
      width: '500px',
      hasBackdrop: true,
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getCustomers();
    });
  }
}

