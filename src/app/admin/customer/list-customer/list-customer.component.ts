import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../../../common/services/customer/customer.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from '../../../common/dialog/delete-dialog/delete-dialog.component';
import { PromoeditComponent } from '../dialog/promoedit/promoedit.component';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from './../../../common/services/access-control/access-control.service';
import * as moment from 'moment/moment';
import { JwtService } from '../../../common/services/api/jwt.service';
import { GlobalService } from '../../../common/services/global/global.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-list-customer',
  templateUrl: './list-customer.component.html',
  styleUrls: ['./list-customer.component.css']
})
export class ListCustomerComponent implements OnInit {
  pageSize = 10;
  del = false;
  public searchSubmit = false;
  public customerData;
  public customerLength;
  key: string = '';
  reverse: boolean = false;
  searchValue: string = '';
  pageNo = 0;
  public changeStatus;
  public is_blocked: any = "";
  totalPage = [1];
  public is_search = false;
  pages = 8;
  public _id;
  public paymentData;
  public payment_extra;
  searchDataPayments = [];
  public companyId: any = [];
  paymentLength;
  page = 1;
  sortOrder = 'asc';
  public keyword;
  public aclBtn = true;
  public name;
  public email;
  public phone;
  public startDate: any = '';
  public endDate: any = '';
  public startDt;
  public endDt
  public Platform = '';
  public userGender = '';
  public sortBy = '';
  public startDob: any = '';
  public endDob: any = '';
  public is_temporary = '';
  public max = new Date();
  session: string;
  email2: any;
  aPPColumn: boolean = false;

  constructor(private router: Router,
    private _customerService: CustomerService,
    public dialog: MatDialog,
    public overlay: Overlay,
    private _toasterservice: ToastsManager,
    vcr: ViewContainerRef,
    public _aclService: AccessControlService,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    private _global: GlobalService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email2 = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.startDate = new Date(Date.now() - 86400000);
    this.endDate = new Date(Date.now());
    this.aclDisplayService();
    //let sData = sessionStorage.getItem('customer');
    let sData = this._global.customer;
    if (sData) {
      this.pageSize = sData.limit;
      this.sortBy = sData.sortOrder;
      this.is_blocked = sData.is_blocked;
      this.name = sData.search;
      this.startDate = sData.startDate ? new Date(sData.startDate) : new Date(Date.now() - 86400000);
      this.endDate = sData.endDate ? new Date(sData.endDate) : new Date(Date.now());
      this.startDt = sData.startDate ? new Date(sData.startDate) : '';
      this.endDt = sData.endDate ? new Date(sData.endDate) : '';
      this.startDob = sData.start_dob ? new Date(sData.start_dob) : '';
      this.endDob = sData.end_dob ? new Date(sData.end_dob) : '';
      this.userGender = sData.gender;
      this.Platform = sData.platform;
      this.is_temporary = sData.is_temporary;
    }
  }

  ngOnInit() {
    this.startDt = '';
    this.endDt = '';
    this.startDate = '';
    this.endDate = '';
    this.getCustomers();
  }

  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email2
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      var data: any = this.encDecService.dwt(this.session, dec.data);
      for (let i = 0; i < data.menu.length; i++) {
        if (data.menu[i] == "Customers - Edit Blocked Status")
          this.aclBtn = false;
        if (data.menu[i] == "Customer List - Applicable Promocode")
          this.aPPColumn = true;
      }
    })

  }
  dateCalc() {
    this.startDt = this.startDate ? (moment(this.startDate).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '';
    this.endDt = this.endDate ? (moment(this.endDate).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : '';
    this.getCustomers();
  }
  /**
    * Pagination for customer module
    * @param data 
    */
  pagingAgent(data) {
    window.scrollTo(0, 200);
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.sortBy == '1' ? 1 : -1,
      is_blocked: this.is_blocked ? this.is_blocked : null,
      search: this.name ? this.name : '',
      startDate: this.name ?'':this.startDt ? this.startDt : null,
      endDate: this.name ?'':this.endDt ? this.endDt : null,
      start_dob: this.name ?'':this.startDob != '' ? (moment(this.startDob).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : null,
      end_dob: this.name ?'':this.endDob != '' ? (moment(this.endDob).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : null,
      gender: this.userGender != '' ? this.userGender : null,
      platform: this.Platform != '' ? this.Platform : null,
      is_temporary: this.is_temporary != '' ? this.is_temporary : '0',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email2
    }
    this._customerService.fetchCustomer(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.customerData = data.customers;
      }
      this.searchSubmit = false;
    });
    this._customerService.fetchCustomerCount(enc_data).then((dec2) => {
      if (dec2 && dec2.status == 200) {
        var data2: any = this.encDecService.dwt(this.session, dec2.data);
        this.customerLength = data2.count;
      }
    });
  }

  public searchName() {
    console.log(this.name);
  }
  /**
   * To change the blocked status of the customer
   * @param customer 
   * @param index 
   */
  ChangeBlockStatus(customer, index) {
    if (customer.is_blocked == '1') {
      this.changeStatus = '0';
      const params = {
        is_blocked: this.changeStatus,
        _id: customer._id,
        company_id: this.companyId
      }
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email2
      }
      this._customerService.changeBlockStatus(enc_data).then((dec) => {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        if (data) {
          this.customerData[index].is_blocked = data.updatedBlockedStatus.is_blocked;
        }
      });
    } else if (customer.is_blocked == '0') {
      this.changeStatus = '1';
      const params = {
        is_blocked: this.changeStatus,
        _id: customer._id,
        company_id: this.companyId
      }
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email2
      }
      this._customerService.changeBlockStatus(enc_data).then((dec) => {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.customerData[index].is_blocked = data.updatedBlockedStatus.is_blocked;
      });

    }
  }

  /**
   * To search the particular customer
   */


  public getCustomers() {
    if(this.searchSubmit)
    return;
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: this.sortBy == '1' ? 1 : -1,
      sortByColumn: 'created_date',
      is_blocked: this.is_blocked ? this.is_blocked : '',
      search: this.name ? this.name : '',
      startDate: this.name ?'':this.startDt ? this.startDt : '',
      endDate: this.name ?'':this.endDt ? this.endDt : '',
      start_dob: this.name ?'':this.startDob != '' ? (moment(this.startDob).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : null,
      end_dob: this.name ?'':this.endDob != '' ? (moment(this.endDob).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm")) : null,
      gender: this.userGender != '' ? this.userGender : '',
      platform: this.Platform != '' ? this.Platform : '',
      is_temporary: this.is_temporary != '' ? this.is_temporary : '0',
      company_id: this.companyId
    };
    //sessionStorage.setItem('customer', JSON.stringify(params));
    this._global.customer = params;
    this.del = true;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email2
    }
    this._customerService.fetchCustomerCount(enc_data).then((dec2) => {
      if (dec2 && dec2.status == 200) {
        var data2: any = this.encDecService.dwt(this.session, dec2.data);
        this.customerLength = data2.count;
      }
    });
    this._customerService.fetchCustomer(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.customerData = data.customers;
      }
      this.searchSubmit = false;
      this.del = false;
      this.is_search = false;
    });
  }

  /**
   * To refresh customer records showing in the grid
   */
  refresh() {
    if (this.is_search == true) {
      this.getCustomers();
      this.clearField();
    }
    else {
      this.clearField();
      this.ngOnInit();
    }
  }

  clearField() {
    this.phone = null;
    this.name = null;
    this.email = null;
    this.startDt = null;
    this.endDt = null;
    this.Platform = '';
    this.sortBy = '';
    this.userGender = '';
    this.is_blocked = '';
    this.startDate = new Date(Date.now() - 86400000);
    this.endDate = new Date(Date.now());
    this.startDob = '';
    this.endDob = '';
    this.is_temporary = '';
    //sessionStorage.removeItem('customer');
  }
  /**
  * To reset the search filters and reload the customer records
  */
  reset() {
    this.del = true;
    this.clearField();
    this.ngOnInit();
  }

  customerDetails(data) {
    this.router.navigate(['/admin/customer/detail', data._id]);
    console.log(data._id);
  }
  UpdateApplicablePromos(customer, index) {
    //alert("customer");
    let dialogRef = this.dialog.open(PromoeditComponent, {
      width: '800px',
      hasBackdrop: true,
      data: customer
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getCustomers();
    });
  }
  UpdateDisabledPromos(customer, index) {

  }
}
