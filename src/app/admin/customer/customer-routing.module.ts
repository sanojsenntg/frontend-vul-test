import { PackageUsersComponent } from './package-users/package-users.component';
import { shukranRoutes } from './shukran/shukran-routing.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { ListCustomerComponent } from './list-customer/list-customer.component';
import { AclAuthervice } from '../../common/services/access-control/acl-auth.service';
import { DetailCustomerComponent } from './detail-customer/detail-customer.component';
import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';
import { PushNotificationComponent } from './push-notification/push-notification.component';
import { SmsNotificationComponent } from './sms-notification/sms-notification.component';
import { EmailNotificationComponent } from './email-notification/email-notification.component';
import { AppNotificationComponent } from './app-notification/app-notification.component';
import { InAppNotificationComponent } from './in-app-notification/in-app-notification.component';
import { DispatcherCustomerComponent } from './dispatcher-customer/dispatcher-customer.component'
import { PackagesRoutes } from '../customer/customer-packages/customer-packages-routing.module'

export const customerRoutes: Routes = [
  { path: 'list', component: ListCustomerComponent, canActivate: [AclAuthervice], data: { roles: ["Customers - List"] } },
  { path: 'detail/:id', component: DetailCustomerComponent, canActivate: [AclAuthervice], data: { roles: ["Customers - List"] } },
  { path: '', component: CustomerDashboardComponent, canActivate: [AclAuthervice], data: { roles: ["Customers - List"] } },
  { path: 'push-notification', component: PushNotificationComponent, canActivate: [AclAuthervice], data: { roles: ["Push-Notification"] } },
  { path: 'sms-notification', component: SmsNotificationComponent, canActivate: [AclAuthervice], data: { roles: ["Email-Notification"] } },
  { path: 'email-notification', component: EmailNotificationComponent, canActivate: [AclAuthervice], data: { roles: ["SMS-Notification"] } },
  { path: 'app-notification', component: AppNotificationComponent, canActivate: [AclAuthervice], data: { roles: ["SMS-Notification"] } },
  { path: 'in-app-notification', component: InAppNotificationComponent, canActivate: [AclAuthervice], data: { roles: ["In-App-Notification"] } },
  { path: 'dispatcher-customers', component: DispatcherCustomerComponent, canActivate: [AclAuthervice], data: { roles: ["Dispatcher-customers - List"] } },
  { path: 'packages', children: PackagesRoutes, canActivate: [AclAuthervice], data: { roles: ["Packages"] } },
  { path: 'package-users', component: PackageUsersComponent, canActivate: [AclAuthervice], data: { roles: ["Package-users"] } },
  { path: 'shukran', children: shukranRoutes, canActivate: [AclAuthervice], data: { roles: ["Shukran"] } },
];
