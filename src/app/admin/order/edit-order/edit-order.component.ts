import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { OrderService } from '../../../common/services/order/order.service';
import { PartnerService } from '../../../common/services/partners/partner.service';
import { TariffService } from '../../../common/services/tariff/tariff.service';
import { PaymentService } from '../../../common/services/payment/payment.service';
import { PaymentTypeService } from '../../../common/services/paymenttype/paymenttype.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../common/services/companies/companies.service';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit {
  public editForm: FormGroup;
  private sub: Subscription;
  public companyId: any = [];
  public companyData;
  public _id;
  public partnerData;
  public tariffData;
  public payment_type_data;
  session: string;
  email: string;
  public dtc = false;
  public super=false;


  constructor(private _orderService: OrderService,
    private _partnerService: PartnerService,
    private _paymentService: PaymentService,
    private _tariffService: TariffService,
    public encDecService: EncDecService,
    private _paymentTypeService: PaymentTypeService,
    private _companyservice: CompanyService,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public jwtService: JwtService,
    public toastr: ToastsManager,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    public _aclService: AccessControlService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.editForm = formBuilder.group({
      user_id: [''],
      pickup_location: [''],
      drop_location: [''],
      distance: [''],
      start_time: [''],
      end_time: [''],
      driver_id: [''],
      partner_id: [''],
      payment_type_id: [''],
      payment_extra_id: [''],
      vehicle_id: [''],
      tariff_id: [''],
      additional_service_id: [''],
      price: ['', [Validators.required]],
      company_id: ['']
    });

  }
  public ngOnInit(): void {
    this.editForm.controls['user_id'].disable();
    this.editForm.controls['driver_id'].disable();
    this.editForm.controls['vehicle_id'].disable();
    this.editForm.controls['pickup_location'].disable();
    this.editForm.controls['drop_location'].disable();
    this.editForm.controls['distance'].disable();
    this.editForm.controls['start_time'].disable();
    this.editForm.controls['end_time'].disable();
    this.editForm.controls['partner_id'].disable();
    this.editForm.controls['tariff_id'].disable();
    this.editForm.controls['payment_type_id'].disable();
    this.getPartner();
    this.getTariff();
    this.getPaymentType();
    this.getCompanies();
    const company_id: any = localStorage.getItem('user_company');
    if (company_id === '5ce12918aca1bb08d73ca25d' || company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    } else {
      this.editForm.controls['company_id'].disable();
    }
    let params1 = {
      company_id: this.companyId
    }
    let enc_data2 = this.encDecService.nwt(this.session, params1);
    let data1 = {
      data: enc_data2,
      email: this.email
    }
    this._aclService.getAclUserMenu(data1).then((data) => {
      if (data && data.status == 200) {
        data = this.encDecService.dwt(this.session, data.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Edit-order-company-select") {
            //this.super = true;
          }
        }
      }
    });
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getOrderById(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        //console.log(JSON.stringify(res));
        this.editForm.setValue({
          user_id: res.response.SingleDetail.user_id ? res.response.SingleDetail.user_id.firstname : '',
          pickup_location: res.response.SingleDetail.pickup_location ? res.response.SingleDetail.pickup_location : '',
          drop_location: res.response.SingleDetail.drop_location ? res.response.SingleDetail.drop_location : '',
          distance: res.response.SingleDetail.distance ? res.response.SingleDetail.distance : '',
          driver_id: res.response.SingleDetail.driver_id ? res.response.SingleDetail.driver_id.username : '',
          start_time: res.response.SingleDetail.start_time ? res.response.SingleDetail.start_time : '',
          end_time: res.response.SingleDetail.end_time ? res.response.SingleDetail.end_time : '',
          partner_id: res.response.SingleDetail.partner_id ? res.response.SingleDetail.partner_id._id : '',
          tariff_id: res.response.SingleDetail.tariff_id ? res.response.SingleDetail.tariff_id._id : '',
          company_id: res.response.SingleDetail.company_id ? res.response.SingleDetail.company_id._id : '',
          payment_type_id: res.response.SingleDetail.sync_trip_id.payment_type ? (res.response.SingleDetail.sync_trip_id.payment_type === "1" ? "CASH" : res.SingleDetail.sync_trip_id.payment_type === "2" ? "CREDIT" : res.SingleDetail.sync_trip_id.payment_type === "4" ? "ONLINE" : "ACCOUNT") : ' ',
          additional_service_id: res.response.SingleDetail.additional_service_id ? res.response.SingleDetail.additional_service_id._id : '',
          vehicle_id: res.response.SingleDetail.vehicle_id ? res.response.SingleDetail.vehicle_id.display_name : '',
          payment_extra_id: res.response.SingleDetail.payment_extra_id ? res.response.SingleDetail.payment_extra_id : '',
          price: res.response.SingleDetail.price ? res.response.SingleDetail.price : ''
        });
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });
  }

  /**
   * To update the order records
   */
  public user_id;
  public updateOrder() {
    if (!this.editForm.valid) {
      // this._toasterService.error('All fields are required.');
      return;
    }
    var user = JSON.parse(window.localStorage['adminUser']);
    this.user_id = user.email;
    const orderData = {
      user_id: this.editForm.value.user_id,
      pickup_location: this.editForm.value.pickup_location,
      drop_location: this.editForm.value.drop_location,
      distance: this.editForm.value.distance,
      driver_id: this.editForm.value.driver_id,
      start_time: this.editForm.value.start_time,
      end_time: this.editForm.value.end_time,
      partner_id: this.editForm.value.partner_id,
      tariff_id: this.editForm.value.tariff_id ? this.editForm.value.tariff_id : null,
      additional_service_id: this.editForm.value.additional_service_id ? this.editForm.value.additional_service_id : null,
      vehicle_id: this.editForm.value.vehicle_id,
      payment_extra_id: this.editForm.value.payment_extra_id,
      price: this.editForm.value.price,
      payment_type_id: this.editForm.value.payment_type_id ? this.editForm.value.payment_type_id : null,
      edited_by_userid: this.user_id,
      company_id: this.editForm.value.company_id ? this.editForm.value.company_id : localStorage.getItem('user_company'),
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, orderData);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.updateOrder(enc_data)
      .then((dec) => {
        if (dec) {
          if (dec.status === 200) {
            this._toasterService.success('Order updated successfully.');
            this.router.navigate(['/admin/orders']);
          }
          else if (dec.status === 201) {
            this._toasterService.error('Order updation failed.');
          }
          else if (dec.status === 202) {
            this._toasterService.error('Order updation failed.Shift Already closed');
          }
          else if (dec.status === 203) {
            this._toasterService.error(dec.message);
          }
        }
      })
  }
  /**
   * Get Partner for drop down
   *
   */
  public getPartner() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._partnerService.getPartner(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.partnerData = data.getPartners;
      }
    });
  }
  /**
   * Get Tariff for drop down
   *
   */
  public getTariff() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._tariffService.getTariffs(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var Tariffdata: any = this.encDecService.dwt(this.session, dec.data);
        this.tariffData = Tariffdata.getTariff;
      }
    });
  }
  /**
    * Get payment_type for drop down
    *
    */
  public getPaymentType() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'asc',
      sortByColumn: 'payment_type',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var paymentData: any = this.encDecService.dwt(this.session, dec.data);
        this.payment_type_data = paymentData.paymentTypes;
      }
    });
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
          //    console.log("companies++++++++++++++"+JSON.stringify(this.companyData));
        }
      }
    });
  }
}

