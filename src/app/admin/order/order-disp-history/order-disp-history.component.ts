import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../../../common/services/order/order.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as moment from 'moment/moment';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { ToastsManager } from 'ng2-toastr';
import { FormControl } from '@angular/forms';
import { MatDialog, MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-order-disp-history',
  templateUrl: './order-disp-history.component.html',
  styleUrls: ['./order-disp-history.component.css']
})
export class OrderDispHistoryComponent implements OnInit {
  public loadingIndicator;
  public orderData;
  public searchLoader = false;
  private sub: Subscription;
  public current_order_status = 'completed';
  public orderLength;
  public searchSubmit = false;
  public unique_order_id = [];
  pageSize = 10;
  pageNo = 0;
  public selectedMoment;
  public selectedMoment1;
  public generated_by = 'customer';
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  public ordersFilter = [];
  public max = new Date();
  public companyId: any = [];
  orderCtrl: FormControl = new FormControl();
  session: string;
  email: string;
  constructor(private _orderService: OrderService, public toastr: ToastsManager, public jwtService: JwtService, private router: Router, public encDecService: EncDecService) {

    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }
  ngOnInit() {
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
    this.orderCtrl.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          search_keyword: query,
          company_id: this.companyId
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._orderService.getAllOrderId(enc_data)
          .then(dec => {
            if (dec && dec.status == 200) {
              var result: any = this.encDecService.dwt(this.session, dec.data);
              this.allOrderIddata = result.getOrders;
            }
            this.loadingIndicator = '';
          });
      });
    this.getOrders();
  }
  public allOrderIddata;
  getOrderId() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getAllOrderId(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.allOrderIddata = data.getOrders;
      }
    });
  }
  checkShiftstartTime() {
    this.selectedMoment1 = '';
  }

  /**
   *  end time timepicker
   */
  checkShiftstartTime1() {

    if (this.selectedMoment1 != '') {
      // this.selectedMoment="";
    }
  }
  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }
  removeOrder(fruit): void {
    const index = this.ordersFilter.indexOf(fruit);
    if (index >= 0) {
      this.ordersFilter.splice(index, 1);
      this.unique_order_id.splice(index, 1)
    }
  }
  displayFnOrderId(data): string {
    return data ? '' : '';
  }
  orderIdselected(event: MatAutocompleteSelectedEvent): void {
    if (this.ordersFilter.indexOf(event.option.value.unique_order_id) > -1) {
      return
    }
    else {
      this.ordersFilter.push(event.option.value.unique_order_id);
      this.unique_order_id.push(event.option.value.unique_order_id);
      this.orderCtrl.setValue(null);
    }
  }
  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }
  public getOrders() {
    this.searchLoader = true;
    this.orderData = [];
    this.pageNo = 1;
    let params;
    if (this.unique_order_id && this.unique_order_id.length > 0) {
      params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        start_date: '',
        end_date: '',
        // userid: this.user_id ? this.user_id : '',
        current_order_status: '',
        generate_by: this.generated_by ? this.generated_by : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
        end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
        // userid: this.user_id ? this.user_id : '',
        current_order_status: this.current_order_status ? this.current_order_status : '',
        generate_by: this.generated_by ? this.generated_by : '',
        company_id: this.companyId
      };
    }
    //this.payment_type_id = this.payment_type_id.id;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getOrderDispHistory(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        // this.searchSubmit = true;
        //console.log("All order data" + JSON.stringify(data.getAllOrder));
        this.orderData = data.getAllOrder;
        this.orderLength = data.count;
      }
      this.searchLoader = false;
      //this.searchSubmit = false;
    });
  }
  public colorFilter(status) {
    this.generated_by = status;
    this.getOrders();
  }
  public statusColor(data) {
    if (this.generated_by == data) data = "rgb(191, 0, 0)";
    else data = "#211c47";
    return data;
  }
  pagingAgent(data) {
    this.searchLoader = true;
    this.orderData = [];
    //this.orderData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      keyword: this.unique_order_id ? this.unique_order_id : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      // userid: this.user_id ? this.user_id : '',
      current_order_status: this.current_order_status ? this.current_order_status : '',
      generate_by: this.generated_by ? this.generated_by : '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    //this.payment_type_id = this.payment_type_id.id;
    this._orderService.getOrderDispHistory(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        // this.searchSubmit = true;
        this.searchLoader = false;
        //console.log("All order data" + JSON.stringify(data.getAllOrder));
        this.orderData = data.getAllOrder;
        this.orderLength = data.count;
      }
      this.searchSubmit = false;
    });
  }
  reset() {
    this.pageNo = 1;
    this.unique_order_id = [];
    this.ordersFilter = [];
    this.generated_by = "customer";
    this.ngOnInit();
    this.current_order_status = 'completed',
      this.searchSubmit = false;
  }
  public creatingcsv = false;
  public getconstantsforcsv() {
    if (this.creatingcsv) {
      this.toastr.warning('Export Operation already in progress.');
    } else {
      this.searchLoader = true;
      this.creatingcsv = true;
      var params = { company_id: this.companyId };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._orderService.getRejectedCSVConstants(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var res: any = this.encDecService.dwt(this.session, dec.data);
            let offset = res.data;
            this.createCsv(offset.order_fetch_interval, offset.order_fetch_offset);
          } else {
            this.creatingcsv = false
            this.toastr.error("Please try again")
          }
        }
      })
    }
  }
  public livetrackingOrder;
  public progressvalue = 0;
  public createCsv(interval_value, offset) {
    let params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      keyword: this.unique_order_id ? this.unique_order_id : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      // userid: this.user_id ? this.user_id : '',
      current_order_status: this.current_order_status ? this.current_order_status : '',
      generate_by: this.generated_by ? this.generated_by : '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getRejectOrderCount(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          var count = res.count;
          let resArray = [];
          let res1Array = [];
          //alert("total_orders" + count);
          let i = 0;
          let fetch_status = true;
          let that = this;
          this.livetrackingOrder = setInterval(function () {
            if (fetch_status) {
              if (res1Array.length >= count) {
                that.searchLoader = false;
                that.creatingcsv = false;
                clearInterval(that.livetrackingOrder);
                let labels = [
                  'ID',
                  'Customer Name',
                  'Customer Phone',
                  'Source',
                  'Start',
                  'Destination',
                  'Stop',
                  'Distance',
                  'Driver',
                  'Vehicle',
                  'Order-type',
                  'Status',
                  'Total',
                  'Rejected Drivers'
                ];
                var options =
                {
                  fieldSeparator: ',',
                  quoteStrings: '"',
                  decimalseparator: '.',
                  showLabels: true,
                  showTitle: false,
                  useBom: true,
                  headers: (labels)
                };
                new Angular2Csv(res1Array, 'Rejected List-Order' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
              } else {
                fetch_status = false;
                let new_params = {
                  offset: i,
                  limit: parseInt(offset),
                  sortOrder: 'desc',
                  sortByColumn: 'updated_at',
                  keyword: that.unique_order_id ? that.unique_order_id : '',
                  start_date: that.selectedMoment ? moment(that.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
                  end_date: that.selectedMoment1 ? moment(that.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
                  // userid: this.user_id ? this.user_id : '',
                  current_order_status: that.current_order_status ? that.current_order_status : '',
                  generate_by: that.generated_by ? that.generated_by : '',
                  company_id: that.companyId
                };
                var encrypted = that.encDecService.nwt(that.session, new_params);
                var enc_data = {
                  data: encrypted,
                  email: that.email
                }
                that._orderService.getOrderDispHistoryCsv(enc_data).then((dec) => {
                  if (dec && dec.status == 200) {
                    var res: any = that.encDecService.dwt(that.session, dec.data);
                    if (res.getAllOrder.length > 0) {
                      for (let j = 0; j < res.getAllOrder.length; j++) {
                        let drivers = '';
                        const csvArray = res.getAllOrder[j];
                        if (csvArray.ExtraDetail.length > 0) {
                          for (var w = 0; w < csvArray.ExtraDetail.length; ++w) {
                            if (drivers.includes(csvArray.ExtraDetail[w].driver_id.username)) {
                            } else {
                              if (csvArray.ExtraDetail[w].order_accept_reject == '2') {
                                drivers = drivers + csvArray.ExtraDetail[w].driver_id.username + ' , ' + drivers + csvArray.ExtraDetail[w].driver_id.phone_number + ' , ' + ' Trip Rejected, ' + csvArray.ExtraDetail[w].order_reject_reason + ' , ' + csvArray.ExtraDetail[w].created_at + '\n';
                              } else if (csvArray.ExtraDetail[w].order_accept_reject == '3') {
                                drivers = drivers + csvArray.ExtraDetail[w].driver_id.username + ' , ' + drivers + csvArray.ExtraDetail[w].driver_id.phone_number + ' , ' + ' Trip Rejected (After accepting), ' + csvArray.ExtraDetail[w].order_reject_reason + ' , ' + csvArray.ExtraDetail[w].created_at + '\n';
                              }
                              else if (csvArray.ExtraDetail[w].order_accept_reject == '4') {
                                drivers = drivers + csvArray.ExtraDetail[w].driver_id.username + ' , ' + drivers + csvArray.ExtraDetail[w].driver_id.phone_number + ' , ' + ' Trip Rejected (Waiting for customer), ' + csvArray.ExtraDetail[w].order_reject_reason + ' , ' + csvArray.ExtraDetail[w].created_at + '\n';
                              }
                            }
                          }
                          res1Array.push({
                            id: csvArray.unique_order_id ? csvArray.unique_order_id : 'N/A',
                            customer_name: csvArray.user_id && csvArray.user_id.firstname ? csvArray.user_id.firstname : 'Offline Customer',
                            customer_phone: csvArray.user_id && csvArray.user_id.phone_number ? csvArray.user_id.phone_number : 'Offline Customer',
                            source: csvArray.pickup_location ? csvArray.pickup_location : 'N/A',
                            start: csvArray.sync_trip_id != null ? csvArray.sync_trip_id.start_trip_calendar ? csvArray.sync_trip_id.start_trip_calendar : 'NA' : 'NA',
                            destination: csvArray.drop_location ? csvArray.drop_location : 'N/A',
                            Stop: csvArray.sync_trip_id != null ? csvArray.sync_trip_id.finished_trip_calendar ? csvArray.sync_trip_id.finished_trip_calendar : 'NA' : 'NA',
                            distance: csvArray.sync_trip_id ? csvArray.sync_trip_id.total_distance_travelled : csvArray.distance,
                            driver: csvArray.driver_id ? csvArray.driver_id.username : 'N/A',
                            vehicle: csvArray.vehicle_id ? csvArray.vehicle_id.vehicle_identity_number : 'N/A ',
                            order_type: csvArray.generate_by ? csvArray.generate_by : 'N/A',
                            status: csvArray.order_status ? csvArray.order_status : 'N/A',
                            total: csvArray.sync_trip_id ? csvArray.sync_trip_id.total_cost : '0 AED',
                            rejected_drivers: drivers ? drivers : 'NA'
                          });
                        }
                        if (j == res.getAllOrder.length - 1) {
                          i = i + parseInt(offset);
                          fetch_status = true;
                        }
                      }
                    }
                  }
                }).catch((Error) => {
                  that.toastr.warning('Network reset, csv creation restarted')
                  that.createCsv(interval_value, offset);
                })
              }
            }
          }, parseInt(interval_value));
        }
        else {
          this.creatingcsv = false;
          this.toastr.error('Please try again after some time');
        }
      }
    })
  }
}
