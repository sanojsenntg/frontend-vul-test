import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../../../common/services/order/order.service';
import { TariffService } from '../../../common/services/tariff/tariff.service';
import { DriverService } from '../../../common/services/driver/driver.service';
import { ShiftsService } from '../../../common/services/shifts/shifts.service';
import { SyncTripsService } from '../../../common/services/syncTrips/sync-trips.service';
import { SyncTripsRestService } from '../../../common/services/syncTrips/sync-tripsrest.service';
import { DeviceService } from '../../../common/services/devices/devices.service';
import { VehicleService } from '../../../common/services/vehicles/vehicle.service';
import { ZoneService } from '../../../common/services/zones/zone.service';
import { AdditionalService } from '../../../common/services/additional_service/additional_service.service';
import { CustomerService } from '../../../common/services/customer/customer.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as moment from 'moment/moment';
import { PartnerService } from '../../../common/services/partners/partner.service';
import { ShiftsDetailsService } from '../../../common/services/shift_details/shift_details.service';
import { PaymentTypeService } from '../../../common/services/paymenttype/paymenttype.service';
import { PaymentService } from '../../../common/services/payment/payment.service';
import { environment } from '../../../../environments/environment';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { ToastsManager } from 'ng2-toastr';
import { FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatDialog } from '@angular/material';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { VehicleModelsService } from '../../../common/services/vehiclemodels/vehiclemodels.service';
import { TransactionLogComponent } from '../../../common/dialog/transaction-log/transaction-log.component';
import { DeleteDialogComponent } from '../../../common/dialog/delete-dialog/delete-dialog.component';
import { CompanyService } from '../../../common/services/companies/companies.service';
declare var jsPDF;
declare var $;
import * as html2canvas from "html2canvas";
import { Overlay } from '@angular/cdk/overlay';
import { GlobalService } from '../../../common/services/global/global.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-list-order',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.css'],
  providers: [SyncTripsService, SyncTripsRestService]
})

export class ListOrderComponent implements OnInit {
  queryField: FormControl = new FormControl();
  device: FormControl = new FormControl();
  driver_Id: FormControl = new FormControl();
  vehicle_side_no: FormControl = new FormControl();
  vehicle_plate_no: FormControl = new FormControl();
  user: FormControl = new FormControl();
  user_phone: FormControl = new FormControl();
  guestPhone: FormControl = new FormControl();
  searchReasonCtrl = new FormControl();
  userRating = new FormControl();
  customerRating = new FormControl();

  company: FormControl = new FormControl();
  public companyId: any = [];
  public company_id = '';
  public companyData;
  public loadingIndicator;
  public amtSplitupArray;
  public amtSplitupValue: any = [];
  public orderData;
  private sub: Subscription;
  public tariffData;
  public driverData;
  public driverGroupData;
  public paymentData;
  public dropDownData: any = [];
  public imageurl;
  public customerData;
  public guestCustomerData;
  public deviceData;
  public vehichleSideData;
  public vehichlePlateData;
  public vehichle_plate_no: any;
  public vehichle_side_no: any;
  public uniquedriverid: any;
  public driverIdData;
  public customersData;
  public shiftData;
  public serviceGroupData;
  public syncData;
  public orderLengths;
  public is_search = false;
  public orderRecords;
  public orders;
  public partner_id: any;
  public pickup_location: any = '';
  public drop_location: any = '';
  public payment_type_id: any = '';
  public bill_printed = '';
  public current_order_status = '';
  public device_id: any;
  public payment_extra_id: any;
  public driver_groups_id: any = [];
  public sync_trip_id = '';
  public partnersData;
  public paymentTypeData;
  public tariff_id: any;
  public shift_details_id = '';
  public driver_id: any;
  public additional_service_id: any;
  public _id = '';
  public unique_order_id: any = [];
  public user_id: any;
  public guest_user_id:any;
  public guest_username;
  public selectedMoment: any = '';
  public selectedMoment1: any = '';
  public searchSubmit = false;
  public order_detail_data = {
    dispatcher_assigned_count: '-1',
    dispatcher_assigned_amount: '',
    offline_order_count: '',
    offline_order_amount: '',
    customer_order_count: '',
    customer_order_amount: '',
    website_order_count: '',
    website_order_amount: '',
    kiosk_order_count: '',
    kiosk_order_amount: '',
    total_distance: '',
    bill_printed_count: '',
    bill_printed_amount: '',
    bill_notprinted_count: '',
    bill_notprinted_amount: '',
    cash_total: '',
    credit_total: '',
    subtotal: '',
    base_price: '',
    vat_price: '',
    account_total: '',
    card_total: '',
  };
  public payment_extra;
  public amt_splitup;
  public zones;
  public transaction_status = '';
  public order_edited;
  public flagged_orders;
  pageSize = 10;
  pageNo = 0;
  orderLength;
  public length = '';
  del = false;
  public aclCheck = false;
  public downloads = false;
  public revenueLog = false;
  public transaction_log = false;
  public max = new Date();
  public pNo = 1;
  public vehicleTypeFilterOrder;
  public driverForm = '';
  public deviceForm = '';
  public sideForm = '';
  public plateForm = '';
  public imageext;
  public apiUrl;
  public promo_id = '';
  public user_name = '';
  session: string;
  email: string;
  dtc: boolean = false;
  vehicleModels: any;
  companyIdFilter: any;
  aaaCompany: boolean;
  companyType: any;
  min: any = '';
  public startPrice;
  public endPrice;
  constructor(
    public _aclService: AccessControlService,
    private _orderService: OrderService,
    private _devicesService: DeviceService,
    private _additional_Service: AdditionalService,
    public overlay: Overlay,
    private _shiftService: ShiftsDetailsService,
    private _zoneService: ZoneService,
    private _paymentService: PaymentService,
    private _shiftsService: ShiftsService,
    private route: ActivatedRoute,
    private _tariffService: TariffService,
    private _driverService: DriverService,
    private _vehichleService: VehicleService,
    private _customerService: CustomerService,
    private _syncTripService: SyncTripsService,
    private router: Router,
    private _paymentTypeService: PaymentTypeService,
    private _partnerService: PartnerService,
    public toastr: ToastsManager,
    public jwtService: JwtService,
    private _vehicleModelsService: VehicleModelsService,
    vcr: ViewContainerRef,
    public dialog: MatDialog,
    private _global: GlobalService,
    public encDecService: EncDecService,
    private _companyservice: CompanyService
  ) {
    this.company_id = localStorage.getItem('user_company');
    if (this.company_id === '5ce12918aca1bb08d73ca25d' || this.company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    else
      this.min = new Date(moment(Date.now()).subtract(90, 'days').format('YYYY-MM-DD, hh:mm'));
    if (this.company_id === '5dcba9e0b0e06e5c5ac1aea1')
      this.aaaCompany = true;
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(this.company_id);
    this.toastr.setRootViewContainerRef(vcr);
    this.aclDisplayService();
    //let oData=sessionStorage.getItem('order-management');
    let oData = this._global.order_management;
    this._global.observableCSV.subscribe(item => {
      this.creatingcsv = this._global.csv;
    });
    this._global.observableCSVProgress.subscribe(item => {
      this.progressvalue = this._global.csvProgress;
    });
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm'));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    this.current_order_status = 'completed';
    if (oData) {
      this.pageSize = oData.limit;
      this.driver_id = oData.driver_id;
      this.driverForm = oData.driver;
      this.uniquedriverid = oData.unique_driver_id;
      this.vehichle_side_no = oData.vehichle_side_no;
      this.sideForm = oData.side;
      this.vehichle_plate_no = oData.vehichle_plate_no;
      this.plateForm = oData.plate;
      this.unique_order_id = oData.keyword;
      this.selectedMoment = oData.start_date ? new Date(oData.start_date) : this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm'));
      this.selectedMoment1 = oData.end_date ? new Date(oData.end_date) : this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm'));
      this.partner_id = oData.partner_id;
      this.tariff_id = oData.tariff_id;
      //this.payment_type_id=oData.payment_type_id;
      this.driver_groups_id = oData.driver_groups_id;
      this.payment_extra_id = oData.payment_extra_id;
      this.additional_service_id = oData.additional_service_id;
      this.device_id = oData.device_id;
      this.deviceForm = oData.device;
      this.shift_details_id = oData.shift_details_id;
      this._id = oData._id;
      // setTimeout(()=>{
      //   console.log(oData.pickup_location)
      //   this.pickup_location=oData.pickup_location?[oData.pickup_location]:'';
      //   this.drop_location=oData.drop_location;
      // },5000)
      this.user_id = oData.user_id;
      this.user_name = oData.user_name;
      this.sync_trip_id = oData.sync_trip_id;
      this.bill_printed = oData.bill_printed;
      this.current_order_status = oData.current_order_status;
      this.vehicleTypeFilterOrder = oData.model_id;
      this.promo_id = oData.promo_id;
      this.generated_by = oData.generate_by;
      this.transaction_status = oData.transaction_status;
      this.order_edited = oData.order_edited;
      this.flagged_orders = oData.flagged_orders;
      if (this.unique_order_id != '')
        this.unique_order_id.forEach(element => {
          this.ordersFilter.push(element)
        });
      this.cancelReasonId = oData.cancel_reason_id;
      oData.cancelReasons.forEach(element => {
        this.cancelReasons.push(element)
      })
    }
  }
  /*order id multi tag begin*/
  userList: any[] = [{ 'text': '1 Star', 'rating': '1' }, { 'text': '2 Star', 'rating': '2' }, { 'text': '3 Star', 'rating': '3' }, { 'text': '4 Star', 'rating': '4' }, { 'text': '5 Star', 'rating': '5' }];
  custList: any[] = [{ 'text': '1 Star', 'rating': '1.0' }, { 'text': '2 Star', 'rating': '2.0' }, { 'text': '3 Star', 'rating': '3.0' }, { 'text': '4 Star', 'rating': '4.0' }, { 'text': '5 Star', 'rating': '5.0' }];
  userRate;
  CustomerRate;
  searchReason: any = [];
  cancelReasons: any = [];
  cancelReasonId: any = [];
  vehicleCategory: any = [];
  orderComments: any = [];
  vehicleCateg: any = [];
  statuses: string[] = [];

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  orderCtrl: FormControl = new FormControl();
  ordersFilter = [
  ];
  public allOrderIddata;
  getOrderId() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getAllOrderId(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var result: any = this.encDecService.dwt(this.session, dec.data);
        this.allOrderIddata = result.getOrders
      }
    });
    //this.allOrderIddata=[100041116,100041169,100041168,250493,112334];

  }
  /*add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.ordersFilter.push({id: value.trim()});
      this.unique_order_id.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }*/
  orderIdselected(event: MatAutocompleteSelectedEvent): void {
    if (this.ordersFilter.indexOf(event.option.value.unique_order_id) > -1) {
      return
    }
    else {
      this.ordersFilter.push(event.option.value.unique_order_id);
      this.unique_order_id.push(event.option.value.unique_order_id);
      this.orderCtrl.setValue(null);
    }
  }
  removeOrder(fruit): void {
    const index = this.ordersFilter.indexOf(fruit);
    if (index >= 0) {
      this.ordersFilter.splice(index, 1);
      this.unique_order_id.splice(index, 1)
    }
  }
  displayFnOrderId(data): string {
    return data ? '' : '';
  }


  /* order id multi tag end */
  ngOnInit() {
    this.imageext = window.localStorage['ImageExt'];
    this.companyType = JSON.parse(window.localStorage['companydata']).company_type ? JSON.parse(window.localStorage['companydata']).company_type : '3';
    this.imageurl = environment.imgUrl;
    this.apiUrl = environment.apiUrl;
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['unique_shift_id'];
      if (params['order_id']) {
        this.unique_order_id.push(params['order_id']);
        this.ordersFilter.push(params['order_id']);
      }
    });
    //this.selectedMoment = moment(new Date).add({days:-30}).format('YYYY-MM-DD HH:mm');
    //this.selectedMoment1 = moment(new Date()).format('YYYY-MM-DD HH:mm');


    //this.selectedMoment.setValue([moment(new Date).add({days:-30}).format('YYYY-MM-DD HH:mm'), moment(new Date).add({days:-30}).format('YYYY-MM-DD HH:mm')]);
    //this.selectedMoment
    //console.log('selectedMoment1', this.selectedMoment1);
    //console.log('selectedMoment:::', this.selectedMoment);
    this.is_search = false;
    //this.getCustomersWithImages();
    this.getSyncTrips();
    this.getZones();
    this.getDispatcher();
    this.getDriver();
    // //this.getVehichleData();
    this.getAllVehicleModels();
    this.getServices();
    this.getPaymentExtra();
    this.getDriverGroups();
    this.getPaymentType();
    this.getPartners();
    this.getTariff();
    this.getOrderId();
    this.getVehicleCategory();
    this.onChanges();
    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriver';
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.driverData = result.driver;
        }
        this.loadingIndicator = '';
      });
    this.getCompanies()
    // this.company.valueChanges
    //   .debounceTime(500)
    //   .subscribe((query) => {
    //     var params = {
    //       offset: 0,
    //       limit: 10,
    //       sortOrder: 'asc',
    //       sortByColumn: 'unique_company_id',
    //       keyword: query,
    //       company_id: this.companyId
    //     }
    //     var encrypted = this.encDecService.nwt(this.session, params);
    //     var enc_data = {
    //       data: encrypted,
    //       email: this.email
    //     }
    //     this._companyservice.getCompanyListing(enc_data).then(dec => {
    //       if (dec && dec.status == 200) {
    //         var result: any = this.encDecService.dwt(this.session, dec.data);
    //         this.companyData = result.getCompanies;
    //       }
    //       this.loadingIndicator = '';
    //     });
    //   })
    this.device.valueChanges.subscribe(event => {
      this.loadingIndicator = 'searchDevice';
    })
    this.device.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'asc',
          sortByColumn: 'unique_device_id',
          search_keyword: query,
          search: null,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._devicesService.getDevicesListing(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.deviceData = result.devices;
        }
        this.loadingIndicator = ''
      });
    this.driver_Id.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        let params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.driverIdData = result.driver
        }
      });
    this.vehicle_side_no.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchVehichleSidenumber';
    })
    this.vehicle_side_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          'searchsidenumber': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._vehichleService.getVehicleListingAdmin(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.vehichleSideData = result.result;
        }
        this.loadingIndicator = '';
      });
    this.vehicle_plate_no.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchVehichlePlateNumber';
    })
    this.vehicle_plate_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          'searchplate': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._vehichleService.getVehicleListingAdmin(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.vehichlePlateData = result.result;
        }
        this.loadingIndicator = '';
      });
    this.user.valueChanges.subscribe(event => {
      this.loadingIndicator = 'searchCustomer';
    })
    this.user.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'updated_at',
          'search': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._customerService.getCustomerByNameFrOrder(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          result = result.result;
          this.customerData = result;
        }
        this.loadingIndicator = '';
      });
    this.orderCtrl.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchOrder';
    })
    this.orderCtrl.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          search_keyword: query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._orderService.getAllOrderId(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.allOrderIddata = result.getOrders;
        }
        this.loadingIndicator = '';
      });
    this.user_phone.valueChanges.subscribe(event => {
      this.loadingIndicator = 'searchCustomer2';
    })
    this.user_phone.valueChanges
      .debounceTime(500).distinctUntilChanged().switchMap(data => {
        console.log(data);
        
        var params = {
          limit: 10,
          search: typeof data !== 'object' ? data.trim().substr(0, 1) == '+' ? data.substr(1) : data.trim() : '',
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._customerService.getCustomerByPhone(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.customerData = result.result;
        }
        this.loadingIndicator = '';
      });
    this.guestPhone.valueChanges.debounceTime(500).distinctUntilChanged().switchMap(data => {
      console.log(data);
      
        var params = {
          limit: 10,
          search: typeof data !== 'object' ? data.trim().substr(0, 1) == '+' ? data.substr(1) : data.trim() : '',
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        console.log(params);
        
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._customerService.getGuestPhone(enc_data);
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.guestCustomerData = result.result;
        }
        this.loadingIndicator = '';
      });
  }


  remove(status: string, type): void {
    if (type == '1') {
      const index = this.cancelReasons.indexOf(status);
      if (index >= 0) {
        this.cancelReasons.splice(index, 1);
        this.cancelReasonId.splice(index, 1);
      }
    }
    // else if (type == '2') {
    //   const index = this.vehicleCategItems.indexOf(status);
    //   if (index >= 0) {
    //     this.vehicleCategItems.splice(index, 1);
    //     this.vehicleCategId.splice(index, 1);
    //   }
    // }
  }
  selected(event: MatAutocompleteSelectedEvent, type): void {
    if (type == '1') {
      if (this.statuses.indexOf(event.option.value) > -1) {
        return;
      } else {
        this.cancelReasons.push(event.option.value);
        this.cancelReasonId.push(event.option.value._id);
        this.searchReasonCtrl.setValue(null);
      }
    }
    // else if (type == '2') {
    //   if (this.statuses.indexOf(event.option.viewValue) > -1) {
    //     return;
    //   } else {
    //     this.vehicleCategItems.push(event.option.value.reason);
    //     this.vehicleCategId.push(event.option.value._id);
    //     this.vehicleCategCtrl.setValue(null);
    //   }
    // }

  }
  onChanges(): void {
    this.customerRating.valueChanges.subscribe(val => {
      this.CustomerRate = val;
    });
    this.userRating.valueChanges.subscribe(val => {
      this.userRate = val;
    });
  }
  searchReasons(event) {
    var params = {
      offset: '0',
      limit: '5',
      search: event,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getCancelStatus(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        this.searchReason = res.data;
      }
    })
  }

  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }

  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }
  public creatingcsv = false;
  public getconstantsforcsv() {
    if (this.creatingcsv) {
      this.toastr.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      var params = {
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._orderService.getCSVConstants(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          let offset = res.data;
          this.createCsv(offset.order_fetch_interval, offset.order_fetch_offset);
        } else {
          this.creatingcsv = false;
          this._global.setCsvStatus(false, 0);
          this.toastr.error("Please try again")
        }
      })
    }
  }
  /**
   * This function is used to create CSV of orders
   * @param message
   * @param action
   */
  public livetrackingOrder;
  public progressvalue = 0;
  public k = 1;
  public createCsv(interval_value, offset) {
    // if (this.payment_type_id != undefined && this.payment_type_id != '') {
    //   this.payment_type_id = this.payment_type_id.id;
    // }
    let drop_location: any = '';
    let pickup_location: any = '';
    if (this.pickup_location.length > 0) {
      pickup_location = [];
      this.pickup_location.forEach(element => {
        pickup_location.push([element.loc.coordinates[0]])
      });
    }
    if (this.drop_location.length > 0) {
      drop_location = [];
      this.drop_location.forEach(element => {
        drop_location.push([element.loc.coordinates[0]])
      });
    }
    const params1 = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      tariff_id: this.tariff_id ? this.tariff_id._id : '',
      driver_id: this.driver_id ? [this.driver_id] : '',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      keyword: this.unique_order_id ? this.unique_order_id : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      // userid: this.user_id ? this.user_id : '',
      partner_id: this.partner_id ? this.partner_id._id : '',
      payment_type_id: this.payment_type_id ? this.payment_type_id.id : '',
      driver_groups_id: '',
      payment_extra_id: this.payment_extra_id ? this.payment_extra_id._id : '',
      additional_service_id: this.additional_service_id ? this.additional_service_id._id : '',
      device_id: this.device_id ? this.device_id : '',
      shift_details_id: this.shift_details_id ? this.shift_details_id : '',
      _id: this._id ? this._id : '',
      pickup_location: pickup_location,
      drop_location: drop_location,
      user_id: this.user_id ? this.user_id : '',
      sync_trip_id: this.sync_trip_id ? this.sync_trip_id : '',
      bill_printed: this.bill_printed ? this.bill_printed : '',
      current_order_status: this.current_order_status ? this.current_order_status : '',
      vehicle_model_ids: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder : '',
      promo_id: this.promo_id ? this.promo_id : '',
      generate_by: this.generated_by ? this.generated_by : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      cancellation_reason_id: this.cancelReasonId ? this.cancelReasonId : '',
      transaction_status: this.transaction_status ? this.transaction_status : '',
      vehicle_category_ids: this.vehicleCategory ? this.vehicleCategory : '',
      order_comment_status: this.orderComments ? this.orderComments : '',
      order_edited: this.order_edited ? this.order_edited : '',
      flagged_orders: this.flagged_orders ? this.flagged_orders : '',
      start_price: this.startPrice ? this.startPrice : '',
      end_price: this.endPrice ? this.endPrice : '',
    };
    if (this.driver_groups_id.length > 0 && !params1.driver_id) {//gets the list of drivers specific to one more driver group
      this.getDriversInDriverGroup(this.driver_groups_id.map(x => x._id)).then((res) => {
        params1.driver_id = res;
        this.getCsvCount(params1, interval_value, offset)
      });
    }
    else {
      this.getCsvCount(params1, interval_value, offset)
    }
  }
  getCsvCount(params1, interval_value, offset) {
    var encrypted1 = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted1,
      email: this.email
    }
    this._orderService.getOrderCount(enc_data).then((dec) => {
      var res: any = this.encDecService.dwt(this.session, dec.data);
      if (dec) {
        if (dec.status == 201) {
          this.creatingcsv = false;
          this._global.setCsvStatus(false, 0);
          this.toastr.error('Please try again after some time');
        } else if (dec.status == 200) {
          var count = res.count;
          let res1Array = [];
          //alert("total_orders" + count);
          let i = 0;
          let fetch_status = true;
          let that = this;
          let drop_location: any = '';
          let pickup_location: any = '';
          if (that.pickup_location.length > 0) {
            pickup_location = [];
            that.pickup_location.forEach(element => {
              pickup_location.push([element.loc.coordinates[0]])
            });
          }
          if (that.drop_location.length > 0) {
            drop_location = [];
            that.drop_location.forEach(element => {
              drop_location.push([element.loc.coordinates[0]])
            });
          }
          let new_params = {
            offset: 0,
            limit: 0,
            sortOrder: 'desc',
            sortByColumn: 'updated_at',
            tariff_id: that.tariff_id ? that.tariff_id._id : '',
            driver_id: that.driver_id ? [that.driver_id] : '',
            unique_driver_id: that.uniquedriverid ? that.uniquedriverid : '',
            vehichle_side_no: that.vehichle_side_no ? that.vehichle_side_no : '',
            company_id: that.companyId.length > 0 ? that.companyId : [that.company_id],
            vehichle_plate_no: that.vehichle_plate_no ? that.vehichle_plate_no : '',
            keyword: that.unique_order_id ? that.unique_order_id : '',
            start_date: that.selectedMoment ? moment(that.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
            end_date: that.selectedMoment1 ? moment(that.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
            // userid: this.user_id ? this.user_id : '',
            partner_id: that.partner_id ? that.partner_id._id : '',
            payment_type_id: that.payment_type_id ? that.payment_type_id.id : '',
            driver_groups_id: '',
            payment_extra_id: that.payment_extra_id ? that.payment_extra_id._id : '',
            additional_service_id: that.additional_service_id ? that.additional_service_id._id : '',
            device_id: that.device_id ? that.device_id : '',
            shift_details_id: that.shift_details_id ? that.shift_details_id : '',
            _id: that._id ? that._id : '',
            pickup_location: pickup_location,
            drop_location: drop_location,
            user_id: that.user_id ? that.user_id : '',
            sync_trip_id: that.sync_trip_id ? that.sync_trip_id : '',
            bill_printed: that.bill_printed ? that.bill_printed : '',
            current_order_status: that.current_order_status ? that.current_order_status : '',
            vehicle_model_ids: that.vehicleTypeFilterOrder ? that.vehicleTypeFilterOrder : '',
            promo_id: that.promo_id ? that.promo_id : '',
            generate_by: that.generated_by ? that.generated_by : '',
            cancellation_reason_id: that.cancelReasonId ? that.cancelReasonId : '',
            transaction_status: that.transaction_status ? that.transaction_status : '',
            vehicle_category_ids: that.vehicleCategory ? that.vehicleCategory : '',
            order_comment_status: that.orderComments ? that.orderComments : '',
            order_edited: that.order_edited ? that.order_edited : '',
            flagged_orders: that.flagged_orders ? that.flagged_orders : '',
            start_price: this.startPrice ? this.startPrice : '',
            end_price: this.endPrice ? this.endPrice : '',
          };
          if (that.driver_groups_id.length > 0 && !new_params.driver_id) {//gets the list of drivers specific to one more driver group
            that.getDriversInDriverGroup(that.driver_groups_id.map(x => x._id)).then((res) => {
              new_params.driver_id = res;
              that.beginCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value, 0)
            });
          }
          else {
            that.beginCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value, 0)
          }
        }
      }
    })
  }
  beginCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value, reset) {
    that.livetrackingOrder = setInterval(function () {
      if (fetch_status) {
        if (res1Array.length >= count) {
          that.progressvalue = 100;
          that.creatingcsv = false;
          that.progressvalue = 0;
          that._global.setCsvStatus(false, 0);
          clearInterval(that.livetrackingOrder);
          let labels = [
            'ID',
            'Company',
            'Customer Name',
            'Customer Phone',
            'Source',
            'Start',
            'Destination',
            'Stop',
            'Distance',
            'Driver',
            'Vehicle',
            'Vehicle-model',
            'Customer Given Rating',
            'Driver Given Rating',
            //'Feedback',
            'Partner',
            'Additional Service',
            'Order-type',
            'Payment Type',
            'Status',
            'Tariff',
            'Kilometer Rate(AED)',
            'Distance Amount(AED)',
            'Discount(AED)',
            'Extra Toll (AED)',
            'Extra Service (AED)',
            'Slow Speed (AED)',
            'Base (AED)',
            'Vat (AED)',
            'Adjustment (AED)',
            'Total (AED)',
            'Bill Printed'
          ];
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(res1Array, 'List-Order' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        } else {
          fetch_status = false;
          let diff = count - i;
          let perc = diff / count;
          let remaining = 100 * perc;
          that.progressvalue = 100 - remaining;
          that._global.setCsvStatus(true, that.progressvalue)
          new_params.offset = i;
          new_params.limit = parseInt(offset);
          var encrypted2 = that.encDecService.nwt(that.session, new_params);
          var enc_data2 = {
            data: encrypted2,
            email: that.email
          }
          let flag = false;
          let k = i;
          that._orderService.searchOrderCSV(enc_data2).then((dec) => {
            if (dec && dec.status == 200) {
              var res: any = that.encDecService.dwt(that.session, dec.data);
              if (res.searchOrder.length > 0) {
                for (let j = 0; j < res.searchOrder.length; j++) {
                  const csvArray = res.searchOrder[j];
                  let adjustment = '0';
                  let discount = '0'
                  if (csvArray.sync_trip_id && csvArray.sync_trip_id.amt_splitup != null && csvArray.sync_trip_id.amt_splitup.length > 0) {
                    let index = csvArray.sync_trip_id.amt_splitup.findIndex(x => x.name == "Adjustment")
                    adjustment = index > -1 ? csvArray.sync_trip_id.amt_splitup[index].amt : '0';
                    let index2 = csvArray.sync_trip_id.amt_splitup.findIndex(x => x.id == "A008")
                    discount = index2 > -1 ? csvArray.sync_trip_id.amt_splitup[index2].amt : '0';
                  }
                  let vehicle_model = '';
                  if (csvArray.vehicle_id && csvArray.vehicle_id.vehicle_model_id) {
                    let index = that.vehicleModels.findIndex(x => x._id == csvArray.vehicle_id.vehicle_model_id)
                    if (index > -1)
                      vehicle_model = that.vehicleModels[index].name;
                  }
                  let km_rate = 0;
                  if (csvArray.sync_trip_id && csvArray.sync_trip_id.amt_splitup != null && csvArray.sync_trip_id.amt_splitup.length > 0) {
                    let index = csvArray.sync_trip_id.amt_splitup.findIndex(x => x.name == "Distance Amount")
                    km_rate = index > -1 && parseFloat(csvArray.sync_trip_id.total_distance_travelled) !== 0 ? parseFloat(csvArray.sync_trip_id.amt_splitup[index].amt) / parseFloat(csvArray.sync_trip_id.total_distance_travelled) : 0;
                  }
                  flag = true;
                  res1Array.push({
                    id: csvArray.unique_order_id ? csvArray.unique_order_id : 'N/A',
                    company_name: csvArray.company_id ? csvArray.company_id.company_name : 'N/A',
                    customer_name: csvArray.user_id && csvArray.user_id.firstname ? csvArray.user_id.firstname : 'Offline Customer',
                    customer_phone: csvArray.user_id && csvArray.user_id.phone_number ? csvArray.user_id.phone_number : 'Offline Customer',
                    source: csvArray.pickup_location ? csvArray.pickup_location : 'N/A',
                    start: csvArray.sync_trip_id != null ? csvArray.sync_trip_id.start_trip_calendar ? csvArray.sync_trip_id.start_trip_calendar : 'NA' : 'NA',
                    destination: csvArray.drop_location ? csvArray.drop_location : 'N/A',
                    Stop: csvArray.sync_trip_id != null ? csvArray.sync_trip_id.finished_trip_calendar ? csvArray.sync_trip_id.finished_trip_calendar : 'NA' : 'NA',
                    distance: csvArray.sync_trip_id ? csvArray.sync_trip_id.total_distance_travelled : csvArray.distance,
                    driver: csvArray.driver_id ? csvArray.driver_id.username : 'N/A',
                    vehicle: csvArray.vehicle_id ? csvArray.vehicle_id.display_name : 'N/A ',
                    vehicle_model: vehicle_model ? vehicle_model : 'N/A',
                    rating: csvArray.rating ? csvArray.rating != -1 ? csvArray.rating : ' N/A ' : 'N/A',
                    driver_rating: csvArray.sync_trip_id ? csvArray.sync_trip_id.trip_rating ? csvArray.sync_trip_id.trip_rating != -1 ? csvArray.sync_trip_id.trip_rating : 'N/A' : 'N/A' : 'N/A',
                    //feedback: csvArray.sync_trip_id ? csvArray.sync_trip_id.trip_comment ? csvArray.sync_trip_id.trip_comment : 'NA' : 'NA',
                    partner: csvArray.partner_id ? csvArray.partner_id.name : 'N/A',
                    additional_service: csvArray.additional_service_id ? csvArray.additional_service_id.name : 'NA',
                    order_type: csvArray.assign_type ? csvArray.assign_type : 'N/A',
                    payment_type: csvArray.sync_trip_id ? csvArray.sync_trip_id.payment_type ? (csvArray.sync_trip_id.payment_type == 1 ? 'Cash' : csvArray.sync_trip_id.payment_type == 2 ? 'Credit' : csvArray.sync_trip_id.payment_type == 3 ? 'Account' : csvArray.sync_trip_id.payment_type == 4 ? 'Online' : 'NA') : 'NA' : 'NA',
                    status: csvArray.order_status ? csvArray.order_status : 'N/A',
                    tariff: csvArray.tariff_id ? csvArray.tariff_id.name : 'N/A',
                    km_rate: km_rate.toFixed(2),
                    distance_amount: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_distance : "0",
                    discount: Math.abs(parseFloat(discount)),
                    extra_toll: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_extra_toll : "0",
                    extra_service: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_extra_service : "0",
                    slow_speed: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_slow_speed : "0",
                    base: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_tariff : "0",
                    vat: csvArray.sync_trip_id ? csvArray.sync_trip_id.vat : "0",
                    adjustment: adjustment,
                    total: csvArray.sync_trip_id ? csvArray.sync_trip_id.total_cost : '0',
                    bill: csvArray.sync_trip_id ? csvArray.sync_trip_id.bill_printed == '1' ? 'Printed' : 'Not Printed' : 'Not Printed'
                  });
                  if (j == res.searchOrder.length - 1) {
                    i = i + parseInt(offset);
                    fetch_status = true;
                  }
                }
              }
            }
            else {
              fetch_status = true;
              i = k;
            }
          }).catch((Error) => {
            console.log(Error)
            reset++;
            if (reset == 3) {
              console.log('-------------------here')
              that.toastr.error('Could not create csv try again later');
              that.progressvalue = 100;
              that.creatingcsv = false;
              that.progressvalue = 0;
              that._global.setCsvStatus(false, 0);
              clearInterval(that.livetrackingOrder);
              return;
            }
            that.toastr.warning('Network reset, csv creation restarted')
            //that.createCsv(interval_value, offset);
            if (flag) {
              res1Array.pop();
              i = k;
              fetch_status = true;
              setTimeout(() => { that.beginCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value, reset); }, 2000)
            }
            else {
              fetch_status = true;
              setTimeout(() => { that.beginCSVInterval(fetch_status, i, offset, res1Array, count, that, new_params, interval_value, reset); }, 2000)
            }
          })
        }
      }
    }, parseInt(interval_value));
  }

  /**
   * To generate pdf of the order records showing in the grid
   */
  public generatePDF() {
    var pdf = new jsPDF('p', 'mm', 'a3');
    pdf.setFontSize(12);
    pdf.setFont("times");
    var start_time_pdf = `Date from:${this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : ''}`;
    pdf.text(10, 20, start_time_pdf);
    pdf.text(10, 25, `Date to:${this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : ''}`);
    pdf.text(10, 30, 'Company: Dubai Taxi');
    pdf.text(10, 35, 'Company:Time zone: Asia/Dubai');

    var column2 = [
      { title: "", dataKey: "Subtotal" },
      { title: "", dataKey: "Count" },

    ];
    var row2 = [
      { "Subtotal": "Subtotal", "Count": this.order_detail_data.subtotal },
    ];

    pdf.autoTable(column2, row2, {
      theme: 'plain',

      margins: {
        top: 48,
        bottom: 0,
        left: 123,
        width: 300,
      },
      columnStyles: { Subtotal: { fontStyle: 'bold' }, Count: { halign: 'right' } },

      styles: {
        halign: 'right'
      },

    });

    var column3 = [
      { title: "", dataKey: "name" },
      { title: "Base Price", dataKey: "Base Price" },
      { title: "Vat Price", dataKey: "Vat Price" },

    ];
    var row3 = [
      { "name": "0.0%(VAT)", "Base Price": this.order_detail_data.bill_notprinted_amount + this.order_detail_data.bill_printed_amount, "Vat Price": this.order_detail_data.vat_price },
    ];
    //pdf.setLineWidth(0.5);
    //pdf.line(10, 75, 200, 75);
    pdf.autoTable(column3, row3, {
      theme: 'plain',
      margins: {
        top: 60,
        bottom: 0,
        left: 112,
        width: 300,
      },
      styles: {
        halign: 'right'
      },
    });
    var column1 = [
      { title: "", dataKey: "Total" },
      { title: "Count", dataKey: "Count" },
      { title: "Amount", dataKey: "Amount" },
    ];
    var row1 = [
      { "Total": 'Total', "Count": this.order_detail_data.bill_printed_count + this.order_detail_data.bill_notprinted_count, "Amount": this.order_detail_data.bill_notprinted_amount + this.order_detail_data.bill_printed_amount },
    ];

    pdf.autoTable(column1, row1, {
      theme: 'plain',
      margins: {
        top: 80,
        bottom: 0,
        left: 132,
        width: 300,
      }, // a number, array or object
      columnStyles: { Count: { halign: 'right' } },
      styles: {
        halign: 'right'
      },

    });
    var column6 = [
      { title: "", dataKey: "ByOrderType" },

    ];
    var row6 = [
      { "ByOrderType": "By Order Type" },
    ];

    pdf.autoTable(column6, row6, {
      theme: 'plain',

      margins: {
        top: 48,
        bottom: 0,
        left: 80,
        width: 300,
      },
      columnStyles: { ByOrderType: { fontStyle: 'bold' } },

    });


    var column7 = [
      { title: "", dataKey: "name" },
      { title: "", dataKey: "ordertype" },
      { title: "", dataKey: "Bypaymenttype" },
      { title: "", dataKey: "cash" },



    ];
    var row7 = [
      { "name": "DISPACTHER ASSIGNED (" + this.order_detail_data.dispatcher_assigned_count + ")", "ordertype": this.order_detail_data.dispatcher_assigned_amount },
      { "name": "FREE_RIDE (" + this.order_detail_data.offline_order_count + ")", "ordertype": this.order_detail_data.offline_order_amount },
      { "name": "CUSTOMER APP (" + this.order_detail_data.customer_order_count + ")", "ordertype": this.order_detail_data.customer_order_amount },
      { "name": "WEBSITE  (" + this.order_detail_data.website_order_count + ")", "ordertype": this.order_detail_data.website_order_amount },
      { "name": "KIOSK (" + this.order_detail_data.kiosk_order_count + ")", "ordertype": this.order_detail_data.kiosk_order_amount },

      { "name": "--------", "ordertype": "--------" },
    ];
    //pdf.setLineWidth(0.5);
    //pdf.line(10, 75, 200, 75);
    pdf.autoTable(column7, row7, {
      theme: 'plain',
      margins: {
        top: 54,
        bottom: 0,
        right: 95,
        width: 300,
      },
    });
    var columns = [
      { dataKey: "name" },
      { dataKey: "Count" },
      { dataKey: "Amount" },
    ];
    var rows = [
      { "name": "Bill printed", "Count": this.order_detail_data.bill_printed_count, "Amount": this.order_detail_data.bill_printed_amount },
      { "name": "Bill not printed", "Count": this.order_detail_data.bill_notprinted_count, "Amount": this.order_detail_data.bill_notprinted_amount },
    ];

    pdf.setLineWidth(0.1);
    pdf.line(10, 55, 270, 55);
    pdf.autoTable(columns, rows, {
      theme: 'plain',
      margins: {
        top: 95,
        bottom: 60,
        left: 120,
        width: 300,
      }, // a number, array or object
      styles: {
        halign: 'right'
      },
      drawHeaderRow: function () {
        return false;
      },
    });
    pdf.setLineWidth(0.1);
    pdf.line(10, 75, 270, 75);

    var columns_payment = [
      { title: "By payment extra", dataKey: "name" },
      { title: "", dataKey: "Amount" }
    ];
    var rows_payment = [];

    for (let i = 0; i < this.payment_extra.length; i++) {
      rows_payment.push({
        name: this.payment_extra[i].name ? this.payment_extra[i].name : 'N/A',
        Amount: this.payment_extra[i].amt ? this.payment_extra[i].amt : 'N/A',
      });
    }
    pdf.autoTable(columns_payment, rows_payment, {
      theme: 'plain',
      margins: {
        top: 100 + (8 * this.payment_extra.length),
        bottom: 60,
        left: 15,
        width: 300,
        right: 105,
      }, // a number, array or object

    });
    // //  pdf.setLineWidth(0.1);
    //  // pdf.line(10,90+(8*this.payment_extra.length), 200, 90+(8*this.payment_extra.length));

    var columns_paymenttype = [
      { title: "By payment type", dataKey: "name" },
      { title: "", dataKey: "Amount" }
    ];
    var rows_paymenttype = [
      { "name": "Cash", "Amount": '' },
      { "name": "Trip total", "Amount": this.order_detail_data.subtotal },
      { "name": "Cash total", "Amount": this.order_detail_data.subtotal },
    ];

    pdf.autoTable(columns_paymenttype, rows_paymenttype, {
      theme: 'plain',
      margins: {
        top: 85,
        //top: 125+(8*this.payment_extra.length),
        bottom: 60,
        left: 15,
        width: 300,
        right: 95,
      }, // a number, array or object

    });

    var columns_totaldistance = [
      { title: "", dataKey: "name" },
      { title: "", dataKey: "Amount" }
    ];
    var rows_totaldistance = [
      { "name": "-----------------", "Amount": '--------' },
      { "name": "Total distance", "Amount": this.order_detail_data.total_distance + ' Km' },

    ];

    pdf.autoTable(columns_totaldistance, rows_totaldistance, {
      theme: 'plain',
      margins: {
        //top: 85,
        top: 125 + (8 * this.payment_extra.length),
        bottom: 60,
        left: 15,
        width: 300,
        right: 80,
      }, // a number, array or object
      columnStyles: { name: { fontStyle: 'bold' } },
    });


    // if (this.payment_type_id != undefined && this.payment_type_id != '') {
    //   this.payment_type_id = this.payment_type_id.id;
    // }
    const params1 = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      tariff_id: this.tariff_id ? this.tariff_id._id : '',
      driver_id: this.driver_id ? this.driver_id._id : '',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid._id : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no._id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no._id : '',
      keyword: this.unique_order_id ? this.unique_order_id : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      // userid: this.user_id ? this.user_id : '',
      partner_id: this.partner_id ? this.partner_id._id : '',
      payment_type_id: this.payment_type_id ? this.payment_type_id.id : '',
      driver_groups_id: this.driver_groups_id ? this.driver_groups_id._id : '',
      payment_extra_id: this.payment_extra_id ? this.payment_extra_id._id : '',
      additional_service_id: this.additional_service_id ? this.additional_service_id._id : '',
      device_id: this.device_id ? this.device_id._id : '',
      shift_details_id: this.shift_details_id ? this.shift_details_id : '',
      _id: this._id ? this._id : '',
      pickup_location: this.pickup_location ? this.pickup_location : '',
      drop_location: this.drop_location ? this.drop_location : '',
      user_id: this.user_id ? this.user_id._id : '',
      sync_trip_id: this.sync_trip_id ? this.sync_trip_id : '',
      bill_printed: this.bill_printed ? this.bill_printed : '',
      current_order_status: this.current_order_status ? this.current_order_status : '',
      model_id: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder._id ? this.vehicleTypeFilterOrder._id : '' : '',
    };
    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.searchOrder(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var res: any = this.encDecService.dwt(this.session, dec.data);
        //console.log('Arrray>>>>', res.searchOrder);
        let labels = [
          'ID/Company',
          'Customer',
          'Source/Start Time/Destination/Stop Time/Distance',
          'Driver/Accept time/Rating/Feedback',
          'Vehichle/Vehicle Plate/Vehicle Side Number',
          'Partner',
          'Additional Service',
          'Order-type/Payment Type/Status',
          'Tariff',
          'Extra Toll',
          'Extra Service',
          'Slow Speed',
          'Base',
          'Vat',
          'Total',
          'Bill Printed',
          'Date'
        ];
        let resArray = [];
        for (let i = 0; i < res.searchOrder.length; i++) {
          const csvArray = res.searchOrder[i];
          var id = csvArray.unique_order_id ? csvArray.unique_order_id : 'N/A';
          var company_name = csvArray.company_name ? csvArray.company_name : 'Dubai Taxi';
          id = id + '/' + company_name;

          var source = csvArray.pickup_location ? csvArray.pickup_location : 'N/A';
          var start = csvArray.sync_trip_id != null ? csvArray.sync_trip_id.start_trip_calendar ? csvArray.sync_trip_id.start_trip_calendar : 'NA' : 'NA';
          var destination = csvArray.drop_location ? csvArray.drop_location : 'N/A';
          var stop = csvArray.sync_trip_id != null ? csvArray.sync_trip_id.finished_trip_calendar ? csvArray.sync_trip_id.finished_trip_calendar : 'NA' : 'NA';
          var distance = csvArray.sync_trip_id ? csvArray.sync_trip_id.total_distance_travelled : csvArray.distance;
          var driver = csvArray.driver_id ? csvArray.driver_id.username : 'N/A';
          var accept_time = csvArray.sync_trip_id ? csvArray.sync_trip_id.start_trip_calendar : "NA";
          var rating = csvArray.sync_trip_id ? csvArray.sync_trip_id.trip_rating : ' N/A ';
          var feedback = csvArray.sync_trip_id ? csvArray.sync_trip_id.trip_comment ? csvArray.sync_trip_id.trip_comment : 'NA' : 'NA';
          var vehicle = csvArray.vehicle_id ? csvArray.vehicle_id.display_name : 'N/A ';
          var vehicle_model = csvArray.vehicle_id ? csvArray.vehicle_id.vehicle_model : 'N/A';
          var partner = csvArray.partner_id ? csvArray.partner_id.name : 'N/A';
          var additional_service = csvArray.additional_service_id ? csvArray.additional_service_id.name : 'NA';
          var order_type = csvArray.assign_type ? csvArray.assign_type : 'N/A';
          var payment_type = csvArray.sync_trip_id ? csvArray.sync_trip_id.payment_type == '1' ? 'CASH' : 'CREDIT' : 'CASH';
          var status = csvArray.order_status ? csvArray.order_status : 'N/A';

          resArray.push({
            id: id,
            customer: csvArray.user_id && csvArray.user_id.firstname ? csvArray.user_id.firstname : 'Offline Customer',
            source: source + "/\n" + start + "/\n" + destination + "/\n" + stop + "/\n" + distance,
            driver: driver + '/' + accept_time + '/' + rating + '/' + feedback,
            vehicle: vehicle + '/' + vehicle_model,
            vehicle_model: csvArray.vehicle_id ? csvArray.vehicle_id.vehicle_model : 'N/A',
            partner: partner,
            additional_service: additional_service,
            order_type: order_type + '/' + payment_type + '/' + status,
            tariff: csvArray.tariff_id ? csvArray.tariff_id.name : 'N/A',

            extra_toll: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_extra_toll : "0 AED",
            extra_service: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_extra_service : "0 AED",
            slow_speed: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_slow_speed : "0 AED",
            base: csvArray.sync_trip_id ? csvArray.sync_trip_id.amt_tariff + '                ' : "0 AED",
            vat: csvArray.sync_trip_id ? csvArray.sync_trip_id.vat + '                ' : "0 AED",
            total: csvArray.sync_trip_id ? csvArray.sync_trip_id.total_cost + '                ' : '0 AED',
            bill: csvArray.sync_trip_id ? csvArray.sync_trip_id.bill_printed == '1' ? 'Printed' : 'Not Printed' : 'Not Printed',
            Date: csvArray.created_at ? csvArray.created_at : 'N/A',
          });
        }
        //var doc = new jsPDF();
        var doc = new jsPDF('landscape');
        var col = [
          { title: "Id/Company", dataKey: "id" },
          { title: "Customer", dataKey: "customer" },
          { title: "Source/\nStart Time/\nDestination/\nStop Time/\nDistance", dataKey: "source" },
          { title: "Driver/Accept time/Rating/Feedback", dataKey: "driver" },
          { title: "Vehichle/Vehicle Plate/Vehicle Side Number", dataKey: "vehicle" },
          { title: "Partner", dataKey: "partner" },
          { title: "Additional Service", dataKey: "additional_service" },
          { title: "Order type/Payment type/Status", dataKey: "order_type" },
          { title: "tariff", dataKey: "tariff" },
          { title: "extra_toll", dataKey: "extra_toll" },
          { title: "extra_service", dataKey: "extra_service" },
          { title: "slow_speed", dataKey: "slow_speed" },
          { title: "base", dataKey: "base" },
          { title: "vat", dataKey: "vat" },
          { title: "total", dataKey: "total" },
          { title: "bill", dataKey: "bill" },
          { title: "Date", dataKey: "Date" }
        ];
        var options = {
          styles: { // Defaul style
            lineWidth: 0.01,
            lineColor: 0,
            fillStyle: 'DF',
            halign: 'center',
            valign: 'middle',
            columnWidth: 'auto',
            overflow: 'linebreak'
          },
        };
        pdf.addPage();
        pdf.autoTable(col, resArray, options);
        pdf.save('Order_List' + moment().format('YYYY-MM-DD_HH_mm_ss') + '.pdf');
      }
    }).catch((error) => {
      if (error.message === 'Cannot read property \'navigate\' of undefined') {
      }
    });

  }



  /**
   * This function is used for pagination in order module
   * @param data
   */
  pagingAgent(data) {
    this.searchSubmit = true;
    this.orderData = [];
    //this.orderData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    let drop_location: any = '';
    let pickup_location: any = '';
    if (this.pickup_location.length > 0) {
      pickup_location = [];
      this.pickup_location.forEach(element => {
        pickup_location.push([element.loc.coordinates[0]])
      });
    }
    if (this.drop_location.length > 0) {
      drop_location = [];
      this.drop_location.forEach(element => {
        drop_location.push([element.loc.coordinates[0]])
      });
    }
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      tariff_id: this.tariff_id ? this.tariff_id : '',
      driver_id: this.driver_id ? [this.driver_id] : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid.unique_driver_id : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      keyword: this.unique_order_id ? this.unique_order_id : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      user_id: this.user_id ? this.user_id : '',
      partner_id: this.partner_id ? this.partner_id : '',
      payment_type_id: this.payment_type_id ? this.payment_type_id.id : '',
      driver_groups_id: '',
      payment_extra_id: this.payment_extra_id ? this.payment_extra_id : '',
      additional_service_id: this.additional_service_id ? this.additional_service_id : '',
      device_id: this.device_id ? this.device_id : '',
      shift_details_id: this.shift_details_id ? this.shift_details_id : '',
      _id: this._id ? this._id : '',
      pickup_location: pickup_location,
      drop_location: drop_location,
      sync_trip_id: this.sync_trip_id ? this.sync_trip_id : '',
      bill_printed: this.bill_printed ? this.bill_printed : '',
      current_order_status: this.current_order_status ? this.current_order_status : '',
      vehicle_model_ids: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder : '',
      promo_id: this.promo_id ? this.promo_id : '',
      generate_by: this.generated_by ? this.generated_by : '',
      transaction_status: this.transaction_status ? this.transaction_status : '',
      order_edited: this.order_edited ? this.order_edited : '',
      flagged_orders: this.flagged_orders ? this.flagged_orders : ''
    };
    this.searchSubmit = true;
    //this.orderData = [];
    if (this.driver_groups_id.length > 0 && !params.driver_id) {//gets the list of drivers specific to one more driver group
      this.getDriversInDriverGroup(this.driver_groups_id.map(x => x._id)).then((res) => {
        params.driver_id = res;
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._orderService.searchOrder(enc_data).then((dec) => {
          if (dec && dec.status == 200) {
            var dec: any = this.encDecService.dwt(this.session, dec.data);
            this.orderData = dec.searchOrder;
          }
          this.searchSubmit = false;
        });
      });
    }
    else {
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._orderService.searchOrder(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var dec: any = this.encDecService.dwt(this.session, dec.data);
          this.orderData = dec.searchOrder;
        }
        this.searchSubmit = false;
      });
    }
  }

  /**
   * For searching particular order record
   */
  public getOrders() {
    //window.scrollTo(0, 500);
    this.orderData = [];
    this.pNo = 1;
    this.searchSubmit = true;
    // if (this.payment_type_id != undefined && this.payment_type_id != '') {
    //   this.payment_type_id = this.payment_type_id.id;
    // }
    let params, params2;
    let drop_location: any = '';
    let pickup_location: any = '';
    if (this.pickup_location.length > 0) {
      pickup_location = [];
      this.pickup_location.forEach(element => {
        pickup_location.push([element.loc.coordinates[0]])
      });
    }
    if (this.drop_location.length > 0) {
      drop_location = [];
      this.drop_location.forEach(element => {
        drop_location.push([element.loc.coordinates[0]])
      });
    }
    if (this._id != undefined && this._id !== '') {
      params = {
        shift_id: this._id,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        tariff_id: this.tariff_id ? this.tariff_id._id : '',
        driver_id: this.driver_id ? [this.driver_id] : '',
        unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
        vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        start_date: '',
        end_date: '',
        // userid: this.user_id ? this.user_id : '',
        partner_id: this.partner_id ? this.partner_id._id : '',
        payment_type_id: this.payment_type_id ? this.payment_type_id.id : '',
        driver_groups_id: '',
        payment_extra_id: this.payment_extra_id ? this.payment_extra_id._id : '',
        additional_service_id: this.additional_service_id ? this.additional_service_id._id : '',
        device_id: this.device_id ? this.device_id : '',
        shift_details_id: this.shift_details_id ? this.shift_details_id : '',
        _id: this._id ? this._id : '',
        pickup_location: '',
        drop_location: '',
        user_id: this.user_id ? this.user_id : '',
        sync_trip_id: this.sync_trip_id ? this.sync_trip_id : '',
        bill_printed: this.bill_printed ? this.bill_printed : '',
        current_order_status: this.current_order_status ? this.current_order_status : '',
        vehicle_model_ids: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder : '',
        promo_id: this.promo_id ? this.promo_id : '',
        generate_by: this.generated_by ? this.generated_by : '',
        transaction_status: this.transaction_status ? this.transaction_status : '',
        cancellation_reason_id: this.cancelReasonId ? this.cancelReasonId : '',
        customer_rating: this.CustomerRate ? this.CustomerRate : '',
        vehicle_category_ids: this.vehicleCategory ? this.vehicleCategory : '',
        order_comment_status: this.orderComments ? this.orderComments : '',
        order_edited: this.order_edited ? this.order_edited : '',
        flagged_orders: this.flagged_orders ? this.flagged_orders : '',
        start_price: this.startPrice ? this.startPrice : '',
        end_price: this.endPrice ? this.endPrice : '',
        guest_user_id: this.guest_user_id ? this.guest_user_id :'',
      }
      params2 = {
        shift_id: this._id,
        tariff_id: this.tariff_id ? this.tariff_id : '',
        driver_id: this.driver_id ? this.driver_id : '',
        driver: this.driverForm ? this.driverForm : '',
        unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
        side: this.sideForm ? this.sideForm : '',
        vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
        plate: this.plateForm ? this.plateForm : '',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        start_date: '',
        end_date: '',
        // userid: this.user_id ? this.user_id : '',
        partner_id: this.partner_id ? this.partner_id : '',
        payment_type_id: this.payment_type_id ? this.payment_type_id : '',
        driver_groups_id: this.driver_groups_id ? this.driver_groups_id : [],
        payment_extra_id: this.payment_extra_id ? this.payment_extra_id : '',
        additional_service_id: this.additional_service_id ? this.additional_service_id : '',
        device_id: this.device_id ? this.device_id : '',
        device: this.deviceForm ? this.deviceForm : '',
        shift_details_id: this.shift_details_id ? this.shift_details_id : '',
        _id: this._id ? this._id : '',
        pickup_location: this.pickup_location ? this.pickup_location : '',
        drop_location: this.drop_location ? this.drop_location : '',
        user_id: this.user_id ? this.user_id : '',
        sync_trip_id: this.sync_trip_id ? this.sync_trip_id : '',
        bill_printed: this.bill_printed ? this.bill_printed : '',
        current_order_status: this.current_order_status ? this.current_order_status : '',
        model_id: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder : '',
        promo_id: this.promo_id ? this.promo_id : '',
        generate_by: this.generated_by ? this.generated_by : '',
        transaction_status: this.transaction_status ? this.transaction_status : '',
        user_name: this.user_name ? this.user_name : '',
        cancel_reason_id: this.cancelReasonId ? this.cancelReasonId : [],
        cancelReasons: this.cancelReasons ? this.cancelReasons : [],
        customer_rating: this.CustomerRate ? this.CustomerRate : '',
        vehicle_category_ids: this.vehicleCategory.length > 0 ? this.vehicleCategory : [],
        order_comment_status: this.orderComments ? this.orderComments : '',
        order_edited: this.order_edited ? this.order_edited : '',
        flagged_orders: this.flagged_orders ? this.flagged_orders : '',
        start_price: this.startPrice ? this.startPrice : '',
        end_price: this.endPrice ? this.endPrice : '',
        guest_user_id: this.guest_user_id ? this.guest_user_id :'',
      }
    }
    else {
      params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        tariff_id: this.tariff_id ? this.tariff_id._id : '',
        driver_id: this.driver_id ? [this.driver_id] : '',
        unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
        vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        start_date: this.unique_order_id.length !== 0 ? "" : this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.unique_order_id.length !== 0 ? "" : this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
        // userid: this.user_id ? this.user_id : '',
        partner_id: this.partner_id ? this.partner_id._id : '',
        payment_type_id: this.payment_type_id ? this.payment_type_id.id : '',
        driver_groups_id: '',
        payment_extra_id: this.payment_extra_id ? this.payment_extra_id._id : '',
        additional_service_id: this.additional_service_id ? this.additional_service_id._id : '',
        device_id: this.device_id ? this.device_id : '',
        shift_details_id: this.shift_details_id ? this.shift_details_id : '',
        _id: this._id ? this._id : '',
        pickup_location: pickup_location,
        drop_location: drop_location,
        user_id: this.user_id ? this.user_id : '',
        sync_trip_id: this.sync_trip_id ? this.sync_trip_id : '',
        bill_printed: this.bill_printed ? this.bill_printed : '',
        current_order_status: this.current_order_status ? this.current_order_status : '',
        vehicle_model_ids: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder : '',
        promo_id: this.promo_id ? this.promo_id : '',
        generate_by: this.generated_by ? this.generated_by : '',
        transaction_status: this.transaction_status ? this.transaction_status : '',
        cancellation_reason_id: this.cancelReasonId ? this.cancelReasonId : '',
        customer_rating: this.CustomerRate ? this.CustomerRate : '',
        vehicle_category_ids: this.vehicleCategory ? this.vehicleCategory : '',
        order_comment_status: this.orderComments ? this.orderComments : '',
        order_edited: this.order_edited ? this.order_edited : '',
        flagged_orders: this.flagged_orders ? this.flagged_orders : '',
        start_price: this.startPrice ? this.startPrice : '',
        end_price: this.endPrice ? this.endPrice : '',
        guest_user_id: this.guest_user_id ? this.guest_user_id :'',
      };
      params2 = {
        limit: this.pageSize,
        tariff_id: this.tariff_id ? this.tariff_id : '',
        driver_id: this.driver_id ? this.driver_id : '',
        driver: this.driverForm ? this.driverForm : '',
        unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
        side: this.sideForm ? this.sideForm : '',
        vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
        plate: this.plateForm ? this.plateForm : '',
        keyword: this.unique_order_id ? this.unique_order_id : '',
        start_date: this.unique_order_id.length !== 0 ? "" : this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.unique_order_id.length !== 0 ? "" : this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
        // userid: this.user_id ? this.user_id : '',
        partner_id: this.partner_id ? this.partner_id : '',
        payment_type_id: this.payment_type_id ? this.payment_type_id : '',
        driver_groups_id: this.driver_groups_id ? this.driver_groups_id : [],
        payment_extra_id: this.payment_extra_id ? this.payment_extra_id : '',
        additional_service_id: this.additional_service_id ? this.additional_service_id : '',
        device_id: this.device_id ? this.device_id : '',
        device: this.deviceForm ? this.deviceForm : '',
        shift_details_id: this.shift_details_id ? this.shift_details_id : '',
        _id: this._id ? this._id : '',
        pickup_location: this.pickup_location ? this.pickup_location : '',
        drop_location: this.drop_location ? this.drop_location : '',
        user_id: this.user_id ? this.user_id : '',
        sync_trip_id: this.sync_trip_id ? this.sync_trip_id : '',
        bill_printed: this.bill_printed ? this.bill_printed : '',
        current_order_status: this.current_order_status ? this.current_order_status : '',
        model_id: this.vehicleTypeFilterOrder ? this.vehicleTypeFilterOrder : '',
        promo_id: this.promo_id ? this.promo_id : '',
        generate_by: this.generated_by ? this.generated_by : '',
        transaction_status: this.transaction_status ? this.transaction_status : '',
        user_name: this.user_name ? this.user_name : '',
        cancel_reason_id: this.cancelReasonId ? this.cancelReasonId : [],
        cancelReasons: this.cancelReasons ? this.cancelReasons : [],
        customer_rating: this.CustomerRate ? this.CustomerRate : '',
        vehicle_category_ids: this.vehicleCategory.length > 0 ? this.vehicleCategory : [],
        order_comment_status: this.orderComments ? this.orderComments : '',
        order_edited: this.order_edited ? this.order_edited : '',
        flagged_orders: this.flagged_orders ? this.flagged_orders : '',
        start_price: this.startPrice ? this.startPrice : '',
        end_price: this.endPrice ? this.endPrice : '',
        guest_user_id: this.guest_user_id ? this.guest_user_id :'',
      };
    }
    //this.payment_type_id = this.payment_type_id.id;
    if (
      this.tariff_id != '' || this.driver_id != '' || this.selectedMoment != '' ||
      this.selectedMoment1 != '' || this.unique_order_id[0] != '' || this.user_id != '' ||
      this.partner_id != '' || this.payment_type_id != '' || this.driver_groups_id.length > 0 ||
      this.payment_extra_id != '' || this.additional_service_id != '' || this.device_id != '' ||
      this.pickup_location != '' || this.drop_location != '' || this.sync_trip_id != '' || this.bill_printed != '' || this.vehichle_plate_no != '' || this.vehichle_side_no != '' ||
      this.uniquedriverid != '' || this._id != undefined) {
      //this.orderData = [];
      this.del = true;
      //sessionStorage.setItem('order-management',JSON.stringify(params2));
      this._global.order_management = params2;
      if (this.driver_groups_id.length > 0 && !params.driver_id) {//gets the list of drivers specific to one more driver group
        this.getDriversInDriverGroup(this.driver_groups_id.map(x => x._id)).then((res) => {
          params.driver_id = res;
          this.getListOrders(params)
        });
      }
      else
        this.getListOrders(params)
    } else {
      const params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      // this.orderData =[];
      this.del = true;
      console.log(params)
      this._orderService.getAllOrder(params).then((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.orderData = data.getAllOrder;
          console.log(this.orderData);
          this.order_detail_data = data.order_detail_data;
          this.payment_extra = data.payment_extra;
          this.orderLength = data.count;
        }
        this.searchSubmit = false;
        this.is_search = false;
      });
    }
  }
  getListOrders(params) {
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    console.log(params);
    this._orderService.searchOrder(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.orderData = data.searchOrder;
        console.log(this.orderData);
        //this.order_detail_data = data.order_detail_data;
        //this.payment_extra = data.payment_extra;
        if (this._id !== undefined && this._id !== '')
          this.orderLength = 1;
        else {
          this.orderLength = data.count;
        }
        this.searchSubmit = false;
        this.is_search = true;
      }
    });
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.searchOrderAssigned(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        // console.log(data.order_detail_data);
        this.order_detail_data.dispatcher_assigned_count = data.order_detail_data.dispatcher_assigned_count;
        this.order_detail_data.dispatcher_assigned_amount = data.order_detail_data.dispatcher_assigned_amount;
        this.order_detail_data.offline_order_count = data.order_detail_data.offline_order_count;
        this.order_detail_data.offline_order_amount = data.order_detail_data.offline_order_amount;
        this.order_detail_data.customer_order_count = data.order_detail_data.customer_order_count;
        this.order_detail_data.customer_order_amount = data.order_detail_data.customer_order_amount;
        this.order_detail_data.website_order_count = data.order_detail_data.website_order_count;
        this.order_detail_data.website_order_amount = data.order_detail_data.website_order_amount;
        this.order_detail_data.kiosk_order_count = data.order_detail_data.kiosk_order_count;
        this.order_detail_data.kiosk_order_amount = data.order_detail_data.kiosk_order_amount;
      }
    })
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.searchOrderSalik(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        //this.orderData = data.searchOrder;
        //this.order_detail_data = data.order_detail_data;
        this.payment_extra = data.payment_extra;
        //console.log(data.payment_extra);
      }
    })
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.searchOrderMOreInfo(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        //this.orderData = data.searchOrder;
        //this.order_detail_data = data.order_detail_data;
        //console.log(data.order_detail_data);
        this.order_detail_data.total_distance = data.order_detail_data.total_distance;
        this.order_detail_data.bill_printed_count = data.order_detail_data.bill_printed_count;
        this.order_detail_data.bill_printed_amount = data.order_detail_data.bill_printed_amount;
        this.order_detail_data.bill_notprinted_count = data.order_detail_data.bill_notprinted_count;
        this.order_detail_data.bill_notprinted_amount = data.order_detail_data.bill_notprinted_amount;
        this.order_detail_data.cash_total = data.order_detail_data.cash_total;
        this.order_detail_data.credit_total = data.order_detail_data.credit_total;
        this.order_detail_data.card_total = data.order_detail_data.card_total;
        this.order_detail_data.subtotal = data.order_detail_data.subtotal;
        this.order_detail_data.base_price = data.order_detail_data.base_price;
        this.order_detail_data.vat_price = data.order_detail_data.vat_price;
      }
    })
  }

  /**
   *  start time timepicker
   */
  checkShiftstartTime() {
    this.selectedMoment1 = '';
  }

  /**
   *  end time timepicker
   */
  checkShiftstartTime1() {

    if (this.selectedMoment1 != '') {
      // this.selectedMoment="";
    }
  }

  /**
   * Reset the search filters
   *
   */
  reset() {
    this.user_name = '';
    this.searchSubmit = true;
    this.pNo = 1;
    this.tariff_id = ''
    this.driver_id = '';
    this._id = '';
    this.unique_order_id = [];
    this.ordersFilter = [];
    this.uniquedriverid = '';
    this.driverIdData = [];
    this.user_id = '';
    this.partner_id = '';
    this.payment_type_id = '';
    this.shift_details_id = '';
    this.additional_service_id = '';
    this.device_id = '';
    this.driver_groups_id = [];
    this.payment_extra_id = '';
    this.bill_printed = '';
    this.sync_trip_id = '';
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm'));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    this.vehichle_plate_no = '';
    this.vehichle_side_no = '';
    this.pickup_location = '';
    this.drop_location = '';
    $('#customer').ddslick('destroy');
    this.user_id = '';
    this.drop_location = '';
    this.generated_by = "";
    this.transaction_status = ''
    this.order_edited = '';
    this.flagged_orders = '';
    //this.orderData = [];
    //sessionStorage.removeItem('order-management');
    this.vehicleTypeFilterOrder = '';
    this.pageSize = 10;
    this.current_order_status = 'completed',
      this.searchSubmit = false;
    this.driverForm = '';
    this.deviceForm = '';
    this.sideForm = '';
    this.plateForm = '';
    this.promo_id = '';
    this.customerRating.setValue(null);
    this.cancelReasons = [];
    this.cancelReasonId = [];
    this.vehicleCategory = [];
    this.orderComments = [];
    this.startPrice ='';
    this.endPrice = '';
    this.guest_user_id = '';
    if (this.dtc) {
      this.companyId = this.companyData.slice().map(x => x._id);
      this.companyIdFilter = this.companyId.slice()
      this.companyIdFilter.push('all')
    }
    this.getOrders();
  }
  /**
   * For sorting orders 
   * @param key 
   */
  /*sort(key) {
    this.searchSubmit = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
    
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: this.sortOrder,
      sortByColumn: this.key
    };
    
    if(this.keyword) {
      this._paymentService.getPaymentListing(this.keyword, params).subscribe((data) => {
        this.paymentData = data.getPayment;
        this.searchSubmit = false;
      });
    }else {
      this._paymentService.getPaymentListing(null, params).subscribe((data) => {
        this.paymentData = data.getPayment;
        this.searchSubmit = false;
      });
    }
  }*/
  /**
   * Refresh
   *
   */
  refresh() {
    if (
      this.is_search == true) {
      this.getOrders();
    }
    else {
      // this.orderData = [];
      this.ngOnInit();
    }

  }

  /**
  * Get Tariff for drop down
  *
  */
  public getTariff() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._tariffService.getTariffs(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var Tariffdata: any = this.encDecService.dwt(this.session, dec.data);
        this.tariffData = Tariffdata.getTariff;
      }
    });
  }

  /**
   * Autocomplete search on tariff drop down
   * @param data
   */
  public searchTariff(data, event) {
    this.loadingIndicator = event;
    if (typeof data !== 'object') {
      let params = {
        offset: 0,
        limit: 10,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        'search': data,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._tariffService.getTariffForOrder(enc_data).then(dec => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.tariffData = data.getTariff;
        }
        this.loadingIndicator = '';
      });
    } else {
      this.tariff_id = data.tariff_id;
      this.loadingIndicator = '';
    }
  }

  displayFnTariff(data): string {
    return data ? data.name : data;
  }
  /**
     * Get Vehichle Side and Plate data for drop down
     *
     */
  public getVehichleData() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      role: 'admin',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehichleService.getVehicleListingAdmin(enc_data).subscribe(dec => {
      if (dec && dec.status == 200) {
        var Vehichledata: any = this.encDecService.dwt(this.session, dec.data);
        this.vehichlePlateData = Vehichledata.getVehicles;
        this.vehichleSideData = Vehichledata.getVehicles;
      }
    });
  }
  searchVehichlePlateNumber(data) {
    if (typeof data === 'object') {
      this.vehichle_plate_no = data._id;
    }
    else {
      this.vehichle_plate_no = '';
    }
  }
  searchVehichleSidenumber(data) {
    if (typeof data === 'object') {
      this.vehichle_side_no = data._id;
    }
    else {
      this.vehichle_side_no = '';
    }
  }
  displayFnVehichleSide(data): string {
    return data ? data.vehicle_identity_number : data;
  }
  displayFnVehichlePlate(data): string {
    return data ? data.plate_number : data;
  }

  /**
   * Get driver for drop down
   *
   */
  public getDriver() {
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.searchForDriver(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var Driverdata: any = this.encDecService.dwt(this.session, dec.data);
        this.driverData = Driverdata.driver;
        this.driverIdData = Driverdata.driver;
      }
    });
  }

  /**
   * Autocomplete search on driver drop down
   * @param data 
   */
  searchDriver(data) {
    if (typeof data === 'object') {
      this.driver_id = data._id;
    }
    else {
      this.driver_id = '';
    }
  }
  displayFnDriver(data): string {
    return data ? data.name : data;
  }
  /**
   * Autocomplete search on driver unique id drop down
   * @param data 
   */
  searchDriverId(data) {
    if (typeof data === 'object') {
      this.uniquedriverid = data._id;
    }
    else {
      this.uniquedriverid = '';
    }
  }
  displayFnDriverId(data): string {
    return data ? data.emp_id : data;
  }

  /**
   * Get Users for drop down
   *
   */
  public getUser() {
    var searchText = this.user_id;
    var params = {
      search: searchText,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._customerService.getCustomerByName(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var userdata: any = this.encDecService.dwt(this.session, dec.data);
        this.customerData = userdata.result;
      }
    });
  }
  displayNameFn(data): string {
    return data ? data.firstname : data;
  }
  public getSelectOrders(data) {
    this.user_id = data.source.value._id;
    this.getOrders();
  }

  /**
   * Autocomplete search on customer dropdown
   * @param data
   */
  public searchCustomer(data, event) {
    if (typeof data === 'object') {
      this.user_id = data._id;
    }
    else {
      this.user_id = '';
    }
  }
  public guestCustomer(data) {
    if (typeof data === 'object') {
      this.guest_user_id = data._id;
    }
    else {
      this.guest_user_id = '';
    }
  }
  displayFnCustomer(data): string {
    return data ? data.firstname : data;
  }
  displayFnCustomerPhone(data): string {
    return data ? data.phone_number : data;
  }
  guestCustPhone(data): string {
    return data ? data._id : data;
  }
  /**
   * Get Partner for drop down
   *
   */
  public getPartners() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._partnerService.getPartner(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.partnersData = data.getPartners;
      }
    });
  }

  /**
   * Autocomplete search on partner drop down
   * @param data 
   */
  public searchPartner(data, event) {
    this.loadingIndicator = event;
    if (typeof data !== 'object') {
      let params = {
        offset: 0,
        limit: 10,
        sortOrder: 'desc',
        sortByColumn: '_id',
        'name': data,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._partnerService.getPartner(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.partnersData = data.getPartners;
        }
        this.loadingIndicator = '';
      });
    } else {
      this.partner_id = data.partner_id;
      this.loadingIndicator = '';
    }
  }
  displayFnPartner(data): string {
    return data ? data.name : data;
  }

  /**
   * Get Payment Type for drop down
   *
   */
  public getPaymentType() {

    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      search: '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._paymentTypeService.getPaymentTypes(enc_data).then((dec) => {
      this.paymentTypeData = [{ "id": "1", "type": "CASH" }, { "id": "2", "type": "CREDIT" }, { "id": "3", "type": "ACCOUNT" }, { "id": "4", "type": "ONLINE" }]
      //alert(JSON.stringify(this.paymentTypeData));
    });
  }
  displayFnPaymentType(data): string {
    //alert(JSON.stringify(data))
    return data ? data.type : data;
  }


  /**
     * Get Driver Groups for drop down
     *
     */
  public getDriverGroups() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      //'search': data,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.getDriversGroup(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.driverGroupData = data.getDriverGroups;
      }
    });
  }

  /**
   * Get Payment Extra for dropdown
   *
   */
  public getPaymentExtra() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      search: '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._paymentService.getPaymentListing(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.paymentData = data.getPayment;
      }
    });
  }

  /**
   * Autocomplete search on payment extra module
   * @param data 
   */
  public searchPaymentExtra(data) {
    if (typeof data !== 'object') {
      let params = {
        offset: 0,
        limit: 10,
        sortOrder: 'desc',
        sortByColumn: '_id',
        'search': data,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._paymentService.getPaymentListing(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.paymentData = data.getPayment;
        }
      });
    } else {
      this.payment_extra_id = data.payment_extra_id;
    }
  }
  displayFnPaymentExtra(data): string {
    return data ? data.payment_extra : data;
  }

  /**
     * Get Additional services for dropdown
     *
     */
  public getServices() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._additional_Service.getaddService(enc_data).subscribe((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.serviceGroupData = data.getService;
      }
    });
  }

  /**
   * Autocomplete search on additional service
   * @param data 
   */
  public searchAddService(data) {
    if (typeof data !== 'object') {
      let params = {
        offset: 0,
        limit: 10,
        sortOrder: 'desc',
        sortByColumn: '_id',
        'name': data,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._additional_Service.getaddService(enc_data).subscribe((dec) => {
        if (dec && dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.serviceGroupData = data.getService;
        }
      });
    } else {
      this.additional_service_id = data.additional_service_id;
    }
  }
  displayFnAddService(data): string {
    return data ? data.name : data;
  }

  /**
   * Autocomplete search on device
   * @param data 
   */
  searchDevice(data) {
    if (typeof data === 'object') {
      this.device_id = data._id;
    }
    else {
      this.device_id = '';
    }
  }

  displayFnDevice(data): string {
    return data ? data.unique_device_id : data;
  }


  /**
     * Get sync trip for dropdown
     *
     */
  public getSyncTrips() {

    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._syncTripService.getSyncTrips(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.syncData = data.getAllTrips;
      }
    })

  }

  /**
   * Get dispatcher for dropdown
   *
   */
  public getDispatcher() {
    var params = {
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._customerService.getDispatchersForDropDown(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.customersData = data;
      }
    })
  }
  public getZones() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._zoneService.getZones(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.zones = data.zones
      }
    });
  }
  /**
   * To get customer records in the search box
   */
  public getCustomersWithImages() {
    var that = this;
    that.imageurl = environment.imgUrl;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'asc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    that._customerService.get(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var userdata: any = this.encDecService.dwt(this.session, dec.data);
        this.customerData = userdata;
        that.customerData.forEach(obj => {
          that.dropDownData.push({
            text: obj.firstname,
            value: obj._id,
            description: '<span>' + obj.lastname + '</span><span>' + obj.phone_number + '</span>',
            selected: false,
            // imageSrc: this.imageurl + '/uploads/drivers/' + obj.profile_picture
          });
        });
        $('#customer').ddslick({
          data: that.dropDownData,
          width: 143,
          selectText: "Customer",
          imagePosition: "left",
          truncateDescription: true,
          background: "#fff",
          showSelectedHTML: false,
          onSelected: function (selectedData) {
            that.user_id = selectedData.selectedData.value;
          }
        });
      }
    });
  }

  /**
   * to fetch the order which have shift id
   */
  public getOrdersByShift() {
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      unique_shift_id: this._id,
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      current_order_status: this.current_order_status ? this.current_order_status : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    //console.log(params)
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftsService.getOrdersData(enc_data).then((dec) => {
      //console.log(data)
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.orderData = data.getOrders;
        this.order_detail_data = data.order_detail_data;
        this.payment_extra = data.payment_extra;
        this.orderLength = data.totalCount;
      }
    });
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == 'Order - Edit') {
            this.aclCheck = true;
          } else if (data.menu[i] == 'Downloads-Order Management') {
            this.downloads = true;
          } else if (data.menu[i] == 'Transaction log-Order Managment') {
            this.transaction_log = true;
          } else if (data.menu[i] == 'Orders-Revenue') {
            this.revenueLog = true;
          }
        };
      }
    });
  }

  public orderDownloadPdf(order: any) {

    //order.driver_id.name = (order.driver_id.name!=null && order.driver_id.name!=undefined)?order.driver_id.name:'';
    var order_type = 'Paid Ride';
    let labels = [
      'Label1',
      'Value1',
      'Label2',
      'Value2'
    ];
    let resArray = [];


    resArray.push({
      label1: 'ID',
      value1: order.unique_order_id,
      label2: 'Driver Name',
      value2: order.driver_id.name
    });

    resArray.push({
      label1: 'Order Type',
      value1: 'Paid Trip',
      label2: '',
      value2: ''
    });


    resArray.push({
      label1: 'Pickup Location',
      value1: order.pickup_location,
      label2: 'Start Time',
      value2: order.start_time
    });

    resArray.push({
      label1: 'Drop Location',
      value1: order.drop_location,
      label2: 'End Time',
      value2: order.end_time
    });

    resArray.push({
      label1: 'Payment Type',
      value1: 'Card',
      label2: 'Price',
      value2: order.price + ' AED'
    });

    var pdf = new jsPDF('landscape');

    pdf.setFontSize(20);
    pdf.setFont("times");

    pdf.text(100, 10, 'Order summary');

    var col = [
      { title: "Field1", dataKey: "label1" },
      { title: "Value1", dataKey: "value1" },
      { title: "Field2", dataKey: "label2" },
      { title: "Value2", dataKey: "value2" },
    ];
    var options = {
      styles: { // Defaul style
        lineWidth: 0,
        lineColor: 0,
        fillStyle: 'DF',
        halign: 'left',
        valign: 'middle',
        columnWidth: 'auto',
        columnHeight: 20,
        cellPadding: 10,
        overflow: 'linebreak',
        fontSize: 14
      },
      showHeader: 'never'
    };

    pdf.autoTable(col, resArray, options);

    pdf.addPage();

    this.toDataURL('https://maps.google.com/maps/api/staticmap?center=' + order.drop_location + '&zoom=9&size=640x300&key=' + environment.map_key + '&maptype=roadmap&markers=color:blue%7Clabel:S%7C' + order.customer_pickup_lat_long + '&markers=color:green%7Clabel:G%7C' + order.customer_drop_lat_long, function (dataUrl) {
      pdf.output('datauri');
      pdf.addImage(dataUrl, 'JPEG', 15, 40, 220, 124);
      pdf.save('Invoice_' + moment().format('YYYY-MM-DD_HH_mm_ss') + '.pdf');
    })

  }

  toDataURL(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      var reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }

  htmlToPdf(order: any, tableid: any) {
    var data = document.getElementById(tableid);
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('html-pdf.pdf'); // Generated PDF   
    });
  }

  newPdf(order) {
    let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
    var position = 0;
    var payment_type = '';
    if (order.payment_type != undefined && order.payment_type != '') {

      if (order.payment_type == 1) payment_type = 'Cash';
      if (order.payment_type == 2) payment_type = 'Credit';
      if (order.payment_type == 3) payment_type = 'Account';
      if (order.payment_type == 4) payment_type = 'Online';
    }

    this.toDataURL('assets/mobility/images/invoice_DTC_blank.jpg', function (dataUrl) {
      pdf.output('datauri');
      pdf.addImage(dataUrl, 'JPEG', 0, 0, 210, 300);

      pdf.setFontSize(14);
      pdf.setFont("times");

      pdf.text(13, 83, '' + order.unique_order_id + '');
      pdf.text(141, 83, typeof order.generate_by !== 'string' ? '' : order.generate_by.charAt(0).toUpperCase() + order.generate_by.slice(1));
      pdf.text(13, 103, order.driver_id.name + '(' + order.driver_id.username + ')');
      var pickup = order.pickup_location.split('-')
      pdf.text(13, 125, pickup[0].replace(/[^\x00-\x7F]/g, ""));
      if (pickup[1])
        pdf.text(12, 130, pickup[1].replace(/[^\x00-\x7F]/g, "") + '-' + pickup[1].replace(/[^\x00-\x7F]/g, ""));
      var drop = order.drop_location
      pdf.text(13, 147, drop)
      // if (drop[1])
      //   pdf.text(12, 152, drop[1].replace(/[^\x00-\x7F]/g, "") + '-' + drop[2].replace(/[^\x00-\x7F]/g, ""));
      pdf.text(13, 170, order.start_time);
      pdf.text(141, 170, order.end_time);
      pdf.text(13, 198, payment_type);
      pdf.setTextColor(255, 255, 255);
      pdf.text(141, 198, Number.parseFloat(order.price).toFixed(2));


      pdf.save('Invoice_' +order.unique_order_id+'_'+ moment().format('YYYY-MM-DD_HH_mm_ss') + '.pdf');
    });
  }
  scroll() {
    window.scrollTo(0, document.getElementById('google').offsetTop);
  }
  public getAllVehicleModels() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: event,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleModels(enc_data).then((dec) => {
      var data: any = this.encDecService.dwt(this.session, dec.data);
      this.vehicleModels = data.vehicleModelsList;
    });
  }
  customerDetails(data) {
    this.router.navigate(['/admin/customer/detail', data._id]);
  }
  removeUnderscore(data) {
    return data.replace(/_/g, " ");
  }
  getTransactionLog(id, flag) {
    const params = {
      order_id: id,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getTransactionHistory(enc_data).then(dec => {
      if (dec && dec.status == 200) {
        this.dialog.closeAll();
        let orderData: any = this.encDecService.dwt(this.session, dec.data);
        orderData.flag = flag;
        let dialogRef = this.dialog.open(TransactionLogComponent, {
          width: "600px",
          data: orderData,
          scrollStrategy: this.overlay.scrollStrategies.noop(),
          hasBackdrop: true
        });
      }
    });
  }
  retryPayment(id, index) {
    let that = this;
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to retry payment for the order', type: 'confirm' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != true) {
      } else {
        var params = {
          order_id: id,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        that._orderService.retryPayment(enc_data).then(dec => {
          if (dec && dec.status == 200) {
            that.orderData[index].transaction_status = "0";
            that.toastr.success("Payment collected successfully")
          } else if (dec) {
            that.toastr.error(dec.message);
          }
        });
      }
    })
  }

  public generated_by = "";
  public statusColor(data) {
    if (this.generated_by == data) data = "rgb(191, 0, 0)";
    else data = "#211c47";
    return data;
  }
  public colorFilter(status) {
    this.generated_by = status;
    this.getOrders();
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
          if (this.dtc && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
            this.companyId = data.getCompanies.map(x => x._id)
            this.companyIdFilter = data.getCompanies.map(x => x._id)
            this.companyIdFilter.push('all')
          }
          this.getOrders();
        }
      }
    });
  }
  changeCompany() {
    this.tariffData = [];
    this.ordersFilter = [];
    this.allOrderIddata = [];
    this.partnersData = [];
    this.customerData = [];
    this.paymentData = [];
    this.serviceGroupData = [];
    this.driverData = [];
    this.driverGroupData = [];
    this.vehichleSideData = [];
    this.vehichlePlateData = [];
    this.deviceData = [];
    this.paymentData = [];
    this.vehicleCateg = [];
    this.vehicleCategory = [];
    this.vehicleTypeFilterOrder = '';
    this.getAllVehicleModels();
    this.getVehicleCategory();
    this.cancelReasons = [];
    this.cancelReasonId = [];
    //this.getOrders();
  }
  selectAllCompany() {
    if (this.companyId.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.companyId = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.companyId = [];
      this.companyIdFilter = [];
    }
  }
  async getDriversInDriverGroup(id) {
    const params = {
      group_id: id,
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    return this._driverService.driversInDriverGroup(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        if (data.Drivers.length > 0)
          return data.Drivers.map(x => x._id);
        else
          return [];
      }
      else {
        return [];
      }
    });
  }
  getVehicleCategory() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var result: any = this.encDecService.dwt(this.session, dec.data);
        this.vehicleCateg = result.vehicleModelsList;
      }
    });
    //this.allOrderIddata=[100041116,100041169,100041168,250493,112334];

  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.companyId = this.companyIdFilter.slice()
    }
    else {
      this.companyId = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }
  parse(data) {
    if (parseInt(data) > 0)
      return parseInt(data);
    else
      return 0;
  }
  viewTripComment(order, index) {
    if (order.generate_by !== 'customer' || order.order_status !== 'completed')
      return;
    else {
      const params = {
        order_id: order._id
      }
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this.orderData[index].customer_comment='Loading...';
      this._customerService.getOrderComments(enc_data).then((dec) => {
      this.orderData[index].customer_comment='';
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.orderData[index].customer_comment=result.data && result.data.comments?result.data.comments:'';
        }
      });
    }
  }
}	