import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { OrderService } from '../../../common/services/order/order.service';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import * as moment from 'moment/moment';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { Angular2Csv } from 'angular2-csv';
import { DriverService } from '../../../common/services/driver/driver.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { VehicleService } from '../../../common/services/vehicles/vehicle.service';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';

@Component({
  selector: 'app-flagged-trip-speed',
  templateUrl: './flagged-trip-speed.component.html',
  styleUrls: ['./flagged-trip-speed.component.css']
})
export class FlaggedTripSpeedComponent implements OnInit {
  orderCtrl: FormControl = new FormControl();
  driverCtrl: FormControl = new FormControl();
  vehicleCtrl: FormControl = new FormControl();
  allOrderIddata: any;
  ordersFilter: any = [];
  driversFilter: any = [];
  vehicleFilter: any = [];
  unique_order_id: any = [];
  public selectedMoment;
  public selectedMoment1;
  public max = new Date();
  generated_by: any = 'customer';
  searchLoader: boolean = false;
  orderData: any[];
  pageSize = 10;
  pageNo = 0;
  orderLength: any;
  public searchSubmit = false;
  session: string;
  email: string;
  companyId: any = [];
  vehicleList: any = [];
  downloads: boolean;

  constructor(private _orderService: OrderService, public toastr: ToastsManager, public jwtService: JwtService, private router: Router,
    private _driverService: DriverService,
    public encDecService: EncDecService,
    private _vehichleService: VehicleService,
    private _aclService: AccessControlService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    var params = {
      company_id: this.companyId,
      limit: 10
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.searchForDriver(enc_data)
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.driverList = result.driver.map((e) => ({ id: e._id, text: e.username.toString() }));
        }
      });
    this.vehicleCtrl.valueChanges
      .debounceTime(500).switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          'searchsidenumber': query,
          company_id: this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._vehichleService.getVehicleListingAdmin(enc_data)
      })
      .subscribe((dec) => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.vehicleList = result.result;
        }
      })
  }

  ngOnInit() {
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
    this.orderCtrl.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          company_id: this.companyId,
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          search_keyword: query
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._orderService.getAllOrderId(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var result: any = this.encDecService.dwt(this.session, dec.data);
          this.allOrderIddata = result.getOrders;
        }
      });
    this.driverCtrl.valueChanges
      .debounceTime(400)
      .switchMap((query) => {
        var params = {
          company_id: this.companyId,
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          search_keyword: query
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data)
      })
      .subscribe(dec => {
        if (dec && dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          this.driverList = res.driver;
        }
      });
    this.getOrders();
    this.aclDisplayService();
  }
  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == 'Downloads-Flagged-Trip-Speed') {
            this.downloads = true;
          }
        };
      }
    });
  }

  removeOrder(fruit): void {
    const index = this.ordersFilter.indexOf(fruit);
    if (index >= 0) {
      this.ordersFilter.splice(index, 1);
      this.unique_order_id.splice(index, 1)
    }
  }
  orderIdselected(event: MatAutocompleteSelectedEvent): void {
    if (this.ordersFilter.indexOf(event.option.value.unique_order_id) > -1) {
      return
    }
    else {
      this.ordersFilter.push(event.option.value.unique_order_id);
      this.unique_order_id.push(event.option.value.unique_order_id);
      this.orderCtrl.setValue(null);
    }
  }
  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }
  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }
  checkShiftstartTime() {
    this.selectedMoment1 = '';
  }
  public statusColor(data) {
    if (this.generated_by == data) data = "rgb(191, 0, 0)";
    else data = "#211c47";
    return data;
  }
  public colorFilter(status) {
    this.generated_by = status;
    this.getOrders();
  }
  public getOrders() {
    this.searchLoader = true;
    this.orderData = [];
    this.pageNo = 1;
    let params;
    if (this.unique_order_id && this.unique_order_id.length > 0) {
      params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        unique_order_id: this.unique_order_id ? this.unique_order_id : '',
        startDate: '',
        endDate: '',
        generate_by: '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: 0,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        unique_order_id: this.unique_order_id ? this.unique_order_id : '',
        startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
        endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
        generate_by: this.generated_by ? this.generated_by : '',
        driver_id: this.driverSelectedList,
        vehicle_side: this.vehicleSelectedList,
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getOverSpeedTrips(enc_data).then((data) => {
      if (data && data.status == 200) {
        var data: any = this.encDecService.dwt(this.session, data.data);
        this.orderData = data.data;
        this.orderLength = data.count;
      }
      this.searchLoader = false;
    });
  }
  public creatingcsv = false;
  public getconstantsforcsv() {
    if (this.creatingcsv) {
      this.toastr.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      var params = { company_id: this.companyId };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._orderService.getRejectedCSVConstants(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var res: any = this.encDecService.dwt(this.session, dec.data);
            let offset = res.data;
            this.createCsv(offset.order_fetch_interval, offset.order_fetch_offset);
          } else {
            this.creatingcsv = false
            this.toastr.error("Please try again")
          }
        }
      })
    }
  }
  public livetrackingOrder;
  public progressvalue = 0;
  public createCsv(interval_value, offset) {
    let params = {
      offset: 0,
      limit: 1,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      unique_order_id: this.unique_order_id ? this.unique_order_id : '',
      startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      generate_by: this.generated_by ? this.generated_by : '',
      driver_id: this.driverSelectedList,
      vehicle_side: this.vehicleSelectedList,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getOverSpeedTrips(enc_data).then((res) => {
      if (res && res.status == 201) {
        this.creatingcsv = false;
        this.toastr.error('Please try again after some time');
      } else if (res && res.status == 200) {
        var res: any = this.encDecService.dwt(this.session, res.data);
        var count = res.count;
        let res1Array = [];
        //alert("total_orders" + count);
        let i = 0;
        let fetch_status = true;
        let that = this;
        this.livetrackingOrder = setInterval(function () {
          if (fetch_status) {
            if (res1Array.length >= count) {
              that.progressvalue = 100;
              that.creatingcsv = false;
              clearInterval(that.livetrackingOrder);
              let labels = [
                'ID',
                'Source',
                //'Start',
                'Destination',
                //'Stop',
                'Distance (KM)',
                'Driver',
                'Vehicle',
                'Order-type',
                'Status',
                'Total (AED)',
                'Speed (KM)'
              ];
              var options =
              {
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: true,
                showTitle: false,
                useBom: true,
                headers: (labels)
              };
              new Angular2Csv(res1Array, 'Flagged Trip speeds-Order' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
            } else {
              fetch_status = false;
              let diff = count - i;
              let perc = diff / count;
              let remaining = 100 * perc;
              that.progressvalue = 100 - remaining;
              let new_params = {
                offset: i,
                limit: parseInt(offset),
                sortOrder: 'desc',
                sortByColumn: 'updated_at',
                unique_order_id: that.unique_order_id ? that.unique_order_id : '',
                startDate: that.selectedMoment ? moment(that.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
                endDate: that.selectedMoment1 ? moment(that.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
                generate_by: that.generated_by ? that.generated_by : '',
                driver_id: that.driverSelectedList,
                vehicle_side: that.vehicleSelectedList,
                company_id: that.companyId
              };
              var encrypted = that.encDecService.nwt(that.session, new_params);
              var enc_data = {
                data: encrypted,
                email: that.email
              }
              that._orderService.getOverSpeedTrips(enc_data).then((res) => {
                if (res && res.status == 200) {
                  var data: any = that.encDecService.dwt(that.session, res.data);
                  res = data.data;
                  if (res.length > 0) {
                    for (let j = 0; j < res.length; j++) {
                      const csvArray = res[j];
                      res1Array.push({
                        id: csvArray.unique_order_id ? csvArray.unique_order_id : 'N/A',
                        source: csvArray.orderDetails && csvArray.orderDetails.pickup_location ? csvArray.orderDetails.pickup_location : 'N/A',
                        //start: csvArray.orderDetails && csvArray.sync_trip_id != null ? csvArray.sync_trip_id.start_trip_calendar ? csvArray.sync_trip_id.start_trip_calendar : 'NA' : 'NA',
                        destination: csvArray.orderDetails && csvArray.orderDetails.drop_location ? csvArray.orderDetails.drop_location : 'N/A',
                        //Stop: csvArray.orderDetails && csvArray.sync_trip_id != null ? csvArray.sync_trip_id.finished_trip_calendar ? csvArray.sync_trip_id.finished_trip_calendar : 'NA' : 'NA',
                        distance: csvArray.orderDetails && csvArray.orderDetails.distance ? csvArray.orderDetails.distance : 'N/A',
                        driver: csvArray.orderDetails && csvArray.driver_emp ? csvArray.driver_emp : 'N/A',
                        vehicle: csvArray.vehicle_side ? csvArray.vehicle_side : 'N/A ',
                        order_type: csvArray.orderDetails && csvArray.orderDetails.generate_by ? csvArray.orderDetails.generate_by : 'N/A',
                        status: csvArray.orderDetails && csvArray.orderDetails.order_status ? csvArray.orderDetails.order_status : 'N/A',
                        total: csvArray.orderDetails && csvArray.orderDetails.price ? csvArray.orderDetails.price : '0 AED',
                        speed: csvArray.speed ? csvArray.speed : 'NA'
                      });
                      if (j == res.length - 1) {
                        i = i + parseInt(offset);
                        fetch_status = true;
                      }
                    }
                  }
                }
              }).catch((Error) => {
                console.log(Error);
                that.toastr.warning('Network reset, csv creation restarted')
                that.createCsv(interval_value, offset);
              })
            }
          }
        }, parseInt(interval_value));
      }
    })
  }
  reset() {
    this.pageNo = 1;
    this.unique_order_id = [];
    this.ordersFilter = [];
    this.generated_by = "customer";
    this.ngOnInit();
    this.searchSubmit = false;
  }
  displayFnOrderId(data): string {
    return data ? '' : '';
  }
  pagingAgent(data) {
    this.searchLoader = true;
    this.orderData = [];
    //this.orderData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      unique_order_id: this.unique_order_id ? this.unique_order_id : '',
      startDate: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      generate_by: this.generated_by ? this.generated_by : '',
      driver_id: this.driverSelectedList,
      company_id: this.companyId,
      vehicle_side: this.vehicleSelectedList
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._orderService.getOverSpeedTrips(enc_data).then((dec) => {
      // this.searchSubmit = true;
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.orderData = data.data;
          this.orderLength = data.count;
        }
        this.searchLoader = false;
        this.searchSubmit = false;
      }
    });
  }
  public driverList: Array<string> = [];
  public driverSelectedList: Array<string> = [];
  public driverIdselected(value: any): void {
    if (this.driversFilter.indexOf(value.option.value) > -1) {
      return
    }
    else {
      this.driversFilter.push(value.option.value);
      this.driverSelectedList.push(value.option.value._id);
      this.driverCtrl.setValue(null);
    }
  }
  public removeDriver(fruit: any): void {
    const index = this.driversFilter.indexOf(fruit);
    if (index >= 0) {
      this.driversFilter.splice(index, 1);
      this.driverSelectedList.splice(index, 1)
    }
  }
  displayFnDriver(data): string {
    return data ? data.username : data;
  }
  public vehicleSelectedList: Array<string> = [];
  public vehicleIdselected(value: any): void {
    if (this.vehicleFilter.indexOf(value.option.value) > -1) {
      return
    }
    else {
      this.vehicleFilter.push(value.option.value);
      this.vehicleSelectedList.push(value.option.value.vehicle_identity_number);
      this.vehicleCtrl.setValue(null);
    }
  }
  public removeVehicle(fruit: any): void {
    const index = this.vehicleFilter.indexOf(fruit);
    if (index >= 0) {
      this.vehicleFilter.splice(index, 1);
      this.vehicleSelectedList.splice(index, 1)
    }
  }
  displayFnVehicle(data): string {
    return data ? data.vehicle_identity_number : data;
  }
}
