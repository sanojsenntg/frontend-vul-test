import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlggedTripSpeedComponent } from './flgged-trip-speed.component';

describe('FlggedTripSpeedComponent', () => {
  let component: FlggedTripSpeedComponent;
  let fixture: ComponentFixture<FlggedTripSpeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlggedTripSpeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlggedTripSpeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
