import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order.component';
import { ListOrderComponent } from './list-order/list-order.component';
import { OrderService } from './../../common/services/order/order.service';
import { OrderRestService } from './../../common/services/order/orderrest.service';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTooltipModule, } from '@angular/material/tooltip';
import { ProgressBarModule } from "angular-progress-bar"
import { MatInputModule, MatNativeDateModule, MatIconModule, MatChipsModule, MatSelectModule } from '@angular/material';
import { TransactionLogComponent } from '../../common/dialog/transaction-log/transaction-log.component';
import { OrderDispHistoryComponent } from './order-disp-history/order-disp-history.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { FlaggedOrdersComponent } from './flagged-orders/flagged-orders.component';
import { FlaggedTripSpeedComponent } from './flagged-trip-speed/flagged-trip-speed.component';
import {SelectModule} from 'ng2-select';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule,
    MatChipsModule,
    MatIconModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    ReactiveFormsModule,
    ProgressBarModule,
    SelectModule
  ],
  declarations: [
    ListOrderComponent,
    OrderComponent,
    EditOrderComponent,
    OrderDispHistoryComponent,
    FlaggedOrdersComponent,
    FlaggedTripSpeedComponent,
  ],
  exports: [
    RouterModule
  ],
  providers: [OrderService, OrderRestService],
  entryComponents: [TransactionLogComponent]
})
export class OrderModule { }
