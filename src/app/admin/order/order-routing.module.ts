import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { OrderComponent } from './order.component';
import { ListOrderComponent } from './list-order/list-order.component';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { OrderDispHistoryComponent } from './order-disp-history/order-disp-history.component';
import { FlaggedOrdersComponent } from './flagged-orders/flagged-orders.component';
import { FlaggedTripSpeedComponent } from './flagged-trip-speed/flagged-trip-speed.component';
import { AclAuthervice } from '../../common/services/access-control/acl-auth.service'


export const orderRoutes: Routes = [
  {
    path: '',
    component: OrderComponent
  },
  {
    path: 'list-order',
    component: ListOrderComponent, canActivate: [AclAuthervice], data: {
      roles: ['Order - List']
    }
  },
  {
    path: 'disp-history',
    component: OrderDispHistoryComponent, canActivate: [AclAuthervice], data: {
      roles: ['Order - Dispatch-history']
    }
  },
  {
    path: 'edit-order/:id', component: EditOrderComponent, canActivate: [AclAuthervice], data: { roles: ['Order - Edit'] }
  },
  {
    path: 'shifts/:unique_shift_id',
    component: ListOrderComponent
  },
  {
    path: 'order/:order_id',
    component: ListOrderComponent
  },
  {
    path: 'flagged-orders',
    component: FlaggedOrdersComponent, canActivate: [AclAuthervice], data: {
      roles: ['Order - flagged-orders']
    }
  },
  {
    path: 'flagged-trip-speed',
    component: FlaggedTripSpeedComponent, canActivate: [AclAuthervice], data: {
      roles: ['Order - flagged-trip-speed']
    }
  }
];
