/**
 *  Imports
 */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ShiftTimingsService } from '../../../../common/services/shift-timings/shift-timings.service';
import { ToastsManager } from 'ng2-toastr';
import { AmazingTimePickerService } from 'amazing-time-picker';
import * as moment from 'moment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

/**
 * the component decorator  (which defines the selector, template and style location)
 */

@Component({
  selector: 'app-add-shift-timing',
  templateUrl: './add-shift-timing.component.html',
  styleUrls: ['./add-shift-timing.component.css']
})

/**
 * Component Class
 */
export class AddShiftTimingComponent implements OnInit {
  public companyId: any = [];
  public shiftTiming: any = {
    name: '',
    start_time: '',
    end_time: '',
    break_start: '',
    break_end: '',
    free_hours: '',
    company_id: this.companyId
  };
  public shiftendError;
  public showError = false;
  public BreakstartError;
  public BreakEndError;
  session: string;
  email: string;

  /**
   * constructor is initialised by the JavaScript engine
   * @param {ShiftTimingsService} _shiftTimingsService
   * @param {ToastsManager} toastr
   * @param {Router} router
   * @param {ViewContainerRef} vcr
   * @param {AmazingTimePickerService} atp
   */
  constructor(private _shiftTimingsService: ShiftTimingsService,
    public toastr: ToastsManager,
    public jwtService: JwtService,
    private router: Router,
    vcr: ViewContainerRef,
    public encDecService: EncDecService,
    private atp: AmazingTimePickerService
  ) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }

  /**
    *   calls ngOnInit after creating a component
    */
  ngOnInit() {
  }

  /**
   *  start time timepicker
   */
  checkShiftstartTime() {
    const ShiftstartTimePicker = this.atp.open();
    ShiftstartTimePicker.afterClose().subscribe(time => {
      this.shiftTiming.start_time = time;
      this.shiftTiming.end_time = '';
      this.shiftTiming.break_start = '';
      this.shiftTiming.break_end = '';
    });
  }

  /**
  * end time timepicker
  */
  checkShiftendTime() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      this.shiftTiming.end_time = time;
      this.shiftTiming.break_start = '';
      this.shiftTiming.break_end = '';
      let diff = moment.utc(moment(this.shiftTiming.end_time, 'HH:mm:ss').diff(moment(this.shiftTiming.start_time, 'HH:mm:ss'))).format('HH')
      if (diff > '12') {
        this.shiftendError = 'The shift should not exceed 12 hours.';
        this.showError = true;
      } else {
        this.shiftendError = '';
        this.showError = false;
      }
    });
  }

  /**
  * break time timepicker
  */

  checkBreakStart() {
    const BreakStartPicker = this.atp.open();
    BreakStartPicker.afterClose().subscribe(time => {
      this.shiftTiming.break_start = time;
      this.shiftTiming.break_end = '';
      const end_time = moment(this.shiftTiming.end_time, 'HH:mm:ss');
      const start_time = moment(this.shiftTiming.start_time, 'HH:mm:ss');
      const break_start = moment(this.shiftTiming.break_start, 'HH:mm:ss');
      let new_break_time = break_start;
      let new_end_time = end_time;

      if (end_time < start_time) {
        new_end_time = moment(end_time, "HH:mm:ss").add(1, 'days');
        if (break_start < start_time) {
          new_break_time = moment(break_start, "HH:mm:ss").add(1, 'days');
        }
      }
      if (new_break_time > start_time && new_break_time < new_end_time) {
        this.BreakstartError = '';
        this.showError = false;
      } else {
        this.BreakstartError = 'The break start time should be in between start time and end time of shift.';
        this.showError = true;
      }


    });
  }

  /**
  * break end timepicker
  */
  checkBreakEnd() {
    const BreakEndPicker = this.atp.open();
    BreakEndPicker.afterClose().subscribe(time => {
      this.shiftTiming.break_end = time;
      let end_time = moment(this.shiftTiming.end_time, 'HH:mm:ss');
      let start_time = moment(this.shiftTiming.start_time, 'HH:mm:ss');
      let break_start = moment(this.shiftTiming.break_start, 'HH:mm:ss');
      let break_end = moment(this.shiftTiming.break_end, 'HH:mm:ss');
      let new_break_time = break_start;
      let new_breakend_time = break_end;
      let new_end_time = end_time;
      if (end_time < start_time) {
        new_end_time = moment(end_time, "HH:mm:ss").add(1, 'days');
        if (break_start < start_time) {
          new_break_time = moment(break_start, "HH:mm:ss").add(1, 'days');
        }
        if (break_end < start_time) {
          new_breakend_time = moment(break_end, "HH:mm:ss").add(1, 'days');
        }
      }
      if (new_breakend_time > start_time && new_breakend_time < new_end_time && new_breakend_time > new_break_time) {
        this.BreakEndError = '';
        this.showError = false;
      } else {
        this.BreakEndError = 'The break end time should be greater than break start time and in between start time and end time of shift.';
        this.showError = true;
      }
    });

  }

  /**
   * Save Shift Timings
   * @param formData
   */
  onSubmit(formData): void {
    if (formData.valid === true) {
      const end_time = moment(this.shiftTiming.end_time, 'HH:mm:ss');
      const start_time = moment(this.shiftTiming.start_time, 'HH:mm:ss');
      const break_start = moment(this.shiftTiming.break_start, 'HH:mm:ss');
      const break_end = moment(this.shiftTiming.break_end, 'HH:mm:ss');
      let new_break_time = break_start;
      let new_end_time = end_time;

      let new_breakend_time = break_end;
      if (end_time < start_time) {
        new_end_time = moment(end_time, "HH:mm:ss").add(1, 'days');
        if (break_start < start_time) {
          new_break_time = moment(break_start, "HH:mm:ss").add(1, 'days');
        }
        if (break_end < start_time) {
          new_breakend_time = moment(break_end, "HH:mm:ss").add(1, 'days');
        }
      }
      if (new_break_time > start_time && new_break_time < new_end_time) {
        if (new_breakend_time > start_time && new_breakend_time < new_end_time && new_breakend_time > new_break_time) {

          var encrypted = this.encDecService.nwt(this.session, this.shiftTiming);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this._shiftTimingsService.saveShiftTiming(enc_data)
            .then((dec) => {
              if (dec) {
                if (dec.status === 200) {
                  this.toastr.success('Shift timings added successfully.');
                  this.router.navigate(['/admin/shift/shift-timings']);
                } else if (dec.status === 201) {
                  this.toastr.error('Shift timing addition failed.');
                } else if (dec.status === 203) {
                  this.toastr.warning('Shift name already exist.');
                }
              }
            });
        } else {
          this.shiftTiming.new_break_time = '';
          this.shiftTiming.new_breakend_time = '';
        }
      } else {
        this.shiftTiming.new_break_time = '';
        this.shiftTiming.new_breakend_time = '';
      }
    } else {
      this.toastr.error('All fields are required.');
      return;
    }
  }
}
