/**
 * imports
 */
import { Component, OnInit } from '@angular/core';
import { ShiftTimingsService } from '../../../../common/services/shift-timings/shift-timings.service';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { MatDialog } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';
import { Overlay } from '@angular/cdk/overlay';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
/**
 *  the component decorator  (which defines the selector, template and style location)
 */
@Component({
  selector: 'app-list-shift-timing',
  templateUrl: './list-shift-timing.component.html',
  styleUrls: ['./list-shift-timing.component.css']
})
export class ListShiftTimingComponent implements OnInit {
  public companyId: any = [];
  public shiftData;
  public shiftlength;
  public is_search = false;
  public ShowLoader;
  public pageSize = 10;
  public pageNo = 0;
  public sortOrder = 'desc';
  public key;
  public name;
  public del;
  email: string;
  session: string;

  /**constructor is initialised by the JavaScript engine
   * @param {ShiftTimingsService} _shiftTimingsService
   * @param {Overlay} overlay
   * @param {MatDialog} dialog
   * @param {ToastsManager} _toasterService
   */
  constructor(
    public toastr: ToastsManager,
    public jwtService: JwtService,
    public encDecService: EncDecService,
    private router: Router,
    private _shiftTimingsService: ShiftTimingsService,
    public overlay: Overlay,
    public dialog: MatDialog,
    private _toasterService: ToastsManager) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }

  /**
   *   calls ngOnInit after creating a component
   */
  ngOnInit() {
    this.is_search = false;
    const params = {
      offset: this.pageNo,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    this.ShowLoader = true;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftTimingsService.getShiftTiming(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.shiftData = data.shift_timings;
          this.shiftlength = data.count;
        }
      }
      this.ShowLoader = false;
    });
  }

  /**
  * To refresh shift timings records showing in the grid
  */
  public refetchShiftTiming() {
    if (
      this.is_search == true) {
      this.getShiftTiming();
    }
    else {
      this.shiftData = [];
      this.ngOnInit();
    }
  }

  /**
  * To reset the search filters and reload the shift timing records
  */
  public reset() {
    this.name = '';
    this.ngOnInit();
  }

  /**
   * Sorting on the base of column
   * @param key
   */
  sort(key) {
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }
    this.shiftData = [];
    let params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        name: this.name ? this.name : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        company_id: this.companyId
      };
    }
    this.shiftData = [];
    this.del = true;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftTimingsService.getShiftTiming(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.shiftData = data.shift_timings;
          this.shiftlength = data.count;
        }
      }
      this.ShowLoader = false;
      this.del = false;
    });
  }

  /**
   * To search the particular shift timings
   */
  public getShiftTiming() {
    this.ShowLoader = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      name: this.name ? this.name : '',
      company_id: this.companyId
    };
    this.shiftData = [];
    this.del = true;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftTimingsService.getShiftTiming(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.shiftData = data.shift_timings;
          this.shiftlength = data.count;
        }
      }
      this.ShowLoader = false;
      this.del = false;
      this.is_search = true;
    });
  }

  /**
   * Pagination for shift timing module
   * @param data
   */
  pagingAgent(data) {
    this.ShowLoader = true;
    this.shiftData = [];
    this.pageNo = (data * this.pageSize) - this.pageSize;
    let params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        name: this.name ? this.name : '',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        company_id: this.companyId
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftTimingsService.getShiftTiming(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.shiftData = data.shift_timings;
          this.shiftlength = data.count;
        }
      }
      this.ShowLoader = false;
    });
  }

  /**
   * Delete shift timings
   * @param id
   * @param index
   */
  deleteDialog(id, index): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        var params = {
          company_id: this.companyId,
          _id: id
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._shiftTimingsService.deleteShiftTimings(enc_data).then((dec) => {
          if (dec) {
            if (dec.status === 200) {
              var data: any = this.encDecService.dwt(this.session, dec.data);
              this.shiftData.splice(index, 1);
              this._toasterService.success('Shift timings deleted successfully.');
            } else if (dec.status === 201) {
            } else if (dec.status === 203) {
              this._toasterService.warning('This shift timing is assign to a driver, so you cannot delete it.');
            }
          }
        });
      } else {
      }
    });
  }
}
