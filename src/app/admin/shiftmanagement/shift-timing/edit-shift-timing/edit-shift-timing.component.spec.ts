import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditShiftTimingComponent } from './edit-shift-timing.component';

describe('EditShiftTimingComponent', () => {
  let component: EditShiftTimingComponent;
  let fixture: ComponentFixture<EditShiftTimingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditShiftTimingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditShiftTimingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
