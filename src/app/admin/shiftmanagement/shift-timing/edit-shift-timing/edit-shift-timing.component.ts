/**
 * imports */
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ShiftTimingsService } from '../../../../common/services/shift-timings/shift-timings.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { AmazingTimePickerService } from 'amazing-time-picker';
import * as moment from 'moment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

/**
 * the component decorator  (which defines the selector, template and style location)
 */
@Component({
  selector: 'app-edit-shift-timing',
  templateUrl: './edit-shift-timing.component.html',
  styleUrls: ['./edit-shift-timing.component.css']
})

/**
 * Component Class
 */
export class EditShiftTimingComponent implements OnInit {
  public _id;
  public companyId: any = [];
  public shift_timings;
  public shiftTiming: any = {
    name: '',
    start_time: '',
    end_time: '',
    break_start: '',
    break_end: '',
    free_hours: '',
    company_id: this.companyId
  };
  public shiftendError;
  public showError = false;
  public BreakstartError;
  public BreakEndError;
  email: string;
  session: string;

  /**
   * constructor is initialised by the JavaScript engine
   * @param {ActivatedRoute} route
   * @param {ShiftTimingsService} _shiftTimingsService
   * @param {ToastsManager} toastr
   * @param {Router} router
   * @param {ViewContainerRef} vcr
   * @param {AmazingTimePickerService} atp
   */
  constructor(
    private route: ActivatedRoute,
    private _shiftTimingsService: ShiftTimingsService,
    public toastr: ToastsManager,
    public jwtService: JwtService,
    private router: Router,
    vcr: ViewContainerRef,
    private atp: AmazingTimePickerService,
    public encDecService: EncDecService
  ) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }

  /**
   *   calls ngOnInit after creating a component
   */
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this._id = params['id'];
      let data = {
        '_id': this._id,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, data);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftTimingsService.getShiftTimingById(enc_data).then((dec) => {
        if (dec) {
          if (dec.status = '200') {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.shift_timings = data.shift_timings;
            this.shiftTiming = {
              name: data.shift_timings.name,
              start_time: data.shift_timings.start_time,
              end_time: data.shift_timings.end_time,
              break_start: data.shift_timings.break_start,
              break_end: data.shift_timings.break_end,
              free_hours: data.shift_timings.free_hours
            };
          }
        }
      });
    });

  }

  /**
   *  start time timepicker
   */
  checkShiftstartTime() {
    const ShiftstartTimePicker = this.atp.open();
    ShiftstartTimePicker.afterClose().subscribe(time => {
      this.shiftTiming.start_time = time;
      this.shiftTiming.end_time = '';
      this.shiftTiming.break_start = '';
      this.shiftTiming.break_end = '';
    });
  }

  /**
   *  end time timepicker
   */
  checkShiftendTime() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      this.shiftTiming.end_time = time;
      this.shiftTiming.break_start = '';
      this.shiftTiming.break_end = '';
      const diff = moment.utc(moment(this.shiftTiming.end_time, 'HH:mm:ss').diff(moment(this.shiftTiming.start_time, 'HH:mm:ss'))).format('HH');
      if (diff > '12') {
        this.shiftendError = 'The shift should not exceed 12 hours.';
        this.showError = true;

      } else {
        this.shiftendError = '';
        this.showError = false;
      }
    });
  }

  /**
   *  break start timepicker
   */
  checkBreakStart() {
    const BreakStartPicker = this.atp.open();
    BreakStartPicker.afterClose().subscribe(time => {
      this.shiftTiming.break_start = time;
      this.shiftTiming.break_end = '';
      let end_time = moment.utc(moment(this.shiftTiming.end_time, 'HH:mm:ss'));
      let start_time = moment.utc(moment(this.shiftTiming.start_time, 'HH:mm:ss'));
      let break_start = moment.utc(moment(this.shiftTiming.break_start, 'HH:mm:ss'));
      let new_break_time = break_start;
      let new_end_time = end_time;
      if (end_time < start_time) {
        new_end_time = moment(end_time, "HH:mm:ss").add(1, 'days');
        if (break_start < start_time) {
          new_break_time = moment(break_start, "HH:mm:ss").add(1, 'days');
        }
      }
      if (new_break_time > start_time && new_break_time < new_end_time) {
        this.BreakstartError = '';
        this.showError = false;
      } else {
        this.BreakstartError = 'The break start time should be in between start time and end time of shift.';
        this.showError = true;
      }

    });
  }


  /**
   *  break end timepicker
   */
  checkBreakEnd() {
    const BreakEndPicker = this.atp.open();
    BreakEndPicker.afterClose().subscribe(time => {
      this.shiftTiming.break_end = time;
      const end_time = moment.utc(moment(this.shiftTiming.end_time, 'HH:mm:ss'));
      const start_time = moment.utc(moment(this.shiftTiming.start_time, 'HH:mm:ss'));
      const break_start = moment.utc(moment(this.shiftTiming.break_start, 'HH:mm:ss'));
      const break_end = moment.utc(moment(this.shiftTiming.break_end, 'HH:mm:ss'));
      let new_break_time = break_start;
      let new_breakend_time = break_end;
      let new_end_time = end_time;
      if (end_time < start_time) {
        new_end_time = moment(end_time, "HH:mm:ss").add(1, 'days');
        if (break_start < start_time) {
          new_break_time = moment(break_start, "HH:mm:ss").add(1, 'days');
        }
        if (break_end < start_time) {
          new_breakend_time = moment(break_end, "HH:mm:ss").add(1, 'days');
        }
      }
      if (new_breakend_time > start_time && new_breakend_time < new_end_time && new_breakend_time > new_break_time) {
        this.BreakEndError = '';
        this.showError = false;
      } else {
        this.BreakEndError = 'The break end time should be greater than break start time and in between start time and end time of shift.';
        this.showError = true;
      }
    });

  }

  /**
   * update shift timings
   * @param formData
   */
  update(formData): void {
    if (formData.valid === true) {
      const end_time = moment.utc(moment(this.shiftTiming.end_time, 'HH:mm:ss'));
      const start_time = moment.utc(moment(this.shiftTiming.start_time, 'HH:mm:ss'));
      const break_start = moment.utc(moment(this.shiftTiming.break_start, 'HH:mm:ss'));
      const break_end = moment.utc(moment(this.shiftTiming.break_end, 'HH:mm:ss'));
      let new_break_time = break_start;
      let new_end_time = end_time;
      let new_breakend_time = break_end;
      if (end_time < start_time) {
        new_end_time = moment(end_time, "HH:mm:ss").add(1, 'days');
        if (break_start < start_time) {
          new_break_time = moment(break_start, "HH:mm:ss").add(1, 'days');
        }
        if (break_end < start_time) {
          new_breakend_time = moment(break_end, "HH:mm:ss").add(1, 'days');
        }
      }
      if (new_break_time > start_time && new_break_time < new_end_time) {
        if (new_breakend_time > start_time && new_breakend_time < new_end_time && new_breakend_time > new_break_time) {

          this.shiftTiming['_id'] = this._id;
          var encrypted = this.encDecService.nwt(this.session, this.shiftTiming);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this._shiftTimingsService.updateShiftTimings(enc_data)
            .then((dec) => {
              if (dec) {
                if (dec.status === 200) {
                  this.toastr.success('Shift timings updated successfully.');
                  this.router.navigate(['/admin/shift/shift-timings']);
                } else if (dec.status === 201) {
                  this.toastr.error('Shift timings updation failed.');
                } else if (dec.status === 203) {
                  this.toastr.warning('Shift name already exist.');
                }
              }
            }).catch((error) => {
            });
        } else {
          this.shiftTiming.break_start = '';
          this.shiftTiming.break_end = '';
        }
      } else {
        this.shiftTiming.break_start = '';
        this.shiftTiming.break_end = '';
      }
    } else {
      this.toastr.error('All fields are required.');
      return;
    }
  }
}
