import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ShiftsService } from '../../../../common/services/shifts/shifts.service';
import { OrderService } from '../../../../common/services/order/order.service';
import { DriverService } from '../../../../common/services/driver/driver.service';
import { DeviceService } from '../../../../common/services/devices/devices.service';
import { VehicleService } from '../../../../common/services/vehicles/vehicle.service';
import { Router } from '@angular/router';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import * as moment from 'moment/moment';
import { environment } from '../../../../../environments/environment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { ShiftDialogComponent } from '../../../../common/dialog/shift-dialog/shift-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { FormControl } from '@angular/forms';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { GlobalService } from '../../../../common/services/global/global.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../../common/services/companies/companies.service';
declare var jsPDF;

@Component({
  selector: 'app-list-shifts',
  templateUrl: './list-shifts.component.html',
  styleUrls: ['./list-shifts.component.css']
})

export class ListShiftsComponent implements OnInit {
  queryField: FormControl = new FormControl();
  device: FormControl = new FormControl();
  driver_Id: FormControl = new FormControl();
  vehicle_side_no: FormControl = new FormControl();
  vehicle_plate_no: FormControl = new FormControl();
  key: string = '';
  public id;
  public i;
  public itemsPerPage = 10;
  public pageSize = 10;
  public pageNo = 0;
  public driverList;
  public DriverData;
  public uniquedriverid;
  public driverIdData;
  public orderRecords;
  public is_search = false;
  public orderLength;
  public sortOrder = 'asc';
  public searchLoader = false;
  public del = false;
  public driverData;
  public deviceData;
  public ShiftIDData;
  public vehichleSideData;
  public vehichlePlateData;
  public vehichle_plate_no: any;
  public vehichle_side_no: any;
  public approve_status = '';
  public empty_status = '';
  public allShiftsData;
  public shift_type = '';
  public orderData;
  public allShiftsLength;
  public imageurl;
  public selectedMoment: any = '';
  public selectedMoment1: any = '';
  public driver_id: any;
  public shift_id: any;
  public _id;
  private sub: Subscription;
  public device_id = '';
  public max = new Date();
  public max2 = new Date();
  public shift_detail_data: any = [];
  public payment_extra;
  public loadingIndicator;
  pNo = 1;
  public driverIdForm = '';
  public driverForm = '';
  public deviceForm = '';
  public sideForm = '';
  public plateForm = '';
  public imageext;
  public apiUrl;
  public downloads = false;
  public companyId: any = [];
  email: string;
  session: string;
  company_id: string;
  companyData: any;
  dtc: boolean = false;
  companyIdFilter: any;
  constructor(
    private _shiftsService: ShiftsService,
    private _driverService: DriverService,
    private route: ActivatedRoute,
    private _deviceService: DeviceService,
    private _vehichleService: VehicleService,
    private router: Router,
    public overlay: Overlay,
    public dialog: MatDialog,
    public toastr: ToastsManager,
    private vcr: ViewContainerRef,
    public jwtService: JwtService,
    public _orderService: OrderService,
    public _aclService: AccessControlService,
    private _global: GlobalService,
    public encDecService: EncDecService,
    private _companyservice: CompanyService
  ) {
    this.company_id = localStorage.getItem('user_company');
    if (this.company_id === '5ce12918aca1bb08d73ca25d' || this.company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(this.company_id);
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format("YYYY-MM-DD HH:mm"));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    //let sData=sessionStorage.getItem('shifts');
    let res = this._global.shifts;
    if (res) {
      //let res=JSON.parse(sData);
      this.itemsPerPage = res.limit;
      this.pageSize = res.limit;
      this.uniquedriverid = res.unique_driver_id;
      this.driverForm = res.driver;
      this.vehichle_side_no = res.vehichle_side_no;
      this.sideForm = res.side;
      this.vehichle_plate_no = res.vehichle_plate_no;
      this.plateForm = res.plate;
      this.selectedMoment = res.start_date ? new Date(res.start_date) : '';
      this.selectedMoment1 = res.end_date ? new Date(res.end_date) : '';
      this.driver_id = res.driver_id;
      this.driverIdForm = res.driverid;
      this.device_id = res.device_id;
      this.deviceForm = res.device;
      this.empty_status = res.empty_status;
      this.approve_status = res.approve_status;
      this.shift_id = res.shift_id;
    }
  }

  ngOnInit() {
    this.getCompanies();
    this.aclDisplayService();
    this.imageext = window.localStorage['ImageExt'];
    this.imageurl = environment.imgUrl;
    this.apiUrl = environment.apiUrl;
    this.is_search = false;
    this.getDriver();
    this.getShiftIds();
    this.queryField.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDrivers';
    })
    this.queryField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 400) { return; } else if (dec.status = 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.DriverData = result.driver;
            this.loadingIndicator = '';
          }
        }
      })
    this.device.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDevice';
    })
    this.device.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'asc',
          sortByColumn: 'unique_device_id',
          search_keyword: query,
          search: '',
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._deviceService.getDevicesListing(enc_data)
      })
      .subscribe((dec) => {
        if (dec) {
          if (dec.status === 400) { return; }
          else if (dec.status == 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.deviceData = result.devices;
            this.loadingIndicator = '';
          }
        }
      })
    this.driver_Id.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchDriverId';
    })
    this.driver_Id.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          limit: 10,
          'search_keyword': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._driverService.searchForDriver(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 400) { return; }
          else if (dec.status == 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.driverIdData = result.driver;
            this.loadingIndicator = '';
          }
        }
      });
    this.vehicle_side_no.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchVehichleSidenumber';
    })
    this.vehicle_side_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          'searchsidenumber': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._vehichleService.getVehicleListingAdmin(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 400) { return; }
          else if (dec.status === 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.vehichleSideData = result.result;
            this.loadingIndicator = '';
          }
        }
      });

    this.vehicle_plate_no.valueChanges.subscribe(data => {
      this.loadingIndicator = 'searchVehichlePlateNumber';
    })
    this.vehicle_plate_no.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          'searchplate': query,
          company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._vehichleService.getVehicleListingAdmin(enc_data);
      })
      .subscribe(dec => {
        if (dec) {
          if (dec.status === 400) { return; }
          else if (dec.status === 200) {
            var result: any = this.encDecService.dwt(this.session, dec.data);
            this.vehichlePlateData = result.result;
            this.loadingIndicator = '';
          }
        }
      });
  }
  public showlogs = [];
  public selectedshift = {};
  public showInnerShifts(shift, index) {
    //alert(shift.shift_logs.length);
    //alert(index);
    //alert(JSON.stringify(this.allShiftsData[index]));
    if (this.allShiftsData[index].selected && !(this.allShiftsData[index].expandedrows)) {
      this.allShiftsData[index].selected = false;
      for (var i = this.allShiftsData.length - 1; i >= 0; i--) {
        if (this.allShiftsData[i].expandedrows) {
          this.allShiftsData.splice(i, 1);
        }
      }
    } else {
      for (var j = 0; j < this.allShiftsData.length; ++j) {
        this.allShiftsData[j].selected = false;
      }
      var shiftlistlenbefore = this.allShiftsData.length;
      for (var i = this.allShiftsData.length - 1; i >= 0; i--) {
        if (this.allShiftsData[i].expandedrows) {
          this.allShiftsData.splice(i, 1);
        }
      }
      var shiftlistlenafter = this.allShiftsData.length;
      var indexdiffer = shiftlistlenbefore - shiftlistlenafter;
      if (index < indexdiffer) {
        index = index;
      } else {
        index = index - indexdiffer;
      }
      if (shift.shift_logs.length > 0) {
        this.allShiftsData[index].selected = true;
        var toaddIndex = index;
        for (var k = 0; k < shift.shift_logs.length; ++k) {
          toaddIndex = toaddIndex + 1;
          if (shift.shift_logs[k].shift_details_id.salik.length > 0) {
            //alert(JSON.stringify(shift.shift_logs[k].shift_details_id.salik));
            for (var w = 0; w < shift.shift_logs[k].shift_details_id.salik.length; ++w) {
              if (typeof shift.shift_logs[k].shift_details_id.salik[w].amt === "object") {
                shift.shift_logs[k].shift_details_id.salik[w].amt = shift.shift_logs[k].shift_details_id.salik[w].amt.$numberDecimal;
              } else {
                shift.shift_logs[k].shift_details_id.salik[w].amt = shift.shift_logs[k].shift_details_id.salik[w].amt;
              }
            }
          }
          this.allShiftsData.splice(toaddIndex, 0, shift.shift_logs[k]);
          this.allShiftsData[toaddIndex].expandedrows = true;
          this.allShiftsData[toaddIndex].shift_logs = [];
          this.allShiftsData[toaddIndex].unique_shift_id = shift.unique_shift_id;
          this.allShiftsData[toaddIndex].driver_id = shift.driver_id;
          this.allShiftsData[toaddIndex].driver_id = shift.driver_id;
          this.allShiftsData[toaddIndex].vehicle_id = shift.vehicle_id;
          this.allShiftsData[toaddIndex].shift_start = shift.shift_logs[k].shift_details_id.shift_start;
          this.allShiftsData[toaddIndex].shift_end = shift.shift_logs[k].shift_details_id.shift_end;
          //this.allShiftsData[toaddIndex].edit_flag = shift.shift_logs[k].shift_details_id.edit_flag ? shift.shift_logs[k].shift_details_id.edit_flag : '';
        }
      } else {

      }
    }
  }
  getDate(selectedMoment): any {
    if (selectedMoment) {
      let dateOld: Date = new Date(selectedMoment);
      return dateOld;
    }
  }

  getDate1(selectedMoment1): any {
    if (selectedMoment1) {
      let dateOld: Date = new Date(selectedMoment1);
      return dateOld;
    }
  }

  public aclDisplayService() {
    var params = {
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          for (let i = 0; i < data.menu.length; i++) {
            if (data.menu[i] == 'Downloads - Shift Management') {
              this.downloads = true;
            }
          }
        }
      };
    })
  }
  /**
   * Reset all filters and refresh the page.
   */
  reset() {
    this.pNo = 1;
    this.driver_id = '';
    this.device_id = '';
    this.vehichle_plate_no = '';
    this.vehichle_side_no = '';
    this.uniquedriverid = '';
    this.selectedMoment = this.getDate(moment().subtract(1, 'days').format("YYYY-MM-DD HH:mm"));
    this.selectedMoment1 = this.getDate1(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    this.approve_status = '';
    this.empty_status = '';
    this.shift_type = '';
    this.shift_id = '';
    this.driverIdForm = '';
    this.driverForm = '';
    this.deviceForm = '';
    this.sideForm = '';
    this.plateForm = '';
    this.pageSize = 10;
    if (this.dtc) {
      this.companyId = this.companyData.slice().map(x => x._id);
      this.companyIdFilter = this.companyId.slice()
      this.companyIdFilter.push('all')
    }
    //sessionStorage.removeItem('shifts');
    this.ngOnInit();
  }

  /**
   * Only refresh the page but not reset the filters.
   */
  public refetchShifts() {
    if (this.is_search == true) {
      this.searchShifts();
    } else {
      this.ngOnInit();
    }
  }
  /**
   *  start time timepicker
   */
  checkShiftstartTime() {
    this.selectedMoment1 = '';
  }

  /**
   *  end time timepicker
   */
  checkShiftstartTime1() {

    if (this.selectedMoment1 != '') {
      // this.selectedMoment="";
    }
  }
  /**
   * Load list of drivers in driver dropdown on page load.
   */
  public getDriver() {
    const params1 = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._driverService.searchForDriver(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.DriverData = data.driver;
          this.driverIdData = data.driver;
        }
      }
    });
  }

  /**
   * Autocomplete search for drivers.
   * @param data
   */
  searchDrivers(data) {
    if (typeof data === 'object') {
      this.driver_id = data._id;
    }
    else {
      this.driver_id = '';
    }
  }

  /**
   * Displaying username of driver in autocomplete search for driver dropdown.
   * @param data
   */
  displayFnDriver(data): string {
    return data ? data.username : data;
  }
  /**
   * Autocomplete search on driver unique id drop down
   * @param data 
   */
  searchDriverId(data) {
    if (typeof data === 'object') {
      this.uniquedriverid = data._id;
    }
    else {
      this.uniquedriverid = '';
    }
  }
  displayFnDriverId(data): string {
    return data ? data.emp_id : data;
  }

  /**
       * Get Vehichle Side and Plate data for drop down
       *
       */
  public getVehichleData() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      role: 'admin',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._vehichleService.getVehicleListingAdmin(enc_data).subscribe(dec => {
      if (dec) {
        if (dec.status == 200) {
          var Vehichledata: any = this.encDecService.dwt(this.session, dec.data);
          this.vehichlePlateData = Vehichledata.result;
          this.vehichleSideData = Vehichledata.result;
        }
      }
    });
  }
  searchVehichlePlateNumber(data) {
    if (typeof data === 'object') {
      this.vehichle_plate_no = data._id;
    }
    else {
      this.vehichle_plate_no = null;
    }
  }
  searchVehichleSidenumber(data) {
    if (typeof data === 'object') {
      this.vehichle_side_no = data._id;
    }
    else {
      this.vehichle_side_no = null;
    }
  }
  displayFnVehichleSide(data): string {
    return data ? data.vehicle_identity_number : data;
  }
  displayFnVehichlePlate(data): string {
    return data ? data.plate_number : data;
  }

  /**
   * Load list of shifts in shift_unique_id dropdown on page load.
   */
  public getShiftIds() {
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftsService.getShiftIds(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.ShiftIDData = data.shifts;
        }
      }
    })
  }

  public searchShiftForId(data) {
    this.loadingIndicator = 'searchShiftForId';
    if (typeof data !== 'object') {
      let params = {
        offset: 0,
        limit: 10,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        'search_keyword': data,
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftsService.getShiftIds(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.ShiftIDData = data.shifts;
            this.loadingIndicator = '';
          }
        }
      })
    } else {
      //alert(JSON.stringify(data));
      this.shift_id = data.shift_id;
      this.loadingIndicator = '';
    }
  }

  /**
   * Load list of devices in device dropdown on page load.
   */
  public getDevices() {
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id],
      unique_device_id: null
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.del = true;
    this._deviceService.getDevicesListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.deviceData = data.devices;
        }
      }
    })
  }

  /**
   * Autocomplete search for devices.
   * @param data
   */
  public searchDevice(data) {
    if (typeof data === 'object') {
      this.device_id = data._id;
    }
    else {
      this.device_id = '';
    }
  }

  /**
   * Displaying unique_device_id of device in autocomplete search field for device.
   * @param data
   */
  displayFnDevice(data): string {
    return data ? data.unique_device_id : data;
  }

  displayFnShift(data): string {
    return data ? data.unique_shift_id : data;
  }


  /**
   * Used for search shift
   */
  public searchShifts() {
    this.allShiftsData = [];
    this.pNo = 1;
    this.searchLoader = true;
    window.scrollTo(0, 300);
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      start_date: this.shift_id ? '' : this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.shift_id ? '' : this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      driver_id: this.driver_id ? this.driver_id : '',
      device_id: this.device_id ? this.device_id : '',
      empty_status: this.empty_status ? this.empty_status : '',
      approve_status: this.approve_status ? this.approve_status : '',
      shift_id: this.shift_id ? this.shift_id._id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    const params2 = {
      limit: this.pageSize,
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      driver: this.driverForm ? this.driverForm : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      side: this.sideForm ? this.sideForm : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      plate: this.plateForm ? this.plateForm : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      driver_id: this.driver_id ? this.driver_id : '',
      driverid: this.driverIdForm ? this.driverIdForm : '',
      device_id: this.device_id ? this.device_id : '',
      device: this.deviceForm ? this.deviceForm : '',
      empty_status: this.empty_status ? this.empty_status : '',
      approve_status: this.approve_status ? this.approve_status : '',
      shift_id: this.shift_id ? this.shift_id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    //sessionStorage.setItem('shifts',JSON.stringify(params2));
    this._global.shifts = params2;
    if (this.vehichle_plate_no != null || this.vehichle_side_no != null || this.uniquedriverid != '' || this.selectedMoment != "" || this.selectedMoment1 != "" || this.driver_id != "" || this.device_id != "" || this.approve_status != "" || this.empty_status != "" || this.shift_id != "") {

      this.del = true;
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftsService.getAllShifts(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.searchLoader = true;
            this.allShiftsData = data.getShifts;
            this.allShiftsLength = data.totalCount;
            this.searchLoader = false;
            this.is_search = true;
            this.del = false;
          }
        }
      });
      //console.log(JSON.stringify(params2));
      var encrypted = this.encDecService.nwt(this.session, params2);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftsService.getAllShiftsummary(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.shift_detail_data = data.shift_detail_data;
            this.payment_extra = data.payment_extra;
          }
        }
      });
    } else {
      const params = {
        offset: 0,
        limit: this.itemsPerPage,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      //sessionStorage.removeItem('shifts');
      this.del = true;
      this._shiftsService.getAllShifts(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.searchLoader = true;
            this.allShiftsData = data.getShifts;
            this.allShiftsLength = data.totalCount;
            this.searchLoader = false;
            this.is_search = false;
            this.del = false;
          }
        }
      });
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftsService.getAllShiftsummary(params).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.shift_detail_data = data.shift_detail_data;
            this.payment_extra = data.payment_extra;
          }
        }
      });
    }
  }

  /**
   * Open dialog to update approve_status for a shift.
   * @param id
   */
  openDialog(id, index): void {
    const dialogRef = this.dialog.open(ShiftDialogComponent, {
      width: '450px',
      height: '150px',
      hasBackdrop: true,
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to approve this shift' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.updateApprovedStatus(id);
        this.allShiftsData[index].approve_status = 1;
        this.toastr.success('Shift approved successfully.');
      } else {
        this.toastr.error('Shift not approved.');
      }
    });
  }

  /**
   * Update approve_status for a shift.
   * @param id
   */
  public updateApprovedStatus(id) {
    this.searchLoader = true;
    this._shiftsService.updateShiftsForApproving(id).then((response) => {
      this.searchLoader = false;
    });
  }

  /**
   * Used in pagination.
   * @param data1
   */
  pagingAgent(data1) {
    //alert(data1);
    this.allShiftsData = [];
    window.scrollTo(0, 300);
    this.searchLoader = true;
    this.pageNo = (data1 * this.itemsPerPage) - this.itemsPerPage;
    const params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: this.key ? this.sortOrder : 'desc',
      sortByColumn: this.key ? this.key : '_id',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      driver_id: this.driver_id ? this.driver_id : '',
      device_id: this.device_id ? this.device_id : '',
      empty_status: this.empty_status ? this.empty_status : '',
      approve_status: this.approve_status ? this.approve_status : '',
      shift_id: this.shift_id ? this.shift_id._id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    if (this.selectedMoment != "" || this.selectedMoment1 != "" || this.driver_id != "" || this.device_id != "" || this.approve_status != "" || this.empty_status != "") {
      //this.del = true;
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftsService.getAllShifts(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.searchLoader = true;
            this.allShiftsData = data.getShifts;
            this.allShiftsLength = data.totalCount;
            this.searchLoader = false;
            this.is_search = false;
          }
        }
        //this.del = false;
      });
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftsService.getAllShiftsummary(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.shift_detail_data = data.shift_detail_data;
            this.payment_extra = data.payment_extra;
          }
        }
        //this.del = false;
      });
    } else {
      const params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      //this.del = true;
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftsService.getAllShifts(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.searchLoader = true;
            this.allShiftsData = data.getShifts;
            this.allShiftsLength = data.totalCount;
            this.searchLoader = false;
            this.is_search = false;
          }
        }
        //this.del = false;
      });
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._shiftsService.getAllShiftsummary(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data: any = this.encDecService.dwt(this.session, dec.data);
            this.shift_detail_data = data.shift_detail_data;
            this.payment_extra = data.payment_extra;
          }
        }
      });
    }

    /*this._shiftsService.getAllShifts( params).then((data) => {
      this.allShiftsData = data.getShifts;
      this.searchLoader = false;
    });*/



  }

  /**
   * Used to generate csv of all shifts details
   */
  public ShiftCsv() {
    const params1 = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      start_date: this.shift_id ? '' : this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.shift_id ? '' : this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      driver_id: this.driver_id ? this.driver_id : '',
      device_id: this.device_id ? this.device_id : '',
      empty_status: this.empty_status ? this.empty_status : '',
      approve_status: this.approve_status ? this.approve_status : '',
      shift_id: this.shift_id ? this.shift_id._id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftsService.getAllShifts(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          let labels = [
            'Shift Id',
            'Company',
            'Duration',
            'Login start',
            'Login end',
            'Occupied time',
            'Free time',
            'First start',
            'Last stop',
            'Orders distance',
            'Total distance',
            'Driver',
            'Vehicle',
            'Bill printed',
            'Bill printed count',
            'Bill not printed',
            'Bill not printed count',
            'No payment',
            'No payment count',
            'Free rides',
            'Status',
            'Total',
            'Payed',
            'To pay',
            'Drive',
            'Trip fully paid by Credit',
            'Trip partially paid by Credit',
            'Credit total',
            'Cash trip total',
            'Created At'
            // 'Credit',
            // 'Cash total',
            // 'Credit card (external) trip total',
            // 'Credit',
            // 'Credit card (external) total',
            // 'On account trip total',
            // 'On account total'
          ];
          let resArray = [];
          for (let i = 0; i < res.getShifts.length; i++) {

            const csvArray = res.getShifts[i];
            resArray.push
              ({
                _id: csvArray.unique_shift_id ? csvArray.unique_shift_id : 'N/A',
                company: csvArray.shift_details_id != null ? csvArray.shift_details_id.company_id ? csvArray.shift_details_id.company_id.company_name : 'N/A' : 'Dubai Taxi',
                duration: csvArray.shift_details_id != null ? csvArray.shift_details_id.shift_duration ? csvArray.shift_details_id.shift_duration : 'N/A' : 'N/A',
                shift_start: csvArray.shift_start ? csvArray.shift_start : 'N/A',
                shift_end: csvArray.shift_end ? csvArray.shift_end : 'N/A ',
                occupied_time: csvArray.shift_details_id != null ? csvArray.shift_details_id.occupied_time ? csvArray.shift_details_id.occupied_time : 'N/A' : 'N/A',
                free_time: csvArray.shift_details_id != null ? csvArray.shift_details_id.free_time ? csvArray.shift_details_id.free_time : 'N/A' : 'N/A',
                first_start: csvArray.shift_details_id != null ? csvArray.shift_details_id.first_start ? csvArray.shift_details_id.first_start : 'N/A' : 'N/A',
                last_stop: csvArray.shift_details_id != null ? csvArray.shift_details_id.last_stop != null ? csvArray.shift_details_id.last_stop : 'N/A' : 'N/A',
                orders_distance: csvArray.shift_details_id != null ? csvArray.shift_details_id.orders_distance ? csvArray.shift_details_id.orders_distance.$numberDecimal : 'N/A' : 'N/A',
                total_distance: csvArray.shift_details_id != null ? csvArray.shift_details_id.total_distance ? csvArray.shift_details_id.total_distance.$numberDecimal : 'N/A' : 'N/A',
                driver_username: csvArray.driver_id != null ? csvArray.driver_id.username ? csvArray.driver_id.username : 'N/A' : 'N/A',
                vehicle_display_name: csvArray.vehicle_id != null ? csvArray.vehicle_id.display_name ? csvArray.vehicle_id.display_name : 'N/A' : 'N/A',
                bill_printed: csvArray.shift_details_id != null ? csvArray.shift_details_id.bill_printed ? csvArray.shift_details_id.bill_printed.$numberDecimal : 'N/A' : 'N/A',
                bill_printed_count: csvArray.shift_details_id != null ? csvArray.shift_details_id.bill_printed_count ? csvArray.shift_details_id.bill_printed_count : 'N/A' : 'N/A',
                bill_not_printed: csvArray.shift_details_id != null ? csvArray.shift_details_id.bill_not_printed ? csvArray.shift_details_id.bill_not_printed.$numberDecimal : 'N/A' : 'N/A',
                bill_not_printed_count: csvArray.shift_details_id != null ? csvArray.shift_details_id.bill_not_printed_count ? csvArray.shift_details_id.bill_not_printed_count : 'N/A' : 'N/A',
                no_payment: csvArray.shift_details_id != null ? csvArray.shift_details_id.no_payment ? csvArray.shift_details_id.no_payment : 'N/A' : 'N/A',
                no_payment_count: csvArray.shift_details_id != null ? csvArray.shift_details_id.no_payment_count ? csvArray.shift_details_id.no_payment_count : 'N/A' : 'N/A',
                free_rides: csvArray.shift_details_id != null ? csvArray.shift_details_id.free_rides ? csvArray.shift_details_id.free_rides : 'N/A' : 'N/A',
                status: csvArray.shift_details_id != null ? csvArray.shift_details_id.status ? csvArray.shift_details_id.status : 'N/A' : 'N/A',
                total: csvArray.shift_details_id != null ? csvArray.shift_details_id.total ? csvArray.shift_details_id.total.$numberDecimal : 'N/A' : 'N/A',
                payed: csvArray.shift_details_id != null ? csvArray.shift_details_id.payed ? csvArray.shift_details_id.payed.$numberDecimal : 'N/A' : 'N/A',
                to_pay: csvArray.shift_details_id != null ? csvArray.shift_details_id.to_pay ? csvArray.shift_details_id.to_pay.$numberDecimal : 'N/A' : 'N/A',
                drive: csvArray.shift_details_id != null ? csvArray.shift_details_id.drive ? csvArray.shift_details_id.drive.$numberDecimal : 'N/A' : 'N/A',
                trip_full_paid_by_credit: csvArray.shift_details_id != null ? csvArray.shift_details_id.trip_full_paid_by_credit ? csvArray.shift_details_id.trip_full_paid_by_credit.$numberDecimal : 'N/A' : 'N/A',
                trip_partially_paid_by_credit: csvArray.shift_details_id != null ? csvArray.shift_details_id.trip_partially_paid_by_credit ? csvArray.shift_details_id.trip_partially_paid_by_credit.$numberDecimal : 'N/A' : 'N/A',
                credit_total: csvArray.shift_details_id != null ? csvArray.shift_details_id.credit_total ? csvArray.shift_details_id.credit_total.$numberDecimal : 'N/A' : 'N/A',
                cash_trip_total: csvArray.shift_details_id != null ? csvArray.shift_details_id.cash_trip_total ? csvArray.shift_details_id.cash_trip_total.$numberDecimal : 'N/A' : 'N/A',
                credit: csvArray.shift_details_id != null ? csvArray.shift_details_id.credit ? csvArray.shift_details_id.credit.$numberDecimal : 'N/A' : 'N/A',
                create_at: csvArray.shift_details_id != null ? csvArray.shift_details_id.create_at ? csvArray.shift_details_id.create_at : 'N/A' : 'N/A',
              });
          }
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(resArray, 'Shifts_List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        }
      }
    });
  }

  /**
   * Used to generate csv of all shifts distance detail.
   */
  public ShiftsDistanceCsv() {
    const params1 = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      unique_driver_id: this.uniquedriverid ? this.uniquedriverid : '',
      vehichle_side_no: this.vehichle_side_no ? this.vehichle_side_no : '',
      vehichle_plate_no: this.vehichle_plate_no ? this.vehichle_plate_no : '',
      start_date: this.shift_id ? '' : this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.shift_id ? '' : this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      driver_id: this.driver_id ? this.driver_id : '',
      device_id: this.device_id ? this.device_id : '',
      empty_status: this.empty_status ? this.empty_status : '',
      approve_status: this.approve_status ? this.approve_status : '',
      shift_id: this.shift_id ? this.shift_id._id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftsService.getAllShifts(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          let labels = [
            'Shift Id',
            'Driver Id',
            'Driver Name',
            'Revenue',
            'Number of trips',
            'Revenue km',
            'Lost km',
            'Total km',
            'Lost/total kilometers',
            'Create At'
          ];
          let resArray = [];
          for (let i = 0; i < res.getShifts.length; i++) {
            const csvArray = res.getShifts[i];
            resArray.push
              ({
                _id: csvArray.unique_shift_id ? csvArray.unique_shift_id : 'N/A',
                driver_id: csvArray.driver_id != null ? csvArray.driver_id.emp_id ? csvArray.driver_id.emp_id : 'N/A' : 'N/A',
                driver_username: csvArray.driver_id != null ? csvArray.driver_id.name ? csvArray.driver_id.name : 'N/A' : 'N/A',
                revenue: csvArray.shift_details_id != null ? csvArray.shift_details_id.revenue ? csvArray.shift_details_id.revenue.$numberDecimal : 'N/A' : 'N/A',
                no_of_trips: csvArray.shift_details_id != null ? csvArray.shift_details_id.no_of_trips ? csvArray.shift_details_id.no_of_trips : 'N/A ' : 'N/A',
                revenue_km: csvArray.shift_details_id != null ? csvArray.shift_details_id.revenue_km ? csvArray.shift_details_id.revenue_km.$numberDecimal : 'N/A' : 'N/A',
                lost_km: csvArray.shift_details_id != null ? csvArray.shift_details_id.lost_km ? csvArray.shift_details_id.lost_km.$numberDecimal : 'N/A' : 'N/A',
                total_km: csvArray.shift_details_id != null ? csvArray.shift_details_id.total_distance ? csvArray.shift_details_id.total_distance.$numberDecimal : 'N/A' : 'N/A',
                lost_total_percentage: csvArray.shift_details_id != null ? csvArray.shift_details_id.lost_total_percentage ? csvArray.shift_details_id.lost_total_percentage.$numberDecimal : 'N/A' : 'N/A',
                created_at: csvArray.created_at
              });
          }
          var options =
          {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            showTitle: false,
            useBom: true,
            headers: (labels)
          };
          new Angular2Csv(resArray, 'Shift_Distance_List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
        }
      }
    });
  }

  /**
   * Used to generate csv of all shifts details
   */
  public generateOtherPDF() {

    /////////////////////////////////////////
    var pdf = new jsPDF('p', 'mm', 'a3');
    pdf.setFontSize(12);
    pdf.setFont("times");
    var start_time_pdf = `Date from:${this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : ''}`;
    pdf.text(10, 20, start_time_pdf);
    pdf.text(10, 25, `Date to:${this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : ''}`);
    pdf.text(10, 30, 'Company: Dubai Taxi');
    pdf.text(10, 35, 'Company:Time zone: Asia/Dubai');
    var column1 = [
      { title: "", dataKey: "Total" },
      { title: "Count", dataKey: "Count" },
      { title: "Amount", dataKey: "Amount" },
    ];
    var row1 = [
      { "Total": 'Total', "Count": this.shift_detail_data.bill_printed_count + this.shift_detail_data.bill_notprinted_count, "Amount": this.shift_detail_data.total_amount },
    ];
    pdf.autoTable(column1, row1, {
      theme: 'plain',
      margins: {
        top: 40,
        bottom: 0,
        left: 133,
        width: 300,
      }, // a number, array or object
    });

    var columns = [
      { dataKey: "name" },
      { dataKey: "Count" },
      { dataKey: "Amount" },
    ];
    var rows = [
      { "name": "Bill printed", "Count": this.shift_detail_data.bill_printed_count, "Amount": this.shift_detail_data.bill_printed ? this.shift_detail_data.bill_printed : '0' },
      { "name": "Bill not printed", "Count": this.shift_detail_data.bill_notprinted_count, "Amount": this.shift_detail_data.bill_notprinted ? this.shift_detail_data.bill_notprinted : '0' },
    ];

    pdf.setLineWidth(0.1);
    pdf.line(10, 55, 270, 55);
    pdf.autoTable(columns, rows, {
      theme: 'plain',
      margins: {
        top: 55,
        bottom: 60,
        left: 120,
        width: 300,
      }, // a number, array or object
      drawHeaderRow: function () {
        return false;
      },
    });
    pdf.setLineWidth(0.1);
    pdf.line(10, 75, 270, 75);

    var columns_payment = [
      { title: "By payment extra", dataKey: "name" },
      { title: "", dataKey: "Amount" }
    ];
    var rows_payment = [];

    for (let i = 0; i < this.payment_extra.length; i++) {
      rows_payment.push({
        name: this.payment_extra[i].name ? this.payment_extra[i].name : 'N/A',
        Amount: this.payment_extra[i].amt ? this.payment_extra[i].amt.$numberDecimal : 'N/A',
      });
    }

    pdf.autoTable(columns_payment, rows_payment, {
      theme: 'plain',
      margins: {
        top: 80,
        bottom: 60,
        left: 120,
        width: 300,
      }, // a number, array or object
    });
    pdf.setLineWidth(0.1);
    pdf.line(10, 90 + (8 * this.payment_extra.length), 270, 90 + (8 * this.payment_extra.length));

    var columns_paymenttype = [
      { title: "By payment type", dataKey: "name" },
      { title: "", dataKey: "Amount" }
    ];
    var rows_paymenttype = [
      { "name": "Cash", "Amount": this.shift_detail_data.cash_trip_total },
      { "name": "Credit", "Amount": this.shift_detail_data.credit_trip_total },
      { "name": "Trip total", "Amount": this.shift_detail_data.total_amount }
    ];

    pdf.autoTable(columns_paymenttype, rows_paymenttype, {
      theme: 'plain',
      margins: {
        top: 100 + (8 * this.payment_extra.length),
        bottom: 60,
        left: 120,
        width: 300,
      }, // a number, array or object
    });

    /////////////////////////////////////
    const params1 = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      shift_start: this.shift_id ? '' : this.selectedMoment ? this.selectedMoment : '',
      shift_end: this.shift_id ? '' : this.selectedMoment1 ? this.selectedMoment1 : '',
      driver_id: this.driver_id ? this.driver_id : '',
      device_id: this.device_id ? this.device_id : '',
      shift_id: this.shift_id ? this.shift_id : ''
    };

    var encrypted = this.encDecService.nwt(this.session, params1);
    var enc_data = {
      data: encrypted,
      email: this.email
    }

    this._shiftsService.getAllShifts(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          let labels = [
            'ID/Company',
            'Duration',
            'Login start/Login end',
            'Occupied time/Free time',
            'First start/Last stop',
            'Orders distance',
            'Total distance',
            'Driver/Vehicle',
            'Bill printed/Bill printed count',
            'Bill not printed/Bill not printed count',
            'No payment/No payment count	',
            'Total/Payed/To pay',
            'Drive',
            'Trip fully paid by credit/Trip partially payed by credit',
            'Credit Total',
            'Cash trip total',
            'Created At'
          ];
          let resArray = [];
          for (let i = 0; i < res.getShifts.length; i++) {
            const csvArray = res.getShifts[i];
            var occupied_time: any = '';
            var trip_full_paid_by_credit: any = '';
            var trip_partially_paid_by_credit: any = '';
            var free_time: any = '';

            var id = csvArray.unique_shift_id ? csvArray.unique_shift_id : 'N/A';
            var company = csvArray.shift_details_id != null ? csvArray.shift_details_id.company ? csvArray.shift_details_id.company : 'N/A' : 'Dubai Taxi';

            id = id + "\n/\n" + company;
            var shift_start = csvArray.shift_start ? csvArray.shift_start : 'N/A';
            var shift_end = csvArray.shift_end ? csvArray.shift_end : 'N/A';
            shift_start = shift_start + "\n/\n" + shift_end;
            var duration = csvArray.shift_details_id != null ? csvArray.shift_details_id.shift_duration ? csvArray.shift_details_id.shift_duration : 'N/A' : 'N/A';

            var occupied_time = csvArray.shift_details_id != null ? csvArray.shift_details_id.occupied_time ? csvArray.shift_details_id.occupied_time : 'N/A' : 'N/A';
            var free_time = csvArray.shift_details_id != null ? csvArray.shift_details_id.free_time ? csvArray.shift_details_id.free_time : 'N/A' : 'N/A';
            occupied_time = occupied_time + "\n/\n" + free_time;

            var first_start = csvArray.shift_details_id != null ? csvArray.shift_details_id.first_start ? csvArray.shift_details_id.first_start : 'N/A' : 'N/A';
            var last_stop = csvArray.shift_details_id != null ? csvArray.shift_details_id.last_stop != null ? csvArray.shift_details_id.last_stop : 'N/A' : 'N/A';
            first_start = first_start + "\n/\n" + last_stop;

            var driver_username = csvArray.driver_id != null ? csvArray.driver_id.username ? csvArray.driver_id.username : 'N/A' : 'N/A';
            var vehicle_display_name = csvArray.vehicle_id != null ? csvArray.vehicle_id.display_name ? csvArray.vehicle_id.display_name : 'N/A' : 'N/A';
            driver_username = driver_username + "\n/\n" + vehicle_display_name;
            var bill_printed = csvArray.shift_details_id != null ? csvArray.shift_details_id.bill_printed ? csvArray.shift_details_id.bill_printed.$numberDecimal : 'N/A' : 'N/A';
            var bill_printed_count = csvArray.shift_details_id != null ? csvArray.shift_details_id.bill_printed_count ? csvArray.shift_details_id.bill_printed_count : 'N/A' : 'N/A';
            bill_printed = bill_printed + "\n/\n" + bill_printed_count;
            var bill_not_printed = csvArray.shift_details_id != null ? csvArray.shift_details_id.bill_not_printed ? csvArray.shift_details_id.bill_not_printed.$numberDecimal : 'N/A' : 'N/A';
            var bill_not_printed_count = csvArray.shift_details_id != null ? csvArray.shift_details_id.bill_not_printed_count ? csvArray.shift_details_id.bill_not_printed_count : 'N/A' : 'N/A';
            bill_not_printed = bill_not_printed + "\n/\n" + bill_not_printed_count;
            var no_payment = csvArray.shift_details_id != null ? csvArray.shift_details_id.no_payment ? csvArray.shift_details_id.no_payment : 'N/A' : 'N/A';
            var no_payment_count = csvArray.shift_details_id != null ? csvArray.shift_details_id.no_payment_count ? csvArray.shift_details_id.no_payment_count : 'N/A' : 'N/A';
            no_payment = no_payment + "\n/\n" + no_payment_count;
            var total = csvArray.shift_details_id != null ? csvArray.shift_details_id.total ? csvArray.shift_details_id.total.$numberDecimal : 'N/A' : 'N/A';
            var payed = csvArray.shift_details_id != null ? csvArray.shift_details_id.payed ? csvArray.shift_details_id.payed.$numberDecimal : 'N/A' : 'N/A';
            var to_pay = csvArray.shift_details_id != null ? csvArray.shift_details_id.to_pay ? csvArray.shift_details_id.to_pay.$numberDecimal : 'N/A' : 'N/A';
            total = total + "\n/\n" + payed + "\n/\n" + to_pay;
            trip_full_paid_by_credit = csvArray.shift_details_id != null ? csvArray.shift_details_id.trip_full_paid_by_credit ? csvArray.shift_details_id.trip_full_paid_by_credit.$numberDecimal : 'N/A' : 'N/A';
            trip_partially_paid_by_credit = csvArray.shift_details_id != null ? csvArray.shift_details_id.trip_partially_paid_by_credit ? csvArray.shift_details_id.trip_partially_paid_by_credit.$numberDecimal : 'N/A' : 'N/A',
              trip_full_paid_by_credit = trip_full_paid_by_credit + "\n/\n" + trip_partially_paid_by_credit;

            resArray.push
              ({
                _id: id,
                duration: duration + '       ',
                shift_start: shift_start,
                occupied_time: occupied_time,
                first_start: first_start,
                orders_distance: csvArray.shift_details_id != null ? csvArray.shift_details_id.orders_distance ? csvArray.shift_details_id.orders_distance.$numberDecimal : 'N/A' : 'N/A',
                total_distance: csvArray.shift_details_id != null ? csvArray.shift_details_id.total_distance ? csvArray.shift_details_id.total_distance.$numberDecimal : 'N/A' : 'N/A',
                driver_username: driver_username,
                bill_printed: bill_printed,
                bill_not_printed: bill_not_printed,
                no_payment: no_payment,
                total: total + '       ',
                drive: csvArray.shift_details_id != null ? csvArray.shift_details_id.drive ? csvArray.shift_details_id.drive.$numberDecimal : 'N/A' : 'N/A',
                trip_full_paid_by_credit: csvArray.shift_details_id != null ? csvArray.shift_details_id.trip_full_paid_by_credit ? csvArray.shift_details_id.trip_full_paid_by_credit.$numberDecimal : 'N/A' : 'N/A',
                trip_partially_paid_by_credit: csvArray.shift_details_id != null ? csvArray.shift_details_id.trip_partially_paid_by_credit ? csvArray.shift_details_id.trip_partially_paid_by_credit.$numberDecimal : 'N/A' : 'N/A',
                credit_total: csvArray.shift_details_id != null ? csvArray.shift_details_id.credit_total ? csvArray.shift_details_id.credit_total.$numberDecimal : 'N/A' : 'N/A',
                cash_trip_total: csvArray.shift_details_id != null ? csvArray.shift_details_id.cash_trip_total ? csvArray.shift_details_id.cash_trip_total.$numberDecimal : 'N/A' : 'N/A',
                created_at: csvArray.created_at
              });
          }

          var col = [

            { title: "Id\n/\nCompany", dataKey: "_id" },
            { title: "Duration", dataKey: "duration" },
            { title: "Occupied time/Free time", dataKey: "occupied_time" },
            { title: "First start\n/\nLast stop", dataKey: "first_start" },
            { title: "Orders distance", dataKey: "orders_distance" },
            { title: "Total distance", dataKey: "total_distance" },
            { title: "Driver\n/\nVehicle", dataKey: "driver_username" },
            { title: "Bill printed\n/\nBill printed count", dataKey: "bill_printed" },
            { title: "Bill not printed\n/\nBill not printed count", dataKey: "bill_not_printed" },
            { title: "No payment\n/\nNo payment count", dataKey: "no_payment" },
            { title: "Total\n/\nPayed\n/\nTo pay", dataKey: "total" },
            { title: "Drive", dataKey: "drive" },
            { title: "Trip fully paid by credit\n/\nTrip partially payed by credit", dataKey: "trip_full_paid_by_credit" },

            { title: "Credit Total", dataKey: "credit_total" },
            { title: "Cash trip total", dataKey: "cash_trip_total" },
            { title: "Created At", dataKey: "created_at" }
          ];
          var options = {
            styles: { // Defaul style
              lineWidth: 0.01,
              lineColor: 0,
              fillStyle: 'DF',
              halign: 'center',
              valign: 'middle',
              columnWidth: 'auto',
              overflow: 'linebreak'
            },
          };
          pdf.addPage();
          pdf.text(90, 10, "List - Shifts");
          pdf.autoTable(col, resArray, options);
          pdf.save('List-Shift' + moment().format('YYYY-MM-DD_HH_mm_ss') + '.pdf');
        }
      }
    });
  }

  /**
   * Generate pdf for list of all shifts.
   */
  generatePDF() {
    var pdf = new jsPDF('p', 'mm', 'a4');
    pdf.setFontSize(12);
    pdf.setFont("times");
    var start_time_pdf = `Date from:${this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : ''}`;
    pdf.text(10, 20, start_time_pdf);
    pdf.text(10, 25, `Date to:${this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : ''}`);
    pdf.text(10, 30, 'Company: Dubai Taxi');
    pdf.text(10, 35, 'Company:Time zone: Asia/Dubai');
    var column1 = [
      { title: "", dataKey: "Total" },
      { title: "Count", dataKey: "Count" },
      { title: "Amount", dataKey: "Amount" },
    ];
    var row1 = [
      { "Total": 'Total', "Count": this.shift_detail_data.bill_printed_count + this.shift_detail_data.bill_notprinted_count, "Amount": this.shift_detail_data.total_amount },
    ];
    pdf.autoTable(column1, row1, {
      theme: 'plain',
      margins: {
        top: 40,
        bottom: 0,
        left: 133,
        width: 300,
      }, // a number, array or object
    });

    var columns = [
      { dataKey: "name" },
      { dataKey: "Count" },
      { dataKey: "Amount" },
    ];
    var rows = [
      { "name": "Bill printed", "Count": this.shift_detail_data.bill_printed_count, "Amount": this.shift_detail_data.bill_printed ? this.shift_detail_data.bill_printed : '0' },
      { "name": "Bill not printed", "Count": this.shift_detail_data.bill_notprinted_count, "Amount": this.shift_detail_data.bill_notprinted ? this.shift_detail_data.bill_notprinted : '0' },
    ];

    pdf.setLineWidth(0.1);
    pdf.line(10, 55, 200, 55);
    pdf.autoTable(columns, rows, {
      theme: 'plain',
      margins: {
        top: 55,
        bottom: 60,
        left: 120,
        width: 300,
      }, // a number, array or object
      drawHeaderRow: function () {
        return false;
      },
    });
    pdf.setLineWidth(0.1);
    pdf.line(10, 75, 200, 75);

    var columns_payment = [
      { title: "By payment extra", dataKey: "name" },
      { title: "", dataKey: "Amount" }
    ];
    var rows_payment = [];

    for (let i = 0; i < this.payment_extra.length; i++) {
      rows_payment.push({
        name: this.payment_extra[i].name ? this.payment_extra[i].name : 'N/A',
        Amount: this.payment_extra[i].amt ? this.payment_extra[i].amt.$numberDecimal : 'N/A',
      });
    }

    pdf.autoTable(columns_payment, rows_payment, {
      theme: 'plain',
      margins: {
        top: 80,
        bottom: 60,
        left: 120,
        width: 300,
      }, // a number, array or object
    });
    pdf.setLineWidth(0.1);
    pdf.line(10, 90 + (8 * this.payment_extra.length), 200, 90 + (8 * this.payment_extra.length));

    var columns_paymenttype = [
      { title: "By payment type", dataKey: "name" },
      { title: "", dataKey: "Amount" }
    ];
    var rows_paymenttype = [
      { "name": "Cash", "Amount": this.shift_detail_data.cash_trip_total },
      { "name": "Credit", "Amount": this.shift_detail_data.credit_trip_total },
      { "name": "Trip total", "Amount": this.shift_detail_data.total_amount }
    ];

    pdf.autoTable(columns_paymenttype, rows_paymenttype, {
      theme: 'plain',
      margins: {
        top: 100 + (8 * this.payment_extra.length),
        bottom: 60,
        left: 120,
        width: 300,
      }, // a number, array or object
    });
    pdf.save('Shifts_List' + moment().format('YYYY-MM-DD_HH_mm_ss') + '.pdf');
  }
  scroll() {
    window.scrollTo(0, document.getElementById('google').offsetTop);
  }
  public creatingcsv = false;
  public getconstantsforcsv() {
    if (this.creatingcsv) {
      this.toastr.warning('Export Operation already in progress.');
    } else {
      this.creatingcsv = true;
      var params = {
        company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
      };
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._orderService.getCSVConstants(enc_data).then((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var res: any = this.encDecService.dwt(this.session, dec.data);
            let offset = res.data;
            this.createCsv(offset.order_fetch_interval, offset.order_fetch_offset);
          } else {
            this.creatingcsv = false
            this.toastr.error("Please try again")
          }
        }
      })
    }
  }
  public livetrackingOrder;
  public progressvalue = 0;
  public createCsv(interval_value, offset) {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      vehicle_id: this.vehichle_side_no ? this.vehichle_side_no : this.vehichle_plate_no ? this.vehichle_plate_no : '',
      start_date: this.shift_id ? '' : this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.shift_id ? '' : this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      driver_id: this.uniquedriverid ? this.uniquedriverid : this.driver_id ? this.driver_id : '',
      device_id: this.device_id ? this.device_id : '',
      shift_id: this.shift_id ? this.shift_id._id : '',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._shiftsService.getShiftCount(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 201) {
          this.creatingcsv = false;
          this.toastr.error('Please try again after some time');
        } else if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          var count = res.count;
          let res1Array = [];
          let i = 0;
          let fetch_status = true;
          let that = this;
          this.livetrackingOrder = setInterval(function () {
            if (fetch_status) {
              if (res1Array.length >= count) {
                that.progressvalue = 100;
                that.creatingcsv = false;
                clearInterval(that.livetrackingOrder);
                let labels = [
                  'Shift Id',
                  'Company',
                  'Duration',
                  'Login start',
                  'Login end',
                  'Occupied time',
                  'Free time',
                  'First start',
                  'Last stop',
                  'Orders distance',
                  'Total distance',
                  'Driver',
                  'Vehicle',
                  'Bill printed',
                  'Bill printed count',
                  'Bill not printed',
                  'Bill not printed count',
                  'Total',
                  'Payed',
                  'To pay',
                  'Drive',
                  'Trip fully paid by Credit',
                  'Trip partially paid by Credit',
                  'Credit total',
                  'Cash trip total',
                  'Account total',
                  'Online total',
                  'Credit',
                  'Created At'
                  // 'Cash total',
                  // 'Credit card (external) trip total',
                  // 'Credit',
                  // 'Credit card (external) total',
                  // 'On account trip total',
                  // 'On account total'
                ];
                var options =
                {
                  fieldSeparator: ',',
                  quoteStrings: '"',
                  decimalseparator: '.',
                  showLabels: true,
                  showTitle: false,
                  useBom: true,
                  headers: (labels)
                };
                new Angular2Csv(res1Array, 'Shifts_List' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
              }
              else {
                fetch_status = false;
                let diff = count - i;
                let perc = diff / count;
                let remaining = 100 * perc;
                that.progressvalue = 100 - remaining;
                const new_params = {
                  offset: i,
                  limit: parseInt(offset),
                  sortOrder: 'desc',
                  sortByColumn: 'updated_at',
                  vehicle_id: that.vehichle_side_no ? that.vehichle_side_no : that.vehichle_plate_no ? that.vehichle_plate_no : '',
                  start_date: that.shift_id ? '' : that.selectedMoment ? moment(that.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
                  end_date: that.shift_id ? '' : that.selectedMoment1 ? moment(that.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
                  driver_id: that.uniquedriverid ? that.uniquedriverid : that.driver_id ? that.driver_id : '',
                  device_id: that.device_id ? that.device_id : '',
                  shift_id: that.shift_id ? that.shift_id._id : '',
                  company_id: that.companyId.length > 0 ? that.companyId : [this.company_id]
                };
                var encrypted = that.encDecService.nwt(that.session, new_params);
                var enc_data = {
                  data: encrypted,
                  email: that.email
                }
                that._shiftsService.getShiftForCsv(enc_data).then((res) => {
                  if (dec) {
                    if (dec.status == 200) {
                      res = that.encDecService.dwt(that.session, res.data);
                      if (res.data.length > 0) {
                        for (let j = 0; j < res.data.length; j++) {
                          const csvArray = res.data[j];
                          res1Array.push({
                            _id: csvArray.shift_id ? csvArray.shift_id.unique_shift_id : 'N/A',
                            company: csvArray != null ? csvArray.company_id ? csvArray.company_id.company_name : 'N/A' : 'Dubai Taxi',
                            duration: csvArray != null ? csvArray.shift_duration ? csvArray.shift_duration : 'N/A' : 'N/A',
                            shift_start: csvArray.shift_start ? csvArray.shift_start : 'N/A',
                            shift_end: csvArray.shift_end ? csvArray.shift_end : 'N/A ',
                            occupied_time: csvArray != null ? csvArray.occupied_time ? csvArray.occupied_time : 'N/A' : 'N/A',
                            free_time: csvArray != null ? csvArray.free_time ? csvArray.free_time : 'N/A' : 'N/A',
                            first_start: csvArray != null ? csvArray.first_start ? csvArray.first_start : 'N/A' : 'N/A',
                            last_stop: csvArray != null ? csvArray.last_stop != null ? csvArray.last_stop : 'N/A' : 'N/A',
                            orders_distance: csvArray != null ? csvArray.orders_distance ? csvArray.orders_distance.$numberDecimal : 'N/A' : 'N/A',
                            total_distance: csvArray != null ? csvArray.total_distance ? csvArray.total_distance.$numberDecimal : 'N/A' : 'N/A',
                            driver_username: csvArray.driver_id != null ? csvArray.driver_id.username ? csvArray.driver_id.username : 'N/A' : 'N/A',
                            vehicle_display_name: csvArray.vehicle_id != null ? csvArray.vehicle_id.display_name ? csvArray.vehicle_id.display_name : 'N/A' : 'N/A',
                            bill_printed: csvArray != null ? csvArray.bill_printed ? csvArray.bill_printed.$numberDecimal : 'N/A' : 'N/A',
                            bill_printed_count: csvArray != null ? csvArray.bill_printed_count ? csvArray.bill_printed_count : 'N/A' : 'N/A',
                            bill_not_printed: csvArray != null ? csvArray.bill_not_printed ? csvArray.bill_not_printed.$numberDecimal : 'N/A' : 'N/A',
                            bill_not_printed_count: csvArray != null ? csvArray.bill_not_printed_count ? csvArray.bill_not_printed_count : 'N/A' : 'N/A',
                            total: csvArray.total ? csvArray.total.$numberDecimal : 'N/A',
                            payed: csvArray.payed ? csvArray.payed.$numberDecimal : 'N/A',
                            to_pay: csvArray.to_pay ? csvArray.to_pay.$numberDecimal : 'N/A',
                            drive: csvArray != null ? csvArray.drive ? csvArray.drive.$numberDecimal : 'N/A' : 'N/A',
                            trip_full_paid_by_credit: csvArray != null ? csvArray.trip_full_paid_by_credit ? csvArray.trip_full_paid_by_credit.$numberDecimal : 'N/A' : 'N/A',
                            trip_partially_paid_by_credit: csvArray != null ? csvArray.trip_partially_paid_by_credit ? csvArray.trip_partially_paid_by_credit.$numberDecimal : 'N/A' : 'N/A',
                            credit_total: csvArray != null ? csvArray.credit_total ? csvArray.credit_total.$numberDecimal : 'N/A' : 'N/A',
                            cash_trip_total: csvArray != null ? csvArray.cash_trip_total ? csvArray.cash_trip_total.$numberDecimal : 'N/A' : 'N/A',
                            account_total: csvArray != null ? csvArray.account_total ? csvArray.account_total.$numberDecimal : 'N/A' : 'N/A',
                            card_total: csvArray != null ? csvArray.card_total ? csvArray.card_total.$numberDecimal : 'N/A' : 'N/A',
                            credit: csvArray != null ? csvArray.credit ? csvArray.credit.$numberDecimal : 'N/A' : 'N/A',
                            create_at: csvArray != null ? csvArray.created_at ? csvArray.created_at : 'N/A' : 'N/A',
                          });
                          if (j == res.data.length - 1) {
                            i = i + parseInt(offset);
                            fetch_status = true;
                          }
                        }
                      }
                    }
                  }
                  else {
                    that.toastr.warning('Network reset, csv creation restarted')
                    that.createCsv(interval_value, offset);
                  }
                }).catch((Error) => {
                  that.toastr.warning('Network reset, csv creation restarted')
                  that.createCsv(interval_value, offset);
                });
              }
            }
          }, parseInt(interval_value));
        }
        else {
          this.creatingcsv = false;
          this.toastr.error('Server responded with an error status');
        }
      }
    });
  }
  changeCompany() {
    this.ShiftIDData = [];
    this.driverIdData = [];
    this.deviceData = [];
    this.vehichleSideData = [];
    this.vehichlePlateData = [];
    this.DriverData = [];
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId.length > 0 ? this.companyId : [this.company_id]
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
          if (this.dtc && ((window.localStorage['adminUser'] && JSON.parse(window.localStorage['adminUser']).role.role !== 'Dispatcher'  && JSON.parse(window.localStorage['adminUser']).role.role !== 'Admin') || (window.localStorage['dispatcherUser'] && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Dispatcher' && JSON.parse(window.localStorage['dispatcherUser']).role.role !== 'Admin'))) {
            this.companyId = data.getCompanies.map(x => x._id);
            this.companyIdFilter = data.getCompanies.map(x => x._id)
            this.companyIdFilter.push('all')
          }
          this.searchShifts();
        }
      }
    });
  }
  selectAllCompany() {
    if (this.companyId.length !== this.companyData.length) {
      this.companyIdFilter = this.companyData.slice().map(x => x._id);
      this.companyId = this.companyIdFilter.slice()
      this.companyIdFilter.push('all')
    }
    else {
      this.companyId = [];
      this.companyIdFilter = [];
    }
  }
  companyManualSelect() {
    let tempArray = this.companyIdFilter.slice()
    this.companyIdFilter = [];
    let index = tempArray.indexOf('all');
    if (index > -1) {
      tempArray.splice(index, 1);
      this.companyIdFilter = tempArray;
      this.companyId = this.companyIdFilter.slice()
    }
    else {
      this.companyId = tempArray.slice();
      this.companyIdFilter = tempArray;
    }
  }
}
