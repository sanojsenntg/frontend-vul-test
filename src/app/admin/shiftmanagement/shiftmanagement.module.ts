import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShiftTimingModule } from './shift-timing/shift-timing.module';
import { ShiftsModule } from './shifts/shifts.module';
import { MatSelectModule } from '@angular/material/select';
import { ShiftmanagementComponent } from './shiftmanagement/shiftmanagement.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    ShiftTimingModule,
    ShiftsModule,
    MatSelectModule,
    RouterModule,
    
  ],
  declarations: [ShiftmanagementComponent],
  exports: [
    RouterModule
  ],
})
export class ShiftmanagementModule { }
