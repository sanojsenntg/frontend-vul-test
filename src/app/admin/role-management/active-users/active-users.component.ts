import { Component, OnInit } from '@angular/core';
import * as moment from 'moment/moment';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { UsersService } from '../../../common/services/user/user.service';
import { ToastsManager } from 'ng2-toastr';
import { CompanyService } from '../../../common/services/companies/companies.service';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.css']
})
export class ActiveUsersComponent implements OnInit {
  searchSubmit: boolean;
  startDate: any = moment(Date.now()).subtract('15', 'minutes').format('YYYY-MM-DD HH:mm:ss');
  endDate: any = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
  session: string;
  email: string;
  companyId = [];
  companyData: any;
  activeUsers: any=[];
  max = Date.now();

  constructor(private _companyservice: CompanyService, private encDecService: EncDecService, public userService: AccessControlService, private toastr: ToastsManager) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }

  ngOnInit() {
    this.getActiveUsers();
  }

  getActiveUsers() {
    this.searchSubmit = true;
    const params = {
      startDate: this.startDate ? moment(this.startDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') : moment(Date.now()).tz('Asia/Dubai').subtract('15', 'minutes').format('YYYY-MM-DD HH:mm:ss'),
      endDate: this.endDate ? moment(this.endDate).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss') : moment(Date.now()).tz('Asia/Dubai').format('YYYY-MM-DD HH:mm:ss'),
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.userService.activeUsers(enc_data).then((dec) => {
      this.searchSubmit = false;
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.activeUsers = data.data;
      }
      else {
        this.toastr.error(dec ? dec.message : 'something went wrong');
      }
    });
  }
  reset() {
    this.startDate = moment(Date.now()).subtract('15', 'minutes').format('YYYY-MM-DD HH:mm:ss');
    this.endDate = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
  }
  resetEndDate() {
    this.endDate = '';
  }
}
