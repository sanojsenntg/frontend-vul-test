import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleListComponent } from './role-list/role-list.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddRoleComponent } from './add-role/add-role.component';
import { UserListComponent } from './user-list/user-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToasterModule } from 'angular2-toaster';
import { AddNewuserRolemanagementComponent } from './add-newuser-rolemanagement/add-newuser-rolemanagement.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { EditExistingUserComponent } from './edit-existing-user/edit-existing-user.component';
import { AccessPermissionsComponent} from './access-permissions/access-permissions.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { RolemanagementDashboardComponent } from './rolemanagement-dashboard/rolemanagement-dashboard.component';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { UserlogComponent } from './userlog/userlog.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { EditLogComponent } from '../../common/dialog/edit-log/edit-log.component';
import { ActiveUsersComponent } from './active-users/active-users.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    FormsModule, 
    ReactiveFormsModule,
    ToasterModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatTooltipModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  declarations: [RoleListComponent, AddRoleComponent, UserListComponent, AddNewuserRolemanagementComponent, EditExistingUserComponent, AccessPermissionsComponent, ChangePasswordComponent, RolemanagementDashboardComponent, UserlogComponent, ActiveUsersComponent],
  entryComponents:[EditLogComponent]
})

export class RoleManagementModule { }
