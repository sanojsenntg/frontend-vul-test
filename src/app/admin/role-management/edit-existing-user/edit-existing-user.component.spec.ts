import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditExistingUserComponent } from './edit-existing-user.component';

describe('EditExistingUserComponent', () => {
  let component: EditExistingUserComponent;
  let fixture: ComponentFixture<EditExistingUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditExistingUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditExistingUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
