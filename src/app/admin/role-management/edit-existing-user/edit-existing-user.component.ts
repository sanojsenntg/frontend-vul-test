import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from './../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../common/services/companies/companies.service';


@Component({
  selector: 'app-edit-existing-user',
  templateUrl: './edit-existing-user.component.html',
  styleUrls: ['./edit-existing-user.component.css']
})
export class EditExistingUserComponent implements OnInit {
  public _id;
  public params;
  public aclUser;
  public editUser;
  public aclRoles;
  public changeRole;
  public editedItem;
  public currentRole;
  public showLoader = false;
  public companyId: any = [];
  private sub: Subscription;
  public session
  public usersModel: any = {
    email: '',
    firstname: '',
    lastname: '',
    phonenumber: '',
    role: '',
    priority: ''
  };
  companyData;
  changeCompany = ''
  disabledCompany = false;
  currentCompany;
  constructor(public formBuilder: FormBuilder, public _aclService: AccessControlService, public toastr: ToastsManager, private router: Router, private route: ActivatedRoute, public encDecService: EncDecService, public _companyservice: CompanyService) {
  }

  ngOnInit() {
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
    if(this.companyId == '5ce12918aca1bb08d73ca25d' || this.companyId == '5cd982667a64fe51e5d3f7a0'){
      this.disabledCompany = true;
    }
    this.editUser = new FormGroup({
      email: new FormControl({ value: '', disabled: true }),
      firstname: new FormControl(''),
      lastname: new FormControl(''),
      phone_number: new FormControl(''),
      // password : new FormControl(''),
      // retype_password : new FormControl(''),
      role: new FormControl(''),
      priority: new FormControl(''),
      company: new FormControl('')
    });
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    this.params = {
      "user_id": this._id,
      company_id: this.companyId
    }
    this.getAclUsers(this.params);
    console.log(this.params);
    this.aclListService();
    this.getCompanies();
  }
  public getAclUsers(params) {
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUsersById(enc_data).then((dec) => {
      console.log(dec);
      if(dec.status == 200){
        var data: any = this.encDecService.dwt(this.session, dec.data);
        console.log(data)
        this.aclUser = data.data;
        console.log(this.aclUser);
      }
    })
  }

  public aclListService() {
    var params = {
      company_id: this.changeCompany ? this.changeCompany : this.companyId 
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.aclList(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var acl: any = this.encDecService.dwt(this.session, dec.data);
          this.aclRoles = acl.roles;
          for (let i = 0; i < this.aclRoles.length; i++) {
            if (this.aclRoles[i]._id == this.aclUser.role) {
              this.currentRole = this.aclRoles[i].role;
            }
          }
        }
      }else{
        console.log(dec.messages)
      }
    })
  }
  public getCompanies() {
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if(dec.status == 200){
      var data: any = this.encDecService.dwt(this.session, dec.data);
      this.companyData = data.getCompanies;
      for (let i = 0; i < this.companyData.length; i++) {
        if (this.companyData[i]._id == this.aclUser.company_id) {
          this.currentCompany = this.companyData[i].company_name;
        }
      }
      }
    });
  }
  OnSelectCompany(data){
    this.changeCompany = data;
    this.aclListService();
  }

  OnSelectRole(data) {
    this.changeRole = data;
  }

  updateUser() {
    //alert(this.aclUser.priority);
    this.showLoader = true;
    this.editedItem = {
      user_id: this._id,
      email: this.aclUser.email,
      firstname: this.editUser.value.firstname,
      lastname: this.editUser.value.lastname,
      phone_number: this.editUser.value.phone_number,
      role: this.changeRole,
      priority: this.aclUser.priority,
      company_id: this.changeCompany == '' ? this.companyId : this.changeCompany
    }
    var encrypted = this.encDecService.nwt(this.session, this.editedItem);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.updateAclUsers(enc_data).then((dec) => {
      if(dec){
        if (dec.status == 200) {
          this.editUser.controls['firstname'].setValue('');
          this.editUser.controls['lastname'].setValue('');
          this.editUser.controls['phone_number'].setValue('');
          this.editUser.controls['role'].setValue('');
          this.toastr.success('User successfully Updated');
          this.router.navigate(['/admin/role-management/user-list']);
        }
      }else{
        this.toastr.error(dec.message);
      }
      this.showLoader = false;
    })
  }
}
