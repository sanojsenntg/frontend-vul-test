import { Component, OnInit } from '@angular/core';
import { AccessControlService } from './../../../common/services/access-control/access-control.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray} from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-access-permissions',
  templateUrl: './access-permissions.component.html',
  styleUrls: ['./access-permissions.component.css']
})
export class AccessPermissionsComponent implements OnInit {
  public menuList;
  public roleList: any=[];
  myForm: FormGroup;
  public roleId;
  public companyId:any = [];
  public session;
  constructor(private route: ActivatedRoute,
  private _accessControlService: AccessControlService,
  private fb: FormBuilder,
  private toast: ToastsManager,
  public router:Router,
  public encDecService:EncDecService
  ) { 
  }
  //public form : FormGroup;
  public id;

  ngOnInit() {
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(this.route.snapshot.paramMap.get('company'));
    this.myForm = this.fb.group({
      menuId: this.fb.array([])
    });
    this.roleId=this.route.snapshot.paramMap.get('id');
    var params = { 
      role_id : this.roleId,
      company_id: this.companyId
    };
    console.log(params);
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._accessControlService.getAccessControlList(enc_data).then((dec) => {
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        console.log(data);
        this.menuList=data.menu;
        if(data.roleMenus){
        this.roleList=data.roleMenus.menu_data;
        }
       }
    });
  }    
  public checkRole(id)
  {
    if(this.roleList){
      for(var i=0;i<this.roleList.length;i++){
      if(this.roleList[i]==id)
      {
        return true;
      }
    }
  }
  return false;
  }
  onChange(email: string, isChecked: boolean,menu_id: string) {
    const selectFormArray = <FormArray>this.myForm.controls.menuId;
    if (isChecked) {
      selectFormArray.push(new FormControl(email));
      this.roleList.push(email);
        let index2=this.roleList.indexOf(menu_id);
        if (index2 === -1){
          this.roleList.push(menu_id);
        }
    } else {
      let index2 = this.roleList.findIndex(x => x == email);
      if (index2 !== -1) {
        this.roleList.splice(index2, 1);
      }
      
    }
    console.log(this.roleList);
  }
  onChangeMenu(email: string, isChecked: boolean,subMenu: any) {
    const selectFormArray = <FormArray>this.myForm.controls.menuId;
    if (isChecked) {
      selectFormArray.push(new FormControl(email));
      this.roleList.push(email);
      for(var i=0;i<subMenu.length;i++)
      { 
        let index2=this.roleList.indexOf(subMenu[i]._id);
        if (index2 === -1){
          this.roleList.push(subMenu[i]._id);
        }
      }
    } else {
      let index2 = this.roleList.findIndex(x => x == email);
      if (index2 !== -1) {
        this.roleList.splice(index2, 1);
      }
      for(var i=0;i<subMenu.length;i++)
      { 
        let index2=this.roleList.indexOf(subMenu[i]._id);
        if (index2 !== -1){
          this.roleList.splice(index2, 1);
        }
      }
    }
    console.log(this.roleList);
  }
  onSubmit()
  {    
    const params = {
      'role_id': this.roleId,
      'menu_data': this.roleList,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._accessControlService.addAccessControlList(enc_data).then((dec) => {
      if(dec.status==200){
      this.router.navigate(['/admin/role-management/user-list']); 
      this.toast.success('Access list updated');
      }
      else
      this.toast.error('Some error has occurred');
    });
  }
  
}
