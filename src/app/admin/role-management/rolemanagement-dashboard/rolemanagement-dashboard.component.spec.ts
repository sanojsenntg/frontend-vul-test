import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolemanagementDashboardComponent } from './rolemanagement-dashboard.component';

describe('RolemanagementDashboardComponent', () => {
  let component: RolemanagementDashboardComponent;
  let fixture: ComponentFixture<RolemanagementDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolemanagementDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolemanagementDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
