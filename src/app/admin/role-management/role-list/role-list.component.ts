import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from './../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from './../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../common/services/companies/companies.service';


@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {
  session;
  public companyId:any = [];
  public aclList;
  public delItem;
  public pageSize = 10;
  public searchSubmit = false;
  public name;
  public disabledDelete = true;
  public companyData;
  company_id_list = ['5cd982667a64fe51e5d3f7a0','5ce12918aca1bb08d73ca25d'];
  public userList;
  public list;
  public disabledCompany = false;
  constructor(public dialog: MatDialog,
    public overlay: Overlay,
    public toastr: ToastsManager,
    public encDecService:EncDecService,
    public _AclService : AccessControlService, public _encDecService: EncDecService,
    public _companyservice: CompanyService) {
    }

  ngOnInit() {
    this.session = window.localStorage['Sessiontoken'];
    this.companyId.push(window.localStorage['user_company']);
    if(this.companyId == '5ce12918aca1bb08d73ca25d' || this.companyId == '5cd982667a64fe51e5d3f7a0'){
      this.disabledCompany = true;
    }else{
      this.company_id_list = [];
    }
    this.aclListService();
    this.getCompanies('');
  }
  public aclListService(){
    this.searchSubmit = true;
    var params = {
      company_id: this.company_id_list.length == 0 ? this.companyId : this.company_id_list
    }
    console.log(params);
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._AclService.aclList(enc_data).then((dec)=>{
      if (dec.status == 200) {
        var acl:any = this.encDecService.dwt(this.session,dec.data);
        this.aclList = acl.roles;
        console.log(this.aclList);
      }
    })
  }
  public setCompanyList(params){
    if((this.company_id_list.indexOf('5ce12918aca1bb08d73ca25d') > -1 || this.company_id_list.indexOf('5cd982667a64fe51e5d3f7a0') > -1) && this.company_id_list.length == 1){
      this.disabledDelete = true;
    }else{
      this.disabledDelete = false;
    }
    this.aclListService();
  }
  public getCompanies(query) {
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId,
      search:query
    };
    var encrypted = this._encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec.status == 200) {
        var data: any = this._encDecService.dwt(this.session, dec.data);
        this.userList = data.getCompanies;
        this.companyData = data.getCompanies;
        this.list = data.getCompanies;
      }
    });
  }
  getPosts(event){
    console.log(event);
  }
  searchPartner(query) {
    console.log(query)
    this.list = this.userList.filter(function(el) {
        return el.company_name.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    })
    console.log(this.list);
  }

  displayFndpartner(data): string {
    return data ? data : data;
   }

  openDialog(selectedItem){
      
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
     width: '450px',
     scrollStrategy: this.overlay.scrollStrategies.noop(),
     data: {
      text: 'Are you sure you want to remove this record'
     }
    });
  
    dialogRef.afterClosed().subscribe(result => {
     if (result == true) {
      this.delItem = {
        "role_id" : selectedItem,
        company_id: this.companyId
      }
      var encrypted = this.encDecService.nwt(this.session,this.delItem);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._AclService.deleteRole(enc_data).then((dec)=>{
        this.aclListService();
      })
     } else {
  
     }
    });
   }
}
