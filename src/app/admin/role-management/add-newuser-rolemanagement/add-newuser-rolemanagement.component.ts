import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from './../../../common/services/access-control/access-control.service';
import { PasswordValidation } from './../../password-validation';
import { group } from '@angular/animations';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../common/services/companies/companies.service';


@Component({
  selector: 'app-add-newuser-rolemanagement',
  templateUrl: './add-newuser-rolemanagement.component.html',
  styleUrls: ['./add-newuser-rolemanagement.component.css']
})

export class AddNewuserRolemanagementComponent implements OnInit {

  public companyId: any = [];
  public addNewUser: FormGroup;
  public aclList=[];
  public changeRole;
  public userRole;
  public showLoader = false;
  public session;
  public companyData;
  public changeCompany = '';
  public disabledCompany = false;
  constructor(public formBuilder: FormBuilder, public _aclService: AccessControlService, public toastr: ToastsManager,
    public encDecService: EncDecService,
    public _companyservice: CompanyService,
    public router: Router) {
    this.addNewUser = formBuilder.group({
      email: ["", [
        Validators.required,
        Validators.pattern("[^ @]*@[^ @]*")
      ]],
      firstname: [''],
      lastname: [''],
      company: [''],
      phone_number: ["", [Validators.required, Validators.pattern("[0-9]{0-10}")]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      role: ['', Validators.required],
      priority: ['4', Validators.required]
    }, {
        validator: PasswordValidation.MatchPassword // your validation method
      })
  }

  ngOnInit() {
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
    if (this.companyId == '5ce12918aca1bb08d73ca25d' || this.companyId == '5cd982667a64fe51e5d3f7a0') {
      this.disabledCompany = true;
    }
    this.aclListService();
    this.getCompanies();
  }

  public getCompanies() {
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.companyData = data.getCompanies;
      }

    });
  }
  OnSelectCompany(data) {
    this.changeCompany = data;
    this.aclListService()
  }
  OnSelectRole(data) {
    this.changeRole = data;
  }

  public aclListService() {
    var params = {
      company_id: this.changeCompany ? this.changeCompany : this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.aclList(enc_data).then((dec) => {
      if (dec.status == 200) {
        var acl: any = this.encDecService.dwt(this.session, dec.data);
        this.aclList = acl.roles;
        console.log(this.aclList);
      }
    })
  }
  _keyPress(event: any) {
    if (event.charCode !== 0) {
      const pattern = /[0-9\+\-\ ]/;
      const inputChar = String.fromCharCode(event.charCode);

      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
    }
  }


  emailChange(data) {
    console.log(data);
  }
  saveNewUser() {
    if (this.addNewUser.valid) {
      return;
    }
    //console.log(this.addNewUser);
    this.showLoader = true;
    this.userRole = {
      email: this.addNewUser.value.email,
      firstname: this.addNewUser.value.firstname,
      lastname: this.addNewUser.value.lastname,
      phone_number: this.addNewUser.value.phone_number,
      role: this.changeRole,
      password: this.addNewUser.value.password,
      priority: this.addNewUser.value.priority,
      company_id: this.changeCompany == '' ? this.companyId : this.changeCompany
    }
    //console.log(this.userRole);
    var encrypted = this.encDecService.nwt(this.session, this.userRole);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.addNewAclUsers(enc_data).then((dec) => {
      if (dec.status == 200) {
        this.addNewUser.controls['email'].setValue('');
        this.addNewUser.controls['firstname'].setValue('');
        this.addNewUser.controls['lastname'].setValue('');
        this.addNewUser.controls['phone_number'].setValue('');
        this.addNewUser.controls['role'].setValue('');
        this.addNewUser.controls['password'].setValue('');
        this.addNewUser.controls['confirmPassword'].setValue('');
        this.router.navigate(['/admin/role-management/user-list']);
        this.toastr.success('User successfully created.');
      }
      else if (dec.status == 203) {
        this.toastr.error("User already exist");
      }
      else {
        this.toastr.error("Invalid User");
      }
      this.showLoader = false;
    })
  }
}
