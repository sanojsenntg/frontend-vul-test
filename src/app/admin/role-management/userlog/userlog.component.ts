import { Component, OnInit } from '@angular/core';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { JwtService } from '../../../common/services/api/jwt.service';
import { UserlogService } from './../../../common/services/userlog/userlog.service';
import { FormControl } from '@angular/forms';
import * as moment from 'moment/moment';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { MatDialog } from '@angular/material';
import { EditLogComponent } from '../../../common/dialog/edit-log/edit-log.component';
import { Overlay } from '@angular/cdk/overlay';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../common/services/companies/companies.service';

@Component({
  selector: 'app-userlog',
  templateUrl: './userlog.component.html',
  styleUrls: ['./userlog.component.css']
})
export class UserlogComponent implements OnInit {
  public LogData;
  public LogDataLength;
  public pageSize = 10;
  public pageNo = 0;
  public del = false;
  public userDets;
  public companyId:any = [];
  session;
  public selectedMoment: any = '';
  public selectedMoment1: any = '';
  constructor(public userLogService: UserlogService, public _aclService: AccessControlService ,
    public overlay: Overlay,public _jwtService: JwtService,
    public dialog: MatDialog, public encDecService:EncDecService,
    public _companyservice: CompanyService) {
      
     }

  public LogDataLength1;
  public LogData1;
  public max = new Date();
  public userList;
  public user_id;
  public api_filter = "";
  public flag = false;
  public searchSubmit = false;
  public activityList = [];
  public activityKeyList = [];
  public activitySubList = [];
  public model = '';
  public activityType = [];
  companyData;
  user: FormControl = new FormControl();
  public pNo = 1;
  disabledDelete = true;
  disabledCompany = false;
  company_id_list = ['5cd982667a64fe51e5d3f7a0','5ce12918aca1bb08d73ca25d'];
  ngOnInit() {
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
    if(this.companyId == '5ce12918aca1bb08d73ca25d' || this.companyId == '5cd982667a64fe51e5d3f7a0'){
      this.disabledCompany = true;
    }else{
      this.company_id_list = [];
    }

    this.selectedMoment = new Date(Date.now() - 86400000);
    this.selectedMoment1 = new Date(Date.now());
    this.getUserlog();
    this.getAclUsers();
    this.getDropValues();
    this.user.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        var params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: 'updated_at',
          'search_keyword': query,
          company_id: this.companyId
        }
        var encrypted = this.encDecService.nwt(this.session,params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._aclService.getAclUsers(enc_data)
        .then(dec => {
          if (dec.status === 400) { 
            return; 
          } else if(dec.status === 200) {
            var result:any = this.encDecService.dwt(this.session,dec.data);
            this.userList = result.users;
          }
        });
      });
      this.getCompanies();
  }

  public getCompanies() {
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.companyData = data.getCompanies;
      }
    });
  }
  public setCompanyList(params){
    if((this.company_id_list.indexOf('5ce12918aca1bb08d73ca25d') > -1 || this.company_id_list.indexOf('5cd982667a64fe51e5d3f7a0') > -1) && this.company_id_list.length == 1){
      this.disabledDelete = true;
    }else{
      this.disabledDelete = false;
    }
    this.getUserlog();
  }

  public getDropValues() {
    var params = {
      'model': '',
      'activityType': '',
      company_id: this.company_id_list.length == 0 ? this.companyId : this.company_id_list
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.dropDownValue(enc_data).then((dec) => {
      if(dec.status == 200){
        var res:any = this.encDecService.dwt(this.session,dec.data);
        this.activityList = res.data;
        for (var key in res.data) {
          this.activityKeyList.push(key);
        }
      }
    })
  }
  public getUserlog() {
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      company_id: this.company_id_list.length == 0 ? this.companyId : this.company_id_list
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getUserlog(enc_data).then((dec) => {
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.LogData = data.userlog;
        this.LogDataLength = data.count;
        this.searchSubmit = false;
      }
    });
  }
  pagingAgent(data) {
    this.searchSubmit = true;
    this.pageNo = (data * this.pageSize) - this.pageSize;
   
    if (this.flag) {
      var params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'created_at',
        user_id: this.user_id ? this.user_id : '',
        start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
        end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
        model: this.model,
        activityType: this.activityType,
        company_id: this.company_id_list.length == 0 ? this.companyId : this.company_id_list
      }
    }else{
      params = {
        offset: this.pageNo,
        limit: this.pageSize,
        sortOrder: 'desc',
        sortByColumn: 'created_at',
        user_id: '',
        start_date: '',
        end_date: '',
        model: '',
        activityType: [],
        company_id: this.company_id_list.length == 0 ? this.companyId : this.company_id_list
      };
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.searchSubmit = true;
    this._aclService.getUserlog(enc_data).then((dec) => {
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.searchSubmit = false;
        this.LogData = data.userlog;
        this.LogDataLength = data.count;
      }
    });
  }
  checkShiftstartTime() {
    this.selectedMoment1 = '';
  }
  public getAclUsers() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getAclUsers(enc_data).then((dec) => {
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.userList = data.users;
      }
    })
  }
  public searchUser(data) {
    if (typeof data === 'object') {
      this.user_id = data._id;
    }
    else {
      this.user_id = '';
    }
  }
  displayFnUser(data): string {
    return data ? data.firstname : data;
  }
  public searchUserlog() {
    this.searchSubmit = true;
    this.flag = true;
    const params = {
      offset: 0,
      limit: this.pageSize,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      user_id: this.user_id ? this.user_id : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      model: this.model,
      activityType: this.activityType,
      company_id: this.company_id_list.length == 0 ? this.companyId : this.company_id_list
    };  
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getUserlog(enc_data).then((dec) => {
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.LogData = data.userlog;
        this.LogDataLength = data.count;
        this.searchSubmit = false;
        this.pNo = 1;
      }
    });
  }
  public reset() {
    this.pNo = 1;
    this.searchSubmit = true;
    this.flag = false;
    this.user_id = '';
    this.selectedMoment = new Date(Date.now() - 86400000);
    this.selectedMoment1 = new Date(Date.now());
    this.api_filter = '';
    this.activitySubList = [];
    this.model = '';
    this.activityType = [];
    this.getUserlog();
  }


  public createCsv() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      user_id: this.user_id ? this.user_id : '',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm') : '',
      model: this.model,
      activityType: this.activityType,
      company_id: this.company_id_list.length == 0 ? this.companyId : this.company_id_list
    };
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._aclService.getUserlog(enc_data).then((dec) => {
      if(dec.status == 200){
        var data:any = this.encDecService.dwt(this.session,dec.data);
        this.LogData1 = data.userlog;
        this.LogDataLength1 = data.count;
        let labels = [
          'Account Information',
          'Api Endpoint',
          'Activity',
          'Response',
          'Reason',
          'Date & Time'
        ];
        let resArray = [];
        for (let i = 0; i < this.LogDataLength1; i++) {
          const csvArray = this.LogData1[i];
          resArray.push({
            acc_info: csvArray.user_id!==null?csvArray.user_id.firstname + "\n" + csvArray.user_id.lastname + "\n" + csvArray.user_id.email:'NA',
            api_url: csvArray.api_url,
            activity: csvArray.activity,
            status: csvArray.status,
            reason: (csvArray.response.length > 0 && csvArray.response[0].reason) ? csvArray.response[0].reason : (csvArray.response.length > 0 && csvArray.response[0].note) ? csvArray.response[0].note : '',
            date: csvArray.created_at
          });
        }
        var options =
        {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: false,
          useBom: true,
          headers: (labels)
        };
        new Angular2Csv(resArray, 'User-Log-' + moment().format('YYYY-MM-DD_HH_mm_ss'), options);
      }
    });
  }
  setActivitySubList(key) {
    this.activitySubList=[];
    this.activitySubList = this.activityList[key];
  }
  editLog(data){
      this.dialog.closeAll();
      let dialogRef = this.dialog.open(EditLogComponent, {
        width: "600px",
        height:"500px",
        data: data,
        scrollStrategy: this.overlay.scrollStrategies.noop(),
        hasBackdrop:true
      });
    }
}
