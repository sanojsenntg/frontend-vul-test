import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes , PreloadAllModules } from '@angular/router';
import { RoleListComponent } from './role-list/role-list.component';
import { AddRoleComponent } from './add-role/add-role.component';
import { UserListComponent } from './user-list/user-list.component';
import { AddNewuserRolemanagementComponent } from './add-newuser-rolemanagement/add-newuser-rolemanagement.component';
import { EditExistingUserComponent } from './edit-existing-user/edit-existing-user.component';
import { AccessPermissionsComponent} from './access-permissions/access-permissions.component'
import { ChangePasswordComponent } from './change-password/change-password.component'
import { AclAuthervice } from './../../common/services/access-control/acl-auth.service'
import { RolemanagementDashboardComponent } from './rolemanagement-dashboard/rolemanagement-dashboard.component'
import { UserlogComponent } from './userlog/userlog.component';
import { ActiveUsersComponent } from './active-users/active-users.component';

export const RoleRoute: Routes = [
  { path: '', component: RolemanagementDashboardComponent, canActivate: [AclAuthervice], data: {roles: ["Role Management"]}},
  { path: 'role-list', component: RoleListComponent, canActivate: [AclAuthervice], data: {roles: ["Roles - List"]}},
  { path: 'add-role', component: AddRoleComponent, canActivate: [AclAuthervice], data: {roles: ["Roles - Add"]}},
  { path: 'user-list', component: UserListComponent, canActivate: [AclAuthervice], data: {roles: ["Users - List"]}},
  { path: 'addnew-user', component: AddNewuserRolemanagementComponent, canActivate: [AclAuthervice], data: {roles: ["Users - Add"]}},
  { path: 'edit-user/:id', component: EditExistingUserComponent, canActivate: [AclAuthervice], data: {roles: ["Users - Edit"]}},
  { path: 'access-permissions/:id/:company', component: AccessPermissionsComponent, canActivate: [AclAuthervice], data: {roles: ["Roles - Add"]}},
  { path: 'change-password/:id', component: ChangePasswordComponent, canActivate: [AclAuthervice], data: {roles: ["Users - Edit"]}},
  { path: 'userlog', component: UserlogComponent, canActivate: [AclAuthervice], data: {roles: ["Role Management"]}},
  { path: 'active-users', component: ActiveUsersComponent, canActivate: [AclAuthervice], data: {roles: ["Active - Users"]}}
];

