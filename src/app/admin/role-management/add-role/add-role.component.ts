import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from './../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../common/services/companies/companies.service';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.css']
})
export class AddRoleComponent implements OnInit {
  showLoader=false;
  validationMessages = {
    'role_name': {
      'required': 'Customer Phone is required.',
      'maxlength': 'Customer Phone should not exceed 15 digits.'
    }
  }
  public companyId:any = [];
  public session;
  public role_data;
  public companyData;
  public currentCompany;
  public changeCompany = [];
  public disabledCompany;
  public addNewRole : FormGroup;
  role_name: FormControl = new FormControl();

  constructor(
    private router: Router,
    formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public toastr: ToastsManager,
    vcr: ViewContainerRef,
    public encDecService:EncDecService,
    public _companyservice: CompanyService,
    public _AclService : AccessControlService) {
      this.addNewRole = formBuilder.group({
        role_name: ['', [Validators.maxLength(15), Validators.required]],
        company: [''],
      });
  }
  
  

  ngOnInit() {
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
    if(this.companyId == '5ce12918aca1bb08d73ca25d' || this.companyId == '5cd982667a64fe51e5d3f7a0'){
      this.disabledCompany = true;
    }
    this.getCompanies();
  }


  saveNewRole(){
    this.showLoader=true;
    var params = {
      role_name:this.addNewRole.value.role_name,
      company_id: this.changeCompany.length != 0 ? this.changeCompany : this.companyId
    }
    console.log(params);
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._AclService.addAclRole(enc_data).then((dec)=>{
      if(dec.status== 200){
        this.addNewRole.controls['role_name'].setValue('');
        this.router.navigate(['/admin/role-management/role-list']); 
        this.toastr.success('Role is successfully created.');
      }
      else if(dec.message=="Role already exist"){
        this.toastr.error("Role already exist");
      }
      else{
        this.toastr.error("Invalid Role");
      }
      this.showLoader=false;
    })
  }
  
  public getCompanies() {
    const param = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if(dec.status == 200){
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.companyData = data.getCompanies;
      }

    });
  }

  OnSelectRole(data) {
    this.changeCompany = data;
  }
}
