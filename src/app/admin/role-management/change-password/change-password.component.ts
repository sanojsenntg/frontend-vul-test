import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators,ReactiveFormsModule} from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from './../../../common/services/access-control/access-control.service';
import { PasswordValidation } from './../../password-validation';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  public showLoader=false;
  public companyId:any = [];
  public _id;
  public updatePasT
  public updatePassword : FormGroup;
  private sub: Subscription;
  public session;
  constructor(public formBuilder: FormBuilder, public _aclService: AccessControlService,public toastr: ToastsManager, private router: Router, private route: ActivatedRoute,public encDecService:EncDecService ) {
    this.updatePassword = formBuilder.group({
      password : ['', Validators.required],
      confirmPassword : ['', Validators.required]
    }, {
      validator: PasswordValidation.MatchPassword // your validation method
    })
  }
  

  ngOnInit() {
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
  }
  updatePass(){
    this.showLoader=true;
    this.updatePasT = {
      user_id : this._id,
      new_password : this.updatePassword.value.password,
      company_id: this.companyId[0]
    }
    console.log(this.updatePasT);
    var encrypted = this.encDecService.nwt(this.session,this.updatePasT);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    } 
    this._aclService.updatePassword(enc_data).then((res)=>{
      if(res.status==200){
        this.updatePassword.controls['password'].setValue('');
        this.updatePassword.controls['confirmPassword'].setValue('');
        this.router.navigate(['/admin/role-management/user-list']); 
        this.toastr.success('Password Successfully Updated');
      }
      else if(res.status==201){
        this.toastr.error("Some error has occured");
      }
      else{
        this.toastr.error("Invalid Password");
      }
      this.showLoader=false;
    })
  }
}
