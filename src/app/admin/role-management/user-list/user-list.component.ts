import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { DeleteDialogComponent } from './../../../common/dialog/delete-dialog/delete-dialog.component'
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { AccessControlService } from './../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../common/services/companies/companies.service';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  
  key: string = '';
  itemsPerPage = 10;
  pageNo = 0;
  public is_search = false;
  public _id;
  public searchSubmit = false;
  sortOrder = 'asc';
  public keyword;
  public usersLength;
  public usersData;
  public searchLoader = false;
  public del = false;
  public code = '';
  public companyId:any = [];
  public session;

  constructor(public dialog: MatDialog,
    public overlay: Overlay,
    public toastr: ToastsManager,
    public _AclService : AccessControlService,
    public _companyservice: CompanyService,
    public encDecService:EncDecService) { 
    }
    public companyData;
    public aclList;
    public delItem;
    public disabledDelete = true;
    public company_id_list = ['5cd982667a64fe51e5d3f7a0','5ce12918aca1bb08d73ca25d'];
    public disabledCompany = false;
    ngOnInit() {
      this.session = localStorage.getItem('Sessiontoken');
      this.companyId.push(localStorage.getItem('user_company'));
      if(this.companyId == '5ce12918aca1bb08d73ca25d' || this.companyId == '5cd982667a64fe51e5d3f7a0'){
        this.disabledCompany = true;
      }else{
        this.company_id_list = [];
      }
      this.getAclUsers();
      this.getCompanies();
    }
    
    public getAclUsers (){
      const params = {
        offset: 0,
        limit: 0,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.company_id_list.length == 0 ? this.companyId : this.company_id_list
      };
      var encrypted = this.encDecService.nwt(this.session,params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._AclService.getAclUsers(enc_data).then((dec) => {
        if (dec.status == 200) {
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.aclList = data.users;
        }
      })
    }
    public getCompanies() {
      const param = {
        offset: 0,
        sortOrder: 'desc',
        sortByColumn: 'updated_at',
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, param);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._companyservice.getCompanyListing(enc_data).then((dec) => {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.companyData = data.getCompanies;
        }
      });
    }
    public setCompanyList(params){
      if((this.company_id_list.indexOf('5ce12918aca1bb08d73ca25d') > -1 || this.company_id_list.indexOf('5cd982667a64fe51e5d3f7a0') > -1) && this.company_id_list.length == 1){
        this.disabledDelete = true;
      }else{
        this.disabledDelete = false;
      }
      this.getAclUsers();
    }
    onSelect(selectedItem: any) {
      console.log("Selected item Id: ", selectedItem._id); // You get the Id of the selected item here
    }
    openDialog(selectedItem){
      
      let dialogRef = this.dialog.open(DeleteDialogComponent, {
       width: '450px',
       scrollStrategy: this.overlay.scrollStrategies.noop(),
       data: {
        text: 'Are you sure you want to remove this record'
       }
      });
    
      dialogRef.afterClosed().subscribe(result => {
       if (result == true) {
        this.delItem = {
          "user_id" : selectedItem,
          company_id: this.companyId
        }
        var encrypted = this.encDecService.nwt(this.session,this.delItem);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._AclService.deleteAclUser(enc_data).then((res)=>{
          if(res.status == 200){
            this.toastr.success("User Successfully Deleted");
            this.getAclUsers ();
          }else{
            this.toastr.error(res.message);
          }
          
        })
       } else {
    
       }
      });
     }
}
