import { Component, OnInit } from '@angular/core';
import { JwtService } from '../../../common/services/api/jwt.service';
import { Router } from '@angular/router';
import { Observable } from "rxjs/Rx";
import { UsersService } from '../../../common/services/user/user.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { environment } from '../../../../environments/environment';
import { Socket } from 'ng-socket-io';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public loginUserData;
  public imageext;
  public apiUrl;
  public session_token;
  public useremail;
  public company_id_array = [];
  public usercompany;
  public socket_session_token;
  public company_details;
  public dtc = false;
  constructor(
    private _jwtService: JwtService,
    private _usersService: UsersService,
    private _EncDecService: EncDecService,
    private router: Router,
    public socket: Socket
  ) {
    this.socket.removeAllListeners("devicelocations_updates");
  }
  ngOnInit() {
    this.company_details = JSON.parse(window.localStorage['companydata']);
    this.apiUrl = environment.apiUrl;
    this.imageext = window.localStorage['ImageExt'];
    this.session_token = window.localStorage['Sessiontoken'];
    this.useremail = window.localStorage['user_email'];
    this.company_id_array = [];
    this.usercompany = window.localStorage['user_company'];
    if (this.usercompany === '5ce12918aca1bb08d73ca25d' || this.usercompany === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.socket_session_token = window.localStorage['SocketSessiontoken'];
    this.company_id_array.push(this.usercompany)
    const vm = this;
    var token = this._jwtService.getToken();
    this._usersService.verifyJWTToken(token, '1').then((data) => {
      if (data.success) {
      } else {
        vm.logout();
      }
    });
    this.socket.on('connect', function () {
      if (window.localStorage['adminUser'] && window.localStorage['adminUser']!==undefined) {
        var admin_id = JSON.parse(window.localStorage['adminUser']);
        if (admin_id) {
          admin_id.token = vm._jwtService.getToken();
          let params = {
            dispatcher: admin_id,
            company_id: vm.company_id_array
          }
          let enc_data = vm._EncDecService.sockNwt(vm.socket_session_token, params);
          vm.socket.emit('open_from_dashboard', enc_data);
          //alert(vm.connIcon);
          console.log('Connection is active...');
        }
      }
    });
    let that = this;
    this.socket.on('invalid_token_dash', function () {
      //alert("invalid");
      that.logout();
      //alert(vm.connIcon);
    });
  }

  /**
   * Logout Function
   * Destroy Token from localstorage.
   */

  public logout() {
    this._jwtService.destroyAdminToken();
    this._jwtService.destroyDispatcherToken();
    window.localStorage.removeItem('CUser');
    window.localStorage.removeItem("notificationList");
    window.localStorage.removeItem('CUser');
    window.localStorage.removeItem('Sessiontoken');
    window.localStorage.removeItem('user_email');
    window.localStorage.removeItem('user_company');
    window.localStorage.removeItem('companydata');
    window.localStorage.removeItem('SocketSessiontoken');
    sessionStorage.clear();
    //this.socket.removeAllListeners();
    this.router.navigate(['/admin/login']);
  }

}
