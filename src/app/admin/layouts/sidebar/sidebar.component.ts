import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtService } from '../../../common/services/api/jwt.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
declare var jQuery: any;
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, AfterViewInit {
  public loginUserData;
  public show: boolean = false;
  public selectedItem = 'home';
  public selectedItem1;
  public aclCheck;
  public aclList: any = [];
  public acl;
  public ulPopup = '';
  session_token: string;
  useremail: any;
  usercompany: any;
  company_details: any;
  companyType: any;
  constructor(
    public _aclService: AccessControlService,
    private _jwtService: JwtService,
    private router: Router,
    public dialog: MatDialog,
    private _EncDecService: EncDecService
  ) {
  }

  /**
   * get admin info
   */
  ngOnInit() {
    // this.session_token = localStorage.getItem('Sessiontoken');
    // this.useremail = window.localStorage['user_email'];
    // this.usercompany = window.localStorage['user_company'];
    // this.company_details = JSON.parse(window.localStorage['companydata']);
     this.companyType = JSON.parse(window.localStorage['companydata']).company_type ? JSON.parse(window.localStorage['companydata']).company_type : '3';
    // this.loginUserData = this._jwtService.getAdminUser();
    this.aclDisplayService();
  }

  /**
   * Load metis menu if not loaded
   */
  ngAfterViewInit() {
    (jQuery('#side-menu').metisMenu());
  }

  public aclDisplayService() {
    // let data = {
    //   company_id: this.company_details._id
    // }
    // let enc_data = this._EncDecService.nwt(this.session_token, data);
    // let data1 = {
    //   data: enc_data,
    //   email: localStorage.getItem('user_email')
    // }
    // this._aclService.getAclUser(data1).then((data) => {
    //   if (data) {
    //     if (data.status == 200) {
    //       data = this._EncDecService.dwt(this.session_token, data.data);
    this.aclList = JSON.parse(localStorage.getItem('userMenu'));
    this.aclCheck = JSON.parse(localStorage.getItem('userMenu'));
    this.aclMenuCheck('home');
    if (this.aclList.length == 0) {
      this.logout();
    }
    //   }
    //   else
    //     this.logout()
    // })
  }
  public aclMenuCheck(aclItem) {
    for (let i = 0; i < this.aclList.length; i++) {
      if (aclItem == this.aclList[i]) {
        return true
      }
    }
  }
  public showMenu(data) {
    let list = [];
    if (!this.aclCheck)
      return false;
    let i = 0;
    switch (data) {
      case 'other':
        list = ['Predefined Messages - List', 'Message History', 'FAQ - List', 'Social Settings - List', 'Contact us Category - List', 'Point Of Interest - List', 'Administration-edit-order'];
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'tariff':
        list = ['Tariffs - List']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'zones':
        list = ['Zone-List', 'Dubai Major Zone - List', 'Drive through payment zones / Tolls', 'Heat Map'];
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'vehicles':
        list = ['Vehicle Categories - List', 'Vehicle Sub Categories - List'];
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'partners':
        list = ['Partners List', 'Companies-List']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'payment':
        list = ['Payment Extra List', 'Payment Type List']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'promo':
        list = ['Promocodes', 'Promocode history']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
      case 'messages':
        list = ['Predefined Messages - List', 'Message History']
        while (i < list.length) {
          if (this.aclCheck.indexOf(list[i]) > -1)
            return true;
          i++;
        }
        break;
    }

  }


  public logout() {
    this._jwtService.destroyAdminToken();
    this._jwtService.destroyDispatcherToken();
    this.router.navigate(['/admin/login']);
  }

  listClick(event, newValue) {
    this.selectedItem = newValue;  // don't forget to update the model here
    if (this.ulPopup == newValue) {
      this.ulPopup = '';
    } else {
      this.ulPopup = newValue;
    }
    // ... do other stuff here ...
  }
  listClick1(event, newValue) {
    this.ulPopup = newValue;  // don't forget to update the model here
    // ... do other stuff here ...
  }

  public closeopups() {
    this.dialog.closeAll();
  }

  public opendispatcher() {
    let url = 'http://13.126.164.30/dispatcher';
    window.open(url, "_blank");
  }
  public gotodispatcher() {
    window.localStorage['dispatcherUser'] = window.localStorage['adminUser'];
    this.router.navigate(['/dispatcher']);
  }
}
