import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JwtService } from '../../common/services/api/jwt.service';
import { UsersService } from '../../common/services/user/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';
import { EncDecService } from '../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-change-profile',
  templateUrl: './change-profile.component.html',
  styleUrls: ['./change-profile.component.css']
})
export class ChangeProfileComponent implements OnInit {
  public form: FormGroup;
  public session;
  public company_id;
  public email;
  private loginUser;
  private verifyToken;
  public companyId: any = [];
  constructor(private router: Router,
    private _flashMessagesService: FlashMessagesService,
    private jwtService: JwtService,
    private _usersService: UsersService,
    public encDecService: EncDecService,
    private formBuilder: FormBuilder, private _toasterservice: ToastsManager) {
    this.form = formBuilder.group({
      firstname: [''],
      password: ['', [
        Validators.required
      ]],
      confirm_password: ['', [
        Validators.required
      ]]
    }, {
      validator: this.checkIfMatchingPasswords // your validation method
    });
  }

  ngOnInit() {
    this.company_id = window.localStorage.getItem('user_company');
    this.session = window.localStorage.getItem('Sessiontoken');
    this.email = window.localStorage.getItem('user_email');
    this.loginUser = this.jwtService.getAdminUser();
    this.companyId.push(this.company_id);
    var params = {
      company_id: this.companyId,
      _id: this.loginUser._id
    }
    //console.log(JSON.stringify(params));
    //console.log("session" + this.session);
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._usersService.getById(enc_data).then((dec) => {
      if(dec && dec.status == 200){
        var userData: any = this.encDecService.dwt(this.session, dec.data);
        this.form.setValue({
          firstname: userData.users[0].firstname,
          password: '',
          confirm_password: ''
        });
      }
    });
  }

  /**
   *
   * @param {string} AbstractControl
   * @returns {(group: FormGroup) => (void | void)}
   */

  checkIfMatchingPasswords(AC: AbstractControl) {
    const password = AC.get('password').value; // to get value in input tag
    const confirmPassword = AC.get('confirm_password').value; // to get value in input tag
    if (password !== confirmPassword) {
      AC.get('confirm_password').setErrors({ checkIfMatchingPasswords: true });
    } else {
      return null;
    }
  }

  /**
   *  Update Password function
   */

  public updatePassword() {
    if (!this.form.valid) {
      return;
    }
    const userdata = {
      _id: this.loginUser._id,
      password: this.form.value.password,
      token: this.verifyToken,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, userdata);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this.verifyToken = this.jwtService.getToken();
    this._usersService.updateUserPassword(enc_data).then((dec) => {
      var user: any = this.encDecService.dwt(this.session, dec.data);
      //console.log(JSON.stringify(user));
      if (user.savedUser) {
        this.form.setValue({
          firstname: user.savedUser.firstname,
          password: '',
          confirm_password: ''
        });
        this._flashMessagesService.show('Password update successfully!!', { cssClass: 'alert-success', timeout: 2000 });
      }
    }).catch((error) => {
      this._flashMessagesService.show(error, { cssClass: 'alert-danger', timeout: 2000 });
    });
  }
}
