import { Component, OnInit } from '@angular/core';
import * as moment from 'moment/moment';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtService } from '../../../common/services/api/jwt.service';
import { MatDialog } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { PromocodeService } from '../../../common/services/promocode/promocode.service';
import { AccessControlService } from '../../../common/services/access-control/access-control.service';
import { CustomerService } from '../../../common/services/customer/customer.service';
import { GlobalService } from '../../../common/services/global/global.service';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { CompanyService } from '../../../common/services/companies/companies.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
  itemsPerPage = 10;
  public companyId: any = [];
  pageNo = 0;
  public is_search = false;
  public _id;
  public searchSubmit = false;
  sortOrder = 'asc';
  public promoCodeLength;
  public promoCodeData;
  public searchLoader = false;
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  public selectedMoment: any = "";
  public selectedMoment1: any = "";
  public validFrom: any = "";
  public validTo: any = "";
  public customer_id: any = '';
  public order_id = '';
  public promo_code = new FormControl;
  public promo_list = [];
  public promo_chips = [];
  public max = new Date();
  public mDAF;
  public mDAT;
  public DAF;
  public DAT;
  public uLF;
  public uLT;
  public tULF;
  public tULT;
  public customer = new FormControl;
  public customerPhoneData = [];
  public errorMessage = {
    mDAF: "",
    mDAT: "",
    DAF: "",
    DAT: "",
    uLF: "",
    uLT: "",
    tULF: "",
    tULT: ""
  }
  public pNo = 1;
  public promoDetails = [];
  email: string;
  session: string;
  companyData: any = [];
  dtc: boolean = false;
  company_id_list: any = [];
  constructor(
    private router: Router,
    public jwtService: JwtService,
    public dialog: MatDialog,
    public overlay: Overlay,
    public toastr: ToastsManager,
    public _promoCodeService: PromocodeService,
    public _aclService: AccessControlService,
    private _global: GlobalService,
    public encDecService: EncDecService,
    private _customerService: CustomerService,
    private _companyservice: CompanyService) {
    const company_id: any = localStorage.getItem('user_company');
    if (company_id === '5ce12918aca1bb08d73ca25d' || company_id === '5cd982667a64fe51e5d3f7a0') {
      this.dtc = true;
    }
    this.company_id_list.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.aclDisplayService();
    this.selectedMoment = new Date(Date.now() - 86400000);
    this.selectedMoment1 = new Date(Date.now() + 86400000);
    let data = this._global.marketing_promo_transaction;
    if (data) {
      this.itemsPerPage = data.limit
      this.selectedMoment = data.start_date ? new Date(data.start_date) : '';
      this.selectedMoment1 = data.end_date ? new Date(data.end_date) : '';
      this.customer_id = data.customer_id;
      this.order_id = data.order_id;
      data.promo_code.forEach(element => {
        this.promo_chips.push(element)
      });
      this.validFrom = data.valid_from ? new Date(data.valid_from) : '';
      this.validTo = data.valid_to ? new Date(data.valid_to) : '';
      this.mDAF = data.max_discount_from;
      this.mDAT = data.max_discount_to;
      this.DAF = data.discount_from;
      this.DAT = data.discount_to;
      this.uLF = data.usage_count_from;
      this.uLT = data.usage_count_to;
      this.tULF = data.total_usage_count_from;
      this.tULT = data.total_usage_count_to;
    }
  }

  ngOnInit() {
    this.getPromocodes();
    this.getCompanies();
    this.promo_code
      .valueChanges.debounceTime(200)
      .subscribe(data => {
        //console.log('driverlist', data);
        if (typeof data === "string") {
          const params = {
            offset: 0,
            limit: this.itemsPerPage,
            sortOrder: 'desc',
            sortByColumn: 'updated_at',
            keyword: data,
            role: 'marketing',
            company_id: this.companyId
          };
          var encrypted = this.encDecService.nwt(this.session, params);
          var enc_data = {
            data: encrypted,
            email: this.email
          }
          this._promoCodeService.getPromocodeListing(enc_data).then((dec) => {
            if (dec.status = 200) {
              var data: any = this.encDecService.dwt(this.session, dec.data);
              this.promo_list = data.getPromocodes;
            }
          });
        }
      });
  }

  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec.status = 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Promocodes -Add") {
            this.aclAdd = true;
          } else if (data.menu[i] == "Promocodes -Edit") {
            this.aclEdit = true;
          } else if (data.menu[i] == "Promocodes -Delete") {
            this.aclDelete = true;
          }
        };
      }
      else {
        console.log(dec.message)
      }
    })
    this.customer.valueChanges.debounceTime(400).distinctUntilChanged()
      .switchMap((data) => {
        var params = {
          search: typeof data !== 'object' ? data.trim().substr(0, 1) == '+' ? data.substr(1) : data.trim() : '',
          limit: 10,
          company_id: this.companyId
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        return this._customerService
          .getCustomerByPhone(enc_data);
      }).subscribe(dec => {
        if (dec.status = 200) {
          var userdata: any = this.encDecService.dwt(this.session, dec.data);
          console.log(userdata)
          this.customerPhoneData = userdata.result;
        }
      });
  }

  public reset() {
    this.selectedMoment = new Date(Date.now() - 86400000);
    this.selectedMoment1 = new Date(Date.now() + 86400000);
    this.customer_id = "";
    this.order_id = "";
    this.promo_list = [];
    this.promo_chips = [];
    this.validFrom = "";
    this.validTo = "";
    this.mDAF = "";
    this.mDAT = "";
    this.DAF = "";
    this.DAT = "";
    this.uLF = "";
    this.uLT = "";
    this.tULF = "";
    this.tULT = "";
    this.errorMessage = {
      mDAF: "",
      mDAT: "",
      DAF: "",
      DAT: "",
      uLF: "",
      uLT: "",
      tULF: "",
      tULT: ""
    }
    this.promoDetails = []
    this.ngOnInit();
  }

  /**
   * Pagination for predefined message module
   * @param data 
   */
  pagingAgent(data) {
    this.pNo = data;
    const promo_ids = this.promo_chips.map(x => x._id);
    this.searchSubmit = true;
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    let params;
    params = {
      offset: this.pageNo,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      customer_id: this.customer_id ? this.customer_id._id : '',
      order_id: this.order_id ? this.order_id : '',
      promo_code: promo_ids.length > 0 ? promo_ids : [],
      valid_from: this.validFrom ? moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss') : '',
      valid_to: this.validTo ? moment(this.validTo).format('YYYY-MM-DD HH:mm:ss') : '',
      max_discount_from: this.mDAF ? this.mDAF : '',
      max_discount_to: this.mDAT ? this.mDAT : '',
      discount_from: this.DAF ? this.DAF : '',
      discount_to: this.DAT ? this.DAT : '',
      usage_count_from: this.uLF ? this.uLF : '',
      usage_count_to: this.uLT ? this.uLT : '',
      total_usage_count_from: this.tULF ? this.tULF : '',
      total_usage_count_to: this.tULT ? this.tULT : '',
      role: 'marketing',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
      company_id: this.company_id_list
    }
    this._promoCodeService.getPromocodeTransactionListing(enc_data).then((dec) => {
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.promoCodeLength = data.count;
        this.promoCodeData = data.getPromocodes;
        this.promoDetails = data.promo_detail;
      }
      else {
        this.toastr.error(dec.message)
      }
      this.searchSubmit = false;
    });
  }

  public getPromocodes() {
    this.searchSubmit = false;
    if (!this.checkValidity())
      return;
    this.pNo = 1;
    const promo_ids = this.promo_chips.map(x => x._id);
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      customer_id: this.customer_id ? this.customer_id._id : '',
      order_id: this.order_id ? this.order_id : '',
      promo_code: promo_ids.length > 0 ? promo_ids : [],
      valid_from: this.validFrom ? moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss') : '',
      valid_to: this.validTo ? moment(this.validTo).format('YYYY-MM-DD HH:mm:ss') : '',
      max_discount_from: this.mDAF ? this.mDAF : '',
      max_discount_to: this.mDAT ? this.mDAT : '',
      discount_from: this.DAF ? this.DAF : '',
      discount_to: this.DAT ? this.DAT : '',
      usage_count_from: this.uLF ? this.uLF : '',
      usage_count_to: this.uLT ? this.uLT : '',
      total_usage_count_from: this.tULF ? this.tULF : '',
      total_usage_count_to: this.tULT ? this.tULT : '',
      role: 'marketing',
      company_id: this.companyId
    };
    //console.log(params)
    const param2 = {
      limit: this.itemsPerPage,
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      customer_id: this.customer_id ? this.customer_id : '',
      order_id: this.order_id ? this.order_id : '',
      promo_code: this.promo_chips.length > 0 ? this.promo_chips : [],
      valid_from: this.validFrom ? moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss') : '',
      valid_to: this.validTo ? moment(this.validTo).format('YYYY-MM-DD HH:mm:ss') : '',
      max_discount_from: this.mDAF ? this.mDAF : '',
      max_discount_to: this.mDAT ? this.mDAT : '',
      discount_from: this.DAF ? this.DAF : '',
      discount_to: this.DAT ? this.DAT : '',
      usage_count_from: this.uLF ? this.uLF : '',
      usage_count_to: this.uLT ? this.uLT : '',
      total_usage_count_from: this.tULF ? this.tULF : '',
      total_usage_count_to: this.tULT ? this.tULT : '',
      role: 'marketing'
    };
    this._global.marketing_promo_transaction = param2;
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
      company_id: this.company_id_list
    }
    this._promoCodeService.getPromocodeTransactionListing(enc_data).then((dec) => {
      if (dec)
        if (dec.status = 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.promoCodeLength = data.count;
          this.promoCodeData = data.getPromocodes;
          this.promoDetails = data.promo_detail;
          this.searchSubmit = false;
        }
        else {
          this.toastr.error(dec.message)
        }
    });
  }
  searchPromoTransactions() {
    this.getPromocodes();
  }
  pChipselected(event) {
    if (typeof event == "object" && event !== null)
      if (this.promo_chips.indexOf(event) > -1) {
        return;
      } else {
        this.promo_chips.push(event);
        this.promo_code.setValue(null);
      }
  }
  removePChip(chip): void {
    const index = this.promo_chips.indexOf(chip);
    if (index >= 0) {
      this.promo_chips.splice(index, 1);
    }
  }
  displayFnPromo(data): String {
    return data ? data.promo_name : '';
  }
  clearEndTime() {
    this.selectedMoment1 = "";
    this.validTo = "";
  }
  displayCustomerFn(data): string {
    return data ? data.phone_number : data;
  }
  checkValidity() {
    let flag = true;
    this.errorMessage = {
      mDAF: "",
      mDAT: "",
      DAF: "",
      DAT: "",
      uLF: "",
      uLT: "",
      tULF: "",
      tULT: ""
    }
    if (this.mDAF !== "" && (parseInt(this.mDAF) < 0) || parseInt(this.mDAF) > 100) {
      this.errorMessage.mDAF = "Values should be between 0-100"
      flag = false;
    }
    if (this.mDAT !== "" && (parseInt(this.mDAT) < 0) || parseInt(this.mDAT) > 100) {
      this.errorMessage.mDAT = "Values should be between 0-100"
      flag = false;
    }
    if (this.DAF !== "" && (parseInt(this.DAF) < 0) || parseInt(this.DAF) > 100) {
      this.errorMessage.DAF = "Values should be between 0-100"
      flag = false;
    }
    if (this.DAT !== "" && (parseInt(this.DAT) < 0) || parseInt(this.DAT) > 100) {
      this.errorMessage.DAT = "Values should be between 0-100"
      flag = false;
    }
    if (this.uLF !== "" && parseInt(this.uLF) < 0) {
      this.errorMessage.uLF = "Value should be greater than zero"
      flag = false;
    }
    if (this.uLT !== "" && parseInt(this.uLT) < 0) {
      this.errorMessage.uLT = "Value should be greater than zero"
      flag = false;
    }
    if (this.tULF !== "" && parseInt(this.tULF) < 0) {
      this.errorMessage.tULF = "Value should be greater than zero"
      flag = false;
    }
    if (this.tULT !== "" && parseInt(this.tULT) < 0) {
      this.errorMessage.tULT = "Value should be greater than zero"
      flag = false;
    }
    if (this.mDAF !== "" && this.mDAT !== "")
      if (parseInt(this.mDAT) < parseInt(this.mDAF)) {
        this.errorMessage.mDAF = "Invalid range"
        this.errorMessage.mDAT = "Invalid range"
        flag = false;
      }
    if (this.DAF !== "" && this.DAT !== "")
      if (parseInt(this.DAT) < parseInt(this.DAF)) {
        this.errorMessage.DAF = "Invalid range"
        this.errorMessage.DAT = "Invalid range"
        flag = false;
      }
    if (this.uLF !== "" && this.uLT !== "")
      if (parseInt(this.uLT) < parseInt(this.uLF)) {
        this.errorMessage.uLT = "Invalid range"
        this.errorMessage.uLF = "Invalid range"
        flag = false;
      }
    if (this.tULF !== "" && this.tULT !== "")
      if (parseInt(this.tULT) < parseInt(this.tULF)) {
        this.errorMessage.tULF = "Invalid range"
        this.errorMessage.tULT = "Invalid range"
        flag = false;
      }
    return flag;
  }
  customerDetails(data) {
    this.router.navigate([]).then(result => { window.open('/admin/customer/detail/' + data, '_blank'); });
    console.log(data._id);
  }
  orderDetails(data) {
    this.router.navigate([]).then(result => { window.open('/admin/orders/order/' + data, '_blank'); });
    console.log(data._id);
  }
  getSum(a, b) {
    return parseFloat(a) + (parseFloat(b) * -1);
  }
  getPercentage(a, b) {
    let c = (parseFloat(a) * -1) + parseFloat(b);
    return (((parseFloat(a) * -1) / (c)) * 100).toFixed(2);
  }
  public getCompanies() {
    const param = {
      offset: 0,
      //limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, param);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._companyservice.getCompanyListing(enc_data).then((dec) => {
      if (dec.status = 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        //this.CompanyLength = data.count;
        this.companyData = data.getCompanies;
      }
      else {
        this.toastr.error(dec.message)
      }
      //this.searchSubmit = false;
    });
  }
  public code;
  public createCsv() {
    this.searchSubmit = true;
    this.code = '';
    const promo_ids = this.promo_chips.map(x => x._id);
    let params;
    params = {
      offset: 0,
      limit: this.promoCodeLength,
      sortOrder: 'desc',
      sortByColumn: 'created_at',
      start_date: this.selectedMoment ? moment(this.selectedMoment).format('YYYY-MM-DD HH:mm:ss') : '',
      end_date: this.selectedMoment1 ? moment(this.selectedMoment1).format('YYYY-MM-DD HH:mm:ss') : '',
      customer_id: this.customer_id ? this.customer_id._id : '',
      order_id: this.order_id ? this.order_id : '',
      promo_code: promo_ids.length > 0 ? promo_ids : [],
      valid_from: this.validFrom ? moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss') : '',
      valid_to: this.validTo ? moment(this.validTo).format('YYYY-MM-DD HH:mm:ss') : '',
      max_discount_from: this.mDAF ? this.mDAF : '',
      max_discount_to: this.mDAT ? this.mDAT : '',
      discount_from: this.DAF ? this.DAF : '',
      discount_to: this.DAT ? this.DAT : '',
      usage_count_from: this.uLF ? this.uLF : '',
      usage_count_to: this.uLT ? this.uLT : '',
      total_usage_count_from: this.tULF ? this.tULF : '',
      total_usage_count_to: this.tULT ? this.tULT : '',
      company_id: this.companyId,
      role: 'marketing'
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email,
      company_id: this.company_id_list
    }
    this._promoCodeService.getPromocodeTransactionListing(enc_data).then((dec) => {
      this.searchSubmit = false;
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.code = data.getPromocodes;
        this.promoDetails = data.promo_detail;
        let labels = [
          'Promocode',
          'RefNo/Budget',
          'Customer Name',
          'Customer Phone',
          'Order id',
          'Order amount',
          'Order amount with promo	Discounted amount',
          'Promo status',
          'Date'
        ];
        let resArray = [];
        for (let i = 0; i < this.code.length; i++) {
          const csvArray = this.code[i];
          var amtSplit = csvArray.synctrips[0].amt_splitup
          var discountAmt;
          for (let i = 0; i < amtSplit.length; i++) {
            if (amtSplit[i].id == 'A008') {
              discountAmt = amtSplit[i].amt * -1;
            }
          }
          resArray.push
            ({
              Promocode: csvArray.promo_info_id ? csvArray.promo_info_id.promo_code : 'N/A',
              'RefNo/Budget': csvArray.promo_info_id ? csvArray.promo_info_id.promo_ref : 'N/A',
              'Customer Name': csvArray.customers.length > 0 ? csvArray.customers[0].firstname + csvArray.customers[0].lastname : 'NA',
              'Customer Phone': csvArray.customers ? csvArray.customers[0].phone_number : 'NA',
              'Order id': csvArray.order_info_id ? csvArray.order_info_id.unique_order_id : 'NA',
              'Order amount': csvArray.order_info_id && csvArray.order_info_id.order_status == 'completed' ? csvArray.order_info_id.price : 'NA',
              'Order amount with promo	Discounted amount': discountAmt,
              'Promo status': csvArray.applied_status,
              'Date': JSON.stringify(csvArray.updated_at)
            });
        }
        var options =
        {
          fieldSeparator: ',',
          quoteStrings: '"',
          decimalseparator: '.',
          showLabels: true,
          showTitle: false,
          useBom: true,
          headers: (labels)
        };
        new Angular2Csv(resArray, 'Marketing_Promocode_Transactions', options);
      }
      else {
        this.toastr.error(dec.message)
      }
      this.searchSubmit = false;
    });
  }
}
