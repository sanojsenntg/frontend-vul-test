import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketingDashboardComponent } from './marketing-dashboard/marketing-dashboard.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { PromocodeService } from '../../common/services/promocode/promocode.service';
import { PromocoderestService } from '../../common/services/promocode/promocoderest.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddMarketingPromoComponent } from './marketing-promo/add-marketing-promo/add-marketing-promo.component';
import { EditMarketingPromoComponent } from './marketing-promo/edit-marketing-promo/edit-marketing-promo.component';
import { ListMarketingPromoComponent } from './marketing-promo/list-marketing-promo/list-marketing-promo.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { MarketingService } from '../../common/services/marketing/marketing.service';
import { MarketingRestService } from '../../common/services/marketing/marketingrest.service';
import { MatInputModule,MatChipsModule ,MatIconModule} from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { PromoHistoryComponent } from './promo-history/promo-history.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    MatSelectModule,
    MatTooltipModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    FormsModule, 
    ReactiveFormsModule,
    MatInputModule,
    MatChipsModule ,
    MatIconModule,
    MatAutocompleteModule
  ],
  declarations: [ PromoHistoryComponent, MarketingDashboardComponent, AddMarketingPromoComponent, EditMarketingPromoComponent, ListMarketingPromoComponent, TransactionsComponent],
  providers: [PromocodeService, PromocoderestService, MarketingService, MarketingRestService],
  exports: [RouterModule]
})
export class MarketingModule { }
