import {  Routes } from '@angular/router';
import { MarketingDashboardComponent } from './marketing-dashboard/marketing-dashboard.component';
import { ListMarketingPromoComponent } from './marketing-promo/list-marketing-promo/list-marketing-promo.component';
import { AddMarketingPromoComponent } from './marketing-promo/add-marketing-promo/add-marketing-promo.component';
import { EditMarketingPromoComponent } from './marketing-promo/edit-marketing-promo/edit-marketing-promo.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { AclAuthervice } from '../../common/services/access-control/acl-auth.service';
import { PromoHistoryComponent } from './promo-history/promo-history.component';

export const marketingRoutes: Routes = [
  { path: '', component: MarketingDashboardComponent },
  { path: 'list', component: ListMarketingPromoComponent, canActivate: [AclAuthervice], data: {roles: ["Marketing Promocode - List"]}},
  { path: 'add', component: AddMarketingPromoComponent, canActivate: [AclAuthervice], data: {roles: ["Marketing Promocode - Add"]}},
  { path: 'edit/:id', component: EditMarketingPromoComponent, canActivate: [AclAuthervice], data: {roles: ["Marketing Promocode - Edit"]}},
  { path: 'transactions', component: TransactionsComponent, canActivate: [AclAuthervice], data: {roles: ["Marketing Transactions"]}},
  { path: 'promohistory', component: PromoHistoryComponent, canActivate: [AclAuthervice], data: {roles: ["Marketing Promocode-History"]}}
];
