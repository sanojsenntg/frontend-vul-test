import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { CustomerService } from '../../../../common/services/customer/customer.service';
import { PromocodeService } from '../../../../common/services/promocode/promocode.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ZoneService } from '../../../../common/services/zones/zone.service';
import { ToastsManager } from 'ng2-toastr';
import * as moment from 'moment/moment';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-add-marketing-promo',
  templateUrl: './add-marketing-promo.component.html',
  styleUrls: ['./add-marketing-promo.component.css']
})
export class AddMarketingPromoComponent implements OnInit {

  public companyId: any = [];
  public max = new Date();
  public customerData;
  public zones;
  public zoneLength;
  public selected = '1';
  public promoCodeModel: any = {
    promo_name: '',
    promo_code: '',
    promo_type: 0,
    description: '',
    valid_from_date: '',
    valid_to_date: '',
    valid_from_time: '',
    valid_to_time: '',
    usage_count: '1',
    total_usage_count: '',
    customer_group_tag: [],
    maximum_discount_amount_upto: '',
    percentage_discount: '',
    min_order_amount: '',
    max_order_amount: '',
    pickup_applicable_zones: [],
    drop_applicable_zones: [],
    pickup_applicable_polygons: [],
    drop_applicable_polygons: [],
    role: 'marketing',
    refno: '',
    promo_match_pick_or_drop:false
  };
  session: string;
  email: string;
  constructor(private _promocodeService: PromocodeService,
    private _customerService: CustomerService,
    formBuilder: FormBuilder,
    private router: Router,
    public _zoneService: ZoneService,
    private _toasterService: ToastsManager,
    vcr: ViewContainerRef,
    public encDecService: EncDecService,
    public toastr: ToastsManager,
    public jwtService: JwtService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this._toasterService.setRootViewContainerRef(vcr);
  }

  public ngOnInit(): void {
    this.getCustomers();
    this.zoneLoad();
    this.promoCodeModel.usage_count = [{ _id: '1', name: '1' }];
  }
  resetEndtime() {
    this.promoCodeModel.valid_to_date = '';
  }
  public zoneLoad() {
    const params = {
      company_id: this.companyId,
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at'
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._zoneService.getZones(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.zones = data.zones
          this.zoneLength = data.count;;
        }
        else
          console.log(dec.message)
      }
    });
  }
  /**
  * Save promocode data
  *
  * @param formData
  */
  addPromoCode(formData): void {
    this.promoCodeModel.usage_count = this.selected;
    if (formData.valid) {
      if (this.promoCodeModel.maximum_discount_amount_upto == '0') {
        this._toasterService.error('Maximum amount cant be zero');
      } else if (this.promoCodeModel.percentage_discount == '0') {
        this._toasterService.error('Discount percentage cant be zero');
      } else if (this.promoCodeModel.min_order_amount == '0') {
        this._toasterService.error('Minimum order value cant be zero');
      } else if (this.promoCodeModel.max_order_amount == '0') {
        this._toasterService.error('Maximum order amount cant be zero');
      } else if (this.promoCodeModel.usage_count == '0') {
        this._toasterService.error('Customer usage cant be zero');
      } else if (this.promoCodeModel.total_usage_count == '0') {
        this._toasterService.error('Total usage cant be zero');
      }
      else if (this.promoCodeModel.valid_from_date == '') {
        this._toasterService.error('Please select a valid from date');
      } else if (this.promoCodeModel.valid_to_date == '') {
        this._toasterService.error('Please select a valid to date.');
      } else if (this.promoCodeModel.valid_from_time != '' && this.promoCodeModel.valid_to_time == '') {
        this._toasterService.error('Please select a valid to time');
      } else if (this.promoCodeModel.valid_to_time != '' && this.promoCodeModel.valid_from_time == '') {
        this._toasterService.error('Please select a valid from time.');
      } else if (this.promoCodeModel.promo_type == '1' && this.promoCodeModel.customer_group_tag.length == 0) {
        this._toasterService.error('Please select a customer group.');
      }
      else {
        this.promoCodeModel.valid_from_date = moment(this.promoCodeModel.valid_from_date).format('YYYY-MM-DD HH:mm:ss');
        this.promoCodeModel.valid_to_date = moment(this.promoCodeModel.valid_to_date).format('YYYY-MM-DD HH:mm:ss');
        if (this.promoCodeModel.valid_from_time != '') {
          this.promoCodeModel.valid_from_time = moment(this.promoCodeModel.valid_from_time).format('HH:mm:ss');
        }
        if (this.promoCodeModel.valid_to_time != '') {
          this.promoCodeModel.valid_to_time = moment(this.promoCodeModel.valid_to_time).format('HH:mm:ss');
        }
        if (this.promoCodeModel.pickup_applicable_zones.length > 0) {
          for (var i = 0; i < this.promoCodeModel.pickup_applicable_zones.length; ++i) {
            let index = this.zones.findIndex(x => x._id == this.promoCodeModel.pickup_applicable_zones[i]);
            if (index > -1) {
              this.promoCodeModel.pickup_applicable_polygons.push(this.zones[index].loc.coordinates[0]);
            }
          }
        }
        if (this.promoCodeModel.drop_applicable_zones.length > 0) {
          for (var j = 0; j < this.promoCodeModel.drop_applicable_zones.length; ++j) {
            let index1 = this.zones.findIndex(y => y._id == this.promoCodeModel.drop_applicable_zones[j]);
            if (index1 > -1) {
              this.promoCodeModel.drop_applicable_polygons.push(this.zones[index1].loc.coordinates[0]);
            }
          }
        }
        console.log(JSON.stringify(this.promoCodeModel));
        this.promoCodeModel['company_id'] = this.companyId;
        var encrypted = this.encDecService.nwt(this.session, this.promoCodeModel);
        var enc_data = {
          data: encrypted,
          email: this.email
        }
        this._promocodeService.addPromocode(enc_data)
          .then((dec) => {
            if (dec) {
              if (dec.status === 200) {
                var res: any = this.encDecService.dwt(this.session, dec.data);
                this._toasterService.success('Promocode added successfully.');
                this.router.navigate(['/admin/marketing/list']);
              } else if (dec.status === 201) {
                this._toasterService.error('Promocode addition failed.');
              } else if (dec.status === 203) {
                this._toasterService.warning('Promocode already exist.');
              }
              else
                this._toasterService.error(dec.message)
            }
          })
          .catch((error) => {
            this._toasterService.error('Promocode addition failed.');
          });
      }
    } else {
      console.log(formData)
      return;
    }
  }
  public getCustomers() {
    const params = {
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._customerService.fetchCustomerGroups(enc_data).then((dec) => {
      var data: any = this.encDecService.dwt(this.session, dec.data);
      if (dec) {
        if (dec.status == 200)
          this.customerData = data.customergroup;
        else
          console.log(dec.message)
      }
    });
  }

}
