import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMarketingPromoComponent } from './add-marketing-promo.component';

describe('AddMarketingPromoComponent', () => {
  let component: AddMarketingPromoComponent;
  let fixture: ComponentFixture<AddMarketingPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMarketingPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMarketingPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
