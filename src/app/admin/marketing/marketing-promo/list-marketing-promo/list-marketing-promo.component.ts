import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { MatDialog } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { ToastsManager } from 'ng2-toastr';
import { PromocodeService } from '../../../../common/services/promocode/promocode.service';
import { AccessControlService } from '../../../../common/services/access-control/access-control.service';
import { DeleteDialogComponent } from '../../../../common/dialog/delete-dialog/delete-dialog.component';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-list-marketing-promo',
  templateUrl: './list-marketing-promo.component.html',
  styleUrls: ['./list-marketing-promo.component.css']
})
export class ListMarketingPromoComponent implements OnInit {

  public companyId: any = [];
  key: string = '';
  itemsPerPage = 10;
  pageNo = 0;
  public is_search = false;
  public _id;
  public searchSubmit = false;
  sortOrder = 'asc';
  public keyword;
  public promoCodeLength;
  public promoCodeData;
  public searchLoader = false;
  public del = false;
  public code = '';
  public aclAdd = false;
  public aclEdit = false;
  public aclDelete = false;
  email: string;
  session: string;
  public validFrom: any = "";
  public validTo: any = "";

  constructor(
    private router: Router,
    public jwtService: JwtService,
    public dialog: MatDialog,
    public overlay: Overlay,
    public toastr: ToastsManager,
    public _promoCodeService: PromocodeService,
    public encDecService: EncDecService,
    public _aclService: AccessControlService) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
    this.aclDisplayService();
  }

  ngOnInit() {
    this.searchSubmit = true;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      role: 'marketing',
      company_id: this.companyId,
      startDate: this.validFrom ? moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.validTo ? moment(this.validTo).format('YYYY-MM-DD HH:mm:ss') : '',
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._promoCodeService.getPromocodeListing(enc_data).then((dec) => {
      if (dec && dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.promoCodeLength = data.count;
        this.promoCodeData = data.getPromocodes;
      }
      this.searchSubmit = false;
    });
  }

  public aclDisplayService() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._aclService.getAclUserMenu(enc_data).then((dec) => {
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        for (let i = 0; i < data.menu.length; i++) {
          if (data.menu[i] == "Marketing Promocode - Add") {
            this.aclAdd = true;
          } else if (data.menu[i] == "Marketing Promocode - Edit") {
            this.aclEdit = true;
          } else if (data.menu[i] == "Marketing Promocode - Delete") {
            this.aclDelete = true;
          }
        }
      }
      else {
        this.toastr.error(dec.message)
      }

    })
  }
  public refreshPromocode() {
    if (this.is_search == true) {
      this.getPromocodes(this.keyword);
    }
    else {
      this.ngOnInit();
    }
  }

  public reset() {
    this.is_search = false;
    this.code = '';
    this.validFrom = "";
    this.validTo = "";
    this.ngOnInit();
  }

  /**
  * Confirmation popup for deleting particular predefined message record
  * @param id 
  */
  openDialog(id): void {
    let dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '450px',
      scrollStrategy: this.overlay.scrollStrategies.noop(),
      data: { text: 'Are you sure you want to remove this record' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.del = true;
        this.deletePromocode(id);
      } else {
      }
    });
  }

  /**
   * Delete predefined message record from view
   * @param id 
   */
  public deletePromocode(id) {
    var params = {
      company_id: this.companyId,
      _id: id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._promoCodeService.updateDeletedStatus(enc_data).then((dec) => {
      var res: any = this.encDecService.dwt(this.session, dec.data);
      if (dec.status === 200) {
        this.refreshPromocode();
        this.promoCodeData.splice(id, 1);
        this.toastr.success('Promocode deleted successfully.');
      } else if (dec.status === 201) {
        this.toastr.success('Promocode deletion failed.');
      }
      else {
        this.toastr.error(dec.message)
      }
    });
  }

  /**
   * For sorting predefined meesage records 
   * @param key 
   */
  sort(key) {
    this.searchLoader = true;
    this.key = key;
    if (this.sortOrder == 'desc') {
      this.sortOrder = 'asc';
    } else {
      this.sortOrder = 'desc';
    }

    let params;
    if (this.is_search) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        keyword: this.keyword ? this.keyword : '',
        role: 'marketing',
        company_id: this.companyId
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.sortOrder,
        sortByColumn: this.key,
        role: 'marketing',
        company_id: this.companyId,
        startDate: this.validFrom ? moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss') : '',
        endDate: this.validTo ? moment(this.validTo).format('YYYY-MM-DD HH:mm:ss') : '',
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._promoCodeService.getPromocodeListing(enc_data).then((dec) => {
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.promoCodeLength = data.count;
        this.promoCodeData = data.getPromocodes;
      }
      else {
        this.toastr.error(dec.message)
      }
      this.searchSubmit = false;
    });
  }

  /**
  * Pagination for predefined message module
  * @param data 
  */
  pagingAgent(data) {
    this.pageNo = (data * this.itemsPerPage) - this.itemsPerPage;
    let params;
    if (this.is_search == true) {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        keyword: this.keyword ? this.keyword : '',
        role: 'marketing',
        company_id: this.companyId,
        startDate: this.validFrom ? moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss') : '',
        endDate: this.validTo ? moment(this.validTo).format('YYYY-MM-DD HH:mm:ss') : '',
      };
    } else {
      params = {
        offset: this.pageNo,
        limit: this.itemsPerPage,
        sortOrder: this.key ? this.sortOrder : 'desc',
        sortByColumn: this.key ? this.key : 'updated_at',
        role: 'marketing',
        company_id: this.companyId,
        startDate: this.validFrom ? moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss') : '',
        endDate: this.validTo ? moment(this.validTo).format('YYYY-MM-DD HH:mm:ss') : '',
      };
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._promoCodeService.getPromocodeListing(enc_data).then((dec) => {
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.promoCodeData = data.getPromocodes;
      }
      else {
        this.toastr.error(dec.message)
      }
    });
  }

  /**
   * For searching payment extra records
   * @param keyword 
   */
  public getPromocodes(keyword) {
    this.is_search = true;
    this.searchSubmit = true;
    this.keyword = keyword;
    const params = {
      offset: 0,
      limit: this.itemsPerPage,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      keyword: this.keyword,
      role: 'marketing',
      company_id: this.companyId,
      startDate: this.validFrom ? moment(this.validFrom).format('YYYY-MM-DD HH:mm:ss') : '',
      endDate: this.validTo ? moment(this.validTo).format('YYYY-MM-DD HH:mm:ss') : '',
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._promoCodeService.getPromocodeListing(enc_data).then((dec) => {
      if (dec.status == 200) {
        var data: any = this.encDecService.dwt(this.session, dec.data);
        this.promoCodeLength = data.count;
        this.promoCodeData = data.getPromocodes;
      }
      else {
        this.toastr.error(dec.message)
      }
      this.searchSubmit = false;
    });
  }
  clearEndTime() {
    this.validTo = "";
  }
}
