import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMarketingPromoComponent } from './list-marketing-promo.component';

describe('ListMarketingPromoComponent', () => {
  let component: ListMarketingPromoComponent;
  let fixture: ComponentFixture<ListMarketingPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMarketingPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMarketingPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
