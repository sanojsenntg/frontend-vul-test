import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMarketingPromoComponent } from './edit-marketing-promo.component';

describe('EditMarketingPromoComponent', () => {
  let component: EditMarketingPromoComponent;
  let fixture: ComponentFixture<EditMarketingPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMarketingPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMarketingPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
