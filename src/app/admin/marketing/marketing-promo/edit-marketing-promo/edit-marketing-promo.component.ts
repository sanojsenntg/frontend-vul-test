import { Component, OnInit, ViewContainerRef } from '@angular/core';
import * as moment from 'moment/moment';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { PromocodeService } from '../../../../common/services/promocode/promocode.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ZoneService } from '../../../../common/services/zones/zone.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../../../../common/services/api/jwt.service';
import { CustomerService } from '../../../../common/services/customer/customer.service';
import { EncDecService } from '../../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
@Component({
  selector: 'app-edit-marketing-promo',
  templateUrl: './edit-marketing-promo.component.html',
  styleUrls: ['./edit-marketing-promo.component.css']
})
export class EditMarketingPromoComponent implements OnInit {
  public companyId: any = [];
  public editForm: FormGroup;
  private sub: Subscription;
  public _id;
  public selected = '1';
  public promoCodeModel: any = {
    promo_name: '',
    promo_code: '',
    promo_type: 0,
    description: '',
    valid_from_date: '',
    valid_to_date: '',
    valid_from_time: '',
    valid_to_time: '',
    usage_count: 1,
    total_usage_count: '',
    customer_group_tag: [],
    //promo_valid_for_customers_tag: '',
    //applicable_for_app_category: [],
    //applicable_for_payment_types: [],
    maximum_discount_amount_upto: '',
    percentage_discount: '',
    min_order_amount: '',
    max_order_amount: '',
    pickup_applicable_zones: [],
    drop_applicable_zones: [],
    pickup_applicable_polygons: [],
    drop_applicable_polygons: [],
    role: 'marketing',
    refno: '',
    promo_match_pick_or_drop:false
  };
  public zones;
  public initialfromdate;
  public initialtodate;
  public initialfromtime;
  public initialtotime;
  public zoneLength;
  public max = new Date();
  customerData: any;
  email: string;
  session: string;
  constructor(private _promocodeService: PromocodeService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public encDecService: EncDecService,
    private router: Router,
    public jwtService: JwtService,
    public _zoneService: ZoneService,
    private _toasterService: ToastsManager,
    private vcr: ViewContainerRef,
    private _customerService: CustomerService,
    public toastr: ToastsManager) {
    const company_id: any = localStorage.getItem('user_company');
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.companyId.push(company_id);
  }

  ngOnInit() {
    this.initialfromdate = true;
    this.initialtodate = true;
    this.initialfromtime = true;
    this.initialtotime = true;
    this.zoneLoad();
    this.getCustomers();
    this.sub = this.route.params.subscribe((params) => {
      this._id = params['id'];
    });
    var params = {
      company_id: this.companyId,
      _id: this._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._promocodeService.getPromocodeById(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          this.promoCodeModel.promo_name = res.PromocodeById.promo_name ? res.PromocodeById.promo_name : '';
          this.promoCodeModel.promo_code = res.PromocodeById.promo_code ? res.PromocodeById.promo_code : '';
          this.promoCodeModel.description = res.PromocodeById.description ? res.PromocodeById.description : '';
          this.promoCodeModel.discount = res.PromocodeById.discount ? res.PromocodeById.discount : '';
          this.promoCodeModel.promo_type = res.PromocodeById.promo_type ? res.PromocodeById.promo_type : '0';
          this.promoCodeModel.valid_from_date = res.PromocodeById.valid_from_date;
          this.promoCodeModel.valid_to_date = res.PromocodeById.valid_to_date;
          this.promoCodeModel.valid_from_time = res.PromocodeById.valid_from_time;
          this.promoCodeModel.valid_to_time = res.PromocodeById.valid_to_time;
          this.promoCodeModel.usage_count = res.PromocodeById.usage_count;
          this.promoCodeModel.total_usage_count = res.PromocodeById.total_usage_count;
          this.promoCodeModel.maximum_discount_amount_upto = res.PromocodeById.maximum_discount_amount_upto != -1 ? res.PromocodeById.maximum_discount_amount_upto : '';
          this.promoCodeModel.percentage_discount = res.PromocodeById.percentage_discount;
          this.promoCodeModel.min_order_amount = res.PromocodeById.min_order_amount != -1 ? res.PromocodeById.min_order_amount : '';
          this.promoCodeModel.max_order_amount = res.PromocodeById.max_order_amount != -1 ? res.PromocodeById.max_order_amount : '';
          this.promoCodeModel.pickup_applicable_zones = res.PromocodeById.pickup_applicable_zones;
          this.promoCodeModel.drop_applicable_zones = res.PromocodeById.drop_applicable_zones;
          this.selected = res.PromocodeById.usage_count.toString();
          this.promoCodeModel.refno = res.PromocodeById.promo_ref;
          this.promoCodeModel.promo_match_pick_or_drop= res.PromocodeById.promo_match_pick_or_drop
        }
        else {
          this.toastr.error(dec.message)
        }
      }
    })
  }
  resetfromdate() {
    this.initialfromdate = false;
  }
  resettodate() {
    this.initialtodate = false;
  }
  resetfromtime() {
    this.initialfromtime = false;
    this.promoCodeModel.valid_from_time = '';
  }
  resettotime() {
    this.initialtotime = false;
    this.promoCodeModel.valid_to_time = '';
  }
  resetEndtime() {
    this.promoCodeModel.valid_to_date = '';
    this.initialtodate = false;
  }
  /**
   * To update the predefined message records
   */
  public updatePromocode(formData): void {
    this.promoCodeModel.usage_count = this.selected;
    console.log(this.promoCodeModel);
    if (this.promoCodeModel.maximum_discount_amount_upto == '0') {
      this._toasterService.error('Maximum amount cant be zero');
    } else if (this.promoCodeModel.percentage_discount == '0') {
      this._toasterService.error('Discount percentage cant be zero');
    } else if (this.promoCodeModel.min_order_amount == '0') {
      this._toasterService.error('Minimum order value cant be zero');
    } else if (this.promoCodeModel.max_order_amount == '0') {
      this._toasterService.error('Maximum order amount cant be zero');
    } else if (this.promoCodeModel.usage_count == '0') {
      this._toasterService.error('Customer usage cant be zero');
    } else if (this.promoCodeModel.total_usage_count == '0') {
      this._toasterService.error('Total usage cant be zero');
    }
    else if (this.promoCodeModel.valid_from_date == '') {
      this._toasterService.error('Please select a valid from date');
    } else if (this.promoCodeModel.valid_to_date == '') {
      this._toasterService.error('Please select a valid to date.');
    } else if (this.promoCodeModel.valid_from_time != '' && this.promoCodeModel.valid_to_time == '') {
      this._toasterService.error('Please select a valid to time');
    } else if (this.promoCodeModel.valid_to_time != '' && this.promoCodeModel.valid_from_time == '') {
      this._toasterService.error('Please select a valid from time.');
    } else if (this.promoCodeModel.promo_type == '1' && this.promoCodeModel.customer_group_tag.length == 0) {
      this._toasterService.error('Please select a customer group.');
    }
    else {
      this.promoCodeModel.valid_from_date = moment(this.promoCodeModel.valid_from_date).format('YYYY-MM-DD HH:mm:ss');
      this.promoCodeModel.valid_to_date = moment(this.promoCodeModel.valid_to_date).format('YYYY-MM-DD HH:mm:ss');
      if (this.promoCodeModel.valid_from_time != '' && !this.initialtotime) {
        this.promoCodeModel.valid_from_time = moment(this.promoCodeModel.valid_from_time).format('HH:mm:ss');
      }
      if (this.promoCodeModel.valid_to_time != '' && !this.initialfromtime) {
        this.promoCodeModel.valid_to_time = moment(this.promoCodeModel.valid_to_time).format('HH:mm:ss');
      }
      if (this.promoCodeModel.pickup_applicable_zones.length > 0) {
        for (var i = 0; i < this.promoCodeModel.pickup_applicable_zones.length; ++i) {
          let index = this.zones.findIndex(x => x._id == this.promoCodeModel.pickup_applicable_zones[i]);
          if (index > -1) {
            this.promoCodeModel.pickup_applicable_polygons.push(this.zones[index].loc.coordinates[0]);
          }
        }
      }
      if (this.promoCodeModel.drop_applicable_zones.length > 0) {
        for (var j = 0; j < this.promoCodeModel.drop_applicable_zones.length; ++j) {
          let index1 = this.zones.findIndex(y => y._id == this.promoCodeModel.drop_applicable_zones[j]);
          if (index1 > -1) {
            this.promoCodeModel.drop_applicable_polygons.push(this.zones[index1].loc.coordinates[0]);
          }
        }
      }
      console.log(JSON.stringify(this.promoCodeModel));
      this.promoCodeModel['company_id'] = this.companyId;
      this.promoCodeModel['_id'] = this._id;
      var encrypted = this.encDecService.nwt(this.session, this.promoCodeModel);
      var enc_data = {
        data: encrypted,
        email: this.email
      }
      this._promocodeService.updatePromocodeById(enc_data).then((dec) => {
        if (dec)
          if (dec.status === 200) {
            console.log(dec)
            var res: any = this.encDecService.dwt(this.session, dec.data);
            console.log(res)
            this._toasterService.success('Promocode updated successfully.');
            setTimeout(() => {
              this.router.navigate(['/admin/marketing/list']);
            }, 1000);
          } else if (dec.status === 201) {
            this._toasterService.error('Promocode updation failed.');
          } else if (dec.status === 203) {
            this._toasterService.error('Promocode usage count already passed limit.current count :' + res.data);
          }
          else {
            this._toasterService.error(dec.error)
          }
      })
        .catch((error) => {
          console.log(error)
          this._toasterService.error('Promocode updation failed.');
        });
    }
  }
  public zoneLoad() {
    const params = {
      offset: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._zoneService.getZones(enc_data).then((dec) => {
      if (dec)
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.zones = data.zones
          this.zoneLength = data.count;
        }
        else {
          console.log(dec.message)
        }
    });
  }
  public getCustomers() {
    const params = {
      limit: 10
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: this.email
    }
    this._customerService.fetchCustomerGroups(enc_data).then((data) => {
      if (data)
      if (data.status == 200) {
        var data: any = this.encDecService.dwt(this.session, data.data);
        this.customerData = data.customergroup;
      }
      else
        console.log(data.message)
    });
  }
}
