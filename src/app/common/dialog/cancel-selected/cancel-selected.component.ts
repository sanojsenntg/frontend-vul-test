import { Component, Inject, OnInit, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { OrderService } from '../../../common/services/order/order.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-cancel-selected',
  templateUrl: './cancel-selected.component.html',
  styleUrls: ['./cancel-selected.component.css']
})
export class CancelSelectedComponent implements OnInit {
  public cancelReasonlModel: any = {
    cancelreason: '',
    cancelText: '',
    gender: '',
    checked: false
  };
  public companyId: any = [];
  public reasonsItems: any;
  public cancelItems: any;
  public status: any;
  public cancelIntervel;
  public cancelFlag = false;
  public session;
  constructor(
    formBuilder: FormBuilder,
    public encDecService: EncDecService,
    public dialogRef: MatDialogRef<CancelSelectedComponent>,
    private _orderService: OrderService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public toastr: ToastsManager,
    vcr: ViewContainerRef) {
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.status = this.data.order_status;
    this.cancelItems = {
      type: this.data.type,
      company_id: this.companyId
     };
    var encrypted = this.encDecService.nwt(this.session,this.cancelItems);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._orderService.getCancelReasons(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data:any = this.encDecService.dwt(this.session,dec.data);
          this.reasonsItems = data.reasons;
        }
      }else{
        console.log(dec.message)
      }
    });
    this.cancelFlag = false;
  }
  ngOnInit() {
  }
  cancelOrder() {
    this.cancelFlag = true;
    let i = 0;
    var user = JSON.parse(window.localStorage["dispatcherUser"]);
    var data = {
      email: user.email,
      type: "order_cancel",
      reason: this.cancelReasonlModel.cancelreason,
      status: true,
      company_id: this.companyId
    };
    let order_status;
    let checkPoint = -1;
    this.cancelIntervel = setInterval(() => {
      if (i < this.status.length) {
        if (checkPoint != i) {
          checkPoint = i;
          order_status = this.status[i].order_status;
          if (
            order_status == "dispatcher_cancel" ||
            order_status == "completed" ||
            order_status == "ended_by_admin" ||
            order_status == "cancel_acknowledged" ||
            order_status == "customer_cancel" ||
            order_status == "rejected_by_driver" ||
            order_status == "order_timed_out"
          ) {
            if (order_status == "dispatcher_cancel") {
              this.status[i].cancel_message = " This order already cancelled By dispatcher.";
            } else if (order_status == "ended_by_admin") {
              this.status[i].cancel_message = " This order already ended By admin.";
            } else if (order_status == "cancel_acknowledged") {
              this.status[i].cancel_message = " This order already cancelled By dispatcher.";
            } else if (order_status == "customer_cancel") {
              this.status[i].cancel_message = " This order already cancelled By customer.";
            } else if (order_status == "rejected_by_driver") {
              this.status[i].cancel_message = " This order already cancelled By Driver.";
            } else if (order_status == "order_timed_out") {
              this.status[i].cancel_message = " This order already timedout.";
            } else {
              this.status[i].cancel_message = "This Order is already Completed.";
            }
            i++;
          }
          else {
            
            this.status[i].cancel_message = "Processing";
            data['_id'] = this.status[i]._id;
            var encrypted = this.encDecService.nwt(this.session,data);
            var enc_data = {
              data: encrypted,
              email: localStorage.getItem('user_email')
            }
            this._orderService.cancelOrder(enc_data).then(dec => {
              
              if (dec) {
                if (dec.status == 202) {
                  this.status[i].cancel_message=dec.message;
                } else if (dec.status == 205) {
                  this.status[i].cancel_message=dec.message;
                } else if (dec.status == 200) {
                  this.status[i].cancel_message=dec.message;
                } else {
                  this.status[i].cancel_message = dec.message;
                }
              } else {
                this.status[i].cancel_message = "Error while Order cancellation!";
              }
              i++;
            });
          }
        }
        // if(true){
        //   this.status[i].cancel_message="Initiated";//202 initiated 205 Already initiated 201 Failed"
        // }

      }
      else {
        clearInterval(this.cancelIntervel)
        this.toastr.success('Order cancellation operation processed.');
        setTimeout(() => {
          for (let i = 0; i < this.status.length; i++)
            this.status[i].cancel_message = '';
          this.dialogRef.close(true);
        }, 2000);
      }
    }, 1000)
  }

}
