import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceblockreasonComponent } from './deviceblockreason.component';

describe('DeviceblockreasonComponent', () => {
  let component: DeviceblockreasonComponent;
  let fixture: ComponentFixture<DeviceblockreasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceblockreasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceblockreasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
