import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NG_VALIDATORS, AbstractControl, ValidatorFn, FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { OrderService } from '../../../common/services/order/order.service';


@Component({
  selector: 'app-deviceblockreason',
  templateUrl: './deviceblockreason.component.html',
  styleUrls: ['./deviceblockreason.component.css']
})
export class DeviceblockreasonComponent implements OnInit {
  public cancelReasonlModel: any = {
    cancelreason: '',
    cancelText: '',
    gender: ''
  };

  public datareason = [{ "reason": "Pending Payment" }, { "reason": "Report to Office" }, { "reason": "Rule Violation" }, { "reason": "Terminated" }, { "reason": "Other" }];
  public reasonsItems: any;
  public type='';
  public cancelItems: any;
  constructor(
    formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DeviceblockreasonComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.type = data.type;
    this.reasonsItems = this.datareason;
  }

  ngOnInit() {
  }

  reasonPopup(formData): void {

  };
}
