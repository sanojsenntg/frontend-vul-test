import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ZoneService } from '../../services/zones/zone.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-rename-subzone',
  templateUrl: './rename-subzone.component.html',
  styleUrls: ['./rename-subzone.component.css']
})
export class RenameSubzoneComponent implements OnInit {
  public companyId:any = [];
  public zone = {
    name: ''
  }
  public id;
  public array
  session
  constructor(public zoneService:ZoneService,
    private _toasterService: ToastsManager,
    public encDecService:EncDecService,
    public dialogRef: MatDialogRef<RenameSubzoneComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log(data)
      this.array = data;
  }
  ngOnInit(){
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
  }
  renameZone(){
    var params = {
      'zone_id': this.array._id,
      'name': this.zone.name,
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session,params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this.zoneService.renameSubZones(enc_data).then((dec)=>{
      if(dec){
        if(dec.status == 200){
          this._toasterService.success('Zone successfully renamed');
          this.dialogRef.close();
        }
      }else{
        console.log(dec.message)
      }
    
    })
  }
}
