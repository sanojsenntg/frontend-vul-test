import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitupdataComponent } from './splitupdata.component';

describe('SplitupdataComponent', () => {
  let component: SplitupdataComponent;
  let fixture: ComponentFixture<SplitupdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitupdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitupdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
