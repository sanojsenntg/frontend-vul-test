import { Component, Inject, OnInit, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EncDecService } from '../../../common/services/encrypt-decrypt-service/encrypt_decrypt_service';
import { WalletServicesService } from '../../../common/services/wallet/wallet-services.service';
import { ToastsManager } from 'ng2-toastr';
@Component({
  selector: 'app-splitupdata',
  templateUrl: './splitupdata.component.html',
  styleUrls: ['./splitupdata.component.css']
})
export class SplitupdataComponent implements OnInit {
  public orderInfo: any = [];
  public jobData;
  public cash_orders = [];
  public online_orders = [];
  public pos_orders = [];
  public cash_orders_pending = [];
  public online_orders_pending = [];
  public pos_orders_pending = [];
  public loader = true;
  public cash_collected = 0.0;
  public cash_revenue = 0.0;
  public online_collected = 0.0;
  public online_revenue = 0.0;
  public pos_collected = 0.0;
  public pos_revenue = 0.0;
  public cash_in_hand = 0.0;
  public total_revenue = 0.0;
  public total_due = 0.0;
  pageSize = 10;
  public carryovertext = false;
  public companyId: any = [];
  pageNo = 0;
  public is_search = false;
  page = 1;
  email: string;
  session: string;
  constructor(public _walletService: WalletServicesService,
    public dialogRef: MatDialogRef<SplitupdataComponent>,
    vcr: ViewContainerRef,
    public toastr: ToastsManager,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    const company_id: any = localStorage.getItem('user_company');
    this.companyId.push(company_id)
    this.session = localStorage.getItem('Sessiontoken');
    this.email = localStorage.getItem('user_email');
    this.toastr.setRootViewContainerRef(vcr);
    this.orderInfo = data.orderData;
    this.jobData = data.jobData;
    console.log("Orderssss++++" + JSON.stringify(this.jobData));
    if (this.orderInfo.length > 0) {
      this.orderInfo.forEach((element, index, array) => {
        //console.log("payment_type++++" + element.payment_type)
        if (element.payment_type == "1") {
          //console.log("element+++" + JSON.stringify(element))
          let data = { unique_order_id: '', total_amount: 0, driver_revenue: 0, discount_amount: 0, fee_collect: 0, collected_amount: 0 };
          data.unique_order_id = element.unique_order_id;
          data.total_amount = element.total_fare ? element.total_fare.$numberDecimal : 0;
          data.driver_revenue = element.driver_revenue ? element.driver_revenue.$numberDecimal : 0;
          data.discount_amount = element.total_fare.$numberDecimal - parseFloat(element.price);
          data.fee_collect = parseFloat(element.dtc_fee.$numberDecimal) + parseFloat(element.rta_fee.$numberDecimal);
          data.collected_amount = parseFloat(element.price);
          this.cash_collected = this.cash_collected + parseFloat(element.total_fare.$numberDecimal);
          this.cash_in_hand = this.cash_in_hand + parseFloat(element.price);
          this.cash_revenue = this.cash_revenue + parseFloat(element.driver_revenue.$numberDecimal);
          //console.log("data++++" + JSON.stringify(data) + "element+++" + JSON.stringify(element))
          //console.log("cash revenue++" + this.cash_revenue);
          this.cash_orders.push(data);
        } else if (element.payment_type == "2") {
          let data = { unique_order_id: '', total_amount: 0, driver_revenue: 0, discount_amount: 0, fee_collect: 0, collected_amount: 0 };
          data.unique_order_id = element.unique_order_id;
          data.total_amount = element.total_fare.$numberDecimal;
          data.driver_revenue = element.driver_revenue.$numberDecimal;
          data.discount_amount = element.total_fare.$numberDecimal - parseFloat(element.price);
          data.fee_collect = parseFloat(element.dtc_fee.$numberDecimal) + parseFloat(element.rta_fee.$numberDecimal);
          data.collected_amount = parseFloat(element.price);
          this.pos_collected = this.pos_collected + parseFloat(element.total_fare.$numberDecimal);
          this.pos_revenue = this.pos_revenue + parseFloat(element.driver_revenue.$numberDecimal);
          this.pos_orders.push(data);
        }
        else if (element.payment_type == "4") {
          let data = { cybersource_id: '', unique_order_id: '', total_amount: 0, driver_revenue: 0, discount_amount: 0, fee_collect: 0, collected_amount: 0 };
          data.unique_order_id = element.unique_order_id;
          data.total_amount = element.total_fare.$numberDecimal;
          data.driver_revenue = element.driver_revenue.$numberDecimal;
          data.discount_amount = element.total_fare.$numberDecimal - parseFloat(element.price);
          data.fee_collect = parseFloat(element.dtc_fee.$numberDecimal) + parseFloat(element.rta_fee.$numberDecimal);
          data.collected_amount = parseFloat(element.price);
          data.cybersource_id = element.cybersource_id;
          this.online_collected = this.online_collected + parseFloat(element.total_fare.$numberDecimal);
          this.online_revenue = this.online_revenue + parseFloat(element.driver_revenue.$numberDecimal);
          this.online_orders.push(data);
        }
        if (index === (this.orderInfo.length - 1)) {
          console.log("end+++++++++++++++++");
          this.cash_collected = parseFloat(parseFloat(this.cash_collected.toString()).toFixed(2));
          this.pos_collected = parseFloat(parseFloat(this.pos_collected.toString()).toFixed(2));
          this.online_collected = parseFloat(parseFloat(this.online_collected.toString()).toFixed(2));
          this.cash_revenue = parseFloat(parseFloat(this.cash_revenue.toString()).toFixed(2));
          this.online_revenue = parseFloat(parseFloat(this.online_revenue.toString()).toFixed(2));
          this.pos_revenue = parseFloat(parseFloat(this.pos_revenue.toString()).toFixed(2));
          this.cash_in_hand = parseFloat(parseFloat(this.cash_in_hand.toString()).toFixed(2));
          this.total_revenue = this.cash_revenue + this.pos_revenue + this.online_revenue;
          this.total_due = this.total_revenue - this.cash_in_hand;
          this.loader = false;
        }
      });
    } else {
      this.loader = false;
    }
  }
  ngOnInit() {
  }
  fetchPreviousSplitups() {
    console.log("innerjob" + JSON.stringify(this.jobData.previous_unsettled.length));
    if (this.jobData.previous_unsettled.length > 0) {
      //console.log("jobdata++++++" + JSON.stringify(this.jobData));
      let new_params = {
        job_id: this.jobData._id,
      }
      var encrypted2 = this.encDecService.nwt(this.session, new_params);
      var enc_data2 = {
        data: encrypted2,
        email: this.email
      }
      this._walletService.getsplitupDueData(enc_data2).subscribe((dec) => {
        //console.log("Response++++" + JSON.stringify(dec));
        if (dec && dec.status == 200) {
          var res: any = this.encDecService.dwt(this.session, dec.data);
          console.log("orderslen++++" + res.orderdata.length);
          if (res.orderdata.length > 0) {
            res.orderdata.forEach((element, index, array) => {
              //console.log("payment_type++++" + element.payment_type)
              if (element.payment_type == "1") {
                //console.log("element+++" + JSON.stringify(element))
                let data = { unique_order_id: '', total_amount: 0, driver_revenue: 0, discount_amount: 0, fee_collect: 0, collected_amount: 0 };
                data.unique_order_id = element.unique_order_id;
                data.total_amount = element.total_fare ? element.total_fare.$numberDecimal : 0;
                data.driver_revenue = element.driver_revenue ? element.driver_revenue.$numberDecimal : 0;
                data.discount_amount = element.total_fare.$numberDecimal - parseFloat(element.price);
                data.fee_collect = parseFloat(element.dtc_fee.$numberDecimal) + parseFloat(element.rta_fee.$numberDecimal);
                data.collected_amount = parseFloat(element.price);
                this.cash_orders_pending.push(data);
              } else if (element.payment_type == "2") {
                let data = { unique_order_id: '', total_amount: 0, driver_revenue: 0, discount_amount: 0, fee_collect: 0, collected_amount: 0 };
                data.unique_order_id = element.unique_order_id;
                data.total_amount = element.total_fare.$numberDecimal;
                data.driver_revenue = element.driver_revenue.$numberDecimal;
                data.discount_amount = element.total_fare.$numberDecimal - parseFloat(element.price);
                data.fee_collect = parseFloat(element.dtc_fee.$numberDecimal) + parseFloat(element.rta_fee.$numberDecimal);
                data.collected_amount = parseFloat(element.price);
                this.pos_orders_pending.push(data);
              }
              else if (element.payment_type == "4") {
                let data = { cybersource_id: '', unique_order_id: '', total_amount: 0, driver_revenue: 0, discount_amount: 0, fee_collect: 0, collected_amount: 0 };
                data.unique_order_id = element.unique_order_id;
                data.total_amount = element.total_fare.$numberDecimal;
                data.driver_revenue = element.driver_revenue.$numberDecimal;
                data.discount_amount = element.total_fare.$numberDecimal - parseFloat(element.price);
                data.fee_collect = parseFloat(element.dtc_fee.$numberDecimal) + parseFloat(element.rta_fee.$numberDecimal);
                data.collected_amount = parseFloat(element.price);
                data.cybersource_id = element.cybersource_id;
                this.online_orders_pending.push(data);
              }
              if (index === (this.orderInfo.length - 1)) {
                console.log("end+++++++++++++++++");
              }
            });
          } else {
            this.carryovertext = true;
          }
        } else {
          this.toastr.error(dec.message);
        }
      })
    } else {
      this.toastr.success("No Previous unsettled records");
    }
  }
}
