import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { DriverService } from '../../services/driver/driver.service';
import { VehicleService } from '../../services/vehicles/vehicle.service';
import { CoverageareaService } from '../../services/coveragearea/coveragearea.service';
import { OrderService } from '../../services/order/order.service';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from "../../services/api/jwt.service";
import { environment } from "../../../../environments/environment";
import { EncDecService } from '../../services/encrypt-decrypt-service/encrypt_decrypt_service';
import { VehicleModelsService } from '../../../common/services/vehiclemodels/vehiclemodels.service';

@Component({
  selector: 'app-reassign',
  templateUrl: './reassign.component.html',
  styleUrls: ['./reassign.component.css']
})
export class ReassignComponent implements OnInit {
  public t4;
  public companyId: any = [];
  public categorylist: any = [];
  public orderInfo;
  public vehicleData;
  public driverData;
  public coverageData: any = [];
  public vehicle;
  public driver;
  public cancelOrdermsg;
  public CategoryData;
  public imageurl;
  public customer_app;
  public allowreassign = true;
  session;
  vehicleCtrl: FormControl = new FormControl();
  driverCtrl: FormControl = new FormControl();
  vehicle_category: FormControl = new FormControl();
  message: any;
  constructor(private _jwtService: JwtService,
    public encDecService: EncDecService,
    private _vehicleModelsService: VehicleModelsService,
    public _coverageAreaService: CoverageareaService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<ReassignComponent>, public _orderService: OrderService, public toastr: ToastsManager, public _driverService: DriverService, public _vehicleService: VehicleService, private router: Router, @Inject(MAT_DIALOG_DATA) public data: any,
    vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    this.orderInfo = data;
    console.log(this.orderInfo);
    //console.log(JSON.stringify(this.orderInfo));
    if (this.orderInfo.generate_by === "customer" || this.orderInfo.generate_by === "delivery") {
      this.customer_app = true;
      this.categorylist.push(this.orderInfo.vehicle_category_ids[0].unique_category_id)
      if (this.orderInfo.order_status === "scheduled_trip" || this.orderInfo.order_status === "dispatcher_cancel" || this.orderInfo.order_status === "order_timed_out" || this.orderInfo.order_status === "customer_cancel" || this.orderInfo.order_status === "driver_cancel" || this.orderInfo.order_status === "search_for_driver" || this.orderInfo.order_status === "search_for_driver_manual" || this.orderInfo.order_status === "rejected_by_driver") {
        this.allowreassign = true;
      } else {
        this.allowreassign = false;
      }
    }
    this.imageurl = environment.imgUrl;
    this.orderInfo.company_id.forEach(element => {
      this.companyId.push(element)
    });
    this.session = localStorage.getItem('Sessiontoken');
  }
  ngOnInit() {
    //this.getDrivers();
    //this.getVehicles();
    if (this.orderInfo.generate_by === "customer" || this.orderInfo.generate_by === "delivery") {
      this.customer_app = true;
      this.getVehiclesCategories();
    }
    this.driverCtrl.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        const params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          company_id: this.companyId,
          search: query,
        };
        if (this.customer_app) {
          params['vehicle_cat_id'] = this.categorylist;
        }
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._driverService.getLoginDriverByName(enc_data)
          .subscribe(dec => {
            if (dec) {
              if (dec.status == 200) {
                var result: any = this.encDecService.dwt(this.session, dec.data);
                this.driverData = result.drivers
                //console.log("DRIVERS++++++++" + JSON.stringify(this.driverData));
              }
            } else {
              console.log(dec.message)
            }

          });
      });
    this.vehicleCtrl.valueChanges
      .debounceTime(500)
      .subscribe((query) => {
        const params = {
          offset: 0,
          limit: 10,
          sortOrder: 'desc',
          sortByColumn: '_id',
          company_id: this.companyId,
          search_keyword: query,
        };
        if (this.customer_app) {
          params['vehicle_cat_id'] = this.categorylist;
        }
        //console.log(JSON.stringify(params));
        var encrypted = this.encDecService.nwt(this.session, params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._vehicleService.getOccupiedVehicle(enc_data)
          .subscribe(dec => {
            if (dec) {
              if (dec.status == 200) {
                var result: any = this.encDecService.dwt(this.session, dec.data);
                this.vehicleData = result.searchVehicle;
                //console.log("Vehiles++++++++" + this.vehicleData.length);
              }
            } else {
              console.log(dec.message)
            }

          });
      })
  }
  public getAllCoverageArea() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._coverageAreaService.getCoverageAreas(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.coverageData = data;
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  public getDrivers() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    if (this.customer_app) {
      params['vehicle_cat_id'] = this.categorylist;
    } else {
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._driverService.getDriverListing(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.driverData = data.drivers;
        }
      } else {
        console.log(dec.message)
      }

    });
  }
  public getVehicles() {
    const params = {
      offset: 0,
      limit: 10,
      sortOrder: 'desc',
      sortByColumn: '_id',
      company_id: this.companyId
    };
    if (this.customer_app) {
      params['vehicle_cat_id'] = this.categorylist;
    } else {
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._vehicleService.getOccupiedVehicle(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.vehicleData = data.searchVehicle;
        }
      } else {
        console.log(dec.message)
      }

    });
  }
  public getVehiclesCategories() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'updated_at',
      search: '',
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._vehicleModelsService.getVehicleCategories(enc_data).then((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.CategoryData = data.vehicleModelsList;
        }
      } else {
        console.log(dec.message)
      }

    });
  }
  OnSelectVehicle(data) {
    //console.log(data);
    this.t4 = data.t4;
    var params = {
      company_id: this.companyId,
      search_by_driverid: data.source.value.driver_id
    }
    //console.log(JSON.stringify(params));
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._driverService.getLoginDriverById
      (enc_data).subscribe((dec) => {
        if (dec) {
          if (dec.status == 200) {
            var data_driverService: any = this.encDecService.dwt(this.session, dec.data);
            //console.log(JSON.stringify(data_driverService));
            this.driver = data_driverService.drivers[0];
          }
        } else {
          console.log(dec.message)
        }
      });
  }
  OnSelectDriver(data) {
    this.t4 = data.t4;
    var params = {
      company_id: this.companyId,
      driver_id: data.source.value._id
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._vehicleService.getVehicleByDriverId(enc_data).subscribe((dec) => {
      if (dec) {
        if (dec.status == 200) {
          var datab: any = this.encDecService.dwt(this.session, dec.data);
          //    console.log(JSON.stringify(datab));
          this.vehicle = datab.getSingleDriver ? datab.getSingleDriver[0] : [];
          //    console.log(this.vehicle);
        }
      } else {
        console.log(dec.message)
      }

    });
  }
  displayFnvehicle(data): string {
    return data ? data.vehicle_unique_id + ' ' + data.display_name : data;
  }
  displayFnDriver(data): string {
    return data ? data.username : data;
  }
  saveReassignOrder() {
    if (this.allowreassign) {
      if (this.customer_app) {
        if (!this.driver || !this.vehicle) {
          this.toastr.error("Please fill all fields!!");
          this.message = "Please fill all fields!!";
        } else {
          let params = {
            driver_id: this.driver ? this.driver._id : null,
            vehicle_id: this.vehicle ? this.vehicle._id : null,
            t4: this.t4,
            order_id: this.orderInfo._id,
            user_id: this.orderInfo.user_id,
            email: localStorage.getItem('user_email')
          }
          var encrypted = this.encDecService.nwt(this.session, params);
          var enc_data = {
            data: encrypted,
            email: localStorage.getItem('user_email')
          }
          if(this.orderInfo.generate_by == 'customer'){
            this._orderService.reassignCustomerAppOrder(enc_data)
            .then((dec) => {
              if (dec) {
                this.message = dec.message;
                if (dec.status === 201) {
                  this.toastr.error("Order creation failed!!");
                }
                else if (dec.status === 202) {
                  this.toastr.error("Driver has order!!");
                }
                else if (dec.status === 203) {
                  this.toastr.error("Customer has order!!");
                }
                else if (dec.status == 200) {
                  this.toastr.success('Order is successfully reassigned.');
                  setTimeout(() => {
                    this.dialogRef.close();
                  }, 2000);
                }
              }
            })
            .catch((error) => {
              setTimeout(() => {
                this.dialogRef.close();
              }, 2000);
            });
          }else{
            this._orderService.reassignDeliveryOrder(enc_data).then((dec) => {
              if (dec) {
                this.message = dec.message;
                if (dec.status == 200) {
                  this.toastr.success(this.message);
                  setTimeout(() => {
                    this.dialogRef.close();
                  }, 2000);
                }else if (dec.status != 200) {
                  this.toastr.error(this.message);
                }
              }
            })
            .catch((error) => {
              setTimeout(() => {
                this.dialogRef.close();
              }, 2000);
            });
          }
        }
      } else {
        //var allow_order = this.checkOrderIsAllowed(this.addDispatcher.value.destinationLatLang, this.addDispatcher.value.sourceLatLang);
        if (!this.driver || !this.vehicle || (this.orderInfo.tariff_id && this.orderInfo.tariff_id._id == '5b9e11e5eb5b0211b4156e27' && this.orderInfo.price <= 0)) {
          this.toastr.error("Please fill all fields!!");
        }
        const orderdata = {
          'phone_number': this.orderInfo.user_id ? this.orderInfo.user_id.phone_number : '',
          'firstname': this.orderInfo.user_id ? this.orderInfo.user_id.firstname : '',
          'lastname': this.orderInfo.user_id ? this.orderInfo.user_id.lastname : '',
          'dispatcher_id': this.orderInfo.dispatcher,
          'is_scheduler': this.orderInfo.is_scheduler == '1' ? 1 : 0,
          'scheduler_start_date': this.orderInfo.scheduler_start_date ? this.orderInfo.scheduler_start_date : '',
          'pickup_location': this.orderInfo.pickup_location ? this.orderInfo.pickup_location : '',
          'distance': this.orderInfo.distance ? this.orderInfo.distance : '',
          'drop_location': this.orderInfo.drop_location ? this.orderInfo.drop_location : '',
          'partner_id': this.orderInfo.partner_id ? this.orderInfo.partner_id._id : '',
          'vehicle_id': this.vehicle ? this.vehicle._id : '',
          'driver_id': this.driver ? this.driver._id : '',
          'tariff_id': this.orderInfo.tariff_id ? this.orderInfo.tariff_id._id : '',
          'additional_service_id': this.orderInfo.additional_service_id ? this.orderInfo.additional_service_id._id : '',
          'payment_extra_id': this.orderInfo.payment_extra_ids ? this.orderInfo.payment_extra_ids : '',
          'order_type': false,
          'price': this.orderInfo.price ? this.orderInfo.price : '',
          'payment_type_id': this.orderInfo.payment_type ? this.orderInfo.payment_type : '',
          'order_group_id': this.orderInfo.OrderGroup ? this.orderInfo.OrderGroup._id : '',
          'message': this.orderInfo.message ? this.orderInfo.message : '',
          'customer_drop_lat_long': this.orderInfo.customer_drop_lat_long ? this.orderInfo.customer_drop_lat_long : '',
          'customer_pickup_lat_long': this.orderInfo.customer_pickup_lat_long ? this.orderInfo.customer_pickup_lat_long : '',
          'vehicle_model_ids': this.orderInfo.vehicle_model_ids ? this.orderInfo.vehicle_model_ids : '',
          'driver_groups': this.orderInfo.driver_groups ? this.orderInfo.driver_groups : [],
          't3': this.orderInfo.t3 ? this.orderInfo.t3 : [],
          'previous_order_id': this.orderInfo._id ? this.orderInfo._id : '',
          't5': this.orderInfo.t5 ? this.orderInfo.t5 : '',
          'payment_type_identifier': this.orderInfo.payment_type_identifier ? this.orderInfo.payment_type_identifier : '',
          'company_id': window.localStorage['user_company'],
          'hourlyend': this.orderInfo.hourlyend ? this.orderInfo.hourlyend : '',
          'isHourlyOrder': this.orderInfo.isHourlyOrder ? 1 : 0
        };
        this.cancelOrder(this.orderInfo._id, this.orderInfo.order_status, orderdata)
      }
    } else {
      this.toastr.error("Reassigning failed.Cannot reassign order now")
    }
  }
  closePop() {
    this.dialogRef.close();
  }
  public cancelOrder(orderId, order_status, orderdata) {
    if (order_status == 'dispatcher_cancel' || order_status == 'completed' || order_status == 'ended_by_admin' || order_status == 'cancel_acknowledged' || order_status == 'customer_cancel') {
      var encrypted = this.encDecService.nwt(this.session, orderdata);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._orderService.saveOrder(enc_data, orderdata.orderTypeFlag)
        .then((dec) => {
          if (dec) {
            this.message = dec.message;
            if (dec.status === 202) {
              this.toastr.error("Driver has order!!");
              setTimeout(() => {
                this.dialogRef.close();
              }, 2000);
            }
            else if (dec.status === 200) {
              this.toastr.success('Order is successfully reassigned.');
              setTimeout(() => {
                this.dialogRef.close();
              }, 2000);
            }
            else {
              this.toastr.error("Order creation failed!!");
              setTimeout(() => {
                this.dialogRef.close();
              }, 2000);
            }
          }
        })
        .catch((error) => {
          console.log('error', error);
          setTimeout(() => {
            this.dialogRef.close();
          }, 2000);
        });
    } else {
      var user = JSON.parse(window.localStorage['dispatcherUser']);
      var params = {
        email: user.email,
        _id: orderId,
        company_id: localStorage.getItem('user_company')
      }
      var encrypted = this.encDecService.nwt(this.session, params);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._orderService.cancelOrder(enc_data).then((dec) => {
        if (dec && dec.status == 200) {
          this.toastr.success('Order cancellation is successfully done.');
          this.message=dec.message;
          let enc_data = this.encDecService.nwt(this.session, orderdata);
          //alert(this.dispatcher_id.email)
          let data1 = {
            data: enc_data,
            email: localStorage.getItem('user_email')
          }
          this._orderService.saveOrder(data1, orderdata.orderTypeFlag)
            .then((res) => {
              if (!res)
                return;
              if (res.status === 202) {
                this.toastr.error('Driver has order!!');
                setTimeout(() => {
                  this.dialogRef.close();
                }, 1000);
              }
              else if (res.status === 201) {
                this.toastr.error('Order creation failed!!');
                setTimeout(() => {
                  this.dialogRef.close();
                }, 1000);
              }
              else {
                this.toastr.success('Order is successfully reassigned.');
                setTimeout(() => {
                  this.dialogRef.close();
                }, 1000);
              }
            })
            .catch((error) => {
              setTimeout(() => {
                this.dialogRef.close();
              }, 2000);
            });
        } else {
          this.message=dec?dec.message:'Error while Order cancellation!';
          this.toastr.warning('Error while Order cancellation!');
          setTimeout(() => {
            this.dialogRef.close();
          }, 1000);
          this.dialogRef.close();
        }
      });
    }
  }
  /*public checkOrderIsAllowed(droplocation, sourcelocation) {
    //var dest_latlngarray = droplocation.split(',');
    //var dlat = parseFloat(dest_latlngarray[0]);
    //var dlng = parseFloat(dest_latlngarray[1]);
    var source_latlngarray = sourcelocation.split(',');
    var slat = parseFloat(source_latlngarray[0]);
    var slng = parseFloat(source_latlngarray[1]);
    //var destlatlng = new google.maps.LatLng(dlat, dlng);
    var sourcelatlng = new google.maps.LatLng(slat, slng);
    //alert(sourcelatlng);
    let coverage_array = this.coverageData.coverageArea;
    //alert(JSON.stringify(coverage_array));
    if (coverage_array.length > 0) {
      for (var i = 0; i < coverage_array.length; ++i) {
        if (coverage_array[i].bounds.length > 0 && coverage_array[i].bounds != "") {
          let bounds = coverage_array[i].bounds;
          //alert(JSON.stringify(bounds));
          let bounds_array = [];
          bounds_array.length = 0;
          for (var j = 0; j < bounds.length; ++j) {
            bounds_array.push(new google.maps.LatLng(bounds[j].lat, bounds[j].lng))
          }
          //alert(JSON.stringify(bounds_array));
          var bermudaTriangle = new google.maps.Polygon({
            paths: bounds_array
          });
          //var destination_inside_value = google.maps.geometry.poly.containsLocation(destlatlng, bermudaTriangle)
          var source_inside_value = google.maps.geometry.poly.containsLocation(sourcelatlng, bermudaTriangle)
          //alert(source_inside_value);
          if (i === coverage_array.length - 1) {
            if (!source_inside_value) {
              return false;
            } else {
              return true;
            }
          }
        }
      }
    } else {
      return true;
    }

  }*/
}
