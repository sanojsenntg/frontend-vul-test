import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FreeVehicleService } from '../../services/free-vehicles/free_vehicle.service';
import { GlobalService } from '../../services/global/global.service';
import { EncDecService } from '../../services/encrypt-decrypt-service/encrypt_decrypt_service';
import { offset } from 'highcharts';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  public freeVehicles = [];
  public sosAlerts: any = [];
  public orderAlerts: any = [];
  public timedOutAlerts: any = [];
  public overSpeed: any = [];
  public notificationCount = { devices: 0, scheduled: 0, timedout: 0, overspeed: 0 };
  public companyId: any = [];
  public isAlert = false;
  public alert = [];
  public uaeTime;
  public session;
  public limit = 15;
  public offset = 0;
  public inactiveCars:any = []; 
  public inactiveCarsLength;
  public scrollCall = false;
  constructor(
    public dialogRef: MatDialogRef<NotificationComponent>,
    public _FreeVehicleService: FreeVehicleService,
    public _global: GlobalService,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
    if (data != '1') {
      this.isAlert = true;
      this.alert = data;
    }
  }
  ngOnInit() {
    this._global.observableLog.subscribe(item => {
      if (item)
        this.dialogRef.close();
    })
    if (!this.isAlert) {

      const params1 = {
        'idle_for': 1,
        limit: 3,
        company_id: this.companyId
      };
      var encrypted = this.encDecService.nwt(this.session, params1);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      // this._FreeVehicleService.getFreeVehicle(enc_data).then((dec) => {
      //   if (dec) {
      //     if (dec.status == 200) {
      //       var res: any = this.encDecService.dwt(this.session, dec.data);
      //       this.freeVehicles = res.myFreeVehicles;
      //       this._FreeVehicleService.setFreeVehicle(this.freeVehicles.length)
      //     } else {
      //       console.log(dec.message)
      //     }
      //   }
      // });

      this._FreeVehicleService.observableDeviceList.subscribe(item => {
        this.sosAlerts = item.vehicleStatus;
      });
      this._FreeVehicleService.observableOrderList.subscribe(item => {
        if (item.length != 0)
          this.orderAlerts = item.Scheduled;
      });
      this._FreeVehicleService.observableTimedOutList.subscribe(item => {
        if (item.length != 0)
          this.timedOutAlerts = item.timed_out;
      });
      this._FreeVehicleService.observableCount.subscribe(item => {
        this.notificationCount = item;
      });
      this._FreeVehicleService.observableOverSpeedList.subscribe(item => {
        if (item.length != 0)
          this.overSpeed = item;
      });
      let that = this;
      setInterval(function () {
        that.uaeTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Dubai" });
      }, 1000);
    }
    //   const params2 = {'type': 'sos' };
    //   this._FreeVehicleService.getAlert(params2).then((res) => {
    //       console.log(res);
    //       this.sosAlerts = res.myAlerts;
    //       console.log(this.sosAlerts);
    //   });

    //   const params3 = {'type': 'lunch_break' };
    //   this._FreeVehicleService.getAlert(params3).then((res) => {
    //       console.log(res);
    //       this.breakAlerts = res.myAlerts;
    //       console.log(this.breakAlerts);
    //   });
    //   const params4 = {'type': 'speed' };
    //   this._FreeVehicleService.getAlert(params4).then((res) => {
    //       console.log(res);
    //       this.speedAlerts = res.myAlerts;
    //       console.log(this.speedAlerts);
    //   });
    this.inactiveVehicles();
  }
  @HostListener('scroll', ['$event']) 
  scrollHandler(event: Event) {
    let tracker:any = event.target;
    let current = (event.target as HTMLElement).scrollTop;
    if(current+3 >= (tracker.scrollHeight - tracker.clientHeight) && !this.scrollCall){
      this.offset = this.offset + this.limit;
      if(this.inactiveCarsLength >= this.limit){
        this.inactiveVehicles();
      }
    }
  }
  public inactiveVehicles(){
    this.scrollCall = true;
    var params = {
      offset: this.offset,
      limit: this.limit,
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    console.log(params);
    this._FreeVehicleService.getInactiveVehicles(enc_data).then((dec)=>{
      this.scrollCall = false;
      var data:any =  this.encDecService.dwt(this.session, dec.data);
      this.inactiveCarsLength = data.count;
      let cars = data.data;
      cars.forEach(element => {
        this.inactiveCars.push(element);
      });
    })
  }
  public since(dt: any) {
    let date1 = new Date(dt).getTime();
    let date2 = new Date(this.uaeTime).getTime();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";

    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output += Math.round(var1) + " secs";
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      output += Math.round(var1) + " mins";
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      output += Math.round(var1) + " hrs";
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output += Math.round(var1) + " days";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output += Math.round(var1) + " months";
    }
    return hoursDiff < 0 ? '0 secs' : output;
  }
  borderColor(dt: any, status) {
    switch (status) {
      case 1:
        let date1 = new Date(dt).getTime();
        let date2 = new Date(this.uaeTime).getTime();
        let time = date2 - date1; //msec
        let hoursDiff = time / 1000; //secs
        var output = "";
        if (hoursDiff <= 60 * 30) {
          output = "5px solid green";
        } else if (hoursDiff < 60 * 60) {
          output = "5px solid orange";
        } else {
          output = "5px solid red";
        }
        break;
      case 2:
        break;
      case 3:
        let date3 = new Date(dt).getTime();
        let date4 = new Date(this.uaeTime).getTime();
        let time2 = date3 - date4; //msec
        let hoursDiff2 = time2 / 1000; //secs
        var output = "";
        if (hoursDiff2 < 60 * 30) {
          output = "5px solid red";
        } else if (hoursDiff2 < 60 * 60) {
          output = "5px solid orange";
        } else {
          output = "5px solid green";
        }
        break;
      case 4:
        if (dt == 'customer' || dt == 'website') {
          output = "5px solid red";
        } else if (dt == 'dispatcher') {
          output = "5px solid orange";
        } else {
          output = "5px solid green";
        }
        break;
      default:
        break;
    }
    return output;
  }
  public toGo(dt: any) {
    let date2 = new Date(dt).getTime();
    let date1 = new Date(this.uaeTime).getTime();
    let time = date2 - date1; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";

    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output += Math.round(var1) + " secs";
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      output += Math.round(var1) + " mins";
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      output += Math.round(var1) + " hrs";
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output += Math.round(var1) + " days";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output += Math.round(var1) + " months";
    }
    return hoursDiff < 0 ? '0 secs' : output;
  }
  iconColor(dt: any, status) {
    switch (status) {
      case 1:
        let date1 = new Date(dt).getTime();
        let date2 = new Date(this.uaeTime).getTime();
        let time = date2 - date1; //msec
        let hoursDiff = time / 1000; //secs
        var output = "";
        if (hoursDiff <= 60 * 30) {
          output = "green";
        } else if (hoursDiff < 60 * 60) {
          output = "orange";
        } else {
          output = "red";
        }
        break;
      case 2:
        break;
      case 3:
        let date3 = new Date(dt).getTime();
        let date4 = new Date(this.uaeTime).getTime();
        let time2 = date3 - date4; //msec
        let hoursDiff2 = time2 / 1000; //secs
        var output = "";
        if (hoursDiff2 < 60 * 30) {
          output = "red";
        } else if (hoursDiff2 < 60 * 60) {
          output = "orange";
        } else {
          output = "green";
        }
        break;
      case 4:
        if (dt == 'customer') {
          output = "red";
        } else if (dt == 'dispatcher') {
          output = "orange";
        } else {
          output = "green";
        }
        break;
      default:
        break;
    }
    return output;
  }
  close(id) {
    this._FreeVehicleService.setOrderId(id);
    this.dialogRef.close();
  }

  nonOccvehic(cars): void {
    this._FreeVehicleService.findnoOccvehicle(cars);
    this.dialogRef.close();
  }
  
  closeAll(){
    this._global.setLogoutStatus(true);
    setTimeout(()=>{ 
      this._global.setLogoutStatus(false);
    },2000)
  }
}
