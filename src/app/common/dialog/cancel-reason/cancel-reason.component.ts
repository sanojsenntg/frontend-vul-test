import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { OrderService } from '../../../common/services/order/order.service';
import { EncDecService } from '../../services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-cancel-reason',
  templateUrl: './cancel-reason.component.html',
  styleUrls: ['./cancel-reason.component.css']
})
export class CancelReasonComponent implements OnInit {
  public companyId: any = [];
  public cancelReasonlModel: any = {
    cancelreason: '',
    cancelText: '',
    gender: '',
    checked: false
  };

  public reasonsItems: any;
  public cancelItems: any;
  public status: any;
  public session;
  constructor(
    formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CancelReasonComponent>,
    private _orderService: OrderService,
    public encDecService: EncDecService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.status = this.data.order_status;
    this.companyId.push( localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.cancelItems = {
      type: this.data.type,
      company_id: this.companyId
    };
    var encrypted = this.encDecService.nwt(this.session, this.cancelItems);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._orderService.getCancelReasons(enc_data).then((dec) => {
      if(dec){
        if(dec.status == 200){
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.reasonsItems = data.reasons;
        }
      }else{
        console.log(dec.message)
      }
    });
  }

  ngOnInit() {
  }

  reasonPopup(formData): void {

  };

}
