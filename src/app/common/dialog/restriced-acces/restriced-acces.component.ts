import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-restriced-acces',
  templateUrl: './restriced-acces.component.html',
  styleUrls: ['./restriced-acces.component.css']
})
export class RestricedAccesComponent{

  constructor(
    public dialogRef: MatDialogRef<RestricedAccesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }

}
