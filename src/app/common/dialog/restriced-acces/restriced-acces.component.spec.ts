import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestricedAccesComponent } from './restriced-acces.component';

describe('RestricedAccesComponent', () => {
  let component: RestricedAccesComponent;
  let fixture: ComponentFixture<RestricedAccesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestricedAccesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestricedAccesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
