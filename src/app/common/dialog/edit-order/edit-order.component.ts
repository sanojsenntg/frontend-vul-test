import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JwtService } from '../../services/api/jwt.service';
import { ToastsManager } from "ng2-toastr";
import { Router } from '@angular/router';
import * as moment from "moment/moment";
import { CoverageareaService } from '../../services/coveragearea/coveragearea.service';
import { OrderService } from '../../services/order/order.service';
import { EncDecService } from '../../services/encrypt-decrypt-service/encrypt_decrypt_service';


@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit {
  public orderInfo: any = [];
  public companyId: any = [];
  public dispatcher_id;
  public max2 = new Date(Date.now());
  public pickupLang;
  public pickupLat;
  public session;
  public coverageData: any = [];
  invalid: string;
  constructor(private _orderService: OrderService,
    public encDecService: EncDecService,
    public toastr: ToastsManager, public dialogRef: MatDialogRef<EditOrderComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private _jwtService: JwtService, vcr: ViewContainerRef, private router: Router
    , private _coverageAreaService: CoverageareaService) {
    this.toastr.setRootViewContainerRef(vcr);
    console.log(data)
    this.orderInfo = data.SingleDetail;
    if (window.localStorage["dispatcherUser"])
      this.dispatcher_id = JSON.parse(window.localStorage["dispatcherUser"]);
    //this.orderInfo.scheduler_start_date=moment(this.orderInfo.scheduler_start_date).format('M/DD/YYYY, h:mm a');
    this.orderInfo.scheduler_start_date = new Date(this.orderInfo.scheduler_start_date);
    this.orderInfo.schedule_before = '';
    const slatlng = this.orderInfo.customer_pickup_lat_long.split(",")
    this.pickupLang = slatlng[1];
    this.pickupLat = slatlng[0];
  }

  ngOnInit() {
    this.companyId.push(localStorage.getItem('user_company'))
    this.session = localStorage.getItem('Sessiontoken');
    this.getAllCoverageArea()
    this.initialize()
  }
  updateOrder(formData): void {
    this.invalid = '';
    if (formData.valid) {
      if (!this.checkOrderIsAllowed())
        return;
      let param = {
        dispatcher_id: this.dispatcher_id._id,
        scheduler_start_date: moment(this.orderInfo.scheduler_start_date).format("YYYY-MM-DD HH:mm:ss"),
        schedule_before: this.orderInfo.schedule_before ? this.orderInfo.schedule_before : '',
        pickup_location: this.orderInfo.pickup_location,
        drop_location: this.orderInfo.drop_location,
        customer_drop_lat_long: this.orderInfo.customer_drop_lat_long
          ? this.orderInfo.customer_drop_lat_long
          : "",
        customer_pickup_lat_long: this.orderInfo.customer_pickup_lat_long
          ? this.orderInfo.customer_pickup_lat_long
          : "",
        order_id: this.orderInfo._id,
        dispatch_now: false,// for rescheduling done by user history
        disp_email: this.dispatcher_id.email,
        company_id: this.companyId
      }
      var encrypted = this.encDecService.nwt(this.session, param);
      var enc_data = {
        data: encrypted,
        email: localStorage.getItem('user_email')
      }
      this._orderService.editScheduledOrder(enc_data).then(dec => {
        if (dec && dec.status == 200) {
          this.toastr.success('Order updated successfully');
          this.invalid = 'Order updated successfully'
          setTimeout(() => {
            this.dialogRef.close();
          }, 1000)
        } else {
          this.toastr.error(dec ? dec.message : 'Something went wrong');
          this.invalid = dec ? dec.message : 'Something went wrong';
          setTimeout(() => {
            this.dialogRef.close();
          }, 1000)
        }
      });
    }
    else {
      this.toastr.error("Please fill sll fields");
    }
  }
  getScheduledTime(myEndDateTime, durationInMinutes) {
    var myStartDate = new Date(myEndDateTime);
    return moment(
      new Date(
        myStartDate.setMinutes(myStartDate.getMinutes() - durationInMinutes)
      )
    ).format("YYYY-MM-DD, h:mm:ss a");
  }
  public pickupChanged(place) {
    console.log(place)
    this.pickupLat = place.geometry.location.lat();
    this.pickupLang = place.geometry.location.lng();
    this.orderInfo.pickup_location =
      place.name
        ? place.name + " " + place.formatted_address
        : place.formatted_address;
  }
  public dropChanged(place) {
    console.log(place)
    this.orderInfo.drop_location = place.name
      ? place.name + " " + place.formatted_address
      : place.formatted_address;
    const lat = place.geometry.location.lat();
    const lng = place.geometry.location.lng();
    this.orderInfo.customer_drop_lat_long = lat + "," + lng;
  }
  public getAllCoverageArea() {
    var params = {
      company_id: this.companyId
    }
    var encrypted = this.encDecService.nwt(this.session, params);
    var enc_data = {
      data: encrypted,
      email: localStorage.getItem('user_email')
    }
    this._coverageAreaService.getCoverageAreas(enc_data).then(dec => {
      if (dec) {
        if (dec.status == 200) {
          var data: any = this.encDecService.dwt(this.session, dec.data);
          this.coverageData = data;
        }
      } else {
        console.log(dec.message)
      }
    });
  }
  public checkOrderIsAllowed() {
    var slat = this.pickupLat;
    var slng = this.pickupLang;
    this.orderInfo.customer_pickup_lat_long = slat + "," + slng;
    //var destlatlng = new google.maps.LatLng(dlat, dlng);
    var sourcelatlng = new google.maps.LatLng(slat, slng);
    //alert(sourcelatlng);
    let coverage_array = this.coverageData.coverageArea;
    if (coverage_array.length > 0) {
      for (var i = 0; i < coverage_array.length; ++i) {
        if (
          coverage_array[i].bounds.length > 0 &&
          coverage_array[i].bounds != ""
        ) {
          let bounds = coverage_array[i].bounds;
          //alert(JSON.stringify(bounds));
          let bounds_array = [];
          bounds_array.length = 0;
          for (var j = 0; j < bounds.length; ++j) {
            bounds_array.push(
              new google.maps.LatLng(bounds[j].lat, bounds[j].lng)
            );
          }
          //alert(JSON.stringify(bounds_array));
          var bermudaTriangle = new google.maps.Polygon({
            paths: bounds_array
          });
          //var destination_inside_value = google.maps.geometry.poly.containsLocation(destlatlng, bermudaTriangle)
          var source_inside_value = google.maps.geometry.poly.containsLocation(
            sourcelatlng,
            bermudaTriangle
          );
          //alert(source_inside_value);
          if (i === coverage_array.length - 1) {
            if (!source_inside_value) {
              this.toastr.error("Trips starting outside coverage area not allowed");
              return false;
            } else {
              return true;
            }
          }
        }
      }
    } else {
      return true;
    }
  }
  initialize() {
    var options = {
      componentRestrictions: { country: "ae" }
    };
    var input: any = document.getElementById('searchTextField');
    var input2: any = document.getElementById('searchTextField2');
    var a1 = new google.maps.places.Autocomplete(input, options);
    var a2 = new google.maps.places.Autocomplete(input2, options);
    let that = this;
    google.maps.event.addListener(a1, 'place_changed', function () {
      var place = a1.getPlace();
      that.pickupLat = place.geometry.location.lat();
      that.pickupLang = place.geometry.location.lng();
      that.orderInfo.pickup_location =
        place.name
          ? place.name + " " + place.formatted_address
          : place.formatted_address;
    });
    google.maps.event.addListener(a2, 'place_changed', function () {
      var place2 = a2.getPlace();
      that.orderInfo.drop_location = place2.name
        ? place2.name + " " + place2.formatted_address
        : place2.formatted_address;
      const lat = place2.geometry.location.lat();
      const lng = place2.geometry.location.lng();
      that.orderInfo.customer_drop_lat_long = lat + "," + lng;
    });
  }
}
