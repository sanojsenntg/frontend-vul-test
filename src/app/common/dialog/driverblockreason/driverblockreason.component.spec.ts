import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverblockreasonComponent } from './driverblockreason.component';

describe('DriverblockreasonComponent', () => {
  let component: DriverblockreasonComponent;
  let fixture: ComponentFixture<DriverblockreasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverblockreasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverblockreasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
