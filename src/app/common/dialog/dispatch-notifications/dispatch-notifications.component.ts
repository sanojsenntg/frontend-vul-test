import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { FileHolder } from 'angular2-image-upload';
import * as moment from 'moment/moment';
import { CustomerService } from '../../services/customer/customer.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../../services/encrypt-decrypt-service/encrypt_decrypt_service';

@Component({
  selector: 'app-dispatch-notifications',
  templateUrl: './dispatch-notifications.component.html',
  styleUrls: ['./dispatch-notifications.component.css']
})
export class DispatchNotificationsComponent implements OnInit {

  pushImg;
  spandata;
  cAppNotify: FormGroup;
  pushNotify: FormGroup;
  smsNotify: FormGroup;
  emailNotify: FormGroup;
  session;
  companyId = [];
  selectedValue = 'customer';
  submitButton = 'Submit';
  time = 10;
  constructor(
    public formBuilder: FormBuilder,
    public _customerService:CustomerService,
    public toastr:ToastsManager,
    public encDecService:EncDecService,
    public dialogRef: MatDialogRef<DispatchNotificationsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.cAppNotify = this.formBuilder.group({
      title: ['',[Validators.required]],
      description:['',[Validators.required]],
      link:['',[Validators.required]],
      expiry:['',[Validators.required]]
    })
    this.pushNotify = this.formBuilder.group({
      title: ['',[Validators.required]],
      description: ['',[Validators.required]]
    })
    this.smsNotify = this.formBuilder.group({
      message: ['',[Validators.required]]
    })
    this.emailNotify = this.formBuilder.group({
      title: ['',[Validators.required]],
      description: ['',[Validators.required]]
    })
    this.session = localStorage.getItem('Sessiontoken');
    this.companyId.push(localStorage.getItem('user_company'));
   }

  ngOnInit() {
  }
  closeDialog() {
    clearInterval(this.clearFor);
    this.dialogRef.close('');
  }
  public sendcAppNotify(formData){
    if(!formData.valid){
      return;
    }else{
    if(this.time != 10 ){
      this.time = 0;
    }
    clearInterval(this.clearFor);
    this.clearFor = setInterval(() => { 
      if(this.time == 0){
        var params = {
          type: 'customer', 
          device_type:this.data.user_id.device_type,
          os_version: [],
          text: this.cAppNotify.controls['description'].value,
          title: this.cAppNotify.controls['title'].value,
          notification_type: 'selected',
          customerIds: [this.data.user_id._id],
          image_url:this.pushImg,
          link:this.cAppNotify.controls['link'].value,
          futureCustomers:'0',
          notification_expiry:moment(this.cAppNotify.controls['expiry'].value).tz('Asia/Dubai').format("YYYY-MM-DD HH:mm"),
          company_id: this.companyId,
        }
        var encrypted = this.encDecService.nwt(this.session,params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._customerService.appNotification(enc_data).then((dec) => {
          if (dec.status == 200) {
            this.toastr.success('App Notifications sent successfully');
          } else {
            this.toastr.error('Some error has occurred');
          }
        })
        this.submitButton = 'Done !';
        this.dontSend();
        }else{
          this.time--;
          this.submitButton = 'Sending In ' + this.time + ' Sec';
        }
      }, 1000);
    }
  }
  public sendpushNotify(formData){
    if(!formData.valid){
      return;
    }else{
    if(this.time != 10 ){
      this.time = 0;
    }
    clearInterval(this.clearFor);
    this.clearFor = setInterval(() => { 
      if(this.time == 0){
        var params = {
          device_type: this.data.user_id.device_type,
          text: this.pushNotify.controls['description'].value,
          title: this.pushNotify.controls['title'].value,
          customerIds: [this.data.user_id._id],
          company_id: this.companyId
        }
        console.log(params);
        var encrypted = this.encDecService.nwt(this.session,params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._customerService.notificationServ(enc_data).then((dec) => {
          if (dec.status == 200) {
            this.toastr.success('Push Notifications sent successfully');
          } else {
            this.toastr.error('Some error has occurred');
          }
        })
        this.submitButton = 'Done !';
        this.dontSend();
        }else{
          this.time--;
          this.submitButton = 'Sending In ' + this.time + ' Sec';
        }
      }, 1000);
    }
  }
  public sendsmsNotify(formData){
    if(!formData.valid){
      return;
    }else{
    if(this.time != 10 ){
      this.time = 0;
    }
    clearInterval(this.clearFor);
    this.clearFor = setInterval(() => { 
      if(this.time == 0){
        var params = {
          message: this.smsNotify.controls['message'].value,
          customerIds: [this.data.user_id._id],
          company_id: this.companyId
        };
        console.log(params);
        var encrypted = this.encDecService.nwt(this.session,params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._customerService.notificationSms(enc_data).then((data) => {
          if (data.status == 200) {
            this.toastr.success('Push Notifications sent successfully');
          } else {
            this.toastr.error('Some error has occurred');
          }
        })
        this.submitButton = 'Done !';
        this.dontSend();
        }else{
          this.time--;
          this.submitButton = 'Sending In ' + this.time + ' Sec';
        }
      }, 1000);
    }
  }
  public sendemailNotify(formData){
    if(!formData.valid){
      return;
    }else{
    if(this.time != 10 ){
      this.time = 0;
    }
    clearInterval(this.clearFor);
    this.clearFor = setInterval(() => { 
      if(this.time == 0){
        var params = {
          subject: this.emailNotify.controls['description'].value,
          content: this.emailNotify.controls['title'].value,
          customerIds: [this.data.user_id._id],
          company_id: this.companyId
        }
        var encrypted = this.encDecService.nwt(this.session,params);
        var enc_data = {
          data: encrypted,
          email: localStorage.getItem('user_email')
        }
        this._customerService.notificationEmail(enc_data).then((data) => {
          if (data.status == 200) {
            this.toastr.success('Email Notifications sent successfully');
            setTimeout(() => {
              
            }, 1000);
          } else {
            this.toastr.error('Some error has occurred');
          }
        })
        this.submitButton = 'Done !';
        this.dontSend();
        }else{
          this.time--;
          this.submitButton = 'Sending In ' + this.time + ' Sec';
        }
      }, 1000);
    }
  }
  imageFinishedUploading(file: FileHolder) {
    this.pushImg = file.src;
    this.spandata = 'yes';
  }

  onRemoved(file: FileHolder) {
    this.pushImg = '';
    this.spandata = 'no';
  }
  public clearFor;
  dontSend(){
    setTimeout(() => {
      this.submitButton = 'Submit'
    }, 5000);
    this.time = 10;
    clearInterval(this.clearFor);
  }
}
