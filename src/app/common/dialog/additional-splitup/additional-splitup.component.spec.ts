import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalSplitupComponent } from './additional-splitup.component';

describe('AdditionalSplitupComponent', () => {
  let component: AdditionalSplitupComponent;
  let fixture: ComponentFixture<AdditionalSplitupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalSplitupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalSplitupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
