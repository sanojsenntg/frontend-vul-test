import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastsManager } from 'ng2-toastr';
@Component({
  selector: 'app-additional-splitup',
  templateUrl: './additional-splitup.component.html',
  styleUrls: ['./additional-splitup.component.css']
})
export class AdditionalSplitupComponent implements OnInit {
  public id;
  public name;
  public amount;
  public additional_data = [];
  constructor(public toastr: ToastsManager, public dialogRef: MatDialogRef<AdditionalSplitupComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log("data"+JSON.stringify(this.data));
    this.additional_data = this.data.splitup;
  }
  ngOnInit() {

  }
  addRow() {
    if (this.id == '') {
      this.toastr.error("Id is required");
    } else if (this.amount == '') {
      this.toastr.error("Amout is required");
    } else if (this.name == '') {
      this.toastr.error("Name is required");
    } else {
      if (this.id == 'A001' || this.id == 'A002' || this.id == 'A003' || this.id == 'A004' || this.id == 'A005' || this.id == 'A006' || this.id == 'A007' || this.id == 'A008') {
        this.toastr.error("Id already used");
      } else {
        this.additional_data.push({ 'id': this.id, 'name': this.name, 'amount': this.amount });
        console.log("len" + this.additional_data.length);
        this.name = '';
        this.id = '';
        this.amount = '';
      }
    }
  }
  remove(position) {
    this.additional_data.splice(position, 1);
  }
  saveData() {

  }
}
