import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-edit-log',
  templateUrl: './edit-log.component.html',
  styleUrls: ['./edit-log.component.css']
})
export class EditLogComponent implements OnInit {
  public editData:any=[]
  constructor(
    public dialogRef: MatDialogRef<EditLogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      console.log(data)
      this.editData=data;
    }

  ngOnInit() {
    // if(typeof this.editData.initialData == 'object')
    //this.editData.initialData=JSON.stringify(this.editData.initialData)
    // if(typeof this.editData.editedData == 'object')
    // this.editData.editedData=JSON.stringify(this.editData.editedData)
    // if(typeof this.editData.updatedData == 'object')
    // this.editData.updatedData=JSON.stringify(this.editData.updatedData)

  }

}
