import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-key-indicators',
  templateUrl: './key-indicators.component.html',
  styleUrls: ['./key-indicators.component.css']
})
export class KeyIndicatorsComponent implements OnInit {
  public orderInfo:any=[];
  constructor(
    public dialogRef: MatDialogRef<KeyIndicatorsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      this.orderInfo=data.SingleDetail;
    }

  ngOnInit() {
  }
  public timetook(a: any,b:any) {
    //var uaeTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Dubai"});
    let date1 = new Date(a).getTime();
    let date2 = new Date(b).getTime();
    let time = date1 - date2; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";

    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output +=  Math.round(var1) + " seconds";
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      output += Math.round(var1) + " minutes";
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      output +=  Math.round(var1) + " hours";
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output += Math.round(var1) + " days";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output += Math.round(var1) + " months";
    }
    return hoursDiff < 0 ? '0 secs' : output;
  }
  onNavigate(link){
    window.open('https://www.google.com/maps/place/?q='+link, "_blank");
  }
  public reverseGCList=true;
  public acceptedLocation='';
  public distanceToReach='';
  public timeToReach='';
  public acceptedTime='';
  reversGC(loc,c_loc,c_dur,assigned_at) {
    if (!this.reverseGCList)
      return;
    else
      this.reverseGCList=false;
    let geocoder = new google.maps.Geocoder();
    let latlngvalue = loc.split(",");
    let latlng = { lat: parseFloat(latlngvalue[0]), lng: parseFloat(latlngvalue[1]) }
    let that=this
    geocoder.geocode({ location: latlng }, function (results, status) {
      if (results) {
        that.acceptedLocation=results[0].formatted_address+'  ('+loc+')';
        that.distanceToReach=c_loc;
        that.timeToReach=c_dur;
        that.acceptedTime=assigned_at;
      }
    });
  }
  public timetookScheduled(a: any,b:any,c:any) {
    //var uaeTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Dubai"});
    let date1 = new Date(a).getTime();
    let date2 = new Date(b).getTime();
    let time = date1 - (date2-(c*60000)); //msec
    let hoursDiff = time / 1000; //secs
    var output = "";

    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output +=  Math.round(var1) + " seconds";
    } else if (hoursDiff < 60 * 60) {
      let var1 = hoursDiff / 60;
      output += Math.round(var1) + " minutes";
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = hoursDiff / (60 * 60);
      output +=  Math.round(var1) + " hours";
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output += Math.round(var1) + " days";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output += Math.round(var1) + " months";
    }
    return hoursDiff < 0 ? '0 secs' : output;
  }
  round(data) {
    if (parseFloat(data) > 0)
      return parseFloat(data).toFixed(2);
    else
      return 0;
  }
  // public get sortedArray(): [] {
  //   try{
  //   return this.orderInfo.assigned_drivers.sort((a, b) => {
  //     return <any>new Date(b.assigned_at) - <any>new Date(a.assigned_at);
  //   });
  //   }
  //   catch(Error){
  //     return this.orderInfo.assigned_drivers;
  //   }
  // }
}
