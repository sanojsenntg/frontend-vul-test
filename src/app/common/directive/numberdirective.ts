import { Directive } from '@angular/core';
import { FormControl,NG_VALIDATORS,AbstractControl, ValidatorFn , FormBuilder, FormGroup,Validator, Validators } from '@angular/forms';

@Directive({  
  selector: '[numbervalidator][ngModel]',  
  providers: [  
   {  
    provide: NG_VALIDATORS,  
    useExisting: Numbervalidator,  
    multi: true  
   }  
  ]  
 })

 export class Numbervalidator implements Validator {

  validator: ValidatorFn;
  
  constructor() {  
    console.log('run');
     this.validator = this.numbervalidator();  
    }
  
  validate(c: FormControl) {  
     return this.validator(c);  
    }
  
    numbervalidator(): ValidatorFn {  
     return (c: FormControl) => {  
      //let isValid = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(c.value);  
     if (c.value <= 100) {  
       return null;  
      } else {  
       return {  
        numbervalidator: {  
         valid: false  
        }  
       };  
      }  
     }  
    }  
   }