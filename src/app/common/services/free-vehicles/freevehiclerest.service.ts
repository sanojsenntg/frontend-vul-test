import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class FreeVehicleRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private vehicleUrl = environment.apiUrl ;
  private messagesUrl = environment.apiUrl + '/message';

  
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

  

  /**
   * Get all free vehicles.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getFreeVehicle(params) {
    
    return this._apiService.post(this.vehicleUrl + '/free_vehicles', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  
  public getAlert(params) {
    
    return this._apiService.post(this.vehicleUrl + '/alerts', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public inactiveVehicle(params){
    return this._apiService.post(this.vehicleUrl + '/vehicle/last_trip', params)
    .toPromise()
    .then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }
}



