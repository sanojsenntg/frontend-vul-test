import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { PromocoderestService } from './promocoderest.service';

@Injectable()

export class PromocodeService {
  constructor(private _promocoderestService: PromocoderestService) { }

  /**
   * Get all promocodes
   * @param data 
   */
  public getPromocodeListing(data) {
    return this._promocoderestService.getPromocodeListing(data)
      .then((res) => res);
  }
  
  public getPromocodeTransactionListing(data) {
    return this._promocoderestService.getPromocodeTransactionListing(data)
      .then((res) => res);
  }
  /**
   * Add New promocode
   * @param data 
   */
  public addPromocode(data) {
    return this._promocoderestService.addPromocode(data)
      .then((res) => res);
  }

  /**
   * Get a promocode by id
   * @param id 
   */
  public getPromocodeById(id) {
    return this._promocoderestService.getPromocodeById(id)
      .then((res) => res);
  }

  /**
   * Update a promocode by id
   * @param id 
   * @param data 
   */
  public updatePromocodeById(data) {
    return this._promocoderestService.updatePromocodeById(data)
      .then((res) => res);
  }

  /**
   * Update delete status of a promocode
   * @param id 
   */
  public updateDeletedStatus(id) {
    return this._promocoderestService.updateDeletedStatus(id)
      .then((res) => res);
  }
  public getindividualPromo(data) {
    return this._promocoderestService.getindividualPromo(data)
    .map((res) => res);
  }
  public updateCustomerPromo(params) {
    return this._promocoderestService.updateCustomerPromo( params)
      .then((res) => res);
  }
  public getPromocodeHistory(data) {
    return this._promocoderestService.getPromocodeHistory(data)
      .then((res) => res);
  }
  public changePromoRole(params){
    return this._promocoderestService.changePromoRole(params)
      .then((res) => res);
  }
}




