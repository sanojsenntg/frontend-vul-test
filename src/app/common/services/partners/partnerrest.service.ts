import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class PartnerRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private partnerUrl = environment.apiUrl + '/partners';
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

  /**
  * Get all partners.
  * @param params
  * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
  */
  public getPartners(params) {
    return this._apiService.post(this.partnerUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Save partners details.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public savePartners(data) {
    return this._apiService.post(this.partnerUrl + '/create', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update partners for corrsponding partners Id.
   * @param id
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updatePartners(params) {
    return this._apiService.post(this.partnerUrl + '/update', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Get partners  for corrsponding partners Id.
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getPartnersById(id) {
    return this._apiService.post(this.partnerUrl + '/getById/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Delete partners  for corrsponding partners Id.
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public deletePartnersById(id) {
    return this._apiService.post(this.partnerUrl + '/delete/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
    * Update delete status for corrsponding payment-type Id.
    * @param id
    * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
    */
  public updateDeletedStatus(id) {
    return this._apiService.post(this.partnerUrl + '/updateDeleteStatus/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}