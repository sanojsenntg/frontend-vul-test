import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PartnerRestService } from './partnerrest.service';

@Injectable()
export class PartnerService {

  constructor(private _partnerRestService: PartnerRestService) { }

  public getPartner(params) {
    return this._partnerRestService.getPartners(params)
      .then((res) => res);
  }

  public savePartner(data) {
    return this._partnerRestService.savePartners(data)
      .then((res) => res);
  }

  public updatePartner(data) {
    return this._partnerRestService.updatePartners(data)
      .then((res) => res);
  }

  public getPartnerById(id) {
    return this._partnerRestService.getPartnersById(id)
      .then((res) => res);
  }

  public deletePartnerById(id) {
    return this._partnerRestService.deletePartnersById(id)
      .then((res) => res);
  }

  public updateDeletedStatus(id) {
    return this._partnerRestService.updateDeletedStatus(id)
      .then((res) => res);
  }
}
