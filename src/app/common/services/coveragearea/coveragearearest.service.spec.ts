import { TestBed, inject } from '@angular/core/testing';

import { CoveragearearestService } from './coveragearearest.service';

describe('CoveragearearestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoveragearearestService]
    });
  });

  it('should be created', inject([CoveragearearestService], (service: CoveragearearestService) => {
    expect(service).toBeTruthy();
  }));
});
