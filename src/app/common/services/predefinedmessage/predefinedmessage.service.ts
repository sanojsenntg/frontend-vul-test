import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { PredefinedmessagerestService } from './predefinedmessagerest.service';

@Injectable()
export class PredefinedmessageService {
  constructor(  private _predefinedmessagerestService: PredefinedmessagerestService ) { }

  public savePredefinedMessages (data) {
      return this._predefinedmessagerestService.savePredefinedMessages(data)
      .then((res) => res);
  }

  public getPredefinedMessages (params) {
      return this._predefinedmessagerestService.getPredefinedMessages( params )
      .then((res) => res);
  }

  public updatePredefinedMessages(params) {
  
    return this._predefinedmessagerestService.updatePredefinedMessages(params)
      .then((res) => res);
  }

  public getPredefinedMessagesById(id) {
    return this._predefinedmessagerestService.getPredefinedMessagesById(id)
      .then((res) => res);
  }
  public deletePredefinedMessagesById(id) {
    return this._predefinedmessagerestService.deletePredefinedMessagesById(id)
      .then((res) => res);
  }
  
  public updateDeletedStatus(id) {
    return this._predefinedmessagerestService.updateDeletedStatus(id)
      .then((res) => res);
  }

  public searchPredefinedMessages(params) {
    return this._predefinedmessagerestService.searchPredefinedMessages(params)
    .then((res) => res);
  }
}




