import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class VehicleRestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private vehiclesUrl = environment.apiUrl + '/vehicle';
  private uploadUrl = environment.apiUrl + '/uploadvehicle';
  private vehicleModelUrl = environment.apiUrl + '/vehicle-model';
  private getAllvehiclesUrl = environment.apiUrl;

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }


  public uploadVehicleList(params) {
    return this._apiService.post(this.uploadUrl + '/xlsxupload', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  /**
 * Get Vehicles.
 * @param params
 * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
 */

  public getVehicles(params) {
    return this._apiService.post(this.vehiclesUrl, params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  public getCategories(params) {
    return this._apiService.post(this.vehiclesUrl + '/sub_categories', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
* Get Vehicles.
* @param params
* @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
*/
  public getOccupiedVehicle(params) {
    return this._apiService.post(this.vehiclesUrl + '/get_occupied_vehicles', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }


  public searchVehicles(params) {
    return this._apiService.post(this.vehiclesUrl + '/search/vehicle_random_search', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });

  }


  /**
   * Get  Vehicle Status
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getVehiclesCountforDashboard(params) {
    return this._apiService.post(this.vehiclesUrl + '/vehicle_status_for_dasboard', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }


  public getVehiclesStatus(params) {
    return this._apiService.post(this.vehiclesUrl + '/vehicle_status', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  /**
     * Get  InActive Vehicle History
     * @param params
     * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
     */
  public getVehicleHistory(params) {
    return this._apiService.post(this.vehiclesUrl + '/vehicle_history', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  /**
     * Get  InActive Vehicle Status
     * @param params
     * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
     */
  public getInActiveVehicles(params) {
    return this._apiService.post(this.vehiclesUrl + '/vehicle_inactive_list', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
 * Get All Vehicles.
 * @param params
 * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
 */
  public getVehiclesAll(params) {
    return this._apiService.post(this.getAllvehiclesUrl + '/devicelocation/getdevicelocation', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }


  /**
  * Function to save  vehicles
  *
  * @param params
  * @returns {Observable<any>}
  */
  public saveVehicles(params) {
    return this._apiService.post(this.vehiclesUrl + '/savevehicle', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  /**
 * Function to delete Vehicles.
 * @param params
 * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
 */
  public delete(id) {
    return this._apiService.post(this.vehiclesUrl + '/deletevehicle/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
 * Get  Vehicle BY Id
 * @param params
 * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
 */
  public getVehicleById(id) {

    return this._apiService.post(this.vehiclesUrl + '/get/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      }
      );
  }

  /**
 * Get searched Vehicles.
 * @param params
 * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
 */
  public updateVehicleDetails(params) {
    return this._apiService.post(this.vehiclesUrl + '/updatevehicle/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }


  /**
   * Get searched Vehicles.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public searchVehicle(params) {
    return this._apiService.post(this.vehiclesUrl + '/searchvehicle', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }


  /**
 * Get Vehicles.
 * @param params
 * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
 */
  public getAllVehicleListing(params) {

    return this._apiService.post(this.vehiclesUrl + '/allvehicles/getallvehicle', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }


  /**
  * Get searched Vehicles.
  * @param params
  * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
  */
  public updateAdditionalService(params) {
    return this._apiService.post(this.vehiclesUrl + '/additionalServiceUpdate/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  public updateVehicleStatus(params) {
    return this._apiService.post(this.vehiclesUrl + '/changevehiclestatus/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
   * Update delete status Vehicles.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updateVehicleForDeletion(id) {
    return this._apiService.post(this.vehiclesUrl + '/status/updateForDeletion/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  public getVehiclesForModel() {
    return this._apiService.get(this.vehicleModelUrl + '/getVehicleModelForVehicles')
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public getvehicleinfo(params) {
    return this._apiService.post(this.vehiclesUrl + '/getvehicleinfo', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public getVehicleMaintenanceHistory(params) {
    return this._apiService.post(this.vehiclesUrl + '/status-log', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public customerAppVehicleList(params) {
    return this._apiService.post(environment.apiUrl + '/dashboard/customerVehicleStatsForCSV', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

}

