import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { VehicleRestService } from './vehiclerest.service';

@Injectable()
export class VehicleService {
  constructor(private _vehicleRestService: VehicleRestService) {
  }
  public uploadVehicleList(params) {
    return this._vehicleRestService.uploadVehicleList(params)
      .then((data) => data);
  }
  public getVehicleListing(params) {
    return this._vehicleRestService.getVehicles(params)
      .map((data) => data);
  }
  public getVehicleListingAdmin(params) {
    return this._vehicleRestService.getVehicles(params)
      .map((data) => data);
  }

  public getOccupiedVehicle(params) {
    return this._vehicleRestService.getOccupiedVehicle(params)
      .map((data) => data);
  }

  public getCategories(params) {
    return this._vehicleRestService.getCategories(params)
      .map((data) => data);
  }

  public getVehicleByDriverId(params) {
    return this._vehicleRestService.getVehicles(params)
      .map((data) => data);

  }

  public getVehicleByModel(data) {

    return this._vehicleRestService.getVehicles(data)
      .map((data) => data);

  }
  public getAllVehicles(request) {

    return this._vehicleRestService.getVehiclesAll(request)
      .then((res) => res);
  }

  public getVehiclesCountforDashboard(request) {
    return this._vehicleRestService.getVehiclesCountforDashboard(request)
      .then((data) => data);
  }
  public getStatusBasedVehicles(request) {

    return this._vehicleRestService.getVehiclesStatus(request)
      .then((data) => data);
  }

  public getInActiveVehicles(request) {
    return this._vehicleRestService.getInActiveVehicles(request)
      .then((data) => data);
  }
  public getVehicleHistory(request) {
    return this._vehicleRestService.getVehicleHistory(request)
      .then((data) => data);
  }
  public getStatusBasedVehiclesWithPagination(request) {
    return this._vehicleRestService.getVehiclesStatus(request)
      .then((data) => data);
  }

  /**
   *
   * @param params
   * @returns {Observable<any>}
   */
  public vehicleListing(params) {
    return this._vehicleRestService.getVehicles(params)
      .map((data) => data);

  }

  /**
 * Function to save  vehicle
 *
 * @returns {Observable<any>}
 */
  public saveVehicles(params) {
    return this._vehicleRestService.saveVehicles(params)
      .then((data) => data);
  }

  /**
    * Function to delete selected Vehicle
    * @returns {Observable<any>}
   */
  public delete(id) {
    return this._vehicleRestService.delete(id)
      .then((data) => data);
  }

  /**
    * Function to get Vehicle By Id
    * @returns {Observable<any>}
   */
  public getById(id) {
    return this._vehicleRestService.getVehicleById(id)
      .then((data) => data);
  }


  /**
    * Function to Update vehicle
    *  @returns {Observable<any>}
   */
  public updateVehicleDetails(params) {
    // let params = {
    //   registration_number: data.registration_number,
    //   licence_number: data.licence_number,
    //   display_name: data.display_name,
    //   vehicle_identity_number: data.vehicle_identity_number,
    //   insurance_expiration_date: data.insurance_expiration_date,
    //   vehicle_model: data.vehicle_model,
    //   vehicle_model_id: data.vehicle_model,
    //   plate_left_text: data.plate_left_text,
    //   plate_middle_text: data.plate_middle_text,
    //   category_id: data.category_id,
    //   sub_category_id: data.sub_category_id,
    //   plate_right_text: data.plate_right_text,
    //   plate_number: data.plate_number,
    //   plate_type: data.plate_type,
    //   register_with_wasl: data.register_with_wasl,
    //   send_to_rta: data.send_to_rta,
    //   in_maintenance: data.in_maintenance,
    //   credit_card: data.credit_card,
    //   company: data.company
    // };
    console.log(params);
    return this._vehicleRestService.updateVehicleDetails(params)
      .then((data) => data);
  }

  /**
     * Function to search vehicles
     *  @returns {Observable<any>}
    */
  public searchVehicle(params) {
    return this._vehicleRestService.searchVehicle(params)
      .then((res) => res);
  }


  /**
     * Function to search vehicles
     *  @returns {Observable<any>}
    */
  public getAllVehicleListing(params) {
    return this._vehicleRestService.getAllVehicleListing(params)
      .then((res) => res);
  }


  public updateAdditionalService(params) {
  
    return this._vehicleRestService.updateAdditionalService( params)
      .then((data) => data);
  }
  public updateVehicleStatus(params) {
    return this._vehicleRestService.updateVehicleStatus(params)
      .then((data) => data);
  }

  public updateVehicleForDeletion(id) {
    return this._vehicleRestService.updateVehicleForDeletion(id)
      .then((res) => res);
  }

  public searchVehiclesByFields(params) {
    return this._vehicleRestService.searchVehicles(params)
      .then((res) => res);
  }

  /**
    * Function to search vehicles
    *  @returns {Observable<any>}
   */
  public getVehiclesForModel() {
    return this._vehicleRestService.getVehiclesForModel()
      .then((res) => res);
  }

  public getvehicleinfo(params) {
    return this._vehicleRestService.getvehicleinfo(params)
      .then((res) => res);
  }
  public getVehicleMaintenanceHistory(request) {
    return this._vehicleRestService.getVehicleMaintenanceHistory(request)
      .then((data) => data);
  }

  public customerAppVehicleList(params) {
    return this._vehicleRestService.customerAppVehicleList(params)
      .then((res) => res);
  }
}




