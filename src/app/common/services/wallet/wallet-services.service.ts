import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { WalletServicesRestService } from './wallet-services-rest.service';

@Injectable()
export class WalletServicesService {

  constructor(private _WalletService: WalletServicesRestService) { }
  public getWalletList(params) {
    return this._WalletService.getWalletList(params)
      .map((res) => res);
  }
  public getWalletFullDetails(params) {
    return this._WalletService.getWalletFullDetails(params)
      .map((res) => res);
  }
  public AdjustAddAmount(params) {
    return this._WalletService.AdjustAddAmount(params)
      .map((res) => res);
  }
  public AdjustReduceAmount(params) {
    return this._WalletService.AdjustReduceAmount(params)
      .map((res) => res);
  }
  public BonusTransaction(params) {
    return this._WalletService.bonustransfer(params)
      .map((res) => res);
  }
  public PenaltyTransaction(params) {
    return this._WalletService.penaltytransfer(params)
      .map((res) => res);
  }

  public collectFromDriver(params) {
    return this._WalletService.collectFromDriver(params)
      .map((res) => res);
  }
  public payDriver(params) {
    return this._WalletService.payDriver(params)
      .map((res) => res);
  }
  public searchForJob(params) {
    return this._WalletService.searchForJob(params)
      .map((res) => res);
  }
  public searchFullJob(params) {
    return this._WalletService.searchFullJob(params)
      .map((res) => res);
  }
  public getJobDetail(params) {
    return this._WalletService.getJobDetail(params)
      .map((res) => res);
  }
  public getJobDetailCSV(params) {
    return this._WalletService.getJobDetailCSV(params)
      .map((res) => res);
  }
  public getJobDetailCSV2(params) {
    return this._WalletService.getJobDetailCSV2(params)
      .map((res) => res);
  }
  public getDueCount(params) {
    return this._WalletService.getDueCount(params)
      .map((res) => res);
  }
  public settleDrivers(params) {
    return this._WalletService.settleDrivers(params)
      .map((res) => res);
  }
  public getsplitupData(params) {
    return this._WalletService.getsplitupData(params)
      .map((res) => res);
  }
  public getsplitupDueData(params) {
    return this._WalletService.getsplitupDueData(params)
      .map((res) => res);
  }
  public getTransactionList(params) {
    return this._WalletService.getTransactionList(params)
      .map((res) => res);
  }
  public getWalletBalance(params) {
    return this._WalletService.getWalletBalance(params)
      .map((res) => res);
  }
  public getWalletDetails(params) {
    return this._WalletService.getWalletDetails(params)
      .map((res) => res);
  }
  public addWallet(data) {
    return this._WalletService.saveWallet(data)
      .map((res) => res);
  }
}
