import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class WalletServicesRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private walletUrl = environment.apiUrl + '/wallet';
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }
  public getWalletList(params) {
    return this._apiService.post(this.walletUrl + '/list', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getWalletFullDetails(params) {
    return this._apiService.post(this.walletUrl + '/detailedlist', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public AdjustAddAmount(params) {
    return this._apiService.post(this.walletUrl + '/addamount', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public AdjustReduceAmount(params) {
    return this._apiService.post(this.walletUrl + '/reduceamount', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public bonustransfer(params) {
    return this._apiService.post(this.walletUrl + '/bonustransfer', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public penaltytransfer(params) {
    return this._apiService.post(this.walletUrl + '/penaltytransfer', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public collectFromDriver(params) {
    return this._apiService.post(this.walletUrl + '/collectamount', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public payDriver(params) {
    return this._apiService.post(this.walletUrl + '/payamount', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public searchForJob(params) {
    return this._apiService.post(this.walletUrl + '/searchforjob', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public searchFullJob(params) {
    return this._apiService.post(this.walletUrl + '/searchfulljob', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getJobDetail(params) {
    return this._apiService.post(this.walletUrl + '/getjobDetail', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getJobDetailCSV(params) {
    return this._apiService.post(this.walletUrl + '/getjobDetailCSV1', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getJobDetailCSV2(params) {
    return this._apiService.post(this.walletUrl + '/getjobDetailCSV2', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public settleDrivers(params) {
    return this._apiService.post(this.walletUrl + '/settle_drivers', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getsplitupData(params) {
    return this._apiService.post(this.walletUrl + '/getsplitup', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getsplitupDueData(params) {
    return this._apiService.post(this.walletUrl + '/getsplitupdueData', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getDueCount(params) {
    return this._apiService.post(this.walletUrl + '/getduecount', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getTransactionList(params) {
    return this._apiService.post(this.walletUrl + '/transactionlist', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getWalletDetails(params) {
    return this._apiService.post(this.walletUrl + '/fetchdriverwallets', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getWalletBalance(params) {
    return this._apiService.post(this.walletUrl + '/checkbalance', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public saveWallet(params) {
    return this._apiService.post(this.walletUrl + '/createwallet', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}
