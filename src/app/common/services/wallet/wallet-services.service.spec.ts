import { TestBed, inject } from '@angular/core/testing';

import { WalletServicesService } from './wallet-services.service';

describe('WalletServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WalletServicesService]
    });
  });

  it('should be created', inject([WalletServicesService], (service: WalletServicesService) => {
    expect(service).toBeTruthy();
  }));
});
