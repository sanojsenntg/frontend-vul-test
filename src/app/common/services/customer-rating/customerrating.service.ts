
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';

import { CustomerRatingRestService } from './customerratingrest.service';

@Injectable()
export class CustomerRatingService {

  constructor( private CustomerRatingRestService: CustomerRatingRestService ) { }

  public getCustomerRatings (params) {
    return this.CustomerRatingRestService.getCustomerRatings( params )
      .then((res) => res);
  }
}






