import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from './../access-control/access-control.service';


@Injectable()
export class CustomerRatingRestService {

  private headers = new Headers({'content-type': 'application/json'});
  private authUrl = environment.authUrl;
  private customerRatingsUrl = environment.apiUrl + '/customer_ratings';

  constructor( private _apiService: ApiService,
    private accessControl: AccessControlService) { }

   /**
   * Function to get all customer ratings.
   *
   */
    public getCustomerRatings (params) {
        return this._apiService.post( this.customerRatingsUrl + '/get_customer_ratings' , params )
        .toPromise()
        .then((res) => {
            if (this.accessControl.checkSession(res.status))
              return res
          });
    }
  
}


