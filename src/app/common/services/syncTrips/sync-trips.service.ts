import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { SyncTripsRestService } from './sync-tripsrest.service';

@Injectable()
export class SyncTripsService {
  constructor(
    private _apiService: ApiService,
    private _syncRestService: SyncTripsRestService) { }

  /**
    * Function to get all Sync trips
   */
  public getSyncTrips(params) {
    return this._syncRestService.get(params)
      .then((res) => res);
  }
}


