import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions} from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class OrderGrouprestService {
  private headers = new Headers({'content-type': 'application/json'});
  private orderGroupUrl = environment.apiUrl + '/orderGroup';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  /**
   * Get all orders.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public get (params) {
    return this._apiService.post(this.orderGroupUrl + '/get', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}
