import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { OrderGrouprestService } from './order_grouprest.service';

@Injectable()
export class OrderGroupService {
  constructor(  private _orderGrouprestService: OrderGrouprestService ) { }
  public get (params) {
    return this._orderGrouprestService.get( params )
      .then((res) => res);
  }
}



