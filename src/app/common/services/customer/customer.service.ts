import { Injectable } from '@angular/core';
import { CustomerRestService } from './customerrest.service';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import { JwtService } from '../api/jwt.service';
import 'rxjs/Rx';

@Injectable()
export class CustomerService {

  constructor(
    private _apiService: ApiService,
    private _customerRestService: CustomerRestService) { }

  public get(params) {
    return this._customerRestService.get(params)
      .then((res) => res);
  }

  public fetchCustomer(params) {
    return this._customerRestService.fetch(params)
      .then((res) => res);
  }
  public fetchCustomerGroups(params) {
    return this._customerRestService.fetchCustomerGroups(params)
      .then((res) => res);
  }
  public searchCustomer(params) {
    return this._customerRestService.search(params)
      .then((res) => res);
  }

  public changeBlockStatus(params) {
    return this._customerRestService.ChangeBlockStatus(params)
      .then((res) => res);
  }

  public getDisaptcher(data) {
    return this._customerRestService.getDispatcherForDropDown(data)
      .then((res) => res);
  }


  public getPhoneNumber(params) {
    return this._customerRestService.getPhoneNumber(params)
      .map((res) => res);

  }


  public getCustomerByName(params) {
    return this._customerRestService.getCustomerByName(params)
      .map((res) => res);

  }

  public getCustomerByNameFrOrder(params) {
    return this._customerRestService.getCustomerByNameFrOrder(params)
      .map((res) => res);

  }

  public getCustomerByPhone(params) {
    return this._customerRestService.getCustomerByPhone(params)
      .map((res) => res);
  }
  public getGuestPhone(params){
    return this._customerRestService.getGuestPhone(params)
      .map((res) => res);
  }

  public getCustomerAddrress(params) {
    return this._customerRestService.getCustomerAddrress(params)
      .then((res) => res);
  }

  public deleteAddress(params) {
    return this._customerRestService.deleteAddress(params)
      .then((res) => res);
  }

  /**
  * Function to get Dispatcher
  *
  */

  public getDispatchersForDropDown(params) {
    return this._customerRestService.getDispatcherForDropDown(params)
      .then((res) => res);
  }
  public getCustomerMessages(params) {
    return this._customerRestService.getCustomerMessages(params)
      .then((res) => res);
  }

  public getCustomerById(id) {
    return this._customerRestService.getCustomerById(id).then((res) => res);
  }

  public getCustomerTrips(id) {
    return this._customerRestService.getCustomerTrips(id).then((res) => res);
  }

  public notificationServ(params) {
    return this._customerRestService.notificationServ(params).then((res) => res);
  }

  public notificationEmail(params){
    return this._customerRestService.notificationEmail(params).then((res)=>res);
  }

  public notificationSms(params){
    return this._customerRestService.notificationSms(params).then((res)=>res);
  }
  public dispatcherSms(params){
    return this._customerRestService.dispatcherSms(params).then((res)=>res);
  }

  public blockCustomer(params){
    return this._customerRestService.blockCustomer(params).then((res)=>res);
  }
  public fetchCustomerCount(params) {
    return this._customerRestService.fetchCount(params)
      .then((res) => res);
  }
  public appDeviceVersion(params){
    return this._customerRestService.appDeviceVersion(params)
    .then((res) => res);
  }
  public appNotification(params){
    return this._customerRestService.appNotification(params)
    .then((res) => res);
  }
  public updateCustomerDetails(params){
    return this._customerRestService.updateCustomerDetails(params)
    .then((res) => res);
  }
  public dispatcherCustomers(params){
    return this._customerRestService.dispatcherCustomers(params)
    .then((res) => res);
  }
  public zendeskTickets(params){
    return this._customerRestService.zendeskTickets(params)
    .then((res) => res);
  }
  public zendeskComments(params){
    return this._customerRestService.zendeskComments(params)
    .then((res) => res);
  }
  public zendeskTags(params){
    return this._customerRestService.zendeskTags(params)
    .then((res) => res);
  }
  public zendeskStatus(params){
    return this._customerRestService.zendeskStatus(params)
    .then((res) => res);
  }
  public getOrderComments(params){
    return this._customerRestService.getOrderComments(params)
    .then((res) => res);
  }
  public zendeskDetails(params){
    return this._customerRestService.zendeskDetails(params)
    .then((res) => res);
  }
  public zendeskAdd(params){
    return this._customerRestService.zendeskAdd(params)
    .then((res) => res);
  }
  public zendeskList(params){
    return this._customerRestService.zendeskList(params)
    .then((res) => res);
  }
  public zendeskId(params){
    return this._customerRestService.zendeskId(params)
    .then((res) => res);
  }
  public zendeskEdit(params){
    return this._customerRestService.zendeskEdit(params)
    .then((res) => res);
  }
  public zendeskDelete(params){
    return this._customerRestService.zendeskDelete(params)
    .then((res) => res);
  }
}
