import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';


@Injectable()


export class DriverGroupRestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private driverGroupUrl = environment.apiUrl + '/driver-groups';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }

  public getDriverGroup(params) {
    return this._apiService.post(this.driverGroupUrl + '/list', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


  public getDriverGroupById(id) {
    return this._apiService.post(this.driverGroupUrl + '/getById/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public saveDriverGroup(params) {
    return this._apiService.post(this.driverGroupUrl + '/add', params, { headers: this.headers })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public delete(id) {
    return this._apiService.post(this.driverGroupUrl + '/remove/' ,id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateDriverGroup(params) {
    return this._apiService.post(this.driverGroupUrl + '/update/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateDriverGroupForDeletion(id) {
    return this._apiService.post(this.driverGroupUrl + '/updateForDelete/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}