import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../api/jwt.service';
import { Router } from '@angular/router';
import { AccessControlService } from './../access-control/access-control.service'
import { Observable } from 'rxjs/Rx';

@Injectable()

export class OrderRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private ordersUrl = environment.apiUrl + '/order';
  private messagesUrl = environment.apiUrl + '/message';
  private orderHistoryUrl = environment.apiUrl + '/orders-status-history';
  private reasonsUrl = environment.apiUrl + '/reasons';
  private dashboardChart = environment.apiUrl + '/dashboard';
  private filter = environment.apiUrl + '/dashboard-filter';
  private dashboard = environment.apiUrl + '/dashboard';
  /**
  *
  * @param {Http} http
  * @param {ApiService} _apiService
  */
  constructor(private http: Http, private _apiService: ApiService,
    public toastr: ToastsManager,
    private _jwtService: JwtService,
    private router: Router,
    private accessControl: AccessControlService) { };

  /**
   * Save order details.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public saveOrder(data) {
    return this._apiService.post(this.ordersUrl + '/saveorder', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public reassignCustomerAppOrder(data) {
    return this._apiService.post(this.ordersUrl + '/reassigncustomerorder', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public reassignDeliveryOrder(data) {
    return this._apiService.post(this.ordersUrl + '/reassigndeliveryorder', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public saveAutomaticOrder(data) {
    return this._apiService.post(this.ordersUrl + '/saveautomaticorder', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }
  /**
   * Get all orders.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public get(params) {
    return this._apiService.post(this.ordersUrl + '/orderslist', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  /**
   * Get order information for corrsponding order Id
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getOrderById(id) {
    return this._apiService.post(this.ordersUrl + '/orderdetail/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getShiftbyOrder(params) {
    return this._apiService.post(this.ordersUrl + '/driver_shift_tracks/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getOrderByUniqueShiftId(id) {
    return this._apiService.post(this.ordersUrl + '/orderdetailbyUniqueShiftid/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getOrderByDriverId(params) {
    return this._apiService.post(this.ordersUrl + '/getOrderByDriverId', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  /**
   * get order By userId
   */

  public getOrderByUserId(params) {
    return this._apiService.post(this.ordersUrl + '/getOrderByUserId', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });


  }

  /**
   * Update Order for corrsponding order Id.
   * @param id
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updateOrder(params) {
    return this._apiService.post(this.ordersUrl + '/updateOrder/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  /**
   * Create duplicate order information.
   * @param id
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public copyOrder(id) {
    return this._apiService.post(this.ordersUrl + '/orderCopy/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  /**
   * Send message to driver for order.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public sendmessage(params) {
    return this._apiService.post(this.messagesUrl + '/createMessage', params)
      .toPromise()
      .then((response) => response);
  }

  /**
   * Update order status with removing allocation of any order.
   * @param orderId
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public cancelOrder(data) {
    return this._apiService.post(this.ordersUrl + '/cancelorder/', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
 * Serach all orders for admin.
 * @param params
 * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
 */
  public searchOrder(params) {
    return this._apiService.post(this.ordersUrl + '/searchOrder', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public searchOrderCSV(params) {
    return this._apiService.post(this.ordersUrl + '/searchOrderCSV', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getCSVConstants(params) {
    return this._apiService.post(this.ordersUrl + '/getOrderExportOffset', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getCSVConstantsWallet(params) {
    return this._apiService.post(this.ordersUrl + '/getOrderExportOffsetWallet', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getRejectedCSVConstants(params) {
    return this._apiService.post(this.ordersUrl + '/getRejectedListExportOffset', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public searchOrderAssigned(params) {
    return this._apiService.post(this.ordersUrl + '/searchOrderOrderAssigned', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public searchOrderSalik(params) {
    return this._apiService.post(this.ordersUrl + '/searchOrderSalik', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public searchOrderMOreInfo(params) {
    return this._apiService.post(this.ordersUrl + '/searchOrderExtraInfo', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
 * Search  orders for Dispatcher
 * @param params
 * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
 */
  public searchDispatcherOrder(params) {
    return this._apiService.post(this.ordersUrl + '/dispatchersearchOrder', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


  /**
   * Get Orders For admin
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getAllOrder(params) {
    return this._apiService.post(this.ordersUrl + '/allorder/getAllOrder', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getOrderDispHistory(params) {
    return this._apiService.post(this.ordersUrl + '/allorder/getAllOrderHistory', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getOrderDispHistoryCsv(params) {
    return this._apiService.post(this.ordersUrl + '/getAllOrderHistoryCSV', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAllOrderCount(params) {
    return this._apiService.post(this.ordersUrl + '/allorder/getAllOrderCount', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAllOrderCount2(params) {
    return this._apiService.post(this.ordersUrl + '/allorder/getAllOrderCount2', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getOrderCount(params) {
    return this._apiService.post(this.ordersUrl + '/getOrderCount', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getRejectOrderCount(params) {
    return this._apiService.post(this.ordersUrl + '/getRejectOrderCount', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public orderHistoryByDriverId(params) {
    return this._apiService.post(this.orderHistoryUrl + '/orderHistoryByDriverId', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  public checkPaymentExraExist(params) {
    return this._apiService.post(this.ordersUrl + '/checkPaymentExraExist', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public countOrder(params) {
    return this._apiService.post(this.ordersUrl + '/countOrder', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAllOrderId(params) {
    return this._apiService.post(this.ordersUrl + '/getOrderForIds', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAllOrderIdDriver(params) {
    return this._apiService.post(this.ordersUrl + '/getOrderForIdsDriver', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAllUniqueShiftId(params) {
    return this._apiService.post(this.ordersUrl + '/getShiftForIds', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public updateOrderAfterEdit(params) {
    return this._apiService.post(this.ordersUrl + '/updateOrdersForEdit', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public sendEditOTP(params) {
    return this._apiService.post(this.ordersUrl + '/sendotpforedit', params)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }
  public verifytOTP(params) {
    return this._apiService.post(this.ordersUrl + '/verifyotpforedit', params)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }
  public getAllReasons(params) {
    return this._apiService.post(this.reasonsUrl, params)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }

  public getDashChart(params) {
    return this._apiService.post(this.dashboardChart + '/stats', params)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }

  public getOfflineChart(params) {
    return this._apiService.post(this.dashboardChart + '/offline_stats', params)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }
  
  public getappVehicleChart(params) {
    return this._apiService.post(this.dashboardChart + '/customer_app_vehicle', params)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }

  public forceCancelOrder(data) {
    return this._apiService.post(this.ordersUrl + '/forcecancelorder/', data)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }
  public getKeyIndicators(id) {
    return this._apiService.post(this.ordersUrl + '/order_details/', id)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }
  public getKeyIndicatorsExtraInfo(id) {
    return this._apiService.post(this.ordersUrl + '/order_key_details/', id)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }
  public getTransactionHistory(id) {
    return this._apiService.post(this.ordersUrl + '/gettransactionhistory', id)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }
  public retryPayment(id) {
    return this._apiService.post(this.ordersUrl + '/retrypayment', id)
      .toPromise()
      .then((response) => {
        if (this.accessControl.checkSession(response.status))
          return response
      });
  }
  public editScheduledOrder(data) {
    return this._apiService.post(this.ordersUrl + '/editscheduledOrder', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public addComments(data) {
    return this._apiService.post(this.ordersUrl + '/add-dispatcher-comments', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getComments(id) {
    return this._apiService.post(this.ordersUrl + '/list-dispatcher-comments', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public changeCommentStatus(id) {
    return this._apiService.post(this.ordersUrl + '/update-comments-status', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getCanceltStatus(params) {
    return this._apiService.post(environment.apiUrl + '/customers/get-cancel-reasons', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getOverSpeedTrips(params) {
    return this._apiService.post(this.ordersUrl + '/overspeedtrips', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getFlaggedTrips(params) {
    return this._apiService.post(this.ordersUrl + '/flaggedordertrips', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public customerOrderDispatchNow(params) {
    return this._apiService.post(this.ordersUrl + '/dispatchcustomerorder', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public hotelDeliveryDispatchNow(params) {
    return this._apiService.post(this.ordersUrl + '/dispatchdeliveryorder', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getFilters(params) {
    return this._apiService.post(this.filter, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public saveFilters(params) {
    return this._apiService.post(this.filter + '/save', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getMailStats(params) {
    return this._apiService.post(this.dashboard + '/email_stats', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getCustomerAppVehicleStats(params) {
    return this._apiService.post(this.dashboard + '/customer_vehicle_stats', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getInTripCordinates(params) {
    return this._apiService.post(this.ordersUrl + '/fetchshitstracks', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getOrderAcceptanceHistory(params) {
    return this._apiService.post(this.ordersUrl + '/order-status-history', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getMailPauseStats(params) {
    return this._apiService.post(this.dashboard + '/pause_stats', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}



