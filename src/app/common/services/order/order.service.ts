import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { OrderRestService } from './orderrest.service';

@Injectable()
export class OrderService {
  constructor(private orderRestService: OrderRestService) { }

  public saveOrder(data, order_type) {
    if (order_type) {
      return this.orderRestService.saveAutomaticOrder(data)
        .then((res) => res);
    } else {
      return this.orderRestService.saveOrder(data)
        .then((res) => res);

    }
  }
  public reassignCustomerAppOrder(data) {
    return this.orderRestService.reassignCustomerAppOrder(data)
      .then((res) => res);
  }
  public reassignDeliveryOrder(data){
    return this.orderRestService.reassignDeliveryOrder(data)
      .then((res) => res);
  }
  public getOrder(params) {
    return this.orderRestService.get(params)
      .then((res) => res);
  }

  public getOrderById(id) {
    return this.orderRestService.getOrderById(id)
      .then((res) => res);
  }

  public getOrderByUniqueShiftId(id) {
    return this.orderRestService.getOrderByUniqueShiftId(id)
      .then((res) => res);
  }

  public getOrderByDriverId(params) {
    return this.orderRestService.getOrderByDriverId(params)
      .then((res) => res);

  }


  public getOrderByUserId(user_id) {
    return this.orderRestService.getOrderByUserId(user_id)
      .then((res) => res);
  }

  public copyOrder(id) {
    return this.orderRestService.copyOrder(id)
      .then((res) => res);
  }

  public updateOrder(params) {
    return this.orderRestService.updateOrder(params)
      .then((res) => res);
  }
  public getPartners() {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: 'name'
    };
    return this.orderRestService.get(params)
      .then((res) => res);
  }
  public sendmessage(data) {
    return this.orderRestService.sendmessage(data)
      .then((res) => res);
  }
  public cancelOrder(data) {
    return this.orderRestService.cancelOrder(data)
      .then((res) => res);
  }

  /**
   * searchOrder
   */
  public searchOrder(params) {
    return this.orderRestService.searchOrder(params)
      .then((res) => res);
  }
  public searchOrderCSV(params) {
    return this.orderRestService.searchOrderCSV(params)
      .then((res) => res);
  }
  public getCSVConstants(params) {
    return this.orderRestService.getCSVConstants(params)
      .then((res) => res);
  }
  public getCSVConstantsWallet(params) {
    return this.orderRestService.getCSVConstantsWallet(params)
      .then((res) => res);
  }
  public getRejectedCSVConstants(params) {
    return this.orderRestService.getRejectedCSVConstants(params)
      .then((res) => res);
  }

  public searchOrderAssigned(params) {
    return this.orderRestService.searchOrderAssigned(params)
      .then((res) => res);
  }
  public searchOrderSalik(params) {
    return this.orderRestService.searchOrderSalik(params)
      .then((res) => res);
  }
  public searchOrderMOreInfo(params) {
    return this.orderRestService.searchOrderMOreInfo(params)
      .then((res) => res);
  }
  public getOrderDispHistory(params) {

    return this.orderRestService.getOrderDispHistory(params)
      .then((res) => res);
  }
  public getOrderDispHistoryCsv(params) {
    return this.orderRestService.getOrderDispHistoryCsv(params)
      .then((res) => res);
  }
  public getAllOrder(params) {

    return this.orderRestService.getAllOrder(params)
      .then((res) => res);
  }
  public getAllOrderCount(params) {
    return this.orderRestService.getAllOrderCount(params)
      .then((res) => res);
  }

  public getAllOrderCount2(params) {
    return this.orderRestService.getAllOrderCount2(params)
      .then((res) => res);
  }
  /**
   * searchOrder for dispatcher
   */
  public searchDispatcherOrder(params) {
    return this.orderRestService.searchDispatcherOrder(params)
      .then((res) => res);
  }

  /**
   * searchOrder for orderHistoryByDriverId
   */
  public orderHistoryByDriverId(params) {
    return this.orderRestService.orderHistoryByDriverId(params)
      .then((res) => res);

  }

  /**
   * check payment Extra exist
   */
  public checkPaymentExraExist(params) {
    return this.orderRestService.checkPaymentExraExist(params)
      .then((res) => res);

  }
  public countOrder(params) {
    return this.orderRestService.countOrder(params)
      .then((res) => res);
  }
  public getAllOrderId(params) {
    return this.orderRestService.getAllOrderId(params)
      .then((res) => res);
  }
  public getAllOrderIdDriver(params) {
    return this.orderRestService.getAllOrderIdDriver(params)
      .then((res) => res);
  }
  public getAllUniqueShiftId(params) {
    return this.orderRestService.getAllUniqueShiftId(params)
      .then((res) => res);
  }
  public updateOrderAfterEdit(params) {
    return this.orderRestService.updateOrderAfterEdit(params)
      .then((res) => res);
  }
  public sendEditOTP(params) {
    return this.orderRestService.sendEditOTP(params)
      .then((res) => res);
  }
  public verifyOTP(params) {
    return this.orderRestService.verifytOTP(params)
      .then((res) => res);
  }
  public getCancelReasons(params) {
    return this.orderRestService.getAllReasons(params)
      .then((res) => res);
  }
  public getOrderCount(params) {
    return this.orderRestService.getOrderCount(params)
      .then((res) => res);
  }
  public getRejectOrderCount(params) {
    return this.orderRestService.getRejectOrderCount(params)
      .then((res) => res);
  }

  public getDashChart(params) {
    return this.orderRestService.getDashChart(params)
      .then((res) => res);
  }

  public getOfflineChart(params) {
    return this.orderRestService.getOfflineChart(params)
      .then((res) => res);
  }
  public getappVehicleChart(params) {
    return this.orderRestService.getappVehicleChart(params)
      .then((res) => res);
  }
  public forceCancelOrder(data) {
    return this.orderRestService.forceCancelOrder(data)
      .then((res) => res);
  }
  public getKeyIndicators(id) {
    return this.orderRestService.getKeyIndicators(id)
      .then((res) => res);
  }
  public getKeyIndicatorsExtraInfo(id) {
    return this.orderRestService.getKeyIndicatorsExtraInfo(id)
      .then((res) => res);
  }
  public getTransactionHistory(id) {
    return this.orderRestService.getTransactionHistory(id)
      .then((res) => res);
  }
  public retryPayment(id) {
    return this.orderRestService.retryPayment(id)
      .then((res) => res);
  }
  public editScheduledOrder(data) {
    return this.orderRestService.editScheduledOrder(data)
      .then((res) => res);
  }
  public getComments(id) {
    return this.orderRestService.getComments(id)
      .then((res) => res);
  }
  public addComments(id) {
    return this.orderRestService.addComments(id)
      .then((res) => res);
  }
  public changeCommentStatus(id) {
    return this.orderRestService.changeCommentStatus(id)
      .then((res) => res);
  }
  public getCancelStatus(params) {
    return this.orderRestService.getCanceltStatus(params)
      .then((res) => res);
  }
  public getOverSpeedTrips(params) {
    return this.orderRestService.getOverSpeedTrips(params)
      .then((res) => res);
  }
  public getFlaggedTrips(params) {
    return this.orderRestService.getFlaggedTrips(params)
      .then((res) => res);
  }
  public customerOrderDispatchNow(params) {
    return this.orderRestService.customerOrderDispatchNow(params)
      .then((res) => res);
  }
  public hotelDeliveryDispatchNow(params) {
    return this.orderRestService.hotelDeliveryDispatchNow(params)
      .then((res) => res);
  }
  public getFilters(params) {
    return this.orderRestService.getFilters(params)
      .then((res) => res);
  }
  public saveFilters(params) {
    return this.orderRestService.saveFilters(params)
      .then((res) => res);
  }
  public getMailStats(params) {
    return this.orderRestService.getMailStats(params)
      .then((res) => res);
  }
  public getCustomerAppVehicleStats(params) {
    return this.orderRestService.getCustomerAppVehicleStats(params)
      .then((res) => res);
  }
  public getInTripCordinates(params){
    return this.orderRestService.getInTripCordinates(params)
      .then((res) => res);
  }
  public getOrderAcceptanceHistory(params) {
    return this.orderRestService.getOrderAcceptanceHistory(params)
      .then((res) => res);
  }
  public getMailPauseStats(params) {
    return this.orderRestService.getMailPauseStats(params)
      .then((res) => res);
  }
  public getShiftbyOrder(params){
    return this.orderRestService.getShiftbyOrder(params)
      .then((res) => res);
  }
}



