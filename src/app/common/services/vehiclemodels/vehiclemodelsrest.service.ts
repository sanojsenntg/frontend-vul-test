import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import { AccessControlService } from '../access-control/access-control.service';


@Injectable()
export class VehicleModelsRestService {

  private vehicleModelsUrl = environment.apiUrl + '/vehicle-model';
  private vehicleCatMsgUrl = environment.apiUrl + '/appmessages';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  /**
   * Function to get list of vehicle models
   *
   * @param params
   * @returns {Observable<any>}
   */
  public getVehicleModels(params) {
    return this._apiService.post(this.vehicleModelsUrl + '/modellist', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public getVehicleCategories(params) {
    return this._apiService.post(this.vehicleModelsUrl + '/categorylist', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public getVehicleCategoriesMessages(params) {
    return this._apiService.post(this.vehicleCatMsgUrl + '/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
   * Function to get vehicle model by id
   *
   * @returns {Observable<any>}
   */
  public getVehicleModelById(id) {
    return this._apiService.post(this.vehicleModelsUrl + '/getbyid/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public getVehicleCategoryById(id) {
    return this._apiService.post(this.vehicleModelsUrl + '/getcategorybyid/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public getVehicleCategoryMessageById(id) {
    return this._apiService.post(this.vehicleCatMsgUrl + '/getcategorymessagebyid/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  /**
   * Function to save vehicle models
   *
   * @param params
   * @returns {Observable<any>}
   */
  public saveVehicleModels(data) {
    return this._apiService.post(this.vehicleModelsUrl + '/save', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  public saveVehicleCategory(data) {
    return this._apiService.post(this.vehicleModelsUrl + '/savecategory', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public saveVehicleCategoryMessage(data) {
    return this._apiService.post(this.vehicleCatMsgUrl + '/add', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  /**
   * Function to update vehicle models
   *
   * @param params
   * @returns {Observable<any>}
   */
  public updateVehicleModel(params) {
    return this._apiService.post(this.vehicleModelsUrl + '/update/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  public updateVehicleCategory(params) {
    return this._apiService.post(this.vehicleModelsUrl + '/updatecategory/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public updateVehicleCategoryMessages(params) {
    return this._apiService.post(this.vehicleCatMsgUrl + '/update_category_messages', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  /**
   * 
   * Function delete vehicle models by id
   * 
   */
  public deleteVehicleModel(id) {
    return this._apiService.post(this.vehicleModelsUrl + '/delete/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
      * Function Update Tariff for Deletion.
      * 
   */
  public updateVehicleModelForDeletion(id) {
    return this._apiService.post(this.vehicleModelsUrl + '/updateDeleteStatus/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public updateVehicleCategoryForDeletion(id) {
    return this._apiService.post(this.vehicleModelsUrl + '/updatecategoryDeleteStatus/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public updateVehicleCategoryMessageForDeletion(id) {
    return this._apiService.post(this.vehicleCatMsgUrl + '/updatecategoryDeleteStatus/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
}

