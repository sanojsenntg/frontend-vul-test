import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { CustomerPackageRestService } from './cusomter_packages-rest.service';

@Injectable()
export class PackageService {

  constructor(private _PackageService: CustomerPackageRestService) { }
  public getPackageList(params) {
    return this._PackageService.getPackageList(params)
      .map((res) => res);
  }
  public savePackage(params) {
    return this._PackageService.savePackage(params)
      .map((res) => res);
  }
  public packageForDeletion(params) {
    return this._PackageService.packageForDeletion(params)
      .map((res) => res);
  }
  public getWalletDetails(params) {
    return this._PackageService.packageForDeletion(params)
      .map((res) => res);
  }
  public addWallet(data) {
    return this._PackageService.saveWallet(data)
      .map((res) => res);
  }
  public packageUsers(data) {
    return this._PackageService.packageUsers(data)
      .then((res) => res);
  }
}