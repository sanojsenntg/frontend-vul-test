import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class CustomerPackageRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private packageUrl = environment.apiUrl + '/customer_packages';
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }
  public getPackageList(params) {
    return this._apiService.post(this.packageUrl + '/list', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public savePackage(params) {
    return this._apiService.post(this.packageUrl + '/savepackage', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public packageForDeletion(params) {
    return this._apiService.post(this.packageUrl + '/updatedeletestatus', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getWalletBalance(params) {
    return this._apiService.post(this.packageUrl + '/checkbalance', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public saveWallet(params) {
    return this._apiService.post(this.packageUrl + '/createwallet', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public packageUsers(params) {
    return this._apiService.post(this.packageUrl + '/customers', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}
