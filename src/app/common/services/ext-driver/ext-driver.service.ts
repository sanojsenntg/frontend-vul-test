import { Injectable } from '@angular/core';
import { ExtDriverRestService } from './ext-driverrest.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Injectable()
export class ExtDriverService {

  constructor(public extDriver: ExtDriverRestService) { }

  public extDriverList(params){
    return this.extDriver.extDriverList(params)
        .then((res) => res);
  }
  public extDriverById(params){
    return this.extDriver.extDriverById(params)
        .then((res) => res);
  }
  public extDriverUpdate(params){
    return this.extDriver.extDriverUpdate(params)
        .then((res) => res);
  }
  public createDriver(params){
    return this.extDriver.createDriver(params)
        .then((res) => res);
  }
  public createVehicle(params){
    return this.extDriver.createVehicle(params)
        .then((res) => res);
  }
  public bankList(params){
    return this.extDriver.bankList(params)
        .then((res) => res);
  }
  public updatePassword(params){
    return this.extDriver.updatePassword(params)
        .then((res) => res);
  }
  public extDriverBankUpdate(params){
    return this.extDriver.extDriverBankUpdate(params)
        .then((res) => res);
  }
}
