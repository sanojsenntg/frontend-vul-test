import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class TariffRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private tariffUrl = environment.apiUrl + '/tariff';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private _http: Http,
    private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  /**
   * Function to get all tariffs
   *
   */
  public get(params) {
    return this._apiService.post(this.tariffUrl + '/tariffslist', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getTariffForOrder(params) {
    return this._apiService.post(this.tariffUrl + '/tariffslist', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Function to search tariffs
   *
   */
  public search(params) {
    return this._apiService.post(this.tariffUrl + '/search', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Function to Get Tariff by id
   * 
   */
  public getTariffById(id) {
    return this._apiService.post(this.tariffUrl + '/tariffdetail/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
    * Function create new Tariff record
    * 
  */
  public addTariff(data) {
    return this._apiService.post(this.tariffUrl + '/savetariff', data, { headers: this.headers })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
      * Function Update Tariff by id.
      * 
   */
  public updateTariff(params) {
    return this._apiService.post(this.tariffUrl + '/updatetariff/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
      * Function Update Tariff for Deletion.
      * 
   */
  public updateTariffForDeletion(id) {
    return this._apiService.post(this.tariffUrl + '/updateDeleteStatus/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
  * Function delete Tariff by id
  * 
  */
  public deleteTariff(id) {
    return this._apiService.post(this.tariffUrl + '/delete/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
  * Function update coordinates by tariff id
  * 
  */
  public updateTariffCoordinates(params) {
    return this._apiService.post(this.tariffUrl + '/updateTariffCoordinates', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


}


