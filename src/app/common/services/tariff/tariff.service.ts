import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { TariffRestService } from './tariffrest.service';

@Injectable()
export class TariffService {
  constructor(
    private _apiService: ApiService,
    private _tariffRestService: TariffRestService) { }

  /**
    * Function to get all Tariffs
   */
  public getTariffs(params) {
    return this._tariffRestService.get(params)
      .then((res) => res);
  }

  /**
    * Function to search Tariffs
   */
  public searchingTariff(params) {
    return this._tariffRestService.search(params)
      .then((res) => res);
  }

  /**
    * Function to Get Tariff by id
    * 
    */
  public getTariffById(id) {
    return this._tariffRestService.getTariffById(id)
      .then((res) => res);
  }

  /**
    * Function create new Tariff record
    * 
  */
  public addTariff(data) {
    let params = {
      name: data.name,
      tariff_type: data.tariff_type,
      vat: data.vat,
      unit_price: data.unit_price,
      partner_only: data.partner_only,
      price_with_vat: data.price_with_vat,
      additional_service_id: data.additional_service_id,
      is_peek_tariff: data.is_peek_tariff,
      start_price: data.start_price,
      price_per_distance: data.price_per_distance,
      standing_price: data.standing_price,
      denomination: data.denomination,
      included_in_start: data.included_in_start,
      distance_charge_unit: data.distance_charge_unit,
      standing_speed_limit: data.standing_speed_limit,
      percentage_multiplier: data.percentage_multiplier,
      minimum_fare: data.minimum_fare,
      peek_days: data.peek_days,
      peek_time_start: data.peek_time_start,
      peek_time_end: data.peek_time_end,
      vehicle_model_id: data.vehicle_model,
      is_general: data.is_general,
      companies: data.companies,
      is_for_driver_list: data.is_for_driver_list,
      additional_splitup: data.additional_splitup
    }
    return this._tariffRestService.addTariff(data)
      .then((res) => res);
  }

  /**
    * Function Update Tariff by id.
    * 
 */
  public updateTariff(params) {

    console.log(params);
    return this._tariffRestService.updateTariff(params)
      .then((res) => res);
  }

  /**
     * Function Update status For Deletion.
     * 
  */
  public updateTariffForDeletion(id) {
    return this._tariffRestService.updateTariffForDeletion(id)
      .then((res) => res);
  }

  /**
     * Function delete Tariff by id
     * 
     */
  public deleteTariffById(id) {
    return this._tariffRestService.deleteTariff(id)
      .then((res) => res);
  }

  public getTariffForOrder(params) {
    return this._tariffRestService.getTariffForOrder(params)
      .then((res) => res);
  }

  public updateTariffCoordinates(params) {
    return this._tariffRestService.updateTariffCoordinates(params)
      .then((res) => res);

  }
}



