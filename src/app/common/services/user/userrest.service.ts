import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';

import { ApiService } from '../api/api.service';

import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class UsersRestService {
  private headers = new Headers({ 'content-type': 'application/json', 'user-details': localStorage.getItem('user-details') });
  private authUrl = environment.authUrl;
  private usersUrl = environment.apiUrl + '/users';
  private socketsessionurl = environment.apiUrl + '/socketsession';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }

  /**
   * Function to login
   * @param data
   * @returns {Promise<T>|Promise<TResult|T>|Promise<TResult>|Promise<TResult2|TResult1>}
   */
  public login(data1: any) {
    return this.http.post(this.authUrl + '/enclogin/',
      { username: data1[0], data: data1[1] }, { headers: this.headers })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res.json();
      })
      .catch(reason => reason.json());
  }
  public getSocketsession(data1: any) {
    let user = window.localStorage['CUser'];
    let headers1 = new Headers({ 'content-type': 'application/json', 'user_id': user, 'x-access-token': window.localStorage['jwtToken'] });
    return this.http.post(this.socketsessionurl + '/getsocketsessiontoken/',
      data1, { headers: headers1 })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res.json();
      })
      .catch(reason => reason);
  }
  public get_initial_token(data: any) {
    return this.http.post(this.authUrl + '/get-initial-token/',
      { email: data.emaildata, company_id: data.company_id, timestamp: data.timestamp }, { headers: this.headers })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status)) {
          return res.json();
        }
      })
      .catch(reason => reason);
  }
  /**
   * Function to get all user
   * @returns {Promise<TResult2|TResult1>}
   */

  public get(params) {
    return this._apiService.post(this.usersUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Function create new user
   * @param data
   * @returns {Promise<TResult2|TResult1>}
   */

  public addUser(params) {
    return this._apiService.post(this.authUrl + '/users', params, { headers: this.headers })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Function delete user by id
   * @param id
   * @returns {Promise<TResult2|TResult1>}
   */

  public delete(id) {
    return this._apiService.post(this.usersUrl + '/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Get user by id
   * @param id
   * @returns {Promise<TResult2|TResult1>}
   */
  public getUserById(id) {
    return this._apiService.post(this.usersUrl + '/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getRefferalUSer(data) {
    return this._apiService.post(this.authUrl + '/getuserByRefferal', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  /**
   * Update user data by id
   * @param id
   * @param data
   * @param params
   * @returns {Promise<TResult2|TResult1>}
   */
  public updateUser(id, params) {
    return this._apiService.post(this.authUrl + '/users/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


  public updateLoyalityPoint(data) {
    return this._apiService.post(this.authUrl + '/updateLoyalityPoint', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }


  /**
   * Function for forgot password
   * @param data
   * @returns {Promise<any>}
   */
  public forgotPassword(data: any) {
    return this.http.post(this.authUrl + '/forgot-password/',
      { email: data.email }, { headers: this.headers })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res.json()
      });
  }

  /**
   * Verify JWT token
   * @param token
   * @returns {Promise<TResult2|TResult1>}
   */
  public verifyJWTToken(token: string, role) {
    return this._apiService.get(this.authUrl + '/verify-token?role=' + role + '&token=' + token)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update user password
   * @param token
   * @param params
   * @returns {Promise<TResult2|TResult1>}
   */
  public updateUserPassword(params) {
    return this._apiService.post(this.usersUrl + '/update-password', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


  /**
   * Update user password
   * @param token
   * @param params
   * @returns {Promise<TResult2|TResult1>}
   */
  public updateUserCustomerId(params) {
    return this._apiService.post(this.usersUrl + '/update-customer-id', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Verify old password
   * @param token
   * @param params
   * @returns {Promise<TResult2|TResult1>}
   */
  public verifyCurrentPassword(token, params) {
    return this._apiService.post(this.authUrl + '/verify-current-password?token=' + token, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public sendRefferalEmail(params) {
    return this._apiService.post(this.usersUrl + '/sendRefferalEmail', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public subscribeToMailChimp(data) {
    return this._apiService.post('api/mail-chimp/', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public updateUserVirtualMoney(data) {
    return this._apiService.post(this.usersUrl + '/updateVirtualMoney', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public contactUs(data) {
    return this._apiService.post(this.authUrl + '/users/contactUs', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Deactivate User by id
   * @param data
   * @returns {Promise<any>}
   */
  public deactivateUser(data) {
    return this._apiService.post(this.usersUrl + '/deactivate', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Reactivate User by id
   * @param data
   * @returns {Promise<any>}
   */
  public reactivateUser(data) {
    return this._apiService.post(this.usersUrl + '/reactivate', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}



