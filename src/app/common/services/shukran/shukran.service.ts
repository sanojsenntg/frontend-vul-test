import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { ShukranRestService } from './shukran-rest.service';

@Injectable()
export class ShukranService {

  constructor(private _ShukranService: ShukranRestService) { }
  public getShukranTransactions(params) {
    return this._ShukranService.getShukranTransactions(params)
      .then((res) => res);
  }
  public getShukranCustomers(params) {
    return this._ShukranService.getShukranCustomers(params)
      .then((res) => res);
  }
}
