import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class ShukranRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private shukranUrl = environment.apiUrl + '/shukran';
  constructor(private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }
  public getShukranTransactions(params) {
    return this._apiService.post(this.shukranUrl + '/transactions', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getShukranCustomers(params) {
    return this._apiService.post(this.shukranUrl + '/customers', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}
