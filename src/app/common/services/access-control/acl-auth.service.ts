import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AccessControlService } from './access-control.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
import { AclUrlCheckService } from './acl-url-check.service';
import { ToastsManager } from 'ng2-toastr';
import { EncDecService } from '../encrypt-decrypt-service/encrypt_decrypt_service';

const company_id:any = localStorage.getItem('user_company');
const session:any = localStorage.getItem('Sessiontoken');
const email:any = localStorage.getItem('user_email');

@Injectable()
export class AclAuthervice implements CanActivate {
  public path;
  public aclListJson;
  public aclPath: any;

  public flag
  public length;
  public constent;
  public aclNames;
  public aclListArr: any = [];
  public val;
  public res;
  public companyId:any = [];
  constructor(public _aclService: AccessControlService, public router: Router, public dialog: MatDialog, public overlay: Overlay, public auth: AclUrlCheckService, public toastr: ToastsManager, public encDecService:EncDecService) { }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let roles = route.data["roles"] as Array<string>;
    if (this.auth.isAuthenticated(roles)) {
      return true;
    } else {
      this.router.navigate(['/admin']);
      //this.toastr.warning('');
      return false;
    }
  }

  // public aclMenuforIconDisable() {
  //   return this.aclListJson = this._aclService.getAclUserMenu();
  // }
}