
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from './../access-control/access-control.service';

@Injectable()

export class CompanyrestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private companyUrl = environment.apiUrl + '/company';

  /**
  *
  * @param {Http} http
  * @param {ApiService} _apiService
  */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

	/**
   * Get all Companies
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getCompanyListing(params) {
    return this._apiService.post(this.companyUrl + '/get_companies', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Add a new company
   * @param params 
   */
  public addCompany(params) {
    return this._apiService.post(this.companyUrl + '/add_company', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Get company by id
   * @param id 
   */
  public getCompanyById(id) {
    return this._apiService.post(this.companyUrl + '/getById/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update company 
   * @param id 
   * @param params 
   */
  public updateCompanyById(params) {
    return this._apiService.post(this.companyUrl + '/updateCompany/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update delete status of company
   * @param id 
   */
  public updateDeletedStatus(id) {
    return this._apiService.post(this.companyUrl + '/updateDeleteStatus/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}

