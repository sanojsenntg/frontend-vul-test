import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';


@Injectable()


export class FaqRestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private QuestionAnswersUrl = environment.apiUrl + '/question_answer';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  /**
   * Get all FAQ
   * @param params 
   */
  public getAllFaq(params) {
    return this._apiService.post(this.QuestionAnswersUrl + '/getAllQuestionAnswers', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Add new FAQ
   * @param params 
   */
  public AddNewFaq(params) {
    return this._apiService.post(this.QuestionAnswersUrl + '/addQuestionAnswers', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Udate FAQ by id
   * @param id 
   * @param params 
   */
  public UpdateFaq(params) {
    return this._apiService.post(this.QuestionAnswersUrl + '/updateQuestionAnswers/' , params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Udate FAQ by id
   * @param id 
   * @param params 
   */
  public getFaqById(id) {
    return this._apiService.post(this.QuestionAnswersUrl + '/getQuestionAnswersById/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update delete status
   * @param id 
   * @param params 
   */
  public updateDeleteStatus(id) {
    return this._apiService.post(this.QuestionAnswersUrl + '/updateDeleteStatus/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}