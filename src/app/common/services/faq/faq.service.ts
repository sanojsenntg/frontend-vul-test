import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { ApiService } from '../api/api.service';
import { FaqRestService } from './faqrest.service';

@Injectable()
export class FaqService {

  constructor( private _faqRestService: FaqRestService) { }

  /**
   * Get all FAQ
   * @param params 
   */
  public getAllFaq (params) {
   return this._faqRestService.getAllFaq(params)
     .then((res) => res);
  }

  /**
   * Add new FAQ
   * @param params 
   */
  public AddNewFaq (params) {
    return this._faqRestService.AddNewFaq(params)
      .then((res) => res);
  }

  /**
   * Add new FAQ
   * @param params 
   */
  public UpdateFaq (params) {
    return this._faqRestService.UpdateFaq(params)
      .then((res) => res);
  }

  /**
   * Get FAQ by id
   * @param id 
   */
  public getFaqById (id) {
    return this._faqRestService.getFaqById(id)
      .then((res) => res);
  }

  /**
   * Update delete status
   * @param id 
   * @param params 
   */
  public updateDeleteStatus(id){
    return this._faqRestService.updateDeleteStatus(id)
     .then((res) => (res));
  }
}
