import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { environment } from '../../../../environments/environment';


@Injectable()
export class TwoGisService {
  private twoGisKey = environment.twogisKey;
  constructor(
    private http: Http,
  ) { }

  get(path: string): Observable<any> {
    return this.http.get(path).map((res: Response) => res.json());;
  }
  public twoGisAutoComplete(params) {
    var url = "https://catalog.api.2gis.ru/2.0/suggest/list?q="+ params +"+&key="+ this.twoGisKey +"&region_id=99";
    return this.get(url)
    .map((res) => {
        return res
    });
  }
  public twoGisPlaceFind(params) {
    var url = "https://catalog.api.2gis.ru/2.0/geo/building/entrances?key="+ this.twoGisKey +"&id=" + params;
    return this.get(url)
    .map((res) => {
        return res
    });
  }
  public twoGisFindId(params) {
    var url = "http://catalog.api.2gis.ru/2.0/geo/search?key="+ this.twoGisKey +"&q="+ params +"&region_id=99";
    return this.get(url)
    .map((res) => {
        return res
    });
  }
}
