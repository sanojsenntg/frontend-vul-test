import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';

import { DevicelocationRestService } from './devicelocationrest.service';

@Injectable()
export class DevicelocationService {

  constructor( private devicelocationRestService: DevicelocationRestService ) { }

  public getDeviceLocations (params) {

    return this.devicelocationRestService.getDeviceLocations(params)
      .then((res) => res);
  }

  public getLoginDeviceLocation (params) {
    return this.devicelocationRestService.getLoginDeviceLocation(params)
      .then((res) => res);
  }
  
   public getFilteredData(request) {

    return this.devicelocationRestService.getFilteredDataStatus(request)
      .then((res) => res);
  }

  public getdatabyvehicleid(params) {

    return this.devicelocationRestService.getdatabyvehicleid(params)
      .then((res) => res);
  }
  public getdatabyvehicleidNew(params) {

    return this.devicelocationRestService.getdatabyvehicleidNew(params)
      .then((res) => res);
  }
  public getVehicleLastLocation(request) {
    return this.devicelocationRestService.getVehicleLastLocation(request)
      .then((res) => res);
  }
  public vehicleTrackLocations(request) {
    return this.devicelocationRestService.vehicleTrackLocations(request)
      .then((res) => res);
  }
  public getMapData(request) {

    return this.devicelocationRestService.getMapData(request)
      .then((res) => res);
  }
}





