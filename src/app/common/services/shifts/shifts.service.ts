import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { ShiftsrestService } from './shiftsrest.service';

@Injectable()
export class ShiftsService {
  constructor(  private _shiftsrestService: ShiftsrestService ) { }

  public getshiftinfo (data) {
    return this._shiftsrestService.getshiftinfo(data)
      .then((res) => res);
  }

  public getOrdersData (data) {
    return this._shiftsrestService.getOrdersBasedOnShift(data)
      .then((res) => res);
  }

  public updateShiftsForApproving(id) {
    return this._shiftsrestService.updateApprovedShifts(id)
      .then((res) => res);
  }

  public getShiftDetailByDriverId (data) {
    return this._shiftsrestService.getShiftDetailByDriverId(data)
      .then((res) => res);
  }
  public getShiftIds(data) {
    return this._shiftsrestService.getShiftIDs(data)
      .then((res) => res);
  }
  public addshiftdetail(){
    let params ={
      'company':'Dubai Taxi',
      'shift_duration':'5:00',
      'occupied_time':'4:00',
      'free_time':'00:30',
      'last_stop':'13:00',
      'first_start':'08:00',
      'orders_distance':'28',
      'driver_formated':'',
      'total_distance':'28',
      'bill_printed':'193',
      'bill_printed_count':'5',
      'bill_not_printed':'8.00',
      'bill_not_printed_count':'7',
      'no_payment':'',
      'no_payment_count':'',
      'free_rides':'0',
      'total':'200',
      'status':'',
      'payed':'193.00',
      'to_pay':'7.00',
      'drive':'',
      'trip_partially_paid_by_credit':'yes',
      'credit_total':'7.00',
     'cash_trip_total':'200',
      'credit':'7.00',
      'revenue':'6',
      'no_of_trips':'8',
      'revenue_km':'5',
      'lost_km':'1',
      'total_km':'28',
      'lost_total_percentage':'0.01',
      'payment_extra':[
        {id:"5ac328a1b96a4bb457aa0cc4",name:"GPS taximeter tariff",amt_without_vat:"",vat:"15.5",amt:"45.5"},
        {id:"5ac328a1b96a4bb457aa0cc4",name:"GPS taximeter tariff",amt_without_vat:"",vat:"15.5",amt:"45.5"}
      ],
      'shift_id':'5ac373bdd51afa4f01926956'
    }
    console.log(params);
    return this._shiftsrestService.addshiftdetail(params)
    .then((res) => res);

  }

  public getShiftData (data) {
    return this._shiftsrestService.getShiftData(data)
      .then((res) => res);
  }

  public getAssignedDrivers (data) {
    return this._shiftsrestService.getAssignedDrivers(data)
      .then((res) => res);
  }
  
  public getAllShifts (params) {
    return this._shiftsrestService.getAllShifts( params )
    .then((res) => res);
  }

  public searchShifts(params) {
    return this._shiftsrestService.searchShifts(params)
    .then((res) => res);
  }
  public getShiftCount(params) {
    return this._shiftsrestService.getShiftCount(params)
    .then((res) => res);
  }
  public getShiftForCsv(params) {
    return this._shiftsrestService.getShiftForCsv(params)
    .then((res) => res);
  }
  public getAllShiftsummary (params) {
    return this._shiftsrestService.getAllShiftsummary( params )
    .then((res) => res);
  }
  public getAssignedSignedOutDrivers (data) {
    return this._shiftsrestService.getAssignedSignedOutDrivers(data)
      .then((res) => res);
  }
}
