import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { Observable } from 'rxjs';
import { ToastsManager } from 'ng2-toastr';
import { JwtService } from '../api/jwt.service';
import { Router } from '@angular/router';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { Socket} from 'ng-socket-io';
@Injectable()
export class EncDecService {
    public error_case = {
        status: 404
    };
    constructor(public toastr: ToastsManager,
        private _jwtService: JwtService,
        private socket: Socket,
        private router: Router,private http: Http) { }

    //The set method is use for decrypt the value.
    public dwt(keys, value): Observable<any> {
        keys = localStorage.getItem('Sessiontoken');
        var rev = CryptoJS.enc.Hex.parse(value);
        var bytes = rev.toString(CryptoJS.enc.Base64);
        var key = CryptoJS.enc.Utf8.parse(keys);
        var iv = CryptoJS.enc.Utf8.parse('9112303577857981');
        try {
            var decrypted: any = CryptoJS.AES.decrypt(bytes, key, {
                keySize: 256 / 32,
                iv: iv,
                mode: CryptoJS.mode.CBC
            });
            var dect = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
            return dect
        }
        catch (e) {
            console.log('Decryption Error', e);
            //this.toastr.error("Invalid");
            // this.socket.removeAllListeners();
            // this._jwtService.destroyDispatcherToken();
            // this._jwtService.destroyAdminToken();
            // this.router.navigate(["/"]);
        }
    }

    //The get method is use for encrypt the value.
    public nwt(keys, value): Observable<any> {
        keys = localStorage.getItem('Sessiontoken');
        var key = CryptoJS.enc.Utf8.parse(keys);
        var iv = CryptoJS.enc.Utf8.parse('9112303577857981');
        try {
            var encrypted: any = CryptoJS.AES.encrypt(JSON.stringify(value), key,
                {
                    keySize: 256 / 32,
                    iv: iv,
                    mode: CryptoJS.mode.CBC
                });
            var dect = encrypted.toString(CryptoJS.format.Hex);
            return dect;
        } catch (e) {
            console.log('Encryption Error', e);
            //this.toastr.error("Invalid");
            // this.socket.removeAllListeners();
            // this._jwtService.destroyDispatcherToken();
            // this._jwtService.destroyAdminToken();
            // this.router.navigate(["/"]);
        }
    }

    public loginDwt(keys, value): Observable<any> {
        var rev = CryptoJS.enc.Hex.parse(value);
        var bytes = rev.toString(CryptoJS.enc.Base64);
        var key = CryptoJS.enc.Utf8.parse(keys);
        var iv = CryptoJS.enc.Utf8.parse('9112303577857981');
        try {
            var decrypted: any = CryptoJS.AES.decrypt(bytes, key, {
                keySize: 256 / 32,
                iv: iv,
                mode: CryptoJS.mode.CBC
            });
            var dect = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
            return dect
        }
        catch (e) {
            console.log('Decryption Error', e);
            //this.toastr.error("Invalid");
            // this._jwtService.destroyDispatcherToken();
            // this._jwtService.destroyAdminToken();
            // this.router.navigate(["/"]);
        }
    }

    //The get method is use for encrypt the value.
    public loginNwt(keys, value): Observable<any> {
        var key = CryptoJS.enc.Utf8.parse(keys);
        var iv = CryptoJS.enc.Utf8.parse('9112303577857981');
        try {
            var encrypted: any = CryptoJS.AES.encrypt(JSON.stringify(value), key,
                {
                    keySize: 256 / 32,
                    iv: iv,
                    mode: CryptoJS.mode.CBC
                });
            var dect = encrypted.toString(CryptoJS.format.Hex);
            return dect;
        } catch (e) {
            console.log('Encryption Error', e);
            //this.toastr.error("Invalid");
            // this.socket.removeAllListeners();
            // this._jwtService.destroyDispatcherToken();
            // this._jwtService.destroyAdminToken();
            // this.router.navigate(["/"]);
        }
    }

    public sockDwt(keys, value): Observable<any> {
        if(localStorage.getItem('socketFlag') == '0'){
            return;
        }
        keys = localStorage.getItem('SocketSessiontoken');
        var rev = CryptoJS.enc.Hex.parse(value);
        var bytes = rev.toString(CryptoJS.enc.Base64);
        var key = CryptoJS.enc.Utf8.parse(keys);
        var iv = CryptoJS.enc.Utf8.parse('9112303577857981');
        try {
            var decrypted: any = CryptoJS.AES.decrypt(bytes, key, {
                keySize: 256 / 32,
                iv: iv,
                mode: CryptoJS.mode.CBC
            });
            var dect = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
            return dect;
        }
        catch (e) {
            console.log('Socket Decryption Error', e);
            let param_socket = {
                company_id: window.localStorage['user_company']
            }
            let enc_data = this.nwt(window.localStorage['Sessiontoken'], param_socket);
            let data1 = {
                data: enc_data,
                email: window.localStorage['user_email']
            }
            let user = window.localStorage['CUser'];
            let headers1 = new Headers({ 'content-type': 'application/json', 'user_id': user, 'x-access-token': window.localStorage['jwtToken'] });
            this.http.post(environment.apiUrl + '/socketsession' + '/getsocketsessiontoken/',
                data1, { headers: headers1 })
                .subscribe((resp: any) => {
                    var dec:any = resp.json();
                    if (dec.status == 200) {
                        var res_data1: any = this.dwt(window.localStorage['Sessiontoken'], dec.data);
                        window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
                    }
                })
        }
    }

    public sockNwt(keys, value): Observable<any> {
        if(localStorage.getItem('socketFlag') == '0'){
            return;
        }
        keys = localStorage.getItem('SocketSessiontoken');
        var key = CryptoJS.enc.Utf8.parse(keys);
        var iv = CryptoJS.enc.Utf8.parse('9112303577857981');
        try {
            var encrypted: any = CryptoJS.AES.encrypt(JSON.stringify(value), key,
                {
                    keySize: 256 / 32,
                    iv: iv,
                    mode: CryptoJS.mode.CBC
                });
            var dect = encrypted.toString(CryptoJS.format.Hex);
            return dect;
        } catch (e) {
            console.log('Socket Encryption Error', e);
            let param_socket = {
                company_id: window.localStorage['user_company']
            }
            let enc_data = this.nwt(window.localStorage['Sessiontoken'], param_socket);
            let data1 = {
                data: enc_data,
                email: window.localStorage['user_email']
            }
            let user = window.localStorage['CUser'];
            let headers1 = new Headers({ 'content-type': 'application/json', 'user_id': user, 'x-access-token': window.localStorage['jwtToken'] });
            this.http.post(environment.apiUrl + '/socketsession' + '/getsocketsessiontoken/',
                data1, { headers: headers1 })
                .toPromise()
                .then((resp: any) => {
                    var dec = resp.json();
                    if (dec.status == 200) {
                        var res_data1: any = this.dwt(dec.session_token, dec.data);
                        window.localStorage['SocketSessiontoken'] = res_data1.socket_token;
                    }
                })
        }
    }

}