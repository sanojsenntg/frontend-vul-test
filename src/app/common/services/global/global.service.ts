import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class GlobalService {
  observableLog;
  observableCSV;
  observableCSVProgress;
  public logout=false;
  constructor() {
    this.observableLog = new BehaviorSubject(this.logout);
    this.observableCSV = new BehaviorSubject(this.csv);
    this.observableCSVProgress = new BehaviorSubject(this.csvProgress);
  }
  public devices:any;
  public drivers:any;
  public dispatcher_orders:any;
  public order_management:any;
  public shifts:any;
  public customer:any;
  public promo_transaction:any;
  public marketing_promo_transaction:any;
  public dispatcher_customers:any;
  public csv:boolean;
  public csvProgress=0;
  public externalDriversList:any;
  public driverStats:any;
  public driverAvailability:any;
  public driverAcceptance :any;
  setLogoutStatus(status){
    this.logout=status;
    this.eventChange();
  }
  eventChange() {
    this.observableLog.next(this.logout);
  }
  setCsvStatus(status,progress){//checks csv creation is active in a page or not
    this.csv=status;
    this.csvProgress=progress;
    this.csvEventChange();
  }
  csvEventChange() {
    this.observableCSV.next(this.csv);
    this.observableCSVProgress.next(this.csvProgress);
  }
}
