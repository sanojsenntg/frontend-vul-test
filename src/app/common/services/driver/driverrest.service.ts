import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';


@Injectable()


export class DriverRestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private driverUrl = environment.apiUrl + '/drive';
  private uploadUrl = environment.apiUrl + '/uploaddriver';
  private driverLanguageUrl = environment.apiUrl + '/driver-language';
  private driverGroupUrl = environment.apiUrl + '/driver-groups';



  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }
  public uploadDriverList(params) {
    return this._apiService.post(this.uploadUrl + '/xlsxupload', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAllDrivers(params) {
    return this._apiService.post(this.driverUrl + '/driver_details', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAllDriversCSV(params) {
    return this._apiService.post(this.driverUrl + '/driver_export', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getDrivers(params) {
    return this._apiService.post(this.driverUrl + '/drivers', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getLoginDrivers(params) {
    return this._apiService.post(this.driverUrl + '/login_drivers', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


  public getDriver(params) {
    return this._apiService.post(this.driverUrl + '/getdriverinfo', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public ChangeDriverStatus(params) {
    return this._apiService.post(this.driverUrl + '/changeBlockStatus', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public searchDriver(params) {
    return this._apiService.post(this.driverUrl + '/driverRandomSearch', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  public searchDevice(params) {
    return this._apiService.post(this.driverUrl + '/deviceSearch', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  public ForceLogout(params) {
    return this._apiService.post(this.driverUrl + '/ForceLogout', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


  public saveDriver(params) {
    return this._apiService.post(this.driverUrl + '/savedriver', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  public updateDriver(params) {
    return this._apiService.post(this.driverUrl + '/updateDriver', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getDriverLanguages(params) {
    return this._apiService.post(this.driverLanguageUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


  public deleteDriver(params) {
    return this._apiService.post(this.driverUrl + '/deleteDriver', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateDriverLanguages(params) {
    return this._apiService.post(this.driverUrl + '/updateDriverLanguage/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getDriverLanguageById(id) {
    return this._apiService.post(this.driverUrl + '/getDriverLanguageById/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateShiftTimings(params) {
    return this._apiService.post(this.driverUrl + '/updateShiftTimings/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getDriverGroup(params) {
    return this._apiService.post(this.driverGroupUrl + '/list', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getDriverGroupsById(id) {
    return this._apiService.post(this.driverUrl + '/getGroup/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateAdditionalService(params) {
    return this._apiService.post(this.driverUrl + '/updatedAdditionalService/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public deleteDriverArray(params) {
    return this._apiService.post(this.driverUrl + '/deleteDriverArray', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public searchForDriver(params) {
    return this._apiService.post(this.driverUrl + '/driversforfilters', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }
  public searchForTempDriver(params) {
    return this._apiService.post(this.driverUrl + '/tempdriversforfilters', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }
  public appDriverdets(params) {
    return this._apiService.post(this.driverUrl + '/getDriverTrips', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public updateDriverStatus(params) {
    return this._apiService.post(this.driverUrl + '/changedriverstatus/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public updateDriverGroup(params) {
    return this._apiService.post(this.driverUrl + '/updatedrivergroup', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public ForceShiftCLose(params) {
    return this._apiService.post(this.driverUrl + '/ForceShiftClose', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public driversInDriverGroup(params) {
    return this._apiService.post(this.driverGroupUrl + '/getDriversInDriverGroup', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getDriverStats(params) {
    return this._apiService.post(this.driverUrl + '/driver-stats', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getDriverShiftInfo(params) {
    return this._apiService.post(this.driverUrl + '/login-logout', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getTripRating(params) {
    return this._apiService.post(this.driverUrl + '/rated-trips', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAvailabilityTime(params) {
    return this._apiService.post(this.driverUrl + '/availability', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  public getAcceptanceRate(params) {
    return this._apiService.post(this.driverUrl + '/acceptance-progress', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}
