import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { ApiService } from '../api/api.service';
import { DriverRestService } from './driverrest.service';
import { DriverGroupRestService } from '../driver_groups/driver_groupsrest.service';

@Injectable()
export class DriverService {
  constructor(private _driverRestService: DriverRestService, private _driverGroupService: DriverGroupRestService) {
  }
  public getDriverListing(params) {
    return this._driverRestService.getLoginDrivers(params)
      .map((res) => res);

  }
  public uploadDriverList(params) {
    return this._driverRestService.uploadDriverList(params)
      .then((data) => data);
  }
  public getAllDrivers(params) {
    return this._driverRestService.getAllDrivers(params)
      .map((res) => res);
  }
  public getAllDriversCSV(params){
    return this._driverRestService.getAllDriversCSV(params)
      .map((res) => res);
  }
  public ChangeDriverStatus(params) {
    return this._driverRestService.ChangeDriverStatus(params)
      .then((res) => res);
  }

  public searchDriver(params) {
    return this._driverRestService.searchDriver(params)
      .then((res) => res);
  }

  public searchForDevices(params) {
    return this._driverRestService.searchDevice(params)
      .then((res) => res);
  }

  public ForceLogout(params) {
    return this._driverRestService.ForceLogout(params)
      .then((res) => res);
  }


  public getDriverByName(data) {
    const params = {
      offset: 0,
      limit: 0,
      sortOrder: 'desc',
      sortByColumn: '_id',
      search: data,
      role: 'dispatcher'
    };
    return this._driverRestService.getDrivers(params)
      .map((res) => res);

  }

  public getLoginDriverByName(data) {
    return this._driverRestService.getLoginDrivers(data)
      .map((res) => res);

  }

  public getLoginDriverById(params) {
    return this._driverRestService.getLoginDrivers(params)
      .map((res) => res);

  }

  public getDriverByDeviceId(data) {
    const params = {
      device_id: data
    };
    return this._driverRestService.getDriver(params)
      .then((res) => res);
  }


  public saveDriver(params) {
    return this._driverRestService.saveDriver(params)
      .then((res) => res);
  }

  public updateDriver(data) {
    return this._driverRestService.updateDriver(data)
      .then((res) => res);
  }

  public getDriverLanguage(params) {
    return this._driverRestService.getDriverLanguages(params)
      .then((res) => res);
  }


  public deleteDriver(data) {
    return this._driverRestService.deleteDriver(data)
      .then((res) => res);

  }

  public updateDriverLanguage(params) {
    return this._driverRestService.updateDriverLanguages(params)
      .then((res) => res);
  }

  public getDriverLanguageById(id) {
    return this._driverRestService.getDriverLanguageById(id)
      .then((res) => res);
  }

  public updateShiftTimings(params) {
    return this._driverRestService.updateShiftTimings(params)
      .then((res) => res);
  }

  public getDriversGroup(params) {

    return this._driverGroupService.getDriverGroup(params)
      .map((res) => res);
  }

  public getDrivergroupsById(id) {
    return this._driverRestService.getDriverGroupsById(id)
      .then((res) => res);
  }

  public updateAdditionalService(params) {

    return this._driverRestService.updateAdditionalService(params)
      .then((data) => data);
  }

  public deleteDriverArray(data) {
    return this._driverRestService.deleteDriverArray(data)
      .then((res) => res);
  }
  public searchForDriver(params) {
    return this._driverRestService.searchForDriver(params)
      .map((res) => res);
  }
  public searchForTempDriver(params) {
    return this._driverRestService.searchForTempDriver(params)
      .map((res) => res);
  }
  public getappDriverdets(data) {
    return this._driverRestService.appDriverdets(data)
      .then((res) => res);
  }
  public updateDriverStatus(params) {
    return this._driverRestService.updateDriverStatus(params)
      .then((data) => data);
  }
  public updateDriverGroup(data) {
    return this._driverRestService.updateDriverGroup(data)
      .then((res) => res);
  }
  public ForceShiftClose(params) {
    return this._driverRestService.ForceShiftCLose(params)
      .then((res) => res);
  }
  public driversInDriverGroup(params) {
    return this._driverRestService.driversInDriverGroup(params)
      .then((res) => res);
  }
  public getDriverStats(params) {
    return this._driverRestService.getDriverStats(params)
      .map((res) => res);
  }
  public getDriverShiftInfo(params) {
    return this._driverRestService.getDriverShiftInfo(params)
      .map((res) => res);
  }
  public getTripRating(params) {
    return this._driverRestService.getTripRating(params)
      .then((res) => res);
  }
  public getAvailabilityTime(params) {
    return this._driverRestService.getAvailabilityTime(params)
      .then((res) => res);
  }
  public getAcceptanceRate(params) {
    return this._driverRestService.getAcceptanceRate(params)
      .then((res) => res);
  }
}