import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()
export class PoiService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private getUrl = environment.apiUrl + '/poi';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  public poiList(params) {
    return this._apiService.post(this.getUrl + '/poiList', params).toPromise().then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }
  public poiAdd(data) {
    return this._apiService.post(this.getUrl + '/addPoi', data).toPromise().then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }
  public poiDelete(data) {
    return this._apiService.post(this.getUrl + '/deletePoi', data).toPromise().then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }
  public poiUpdate(data) {
    return this._apiService.post(this.getUrl + '/updatePoiDetails', data).toPromise().then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }
  public poiDetails(data) {
    return this._apiService.post(this.getUrl + '/poiDetails', data).toPromise().then((res) => {
      if (this.accessControl.checkSession(res.status))
        return res
    });
  }

}
