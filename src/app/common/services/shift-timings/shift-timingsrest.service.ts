
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class ShiftTimingsrestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private ShiftTimingUrl = environment.apiUrl + '/shift-timings';

  /**
   *
   * @param {Http} http
   * @param {ApiService} _apiService
   */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

  /**
   * Save Predefined-message details.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getShiftTiming(params) {
    return this._apiService.post(this.ShiftTimingUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * get shift timings by Id
   * @param params
   * @returns {Promise<any>}
   */

  public getShiftTimingById(params) {
    return this._apiService.post(this.ShiftTimingUrl + '/getShiftTimingsById', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * save shift timings
   * @param params
   * @returns {Promise<any>}
   */
  public saveShiftTiming(params) {
    return this._apiService.post(this.ShiftTimingUrl + '/addShiftTimings', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  /**
   * update shiftimings
   * @param shift_id
   * @param params
   * @returns {Promise<any>}
   */
  public updateShiftTiming(params) {
    return this._apiService.post(this.ShiftTimingUrl + '/updateShiftTimings', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  /**
   * delete shift timings
   * @param shift_id
   * @returns {Promise<any>}
   */
  public deleteShiftTimings(shift_id) {
    return this._apiService.post(this.ShiftTimingUrl + '/deleteShiftTimings/', shift_id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
}

