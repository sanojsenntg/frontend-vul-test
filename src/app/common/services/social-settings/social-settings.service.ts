import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { SocialSettingsrestService } from './social-settingsrest.service'
import { ApiService } from '../api/api.service';

@Injectable()
export class SocialSettingsService {

  constructor( public _socialSettingsrestService : SocialSettingsrestService ) { }

  /**
   * Get all social settings
   * @param params 
   */
  public getAllSocialSettings(params){
    return this._socialSettingsrestService.getAllSocialSettings(params)
    .then((res)=>(res));    
  }

  /**
   * Update status of social settings
   * @param id 
   * @param params 
   */
  public updateSocialStatus(params){
    return this._socialSettingsrestService.updateSocialStatus( params)
    .then((res)=>(res));    
  }

}
