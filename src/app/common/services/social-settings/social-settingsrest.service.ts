import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';


@Injectable()


export class SocialSettingsrestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private socialSettingsUrl = environment.apiUrl + '/social_settings';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  /**
   * Get all social settings
   * @param params 
   */
  public getAllSocialSettings(params) {
    return this._apiService.post(this.socialSettingsUrl + '/getAllSocialSettings', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update status of social settings
   * @param params 
   */
  public updateSocialStatus(params) {
    return this._apiService.post(this.socialSettingsUrl + '/updateSocialStatus/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}