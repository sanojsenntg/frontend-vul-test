import { Injectable } from '@angular/core';
declare var require: any;
import { environment } from '../../../../environments/environment';

const wsEvents = require('ws-events/index.js')
//const ws = wsEvents(new WebSocket(environment.socketUrl));
const ws = new WebSocket(environment.socketUrl);

@Injectable()
export class WebsocketConnService {

  public socketconn() {
    return ws;

  }
}
