import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from './../access-control/access-control.service';


@Injectable()


export class AdditionalrestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private AddUrl = environment.apiUrl + '/addservice';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }
  public get(params) {
    return this._apiService.post(this.AddUrl, params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
* Get Vehicles.
* @param params
* @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
*/
  public getaddService(params) {
    return this._apiService.post(this.AddUrl + '/service', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Function to save  vehicles
   *
   * @param params
   * @returns {Observable<any>}
   */
  public saveServiceGroup(params) {
    return this._apiService.post(this.AddUrl + '/addservicegroup', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }
  /**
  * Function to delete Vehicles.
  * @param params
  * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
  */
  public delete(id) {
    return this._apiService.post(this.AddUrl + '/deleteservicegroup/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
  * Get  Vehicle BY Id
  * @param params
  * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
  */
  public getServiceById(id) {
    return this._apiService.post(this.AddUrl + '/additionalservice/',id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
  * Get searched Vehicles.
  * @param params
  * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
  */
  public updateService(params) {
    return this._apiService.post(this.AddUrl + '/updateservicegroup/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }



  /**
   * Update delete status Vehicles.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updateServiceForDeletion(id) {
    return this._apiService.post(this.AddUrl + '/status/updateForDeletion/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


}
