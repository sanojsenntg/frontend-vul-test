import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { JwtService } from '../api/jwt.service';
import { json } from 'body-parser';
import { Observable } from "rxjs/Observable";
import { DispatcherRestService } from './dispatcherrest.service';

@Injectable()
export class DispatcherService implements CanActivate {

  /**
   * Constructor for admin services
   * @param {JwtService} _jwtService
   * @param {Router} router
   */
  constructor(private _jwtService: JwtService,
    private _dispatcherRestService: DispatcherRestService,
    private router: Router) {
  };

  /**
   * Activate route
   * @param route
   * @param state
   * @returns {boolean}
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;
    return this.verifyLogin(url);
  }

  /**
   * Verify if user is logged in or not
   * @param url
   * @returns {boolean}
   */
  verifyLogin(url: string): boolean {
    if (this._jwtService.getToken()) {
      if (window.localStorage['dispatcherUser']) {
        return true;
      } else if (window.localStorage['adminUser']) {
        let Menu = localStorage.getItem('userMenu');
        let userMenu = JSON.parse(Menu);
        for (let i = 0; i < userMenu.length; i++) {
          if (userMenu[i] == "Dispatcher Login") {
            return true;
          }
        }
      }
    }
    this._jwtService.destroyDispatcherToken();
    this.router.navigate(['/dispatcher/login']);
    return false;
  }

  getDispatcherUser(params) {
    return this._dispatcherRestService.getDispatcherUser(params).then((res) => res);
  }



}
