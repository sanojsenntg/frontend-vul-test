import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';

import {VehicleCategoryRestService} from './vehicle-categoryrest.service';

@Injectable()
export class VehicleCategoryService {

  constructor(private _vehicleCategoryRestService: VehicleCategoryRestService) {
  }

  /**
   * Function to get list of vehicle category
   *
   * @returns {Observable<any>}
   */
  public getVehicleCategories(params) {
    return this._vehicleCategoryRestService.getVehicleCategory(params)
      .then((res) => res);
  }
  
  /**
   * Function to get vehicle category by id
   *
   * @returns {Observable<any>}
   */
  public getVehicleCategoryById(id) {
    return this._vehicleCategoryRestService.getVehicleCategoryById(id)
      .then((res) => res);
  }

  /**
   * Function to save vehicle category
   *
   * @returns {Observable<any>}
   */
  public addVehicleCategory(data) {
    return this._vehicleCategoryRestService.saveVehicleCategory(data)
      .then((res) => res);
  }

  /**
   * Function Update vehicle category by id.
   * 
   */
  public updateVehicleCategory(params) {
    return this._vehicleCategoryRestService.updateVehicleCategory( params)
      .then((res) => res);
  }

  /**
    * Function to update status for deletion.
    * 
    */
  public updateVehicleCategoryForDeletion(id) {
    return this._vehicleCategoryRestService.deleteVehicleCategory(id)
      .then((res) => res);
  }

}



