import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from './../access-control/access-control.service';

@Injectable()

export class BlockedDeviceRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private deviceUrl = environment.apiUrl + '/blocked-device';
  private devicesUrl = environment.apiUrl + '/blocked-device/searchDeviceid';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  public getBlockedDevices(params) {
    return this._apiService.post(this.deviceUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  public getBlockedDevicesIdForSearch(params) {
    return this._apiService.post(this.devicesUrl, params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }

  public delete(id) {
    return this._apiService.post(this.deviceUrl + '/delete/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


  public addBlockedDevice(params) {
    return this._apiService.post(this.deviceUrl + '/add/', params, { headers: this.headers })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateDevice(params) {
    return this._apiService.post(this.deviceUrl + '/update/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getBlockedDeviceById(id) {
    return this._apiService.post(this.deviceUrl + '/get/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateforDeletion(id) {
    return this._apiService.post(this.deviceUrl + '/updateForDeletion/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getBlockedDevicesforDeletion(params) {
    return this._apiService.post(this.deviceUrl + '/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });

  }





}