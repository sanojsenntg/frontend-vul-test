import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import {MessagerestService} from './messagerest.service';

@Injectable()
export class MessageService {
  constructor(  private _messagerestService: MessagerestService ) { }

  public preMessage (params) {
    return this._messagerestService.preMessage(params)
        .then((res) => res);
  }

  public sendMessage(params) {
    return this._messagerestService.sendMessage(params)
      .then((res) => res);

  }

  public sendBroadcastMessage(data) {
    return this._messagerestService.sendBroadcastMessage(data)
      .then((res) => res);

  }

  public getMessageHistory(data) {

    return this._messagerestService.getMessageHistory(data)
      .then((res) => res);
  }

  public getAllMessageHistories(params) {
    return this._messagerestService.getAllMessagesHistory(params)
      .then((res) => res);
  }

}



