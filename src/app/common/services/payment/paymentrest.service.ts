import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';


@Injectable()

export class PaymentRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private paymentUrl = environment.apiUrl + '/payment';
  private paymentsUrl = environment.apiUrl + '/payment/add';
  private paymentEditUrl = environment.apiUrl + '/payment/edit';
  private paymentGetUrl = environment.apiUrl + '/payment/get';
  private paymentDeleteUrl = environment.apiUrl + '/payment/delete';
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { }

  public getPayment(params) {
    return this._apiService.post(this.paymentUrl, params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public savePayment(params) {
    return this._apiService.post(this.paymentsUrl, params, { headers: this.headers })
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getPaymentById(id) {
    return this._apiService.post(this.paymentGetUrl + '/' + id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updatePayment(params) {
    return this._apiService.post(this.paymentEditUrl + '/' + params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public delete(id) {
    return this._apiService.post(this.paymentDeleteUrl + '/' + id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updatePaymentForDeletion(id) {
    return this._apiService.post(this.paymentUrl + '/updateForDeletion/' + id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}
