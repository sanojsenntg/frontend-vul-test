import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { ApiService } from '../api/api.service';
import { PaymentRestService } from './paymentrest.service';

@Injectable()
export class PaymentService {

  constructor(private _paymentRestService: PaymentRestService) { }

  public getPaymentListing (params) {
   return this._paymentRestService.getPayment(params)
     .map((res) => res);
 }

  public getPaymentListingByname(params) {
    return this._paymentRestService.getPayment(params)
      .map((res) => res);
  }


  public addPayments(data) {
    return this._paymentRestService.savePayment(data)
      .then((res) => res);
  }

  public getById(id) {
    return this._paymentRestService.getPaymentById(id)
      .then((res) => res);
  }

  public updatePaymentDetails(params) {
    return this._paymentRestService.updatePayment(params)
      .then((res) => res);
  }

  public updatePaymentForDeletion(id) {
    return this._paymentRestService.updatePaymentForDeletion(id)
      .then((res) => res);
  }

  public delete(id) {
    return this._paymentRestService.delete(id)
      .then((res) => res);
  }


}
