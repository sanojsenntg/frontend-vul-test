import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { ApiService } from '../api/api.service';
import { ContactusCategoryRestService } from './contactus-category-rest.service';

@Injectable()

export class ContactusCategoryService {

  constructor( private _contactusCategoryRestService: ContactusCategoryRestService) { }

  /**
   * Get all contactus category
   * @param params 
   */
  public getAllContactusCategory (params) {
   return this._contactusCategoryRestService.getAllContactusCategory(params)
     .then((res) => res);
  }

  /**
   * Add new contactus category
   * @param params 
   */
  public AddNewContactusCategory (params) {
    return this._contactusCategoryRestService.AddNewContactusCategory(params)
      .then((res) => res);
  }

  /**
   * Add new contactus category
   * @param params 
   */
  public UpdateContactusCategory (params) {
    return this._contactusCategoryRestService.UpdateContactusCategory( params)
      .then((res) => res);
  }

  /**
   * Get contactus category by id
   * @param id 
   */
  public getContactusCategoryById (id) {
    return this._contactusCategoryRestService.getContactusCategoryById(id)
      .then((res) => res);
  }

  /**
   * Update delete status
   * @param id 
   * @param params 
   */
  public updateDeleteStatus(id){
    return this._contactusCategoryRestService.updateDeleteStatus(id)
     .then((res) => (res));
  }
}

