import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router';

import { JwtService } from './jwt.service';

@Injectable()
export class ApiService {
  constructor(
    private http: Http,
    private jwtService: JwtService,
    private router: Router
  ) { }

  private setHeaders(headers: Object = {}): Headers {
    headers['Accept'] = 'application/json';

    if (this.jwtService.getToken()) {
      let user = window.localStorage['CUser'];
      //console.log(user);
      headers['user_id'] = user;
      headers['x-access-token'] = this.jwtService.getToken();
      headers['user-details'] = localStorage.getItem('user-details')
    }
    return new Headers(headers);
  }

  /**
   *
   * @param error
   * @returns {ErrorObservable<any>}
   */
  private formatErrors(error: any) {
    console.log('error', error);
    let err = error;
    // if (err.message == 'Session expired') {
    //   localStorage.clear();
    //   this.router.navigate(['/']);
    // }
    return Observable.throw(err);

  }

  /**
   *
   * @param {string} path
   * @param {URLSearchParams} params
   * @returns {Observable<any>}
   */
  get(path: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
    return this.http.get(path, { headers: this.setHeaders({}), search: params })
      .catch((err => this.formatErrors(err)))
      .map((res: Response) => res.json());
  }

  /**
   *
   * @param {string} path
   * @param {Object} body
   * @returns {Observable<any>}
   */
  put(path: string, body: Object = {}): Observable<any> {
    let options = new RequestOptions({ headers: this.setHeaders() });
    return this.http.put(
      path,
      body,
      options
    )
      .catch(this.formatErrors)
      .map((res: Response) => res.json());
  }

  /**
   *
   * @param {string} path
   * @param {Object} body
   * @param {Object} headers
   * @returns {Observable<any>}
   */
  post(path: string, body: Object = {}, headers: Object = {}): Observable<any> {
    let options = new RequestOptions({ headers: this.setHeaders(headers) });
    return this.http.post(
      path,
      body,
      options
    ).catch(this.formatErrors)
      .map((res: Response) =>res.json());
  }

  /**
   *
   * @param path
   * @returns {Observable<any>}
   */
  delete(path): Observable<any> {
    return this.http.delete(
      path,
      { headers: this.setHeaders({}) }
    )
      .catch(this.formatErrors)
      .map((res: Response) => res.json());
  }
}
