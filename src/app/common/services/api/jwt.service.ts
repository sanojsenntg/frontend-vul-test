import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';


@Injectable()
export class JwtService {

  /**
   *
   * @param {Router} router
   */
  constructor(private router: Router) { }

  getToken(): String {
    return window.localStorage['jwtToken'];
  }

  saveToken(token: String) {
    window.localStorage['jwtToken'] = token;
  }

  destroyToken() {
    window.localStorage.removeItem('jwtToken');
    window.localStorage.removeItem('currentUser');
    window.localStorage.removeItem('user-details');
  }

  destroyAdminToken() {
    window.localStorage.removeItem('Sessiontoken');
    window.localStorage.removeItem('user_email');
    window.localStorage.removeItem('companydata');
    window.localStorage.removeItem('jwtToken');
    window.localStorage.removeItem('adminUser');
    window.localStorage.removeItem('userMenu');
    window.localStorage.removeItem('user-details');
    window.localStorage.removeItem('SocketSessiontoken');
    window.localStorage['socketFlag'] = '0';
  }

  destroyDispatcherToken() {
    window.localStorage.removeItem('Sessiontoken');
    window.localStorage.removeItem('user_email');
    window.localStorage.removeItem('companydata');
    window.localStorage.removeItem('jwtToken');
    window.localStorage.removeItem('dispatcherUser');
    window.localStorage.removeItem('userMenu');
    window.localStorage.removeItem('user-details');
    window.localStorage.removeItem('SocketSessiontoken');
    window.localStorage['socketFlag'] = '0';
  }

  getUser(): any {
    return JSON.parse(window.localStorage['currentUser']);
  }
  getAdminUser(): any {
    if (window.localStorage['adminUser']) {
      if(window.localStorage['adminUser'])
      return JSON.parse(window.localStorage['adminUser']);
    } else if (window.localStorage['dispatcherUser']) {
      if(window.localStorage['dispatcherUser'])
      return JSON.parse(window.localStorage['dispatcherUser']);
    }
  }

  getDispatcherUser(): any {
    if (window.localStorage['dispatcherUser']) {
      return JSON.parse(window.localStorage['dispatcherUser']);
    } else {
      return false;
    }
  }

  saveAdminUser(token: String, user: any, menu: any) {
    window.localStorage.clear();
    window.localStorage['jwtToken'] = token;
    window.localStorage['adminUser'] = JSON.stringify(user);
    window.localStorage['userMenu'] = JSON.stringify(menu);
    if (user.role == "1") {
      window.localStorage['dispatcherUser'] = JSON.stringify(user);
    }
  }

  saveUser(token: String, user: any, menu: any) {
    window.localStorage.clear();
    window.localStorage['jwtToken'] = token;
    window.localStorage['dispatcherUser'] = JSON.stringify(user);
    window.localStorage['userMenu'] = JSON.stringify(menu);
    if (user.role == "1") {
      window.localStorage['adminUser'] = JSON.stringify(user);
    }
  }
  verifyLogin(url: string): boolean {
    if (window.localStorage['jwtToken']) {
      return true;
    }
    return false;
  }

}
