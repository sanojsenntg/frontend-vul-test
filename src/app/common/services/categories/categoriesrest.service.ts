import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from './../access-control/access-control.service';


@Injectable()


export class CategoriesrestService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private getUrl = environment.apiUrl + '/sub_categories';
  private categoryUrl = environment.apiUrl + '/categories';

  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) {
  }

  public get(params) {
    return this._apiService.post(this.getUrl, params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public searchCategoyByName(params) {
    return this._apiService.post(this.getUrl + '/searchCategoyByName', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public search(params) {
    return this._apiService.post(this.getUrl + '/search', params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getSubCategoryForCategory( params) {
    return this._apiService.post(this.getUrl + '/getSubCategory/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getCategories(params) {
    return this._apiService.post(this.categoryUrl, params)
      .map((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }


  /**
   * Function to save categories
   *
   * @param params
   * @returns {Observable<any>}
   */
  public saveCategories(data) {
    console.log('data>>', data);
    return this._apiService.post(this.getUrl + '/add', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  /**
   * Update delete status for categories
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public updateServiceForDeletion(id) {
    return this._apiService.post(this.getUrl + '/updateForDelete/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateCategoriesForDeletion(id) {
    return this._apiService.post(this.getUrl + '/updateForDelete/', id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public updateCategory(params) {
    return this._apiService.post(this.getUrl + '/updateCategory/', params)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getCategoryById(id) {
    return this._apiService.post(this.getUrl + '/getSingleCategory/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

  public getSubCategoryById(id) {
    return this._apiService.post(this.getUrl + '/getSingleSubCategory/' , id)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res
      });
  }

}
