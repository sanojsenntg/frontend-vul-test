import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { FinanceServicesRestService } from './finance-services-rest.service';
@Injectable()
export class FinanceServicesService {
  constructor(private _FinanceService: FinanceServicesRestService) { }
  public getLastreco(params) {
    return this._FinanceService.getLastReco(params)
      .map((res) => res);
  }
  public getTripsRevenue(params) {
    return this._FinanceService.getTripfromReco(params)
      .map((res) => res);
  }
  public getRecoDetails(params) {
    return this._FinanceService.getRecoDetails(params)
      .map((res) => res);
  }
  public getRecoCount(params) {
    return this._FinanceService.getRecoCount(params)
      .map((res) => res);
  }
  public searchReco(params) {
    return this._FinanceService.searchReco(params)
      .map((res) => res);
  }
}

