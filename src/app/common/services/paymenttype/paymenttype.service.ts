import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { PaymentTypeRestService } from './paymenttyperest.service';

@Injectable()
export class PaymentTypeService {
  constructor(private _paymentTypeRestService: PaymentTypeRestService) { }

  public savePaymentType(data) {
    return this._paymentTypeRestService.savePaymentTypeRest(data)
      .then((res) => res);
  }

  public getPaymentTypes(params) {
    return this._paymentTypeRestService.getPaymentTypes(params)
      .then((res) => res);
  }

  public updatePaymentType(params) {
    return this._paymentTypeRestService.updatePaymentType(params)
      .then((res) => res);
  }

  public getPaymentTypeById(id) {
    return this._paymentTypeRestService.getPaymentTypeById(id)
      .then((res) => res);
  }

  public deletePaymentTypeById(id) {
    return this._paymentTypeRestService.deletePaymentTypeById(id)
      .then((res) => res);
  }

  public updateDeletedStatus(id) {
    return this._paymentTypeRestService.updateDeletedStatus(id)
      .then((res) => res);
  }

}



