import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { ApiService } from '../api/api.service';
import { ZoneRestService } from './zonerest.service';

@Injectable()
export class ZoneService {
  constructor(private _ZoneRestService: ZoneRestService) { }

  public saveZone(data) {
    return this._ZoneRestService.saveZone(data)
      .then((res) => res);
  }

  public getZones(data) {
    return this._ZoneRestService.getZones(data)
      .then((res) => res);
  }

  public getZoneDetails(data){
    return this._ZoneRestService.detailsZones(data)
    .then((res) => res);
  }

  public deleteZones(data){
    return this._ZoneRestService.deleteZones(data)
    .then((res) => res);
  }

  public saveSubZone(data) {
    return this._ZoneRestService.saveSubZone(data)
      .then((res) => res);
  }

  public getSubZones(data) {
    return this._ZoneRestService.getSubZones(data)
      .then((res) => res);
  }

  public detailSubZones(data){
    return this._ZoneRestService.detailSubZones(data)
    .then((res) => res);
  }

  public deleteSubZones(data){
    return this._ZoneRestService.deleteSubZones(data)
    .then((res) => res);
  }
  public renameSubZones(data){
    return this._ZoneRestService.renameSubZones(data)
    .then((res) => res);
  }
  public driverMaping(data){
    return this._ZoneRestService.driverMaping(data)
    .then((res) => res);
  }
  public savePetrolZone(data){
    return this._ZoneRestService.savePetrolZone(data)
    .then((res) => res);
  }
  public listPetrolZone(data){
    return this._ZoneRestService.listPetrolZone(data)
    .then((res) => res);
  }
  public saveHighwayZone(data){
    return this._ZoneRestService.saveHighwayZone(data)
    .then((res) => res);
  }
  public listHighwayZone(data){
    return this._ZoneRestService.listHighwayZone(data)
    .then((res) => res);
  }
}



