import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../api/api.service';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { AccessControlService } from '../access-control/access-control.service';

@Injectable()

export class ZoneRestService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private authUrl = environment.authUrl;
  private ZoneUrl = environment.apiUrl + '/geofencing';
  private subZoneUrl = environment.apiUrl + '/geofencing_subzone';
  private driverMap = environment.apiUrl + '/dashboard/heatmap';
  private multiZone = environment.apiUrl + '/zone';
  /**
  *
  * @param {Http} http
  * @param {ApiService} _apiService
  */
  constructor(private http: Http, private _apiService: ApiService,
    private accessControl: AccessControlService) { };

  /**
   * Save Payment Zone details.
   * @param array data
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public saveZone(data) {
    return this._apiService.post(this.ZoneUrl + '/add-zone', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
   * Get all Payment Zone.
   * @param params
   * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
   */
  public getZones(data) {
    return this._apiService.post(this.ZoneUrl + '/zone-list', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  /**
  * Get all Payment Zone.
  * @param params
  * @return {Promise<TResult2|TResult1>|Promise<TResult|any>|Promise<TResult>|Promise<any>}
  */
  public detailsZones(data) {
    return this._apiService.post(this.ZoneUrl + '/zone-details', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  public deleteZones(data) {
    return this._apiService.post(this.ZoneUrl + '/delete-zone', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  public saveSubZone(data) {
    return this._apiService.post(this.subZoneUrl + '/add-zone', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  public getSubZones(data) {
    return this._apiService.post(this.subZoneUrl + '/zone-list', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }

  public detailSubZones(data) {
    return this._apiService.post(this.subZoneUrl + '/zone-details', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public deleteSubZones(data) {
    return this._apiService.post(this.subZoneUrl + '/delete-zone', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public renameSubZones(data) {
    return this._apiService.post(this.subZoneUrl + '/update-zone-details', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public driverMaping(data) {
    return this._apiService.post(this.driverMap, data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public savePetrolZone(data) {
    return this._apiService.post(this.multiZone + '/add_petrol_zones', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public saveHighwayZone(data) {
    return this._apiService.post(this.multiZone + '/add_highway_zones', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public listPetrolZone(data) {
    return this._apiService.post(this.multiZone + '/list_petrol_zones', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
  public listHighwayZone(data) {
    return this._apiService.post(this.multiZone + '/list_highway_zones', data)
      .toPromise()
      .then((res) => {
        if (this.accessControl.checkSession(res.status))
          return res;
      });
  }
}
