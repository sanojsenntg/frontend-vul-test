
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeDifference2'
})
export class TimeDifferencePipe2 implements PipeTransform {
  transform(a: any, b?: any, c?: any): any {
    let date1 = new Date(a).getTime();
    let date2 = new Date(b).getTime();
    let time
    if(c)
    time = date1 - (date2 - (c * 60000));
    else
    time = date1 - date2; //msec
    let hoursDiff = time / 1000; //secs
    var output = "";
    if (hoursDiff <= 60) {
      let var1 = hoursDiff;
      output += Math.round(var1) == 1 ? Math.round(var1) + " second" : Math.round(var1) + " seconds";
    } else if (hoursDiff < 60 * 60) {
      let var1 = Math.floor(hoursDiff / 60);
      output += var1 == 1 ? var1 + " minute " + (hoursDiff % 60 == 1 ? hoursDiff % 60 + ' second' : hoursDiff % 60 + ' seconds') : var1 + " minutes " + (hoursDiff % 60 == 1 ? hoursDiff % 60 + ' second' : hoursDiff % 60 + ' seconds');
    } else if (hoursDiff < 60 * 60 * 24) {
      let var1 = Math.floor(hoursDiff / (60 * 60));
      let minutes = Math.floor(hoursDiff / 60) - (var1 * 60)
      let seconds= hoursDiff % 60== 1 ? hoursDiff % 60 + ' second' : hoursDiff % 60 + ' seconds';
      output += var1==1? var1+ " hour "+(minutes == 1 ? minutes + " minute " +  seconds : minutes + " minutes "+seconds) : var1 + " hours "+(minutes == 1 ? minutes + " minute " +  seconds : minutes + " minutes "+seconds);
    } else if (hoursDiff < 60 * 60 * 24 * 30) {
      let var1 = hoursDiff / (60 * 60 * 24);
      output += Math.floor(var1) + " days";
    } else if (hoursDiff < 60 * 60 * 24 * 30 * 12) {
      let var1 = hoursDiff / (60 * 60 * 24 * 30);
      output += Math.floor(var1) + " months";
    }
    return hoursDiff < 0 ? '0 second' : output; // return adjusted time or original string
  }
}